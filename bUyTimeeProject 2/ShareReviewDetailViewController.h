//
//  ShareReviewDetailViewController.h
//  BuyTimee
//
//  Created by User-5 on 18/04/17.
//  Copyright © 2017 User14. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Flurry.h"

@interface ShareReviewDetailViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *phoneContactBtn;
@property (weak, nonatomic) IBOutlet UIButton *facebookBtn;
@property (weak, nonatomic) IBOutlet UIButton *twitterBtn;
@property (weak, nonatomic) IBOutlet UILabel *phoneLIneLbl;
@property (weak, nonatomic) IBOutlet UILabel *facebookLineLbl;
@property (weak, nonatomic) IBOutlet UILabel *twitterLineLbl;
- (IBAction)phoneContactBtnClciked:(id)sender;
- (IBAction)facebookBtnClicked:(id)sender;
- (IBAction)twitterBtnClicked:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *shareWithBtn;
- (IBAction)shareWithBtnClciked:(id)sender;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consPhoneBtnLeft;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consPhoneLineTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consPhonelIneLeft;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consPhoneBtnTop;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consFbBtnTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consFbBtnLeft;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consFbLineLeft;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consFbLineTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consTwitterBtnTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consTwitterBtnLeft;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consTwitterLineLeft;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consTwitterLineTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *shareWithBtnHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *phonelIneLblWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *fbLineLblWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *twitterLineLblWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consPhoneContactBtnHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consFbBtnHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consTwitterBtnHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consShareWithBtnTop;

@end
