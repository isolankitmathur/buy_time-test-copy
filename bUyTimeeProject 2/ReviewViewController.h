//
//  ReviewViewController.h
//  BuyTimee
//
//  Created by mitesh on 29/03/17.
//  Copyright © 2017 User14. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Flurry.h"

@interface ReviewViewController : UIViewController<UIGestureRecognizerDelegate>

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *scrollViewWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *scrollViewHeight;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentViewWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentViewHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *sliderImageHeight;


@property (weak, nonatomic) IBOutlet UILabel *headingLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *headingLblLeft;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *headingLblTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *headingLblWidth;




@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *dateLblTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *dateLblleft;

@property (weak, nonatomic) IBOutlet UILabel *prceLbl;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *priceLblTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *prceLblRight;

@property (weak, nonatomic) IBOutlet UILabel *detailLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *detailLblLeft;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *detaiLblTop;

@property (weak, nonatomic) IBOutlet UILabel *lineLbl;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lineLblTop;

@property (weak, nonatomic) IBOutlet UIImageView *userImageView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *userImgViewLeft;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *userImgViewTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *userImgViewHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *userimgViewWidth;

@property (weak, nonatomic) IBOutlet UILabel *postedByLbl;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *postedByLblLeft;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *postedByLblTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *postedByLblHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *postedByLblWidth;

@property (weak, nonatomic) IBOutlet UIImageView *starOne;
@property (weak, nonatomic) IBOutlet UIImageView *starTwo;
@property (weak, nonatomic) IBOutlet UIImageView *starThree;
@property (weak, nonatomic) IBOutlet UIImageView *starFour;
@property (weak, nonatomic) IBOutlet UIImageView *starFive;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *starImgWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *starImgHeight;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *starOneLeft;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *starOneTop;
@property (weak, nonatomic) IBOutlet UIButton *approveBtn;
@property (weak, nonatomic) IBOutlet UIButton *rejectBnt;
- (IBAction)approveClicked:(id)sender;
- (IBAction)rejectBtnClciked:(id)sender;

@property (weak, nonatomic) IBOutlet UIView *btnBackgroundView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *approveBtnWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *approveBtnHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *approveBtnBottom;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *rejectBtnWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *rejectBtnHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *rejectBtnBottom;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *btnBackgroundViewWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *btnBackgroundViewHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *btnBackgroundViewBottom;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *starTwoLeft;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *starThreeLeft;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *starFourLeft;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *starFiveLeft;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *starTwoTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *starThreeTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *starFourTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *starFiveTop;


@property (weak, nonatomic) IBOutlet UIView *reviewRejectionView;

@property (weak, nonatomic) IBOutlet UIView *viewCircleImgView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewCircleImgViewLeft;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewCircleImgViewTop;
@property (weak, nonatomic) IBOutlet UIImageView *circleImageOne;
@property (weak, nonatomic) IBOutlet UIImageView *circleImageTwo;
@property (weak, nonatomic) IBOutlet UIImageView *circleImageThree;
@property (weak, nonatomic) IBOutlet UIImageView *circleImageFour;
@property (weak, nonatomic) IBOutlet UIImageView *circleImageFive;



///by vs





@property (weak, nonatomic) IBOutlet UIImageView *mainImgView;
@property(nonatomic,strong)UISwipeGestureRecognizer*leftSwipe;
@property(nonatomic,strong)UISwipeGestureRecognizer*rightSwipe;

@property (weak, nonatomic) IBOutlet UIView *viewImageViewAndActivityHolder;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consLblDetailHeight;


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *detailBgViewHeight;
@property (weak, nonatomic) IBOutlet UIView *detailLblBgView;


@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *activityIndiactorLeft;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *activityIndicatorTop;











@end
