//
//  ReviewRejectionViewController.m
//  BuyTimee
//
//  Created by mitesh on 30/03/17.
//  Copyright © 2017 User14. All rights reserved.
//

#import "ReviewRejectionViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "Constants.h"
#import "GlobalFunction.h"

@interface ReviewRejectionViewController ()

@end

@implementation ReviewRejectionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    

    
    // Do any additional setup after loading the view.
    
   // self.reasonTextView.delegate=self;
    self.reasonTextView.layer.borderWidth = 1.0f;
    self.reasonTextView.layer.borderColor = [[UIColor colorWithRed:196.0/255.0f green:196.0/255.0f blue:196.0/255.0f alpha:1.0] CGColor];
    self.reasonTextView.clipsToBounds = YES;
    self.reasonTextView.layer.cornerRadius = 3.0f;

    if(IS_IPHONE_6)
    {
    
    [self.reasonTextView setTextContainerInset:UIEdgeInsetsMake(12,8, 0, 0)];    //top,left,bottom,right
    }
    
    else if (IS_IPHONE_6P)
    {
        [self.reasonTextView setTextContainerInset:UIEdgeInsetsMake(10,8, 0, 0)];    //top,left,bottom,right

    }
    
    else if (IS_IPHONE5 || IS_IPHONE_4_OR_LESS)
    {
        [self.reasonTextView setTextContainerInset:UIEdgeInsetsMake(9,5, 0, 0)];    //top,left,bottom,right

        
    }
    
    self.reasonTextView.delegate = self;
 //  self.reasonTextView.text = @"describe your reason here*";
 //  self.reasonTextView.textColor = [UIColor colorWithRed:196.0/255.0f green:196.0/255.0f blue:196.0/255.0f alpha:1.0];
    
    
    NSAttributedString *star = [[NSAttributedString alloc] initWithString:@"*" attributes:@{ NSForegroundColorAttributeName :[UIColor redColor],
                                                                                             }];
    NSMutableAttributedString *strQuantityTitle = [[NSMutableAttributedString alloc] initWithString:@"describe your reason here" attributes:@{ NSForegroundColorAttributeName : [UIColor colorWithRed:196.0/255.0f green:196.0/255.0f blue:196.0/255.0f alpha:1.0],
                                                                                                                                  }];
    [strQuantityTitle appendAttributedString:star];
    
    [self.reasonTextView setAttributedText:strQuantityTitle];
    
    [self addInputViewToTextField:self.reasonTextView];
    
  //  [self.reasonTextView setDelegate:self];
    
}
-(void)viewDidAppear:(BOOL)animated
{
    NSString* final = @"Reject reservation page";
    
    [Flurry logEvent:final];
    
    [self updateCons];

}

-(void)updateCons
{
    _continueBtn.backgroundColor=[UIColor colorWithRed:63.0/255.0f green:67.0/255.0f blue:70.0/255.0f alpha:1.0];
    _reasonForRejectionLbl.textColor=[UIColor colorWithRed:0.0/255.0f green:0.0/255.0f blue:0.0/255.0f alpha:1.0];
    
    if(IS_IPHONE_6P)
    {
        
        [self.reasonForRejectionLbl setFont:[UIFont fontWithName:@"Lato-Regular" size:18]];
        
        [self.reasonTextView setFont:[UIFont fontWithName:@"Lato-Regular" size:18]];
        
        
        _rejectionLblTop.constant=18;
        _reasonTxtViewTop.constant=19;
        _reasonTxtViewLeft.constant=20;
        _reasonTxtViewWidth.constant=371;
        _reasonTxtViewHeight.constant=285;
        _continueBtnTop.constant=264;
        _continueBtnHeight.constant=50;
        [self.continueBtn.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:20]];
        
        
    }
    
    else if(IS_IPHONE5 || IS_IPHONE_4_OR_LESS)
    {
        
        
        
        [self.reasonForRejectionLbl setFont:[UIFont fontWithName:@"Lato-Regular" size:14]];
        
        [self.reasonTextView setFont:[UIFont fontWithName:@"Lato-Regular" size:14]];
        
        _rejectionLblTop.constant=10;
        _reasonLblLeft.constant=16;
        
        _reasonTxtViewTop.constant=10;
        _reasonTxtViewLeft.constant=15;
        _reasonTxtViewWidth.constant=286;
        _reasonTxtViewHeight.constant=220;
        _continueBtnTop.constant=205;
        _continueBtnHeight.constant=38;
        [self.continueBtn.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:17]];
        
        
    }
    

}
- (void)addInputViewToTextField:(UITextView *)textField{
    
    [self createInputAccessoryView];
    
    [textField setInputAccessoryView:_inputAccView];
    
}


//-(void)createInputAccessoryView{
//    // Create the view that will play the part of the input accessory view.
//    // Note that the frame width (third value in the CGRectMake method)
//    // should change accordingly in landscape orientation. But we don’t care
//    // about that now.
//    _inputAccView = [[UIView alloc] initWithFrame:CGRectMake(10.0, 0.0, self.view.frame.size.width, 40.0)];
//    // Set the view’s background color. We’ ll set it here to gray. Use any color you want.
//    [_inputAccView setBackgroundColor:[UIColor lightGrayColor]];
//    // We can play a little with transparency as well using the Alpha property. Normally
//    // you can leave it unchanged.
//    [_inputAccView setAlpha: 0.8];
//    // If you want you may set or change more properties (ex. Font, background image,e.t.c.).
//    // For now, what we’ ve already done is just enough.
//    // Let’s create our buttons now. First the previous button.
//    // (as well as the methods for the rest of our buttons) later.
//    _btnDone = [UIButton buttonWithType:UIButtonTypeCustom];
//    [_btnDone setFrame:CGRectMake(self.view.frame.size.width - 80.0f, 0.0f, 80.0f, 40.0f)];
//    [_btnDone setTitle:@"Done" forState:UIControlStateNormal];
//    [_btnDone setBackgroundColor:[UIColor clearColor]];
//    [_btnDone setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
//    [_btnDone addTarget:self action:@selector(doneTyping) forControlEvents:UIControlEventTouchUpInside];
//    // Now that our buttons are ready we just have to add them to our view.
//    [_inputAccView addSubview:_btnDone];
//}



-(void)createInputAccessoryView

{
    _inputAccView = [[UIView alloc] initWithFrame:CGRectMake(10.0, 0.0, self.view.frame.size.width, 40.0)];
    [_inputAccView setBackgroundColor:[UIColor colorWithRed:196.0/255.0f green:196.0/255.0f blue:196.0/255.0f alpha:1.0]];
    [_inputAccView setAlpha: 0.8];
    _btnDone = [UIButton buttonWithType:UIButtonTypeCustom];
    [_btnDone setFrame:CGRectMake(self.view.frame.size.width - 80.0f, 0.0f, 80.0f, 40.0f)];
    [_btnDone setTitle:@"Done" forState:UIControlStateNormal];
    [_btnDone setBackgroundColor:[UIColor clearColor]];
    [_btnDone setTitleColor:[UIColor colorWithRed:63.0/255.0f green:67.0/255.0f blue:70.0/255.0f alpha:1.0] forState:UIControlStateNormal];
    [_btnDone addTarget:self action:@selector(doneTyping) forControlEvents:UIControlEventTouchUpInside];
    [_inputAccView addSubview:_btnDone];
}
-(void)doneTyping
{
    [UIView animateWithDuration:0.5
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         
                         [self.reasonTextView resignFirstResponder];
                         
                     } completion:^(BOOL finished) {
                         
                         
                     }];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)textViewDidBeginEditing:(UITextView *)textView{
    
    _txtActiveField = textView;
    
    
    
    [self createInputAccessoryView];
    [_txtActiveField setInputAccessoryView:_inputAccView];
    
    
    
    
    CGRect frame = self.view.frame;
    frame.origin.y = -150;
    
        if([self.reasonTextView.text isEqualToString:@"describe your reason here*"])
        {
            
           // self.reasonTextView.text=@"";
        }
    
    
    NSLog(@"begin editing,,,");
    
    
}






- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    
    
    NSLog(@"range change");
    
    
    
    NSLog(@"range change");
    NSAttributedString *star = [[NSAttributedString alloc] initWithString:@"*" attributes:@{ NSForegroundColorAttributeName :[UIColor redColor],
                                                                                             }];
    NSMutableAttributedString *strQuantityTitle = [[NSMutableAttributedString alloc] initWithString:@"describe your reason here" attributes:@{ NSForegroundColorAttributeName : [UIColor colorWithRed:196.0/255.0f green:196.0/255.0f blue:196.0/255.0f alpha:1.0],
                                                                                                                                               }];
    [strQuantityTitle appendAttributedString:star];
    
   // [self.reasonTextView setAttributedText:strQuantityTitle];
    
    
    NSString *value = [textView.text stringByReplacingCharactersInRange:range withString:text];
    
    
    
    if ([self.reasonTextView.text isEqualToString:@"describe your reason here*"]) {
        self.reasonTextView.text = @"";
        self.reasonTextView.textColor=[UIColor colorWithRed:63.0/255.0f green:67.0/255.0f blue:70.0/255.0f alpha:1.0];
        
    }
    else if (value.length==0)
    {
//        self.reasonTextView.text = @"describe your reason here*";
//        self.reasonTextView.textColor=[UIColor colorWithRed:196.0/255.0f green:196.0/255.0f blue:196.0/255.0f alpha:1.0];
        [self.reasonTextView setAttributedText:strQuantityTitle];
        [self.reasonTextView resignFirstResponder];
        
        
        return NO;
    }
    else if (value.length>0)
    {
        self.reasonTextView.textColor=[UIColor colorWithRed:63.0/255.0f green:67.0/255.0f blue:70.0/255.0f alpha:1.0];
        
    }
    
    
    
    
    return YES;
    

    
    
    if (self.reasonTextView.text.length > 1 && [self.reasonTextView.text isEqualToString:@"describe your reason here*"]) {
        self.reasonTextView.text = @"";
        self.reasonTextView.textColor = [UIColor colorWithRed:63.0/255.0f green:67.0/255.0f blue:70.0/255.0f alpha:1.0];
        
    }
    if ([text isEqualToString:@"\n"]) {
        
        CGRect frame = self.view.frame;
        frame.origin.y = 0;
        
        [UIView animateWithDuration:0.3 animations:^{
            self.view.frame = frame;
        } completion:^(BOOL finished) {
            
        }];
        
        [self.reasonTextView resignFirstResponder];
        return NO;
    }
    return YES;
    
}
- (void)textViewDidChange:(UITextView *)textView{
    
    return;

    if (self.reasonTextView.text.length != 0 && [[self.reasonTextView.text substringFromIndex:1] isEqualToString:@"describe your reason here*"] && [self.reasonTextView.textColor isEqual:[UIColor colorWithRed:196.0/255.0f green:196.0/255.0f blue:196.0/255.0f alpha:1.0]]){
        self.reasonTextView.text = [self.reasonTextView.text substringToIndex:1];
        self.reasonTextView.textColor = [UIColor colorWithRed:196.0/255.0f green:196.0/255.0f blue:196.0/255.0f alpha:1.0]; //optional
        
    }
    else if(self.reasonTextView.text.length == 0){
        self.reasonTextView.text = @"describe your reason here*";
        self.reasonTextView.textColor = [UIColor colorWithRed:196.0/255.0f green:196.0/255.0f blue:196.0/255.0f alpha:1.0];
        [self.reasonTextView setSelectedRange:NSMakeRange(0, 0)];
    }
    
    
    CGRect line = [self.reasonTextView caretRectForPosition:
                   self.reasonTextView.selectedTextRange.start];
    CGFloat overflow = line.origin.y + line.size.height
    - ( self.reasonTextView.contentOffset.y + self.reasonTextView.bounds.size.height
       - self.reasonTextView.contentInset.bottom - self.reasonTextView.contentInset.top );
    if ( overflow > 0 ) {
        // We are at the bottom of the visible text and introduced a line feed, scroll down (iOS 7 does not do it)
        // Scroll caret to visible area
        CGPoint offset = self.reasonTextView.contentOffset;
        offset.y += overflow + 7; // leave 7 pixels margin
        // Cannot animate with setContentOffset:animated: or caret will not appear
        [UIView animateWithDuration:.2 animations:^{
            [self.reasonTextView setContentOffset:offset];
        }];
    }
    
}

-(void)textViewDidEndEditing:(UITextView *)textView{
    
    [self.reasonTextView resignFirstResponder];
    NSAttributedString *star = [[NSAttributedString alloc] initWithString:@"*" attributes:@{ NSForegroundColorAttributeName :[UIColor redColor],
                                                                                             }];
    NSMutableAttributedString *strQuantityTitle = [[NSMutableAttributedString alloc] initWithString:@"describe your reason here" attributes:@{ NSForegroundColorAttributeName : [UIColor colorWithRed:196.0/255.0f green:196.0/255.0f blue:196.0/255.0f alpha:1.0],
                                                                                                                                               }];
    [strQuantityTitle appendAttributedString:star];
    
    // [self.reasonTextView setAttributedText:strQuantityTitle];
    
    
           if ([self.reasonTextView.text isEqualToString:@""])
           {
               [self.reasonTextView setAttributedText:strQuantityTitle];
//            self.reasonTextView.text = @"describe your reason here*";
//            self.reasonTextView.textColor = [UIColor colorWithRed:196.0/255.0f green:196.0/255.0f blue:196.0/255.0f alpha:1.0]; //optional
        }
    
}





- (IBAction)continueBtnClciked:(id)sender {
    
    [GlobalFunction addIndicatorView];
    
    if (self.reasonTextView.text.length<= 0)
    
    {
        [GlobalFunction removeIndicatorView];
        
        [[GlobalFunction shared] showAlertForMessage:@"Please describe your reason first."];
        
        return;
    }
    if ([self.reasonTextView.text isEqualToString:@"describe your reason here*"])
        
    {
        [GlobalFunction removeIndicatorView];
        
        [[GlobalFunction shared] showAlertForMessage:@"Please describe your reason first."];
        
        return;
    }

    
      [self performSelector:@selector(performWebWork) withObject:nil afterDelay:0.4];
    
    
}

-(void)performWebWork
{
    if (![[WebService shared] connected]) {
        
        [GlobalFunction removeIndicatorView];
        
        [[GlobalFunction shared] showAlertForMessage:@"Can't connect to internet, please check and try again."];
        
        return;
    }
    
    //  NSString *timeZoneSeconds = [GlobalFunction getTimeZone];
    
    NSDictionary *dictTaskInfoResponse;
    NSLog(@"task id from managed string..%@",myAppDelegate.taskIdFromReviewClass_forRejectionClass);
    NSLog(@"task id from main array is..%@",myAppDelegate.workerTaskDetail.taskDetailObj.taskid);
    
    dictTaskInfoResponse = [[WebService shared] vendorstatusTaskid:myAppDelegate.taskIdFromReviewClass_forRejectionClass status:@"2" vendorComment:self.reasonTextView.text userId:myAppDelegate.userData.userid];
    
    if (dictTaskInfoResponse) {
        
        if ([[EncryptDecryptFile decrypt:[dictTaskInfoResponse valueForKey:@"responseCode"]] isEqualToString:@"200"]) {
            
            NSLog(@"response message == %@",[EncryptDecryptFile decrypt:[dictTaskInfoResponse valueForKey:@"responseMessage"]]);
            
            [GlobalFunction removeIndicatorView];
            
            NSString* final = @"Reservation rejected by vendor Event";
            
            [Flurry logEvent:final];
            
            [self bioUpdatedSuccefully:[EncryptDecryptFile decrypt:[dictTaskInfoResponse valueForKey:@"responseMessage"]]];
            
            
        }
        else{
            [GlobalFunction removeIndicatorView];
            [[GlobalFunction shared] showAlertForMessage:[EncryptDecryptFile decrypt:[dictTaskInfoResponse valueForKey:@"responseMessage"]]];
        }
    }
    else{
        
        [GlobalFunction removeIndicatorView];
        
        [[GlobalFunction shared] showAlertForMessage:@"Server is down, please try again later."];
    }
    

}

-(void)bioUpdatedSuccefully:(NSString *)response{
    
    [GlobalFunction removeIndicatorView];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"BUY TIMEE" message:response delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    
    alert.tag = 10001;
    
    [alert show];
    
}

-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex{
    
    if (alertView.tag == 10001) {
        
        if(buttonIndex == [alertView cancelButtonIndex]){

  
            CATransition *transitionAnimation = [CATransition animation];
            [transitionAnimation setType:kCATransitionPush];
            [transitionAnimation setSubtype:kCATransitionFromLeft];
            [transitionAnimation setDuration:0.3f];
            [transitionAnimation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
            [transitionAnimation setFillMode:kCAFillModeBoth];
            
            
            
            //  [myAppDelegate.activeTaskVCObj.viewTaskDetailHolder.layer addAnimation:transitionAnimation forKey:@"changeView"];
            
            [myAppDelegate.vcObj.viewLoginContainer.layer addAnimation:transitionAnimation forKey:@"changeView"];
            
            
            //            for(UIView *view in myAppDelegate.vcObj.viewLoginContainer.subviews){
            //
            //                [view removeFromSuperview];
            //
            //            }
            
            
            for(UIView *view in myAppDelegate.vcObj.viewHolderWithHeader.subviews){
                
                [view removeFromSuperview];
                
            }
            
            [myAppDelegate.vcObj performSegueWithIdentifier:@"taskhistory" sender:myAppDelegate.loginVCObj];
            myAppDelegate.vcObj.btnBack.hidden = YES;
            myAppDelegate.vcObj.btnSideBarOpen.hidden =NO;
            // myAppDelegate.vcObj = nil;
            myAppDelegate.termsNconditionVCObj = nil;
            myAppDelegate.stringNameOfChildVC = @"";
            // return;

        }
    }
    
}

@end
