//
//  ReivewDetailViewController.m
//  BuyTimee
//
//  Created by mitesh on 31/03/17.
//  Copyright © 2017 User14. All rights reserved.
//

#import "ReivewDetailViewController.h"
#import "Constants.h"
#import "GlobalFunction.h"
#import "TaskTableViewCell.h"


@interface ReivewDetailViewController ()<UIWebViewDelegate>
{
    int count;
    NSMutableArray *sliderImagesArrayDownloadImages;

}

@end

@implementation ReivewDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
//    UIImage *btnImage = [UIImage imageNamed:@"shareIcon.png"];
//    [self.shareBtn setImage:btnImage forState:UIControlStateNormal];
    
    self.shareBtn.layer.zPosition = 0;
    self.webBlurView.alpha = 0.0;
    self.editOrShareBtn.hidden = YES;
    
    count=0;
    sliderImagesArrayDownloadImages=[[NSMutableArray alloc]init];
    [self.sliderImageView setUserInteractionEnabled:YES];
    self.leftSwipe=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeLeft:)];
    
    self.leftSwipe.direction=UISwipeGestureRecognizerDirectionLeft;
    [self.sliderImageView addGestureRecognizer:self.leftSwipe];
    
    self.rightSwipe=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(rightSwipe:)];
    
    self.rightSwipe.direction=UISwipeGestureRecognizerDirectionRight;
    [self.sliderImageView addGestureRecognizer:self.rightSwipe];
    
    myAppDelegate.isFromPushNotification = false;
    [self ShowImageIndicators];
    
    
}



-(void)viewWillAppear:(BOOL)animated{
    

    [self manageConss];
    
    
    //by kp on 24 oct
    
    NSLog(@" staus code ---------  %@",myAppDelegate.workerTaskDetail.taskDetailObj.soldStatus);
    NSLog(@" staus code ---------  %@",myAppDelegate.workerTaskDetail.taskDetailObj.priceNewVal);
    NSLog(@" purchseID code ---------  %@",myAppDelegate.workerTaskDetail.taskDetailObj.purchaseID);
    NSLog(@" purchseID code ---------  %@",myAppDelegate.workerTaskDetail.taskDetailObj.requestAppointmentStatus);
    
    if ([myAppDelegate.workerTaskDetail.taskDetailObj.requestAppointmentStatus isEqualToString:@"63"])
    {
        self.shareBtn.hidden = YES; // edit by vs for once uncomment it later 22 Oct 18

    }
    else
    {
        self.shareBtn.hidden = NO; // edit by vs for once uncomment it later 22 Oct 18

    }
     if([myAppDelegate.workerTaskDetail.taskDetailObj.soldStatus isEqualToString:@"1"] || (myAppDelegate.workerTaskDetail.taskDetailObj.purchaseID)/* && myAppDelegate.workerTaskDetail.taskDetailObj.priceNewVal !=0 && (myAppDelegate.workerAllTaskDetail.taskDetailObj.discountValue)*/)   // means reservation is sold
    {
        self.lblTransactionLink.hidden=NO;

        NSMutableAttributedString *signUpString = [[NSMutableAttributedString alloc] initWithString:@"Transaction summary"];
        [signUpString appendAttributedString:[[NSAttributedString alloc] initWithString:@""
                                                                         attributes:@{NSUnderlineStyleAttributeName: @(NSUnderlineStyleNone)}]];
        [signUpString addAttributes: @{NSUnderlineStyleAttributeName:[NSNumber numberWithInteger:NSUnderlineStyleSingle]} range:NSMakeRange(0,[signUpString length])];
    
        UIFont *font_regular=[UIFont fontWithName:@"Lato-Bold" size:12.0f];
        [signUpString addAttribute:NSFontAttributeName value:font_regular range:NSMakeRange(0,[signUpString length])];
        self.lblTransactionLink.attributedText = [signUpString copy];
        self.lblTransactionLink.userInteractionEnabled=YES;
        [self.lblTransactionLink addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapOnTransactionLabel:)]];

    }
    else
    {
        self.lblTransactionLink.hidden=YES;
    }

    //*******************************

    
    
    
    
    
    if (myAppDelegate.workerTaskDetail.taskDetailObj.taskImagesArray.count<=0)
    {
        [_activityIndicator stopAnimating];
        [_activityIndicator hidesWhenStopped];
        
        _activityIndicator.hidden=YES;
        
        [self removeSliderImageWithAnimation];
        
    }
    
    [GlobalFunction removeIndicatorView];
    self.firstViewHeadingLabel.text = [myAppDelegate.workerTaskDetail.taskDetailObj.title uppercaseString];
    
//    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
//    [dateFormatter setDateFormat:dateFormatFull];
//    NSDate *date = [dateFormatter dateFromString:myAppDelegate.workerTaskDetail.taskDetailObj.startDate];
//    [dateFormatter setDateFormat:dateFormatDate];
//    NSString *dateString = [dateFormatter stringFromDate:date];
//    
//    
//    NSString *timeString = [GlobalFunction convertTimeFormat:myAppDelegate.workerTaskDetail.taskDetailObj.endDate];
//    
//    NSLog(@"time string is....%@",timeString);
//    
//    
//    dateString = [GlobalFunction convertDateFormat:dateString];
//    
//    
//    NSString *str = [@[dateString, timeString] componentsJoinedByString:@" at "];
//    
//    
//    self.dateLabel.text = str;
    
    

    
    
    NSLog(@"date from server is...%@",myAppDelegate.workerTaskDetail.taskDetailObj.startDate);
    NSLog(@"time from server is...%@",myAppDelegate.workerTaskDetail.taskDetailObj.endDate);
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:dateFormatFull];
    
    NSString *timeString = @"";
    NSString *dateString = @"";
    
    if ([myAppDelegate.workerTaskDetail.taskDetailObj.startDate isEqualToString:@"AnyDate"])
    {
        dateString = @"AnyDate";
    }
    else
    {
        NSDate *date = [dateFormatter dateFromString:myAppDelegate.workerTaskDetail.taskDetailObj.startDate];
        [dateFormatter setDateFormat:dateFormatDate];
        dateString = [dateFormatter stringFromDate:date];
        
        dateString = [GlobalFunction convertDateFormat:dateString];
    }
    
    if ([myAppDelegate.workerTaskDetail.taskDetailObj.endDate isEqualToString:@"AnyTime"])
    {
        timeString = @"AnyTime";
    }
    else
    {
        
        if ([myAppDelegate.workerTaskDetail.taskDetailObj.requestAppointmentStatus isEqualToString:@"63"])
        {
            self.consSecondViewLblAddressHeight.constant = 40;
            //self.addressLabel.numberOfLines = 0;
           // timeString = @"AnyTime";
              timeString = [NSString stringWithFormat:@"%@",myAppDelegate.workerTaskDetail.taskDetailObj.endDate];

        }
        else
        {
        timeString = [GlobalFunction convertTimeFormat:myAppDelegate.workerTaskDetail.taskDetailObj.endDate];
          
        
        }
    }
    
    NSLog(@"Date string is....%@",dateString);
    NSLog(@"time string is....%@",timeString);

        NSString *str = [@[dateString, timeString] componentsJoinedByString:@" at "];
        self.dateLabel.text = str;
    
   
    
    
    NSString *priceVal;
    if ([GlobalFunction checkIfContainFloat:myAppDelegate.workerTaskDetail.taskDetailObj.price]) {
        priceVal=  [NSString stringWithFormat: @"%0.2f", myAppDelegate.workerTaskDetail.taskDetailObj.price];
        
    }else{
        priceVal=  [NSString stringWithFormat: @"%0.0f", myAppDelegate.workerTaskDetail.taskDetailObj.price];
    }
    
    
    NSString *prefixVal =@"$";
    NSString* finalPriceVal;
    if ([priceVal isEqualToString:@"0"])
    {
        finalPriceVal = @"";
    }
    else
    {
     finalPriceVal = [prefixVal stringByAppendingString:priceVal];
    }
     NSString *fees = myAppDelegate.workerTaskDetail.taskDetailObj.feeValue;
    
    int soldValue = [myAppDelegate.workerTaskDetail.taskDetailObj.discountValue intValue];
    
    int price = [priceVal intValue];
    int fee = [fees intValue];
    
    
    NSString *CalculatedString = [NSString stringWithFormat:@"%d", price-fee];
    CalculatedString = [@"$" stringByAppendingString:CalculatedString];
    
    if (soldValue >0)
    {
        self.priceLabel.textColor=[UIColor colorWithRed:214.0/255.0f green:52.0/255.0f blue:47.0/255.0f alpha:1.0];
         self.priceLabel.text=  [NSString stringWithFormat:@"$%@ off",priceVal];
    }
    else
    {
         self.priceLabel.textColor=[UIColor blackColor];
        
        if ([fees isEqualToString:@"0"])
        {
            self.priceLabel.text = finalPriceVal;
            self.priceLabel.textColor=[UIColor blackColor];
        }
        else
        {
            NSString *first = @"(+$";
            
            first = [first stringByAppendingString:fees];
            
            NSString *main = [first stringByAppendingString:@" fee)"];
            
            NSString *str1 = [@[CalculatedString, main]componentsJoinedByString:@" "];
            
            NSRange startRange = [str1 rangeOfString:@"("];
            NSRange endRange = NSMakeRange(startRange.location, str1.length  - startRange.location);
            
            NSMutableAttributedString * string = [[NSMutableAttributedString alloc] initWithString:str1];
            //  [string addAttribute:NSFontAttributeName
            //                 value:[UIFont fontWithName:@"Lato-Regular" size:10] range:endRange];
            
            
            
            //Red and large
            [string setAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"Lato-Regular" size:9.0f], NSForegroundColorAttributeName:[UIColor colorWithRed:63.0/255.0f green:67.0/255.0f blue:70.0/255.0f alpha:0.9]} range:endRange];
            
            //Rest of text -- just futura
            //   [hintText setAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"Futura-Medium" size:16.0f]} range:NSMakeRange(20, hintText.length - 20)];
                        
            self.priceLabel.attributedText = string;
        }
       
    }
    
    if ([myAppDelegate.workerTaskDetail.taskDetailObj.requestAppointmentStatus isEqualToString:@"63"])
    {
        
        NSString *vaaal = [prefixVal stringByAppendingString:[NSString stringWithFormat: @"%@", myAppDelegate.workerTaskDetail.taskDetailObj.priceNewVal]];
        
        self.priceLabel.text = vaaal;

        
        //[NSString stringWithFormat: @"%0.2f", myAppDelegate.workerTaskDetail.taskDetailObj.price];
    }
    
//********************************************************
    
    NSString *detailStr = myAppDelegate.workerTaskDetail.taskDetailObj.taskDescription;
    
    
    self.firstViewDetailLabel.numberOfLines = 0;
    //self.firstViewDetailLabel.text = detailStr;
    CGSize maximumLabelSize = CGSizeMake(350,9999);
    CGSize expectedLabelSize = [detailStr sizeWithFont:_firstViewDetailLabel.font
                                     constrainedToSize:maximumLabelSize
                                         lineBreakMode:_firstViewDetailLabel.lineBreakMode];
    
    CGRect newFrame;
    
    
    
    
    if(IS_IPHONE_6P)
    {
        
        if(expectedLabelSize.height<=70)    //for 6Plus
        {
            NSLog(@"label height less then 77");
            _consFirstViewHeight.constant=127+10;   //for 6Plus
            
        }
        else
        {
            newFrame = _firstViewDetailLabel.frame;
            newFrame.size.height = expectedLabelSize.height+30;
            NSLog(@"address frame ----- %f",newFrame.size.height);
            NSLog(@"address detial.....%@",_firstViewDetailLabel.text);
            _consFirstViewHeight.constant=newFrame.size.height + _firstViewDetailLabel.frame.origin.y-20;
            
        }
        
    }
    
    
    else if (IS_IPHONE_5 || IS_IPHONE_4_OR_LESS)
    {
        if(expectedLabelSize.height<=50)    //for 5
        {
            NSLog(@"label height less then 50");
            _consFirstViewHeight.constant=100+20;   //for 5
        }
        else
        {
            newFrame = _firstViewDetailLabel.frame;
            newFrame.size.height = expectedLabelSize.height+30;
            NSLog(@"address frame ----- %f",newFrame.size.height);
            NSLog(@"address detial.....%@",_firstViewDetailLabel.text);
            if (myAppDelegate.workerTaskDetail.taskDetailObj.taskImagesArray.count<=0)
            {
                NSLog(@"no slider image...");
                _consFirstViewHeight.constant=newFrame.size.height + _firstViewDetailLabel.frame.origin.y+15;
            }
            
            else
                
            {
                _consFirstViewHeight.constant=newFrame.size.height + _firstViewDetailLabel.frame.origin.y+10;
            }
        }
    }
    
    else if(IS_IPHONE_6)
    {
        
        
        if(expectedLabelSize.height<=50)    //for 6 // 54 to 50 by vss on 23 oct 18 to solve first view text cut issue.
        {
            NSLog(@"label height less then 54");
            _consFirstViewHeight.constant=122+10;   //for 6
        }
        else
        {
            newFrame = _firstViewDetailLabel.frame;
            newFrame.size.height = expectedLabelSize.height+30;
            NSLog(@"address frame ----- %f",newFrame.size.height);
            NSLog(@"address detial.....%@",_firstViewDetailLabel.text);
            _consFirstViewHeight.constant=newFrame.size.height + _firstViewDetailLabel.frame.origin.y;
        }
    }
    
    
    if (detailStr.length > 0)
    {
        
        NSMutableAttributedString *signUpString = [[NSMutableAttributedString alloc] initWithString:@"Description:"];
        
        detailStr = [NSString stringWithFormat:@"%@%@",@" ",detailStr];
        
        [signUpString appendAttributedString:[[NSAttributedString alloc] initWithString:detailStr
                                                                             attributes:@{NSUnderlineStyleAttributeName: @(NSUnderlineStyleNone)}]];
        
        [signUpString addAttributes: @{NSUnderlineStyleAttributeName:[NSNumber numberWithInteger:NSUnderlineStyleSingle]} range:NSMakeRange(0,11)];
        
        UIFont *font_regular=[UIFont fontWithName:@"Lato-Regular" size:15.0f];
        
        [signUpString addAttribute:NSFontAttributeName value:font_regular range:NSMakeRange(0,12)];
        
        
        self.firstViewDetailLabel.attributedText = [signUpString copy];
        

        
    }
    else
    {
    
    _firstViewDetailLabel.text=detailStr;
    }
    
    // [self.view layoutIfNeeded];
    
    
    // ********* end handle first view data ************
    
    
    
    // ********* handle second view data ************
    
    
    
    
    //*******************************************
    
    
    //hyper link work done by KP
    
    
  // myAppDelegate.workerTaskDetail.instaLink_Business=@"vs_suryawanshi";//@"https://www.instagram.com/vs_suryawanshi"
    
    if(myAppDelegate.workerTaskDetail.instaLink_Business.length<=0)
    {
        self.labelInstaLink.text=myAppDelegate.workerTaskDetail.instaLink_Business;
    }
    else
    {
        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:myAppDelegate.workerTaskDetail.instaLink_Business attributes:nil];
        NSRange linkRange = NSMakeRange(0, [myAppDelegate.workerTaskDetail.instaLink_Business length]); // for the word "link" in the string above
        NSDictionary *linkAttributes = @{ NSForegroundColorAttributeName : [UIColor colorWithRed:0.05 green:0.4 blue:0.65 alpha:1.0],
                                          NSUnderlineStyleAttributeName : @(NSUnderlineStyleSingle) };
        [attributedString setAttributes:linkAttributes range:linkRange];
        self.labelInstaLink.attributedText = attributedString;
        
        
        self.labelInstaLink.userInteractionEnabled = YES;
        
        [self.labelInstaLink addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapOnLabel:)]];

    }
    
    //_addressViewURLlbl.text=myAppDelegate.workerAllTaskDetail.instaLink_Business;
    
    
    //*******************************************
    

    
    
    
    
    
    self.addressLabel.numberOfLines = 0;

    self.secondViewHeadingLabel.text = [myAppDelegate.workerTaskDetail.name_Business uppercaseString];
    self.addressLabel.text = myAppDelegate.workerTaskDetail.address_Business;
//    self.labelInstaLink.text = myAppDelegate.workerTaskDetail.instaLink_Business;
    
    
    NSString *detailStr1= myAppDelegate.workerTaskDetail.services_Business;
    
    
    
    
    //comment by kp
    
    self.secondViewDetailLabel.numberOfLines = 0;
    //  self.secondViewDetailLabel.text = detailStr1;
    
    CGSize maximumLabelSize1 = CGSizeMake(350,9999);
    CGSize expectedLabelSize1= [detailStr1 sizeWithFont:_secondViewDetailLabel.font
                                      constrainedToSize:maximumLabelSize1
                                          lineBreakMode:_secondViewDetailLabel.lineBreakMode];
    
    CGRect newFrame1;
    
    if(IS_IPHONE_6P)
    {
        if(expectedLabelSize1.height<=96)   //for 6Plus
        {
            NSLog(@"label height less then 96");
            _consSecondViewHeight.constant=165;    //for 6Plus
        }
        else
        {
            newFrame1 = _secondViewDetailLabel.frame;
            newFrame1.size.height = expectedLabelSize1.height+20;
            NSLog(@"time frame ----- %f",newFrame1.size.height);
            NSLog(@"address detial.....%@",_secondViewDetailLabel.text);
            _consSecondViewHeight.constant=newFrame1.size.height + _secondViewDetailLabel.frame.origin.y;
            
        }
        
    }
    
    
    else if(IS_IPHONE_5 || IS_IPHONE_4_OR_LESS)
    {
        
        if(expectedLabelSize1.height<=63)   //for 5
        {
            NSLog(@"label height less then 63");
            _consSecondViewHeight.constant=115;    //124   //for 5
        }
        else
        {
            newFrame1 = _secondViewDetailLabel.frame;
            newFrame1.size.height = expectedLabelSize1.height+40;  //20   by kp on 24 oct
            NSLog(@"time frame ----- %f",newFrame1.size.height);
            NSLog(@"address detial.....%@",_secondViewDetailLabel.text);
            
            if (myAppDelegate.workerTaskDetail.taskDetailObj.taskImagesArray.count<=0)
            {
                NSLog(@"no slider image...");
                _consSecondViewHeight.constant=newFrame1.size.height + _secondViewDetailLabel.frame.origin.y+15;
            }
            
            else
                
            {
                _consSecondViewHeight.constant=newFrame1.size.height + _secondViewDetailLabel.frame.origin.y+28;
                
            }
            
        }
        
        
        
    }
    
    else if(IS_IPHONE_6)
    {
        
        if(expectedLabelSize1.height<=72)   //for 6
        {
            NSLog(@"label height less then 72");
            _consSecondViewHeight.constant=147;    //for 6
        }
        else
        {
            newFrame1 = _secondViewDetailLabel.frame;
            newFrame1.size.height = expectedLabelSize1.height+20;
            NSLog(@"time frame ----- %f",newFrame1.size.height);
            NSLog(@"address detial.....%@",_secondViewDetailLabel.text);
            _consSecondViewHeight.constant=newFrame1.size.height + _secondViewDetailLabel.frame.origin.y;
            
        }
        
    }
    
    _secondViewDetailLabel.text=detailStr1;
    //  [self.view layoutIfNeeded];
    
    
    if(IS_IPHONE_6)
    {
        if(expectedLabelSize.height<=54 && expectedLabelSize1.height<=72){
            _scrollView.scrollEnabled=NO;
        }
        else{
            
            if (myAppDelegate.workerTaskDetail.taskDetailObj.taskImagesArray.count<=0)
            {
                NSInteger totalViewHeightExceptButton;
                NSInteger totalExpectedLabelSizeHeight;
                totalViewHeightExceptButton=self.view.frame.size.height;
                totalExpectedLabelSizeHeight=expectedLabelSize.height+ expectedLabelSize1.height +_sliderImageView.frame.size.height+380;;
                NSLog(@"totalViewHeightExceptButton---- %ld",(long)totalViewHeightExceptButton);
                NSLog(@"totalExpectedLabelSizeHeight---- %ld",(long)totalExpectedLabelSizeHeight);
                
                if(totalExpectedLabelSizeHeight < totalViewHeightExceptButton)
                {
                    NSLog(@"SCROLL NOT ENABLED.....");
                    _scrollView.scrollEnabled=NO;
                }
                else
                {
                    NSLog(@"SCROLL ENABLED.....");
                    _contentViewHeght.constant= expectedLabelSize.height+ expectedLabelSize1.height+400;
                    
                }
            }
            
            _contentViewHeght.constant= _sliderImageView.frame.size.height+expectedLabelSize.height+expectedLabelSize1.height+420; //for 6
            
        }
    }
    else if (IS_IPHONE_6P)
    {
        if(expectedLabelSize.height<=77 && expectedLabelSize1.height<=96){
            _scrollView.scrollEnabled=NO;
        }
        else{
            
            if (myAppDelegate.workerTaskDetail.taskDetailObj.taskImagesArray.count<=0)
                
            {
                NSInteger totalViewHeightExceptButton;
                NSInteger totalExpectedLabelSizeHeight;
                totalViewHeightExceptButton=self.view.frame.size.height;
                totalExpectedLabelSizeHeight=expectedLabelSize.height+ expectedLabelSize1.height +_sliderImageView.frame.size.height+400;;
                NSLog(@"totalViewHeightExceptButton---- %ld",(long)totalViewHeightExceptButton);
                NSLog(@"totalExpectedLabelSizeHeight---- %ld",(long)totalExpectedLabelSizeHeight);
                
                if(totalExpectedLabelSizeHeight < totalViewHeightExceptButton)
                {
                    NSLog(@"SCROLL NOT ENABLED.....");
                    _scrollView.scrollEnabled=NO;
                }
                else
                {
                    NSLog(@"SCROLL ENABLED.....");
                    _contentViewHeght.constant= expectedLabelSize.height+ expectedLabelSize1.height+430;
                    
                }
            }
            
            else{
                _contentViewHeght.constant= _sliderImageView.frame.size.height+expectedLabelSize.height+expectedLabelSize1.height+470; //for 6plus
                
            }
            
            
            
        }
    }
    else if (IS_IPHONE_5 || IS_IPHONE_4_OR_LESS)
    {
        
        NSLog(@"%f",expectedLabelSize.height);
        NSLog(@"%f",expectedLabelSize1.height);

        
        if(expectedLabelSize.height<=50 && expectedLabelSize1.height<=63){
            
            if(IS_IPHONE_4_OR_LESS)
            {
                _scrollView.scrollEnabled=YES;
                _contentViewHeght.constant= _sliderImageView.frame.size.height+expectedLabelSize.height+expectedLabelSize1.height+450;
            }
            
            else
            {
                _scrollView.scrollEnabled=NO;
            }
            
        }
        else{
            
            if (myAppDelegate.workerTaskDetail.taskDetailObj.taskImagesArray.count<=0)
            {
                NSInteger totalViewHeightExceptButton;
                NSInteger totalExpectedLabelSizeHeight;
                totalViewHeightExceptButton=self.view.frame.size.height;
                totalExpectedLabelSizeHeight=expectedLabelSize.height+ expectedLabelSize1.height +_sliderImageView.frame.size.height+350;;
                NSLog(@"totalViewHeightExceptButton---- %ld",(long)totalViewHeightExceptButton);
                NSLog(@"totalExpectedLabelSizeHeight---- %ld",(long)totalExpectedLabelSizeHeight);
                
                if(totalExpectedLabelSizeHeight < totalViewHeightExceptButton)
                {
                    NSLog(@"SCROLL NOT ENABLED.....");
                    _scrollView.scrollEnabled=NO;
                }
                else
                {
                    NSLog(@"SCROLL ENABLED.....");
                    _contentViewHeght.constant= expectedLabelSize.height+ expectedLabelSize1.height+410;
                    
                }
            }
            
            else
            {
                _contentViewHeght.constant= _sliderImageView.frame.size.height+expectedLabelSize.height+expectedLabelSize1.height+410; //for 5
            }
            
        }
    }
    
    
    // ********* end handle second view data ************
    
 //   self.profileImgView.layer.cornerRadius = self.profileImgView.frame.size.height/2;
 //   self.profileImgView.layer.masksToBounds = YES;
    
    if ([myAppDelegate.workerTaskDetail.taskDetailObj.requestAppointmentStatus isEqualToString:@"63"])
    {
        [UIView animateWithDuration:0.5
                              delay:0.0
                            options:UIViewAnimationOptionCurveEaseOut
                         animations:^{
                             [self.view layoutIfNeeded];
                             _consSecondViewHeight.constant=78;
                             [self.view layoutIfNeeded];
                         } completion:^(BOOL finished) {
                             
                             
                         }];
    }
}


-(void)viewDidAppear:(BOOL)animated
{
    
    [self showCircleImage];
    
//    _consSecondViewHeight.constant=0;
    
  
}

-(void)removeSliderImageWithAnimation
{
    //    [UIView animateWithDuration:0.5f
    //                          delay:0.0f
    //                        options:UIViewAnimationOptionCurveEaseInOut
    //                     animations:^{
    //                         [self.view layoutIfNeeded];
    
    _sliderImgViewHeight.constant=0;
    
    //    }completion:NULL];
    
}


-(void)showCircleImage
{
    NSString *oneStr = @"Posted by:";
    NSString *fullname = [[oneStr stringByAppendingString:@" "] stringByAppendingString:myAppDelegate.workerTaskDetail.name_createdBy];
    self.postedByLabel.text = fullname;
    
    if([myAppDelegate.workerTaskDetail.picPath_createdBy isEqualToString:@""])
    {
        NSLog(@"image not exist....");
        self.profileImgView.image=imageUserDefault;
        
    }
    
    else
    {
        
        NSString *fullPath=myAppDelegate.workerTaskDetail.picPath_createdBy;
        NSArray *parts = [fullPath componentsSeparatedByString:@"/"];
        NSString *imageName=[parts lastObject];
        NSArray *imageNameParts = [imageName componentsSeparatedByString:@"."];
        NSString *userImageName = [NSString stringWithFormat:@"%@",[imageNameParts firstObject]];
        NSString *userImageNameWithJPEG = [NSString stringWithFormat:@"%@.jpeg",[imageNameParts firstObject]];
        UIImage *img = [GlobalFunction loadImageOfLogoFromCacheFolder:userImageNameWithJPEG dirname:@"taskImages"];
        
        if (img)
        {
            self.profileImgView.image = img;
            
        }
        else{
            
            NSString *UrlStr =fullPath;
            NSString* webStringURL = [UrlStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            
            
            
            //  NSString *fullPath=[NSString stringWithFormat:urlServer,mainPic];
            
            UIImage *img11 = [GlobalFunction getImageFromURL:webStringURL];
            
            if (img11) {
                
                self.profileImgView.image = img11;
                [GlobalFunction saveimage:img11 imageName:userImageName dirname:@"taskImages"];
            }
            else
            {
                self.profileImgView.image=imageUserDefault;
            }
        }
        
    }
    
    
    NSString *starVal=  [NSString stringWithFormat: @"%@", myAppDelegate.workerTaskDetail.avg_rating_createdBy];
    
    if([starVal isEqualToString:@"0"]){
        self.starOne.image=[UIImage imageNamed:@"greystar"];
        self.starTwo.image=[UIImage imageNamed:@"greystar"];
        self.starThree.image=[UIImage imageNamed:@"greystar"];
        self.starFour.image=[UIImage imageNamed:@"greystar"];
        self.starFive.image=[UIImage imageNamed:@"greystar"];
    }
    else if([starVal isEqualToString:@"1"]){
        self.starOne.image=[UIImage imageNamed:@"redstar"];
        self.starTwo.image=[UIImage imageNamed:@"greystar"];
        self.starThree.image=[UIImage imageNamed:@"greystar"];
        self.starFour.image=[UIImage imageNamed:@"greystar"];
        self.starFive.image=[UIImage imageNamed:@"greystar"];
        
    }
    else if([starVal isEqualToString:@"2"]){
        self.starOne.image=[UIImage imageNamed:@"redstar"];
        self.starTwo.image=[UIImage imageNamed:@"redstar"];
        self.starThree.image=[UIImage imageNamed:@"greystar"];
        self.starFour.image=[UIImage imageNamed:@"greystar"];
        self.starFive.image=[UIImage imageNamed:@"greystar"];
        
    }
    else if([starVal isEqualToString:@"3"]){
        self.starOne.image=[UIImage imageNamed:@"redstar"];
        self.starTwo.image=[UIImage imageNamed:@"redstar"];
        self.starThree.image=[UIImage imageNamed:@"redstar"];
        self.starFour.image=[UIImage imageNamed:@"greystar"];
        self.starFive.image=[UIImage imageNamed:@"greystar"];
        
    }
    else if([starVal isEqualToString:@"4"]){
        self.starOne.image=[UIImage imageNamed:@"redstar"];
        self.starTwo.image=[UIImage imageNamed:@"redstar"];
        self.starThree.image=[UIImage imageNamed:@"redstar"];
        self.starFour.image=[UIImage imageNamed:@"redstar"];
        self.starFive.image=[UIImage imageNamed:@"greystar"];
        
    }
    else if([starVal isEqualToString:@"5"]){
        self.starOne.image=[UIImage imageNamed:@"redstar"];
        self.starTwo.image=[UIImage imageNamed:@"redstar"];
        self.starThree.image=[UIImage imageNamed:@"redstar"];
        self.starFour.image=[UIImage imageNamed:@"redstar"];
        self.starFive.image=[UIImage imageNamed:@"redstar"];
        
    }
    
    [self downloadSliderImages];
}


-(void)ShowImageIndicators{
    
    
    
    
    [_circleImgOne setHidden:YES];
    [_circleImgTwo setHidden:YES];
    [_circleImgThree setHidden:YES];
    [_circleImgFour setHidden:YES];
    [_circleImgFive setHidden:YES];
    
    
    
    if (myAppDelegate.workerTaskDetail.taskDetailObj.taskImagesArray.count>0) {
        [_circleImgOne setHidden:YES];
        [_circleImgTwo setHidden:YES];
        [_circleImgThree setHidden:YES];
        [_circleImgFour setHidden:YES];
        [_circleImgFive setHidden:YES];
        
        
        
        if (myAppDelegate.workerTaskDetail.taskDetailObj.taskImagesArray.count==1) {
            [_circleImgOne setHidden:YES];
            [_circleImgTwo setHidden:YES];
            [_circleImgThree setHidden:YES];
            [_circleImgFour setHidden:YES];
            [_circleImgFive setHidden:YES];
        }else if (myAppDelegate.workerTaskDetail.taskDetailObj.taskImagesArray.count==2) {
            [_circleImgOne setHidden:NO];
            [_circleImgTwo setHidden:NO];
        }else if (myAppDelegate.workerTaskDetail.taskDetailObj.taskImagesArray.count==3) {
            [_circleImgOne setHidden:NO];
            [_circleImgTwo setHidden:NO];
            [_circleImgThree setHidden:NO];
        }else if (myAppDelegate.workerTaskDetail.taskDetailObj.taskImagesArray.count==4) {
            [_circleImgOne setHidden:NO];
            [_circleImgTwo setHidden:NO];
            [_circleImgThree setHidden:NO];
            [_circleImgFour setHidden:NO];
        }else if (myAppDelegate.workerTaskDetail.taskDetailObj.taskImagesArray.count==5) {
            [_circleImgOne setHidden:NO];
            [_circleImgTwo setHidden:NO];
            [_circleImgThree setHidden:NO];
            [_circleImgFour setHidden:NO];
            [_circleImgFive setHidden:NO];
        }
    }
    
    // [myAppDelegate.workerTaskDetail.taskDetailObj.taskImagesArray removeAllObjects];
    
    if (myAppDelegate.workerTaskDetail.taskDetailObj.taskImagesArray.count<=0)
    {
        [self removeSliderImageWithAnimation];
        
    }
    
    
    
}
-(void)swipeLeft:(UISwipeGestureRecognizer*)sender
{
    
    
    NSLog(@"%lu",(unsigned long)sliderImagesArrayDownloadImages.count);
    
    NSLog(@"Count :%lu", sliderImagesArrayDownloadImages.count-1);
    
    
    if(sliderImagesArrayDownloadImages.count==0)
    {
        
    }
    
    else
    {
        
        
        
        
        if (count<sliderImagesArrayDownloadImages.count-1) {
            
            
            count++;
            CATransition *animation = [CATransition animation];
            // [animation setDelegate:self];
            [animation setType:kCATransitionPush];
            [animation setSubtype:kCATransitionFromRight];
            [animation setDuration:0.40];
            [animation setTimingFunction:
             [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear]];
            [_sliderImageView.layer addAnimation:animation forKey:kCATransition];
            
            NSLog(@"%@",sliderImagesArrayDownloadImages);
            NSLog(@"%@",[sliderImagesArrayDownloadImages objectAtIndex:1]);
            
            _sliderImageView.image=[sliderImagesArrayDownloadImages objectAtIndex:count];
            
            
            NSLog(@"Count :%d", count);
            [_circleImgOne setImage:[UIImage imageNamed:@"circleImg"]];
            [_circleImgTwo setImage:[UIImage imageNamed:@"circleImg"]];
            [_circleImgThree setImage:[UIImage imageNamed:@"circleImg"]];
            [_circleImgFour setImage:[UIImage imageNamed:@"circleImg"]];
            [_circleImgFive setImage:[UIImage imageNamed:@"circleImg"]];
            
            
            if (count==0) {
                [_circleImgOne setImage:[UIImage imageNamed:@"circleFillImg"]];
            }else if (count==1){
                [_circleImgTwo setImage:[UIImage imageNamed:@"circleFillImg"]];
            }else if (count==2){
                [_circleImgThree setImage:[UIImage imageNamed:@"circleFillImg"]];
            }else if (count==3){
                [_circleImgFour setImage:[UIImage imageNamed:@"circleFillImg"]];
            }else if (count==4){
                [_circleImgFive setImage:[UIImage imageNamed:@"circleFillImg"]];
            }
            
        }
        
        
    }
    
    
}




-(void)rightSwipe:(UISwipeGestureRecognizer*)sender
{
    
    
    
    if (count>0) {
        
        
        count--;
        CATransition *animation = [CATransition animation];
        // [animation setDelegate:self];
        [animation setType:kCATransitionPush];
        [animation setSubtype:kCATransitionFromLeft];
        [animation setDuration:0.40];
        [animation setTimingFunction:
         [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear]];
        [_sliderImageView.layer addAnimation:animation forKey:kCATransition];
        
        NSLog(@"%@",sliderImagesArrayDownloadImages);
        NSLog(@"%@",[sliderImagesArrayDownloadImages objectAtIndex:1]);
        
        _sliderImageView.image=[sliderImagesArrayDownloadImages objectAtIndex:count];
        
        
        NSLog(@"Count :%d", count);
        [_circleImgOne setImage:[UIImage imageNamed:@"circleImg"]];
        [_circleImgTwo setImage:[UIImage imageNamed:@"circleImg"]];
        [_circleImgThree setImage:[UIImage imageNamed:@"circleImg"]];
        [_circleImgFour setImage:[UIImage imageNamed:@"circleImg"]];
        [_circleImgFive setImage:[UIImage imageNamed:@"circleImg"]];
        
        
        if (count==0) {
            [_circleImgOne setImage:[UIImage imageNamed:@"circleFillImg"]];
        }else if (count==1){
            [_circleImgTwo setImage:[UIImage imageNamed:@"circleFillImg"]];
        }else if (count==2){
            [_circleImgThree setImage:[UIImage imageNamed:@"circleFillImg"]];
        }else if (count==3){
            [_circleImgFour setImage:[UIImage imageNamed:@"circleFillImg"]];
        }else if (count==4){
            [_circleImgFive setImage:[UIImage imageNamed:@"circleFillImg"]];
        }
        
    }
    
    
    return;
    
    
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)downloadSliderImages
{
    //  taskDetailObj.taskImagesArray
    
    
    
    
    
    if (myAppDelegate.workerTaskDetail.taskDetailObj.taskImagesArray.count>0)
        
    {
        
        NSLog(@"%lu",(unsigned long)myAppDelegate.workerTaskDetail.taskDetailObj.taskImagesArray.count);
        
        
        
        for(int i=0; i<myAppDelegate.workerTaskDetail.taskDetailObj.taskImagesArray.count;i++)
        {

            NSString *fullPath=[myAppDelegate.workerTaskDetail.taskDetailObj.taskImagesArray objectAtIndex:i];
            NSArray *parts = [fullPath componentsSeparatedByString:@"/"];
            NSString *imageName=[parts lastObject];
            NSArray *imageNameParts = [imageName componentsSeparatedByString:@"."];
            
            NSString *userImageName = [NSString stringWithFormat:@"%@_%@_%ld",[imageNameParts firstObject],myAppDelegate.workerTaskDetail.taskDetailObj.taskid,(long)i];
            NSString *userImageNameWithJPEG = [NSString stringWithFormat:@"%@_%@_%ld.jpeg",[imageNameParts firstObject],myAppDelegate.workerTaskDetail.taskDetailObj.taskid,(long)i];
            
            UIImage *img = [GlobalFunction loadImageOfLogoFromCacheFolder:userImageNameWithJPEG dirname:@"taskImages"];
            
            if (img) {
                
                [sliderImagesArrayDownloadImages addObject:img];
                
                self.sliderImageView.image = img;
                
            }else{
                
                //NSMutableAttributedString attString
                NSString *UrlStr =fullPath;
                
                NSString* webStringURL = [UrlStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                
                dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH,  0ul);
                dispatch_async(queue, ^{
                    //This is what you will load lazily
                    NSURL *imageURL=[NSURL URLWithString:webStringURL];
                    
                    NSData *images=[NSData dataWithContentsOfURL:imageURL];
                    
                    UIImage *img = [UIImage imageWithData:images];
                    
                    if (img) {
                        
                        [GlobalFunction saveimage:img imageName:userImageName dirname:@"taskImages"];
                        
                        dispatch_sync(dispatch_get_main_queue(), ^{
                            
                            [sliderImagesArrayDownloadImages addObject:img];
                            
                            self.sliderImageView.image = img;
                            
                            if (!self.viewImageViewAndActivityHolder.hidden) {
                                
                                
                                //    [sliderImagesArrayDownloadImages addObject:img];
                                
                                self.sliderImageView.image = img;
                                //  self.activityIndicatorTaskDetailImages.hidden = YES;
                                // [self.activityIndicatorTaskDetailImages stopAnimating];
                            }
                            
                            
                        });
                        
                    }
                    
                    
                });
                
            }
            
        }
        
        
        
        
    }
    
    else
        
        
    {
        
        
        
    }
}

-(void)manageConss
{
    int imagesArrayCountVal=myAppDelegate.workerTaskDetail.taskDetailObj.taskImagesArray.count;
    NSLog(@"images array count value --- %d",imagesArrayCountVal);
    
    
    if(myAppDelegate.workerTaskDetail.taskDetailObj.taskImagesArray.count==0)
    {
        _activityIndicator.hidden=YES;
        
    }
    
    
    _firstViewHeadingLabel.textColor=[UIColor colorWithRed:0.0/255.0f green:0.0/255.0f blue:0.0/255.0f alpha:1.0];
    _dateLabel.textColor=[UIColor colorWithRed:63.0/255.0f green:67.0/255.0f blue:70.0/255.0f alpha:1.0];
    _priceLabel.textColor=[UIColor colorWithRed:0.0/255.0f green:0.0/255.0f blue:0.0/255.0f alpha:1.0];
    _firstViewDetailLabel.textColor=[UIColor colorWithRed:0.0/255.0f green:0.0/255.0f blue:0.0/255.0f alpha:1.0];
    _firstViewLine.backgroundColor=[UIColor colorWithRed:77.0/255.0f green:96.0/255.0f blue:111.0/255.0f alpha:1.0];
    _secondViewHeadingLabel.textColor=[UIColor colorWithRed:0.0/255.0f green:0.0/255.0f blue:0.0/255.0f alpha:1.0];
    _addressLabel.textColor=[UIColor colorWithRed:0.0/255.0f green:0.0/255.0f blue:0.0/255.0f alpha:1.0];
    _secondViewDetailLabel.textColor=[UIColor colorWithRed:0.0/255.0f green:0.0/255.0f blue:0.0/255.0f alpha:1.0];
    _secondViewLine.backgroundColor=[UIColor colorWithRed:77.0/255.0f green:96.0/255.0f blue:111.0/255.0f alpha:1.0];
    _postedByLabel.textColor=[UIColor colorWithRed:0.0/255.0f green:0.0/255.0f blue:0.0/255.0f alpha:1.0];
    
    [self.view layoutIfNeeded];
    
    if(IS_IPHONE_6P)
    {
        
        _contentViewWidth.constant=414;
        _contentViewHeght.constant=736;
        _scrollViewHeight.constant=736;
        _scrollViewWidth.constant=414;
        _sliderImgViewHeight.constant=193;
        
        _activityIndicatorLeft.constant=190;
        _activityIndicatorTop.constant=95;

        
        _viewCircleImgViewLeft.constant=(_contentViewWidth.constant/2)-imagesArrayCountVal*(self.circleImgOne.frame.size.width);
        _viewCircleImgViewTop.constant=175;
        
        
        _consFirstViewHeadingLblTop.constant=7;
        _consFirstViewHeadingLblLeft.constant=10;
        //   _consFirstViewHeight.constant=127;
        _consFirstViewDateLblTop.constant=20;
        
        [self.firstViewHeadingLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:19]];
        [self.dateLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:16]];
        
        _consFirstViewDateLblLeft.constant=10;
        _consFirstViewDateLblTop.constant=6;
        _consPriceLblTop.constant=6;
        _consPriceLblRight.constant=10;
        //  _dateLabel.text=@"Dec. 12 2017 at 1.00 PM";
        [self.firstViewDetailLabel setFont:[UIFont fontWithName:@"Lato-Light" size:16]];
        _consFirstViewDetailLblLeft.constant=10;
        _consFirstViewDEtailLblTop.constant=11;
        
        
        // _consSecondViewHeight.constant=165;
        [self.secondViewHeadingLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:18]];
        [self.addressLabel setFont:[UIFont fontWithName:@"Lato-Light" size:16]];
        [self.labelInstaLink setFont:[UIFont fontWithName:@"Lato-Light" size:16]];
        _consSecondViewHeadingLblTop.constant=2;
        _consAddressLblTop.constant=4;
        _consAddressLblLeft.constant=10;
        _consInstaLinkTop.constant=5;
        
        _consInstaLinkLeft.constant=10;
        _consSecondViewHeadingLblLeft.constant=10;
        
        // _addressLabel.text=@"Eastbluff Village Center, 2525 Eastbluff Dr";
        // _labelInstaLink.text=@"T: 720-9333 (949)";
        [self.secondViewDetailLabel setFont:[UIFont fontWithName:@"Lato-Light" size:16]];
        _consSecondViewDetailLblTop.constant=10;
        _consSecondViewDeatilLblLeft.constant=10;
        
        _consLblDetailSecondViewHeight.constant=60;
        
        _consProfileImgViewWidth.constant=72;
        _consProfileImgViewHeight.constant=73;
        _consProfileImgViewLeft.constant=170;
        _consProfileImgViewTop.constant=16;
        
        [self.postedByLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:18]];
        _consPostedByTop.constant=15;
        _consPostedBuLeft.constant=120;
        _consPostedByLblWidth.constant=185;
        
        _consStarOneLeft.constant=152;
        _consStarOneTop.constant=10;
        _consStarTwoLeft.constant=4;
        _consStarThreeLeft.constant=5;
        _consStarFourLeft.constant=4;
        _consStarFiveLeft.constant=4;
        
        self.consWebViewTop.constant = 22;
        self.consBtnCloseWebViewTop.constant = 5;
        self.consBlurViewHeight.constant = 661;
        self.consBtnCloseWebViewTrailing.constant = 8;
        self.consBtnCloseWebViewHeight.constant = 35;
        self.consBtnCloseWebViewWidth.constant = 35;
        
        self.profileImgView.layer.cornerRadius = _consProfileImgViewHeight.constant/2;
        self.profileImgView.layer.masksToBounds = YES;
    }
    
    
    else if(IS_IPHONE_5 || IS_IPHONE_4_OR_LESS)
    {
        if(IS_IPHONE_4_OR_LESS)
        {
            _consWebViewBottom.constant=100;
        }
        
        _contentViewWidth.constant=320;
        _contentViewHeght.constant=700;
        _scrollViewHeight.constant=568;
        _scrollViewWidth.constant=320;
        
        _activityIndicatorLeft.constant=150;
        _activityIndicatorTop.constant=85;

        
        _viewCircleImgViewLeft.constant=(_contentViewWidth.constant/2)-imagesArrayCountVal*(self.circleImgOne.frame.size.width);
        _viewCircleImgViewTop.constant=131;
        _sliderImgViewHeight.constant=148;
        
        _consFirstViewHeadingLblTop.constant=2;
        _consFirstViewHeadingLblLeft.constant=10;
        [self.firstViewHeadingLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:14.5]];
        _consFirstViewDateLblLeft.constant=10;
        _consFirstViewDateLblTop.constant=-1;
        [self.dateLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:12.5]];
        
        _consPriceLblTop.constant=-1;
        _consPriceLblRight.constant=10;
        [self.priceLabel setFont:[UIFont fontWithName:@"Lato-Bold" size:13]];
        
        [self.firstViewDetailLabel setFont:[UIFont fontWithName:@"Lato-Light" size:12.5]];
        _consFirstViewDetailLblLeft.constant=10;
        _consFirstViewDEtailLblTop.constant=2;
        
        
        [self.firstViewDetailLabel setFont:[UIFont fontWithName:@"Lato-Light" size:12.5]];
        _consDiscountDescriptionLblLeft.constant=10;
        _consDiscountDesciptionLblTop.constant=2;
        self.nsConsLblAddressWidth.constant = 270;
        
        [self.secondViewHeadingLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:14]];
        _consSecondViewHeadingLblTop.constant=-3;
        _consSecondViewHeadingLblLeft.constant=10;
        
        
        [self.addressLabel setFont:[UIFont fontWithName:@"Lato-Light" size:12.5]];
        _consAddressLblTop.constant=-2;
        _consAddressLblLeft.constant=10;
        //_addressLabel.text=@"Eastbluff Village Center, 2525 Eastbluff Dr";
        
        [self.labelInstaLink setFont:[UIFont fontWithName:@"Lato-Light" size:12.5]];
//        [self.lblTransactionLink setFont:[UIFont fontWithName:@"Lato-Light" size:10]];

        
        
        _consInstaLinkTop.constant=0;
        // _labelInstaLink.text=@"T: 720-9333 (949)";
        _consInstaLinkLeft.constant=10;
        
        
        [self.secondViewDetailLabel setFont:[UIFont fontWithName:@"Lato-Light" size:12]];
        _consSecondViewDetailLblTop.constant=7;
        _consSecondViewDeatilLblLeft.constant=10;
        _consLblDetailSecondViewHeight.constant=45;
        
        _consProfileImgViewWidth.constant=55;
        _consProfileImgViewHeight.constant=56;
        _consProfileImgViewLeft.constant=132;
        _consProfileImgViewTop.constant=12;
        
        [self.postedByLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:14]];
        _consPostedByTop.constant=8;
        _consPostedBuLeft.constant=74;
        
        _consStarOneLeft.constant=116;
        _consStarOneTop.constant=4;
        _consStarTwoLeft.constant=0;
        _consStarThreeLeft.constant=0;
        _consStarFourLeft.constant=-1;
        _consStarFiveLeft.constant=0;
        
        self.consWebViewTop.constant = 22;
        self.consBlurViewHeight.constant = 510;
        self.consBtnCloseWebViewTop.constant = 6;
        self.consBtnCloseWebViewWidth.constant = 31;
         self.consBtnCloseWebViewHeight.constant = 31;
        
        self.profileImgView.layer.cornerRadius = _consProfileImgViewHeight.constant/2;
        self.profileImgView.layer.masksToBounds = YES;
        
    }
    
    else if (IS_IPHONE_6)
    {
        _viewCircleImgViewLeft.constant=(375/2)-imagesArrayCountVal*(self.circleImgOne.frame.size.width)-10;
        
        self.consFirstViewDetailLblLeft.constant = 10;
        self.consSecondViewDeatilLblLeft.constant = 10;
        self.consDiscountDescriptionLblLeft.constant=10;
        
        self.consFirstViewDateLblLeft.constant=10;
        self.consFirstViewHeadingLblLeft.constant=10;
        
        self.consSecondViewHeadingLblLeft.constant=10;
        
        self.nsConsLblAddressWidth.constant = 320;
        
        self.consAddressLblLeft.constant=10;
        self.consInstaLinkLeft.constant=10;
        
        self.consWebViewTop.constant = 22;
        self.consBtnCloseWebViewTop.constant = 8;
        self.consBlurViewHeight.constant = 598;
        self.consBtnCloseWebViewTrailing.constant = 7;
           self.profileImgView.layer.cornerRadius = self.profileImgView.frame.size.height/2;
           self.profileImgView.layer.masksToBounds = YES;
        
        
        _activityIndicatorLeft.constant=180;
        _activityIndicatorTop.constant=90;
        _consPriceLblRight.constant=10;


        
        
        
    }
    
     [self.view layoutIfNeeded];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)shareBtnClicked:(id)sender {
    
    
   // [txtActiveField resignFirstResponder];
    

    
    
    
//    TaskTableViewCell *cell = (TaskTableViewCell *)[myAppDelegate.vcObj.tblViewSideBar cellForRowAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:0]];
//    [myAppDelegate.vcObj performSegueWithIdentifier:@"createTask" sender:cell];
//    
//    return;
//    
    [self updateShareWork];
    
  //  [self performSegueWithIdentifier:@"shareReviewDetail" sender:_shareBtn];

}

-(void) gotoCreate
{
    TaskTableViewCell *cell1 = (TaskTableViewCell *)[myAppDelegate.vcObj.tblViewSideBar cellForRowAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:0]];
    [myAppDelegate.vcObj performSegueWithIdentifier:@"createTask" sender:cell1];

}


- (void)actionSheet:(UIActionSheet *)popup clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    switch (popup.tag) {
        case 1: {
            switch (buttonIndex) {
                case 0:
                    [self shareBtnClicked:self.shareBtn];
                  //  [self openPhotos :(int)buttonIndex];
                    break;
                case 1:
                   
                    [self gotoCreate];
                    
                    break;
                default:
                    break;
            }
            break;
        }
        default:
            break;
    }
}


-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    // [GlobalFunction addIndicatorView];
    
    myAppDelegate.superViewClassName = @"ReivewDetailViewController";
    
    if ([segue.identifier isEqualToString:@"shareReviewDetail"]) {
        
        myAppDelegate.vcObj.btnBack.hidden = NO;
        myAppDelegate.vcObj.btnSideBarOpen.hidden = YES;
        myAppDelegate.shareReviewDetailVcObj = nil;
        myAppDelegate.shareReviewDetailVcObj = (ShareReviewDetailViewController *)segue.destinationViewController;
        
    }
    
}


//-(BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
//{
//    
//    if ([identifier isEqualToString:@"shareReviewDetail"]) {
//        
//        
//    }
//    return YES;
//    
//}
-(void) updateShareWork

{
    
    NSDateFormatter *dfForShare = [[NSDateFormatter alloc]init];
    [dfForShare setDateFormat:@"YYYY/MM/dd"];
    
    // I just booked this awesome discount with (vendor) ! You can book one too by using the Buytimee app!
    
//    NSDate *startDate = [dfForShare dateFromString:myAppDelegate.workerTaskDetail.taskDetailObj.startDate];
//    NSString *startDateForShare = [dfForShare stringFromDate:startDate];
//    
//    NSString *timeString = [GlobalFunction convertTimeFormat:myAppDelegate.workerTaskDetail.taskDetailObj.endDate];
//    NSString *dateString1 = [GlobalFunction convertDateFormat:startDateForShare];
//
  
    NSString *reservationStatus = myAppDelegate.reservationBoughtStatus_ForShare;
    NSString *businessName = myAppDelegate.workerTaskDetail.name_Business;
    NSString *startDateShare=myAppDelegate.workerTaskDetail.taskDetailObj.startDate;
    NSString *startTimeShare=myAppDelegate.workerTaskDetail.taskDetailObj.endDate;
    
    NSString *dateString1=@"";
    NSString *timeString=@"";
    
    
    NSString *mainMsgStr = @"I have an appointment for sale with";
    NSString *mainLastMsgStr = @"if anyone is interested check out the  Buy Timee app!";
    
    NSString *discountMsgStr = @"What an amazing deal get";
    NSString *discountvalueStr = [NSString stringWithFormat:@"$%@ off with",myAppDelegate.workerTaskDetail.taskDetailObj.discountValue];
    NSString *discountLastMsgStr = @"Check out the Buy Timee app for more information! ";
    
    NSString *boughtDiscountMsgStr = @"I just booked this awesome discount with";
    NSString *boughtMsgStr = @"I just booked this awesome listing with";
    NSString *boughtLastMsgStr = @"! You can book one too by using the Buy Timee app!";
    
    
    if ([startDateShare isEqualToString:@"AnyDate"])
    {
        
        dateString1 = @"AnyDate";
    }
    else
    {
        NSDate *startDate = [dfForShare dateFromString:myAppDelegate.workerTaskDetail.taskDetailObj.startDate];
        NSString *startDateForShare = [dfForShare stringFromDate:startDate];
        
        dateString1 = [GlobalFunction convertDateFormat:startDateForShare];
    }
    if ([startTimeShare isEqualToString:@"AnyTime"] )
    {
        timeString = @"AnyTime";
    }
    else
    {
        timeString = [GlobalFunction convertTimeFormat:myAppDelegate.workerTaskDetail.taskDetailObj.endDate];
        
    }
    
    
    NSString *finalStr = @"";
    
    int soldValue1 = [myAppDelegate.workerTaskDetail.taskDetailObj.discountValue intValue];
    
    if ([reservationStatus isEqualToString:@"1"])
    {
        
        if (soldValue1 >0)
        {
            
           NSString *str11 = [@[boughtDiscountMsgStr, businessName]componentsJoinedByString:@" "];
           finalStr = [@[str11, boughtLastMsgStr]componentsJoinedByString:@" "];
            
        }
        
        else
        {
            NSString *str11 = [@[boughtMsgStr, businessName]componentsJoinedByString:@" "];
            finalStr = [@[str11, boughtLastMsgStr]componentsJoinedByString:@" "];
            
        }

        
        
        
    }
    
    else
    {
    if (soldValue1 >0)
    {
    
        NSString *str11 = [@[discountMsgStr, discountvalueStr]componentsJoinedByString:@" "];
        NSString *str12 = [@[str11, businessName]componentsJoinedByString:@" "];
        NSString *str13 = [@[str12, timeString]componentsJoinedByString:@" at "];
        NSString *str14 = [@[str13, dateString1]componentsJoinedByString:@", "];
        finalStr = [@[str14, discountLastMsgStr]componentsJoinedByString:@". "];
        
    }
    
    else
    {
        NSString *str1 = [@[mainMsgStr, businessName]componentsJoinedByString:@" "];
        NSString *str2 = [@[str1, timeString]componentsJoinedByString:@" at "];
        NSString *str3 = [@[str2, dateString1]componentsJoinedByString:@", "];
        finalStr = [@[str3, mainLastMsgStr]componentsJoinedByString:@" "];

        
    }
    
    }
//    NSString *strMain0 = [@[myAppDelegate.workerTaskDetail.taskDetailObj.title, self.secondViewHeadingLabel.text] componentsJoinedByString:@" at "];
//    NSString *strMain = [@[strMain0, self.addressLabel.text] componentsJoinedByString:@", "];
//    NSString *strMain1 = [@[dateString1, timeString] componentsJoinedByString:@"  "];
//    NSString *strMain2 = [@[strMain, strMain1] componentsJoinedByString:@" on "];
    
    myAppDelegate.Image_forReviewDetailToShare =nil;
    myAppDelegate.ImgName_forReviewDetailToShare = @"";
    myAppDelegate.DetailString_forReviewDetailToShare = @"";
    
    if (sliderImagesArrayDownloadImages.count == 0)
    {
        
    }
    else
    {
        
        //UIImage *imageShare = [sliderImagesArrayDownloadImages objectAtIndex:0];
        //NSString *js = @"";
        
        UIImage *imgShare =  [sliderImagesArrayDownloadImages objectAtIndex:0];
        NSString *imgNameShare =  @"ShareImg";
        
        myAppDelegate.Image_forReviewDetailToShare = imgShare;
        myAppDelegate.ImgName_forReviewDetailToShare = imgNameShare;
    }
    
    
    myAppDelegate.DetailString_forReviewDetailToShare = finalStr;
    
}


-(void)handleTapOnTransactionLabel:(UISwipeGestureRecognizer*)sender
{
//    [_instaWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"about:blank"]]];
    
    [GlobalFunction addIndicatorView];

    NSLog(@"transaction label tap");
    NSLog(@"%@",myAppDelegate.workerTaskDetail.taskDetailObj.taskid);
    NSLog(@"%@",myAppDelegate.userData.userid);
    
    [self performSelector:@selector(webServiceCall) withObject:nil afterDelay:2.0];

}

-(void)webServiceCall

{
    
    NSLog(@"method call");
    
//    NSDictionary *dictResult = [webServicesShared webViewInfoUserId:myAppDelegate.userData.userid  reservationId:myAppDelegate.workerTaskDetail.taskDetailObj.taskid];
    
    
    NSString *timeZoneSeconds = [GlobalFunction getTimeZone];
    NSString*purchaseID = myAppDelegate.workerTaskDetail.taskDetailObj.purchaseID;
    NSLog(@"webServiceCall purchseID code --  %@",myAppDelegate.workerTaskDetail.taskDetailObj.purchaseID);
    if(!purchaseID)
    {
        purchaseID = @"";
    }
    
    NSDictionary *dictResult = [webServicesShared webViewInfoUserId:myAppDelegate.userData.userid  reservationId:myAppDelegate.workerTaskDetail.taskDetailObj.taskid withPurchaseID:purchaseID timeZone:timeZoneSeconds];
    
    if(dictResult!=nil)
    {
        if ([[EncryptDecryptFile decrypt:[dictResult valueForKey:@"responseCode"]] isEqualToString:@"200"])
        {
            _instaWebView.delegate = self;
            [UIView animateWithDuration:0.5
                                  delay:0.0
                                options:UIViewAnimationOptionCurveEaseOut
                             animations:^{
                                 
                                 _webBlurView.alpha=1.0;
                                 
                             } completion:^(BOOL finished) {
                                 
                                 
                             }];
            
            
            NSString *htmlTransactionInfo=[EncryptDecryptFile decrypt:[dictResult valueForKey:@"data"]];
            [_instaWebView loadHTMLString:htmlTransactionInfo baseURL:nil];
            
        }
    }
    else
    {
        [[GlobalFunction shared] showAlertForMessage:@"Can't connect to internet, please check and try again."];
        
        [GlobalFunction removeIndicatorView];
    }

}

-(void)handleTapOnLabel:(UISwipeGestureRecognizer*)sender
{
    [GlobalFunction addIndicatorView];

//    [_instaWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"about:blank"]]];

    
    _instaWebView.delegate = self;
    
    
    [UIView animateWithDuration:0.5
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         
                         _webBlurView.alpha=1.0;
                         
                     } completion:^(BOOL finished) {
                         
                         
                     }];
    
    NSLog(@"hyper link tap .... ");
    
    //Create a URL object.
    
    //  NSString *mainURL = @"http://google.com"; https://www.instagram.com/
    
    
    NSArray *components = [self.labelInstaLink.text componentsSeparatedByString:@"/"];
    NSString *userName = [components lastObject];
    NSString *prefix = @"https://www.instagram.com/";
    prefix = [prefix stringByAppendingString:userName];
    NSLog(@"%@", prefix);
    NSString *combined = [NSString stringWithFormat:@"%@%@", prefix,userName];
    NSLog(@"%@", combined);
    NSURL *url = [NSURL URLWithString: prefix];
    // NSURL *url = [NSURL URLWithString: mainURL];
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    [_instaWebView loadRequest:requestObj];
    
    
}
-(void)webViewDidFinishLoad:(UIWebView *)webView{
    [GlobalFunction removeIndicatorView];
}

-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    [GlobalFunction removeIndicatorView];
}


- (IBAction)pressCloseWebBtn:(id)sender
{

    
    [UIView animateWithDuration:0.5
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         
                         self.webBlurView.alpha=0.0;
                         
                     } completion:^(BOOL finished) {
                         
                         
                     }];
    
    [_instaWebView stopLoading];
    
}
- (IBAction)editOrshareClicked:(id)sender
{
    
    UIActionSheet *popup = [[UIActionSheet alloc] initWithTitle:@"Select option:" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:
                            @"Share",
                            @"Edit", nil];
    popup.tag = 1;
    
    //    myAppDelegate.actionS=popup;
    //    myAppDelegate.isActionOpen=true;
    [popup showInView:[UIApplication sharedApplication].keyWindow];
    
}
@end
