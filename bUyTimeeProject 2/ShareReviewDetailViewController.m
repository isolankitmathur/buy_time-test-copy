//
//  ShareReviewDetailViewController.m
//  BuyTimee
//
//  Created by User-5 on 18/04/17.
//  Copyright © 2017 User14. All rights reserved.
//

#import "ShareReviewDetailViewController.h"
#import "Constants.h"
#import <MessageUI/MessageUI.h>
#import <Social/Social.h>
#import "GlobalFunction.h"
@interface ShareReviewDetailViewController ()

@end

@implementation ShareReviewDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    self.shareWithBtn.userInteractionEnabled = NO;
    
  //myAppDelegate.reservationBoughtStatus_ForShare = @"";
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    [GlobalFunction addIndicatorView];
    if ([segue.identifier isEqualToString:@"shareReviewDetail"]) {
        
        myAppDelegate.superViewClassName = @"";
        myAppDelegate.shareReviewDetailVcObj = nil;
        myAppDelegate.shareReviewDetailVcObj = (ShareReviewDetailViewController *)segue.destinationViewController;
        
    }
}


-(void)viewDidAppear:(BOOL)animated
{
    
    NSString* final = @"Share reservation";
    
    [Flurry logEvent:final];
    
    NSLog(@"after cancel view did appear call");
   NSLog(@"y is..%f",self.view.frame.origin.y);
    
    NSLog(@"%f",_consPhoneBtnTop.constant);
    
     NSLog(@"%f",_phoneContactBtn.frame.origin.y);
    
}

-(void)viewWillAppear:(BOOL)animated
{
    NSLog(@"y is..%f",self.view.frame.origin.y);
    NSLog(@"after cancel view will appear call with cons");
    NSLog(@"%f",_phoneContactBtn.frame.origin.y);
NSLog(@"%f",_consPhoneBtnTop.constant);
    
     [self updateCons];
   
}

-(void)updateCons
{
    _shareWithBtn.backgroundColor=[UIColor colorWithRed:63.0/255.0f green:67.0/255.0f blue:70.0/255.0f alpha:1.0];
    [_phoneContactBtn setTitleColor:[UIColor colorWithRed:63.0/255.0f green:67.0/255.0f blue:70.0/255.0f alpha:1.0] forState:UIControlStateNormal];
    [_facebookBtn setTitleColor:[UIColor colorWithRed:63.0/255.0f green:67.0/255.0f blue:70.0/255.0f alpha:1.0] forState:UIControlStateNormal];
    [_twitterBtn setTitleColor:[UIColor colorWithRed:63.0/255.0f green:67.0/255.0f blue:70.0/255.0f alpha:1.0] forState:UIControlStateNormal];
    
    
    
    _phoneLIneLbl.backgroundColor=[UIColor colorWithRed:196.0/255.0f green:196.0/255.0f blue:196.0/255.0f alpha:1.0];
    _facebookLineLbl.backgroundColor=[UIColor colorWithRed:196.0/255.0f green:196.0/255.0f blue:196.0/255.0f alpha:1.0];
    _twitterLineLbl.backgroundColor=[UIColor colorWithRed:196.0/255.0f green:196.0/255.0f blue:196.0/255.0f alpha:1.0];
    
    // Do any additional setup after loading the view.
    
    
   
    
    if(IS_IPHONE_6)
    {
        
        [self.view layoutIfNeeded];
  
        [self.shareWithBtn.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:19]];
        
        [self.phoneContactBtn.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:16]];
        [self.facebookBtn.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:16]];
        [self.twitterBtn.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:16]];
        
        if (myAppDelegate.isMessageViewTapped == true)
        {
            _consPhoneBtnTop.constant=187+72;
            myAppDelegate.isMessageViewTapped = false;
        }
        else
        {
            _consPhoneBtnTop.constant=187;
        }
        
        _consPhoneContactBtnHeight.constant=30;
        _consPhoneLineTop.constant=1;
        
        _consFbBtnHeight.constant=30;
        _consFbLineTop.constant=1;
        
        _consTwitterBtnHeight.constant=30;
        _consTwitterLineTop.constant=1;
        
        _consFbBtnTop.constant=15;
        _consTwitterBtnTop.constant=15;
        
        _phonelIneLblWidth.constant=167;
        _fbLineLblWidth.constant=167;
        _twitterLineLblWidth.constant=167;
        
        _consPhonelIneLeft.constant=105;
        _consFbLineLeft.constant=105;
        _consTwitterLineLeft.constant=105;
        
        
        _shareWithBtnHeight.constant=45;
        _consShareWithBtnTop.constant=234;
        
          [self.view layoutIfNeeded];
    }
    
    else if (IS_IPHONE_6P)
    {
          [self.view layoutIfNeeded];
        
        _consPhoneBtnTop.constant=190;
        
        [self.shareWithBtn.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:21]];
        [self.phoneContactBtn.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:18]];
        [self.facebookBtn.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:18]];
        [self.twitterBtn.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:18]];
        
        if (myAppDelegate.isMessageViewTapped == true)
        {
            _consPhoneBtnTop.constant=211+76;
            myAppDelegate.isMessageViewTapped = false;
        }
        else
        {
        _consPhoneBtnTop.constant=211;
        }
        _consPhoneContactBtnHeight.constant=30;
        _consPhoneLineTop.constant=2;
        
        _consFbBtnHeight.constant=30;
        _consFbLineTop.constant=2;
        
        _consTwitterBtnHeight.constant=30;
        _consTwitterLineTop.constant=2;
        
        _consFbBtnTop.constant=19;
        _consTwitterBtnTop.constant=19;
        
        _phonelIneLblWidth.constant=183;
        _fbLineLblWidth.constant=183;
        _twitterLineLblWidth.constant=183;
        
        _consPhonelIneLeft.constant=116;
        _consFbLineLeft.constant=116;
        _consTwitterLineLeft.constant=116;
        
        _consPhoneBtnLeft.constant=108;
        _consFbBtnLeft.constant=108;
        _consTwitterBtnLeft.constant=108;
        
        [_shareWithBtn setTitleEdgeInsets:UIEdgeInsetsMake(-5, 0, 0, 0)];
        
        _shareWithBtnHeight.constant=53;
        
        _consShareWithBtnTop.constant=260;
  
          [self.view layoutIfNeeded];
    }
    
    else if(IS_IPHONE_5 || IS_IPHONE_4_OR_LESS)
    {
        
        [self.shareWithBtn.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:16]];
        
        [self.phoneContactBtn.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:14]];
        
        [self.facebookBtn.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:14]];
        
        [self.twitterBtn.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:14]];
  
        [self.view layoutIfNeeded];

        if (myAppDelegate.isMessageViewTapped == true)
        {
            _consPhoneBtnTop.constant=161+60;
            myAppDelegate.isMessageViewTapped = false;
        }
        else
        {
           _consPhoneBtnTop.constant=161;
            
        }
        
        _consPhoneContactBtnHeight.constant=25;
        
        _consPhoneLineTop.constant=1;
        
        _consFbBtnHeight.constant=25;
        
        _consFbLineTop.constant=1;
        
        
        _consTwitterBtnHeight.constant=25;
        
        _consTwitterLineTop.constant=1;
        
        
        _consFbBtnTop.constant=13;
        
        _consTwitterBtnTop.constant=13;
        
        
        _consPhoneBtnLeft.constant=61;
        
        _consTwitterBtnLeft.constant=61;
        
        _consFbBtnLeft.constant=61;
        
        _phonelIneLblWidth.constant=142;
        
        _fbLineLblWidth.constant=142;
        
        _twitterLineLblWidth.constant=142;
        
        
        _consPhonelIneLeft.constant=90;
        
        _consFbLineLeft.constant=90;
        
        _consTwitterLineLeft.constant=90;
        
        [_shareWithBtn setTitleEdgeInsets:UIEdgeInsetsMake(-2, 0, 0, 0)];
        
        _shareWithBtnHeight.constant=40;
        _consShareWithBtnTop.constant=199;
          [self.view layoutIfNeeded];
    }
      

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)phoneContactBtnClciked:(id)sender
{
    
    MFMessageComposeViewController *controller = [[MFMessageComposeViewController alloc] init];
    if([MFMessageComposeViewController canSendText]) {
         myAppDelegate.isMessageViewTapped = true;
        controller.body = myAppDelegate.DetailString_forReviewDetailToShare;
        controller.messageComposeDelegate = self;
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            //UIViewController *vc = [[[[UIApplication sharedApplication] delegate] window] rootViewController];
            [self presentViewController:controller animated:YES completion:nil];
        }];
    }


}

- (IBAction)facebookBtnClicked:(id)sender
{
    
    
    if (![[WebService shared] connected]) {
        
        [[GlobalFunction shared] showAlertForMessage:@"Can't connect to internet, please check and try again."];
        
        return;
        
    }else{
        
        SLComposeViewController *FBSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
        
        
        [FBSheet setInitialText:myAppDelegate.DetailString_forReviewDetailToShare];
        [FBSheet addImage:myAppDelegate.Image_forReviewDetailToShare];
        
        [self presentViewController:FBSheet animated:YES completion:nil];
        
        [FBSheet setCompletionHandler:^(SLComposeViewControllerResult result) {
            
            NSString *output;
            
            switch (result) {
                    
                case SLComposeViewControllerResultCancelled:
                    
                    output = @"Action Cancelled";
                    
                    break;
                    
                case SLComposeViewControllerResultDone:
                    
                    output = @"Post Successfully";
                    //  [self.doneButton setTitle: @"Done" forState: UIControlStateNormal];
                    break;
                    
                default:
                    
                    break;
                    
            } //check if everything worked properly. Give out a message on the state.
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"BUY TIMEE" message:output delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            
            [alert show];
            
        }];
        
    }
    
}

- (IBAction)twitterBtnClicked:(id)sender
{
    
    
    
    
    if (![[WebService shared] connected]) {
        
        [[GlobalFunction shared] showAlertForMessage:@"Can't connect to internet, please check and try again."];
        
        return;
        
    }else{
        
        if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])
        {
            SLComposeViewController *tweetSheet = [SLComposeViewController
                                                   composeViewControllerForServiceType:SLServiceTypeTwitter];
            [tweetSheet setInitialText:myAppDelegate.DetailString_forReviewDetailToShare];
            
            [tweetSheet addImage:myAppDelegate.Image_forReviewDetailToShare];
            [self presentViewController:tweetSheet animated:YES completion:nil];
            
            
            
            [tweetSheet setCompletionHandler:^(SLComposeViewControllerResult result) {
                
                NSString *output;
                
                switch (result) {
                        
                    case SLComposeViewControllerResultCancelled:
                        
                        output = @"Action Cancelled";
                        
                        break;
                        
                    case SLComposeViewControllerResultDone:
                        
                        output = @"Post Successfull";
                        
                        //  [self.doneButton setTitle: @"Done" forState: UIControlStateNormal];
                        
                        break;
                        
                    default:
                        
                        break;
                        
                } //check if everything worked properly. Give out a message on the state.
                
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"BUY TIMEE" message:output delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                
                [alert show];
                
                
                
                
            }];
            
        }
        
    }
    
    
    

    
    
}
- (IBAction)shareWithBtnClciked:(id)sender
{
    
    
    
    
    
    
}


- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult) result
{
    
    NSLog(@"%f",_phoneContactBtn.frame.origin.y);

    switch (result) {
        case MessageComposeResultCancelled:
            break;
            
        case MessageComposeResultFailed:
        {
            UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Failed to send SMS!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [warningAlert show];
            break;
        }
            
        case MessageComposeResultSent:
            break;
            
        default:
            break;
    }
    
    [controller dismissViewControllerAnimated:YES completion:nil];
}



@end
