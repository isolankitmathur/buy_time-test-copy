//
//  ReviewViewController.m
//  BuyTimee
//
//  Created by mitesh on 29/03/17.
//  Copyright © 2017 User14. All rights reserved.
//

#import "ReviewViewController.h"
#import "Constants.h"
#import "GlobalFunction.h"



@interface ReviewViewController ()
{
    int count;
    NSMutableArray *sliderImagesArrayDownloadImages;
    NSString *stausStr;
}
@end

@implementation ReviewViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    stausStr = @"1";
    
   
   // self.reviewRejectionView.hidden=true;
    
    count=0;
    sliderImagesArrayDownloadImages=[[NSMutableArray alloc]init];

    [self.mainImgView setUserInteractionEnabled:YES];
    self.leftSwipe=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeLeft:)];
    
    self.leftSwipe.direction=UISwipeGestureRecognizerDirectionLeft;
    [self.mainImgView addGestureRecognizer:self.leftSwipe];
    
    self.rightSwipe=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(rightSwipe:)];
    
    self.rightSwipe.direction=UISwipeGestureRecognizerDirectionRight;
    [self.mainImgView addGestureRecognizer:self.rightSwipe];
    
}

-(void)ShowImageIndicators{
    
    
    
    
    if (myAppDelegate.workerTaskDetail.taskDetailObj.taskImagesArray.count<=0)
    {
 
        [_activityIndicator stopAnimating];
        [_activityIndicator hidesWhenStopped];
    
        
        [self removeSliderImageWithAnimation];
        
    }

    
    
    
    [self.circleImageOne setHidden:YES];
    [self.circleImageTwo setHidden:YES];
    [self.circleImageThree setHidden:YES];
    [self.circleImageFour setHidden:YES];
    [self.circleImageFive setHidden:YES];
    
    
    
    if (myAppDelegate.workerTaskDetail.taskDetailObj.taskImagesArray.count>0) {
        [self.circleImageOne setHidden:YES];
        [self.circleImageTwo setHidden:YES];
        [self.circleImageThree setHidden:YES];
        [self.circleImageFour setHidden:YES];
        [self.circleImageFive setHidden:YES];
        
        
        
        if (myAppDelegate.workerTaskDetail.taskDetailObj.taskImagesArray.count==1) {
            [self.circleImageOne setHidden:YES];
            [self.circleImageTwo setHidden:YES];
            [self.circleImageThree setHidden:YES];
            [self.circleImageFour setHidden:YES];
            [self.circleImageFive setHidden:YES];
        }else if (myAppDelegate.workerTaskDetail.taskDetailObj.taskImagesArray.count==2) {
            [self.circleImageOne setHidden:NO];
            [self.circleImageTwo setHidden:NO];
        }else if (myAppDelegate.workerTaskDetail.taskDetailObj.taskImagesArray.count==3) {
            [self.circleImageOne setHidden:NO];
            [self.circleImageTwo setHidden:NO];
            [self.circleImageThree setHidden:NO];
        }else if (myAppDelegate.workerTaskDetail.taskDetailObj.taskImagesArray.count==4) {
            [self.circleImageOne setHidden:NO];
            [self.circleImageTwo setHidden:NO];
            [self.circleImageThree setHidden:NO];
            [self.circleImageFour setHidden:NO];
        }else if (myAppDelegate.workerTaskDetail.taskDetailObj.taskImagesArray.count==5) {
            [self.circleImageOne setHidden:NO];
            [self.circleImageTwo setHidden:NO];
            [self.circleImageThree setHidden:NO];
            [self.circleImageFour setHidden:NO];
            [self.circleImageFive setHidden:NO];
        }
    }
    
    
    
    
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated

{
    [self manageCons];
    
    [GlobalFunction removeIndicatorView];
    myAppDelegate.isFromPushNotification = false;
    
    // [myAppDelegate.workerTaskDetail.taskDetailObj.taskImagesArray removeAllObjects];
    if (myAppDelegate.workerTaskDetail.taskDetailObj.taskImagesArray.count<=0)
    {
        
        [self removeSliderImageWithAnimation];
        
    }
    
    
    NSLog(@"%@",myAppDelegate.workerTaskDetail.taskDetailObj.taskid);
    NSLog(@"%@",myAppDelegate.workerTaskDetail.taskDetailObj.title);
    NSLog(@"%f",myAppDelegate.workerTaskDetail.taskDetailObj.price);
    NSLog(@"%@",myAppDelegate.workerTaskDetail.taskDetailObj.picpath);
    NSLog(@"%@",myAppDelegate.workerTaskDetail.taskDetailObj.reservationType_ForHistory);
    NSLog(@"%@",myAppDelegate.workerTaskDetail.taskDetailObj.reservationStatus_ForHistory);
    NSLog(@"%@",myAppDelegate.workerTaskDetail.userId_createdBy);
    NSLog(@"%@",myAppDelegate.workerTaskDetail.userName_createdBy);
    NSLog(@"%@",myAppDelegate.workerTaskDetail.picPath_createdBy);
    
    // manage task id for rejection screen
    
    myAppDelegate.taskIdFromReviewClass_forRejectionClass = @"";
    myAppDelegate.taskIdFromReviewClass_forRejectionClass = myAppDelegate.workerTaskDetail.taskDetailObj.taskid;
    
    // manage task id for rejection screen
    
    
    /// calculate detail lbl height..
    
    
    
    NSLog(@"line top is..%f",_lineLblTop.constant);
    
    NSString *detailStr = myAppDelegate.workerTaskDetail.taskDetailObj.taskDescription;
    self.headingLabel.text = [myAppDelegate.workerTaskDetail.taskDetailObj.title uppercaseString];
    
    
    
    self.detailLabel.numberOfLines = 0;
    // self.detailLabel.text = detailStr;
    
    CGSize maximumLabelSize = CGSizeMake(350,9999);
    CGSize expectedLabelSize = [detailStr sizeWithFont:self.detailLabel.font
                                     constrainedToSize:maximumLabelSize
                                         lineBreakMode:self.detailLabel.lineBreakMode];
    
    CGRect newFrame;
    
    
    
    
    if(IS_IPHONE_6P)
    {
        
        
        if(expectedLabelSize.height<=120)    //for 6Plus
        {
            NSLog(@"label height less then 120");
            self.detailBgViewHeight.constant=115;   //for 6PLus
        }
        else
        {
            newFrame = self.detailLabel.frame;
            newFrame.size.height = expectedLabelSize.height+10;
            NSLog(@"address frame ----- %f",newFrame.size.height);
            NSLog(@"address detial.....%@",self.detailLabel.text);
            self.detailBgViewHeight.constant=newFrame.size.height-15;
        }
    }
    
    
    if(IS_IPHONE_5 || IS_IPHONE_4_OR_LESS)
    {
        
        
        if(expectedLabelSize.height<=85)    //for 5
        {
            NSLog(@"label height less then 85");
            self.detailBgViewHeight.constant=90;   //for 5
        }
        else
        {
            newFrame = self.detailLabel.frame;
            newFrame.size.height = expectedLabelSize.height+30; // change by vs 20 to 30 9 oct 17 after add description heading...
            NSLog(@"address frame ----- %f",newFrame.size.height);
            NSLog(@"address detial.....%@",self.detailLabel.text);
            
            if (myAppDelegate.workerTaskDetail.taskDetailObj.taskImagesArray.count<=0)
            {
                NSLog(@"no slider image...");
                self.detailBgViewHeight.constant=newFrame.size.height+20;
                
            }
            
            else
            {
                self.detailBgViewHeight.constant=newFrame.size.height+10;
            }
            
        }
    }
    
    
    
    if(IS_IPHONE_6)
    {
        
        if(expectedLabelSize.height<=87)    //for 6
        {
            NSLog(@"label height less then 87");
            self.detailBgViewHeight.constant=100;   //for 6
        }
        else
        {
            newFrame = self.detailLabel.frame;
            newFrame.size.height = expectedLabelSize.height+20;
            NSLog(@"address frame ----- %f",newFrame.size.height);
            NSLog(@"address detial.....%@",self.detailLabel.text);
            self.detailBgViewHeight.constant=newFrame.size.height;
        }
    }
    
    
    if (detailStr.length>0) {
        
    
    
    NSMutableAttributedString *signUpString = [[NSMutableAttributedString alloc] initWithString:@"Description:"];
    
    detailStr = [NSString stringWithFormat:@"%@%@",@" ",detailStr];
    
    [signUpString appendAttributedString:[[NSAttributedString alloc] initWithString:detailStr
                                                                         attributes:@{NSUnderlineStyleAttributeName: @(NSUnderlineStyleNone)}]];
    
    [signUpString addAttributes: @{NSUnderlineStyleAttributeName:[NSNumber numberWithInteger:NSUnderlineStyleSingle]} range:NSMakeRange(0,11)];
    
    UIFont *font_regular=[UIFont fontWithName:@"Lato-Regular" size:15.0f];
    
    [signUpString addAttribute:NSFontAttributeName value:font_regular range:NSMakeRange(0,12)];

    self.detailLabel.attributedText = [signUpString copy];
    
    }
    else
    {
        self.detailLabel.text=detailStr;
    }
        
    
    [self.view layoutIfNeeded];
    
    
    if(IS_IPHONE_6)
    {
        if(expectedLabelSize.height<=87){
            _scrollView.scrollEnabled=NO;
        }
        else{
            
            
            
            if (myAppDelegate.workerTaskDetail.taskDetailObj.taskImagesArray.count<=0)
            {
                {
                    NSInteger totalViewHeightExceptButton;
                    NSInteger totalExpectedLabelSizeHeight;
                    totalViewHeightExceptButton=self.view.frame.size.height-_approveBtn.frame.size.height;
                    totalExpectedLabelSizeHeight=expectedLabelSize.height+_mainImgView.frame.size.height+430;;
                    NSLog(@"totalViewHeightExceptButton---- %ld",(long)totalViewHeightExceptButton);
                    NSLog(@"totalExpectedLabelSizeHeight---- %ld",(long)totalExpectedLabelSizeHeight);
                    
                    if(totalExpectedLabelSizeHeight < totalViewHeightExceptButton)
                    {
                        NSLog(@"SCROLL NOT ENABLED.....");
                        _scrollView.scrollEnabled=NO;
                    }
                    else
                    {
                        NSLog(@"SCROLL ENABLED.....");
                        _contentViewHeight.constant= _mainImgView.frame.size.height+expectedLabelSize.height+450; //for 6
                        
                    }
                }
                
            }
            
            
            else{
                _contentViewHeight.constant= _mainImgView.frame.size.height+expectedLabelSize.height+450; //for 6
            }
            
            
            
            
        }
    }
    else if (IS_IPHONE_6P)
    {
        if(expectedLabelSize.height<=120){
            _scrollView.scrollEnabled=NO;
        }
        else{
            
            if (myAppDelegate.workerTaskDetail.taskDetailObj.taskImagesArray.count<=0)
            {
                {
                    NSInteger totalViewHeightExceptButton;
                    NSInteger totalExpectedLabelSizeHeight;
                    totalViewHeightExceptButton=self.view.frame.size.height-_approveBtn.frame.size.height;
                    totalExpectedLabelSizeHeight=expectedLabelSize.height+_mainImgView.frame.size.height+530;;
                    NSLog(@"totalViewHeightExceptButton---- %ld",(long)totalViewHeightExceptButton);
                    NSLog(@"totalExpectedLabelSizeHeight---- %ld",(long)totalExpectedLabelSizeHeight);
                    
                    if(totalExpectedLabelSizeHeight < totalViewHeightExceptButton)
                    {
                        NSLog(@"SCROLL NOT ENABLED.....");
                        _scrollView.scrollEnabled=NO;
                    }
                    else
                    {
                        NSLog(@"SCROLL ENABLED.....");
                        _contentViewHeight.constant= _mainImgView.frame.size.height+expectedLabelSize.height+550; //for 6PLus
                        
                    }
                }
                
            }
            
            
            
            else{
                _contentViewHeight.constant= _mainImgView.frame.size.height+expectedLabelSize.height+550; //for 6PLus
                
            }
            
            
            
            
        }
    }
    else if (IS_IPHONE_5 || IS_IPHONE_4_OR_LESS)
    {
        if(expectedLabelSize.height<=85 ){
            _scrollView.scrollEnabled=NO;
        }
        else{
            
            if (myAppDelegate.workerTaskDetail.taskDetailObj.taskImagesArray.count<=0)
            {
                {
                    NSInteger totalViewHeightExceptButton;
                    NSInteger totalExpectedLabelSizeHeight;
                    totalViewHeightExceptButton=self.view.frame.size.height-_approveBtn.frame.size.height;
                    totalExpectedLabelSizeHeight=expectedLabelSize.height+_mainImgView.frame.size.height+260;;
                    NSLog(@"totalViewHeightExceptButton---- %ld",(long)totalViewHeightExceptButton);
                    NSLog(@"totalExpectedLabelSizeHeight---- %ld",(long)totalExpectedLabelSizeHeight);
                    
                    if(totalExpectedLabelSizeHeight < totalViewHeightExceptButton)
                    {
                        NSLog(@"SCROLL NOT ENABLED.....");
                        _scrollView.scrollEnabled=NO;
                    }
                    else
                    {
                        NSLog(@"SCROLL ENABLED.....");
                        _contentViewHeight.constant= _mainImgView.frame.size.height+expectedLabelSize.height+500; //for 5
                        
                    }
                }
                
            }
            else{
                
                _contentViewHeight.constant= _mainImgView.frame.size.height+expectedLabelSize.height+500; //for 5
            }
        }
    }
    
    
    
    
//    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
//    [dateFormatter setDateFormat:dateFormatFull];
//    NSDate *date = [dateFormatter dateFromString:myAppDelegate.workerTaskDetail.taskDetailObj.startDate];
//    [dateFormatter setDateFormat:dateFormatDate];
//    NSString *dateString = [dateFormatter stringFromDate:date];
//    
//    
//    NSString *timeString = [GlobalFunction convertTimeFormat:myAppDelegate.workerTaskDetail.taskDetailObj.endDate];
//    
//    NSLog(@"time string is....%@",timeString);
//    
//    
//    dateString = [GlobalFunction convertDateFormat:dateString];
//    
//    
//    NSString *str = [@[dateString, timeString] componentsJoinedByString:@" at "];
//    
//    
//    self.dateLabel.text = str;
    
    NSLog(@"date from server is...%@",myAppDelegate.workerTaskDetail.taskDetailObj.startDate);
    NSLog(@"time from server is...%@",myAppDelegate.workerTaskDetail.taskDetailObj.endDate);
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:dateFormatFull];
    
    NSString *timeString = @"";
    NSString *dateString = @"";
    
    if ([myAppDelegate.workerTaskDetail.taskDetailObj.startDate isEqualToString:@"AnyDate"])
    {
        dateString = @"AnyDate";
    }
    else
    {
        NSDate *date = [dateFormatter dateFromString:myAppDelegate.workerTaskDetail.taskDetailObj.startDate];
        [dateFormatter setDateFormat:dateFormatDate];
        dateString = [dateFormatter stringFromDate:date];
        
        dateString = [GlobalFunction convertDateFormat:dateString];
    }
    
    if ([myAppDelegate.workerTaskDetail.taskDetailObj.endDate isEqualToString:@"AnyTime"])
    {
        timeString = @"AnyTime";
    }
    else
    {
        timeString = [GlobalFunction convertTimeFormat:myAppDelegate.workerTaskDetail.taskDetailObj.endDate];
        
        
    }
    
    NSLog(@"Date string is....%@",dateString);
    NSLog(@"time string is....%@",timeString);
    
    NSString *str = [@[dateString, timeString] componentsJoinedByString:@" at "];
     self.dateLabel.text = str;
 
    
    
    NSString *priceVal;
    if ([GlobalFunction checkIfContainFloat:myAppDelegate.workerTaskDetail.taskDetailObj.price]) {
        priceVal=  [NSString stringWithFormat: @"%0.2f", myAppDelegate.workerTaskDetail.taskDetailObj.price];
        
    }else{
        priceVal=  [NSString stringWithFormat: @"%0.0f", myAppDelegate.workerTaskDetail.taskDetailObj.price];
    }
    
    
    NSString *prefixVal =@"$";
    NSString* finalPriceVal = [prefixVal stringByAppendingString:priceVal];
    
    
   // self.prceLbl.text = finalPriceVal;
    
    NSString *fees = myAppDelegate.workerTaskDetail.taskDetailObj.feeValue;
    
    int price = [priceVal intValue];
    int fee = [fees intValue];
    
    
    NSString *CalculatedString = [NSString stringWithFormat:@"%d", price-fee];
    CalculatedString = [@"$" stringByAppendingString:CalculatedString];
    
    NSString *first = @"(+$";
    
    first = [first stringByAppendingString:fees];
    
    NSString *main = [first stringByAppendingString:@" fee)"];
    
    NSString *str1 = [@[CalculatedString, main]componentsJoinedByString:@" "];
    
    NSRange startRange = [str1 rangeOfString:@"("];
    NSRange endRange = NSMakeRange(startRange.location, str1.length  - startRange.location);
    
    NSMutableAttributedString * string = [[NSMutableAttributedString alloc] initWithString:str1];
    //  [string addAttribute:NSFontAttributeName
    //                 value:[UIFont fontWithName:@"Lato-Regular" size:10] range:endRange];
    
    
    
    //Red and large
    [string setAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"Lato-Regular" size:9.0f], NSForegroundColorAttributeName:[UIColor colorWithRed:63.0/255.0f green:67.0/255.0f blue:70.0/255.0f alpha:0.9]} range:endRange];
    
    //Rest of text -- just futura
    //   [hintText setAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"Futura-Medium" size:16.0f]} range:NSMakeRange(20, hintText.length - 20)];
    
    self.prceLbl.attributedText = string;
    
    
    NSLog(@"%@",myAppDelegate.workerTaskDetail.picPath_acceptedBy);
    NSLog(@"%@",myAppDelegate.workerTaskDetail.picPath_createdBy);
    NSLog(@"%f",myAppDelegate.workerTaskDetail.avgStars_createdBy);
    
    self.userImageView.layer.cornerRadius = self.userImageView.frame.size.height/2;
    self.userImageView.layer.masksToBounds = YES;
    
    [self ShowImageIndicators];
}


-(void)removeSliderImageWithAnimation
{
    _sliderImageHeight.constant=0;
    
}


-(void)viewDidAppear:(BOOL)animated
{
    NSString* final = @"Reservation review request page";
    
    [Flurry logEvent:final];
    
    [self showCircleImage];
    
}

-(void)showCircleImage
{
    NSString *oneStr = @"Posted by:";
    NSString *fullname = [[oneStr stringByAppendingString:@" "] stringByAppendingString:myAppDelegate.workerTaskDetail.name_createdBy];
    self.postedByLbl.text = fullname;
    
    if([myAppDelegate.workerTaskDetail.picPath_createdBy isEqualToString:@""])
    {
        NSLog(@"image not exist....");
        self.userImageView.image=imageUserDefault;
        
    }
    
    else
    {
        
        NSString *fullPath=myAppDelegate.workerTaskDetail.picPath_createdBy;
        NSArray *parts = [fullPath componentsSeparatedByString:@"/"];
        NSString *imageName=[parts lastObject];
        NSArray *imageNameParts = [imageName componentsSeparatedByString:@"."];
        NSString *userImageName = [NSString stringWithFormat:@"%@",[imageNameParts firstObject]];
        NSString *userImageNameWithJPEG = [NSString stringWithFormat:@"%@.jpeg",[imageNameParts firstObject]];
        UIImage *img = [GlobalFunction loadImageOfLogoFromCacheFolder:userImageNameWithJPEG dirname:@"taskImages"];
        
        if (img)
        {
            self.userImageView.image = img;
            
        }
        else{
            
            NSString *UrlStr =fullPath;
            NSString* webStringURL = [UrlStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            
            
            
            //  NSString *fullPath=[NSString stringWithFormat:urlServer,mainPic];
            
            UIImage *img11 = [GlobalFunction getImageFromURL:webStringURL];
            
            if (img11) {
                
                self.userImageView.image = img11;
                [GlobalFunction saveimage:img11 imageName:userImageName dirname:@"taskImages"];
            }
            else
            {
                self.userImageView.image=imageUserDefault;
            }
        }
        
        
    }
    

    NSString *starVal=  [NSString stringWithFormat: @"%@", myAppDelegate.workerTaskDetail.avg_rating_createdBy];
    
    if([starVal isEqualToString:@"0"]){
        self.starOne.image=[UIImage imageNamed:@"greystar"];
        self.starTwo.image=[UIImage imageNamed:@"greystar"];
        self.starThree.image=[UIImage imageNamed:@"greystar"];
        self.starFour.image=[UIImage imageNamed:@"greystar"];
        self.starFive.image=[UIImage imageNamed:@"greystar"];
    }
    else if([starVal isEqualToString:@"1"]){
        self.starOne.image=[UIImage imageNamed:@"redstar"];
        self.starTwo.image=[UIImage imageNamed:@"greystar"];
        self.starThree.image=[UIImage imageNamed:@"greystar"];
        self.starFour.image=[UIImage imageNamed:@"greystar"];
        self.starFive.image=[UIImage imageNamed:@"greystar"];
        
    }
    else if([starVal isEqualToString:@"2"]){
        self.starOne.image=[UIImage imageNamed:@"redstar"];
        self.starTwo.image=[UIImage imageNamed:@"redstar"];
        self.starThree.image=[UIImage imageNamed:@"greystar"];
        self.starFour.image=[UIImage imageNamed:@"greystar"];
        self.starFive.image=[UIImage imageNamed:@"greystar"];
        
    }
    else if([starVal isEqualToString:@"3"]){
        self.starOne.image=[UIImage imageNamed:@"redstar"];
        self.starTwo.image=[UIImage imageNamed:@"redstar"];
        self.starThree.image=[UIImage imageNamed:@"redstar"];
        self.starFour.image=[UIImage imageNamed:@"greystar"];
        self.starFive.image=[UIImage imageNamed:@"greystar"];
        
    }
    else if([starVal isEqualToString:@"4"]){
        self.starOne.image=[UIImage imageNamed:@"redstar"];
        self.starTwo.image=[UIImage imageNamed:@"redstar"];
        self.starThree.image=[UIImage imageNamed:@"redstar"];
        self.starFour.image=[UIImage imageNamed:@"redstar"];
        self.starFive.image=[UIImage imageNamed:@"greystar"];
        
    }
    else if([starVal isEqualToString:@"5"]){
        self.starOne.image=[UIImage imageNamed:@"redstar"];
        self.starTwo.image=[UIImage imageNamed:@"redstar"];
        self.starThree.image=[UIImage imageNamed:@"redstar"];
        self.starFour.image=[UIImage imageNamed:@"redstar"];
        self.starFive.image=[UIImage imageNamed:@"redstar"];
        
    }
    

    [self downloadSliderImages];
}

-(void)manageCons
{
    int imagesArrayCountVal=myAppDelegate.workerTaskDetail.taskDetailObj.taskImagesArray.count;
    NSLog(@"images array count value --- %d",imagesArrayCountVal);
    
    
    
    _approveBtn.backgroundColor=[UIColor colorWithRed:63.0/255.0f green:67.0/255.0f blue:70.0/255.0f alpha:1.0];
    _rejectBnt.backgroundColor=[UIColor colorWithRed:63.0/255.0f green:67.0/255.0f blue:70.0/255.0f alpha:1.0];
    
    _headingLabel.textColor=[UIColor colorWithRed:0.0/255.0f green:0.0/255.0f blue:0.0/255.0f alpha:1.0];
    _dateLabel.textColor=[UIColor colorWithRed:63.0/255.0f green:67.0/255.0f blue:70.0/255.0f alpha:1.0];
    
    _prceLbl.textColor=[UIColor colorWithRed:0.0/255.0f green:0.0/255.0f blue:0.0/255.0f alpha:1.0];
    
    _detailLabel.textColor=[UIColor colorWithRed:0.0/255.0f green:0.0/255.0f blue:0.0/255.0f alpha:1.0];
    
    _lineLbl.backgroundColor=[UIColor colorWithRed:77.0/255.0f green:96.0/255.0f blue:111.0/255.0f alpha:1.0];
    
    _postedByLbl.textColor=[UIColor colorWithRed:0.0/255.0f green:0.0/255.0f blue:0.0/255.0f alpha:1.0];
    
    if(IS_IPHONE_6)
    {
        
        _detaiLblTop.constant=4;
        _viewCircleImgViewLeft.constant=(375/2)-imagesArrayCountVal*(self.circleImageOne.frame.size.width)-10;
        
        //_scrollViewHeight.constant=667;
        //_scrollViewWidth.constant=375;
        
        
        _headingLblLeft.constant=10;
        _dateLblleft.constant=10;
        _detailLblLeft.constant=10;
        
        _activityIndiactorLeft.constant=180;
        _activityIndicatorTop.constant=90;

        
        
    }
    if(IS_IPHONE_6P)
    {
        _activityIndiactorLeft.constant=190;
        _activityIndicatorTop.constant=95;
        

        _contentViewWidth.constant=414;
        //  _contentViewHeight.constant=900;
        _scrollViewHeight.constant=736;
        _scrollViewWidth.constant=414;
        _viewCircleImgViewLeft.constant=(_contentViewWidth.constant/2)-imagesArrayCountVal*(self.circleImageOne.frame.size.width);
        
        _sliderImageHeight.constant=193;
        [self.headingLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:19]];
        
        _headingLblTop.constant=6;
        _headingLblLeft.constant=10;
        _headingLblWidth.constant=412;
        
        _dateLblTop.constant=4;
        _dateLblleft.constant=10;
        
        _priceLblTop.constant=3;
        _prceLblRight.constant=10;
        
        
        [self.dateLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:16]];
        [self.prceLbl setFont:[UIFont fontWithName:@"Lato-Bold" size:17]];
        [self.detailLabel setFont:[UIFont fontWithName:@"Lato-Light" size:16]];
        
        //  _lineLblTop.constant=63;
        _detaiLblTop.constant=8;
        _detailLblLeft.constant=10;
        
        
        _userImgViewTop.constant=40;
        _userimgViewWidth.constant=73;
        _userImgViewHeight.constant=73;
        _userImgViewLeft.constant=170;
        
        
        [self.postedByLbl setFont:[UIFont fontWithName:@"Lato-Regular" size:18]];
        
        
        _postedByLblTop.constant=21;
        _postedByLblLeft.constant=120;
        _postedByLblWidth.constant=185;
        
        
        _starOneLeft.constant=152;
        _starOneTop.constant=11;
        
        // _postedByLbl.text=@"Posted by: Jack Adams";
        
        _approveBtnBottom.constant=75;
        _approveBtnHeight.constant=50;
        _approveBtnWidth.constant=206;
        
        
        _rejectBtnBottom.constant=75;
        _rejectBtnHeight.constant=50;
        _rejectBtnWidth.constant=207;
        _btnBackgroundViewBottom.constant=75;
        _btnBackgroundViewHeight.constant=50;
        _btnBackgroundViewWidth.constant=206;
        
        
        [self.approveBtn.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:20]];
        [self.rejectBnt.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:20]];
        
        
        _approveBtn.titleEdgeInsets=UIEdgeInsetsMake(-4, 0, 0, 0);
        _rejectBnt.titleEdgeInsets=UIEdgeInsetsMake(-4, 0, 0, 0);
        
        _starThreeLeft.constant=4;
        _starFourLeft.constant=5;
        _starFiveLeft.constant=5;
        
        
        _starTwoTop.constant=11;
        _starThreeTop.constant=11;
        _starFourTop.constant=11;
        _starFiveTop.constant=11;
        
    }
    
    
    else if (IS_IPHONE5)
    {
        _activityIndiactorLeft.constant=150;
        _activityIndicatorTop.constant=85;

        
        _contentViewWidth.constant=320;
        //  _contentViewHeight.constant=900;
        _scrollViewHeight.constant=568;
        _scrollViewWidth.constant=320;
        _viewCircleImgViewLeft.constant=(_contentViewWidth.constant/2)-imagesArrayCountVal*(self.circleImageOne.frame.size.width);
        _viewCircleImgViewTop.constant=130;
        
        _sliderImageHeight.constant=147;
        
        [self.headingLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:14]];
        
        _headingLblTop.constant=3;
        _headingLblLeft.constant=10;
        _headingLblWidth.constant=330;
        
        [self.dateLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:12]];
        _dateLblTop.constant=-3;
        _dateLblleft.constant=10;
        
        [self.prceLbl setFont:[UIFont fontWithName:@"Lato-Bold" size:12]];
        _priceLblTop.constant=-5;
        _prceLblRight.constant=20;
        
        [self.detailLabel setFont:[UIFont fontWithName:@"Lato-Light" size:12]];
        _detaiLblTop.constant=-2;
        _detailLblLeft.constant=10;
        
        //  _lineLblTop.constant=43;
        
        _userImgViewTop.constant=31;
        _userimgViewWidth.constant=57;
        _userImgViewHeight.constant=57;
        _userImgViewLeft.constant=132;
        
        [self.postedByLbl setFont:[UIFont fontWithName:@"Lato-Regular" size:14]];
        _postedByLblTop.constant=13;
        _postedByLblLeft.constant=82;
        //_postedByLbl.text=@"Posted by: Jack Adams";
        
        _starOneLeft.constant=116;
        _starThreeLeft.constant=1;
        _starFourLeft.constant=3;
        _starFiveLeft.constant=0;
        _starTwoLeft.constant=2;
        
        _starOneTop.constant=4;
        _starTwoTop.constant=4;
        _starThreeTop.constant=4;
        _starFourTop.constant=4;
        _starFiveTop.constant=4;
        
        _starImgWidth.constant=16;
        _starImgHeight.constant=15;
        
        
        
        _approveBtnBottom.constant=60;
        _approveBtnHeight.constant=38;
        _approveBtnWidth.constant=159;
        
        _rejectBtnBottom.constant=60;
        _rejectBtnHeight.constant=38;
        _rejectBtnWidth.constant=160;
        
        _btnBackgroundViewBottom.constant=60;
        _btnBackgroundViewHeight.constant=38;
        _btnBackgroundViewWidth.constant=155;
        
        [self.approveBtn.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:17]];
        [self.rejectBnt.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:17]];
        _approveBtn.titleEdgeInsets=UIEdgeInsetsMake(-4, 0, 0, 0);
        _rejectBnt.titleEdgeInsets=UIEdgeInsetsMake(-4, 0, 0, 0);
        
    }
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/



-(void)swipeLeft:(UISwipeGestureRecognizer*)sender
{
    
    
    
    NSLog(@"%lu",(unsigned long)sliderImagesArrayDownloadImages.count);
    
    NSLog(@"Count :%lu", sliderImagesArrayDownloadImages.count-1);
    
    
    if(sliderImagesArrayDownloadImages.count==0)
    {
        
    }
    
    else
    {
        
        
        
        
        if (count<sliderImagesArrayDownloadImages.count-1) {
            
            
            count++;
            CATransition *animation = [CATransition animation];
            // [animation setDelegate:self];
            [animation setType:kCATransitionPush];
            [animation setSubtype:kCATransitionFromRight];
            [animation setDuration:0.40];
            [animation setTimingFunction:
             [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear]];
            [self.mainImgView.layer addAnimation:animation forKey:kCATransition];
            
            NSLog(@"%@",sliderImagesArrayDownloadImages);
            NSLog(@"%@",[sliderImagesArrayDownloadImages objectAtIndex:1]);
            
            self.mainImgView.image=[sliderImagesArrayDownloadImages objectAtIndex:count];
            
            
            NSLog(@"Count :%d", count);
                        [self.circleImageOne setImage:[UIImage imageNamed:@"circleImg"]];
                        [self.circleImageTwo setImage:[UIImage imageNamed:@"circleImg"]];
                        [self.circleImageThree setImage:[UIImage imageNamed:@"circleImg"]];
                        [self.circleImageFour setImage:[UIImage imageNamed:@"circleImg"]];
                        [self.circleImageFive setImage:[UIImage imageNamed:@"circleImg"]];
            
            
                        if (count==0) {
                            [self.circleImageOne setImage:[UIImage imageNamed:@"circleFillImg"]];
                        }else if (count==1){
                            [self.circleImageTwo setImage:[UIImage imageNamed:@"circleFillImg"]];
                        }else if (count==2){
                            [self.circleImageThree setImage:[UIImage imageNamed:@"circleFillImg"]];
                        }else if (count==3){
                            [self.circleImageFour setImage:[UIImage imageNamed:@"circleFillImg"]];
                        }else if (count==4){
                            [self.circleImageFive setImage:[UIImage imageNamed:@"circleFillImg"]];
                        }
            
        }
        
        
    }
}




-(void)rightSwipe:(UISwipeGestureRecognizer*)sender
{
    
    
    
    if (count>0) {
        
        
        count--;
        CATransition *animation = [CATransition animation];
        // [animation setDelegate:self];
        [animation setType:kCATransitionPush];
        [animation setSubtype:kCATransitionFromLeft];
        [animation setDuration:0.40];
        [animation setTimingFunction:
         [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear]];
        [self.mainImgView.layer addAnimation:animation forKey:kCATransition];
        
        NSLog(@"%@",sliderImagesArrayDownloadImages);
        NSLog(@"%@",[sliderImagesArrayDownloadImages objectAtIndex:1]);
        
        self.mainImgView.image=[sliderImagesArrayDownloadImages objectAtIndex:count];
        
        
        NSLog(@"Count :%d", count);
                [self.circleImageOne setImage:[UIImage imageNamed:@"circleImg"]];
                [self.circleImageTwo setImage:[UIImage imageNamed:@"circleImg"]];
                [self.circleImageThree setImage:[UIImage imageNamed:@"circleImg"]];
                [self.circleImageFour setImage:[UIImage imageNamed:@"circleImg"]];
                [self.circleImageFive setImage:[UIImage imageNamed:@"circleImg"]];
        
        
                if (count==0) {
                    [self.circleImageOne setImage:[UIImage imageNamed:@"circleFillImg"]];
                }else if (count==1){
                    [self.circleImageTwo setImage:[UIImage imageNamed:@"circleFillImg"]];
                }else if (count==2){
                    [self.circleImageThree setImage:[UIImage imageNamed:@"circleFillImg"]];
                }else if (count==3){
                    [self.circleImageFour setImage:[UIImage imageNamed:@"circleFillImg"]];
                }else if (count==4){
                    [self.circleImageFive setImage:[UIImage imageNamed:@"circleFillImg"]];
                }
        
    }
    
    
    return;
    
    
}

-(void)downloadSliderImages
{
    //  taskDetailObj.taskImagesArray
    
    if (myAppDelegate.workerTaskDetail.taskDetailObj.taskImagesArray.count>0)
        
    {
        
        NSLog(@"%lu",(unsigned long)myAppDelegate.workerTaskDetail.taskDetailObj.taskImagesArray.count);
        
        
        
        for(int i=0; i<myAppDelegate.workerTaskDetail.taskDetailObj.taskImagesArray.count;i++)
        {
            
            
            
            
            NSString *fullPath=[myAppDelegate.workerTaskDetail.taskDetailObj.taskImagesArray objectAtIndex:i];
            NSArray *parts = [fullPath componentsSeparatedByString:@"/"];
            NSString *imageName=[parts lastObject];
            NSArray *imageNameParts = [imageName componentsSeparatedByString:@"."];
            
            NSString *userImageName = [NSString stringWithFormat:@"%@_%@_%ld",[imageNameParts firstObject],myAppDelegate.workerTaskDetail.taskDetailObj.taskid,(long)i];
            NSString *userImageNameWithJPEG = [NSString stringWithFormat:@"%@_%@_%ld.jpeg",[imageNameParts firstObject],myAppDelegate.workerTaskDetail.taskDetailObj.taskid,(long)i];
            
            UIImage *img = [GlobalFunction loadImageOfLogoFromCacheFolder:userImageNameWithJPEG dirname:@"taskImages"];
            
            if (img) {
                
                [sliderImagesArrayDownloadImages addObject:img];
                
                self.mainImgView.image = img;
                
            }else{
                
                
                NSString *UrlStr =fullPath;
                
                NSString* webStringURL = [UrlStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                
                dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH,  0ul);
                dispatch_async(queue, ^{
                    //This is what you will load lazily
                    NSURL *imageURL=[NSURL URLWithString:webStringURL];
                    
                    NSData *images=[NSData dataWithContentsOfURL:imageURL];
                    
                    UIImage *img = [UIImage imageWithData:images];
                    
                    if (img) {
                        
                        [GlobalFunction saveimage:img imageName:userImageName dirname:@"taskImages"];
                        
                        dispatch_sync(dispatch_get_main_queue(), ^{
                            
                            [sliderImagesArrayDownloadImages addObject:img];
                            
                            self.mainImgView.image = img;
                            
                            if (!self.viewImageViewAndActivityHolder.hidden) {
                                
                                
                                //    [sliderImagesArrayDownloadImages addObject:img];
                                
                                self.mainImgView.image = img;
                                //  self.activityIndicatorTaskDetailImages.hidden = YES;
                                // [self.activityIndicatorTaskDetailImages stopAnimating];
                            }
                            
                            
                        });
                        
                    }
                    
                    
                });
                
            }
            
        }
        
        
        
        
    }
    
    else
    {
        
        
        
    }
}




- (IBAction)approveClicked:(id)sender {
    
    
    [GlobalFunction addIndicatorView];
    
    stausStr = @"1";
    
   [self performSelector:@selector(performWebWork) withObject:nil afterDelay:0.4];
}


-(void)performWebWork
{
    if (![[WebService shared] connected]) {
        
        [GlobalFunction removeIndicatorView];
        
        [[GlobalFunction shared] showAlertForMessage:@"Can't connect to internet, please check and try again."];
        
        return;
    }
    
    NSString *timeZoneSeconds = [GlobalFunction getTimeZone];
    
    NSDictionary *dictTaskInfoResponse;
    
    dictTaskInfoResponse = [[WebService shared] vendorstatusTaskid:myAppDelegate.workerTaskDetail.taskDetailObj.taskid status:stausStr vendorComment:@"" userId:myAppDelegate.userData.userid];
    
    if (dictTaskInfoResponse) {
        
        if ([[EncryptDecryptFile decrypt:[dictTaskInfoResponse valueForKey:@"responseCode"]] isEqualToString:@"200"]) {
            
            NSLog(@"response message == %@",[EncryptDecryptFile decrypt:[dictTaskInfoResponse valueForKey:@"responseMessage"]]);
            
            [GlobalFunction removeIndicatorView];
            
            NSString* final = @"Reservation approved by vendor event";
            
            [Flurry logEvent:final];
            
            [self bioUpdatedSuccefully:[EncryptDecryptFile decrypt:[dictTaskInfoResponse valueForKey:@"responseMessage"]]];
            
        }
        else{
            [GlobalFunction removeIndicatorView];
            [[GlobalFunction shared] showAlertForMessage:[EncryptDecryptFile decrypt:[dictTaskInfoResponse valueForKey:@"responseMessage"]]];
        }
    }
    else{
        
        [GlobalFunction removeIndicatorView];
        
        [[GlobalFunction shared] showAlertForMessage:@"Server is down, please try again later."];
    }
}

- (IBAction)rejectBtnClciked:(id)sender {
    
//    stausStr = @"0";
//    
//    [GlobalFunction addIndicatorView];
//    
//    if (![[WebService shared] connected]) {
//        
//        [GlobalFunction removeIndicatorView];
//        
//        [[GlobalFunction shared] showAlertForMessage:@"Can't connect to internet, please check and try again."];
//        
//        return;
//    }

    
    
    
}
- (IBAction)backBtnClciked:(id)sender {
}

-(BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{
    
    if ([identifier isEqualToString:@"reviewRejection"]) {
        
        
    }
    return YES;
    
}



-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    // [GlobalFunction addIndicatorView];
    
    myAppDelegate.superViewClassName = @"ReviewViewController";
    
    if ([segue.identifier isEqualToString:@"reviewRejection"]) {
        
        //    myAppDelegate.TandC_Buyee=@"review_rejection";
        //   myAppDelegate.superViewClassName = @"";
        
        myAppDelegate.vcObj.btnBack.hidden = NO;
        myAppDelegate.vcObj.btnSideBarOpen.hidden = YES;
        myAppDelegate.reviewRejectionVcObj = nil;
        myAppDelegate.reviewRejectionVcObj = (ReviewRejectionViewController *)segue.destinationViewController;
        
    }
    
}
-(void)bioUpdatedSuccefully:(NSString *)response{
    
    [GlobalFunction removeIndicatorView];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"BUY TIMEE" message:response delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    
    alert.tag = 10011;
    
    [alert show];
    
}

-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex{
    
    if (alertView.tag == 10011) {
        
        if(buttonIndex == [alertView cancelButtonIndex]){
            
        
            
            CATransition *transitionAnimation = [CATransition animation];
            [transitionAnimation setType:kCATransitionPush];
            [transitionAnimation setSubtype:kCATransitionFromLeft];
            [transitionAnimation setDuration:0.3f];
            [transitionAnimation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
            [transitionAnimation setFillMode:kCAFillModeBoth];
            
            
            
            //  [myAppDelegate.activeTaskVCObj.viewTaskDetailHolder.layer addAnimation:transitionAnimation forKey:@"changeView"];
            
            [myAppDelegate.vcObj.viewLoginContainer.layer addAnimation:transitionAnimation forKey:@"changeView"];
            
            
            //            for(UIView *view in myAppDelegate.vcObj.viewLoginContainer.subviews){
            //
            //                [view removeFromSuperview];
            //
            //            }
            
            
            for(UIView *view in myAppDelegate.vcObj.viewHolderWithHeader.subviews){
                
                [view removeFromSuperview];
                
            }
            
            [myAppDelegate.vcObj performSegueWithIdentifier:@"taskhistory" sender:myAppDelegate.loginVCObj];
            myAppDelegate.vcObj.btnBack.hidden = YES;
            myAppDelegate.vcObj.btnSideBarOpen.hidden =NO;
            // myAppDelegate.vcObj = nil;
            myAppDelegate.termsNconditionVCObj = nil;
            myAppDelegate.stringNameOfChildVC = @"";
            // return;
            
        }
    }
    
}

@end
