//
//  AppDelegate.m
//  BuyTimee
//
//  Created by User14 on 26/02/16.

//

#import "AppDelegate.h"
#import "Constants.h"
#import "DatabaseClass.h"
#import "GlobalFunction.h"
#import "TaskDetailTableViewCell.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <Fabric/Fabric.h>
#import <TwitterKit/TwitterKit.h>
#import <Stripe/Stripe.h>
//#import <GooglePlus/GooglePlus.h>
#import "ViewAllTaskViewController.h"
#import "MapViewController.h"
#import "Flurry.h"
#import "cities.h"
//#import <GoogleOpenSource/GoogleOpenSource.h>

@import GooglePlaces;
@import GooglePlacePicker;
@import GoogleMaps;


//#import "PayPalMobile.h"
#import "BraintreeCore.h"


#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)

@interface AppDelegate ()<UIAlertViewDelegate,UIApplicationDelegate>

@end

@implementation AppDelegate{
    NSString *pushTaskId;
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
//    _isTermsAndPolicyViewOpen=NO;
    
    // Enable Network Diagnostic Logs
    setenv("CFNETWORK_DIAGNOSTICS", "3", 1);
    
//    
//    if ((37.778132>=37.857360) && (-122.412753 >=-122.373650 ) && ((37.778132<=38.002288) && (-122.412753 <=-122.189910)){
//        
//        NSLog(@"True");
//    }
    
    
   // [GlobalFunction getLocationFromAddressString:@"1190 mission st, san francisco, ca 94103, usa"];
    

    
    [NSThread sleepForTimeInterval:1.0];
    
    
//    [Flurry startSession:@"B3BS954DS2VJ3FTF5N3M"];// Flurry Key with vs dummy account on flurry.
    [Flurry startSession:@"NHY9NXRVVM9M3JRSSR62"];// Flurry Key with clients main account on flurry.
    
    [Flurry setLogLevel:FlurryLogLevelAll];
    NSSetUncaughtExceptionHandler(&uncaughtExceptionHandler);
    
   // [Flurry setLogLevel:FlurryLogLevelAll];
  //  [Flurry setGender:@"m"];
  //  [Flurry setAge:24];
    [self performSelector:@selector(enableLocationServices) withObject:nil afterDelay:0.1];
    
   // [self enableLocationServices];
    
    self.vcObj = (ViewController *)[self.window rootViewController];
    
    [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];

    [[Twitter sharedInstance] startWithConsumerKey:@"3Z0U2AYcDgbMoGTgssRWe5Ifx" consumerSecret:@"mDwCpsKhAwXD2YYll1zH6p373rxgWd0cg8uhhOAJDG2SBuZeKs"];
    
    
  //  [Fabric with:@[[Twitter class]]];
//    [Stripe setDefaultPublishableKey:STRIPE_TEST_PUBLIC_KEY]; // change id given by ishan & edited by vs on 26/12/2016...(johnjdixson stripe acc)

    [Stripe setDefaultPublishableKey:STRIPE_LIVE_PUBLIC_KEY];
    
    [GIDSignIn sharedInstance].clientID = @"292616665360-ctqcbcheau6tp8sik9u9344vsn5govnj.apps.googleusercontent.com";

    
     NSLog(@"testBool = %@", keyIsLogin);

    if (![[[UserDefaults dictionaryRepresentation] allKeys] containsObject:keyIsLogin]) {

        [UserDefaults setBool:NO forKey:keyIsLogin];
        
        [UserDefaults setValue:last_update_time_default forKey:keyLastModifyTime];
        
        [UserDefaults setBool:NO forKey:keyAgreeTnC];
        
        [UserDefaults setBool:NO forKey:keyBioFilled];

        [UserDefaults synchronize];
        
    }
   
    
    self.isFromSkip = NO;
    self.isComingFromPushNotification = false;
    [GlobalFunction createDirForImageInCache:@"vender"];
    [GlobalFunction createDirForImageInCache:@"user"];
    [GlobalFunction createDirForImageInCache:@"taskImages"];
    
    [DatabaseClass checkDataBase];
    
    
    
    self.deviceTokenString = @"";
    
    // Let the device know we want to receive push notifications
    if ([application respondsToSelector:@selector(registerUserNotificationSettings:)]) {
        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeBadge
                                                                                             |UIUserNotificationTypeSound
                                                                                             |UIUserNotificationTypeAlert) categories:nil];
        [application registerUserNotificationSettings:settings];
    }

    
    
    
    self.alertViewNotification = nil;
    
    self.vcObj.btnCreateBioSegue.hidden = true;


    NSString* path = [[NSBundle mainBundle] pathForResource:@"city"
                                                     ofType:@"json"];
    NSData *data = [NSData dataWithContentsOfFile:path];
    
    
    NSArray *tempCities=[[NSArray alloc] init];
    
    
    tempCities = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
    
    self.allCityNames=[[NSMutableArray alloc] init];
    for (int i=0; i<tempCities.count; i++) {
        cities *obj=[[cities alloc] init];
        obj.cityName=[tempCities objectAtIndex:i];
        [self.allCityNames addObject:obj];
    }
    
//    // paypal work by VS 12 June 18...
//    
////    [PayPalMobile initializeWithClientIdsForEnvironments:@{
////                                                           PayPalEnvironmentSandbox : @"ASVdaFsFQbTgbuRIXzH-_9r0jP9SqB-xYqsfHyWNh-rr6cPedApNawO7wrJaCsgYEdAlxmD9ut_jSu8"}];
//    
//    
//    [PayPalMobile initializeWithClientIdsForEnvironments:@{
//                                                           PayPalEnvironmentSandbox: @"AViNcnTmPaYZ3VltsmWEN3UmogFcZnjKsnqaitDo2cHrEEl1Rlns4GSz36CSUl69q9eADJwEItR0Rq7M"}];
//    
//    
//    
//   // ASVdaFsFQbTgbuRIXzH-_9r0jP9SqB-xYqsfHyWNh-rr6cPedApNawO7wrJaCsgYEdAlxmD9ut_jSu8J // client id from gd-facilitator@buytimee.com
//    
//    // AViNcnTmPaYZ3VltsmWEN3UmogFcZnjKsnqaitDo2cHrEEl1Rlns4GSz36CSUl69q9eADJwEItR0Rq7M // client id from technical support
    
    
    // BrainTree work by VS 10 Juky 18.
    
        [BTAppSwitch setReturnURLScheme:@"com.BTime.BuyTimeeApps.payments"];
    
    
    
    [GMSPlacesClient provideAPIKey:@"AIzaSyCXCXTyEwyOr9g-EMsu-wdsTesytP6QEw0"];
    
    [GMSServices provideAPIKey:@"AIzaSyCXCXTyEwyOr9g-EMsu-wdsTesytP6QEw0"];
                                // AIzaSyCXCXTyEwyOr9g-EMsu-wdsTesytP6QEw0
    
                                  // AIzaSyCXCXTyEwyOr9g-EMsu-wdsTesytP6QEw0 // john la, my project...
    
    
    
    
  //  [GMSServices]
    return YES;
}

void uncaughtExceptionHandler(NSException *exception)
{
    [Flurry logError:@"Uncaught" message:@"Crash!" exception:exception];
}
#ifdef __IPHONE_8_0
- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings{
    //register to receive notifications
    [application registerForRemoteNotifications];
}

- (void)application:(UIApplication *)application handleActionWithIdentifier:(NSString *)identifier forRemoteNotification:(NSDictionary *)userInfo completionHandler:(void(^)())completionHandler{
    //handle the actions
    if ([identifier isEqualToString:@"declineAction"]){
        
    }
    else if ([identifier isEqualToString:@"answerAction"]){
        
    }
}
#endif

- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken{
    
    NSString *dTokenString = [NSString stringWithFormat:@"%@",deviceToken];
    
    NSLog(@"My token is: %@", [self convertDeviceTokenString:dTokenString]);
    
    self.deviceTokenString = [self convertDeviceTokenString:dTokenString];
}

-(NSString *)convertDeviceTokenString:(NSString *)deviceToken{
    
    NSString *deviceTokenStr;
    deviceTokenStr = [deviceToken stringByReplacingOccurrencesOfString:@">" withString:@""];
    deviceTokenStr = [deviceTokenStr stringByReplacingOccurrencesOfString:@"<" withString:@""];
    deviceTokenStr = [deviceTokenStr stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    // Our API returns token in all uppercase, regardless how it was originally sent.
    // To make the two consistent, I am uppercasing the token string here.
    deviceTokenStr = [deviceTokenStr uppercaseString];
    
    return deviceTokenStr;
    
}
- (void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError*)error{
    
    NSLog(@"Failed to get token, error: %@", [error localizedDescription]);
    
}

-(void)enableLocationServices{
    
    if (self.locationManager==nil) {
        self.locationManager = [[CLLocationManager alloc]init];
    }
    self.locationManager.delegate = self;
    
    CLAuthorizationStatus status = [CLLocationManager authorizationStatus];
    
    // If the status is denied or only granted for when in use, display an alert
    if (status == kCLAuthorizationStatusDenied || status == kCLAuthorizationStatusRestricted) {
        
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0"))
        {
            NSString *title;
            title = (status == kCLAuthorizationStatusDenied) ? @"Location services are off" : @"Background location is not enabled";
            NSString *message = @"To use background location you must turn on 'Always' in the Location Services Settings";
            
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title
                                                                message:message
                                                               delegate:self
                                                      cancelButtonTitle:@"Cancel"
                                                      otherButtonTitles:@"Settings", nil];
            [alertView setTag:1001];
            [alertView show];
        }
        else{
            NSString *titles;
            titles = @"Buy Timee";
            NSString *msg = @"Location services are off. To use location services you must turn on 'Always' in the Location Services Settings from Click on 'Settings' > 'Privacy' > 'Location Services'. Enable the 'Location Services' ('ON')";
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:titles
                                                                message:msg
                                                               delegate:nil
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
            [alertView show];
            
        }
        
        
    }// The user has not enabled any location services. Request background authorization.
    else if (status == kCLAuthorizationStatusNotDetermined) {
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0"))
        {
            [self.locationManager requestWhenInUseAuthorization];
            
        }
    }
    
//    //for ios 9 bakground location updates
//    if ([self.locationManager respondsToSelector:@selector(setAllowsBackgroundLocationUpdates:)]) {
//        [self.locationManager setAllowsBackgroundLocationUpdates:YES];
//    }
    
    [self.locationManager startMonitoringSignificantLocationChanges];
   
    [self.locationManager startUpdatingLocation];

}

#pragma mark location manager delegate
-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations{
    
    self.latitude = [NSString stringWithFormat:@"%f", manager.location.coordinate.latitude];
    self.longitude = [NSString stringWithFormat:@"%f", manager.location.coordinate.longitude];

    
    [self.locationManager stopUpdatingLocation];
    
    CLGeocoder * geoCoder = [[CLGeocoder alloc] init];
    [geoCoder reverseGeocodeLocation:self.locationManager.location
                   completionHandler:^(NSArray *placemarks, NSError *error) {
                       
                       NSString *cityName = @"";
//                       CLPlacemark *myPlacemark = [placemarks objectAtIndex:0];
//                       
//                       NSString *cityName =  myPlacemark.subAdministrativeArea;
                       
                       for (CLPlacemark *placemark in placemarks) {
                           self.currentCityName = [placemark locality];
                           
                           
                           
                              self.viewAllTaskVCObject.enteredLocation = self.currentCityName;
                           
                           
                       }
                   }];

    
}


- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation{
    
    self.latitude = [NSString stringWithFormat:@"%f", manager.location.coordinate.latitude];
    self.longitude = [NSString stringWithFormat:@"%f", manager.location.coordinate.longitude];
    
   // NSString *cityName = @"";
    
//    [self.locationManager stopUpdatingLocation];
//    
//    CLGeocoder * geoCoder = [[CLGeocoder alloc] init];
//    [geoCoder reverseGeocodeLocation:newLocation
//                   completionHandler:^(NSArray *placemarks, NSError *error) {
//                       
//                       
//                       CLPlacemark *myPlacemark = [placemarks objectAtIndex:0];
//                      
//                       NSString *cityName =  myPlacemark.subAdministrativeArea;
//                       
//                       for (CLPlacemark *placemark in placemarks) {
//                           cityName = [placemark locality];
//                       }
//                   }];
    
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult result))handler{
    
     // [UIApplication sharedApplication].applicationIconBadgeNumber = 5;
    
        [application setApplicationIconBadgeNumber:[[[userInfo objectForKey:@"aps"] objectForKey:@"badge"] intValue]];
    
    NSLog(@"aps  ==== %@",userInfo);
    
   // [UIApplication sharedApplication].applicationIconBadgeNumber =
    //[[userInfo objectForKey:@"badge"] integerValue];
    
    
    
    NSDictionary *aps = [userInfo valueForKey:@"aps"];
    NSDictionary *person = [userInfo valueForKey:@"Person"];
    
    
    NSLog(@"User info : %@", userInfo);
    NSLog(@"person info : %@", person);
    
    
    
//    UIAlertView *alert =[[UIAlertView alloc] initWithTitle:@"Push 2" message: [[userInfo objectForKey:@"aps"] objectForKey:@"badge"] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
//    [alert show];
    
    
//    UIAlertView *alert1 =[[UIAlertView alloc] initWithTitle:@"Push 2" message: userInfo delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
//    [alert1 show];
//    
    
 
 //   [UIApplication sharedApplication].applicationIconBadgeNumber = [ [person valueForKey:@"badge"] integerValue];
  
    
    
    
   // NSDictionary *creatorDict = [person valueForKey:@"creater"];
   // NSDictionary *acceptorDict = [person valueForKey:@"accepter"];
    
    
//    if (self.vcObj.viewLoginContainer.hidden) {
//        
//        if ([self.userData.userid isEqualToString:[NSString stringWithFormat:@"%@",[creatorDict valueForKey:@"userId"]]] || [self.userData.userid isEqualToString:[NSString stringWithFormat:@"%@",[acceptorDict valueForKey:@"workerId"]]]) {
//            
//            if ([[person valueForKey:@"type"] isEqualToString:@"101"]) {
//                
//                UIAlertView *alertViewPayment = [[UIAlertView alloc] initWithTitle:@"BuyTimee" message:[NSString stringWithFormat:@"%@",[aps valueForKey:@"alert"]] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
//                
//                [alertViewPayment show];
//                
//            }else{
//                
//                self.alertViewNotification = [[UIAlertView alloc] initWithTitle:@"BuyTimee" message:[NSString stringWithFormat:@"%@",[aps valueForKey:@"alert"]] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
//                
//                self.alertViewNotification.tag = 1002;
//
//                [GlobalFunction addIndicatorView];
//                
//                for (task *obj in self.arrayTaskDetail) {
//                    if ([obj.taskid isEqualToString:[NSString stringWithFormat:@"%@",[person valueForKey:@"taskid"]]]) {
//                        self.taskPushData = [[task alloc] init];
//                        self.taskPushData = obj;
//                        break;
//                    }
//                }
//                
//                self.notificationType = [person valueForKey:@"type"];
//                
//                if (self.taskPushData) {
//                    [self performSelector:@selector(fetchTaskDetailForTaskId:) withObject:[person valueForKey:@"taskid"] afterDelay:0.2];
//                }else{
//                    [GlobalFunction removeIndicatorView];
//                }
//                
//            }
//
//        }
//        else{
//            
//            NSLog(@"notification for other user");
//            self.alertViewNotification = nil;
//            
//        }
//        
//    }
//    else{
//        self.alertViewNotification = nil;
//    }
    
    
    //************* My Work **********//
    
    UIApplicationState state = [application applicationState];
    if (state == UIApplicationStateActive){
        //When your app was active and it got push notification
        
        if (self.vcObj.viewLoginContainer.hidden) {
            
            self.isFromPushNotification = YES;
            self.reservationId_FromPush = @"";
            self.notificationType_FromPush = @"";
            
            self.reservationId_FromPush = [person valueForKey:@"reservationId"];
            self.notificationType_FromPush = [person valueForKey:@"type"];
            
            NSString *messageString = [aps valueForKey:@"alert"];
            
            self.alertViewNotification11 = [[UIAlertView alloc] initWithTitle:@"BUY TIMEE" message:messageString delegate:self cancelButtonTitle:@"Later" otherButtonTitles:@"Open",nil];
            
            self.alertViewNotification11.tag = 1002;
            
             [self.alertViewNotification11 show];
         }
        else{
            
            self.isFromPushNotification = NO;
            self.alertViewNotification = nil;
        }

    }
    else
    {
          if (self.vcObj.viewLoginContainer.hidden)
          {
              
              self.isFromPushNotification = YES;
              
              self.reservationId_FromPush = @"";
              self.notificationType_FromPush = @"";
              
              self.reservationId_FromPush = [person valueForKey:@"reservationId"];
              self.notificationType_FromPush = [person valueForKey:@"type"];
              
              //  [self performSelector:@selector(fetchTaskDetailForTaskId:) withObject:[person valueForKey:@"reservationId"] afterDelay:0.2];
              // [self performSelector:@selector(gotoHistoryScreen) withObject:nil afterDelay:0.2];
              [self performSelector:@selector(gotoHistoryScreen) withObject:nil afterDelay:0.2];

              
          }
        
          else{
              self.isFromPushNotification = NO;
              self.alertViewNotification = nil;
          }
    }
    
//    
//    else if (state == UIApplicationStateInactive){
//        
//        
//        //When your app was in background and it got push notification
//    }
    
}

-(void)gotoHistoryScreen
{
   // [self.vco shouldPerformSegueWithIdentifier:@"mapView" sender:nil];
    [self.vcObj shouldPerformSegueWithIdentifier:@"taskhistory" sender:nil];
    
//    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:4 inSection:4];
//    [self.vcObj.tblViewSideBar.delegate tableView:self.vcObj.tblViewSideBar didSelectRowAtIndexPath:indexPath];
//    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:4 inSection:1];
//    [self tableView:self.vcObj.tblViewSideBar didSelectRowAtIndexPath:indexPath];
    
   // [self.vcObj.tblViewSideBar didse
}

-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex{
    
    if (alertView.tag == 1002) {
        if (buttonIndex == [alertView cancelButtonIndex]) {

          //  [self gotoTaskDetail];
            self.isFromPushNotification = NO;
            self.alertViewNotification = nil;
            
            
        }
        else
        {
             [self performSelector:@selector(gotoHistoryScreen) withObject:nil afterDelay:0.2];
        }
        
        
    }else if (alertView.tag == 999){

        if (buttonIndex != [alertView cancelButtonIndex]) {
            
            [GlobalFunction addIndicatorView];
            
            [self performSelector:@selector(fetchTaskInformationHandling) withObject:nil afterDelay:0.2];

        }

    }
    else if (alertView.tag==1001)
    {
        
        if(buttonIndex==1)
        {
            //  [[UIApplication sharedApplication] openURL:[NSURL  URLWithString:UIApplicationOpenSettingsURLString]];
            
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
            
        }
    }

}

-(void)fetchTaskInformationHandling{
    
    if (![webServicesShared connected]) {
        
        [GlobalFunction removeIndicatorView];

        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"BUY TIMEE" message:@"Can't connect to internet, task information was not fetched. Please check your internet connection then try again." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        [alertView show];
        
        return;
    }
    
    NSDictionary *dictTaskInfoResponse = [[WebService shared] viewTaskInfoTaskid:pushTaskId userid:myAppDelegate.userData.userid timeZone:@"" view:@""];

    if (dictTaskInfoResponse) {
        
        NSLog(@"response message == %@",[EncryptDecryptFile decrypt:[dictTaskInfoResponse valueForKey:@"responseMessage"]]);
        
        if ([[EncryptDecryptFile decrypt:[dictTaskInfoResponse valueForKey:@"responseCode"]] isEqualToString:@"200"]) {
            
            NSDictionary *dictDataReg = [dictTaskInfoResponse objectForKey:@"data"];
            
            if (dictDataReg != nil) {
                
                myAppDelegate.workerPushTaskDetail = nil;
                
                myAppDelegate.workerPushTaskDetail = [[WorkerEmployerTaskDetail alloc] init];
                
                [self fillWorkerTaskDataForWorkerTaskDataObj:myAppDelegate.workerPushTaskDetail fromDictionary:dictDataReg andUserData:myAppDelegate.userData];
                
                [GlobalFunction removeIndicatorView];
                
                if (self.alertViewNotification) {
                    
                    [self.alertViewNotification show];
                    
                }
                
            }else{
               
                [GlobalFunction removeIndicatorView];
                
                [[GlobalFunction shared] showAlertForMessage:[EncryptDecryptFile decrypt:[dictTaskInfoResponse valueForKey:@"responseMessage"]]];

            }
        }else{
            
            [GlobalFunction removeIndicatorView];

            [[GlobalFunction shared] showAlertForMessage:[EncryptDecryptFile decrypt:[dictTaskInfoResponse valueForKey:@"responseMessage"]]];
            
        }
        
    }else{
        
        [GlobalFunction removeIndicatorView];
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"BUY TIMEE" message:@"Unable to fetch task information, would you like to retry?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Retry", nil];
        
        alertView.tag = 999;
        
        [alertView show];
        
    }
    
}


-(void)fetchTaskDetailForTaskId:(NSString *)taskId{
    
    self.isComingFromPushNotification = YES;

    pushTaskId = taskId;
    NSString *timeZoneSeconds = [GlobalFunction getTimeZone];
    NSDictionary *dictTaskInfoResponse = [[WebService shared] viewTaskInfoTaskid:taskId userid:myAppDelegate.userData.userid timeZone:timeZoneSeconds view:@""];
    
    if (dictTaskInfoResponse) {
        
        NSLog(@"response message == %@",[EncryptDecryptFile decrypt:[dictTaskInfoResponse valueForKey:@"responseMessage"]]);
        
        if ([[EncryptDecryptFile decrypt:[dictTaskInfoResponse valueForKey:@"responseCode"]] isEqualToString:@"200"]) {
            
            NSDictionary *dictDataReg = [dictTaskInfoResponse objectForKey:@"data"];
            
            if (dictDataReg != nil) {
                
                myAppDelegate.workerPushTaskDetail = nil;
                
                myAppDelegate.workerPushTaskDetail = [[WorkerEmployerTaskDetail alloc] init];
                
                [self fillWorkerTaskDataForWorkerTaskDataObj:myAppDelegate.workerPushTaskDetail fromDictionary:dictDataReg andUserData:myAppDelegate.userData];
                
                [GlobalFunction removeIndicatorView];
                
                if (self.alertViewNotification) {
                    
                    [self.alertViewNotification show];
                    
                }

            }else{
                
                [GlobalFunction removeIndicatorView];

                [[GlobalFunction shared] showAlertForMessage:[EncryptDecryptFile decrypt:[dictTaskInfoResponse valueForKey:@"responseMessage"]]];

            }
        }else{
            
            [GlobalFunction removeIndicatorView];

            [[GlobalFunction shared] showAlertForMessage:[EncryptDecryptFile decrypt:[dictTaskInfoResponse valueForKey:@"responseMessage"]]];
            
        }
    }else{
        
        [GlobalFunction removeIndicatorView];
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"BUY TIMEE" message:@"Unable to fetch task information, would you like to retry?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Retry", nil];
        
        alertView.tag = 999;
        
        [alertView show];
        
    }

}

-(void)gotoTaskDetail{
    
    pushTaskId = @"";
    
    self.lastOpenTitleText = self.vcObj.lblTitleText.text;
    self.isSideBarButtonHiddenOrShown = self.vcObj.btnSideBarOpen.hidden;
    self.isMapViewLocationShowing = self.vcObj.mapViewToShowUserLocationOnMap.hidden;
    
    if (self.vcObj.mapViewToShowUserLocationOnMap.annotations.count>0) {
        self.lastLocationSaved = [[self.vcObj.mapViewToShowUserLocationOnMap.annotations firstObject] coordinate];
    }
    
    [self.vcObj performSegueWithIdentifier:@"pushTask" sender:self.vcObj.btnPushForTaskDetail];
    
    self.alertViewNotification = nil;

}

-(void)fillWorkerTaskDataForWorkerTaskDataObj:(WorkerEmployerTaskDetail *)obj fromDictionary:(NSDictionary *)dict andUserData:(user *)userObj{
    
    NSArray *arrayTerms= [dict valueForKey:@"Accepted By"];
    if ([[arrayTerms objectAtIndex:0] isKindOfClass:[NSString class]]) {
        
        obj.isTaskAccepted = false;
        
    }else if ([[arrayTerms objectAtIndex:0] isKindOfClass:[NSDictionary class]]){
        
        obj.isTaskAccepted = true;
        
        NSDictionary *dictTemp0=[arrayTerms objectAtIndex:0];
        
        obj.userId_acceptedBy = [EncryptDecryptFile decrypt:[dictTemp0 valueForKey:@"userId"]];
        obj.userName_acceptedBy = [EncryptDecryptFile decrypt:[dictTemp0 valueForKey:@"userName"]];
        obj.description_acceptedBy = [EncryptDecryptFile decrypt:[dictTemp0 valueForKey:@"description"]];
        obj.emailId_acceptedBy = [EncryptDecryptFile decrypt:[dictTemp0 valueForKey:@"emailId"]];
        obj.latitude_acceptedBy = [[EncryptDecryptFile decrypt:[dictTemp0 valueForKey:@"latitude"]] floatValue];
        obj.longitude_acceptedBy = [[EncryptDecryptFile decrypt:[dictTemp0 valueForKey:@"longitude"]] floatValue];
        obj.locationName_acceptedBy = [EncryptDecryptFile decrypt:[dictTemp0 valueForKey:@"location"]];
        obj.mobile_acceptedBy = [EncryptDecryptFile decrypt:[dictTemp0 valueForKey:@"mobile"]];
        obj.picPath_acceptedBy = [EncryptDecryptFile decrypt:[dictTemp0 valueForKey:@"picPath"]];
        obj.avg_rating_acceptedBy = [EncryptDecryptFile decrypt:[dictTemp0 valueForKey:@"avg_rating"]];
        
    }
    
    //task accepted by rating detail
    obj.arrayRatings_acceptedBy = [[NSMutableArray alloc]init];
    arrayTerms= [dict valueForKey:@"Worker Rating Detail"];
    if ([[arrayTerms objectAtIndex:0] isKindOfClass:[NSString class]]) {
        
        obj.rating_acceptedBy = [EncryptDecryptFile decrypt:[arrayTerms objectAtIndex:0]];
        
    }else if ([[arrayTerms objectAtIndex:0] isKindOfClass:[NSDictionary class]]){
        
        NSInteger noOfStars = 0;
        for (NSDictionary *dicts in arrayTerms) {
            
            NSString *stars = [EncryptDecryptFile decrypt:[dicts valueForKey:@"ratingStar"]];
            noOfStars = noOfStars + [stars integerValue];
            
            obj.ratingObj = nil;
            obj.ratingObj = [[rating alloc]init];
            
            obj.ratingObj.ratingdescription = [EncryptDecryptFile decrypt:[dicts valueForKey:@"description"]];
            obj.ratingObj.employerId = [EncryptDecryptFile decrypt:[dicts valueForKey:@"employerId"]];
            obj.ratingObj.ratingId = [EncryptDecryptFile decrypt:[dicts valueForKey:@"ratingId"]];
            obj.ratingObj.ratingStar = [EncryptDecryptFile decrypt:[dicts valueForKey:@"ratingStar"]];
            obj.ratingObj.taskId = [EncryptDecryptFile decrypt:[dicts valueForKey:@"taskId"]];
            obj.ratingObj.workerId = [EncryptDecryptFile decrypt:[dicts valueForKey:@"workerId"]];
            obj.ratingObj.userName = [EncryptDecryptFile decrypt:[dicts valueForKey:@"userName"]];
            
            [obj.arrayRatings_acceptedBy addObject:obj.ratingObj];
        }
        CGFloat avg = noOfStars/arrayTerms.count;
        obj.avgStars_acceptedBy = avg;
    }else if ([[arrayTerms objectAtIndex:0] isKindOfClass:[NSArray class]]){
        
        NSArray *arrRatings = [arrayTerms objectAtIndex:0];
        
        NSInteger noOfStars = 0;
        
        for (NSDictionary *dicts in arrRatings) {
            
            NSString *stars = [EncryptDecryptFile decrypt:[dicts valueForKey:@"ratingStar"]];
            noOfStars = noOfStars + [stars integerValue];
            
            obj.ratingObj = nil;
            obj.ratingObj = [[rating alloc]init];
            
            obj.ratingObj.ratingdescription = [EncryptDecryptFile decrypt:[dicts valueForKey:@"description"]];
            obj.ratingObj.employerId = [EncryptDecryptFile decrypt:[dicts valueForKey:@"employerId"]];
            obj.ratingObj.ratingId = [EncryptDecryptFile decrypt:[dicts valueForKey:@"ratingId"]];
            obj.ratingObj.ratingStar = [EncryptDecryptFile decrypt:[dicts valueForKey:@"ratingStar"]];
            obj.ratingObj.taskId = [EncryptDecryptFile decrypt:[dicts valueForKey:@"taskId"]];
            obj.ratingObj.workerId = [EncryptDecryptFile decrypt:[dicts valueForKey:@"workerId"]];
            obj.ratingObj.userName = [EncryptDecryptFile decrypt:[dicts valueForKey:@"userName"]];
            
            [obj.arrayRatings_acceptedBy addObject:obj.ratingObj];
        }
        
        CGFloat avg = noOfStars/arrayTerms.count;
        obj.avgStars_acceptedBy = avg;
        
    }
    
    //task created by rating detail
    obj.arrayRatings_createdBy = [[NSMutableArray alloc]init];
    arrayTerms= [dict valueForKey:@"Created By Rating Detail"];
    
    if ([[arrayTerms objectAtIndex:0] isKindOfClass:[NSString class]]) {
        
        obj.rating_createdBy = [EncryptDecryptFile decrypt:[arrayTerms objectAtIndex:0]];
        
    }else if ([[arrayTerms objectAtIndex:0] isKindOfClass:[NSDictionary class]]){
        
        NSInteger noOfStars = 0;
        for (NSDictionary *dicts in arrayTerms) {
            
            NSString *stars = [EncryptDecryptFile decrypt:[dicts valueForKey:@"ratingStar"]];
            noOfStars = noOfStars + [stars integerValue];
            
            obj.ratingObj = nil;
            obj.ratingObj = [[rating alloc]init];
            
            obj.ratingObj.ratingdescription = [EncryptDecryptFile decrypt:[dicts valueForKey:@"description"]];
            obj.ratingObj.employerId = [EncryptDecryptFile decrypt:[dicts valueForKey:@"employerId"]];
            obj.ratingObj.ratingId = [EncryptDecryptFile decrypt:[dicts valueForKey:@"ratingId"]];
            obj.ratingObj.ratingStar = [EncryptDecryptFile decrypt:[dicts valueForKey:@"ratingStar"]];
            obj.ratingObj.taskId = [EncryptDecryptFile decrypt:[dicts valueForKey:@"taskId"]];
            obj.ratingObj.workerId = [EncryptDecryptFile decrypt:[dicts valueForKey:@"workerId"]];
            obj.ratingObj.userName = [EncryptDecryptFile decrypt:[dicts valueForKey:@"userName"]];
            
            [obj.arrayRatings_createdBy addObject:obj.ratingObj];
        }
        
        CGFloat avg = noOfStars/arrayTerms.count;
        obj.avgStars_createdBy = avg;
        
    }else if ([[arrayTerms objectAtIndex:0] isKindOfClass:[NSArray class]]){
        
        NSArray *arrRatings = [arrayTerms objectAtIndex:0];
        
        NSInteger noOfStars = 0;
        
        for (NSDictionary *dicts in arrRatings) {
            
            NSString *stars = [EncryptDecryptFile decrypt:[dicts valueForKey:@"ratingStar"]];
            noOfStars = noOfStars + [stars integerValue];
            
            obj.ratingObj = nil;
            obj.ratingObj = [[rating alloc]init];
            
            obj.ratingObj.ratingdescription = [EncryptDecryptFile decrypt:[dicts valueForKey:@"description"]];
            obj.ratingObj.employerId = [EncryptDecryptFile decrypt:[dicts valueForKey:@"employerId"]];
            obj.ratingObj.ratingId = [EncryptDecryptFile decrypt:[dicts valueForKey:@"ratingId"]];
            obj.ratingObj.ratingStar = [EncryptDecryptFile decrypt:[dicts valueForKey:@"ratingStar"]];
            obj.ratingObj.taskId = [EncryptDecryptFile decrypt:[dicts valueForKey:@"taskId"]];
            obj.ratingObj.workerId = [EncryptDecryptFile decrypt:[dicts valueForKey:@"workerId"]];
            obj.ratingObj.userName = [EncryptDecryptFile decrypt:[dicts valueForKey:@"userName"]];
            
            [obj.arrayRatings_createdBy addObject:obj.ratingObj];
        }
        
        CGFloat avg = noOfStars/arrayTerms.count;
        obj.avgStars_createdBy = avg;
        
    }
    
    
    arrayTerms= [dict valueForKey:@"Card Detail"];
    if ([[arrayTerms objectAtIndex:0] isKindOfClass:[NSString class]]) {
        obj.cardDetail = [EncryptDecryptFile decrypt:[arrayTerms objectAtIndex:0]];
    }else if ([[arrayTerms objectAtIndex:0] isKindOfClass:[NSDictionary class]]){
        
    }else if ([[arrayTerms objectAtIndex:0] isKindOfClass:[NSArray class]]){
        obj.cardDetail = [EncryptDecryptFile decrypt:[[arrayTerms objectAtIndex:0] objectAtIndex:0]];
    }
    
    arrayTerms= [dict valueForKey:@"Created By"];
    NSDictionary *dictTemp2=[arrayTerms objectAtIndex:0];
    
    obj.userId_createdBy = [EncryptDecryptFile decrypt:[dictTemp2 valueForKey:@"userId"]];
    obj.userName_createdBy = [EncryptDecryptFile decrypt:[dictTemp2 valueForKey:@"userName"]];
    obj.description_createdBy = [EncryptDecryptFile decrypt:[dictTemp2 valueForKey:@"description"]];
    obj.emailId_createdBy = [EncryptDecryptFile decrypt:[dictTemp2 valueForKey:@"emailId"]];
    obj.latitude_createdBy = [[EncryptDecryptFile decrypt:[dictTemp2 valueForKey:@"latitude"]] floatValue];
    obj.longitude_createdBy = [[EncryptDecryptFile decrypt:[dictTemp2 valueForKey:@"longitude"]] floatValue];
    obj.locationName_createdBy = [EncryptDecryptFile decrypt:[dictTemp2 valueForKey:@"location"]];
    obj.mobile_createdBy = [EncryptDecryptFile decrypt:[dictTemp2 valueForKey:@"mobile"]];
    obj.picPath_createdBy = [EncryptDecryptFile decrypt:[dictTemp2 valueForKey:@"picPath"]];
    obj.avg_rating_createdBy = [EncryptDecryptFile decrypt:[dictTemp2 valueForKey:@"avg_rating"]];
    
    arrayTerms= [dict valueForKey:@"Task Detail"];
    NSDictionary *dictTemp3=[arrayTerms objectAtIndex:0];
    
    obj.taskDetailObj = [[task alloc]init];
    
    obj.taskDetailObj.userid = [EncryptDecryptFile decrypt:[dictTemp3 valueForKey:@"userId"]];
    obj.taskDetailObj.title = [EncryptDecryptFile decrypt:[dictTemp3 valueForKey:@"title"]];
    obj.taskDetailObj.taskid = [EncryptDecryptFile decrypt:[dictTemp3 valueForKey:@"taskId"]];
    obj.taskDetailObj.subcategoryId = [EncryptDecryptFile decrypt:[dictTemp3 valueForKey:@"subcategoryId"]];
    obj.taskDetailObj.status = [EncryptDecryptFile decrypt:[dictTemp3 valueForKey:@"status"]];
    obj.taskDetailObj.startDate = [EncryptDecryptFile decrypt:[dictTemp3 valueForKey:@"startDate"]];
    obj.taskDetailObj.price = [[EncryptDecryptFile decrypt:[dictTemp3 valueForKey:@"price"]] floatValue];
    obj.taskDetailObj.longitude = [[EncryptDecryptFile decrypt:[dictTemp3 valueForKey:@"longitude"]] floatValue];
    obj.taskDetailObj.locationName = [EncryptDecryptFile decrypt:[dictTemp3 valueForKey:@"locationName"]];
    obj.taskDetailObj.latitude = [[EncryptDecryptFile decrypt:[dictTemp3 valueForKey:@"latitude"]] floatValue];
    obj.taskDetailObj.endDate = [EncryptDecryptFile decrypt:[dictTemp3 valueForKey:@"endDate"]];
    obj.taskDetailObj.taskDescription = [EncryptDecryptFile decrypt:[dictTemp3 valueForKey:@"description"]];
    obj.taskDetailObj.lastModifyTime = [EncryptDecryptFile decrypt:[dictTemp3 valueForKey:@"lastModifyTime"]];
    obj.taskDetailObj.taskImagesArray = [[NSMutableArray alloc] init];
    obj.taskDetailObj.imagesArray = [[NSMutableArray alloc]init];
    
    for (rating *ratingObjs in obj.arrayRatings_acceptedBy) {
        
        if ([ratingObjs.taskId isEqualToString:[NSString stringWithFormat:@"%@",obj.taskDetailObj.taskid]]) {
            
            obj.isAlreadyRated = true;
            
            break;
            
        }
        
    }
    
    arrayTerms= [dict valueForKey:@"taskImage"];
    NSArray *newArray = [arrayTerms objectAtIndex:0];
    
    int i = 0;
    
    for (NSDictionary *dicts in newArray) {
        
        NSArray *componentsArray = [[EncryptDecryptFile decrypt:[dicts valueForKey:@"taskImg"]] componentsSeparatedByString:@"http://192.168.5.134/handapp/"];
        
        if (componentsArray.count>1) {
            [obj.taskDetailObj.taskImagesArray addObject:[NSString stringWithFormat:urlServer,[componentsArray objectAtIndex:1]]];
        }else{
            [obj.taskDetailObj.taskImagesArray addObject:[NSString stringWithFormat:urlServer,[componentsArray objectAtIndex:0]]];
        }
        
        NSString *fullPath=[obj.taskDetailObj.taskImagesArray objectAtIndex:i];
        NSArray *parts = [fullPath componentsSeparatedByString:@"/"];
        NSString *imageName=[parts lastObject];
        NSArray *imageNameParts = [imageName componentsSeparatedByString:@"."];
        
        NSString *userImageNameWithJPEG = [NSString stringWithFormat:@"%@_%@_%d.jpeg",[imageNameParts firstObject],obj.taskDetailObj.taskid,i];
        
        UIImage *img = [GlobalFunction loadImageOfLogoFromCacheFolder:userImageNameWithJPEG dirname:@"taskImages"];
        
        if (img) {
            
            [obj.taskDetailObj.imagesArray addObject:img];
            
        }
        
        i = i+1;
    }
    
    if (obj.taskDetailObj.taskImagesArray.count != obj.taskDetailObj.imagesArray.count) {
        [obj.taskDetailObj.imagesArray removeAllObjects];
    }
    
    
    if ([userObj.userid isEqualToString:obj.userId_acceptedBy]) {
        
        if ([obj.picPath_createdBy isEqualToString:@""]) {
            obj.userImage_createdBy = imageUserDefault;
        }else{
            NSArray *components = [obj.picPath_createdBy componentsSeparatedByString:@"/"];
            
            NSString *imgNameWithJPEG = [components lastObject];
            
            
            NSArray *componentsWithDots = [imgNameWithJPEG componentsSeparatedByString:@"."];
            
            if (componentsWithDots.count>2) {
                
                imgNameWithJPEG = [NSString stringWithFormat:@"%@.%@",[componentsWithDots firstObject],[componentsWithDots lastObject]];
                
            }
            
            UIImage *img = [GlobalFunction loadImageOfLogoFromCacheFolder:imgNameWithJPEG dirname:@"user"];
            
            if (img) {
                obj.userImage_createdBy = img;
            }else{
                UIImage *img2 = [GlobalFunction getImageFromURL:[NSString stringWithFormat:urlServer,obj.picPath_createdBy]];
                
                if (img2) {
                    obj.userImage_createdBy = img2;
                    
                    NSArray *components2 = [imgNameWithJPEG componentsSeparatedByString:@"."];
                    
                    NSString *imgNameWithoutJPEG = [components2 firstObject];
                    
                    [GlobalFunction saveimage:img2 imageName:imgNameWithoutJPEG dirname:@"user"];
                    
                }else{
                    obj.userImage_createdBy = imageUserDefault;
                }
            }
        }
    }else if ([userObj.userid isEqualToString:obj.userId_createdBy]){
        
        if ([obj.picPath_acceptedBy isEqualToString:@""]) {
            obj.userImage_acceptedBy = imageUserDefault;
        }else{
            NSArray *components = [obj.picPath_acceptedBy componentsSeparatedByString:@"/"];
            
            NSString *imgNameWithJPEG = [components lastObject];
            
            
            NSArray *componentsWithDots = [imgNameWithJPEG componentsSeparatedByString:@"."];
            
            if (componentsWithDots.count>2) {
                
                imgNameWithJPEG = [NSString stringWithFormat:@"%@.%@",[componentsWithDots firstObject],[componentsWithDots lastObject]];
                
            }
            
            UIImage *img = [GlobalFunction loadImageOfLogoFromCacheFolder:imgNameWithJPEG dirname:@"user"];
            
            if (img) {
                obj.userImage_acceptedBy = img;
            }else{
                UIImage *img2 = [GlobalFunction getImageFromURL:[NSString stringWithFormat:urlServer,obj.picPath_acceptedBy]];
                
                if (img2) {
                    obj.userImage_acceptedBy = img2;
                    
                    NSArray *components2 = [imgNameWithJPEG componentsSeparatedByString:@"."];
                    
                    NSString *imgNameWithoutJPEG = [components2 firstObject];
                    
                    [GlobalFunction saveimage:img2 imageName:imgNameWithoutJPEG dirname:@"user"];
                    
                }else{
                    obj.userImage_acceptedBy = imageUserDefault;
                }
            }
        }
    }
}

- (BOOL)application:(UIApplication *)app
            openURL:(NSURL *)url
            options:(NSDictionary<NSString *, id> *)options
{
        return [[GIDSignIn sharedInstance] handleURL:url
                                   sourceApplication:options[UIApplicationOpenURLOptionsSourceApplicationKey]
                                          annotation:options[UIApplicationOpenURLOptionsAnnotationKey]];
}
- (void)signIn:(GIDSignIn *)signIn
didSignInForUser:(GIDGoogleUser *)user
     withError:(NSError *)error {
    // Perform any operations on signed in user here.
    NSString *userId = user.userID;                  // For client-side use only!
    NSString *idToken = user.authentication.idToken; // Safe to send to the server
    NSString *fullName = user.profile.name;
    NSString *givenName = user.profile.givenName;
    NSString *familyName = user.profile.familyName;
    NSString *email = user.profile.email;
    // ...
}

- (void)signIn:(GIDSignIn *)signIn
didDisconnectWithUser:(GIDGoogleUser *)user
     withError:(NSError *)error {
    // Perform any operations when the user disconnects from app here.
    // ...
}


- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
    
    
    if ([url.scheme localizedCaseInsensitiveCompare:@"com.BTime.BuyTimeeApps.payments"] == NSOrderedSame)
    {
        return [BTAppSwitch handleOpenURL:url sourceApplication:sourceApplication];
    
    }
  //  if ([[url scheme] isEqualToString:@"taskmobistripe"]) {
     if ([[url scheme] isEqualToString:@"BuyTimeeStripe"]) {
        [GlobalFunction addIndicatorView];
        
        NSString *urlString = [NSString stringWithFormat:@"%@",url];
        
        NSArray *lastPathArray = [urlString componentsSeparatedByString:@"//"];
        
        NSString *lastPathString = [lastPathArray lastObject];
        
        NSLog(@"URL query: %@", lastPathString);
        
        [self performSelector:@selector(fetchAuthCode:) withObject:lastPathString afterDelay:0.2];
        
    }else{
        
//        return [[FBSDKApplicationDelegate sharedInstance] application:application
//                                                              openURL:url
//                                                    sourceApplication:sourceApplication
//                                                           annotation:annotation];
        
//        return [GPPURLHandler handleURL:url
//                      sourceApplication:sourceApplication
//                             annotation:annotation];
    }
    
    return YES;
    
}

-(void)fetchAuthCode:(NSString *)authCode{
    
    NSDictionary *dictResponse = [webServicesShared generateStripeUserIdForUserId:self.userData.userid andAuthCode:authCode];
    
    if (dictResponse) {
        
        NSLog(@"success === %@",[EncryptDecryptFile decrypt:[dictResponse valueForKey:@"responseMessage"]]);
        
        if ([[EncryptDecryptFile decrypt:[dictResponse valueForKey:@"responseCode"]] isEqualToString:@"200"]) {
            
            [GlobalFunction removeIndicatorView];
            
            [[GlobalFunction shared] showAlertForMessage:[EncryptDecryptFile decrypt:[dictResponse valueForKey:@"responseMessage"]]];
            
            NSDictionary *stripeId = [dictResponse valueForKey:@"data"];
            
            self.userData.stripe_userid = [EncryptDecryptFile decrypt:[stripeId valueForKey:@"stripe_id"]];
            
            NSLog(@"stripeUser id === %@",self.userData.stripe_userid);

            [DatabaseClass updateStripeIdInDataBase:[EncryptDecryptFile EncryptIT:@"1"] forUserId:[EncryptDecryptFile EncryptIT:self.userData.userid]];
            
            NSMutableArray *arr = [DatabaseClass getDataFromUserData:[[NSString stringWithFormat:@"Select * from %@",tableUser] UTF8String]];
            
            self.userData = nil;
            self.userData = (user *)[arr firstObject];
            
            if (myAppDelegate.createBioVCObj) {
                
              //  [myAppDelegate.createBioVCObj.btnAddBankInfo setTitle:@"Disconnect from stripe" forState:UIControlStateNormal];

            }

        }else{
            
            [GlobalFunction removeIndicatorView];
            
            [[GlobalFunction shared] showAlertForMessage:[EncryptDecryptFile decrypt:[dictResponse valueForKey:@"responseMessage"]]];
            
        }
        
        
    }else{
        
        NSLog(@"failure");
        
        [GlobalFunction removeIndicatorView];
        
        [[GlobalFunction shared] showAlertForMessage:@"Server is down, please try again later."];

    }

}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    
    
    bgTask=(UIBackgroundTaskIdentifier *)UIBackgroundTaskInvalid;
    UIApplication *app=[UIApplication sharedApplication];
    bgTask=(UIBackgroundTaskIdentifier *)[app beginBackgroundTaskWithExpirationHandler:^{}];
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    [FBSDKAppEvents activateApp];
//    GPPSignIn *signIn = [GPPSignIn sharedInstance];
//    signIn.delegate = self;
//    [signIn shouldFetchGoogleUserEmail];
//    if ([signIn shouldFetchGoogleUserEmail]) {
//        NSLog(@"fetched");
//        [GlobalFunction removeIndicatorView];
//    }
//    else
//    {
//        NSLog(@"not fetched");
//    }
//    
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    
  [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
