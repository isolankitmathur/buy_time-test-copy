//
//  AppDelegate.h
//  BuyTimee
//
//  Created by User14 on 26/02/16.

//

#import <UIKit/UIKit.h>
#import "ViewController.h"
#import "ViewAllTaskViewController.h"
#import "TermsViewController.h"
#import "TaskHistoryViewController.h"
#import "RegistrationViewController.h"
#import "PaymentInfoViewController.h"
#import "LoginViewController.h"
#import "ForgotPasswordViewController.h"
#import "CreateBioViewController.h"
#import "ActiveTaskViewController.h"
#import "TaskCreateViewController.h"
#import "TaskDetailViewController.h"
#import "RatingViewController.h"
#import "MapViewController.h"
#import "ProfileViewController.h"
#import "ResetPasswordViewController.h"
#import "SettingsViewController.h"
#import <CoreLocation/CoreLocation.h>
#import "BankInfoViewController.h"
#import "SetUpBankViewController.h"
#import "ShareViewController.h"
#import "ReviewViewController.h"
#import "ReviewRejectionViewController.h"
#import "ReivewDetailViewController.h"
#import "ShareReviewDetailViewController.h"
#import "RequestReservationViewController.h"
#import "cashOutPayPalViewController.h"
#import "Flurry.h"


//data class objects
#import "user.h"
#import "task.h"
#import "WorkerEmployerTaskDetail.h"
#import "TermsAndCondition.h"
#import <GoogleSignIn/GoogleSignIn.h>


//#import "PayPalMobile.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate,CLLocationManagerDelegate,GIDSignInDelegate>
{
    UIBackgroundTaskIdentifier *bgTask;

}
@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) ViewController *vcObj;
@property(strong,nonatomic) UINavigationController *NavController;
@property (strong, nonatomic) ViewAllTaskViewController *viewAllTaskVCObj;

@property (strong, nonatomic) ViewAllTaskViewController *viewAllTaskVCObject;  // added by rohit


@property (strong, nonatomic) CreateBioViewController *createBioVCObj;
@property (strong, nonatomic) ActiveTaskViewController *activeTaskVCObj;
@property (strong, nonatomic) TaskHistoryViewController *taskHistoryVCObj;
@property (strong, nonatomic) TermsViewController *termsNconditionVCObj;
@property (strong, nonatomic) LoginViewController *loginVCObj;
@property (strong, nonatomic) RegistrationViewController *registrationVCObj;
@property (strong, nonatomic) ForgotPasswordViewController *forgotPassVCObj;
@property (strong, nonatomic) PaymentInfoViewController *paymentInfoVCObj;
@property (strong, nonatomic) TaskCreateViewController *taskCreateVCObj;
@property (strong, nonatomic) TaskDetailViewController *taskDetailVCObj;
@property (strong, nonatomic) RatingViewController *ratingVCObj;
@property (strong, nonatomic) MapViewController *mapVCObj;
@property (strong, nonatomic) ProfileViewController *profileVCObj;
@property (strong, nonatomic) SettingsViewController *settingsVCObj;
@property (strong, nonatomic) ResetPasswordViewController *resetPassVCObj;
@property (strong, nonatomic) BankInfoViewController *bankInfObj;
@property (strong, nonatomic) SetUpBankViewController *setUpBankInfoInfObj;
@property (strong, nonatomic) ShareViewController *shareVcObj;
@property (strong, nonatomic) ReviewViewController *reviewVcObj;
@property (strong, nonatomic) ReviewRejectionViewController *reviewRejectionVcObj;
@property (strong, nonatomic) ReivewDetailViewController *reviewDetailVcObj;
@property (strong, nonatomic) ShareReviewDetailViewController *shareReviewDetailVcObj;
@property (strong, nonatomic) RequestReservationViewController *requestReservationVcObj;
@property (strong, nonatomic) cashOutPayPalViewController *cashOutPayPalVCObj;



//other user handling
@property (strong, nonatomic) TaskHistoryViewController *taskHistoryOtherUserVCObj;
@property (strong, nonatomic) TaskDetailViewController *taskDetailOtherUserVCObj;
@property (strong, nonatomic) ProfileViewController *profileOtherUserVCObj;

@property (strong, nonatomic) WorkerEmployerTaskDetail *workerOtherUserTaskDetail;
@property (strong, nonatomic) user *userOtherData;
@property (strong, nonatomic) task *taskOtherData;

@property (assign, nonatomic) BOOL isOfOtherUser;
@property (assign, nonatomic) BOOL isOfOtherUserTaskHistory;
@property (assign, nonatomic) BOOL isOfOtherUserTaskDetail;
@property (assign, nonatomic) BOOL isFirstOtherUser;


//self data handling
@property (strong, nonatomic) NSString *superViewClassName;
@property (strong, nonatomic) NSString *comingFromClass;
@property (strong, nonatomic) NSString *stringNameOfChildVC;
@property (strong, nonatomic) NSString *activeViewControllerClassName;

@property (strong, nonatomic) NSString *TandC_Buyee;

@property (strong, nonatomic) NSString *deviceTokenString;

@property (strong, nonatomic) NSMutableArray *arrayTaskDetail;
@property (strong, nonatomic) NSMutableArray *arrayAcceptedTaskDetail;
@property (strong, nonatomic) NSMutableArray *arrayCategoryDetail;
@property (strong, nonatomic) NSMutableArray *arrayBusinessDetail;

@property (strong, nonatomic) NSMutableArray *allCityNames;

@property (strong, nonatomic) user *userData;
@property (strong, nonatomic) task *taskData;
@property (strong, nonatomic) task *taskActiveData;

@property (strong, nonatomic) TermsAndCondition *termsAndConditionData;


@property (strong, nonatomic) WorkerEmployerTaskDetail *workerTaskDetail;
@property (strong, nonatomic) WorkerEmployerTaskDetail *workerAllTaskDetail;

//push data handling
@property (strong, nonatomic) WorkerEmployerTaskDetail *workerPushTaskDetail;
@property (strong, nonatomic) TaskDetailViewController *taskDetailPushVCObj;
@property (strong, nonatomic) TaskDetailViewController *taskDetailOtherPushVCObj;
@property (strong, nonatomic) TaskHistoryViewController *taskHistoryPushOtherUserVCObj;
@property (strong, nonatomic) ProfileViewController *profilePushOtherUserVCObj;

@property (strong, nonatomic) task *taskPushData;
@property (strong, nonatomic) user *userPushOtherData;

@property (strong, nonatomic) NSString *lastOpenTitleText;
@property (assign, nonatomic) BOOL isSideBarButtonHiddenOrShown;
@property (assign, nonatomic) BOOL isMapViewLocationShowing;
@property (assign, nonatomic) BOOL isOfPushOtherUser;
@property (assign, nonatomic) BOOL isGooglePressed;

@property (assign, nonatomic) CLLocationCoordinate2D lastLocationSaved;

//@property (strong, nonatomic) WorkerEmployerTaskDetail *workerPushOtherUserTaskDetail;
//@property (strong, nonatomic) task *taskPushOtherData;
//
//@property (assign, nonatomic) BOOL isOfPushOtherUserTaskHistory;
//@property (assign, nonatomic) BOOL isOfPushOtherUserTaskDetail;
//@property (assign, nonatomic) BOOL isPushFirstOtherUser;


@property (strong, nonatomic) UIAlertView *alertViewNotification;
@property (strong, nonatomic) UIAlertView *alertViewNotification11;

@property (strong, nonatomic) NSString *mapViewFromClass;
@property (assign, nonatomic) BOOL isComingBackAfterMapOpen;
@property (assign, nonatomic) BOOL isSideBarAccesible;

@property (nonatomic, assign) BOOL isComingFromRating;
@property (nonatomic, assign) BOOL isComingBackToSettings;

//for handling create bio navigation
@property (assign, nonatomic) BOOL isFromAlert;
@property (assign, nonatomic) BOOL isFromTnC;
@property (assign, nonatomic) BOOL isFromEdit;
@property (assign, nonatomic) BOOL isPaymentORBankInfo;

//for handling other user profile and task history navigation
@property (assign, nonatomic) BOOL isFromProfile;
@property (assign, nonatomic) BOOL isLoginComplete;
@property (assign, nonatomic) BOOL isFromSkip;
@property (assign, nonatomic) BOOL isPaymentFilled;
@property (assign, nonatomic) BOOL isBankInfoFilled;
@property (assign, nonatomic) BOOL isBankDetailFilled;
@property (assign, nonatomic) BOOL isRatingGiven;

@property (assign, nonatomic) BOOL isComingFromPushNotification;

@property (assign, nonatomic) BOOL isActivityIndicatorShowing;

@property (strong, nonatomic) NSString *notificationType;

@property (nonatomic, strong) CLLocationManager *locationManager;
@property(nonatomic,strong)NSString *latitude;
@property(nonatomic,strong)NSString *longitude;
@property(nonatomic,strong)NSString *currentCityName;
@property(nonatomic,strong)UIViewController *CurrentVCObj;
//comment by kp

@property (assign, nonatomic) BOOL isFromCreateBioOnTnC;
@property (assign, nonatomic) BOOL isTermsAndServiceBtnClcik;
// forward filter work...

@property (strong, nonatomic) NSString *mainMiles;
@property (strong, nonatomic) NSString *mainLocation;
@property (strong, nonatomic) NSDate *mainStartDate;
@property (strong, nonatomic) NSDate *mainEndDate;
@property (strong, nonatomic) NSString *mainMinPrice;
@property (strong, nonatomic) NSString *mainmaxPrice;

// property for rate screen.

@property (strong, nonatomic) NSString *workerTaskDetail_taskDetailObj_taskid;
@property (strong, nonatomic) NSString *workerTaskDetail_userId_createdBy;
@property (strong, nonatomic) NSString *workerTaskDetail_taskDetailObj_taskName;
@property (strong, nonatomic) NSString *workerTaskDetail_name_createdBy;

// property for share screen.

@property (strong, nonatomic) NSString *reservationTitle_forShare;
@property (strong, nonatomic) NSString *reservationDate_Time_forShare;
@property (strong, nonatomic) NSString *reservationLocation_forShare;
@property (strong, nonatomic) NSString *reservationDetailString_forShare;

@property (strong, nonatomic) UIImage *reservationImage_forShare;
@property (strong, nonatomic) NSString *reservationImgName_forShare;

@property (strong, nonatomic) NSString *taskIdFromReviewClass_forRejectionClass;
@property (strong, nonatomic) NSString *reviewCounter_ForHamburger;
@property (assign, nonatomic) BOOL isFromPushNotification;
@property (strong, nonatomic) NSString *reservationId_FromPush;
@property (strong, nonatomic) NSString *notificationType_FromPush;

@property (assign, nonatomic) BOOL isFromMapVCForFilter;

@property (strong, nonatomic) NSIndexPath *SelectedIndexPathForFilter;



@property (strong, nonatomic) UIImage *Image_forReviewDetailToShare;
@property (strong, nonatomic) NSString *ImgName_forReviewDetailToShare;
@property (strong, nonatomic) NSString *DetailString_forReviewDetailToShare;

@property (strong, nonatomic) NSString *Address_FromBioScreen;

@property (strong, nonatomic) NSString *reservationBoughtStatus_ForShare; // by vs 1 june 17.

@property (assign, nonatomic) BOOL isMessageViewTapped;

@property (assign, nonatomic) NSUserDefaults *LoginCheck;

@property (assign, nonatomic) BOOL isFromHistory_ForEdit; // By vs on 22 June 17

//@property (strong, nonatomic) NSString *viewAPI_ViewName;


@property (assign, nonatomic) BOOL isTermsAndPolicyViewOpen;    //by kp on 24Jan

@property (strong, nonatomic) NSString *Authorize_String_BrainTree; // By vs 11 July 18...

@property (assign, nonatomic) BOOL isLocationApiFail; //By VS for manage alert for location error 30 Aug 18...

@property (strong, nonatomic) NSString *lastSearchLocation; // By VS for manage to show last successfull searched location name 31 Aug 18...

@property (strong, nonatomic) NSString * selectedPaymentTypeFromServer; // By VS for manage to send selected payment method to server name 31 Aug 18...

@property (assign, nonatomic) BOOL isFromTapPayPalRadioBtnOnCreateBio;
@property (assign, nonatomic) BOOL isFromTapStripeRadioBtnOnCreateBio;

@property (assign, nonatomic) BOOL iscashOutPayPallFilled;
@property (assign, nonatomic) BOOL ispayWithPayPallFilled;
@property (assign, nonatomic) BOOL isStripeBankInfoFilled;
@property (assign, nonatomic) BOOL isStripeCardInfoFilled;

@end

