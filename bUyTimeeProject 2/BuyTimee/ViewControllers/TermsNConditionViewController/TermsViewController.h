//
//  TermsViewController.h
//  HandApp
//
//  Created by  ~ on 01/03/16.
//  Copyright (c) 2016 . All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Flurry.h"

@interface TermsViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintBtnAgreeHeight;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consBtnAgreeBottom;

- (IBAction)agreeBtnClicked:(id)sender;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consLblTermsHeight;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consLblTermsTop;

@end
