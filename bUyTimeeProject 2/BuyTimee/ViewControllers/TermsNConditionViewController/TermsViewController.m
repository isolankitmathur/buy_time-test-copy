//
//  TermsViewController.m
//  HandApp
//
//  Created by  ~ on 01/03/16.
//  Copyright (c) 2016 . All rights reserved.
//

#import "TermsViewController.h"
#import "Constants.h"
#import "TermsAndCondition.h"
#import "GlobalFunction.h"
#import "TaskTableViewCell.h"

@interface TermsViewController ()<UIWebViewDelegate>

@property (weak, nonatomic) IBOutlet UIButton *btnAgree;
- (IBAction)agreeBtnClicked:(id)sender;

@end

@implementation TermsViewController{
    
}

- (void)viewDidLoad {
    
    [super viewDidLoad];

    NSString* final = @"T&C detail page";
    
    [Flurry logEvent:final];
    
    myAppDelegate.vcObj.btnMapOnListView.hidden = true;//vs
    myAppDelegate.vcObj.BtnTransparentMap.hidden = true;//vs
    
  // NSDictionary*dictReg = [webServicesShared token:myAppDelegate.deviceTokenString];
    NSDictionary*dictReg = [webServicesShared token:@"1234"];
    NSArray *arrayTerms;
    NSString * result = @"";
    NSString *myDescriptionHTML = @"";;
    
    if (dictReg != nil) {
        
        
        NSLog(@"%@",[EncryptDecryptFile decrypt:[dictReg valueForKey:@"responseCode"]]);
        
        
        if ([[EncryptDecryptFile decrypt:[dictReg valueForKey:@"responseCode"]] isEqualToString:@"200"]) {
         
            
            
            NSDictionary *dictDataReg = [dictReg objectForKey:@"data"];
            
          // arrayTerms= [dictDataReg valueForKey:@"description"];
            result= [dictDataReg valueForKey:@"description"];
            
            myDescriptionHTML = [NSString stringWithFormat:@"<html> \n"
                                 "<head> \n"
                                 "<style type=\"text/css\"> \n"
                                 "body {font-family: \"%@\"; font-size: %f;}\n"
                                 "</style> \n"
                                 "</head> \n"
                                 "<body>%@</body> \n"
                                 "</html>", @"Lato-Light", 14.0, [EncryptDecryptFile decrypt:result]];
            


        }
    }
    else
    {
        
        myDescriptionHTML = [NSString stringWithFormat:@"<html> \n"
                                                          "<head> \n"
                                                          "<style type=\"text/css\"> \n"
                                                          "body {font-family: \"%@\"; font-size: %f;}\n"
                                                          "</style> \n"
                                                          "</head> \n"
                                                          "<body>%@</body> \n"
                                                          "</html>", @"Lato-Light", 14.0, @"Network error, please try again in few minutes."];

        
    }
    
  ////////////////////////////////// Lato-Medium Lato-Regular Lato-Light
    NSLog(@"main file is..%@",[EncryptDecryptFile decrypt:result]);
    
//    NSMutableArray *arr = [DatabaseClass getDataFromTnCData:[[NSString stringWithFormat:@"Select * from %@",tableTnC] UTF8String]];
//    
    
    
    
    
    //    if (arr.count>0) {
//        //TermsAndCondition *tncObj = [arr firstObject];
//        TermsAndCondition *tncObj = [arr firstObject];
//        
//        myDescriptionHTML = [NSString stringWithFormat:@"<html> \n"
//                                       "<head> \n"
//                                       "<style type=\"text/css\"> \n"
//                                       "body {font-family: \"%@\"; font-size: %f;}\n"
//                                       "</style> \n"
//                                       "</head> \n"
//                                       "<body>%@</body> \n"
//                                       "</html>", @"helvetica", 14.0, [EncryptDecryptFile decrypt:tncObj.termsAndCondition]];
//        
//    }else{
//        
//        myDescriptionHTML = [NSString stringWithFormat:@"<html> \n"
//                             "<head> \n"
//                             "<style type=\"text/css\"> \n"
//                             "body {font-family: \"%@\"; font-size: %f;}\n"
//                             "</style> \n"
//                             "</head> \n"
//                             "<body>%@</body> \n"
//                             "</html>", @"helvetica", 14.0, @"Network error, please try again in few minutes."];
//
//    }
    
    [_webView loadHTMLString:myDescriptionHTML baseURL:nil];

    self.webView.delegate = self;

   // myAppDelegate.vcObj.lblTitleText.text =@"B U Y  T I M E E";

    if (IS_IPHONE_4_OR_LESS) {
        [self.btnAgree.titleLabel setFont:[UIFont systemFontOfSize:12.0]];
    }
    if (IS_IPHONE_5) {
        [self.btnAgree.titleLabel setFont:[UIFont systemFontOfSize:15.0]];
    }
    if (IS_IPHONE_6) {
        [self.btnAgree.titleLabel setFont:[UIFont systemFontOfSize:19.0]];
    }
    if (IS_IPHONE_6P) {
        [self.btnAgree.titleLabel setFont:[UIFont systemFontOfSize:23.0]];
    }

    if ([UserDefaults boolForKey:keyAgreeTnC]) {
        
        [self.btnAgree setTitle:@"You have already agreed" forState:UIControlStateNormal];
        
        self.btnAgree.enabled = YES;

    }
    
//    myAppDelegate.stringNameOfChildVC = [NSString stringWithFormat:@"%@",[self class]];
    NSLog(@"name of stringNameOfChildVC in tnc is...%@",myAppDelegate.stringNameOfChildVC);
    
}

-(void) viewWillAppear:(BOOL)animated
{
//    CGRect screenRect = [[UIScreen mainScreen] bounds];
//    //CGFloat screenWidth = screenRect.size.width;
//    CGFloat screenHeight = screenRect.size.height;
//    
//    
//   // CGFloat height = [string floatValue] + 8;
//    CGRect frame = [_webView frame];
//    frame.size.height  = screenHeight - 18;
//     NSLog(@"height%f",frame.size.height);
//    [_webView setFrame:frame];

   }

-(void)webViewDidFinishLoad:(UIWebView *)webView{
    [GlobalFunction removeIndicatorView];
}

-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    [GlobalFunction removeIndicatorView];
}

-(void)viewDidAppear:(BOOL)animated{
    
    if(myAppDelegate.isFromCreateBioOnTnC == true)
    {
        myAppDelegate.TandC_Buyee=@"home";
        myAppDelegate.isFromCreateBioOnTnC = true;
    }

    
    
    [self.view layoutIfNeeded];
    
   // self.constraintBtnAgreeHeight.constant = 48;
    self.consBtnAgreeBottom.constant = 15;
    self.consLblTermsHeight.constant = 0;
    self.consLblTermsTop.constant = 10;
    
//    if (IS_IPHONE_4_OR_LESS) {
//        self.constraintBtnAgreeHeight.constant = 48;
//    }
//    if (IS_IPHONE_5) {
//        self.constraintBtnAgreeHeight.constant = 58;
//    }
//    if (IS_IPHONE_6) {
//        self.constraintBtnAgreeHeight.constant = 68;
//    }
//    if (IS_IPHONE_6P) {
//        self.constraintBtnAgreeHeight.constant = 78;
//    }
//    
    [self.view layoutIfNeeded];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)agreeBtnClicked:(id)sender {
    
    if (![UserDefaults boolForKey:keyBioFilled]) {
        
     //   myAppDelegate.isFromTnC = true;
        
        myAppDelegate.isSideBarAccesible = false;
        
//        [myAppDelegate.vcObj performSegueWithIdentifier:@"createBio" sender:myAppDelegate.vcObj.btnCreateBioSegue];
        
          [myAppDelegate.vcObj performSegueWithIdentifier:@"login" sender:myAppDelegate.loginVCObj];

        myAppDelegate.vcObj.btnBack.hidden = YES;
        myAppDelegate.vcObj.btnSideBarOpen.hidden = YES;

        if (![UserDefaults boolForKey:keyAgreeTnC]) {
            
            [UserDefaults setBool:YES forKey:keyAgreeTnC];
            [UserDefaults synchronize];
            
        }
        
        myAppDelegate.termsNconditionVCObj = nil;

    }

}

@end
