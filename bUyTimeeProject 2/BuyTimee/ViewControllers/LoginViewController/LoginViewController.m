//
//  LoginViewController.m
//  BuyTimee
//
//  Created by User14 on 02/03/16.

//

#import "LoginViewController.h"
#import "Constants.h"
#import "ForgotPasswordViewController.h"
#import "TaskTableViewCell.h"
#import "GlobalFunction.h"
#import "user.h"
#import "subCategory.h"
#import "category.h"
#import "BioInfoTableViewCell.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <Fabric/Fabric.h>
#import <TwitterKit/TwitterKit.h>
#import "BusinessInfo.h"

//#import <GoogleOpenSource/GoogleOpenSource.h>

@import SafariServices;


@interface LoginViewController ()

@end

 //601998664624-3cpojkemfcfpjli3bqdn98q1dhhqd2na.apps.googleusercontent.com // dummy client id for ggl login creted by vs on 5 feb 17 BTimeeDummy (app name on console).
//com.BT.BuyTimeeApp

//static NSString * const kClientId = @"601998664624-3cpojkemfcfpjli3bqdn98q1dhhqd2na.apps.googleusercontent.com";

static NSString * const kClientId = @"292616665360-ctqcbcheau6tp8sik9u9344vsn5govnj.apps.googleusercontent.com";

@implementation LoginViewController{
    
    NSInteger socialFlag;
    NSString *socialEmail;
    NSString *socialId;
    NSString *fbId;
    NSString *googleId;
    NSString *socialUsername;
    NSString *picPath;
    NSString *description;
    NSString *fname;
    NSString *lname;
    
    NSString *savedLoginId1;
    NSString *savedMailId1;
}

@synthesize txtActiveField;
@synthesize inputAccView;
@synthesize btnDone;
@synthesize btnNext;
@synthesize btnPrev;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSLog(@"view height...%f",self.overlaySplashView.frame.size.height);
    NSLog(@"view width...%f",self.overlaySplashView.frame.size.width);
    

    if (IS_IPHONE_5)
    {
        
    [self.overlaySplashView setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"Group5.png"]]];
    
    self.overlaySplashView.clipsToBounds = YES;
    }
    if (IS_IPHONE_6)
    {
        
        UIGraphicsBeginImageContext(self.overlaySplashView.frame.size);
                [[UIImage imageNamed:@"Group.png"] drawInRect:self.overlaySplashView.bounds];
                UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
                UIGraphicsEndImageContext();
                self.overlaySplashView.backgroundColor = [UIColor colorWithPatternImage:image];
        
    }
    if (IS_IPHONE_6P)
    {
        
        [self.overlaySplashView setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"Group6Plus.png"]]];
        
        self.overlaySplashView.clipsToBounds = YES;
    }
    self.overlaySplashView.hidden = YES;
    //set delegate
    self.textFieldPassword.delegate = self;
    self.textFieldUsername.delegate = self;
    
    //set place holders
    NSAttributedString *str1 = [[NSAttributedString alloc] initWithString:@"Username/Email" attributes:@{ NSForegroundColorAttributeName : [UIColor whiteColor],
                                                                                                    }];
    self.textFieldUsername.attributedPlaceholder = str1;
    
    NSAttributedString *str3 = [[NSAttributedString alloc] initWithString:@"Password" attributes:@{ NSForegroundColorAttributeName : [UIColor whiteColor],
                                                                                                    }];
    self.textFieldPassword.attributedPlaceholder = str3;
    
    UIFont *font = self.textFieldPassword.font;
    
    //font adjustments
    if (IS_IPHONE_4_OR_LESS) {
        self.textFieldPassword.font = [font fontWithSize:14.0];
        self.textFieldUsername.font = [font fontWithSize:14.0];
    }
    if (IS_IPHONE_5) {
        self.textFieldPassword.font = [font fontWithSize:15.0];
        self.textFieldUsername.font = [font fontWithSize:15.0];
    }
    if (IS_IPHONE_6) {
        self.textFieldPassword.font = [font fontWithSize:16.0];
        self.textFieldUsername.font = [font fontWithSize:16.0];
    }
    if (IS_IPHONE_6P) {
        self.textFieldPassword.font = [font fontWithSize:17.0];
        self.textFieldUsername.font = [font fontWithSize:17.0];
    }
    
    socialFlag = 0;
    socialEmail = @"";
    socialId = @"";
    fbId = @"";
    socialUsername = @"";
    googleId = @"";
    picPath = @"";
    description = @"";
    fname = @"";
    lname = @"";
    
    
    
    [GIDSignIn sharedInstance].uiDelegate = self;
    [GIDSignIn sharedInstance].delegate=self;
    [[GIDSignIn sharedInstance] signOut];

    
    
//    //temporary
//    self.textFieldPassword.text = @"123456";
//    self.textFieldUsername.text = @"mollyhayes";
    
    
    
    
    
    // google signin by vs
    
    // Do any additional setup after loading the view, typically from a nib.
    
    //AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
  //  GPPSignIn *signIn = [GPPSignIn sharedInstance];
    
    
    
  //  signIn.shouldFetchGoogleUserEmail = YES;  // Uncomment to get the user's email
    
    // You previously set kClientId in the "Initialize the Google+ client" step
 //   signIn.clientID = kClientId;
    
    // Your server's OAuth 2.0 client ID
    // signIn.homeServerClientID = @"<SERVER-CLIENT-ID>";
    
    
    
    //signIn.clientID = @"486705077594-e6fdqju30tg801c0oh3eb1pg8j24053b.apps.googleusercontent.com";
    // Uncomment one of these two statements for the scope you chose in the previous step
//    signIn.scopes = @[ kGTLAuthScopePlusLogin ];  // "https://www.googleapis.com/auth/plus.login" scope
    // signIn.scopes = @[ @"profile" ];            // "profile" scope
    
    // Optional: declare signIn.actions, see "app activities"
   // signIn.delegate = self;
    
   //  [signIn authenticate];
    

  //  [signIn trySilentAuthentication];
    
    
    // end google signin work by vs
    
    
}


//-(void)refreshInterfaceBasedOnSignIn {
//    if ([[GPPSignIn sharedInstance] authentication]) {
//        // The user is signed in.
//        // self.signInButton.hidden = YES;
//        // Perform other actions here, such as showing a sign-out button
//
//
//
//
//    } else {
//        self.signInButton.hidden = NO;
//        // Perform other actions here
//    }
//}

- (void)didDisconnectWithError:(NSError *)error {
    
    [GlobalFunction removeIndicatorView];
    myAppDelegate.isGooglePressed = NO;
    if (error) {
        
        NSLog(@"Received error %@", error);
    } else {
        // The user is signed out and disconnected.
        // Clean up user data as specified by the Google+ terms.
    }
}

/*
- (void)finishedWithAuth:(GTMOAuth2Authentication *)auth error:(NSError *)error {
    NSLog(@"Received Error %@ and auth object==%@", error, auth);
    
   // [self performSelector:@selector(finallyAddIndicator) withObject:nil afterDelay:0.1];
   
    
   // [self performSelector:@selector(finallyAddIndicator) withObject:nil afterDelay:0.0];
    if (error) {
        // Do some error handling here.
        [GlobalFunction removeIndicatorView];
         myAppDelegate.isGooglePressed = NO;
    } else {
        
        [GlobalFunction addIndicatorView];
        
        [self refreshInterfaceBasedOnSignIn];
        NSString *serverCode = [GPPSignIn sharedInstance].homeServerAuthorizationCode;
        GTLQueryPlus *query = [GTLQueryPlus queryForPeopleGetWithUserId:@"me"];
        
        NSString  *accessTocken = [auth valueForKey:@"accessToken"]; // access tocken pass in .pch file
        NSLog(@"token is.... %@",accessTocken);
        
        
        NSLog(@"email %@ ", [NSString stringWithFormat:@"Email: %@",[GPPSignIn sharedInstance].authentication.userEmail]);
        NSLog(@"Received error %@ and auth object %@",error, auth);
        // 1. Create a |GTLServicePlus| instance to send a request to Google+.
        GTLServicePlus* plusService = [[GTLServicePlus alloc] init] ;
        plusService.retryEnabled = YES;
        
        // 2. Set a valid |GTMOAuth2Authentication| object as the authorizer.
        [plusService setAuthorizer:[GPPSignIn sharedInstance].authentication];
        
        // 3. Use the "v1" version of the Google+ API.*
        plusService.apiVersion = @"v1";
        [plusService executeQuery:query
                completionHandler:^(GTLServiceTicket *ticket,
                                    GTLPlusPerson *person,
                                    NSError *error) {
                    if (error) {
                        //Handle Error
                        
                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Buy Timee"
                                                                        message:error.localizedDescription
                                                                       delegate:self
                                                              cancelButtonTitle:@"Cancel"
                                                              otherButtonTitles:nil];
                        [alert show];
                        
                        return;
                        
                        
                    } else {
                        //NSLog(@"Email= %@", [GPPSignIn sharedInstance].authentication.userEmail);
                        NSLog(@"GoogleID=%@", person.identifier);
                        NSLog(@"Email is= %@", person.emails);
                        NSLog(@"User Name=%@", [person.name.givenName stringByAppendingFormat:@" %@", person.name.familyName]);
                        NSLog(@"Gender=%@", person.gender);
                        NSLog(@"BirthDay =%@", person.birthday);
                        NSLog(@"Description All =%@", person.description);
                        NSLog(@"Lang=%@", person.language);
                        NSLog(@"About me =%@", person.aboutMe);
                        NSLog(@"Display Name =%@", person.displayName);
                        
                      //  socialFlag = 2;
                        socialUsername = person.displayName;
                        socialFlag = 2;
                        googleId = person.identifier;
                        
                        socialEmail = [GPPSignIn sharedInstance].authentication.userEmail;
                        
           // NSURL *urlPic = [NSURL URLWithString:[NSString stringWithFormat:@"%@",person.image.url]];
                        
                        picPath = [NSString stringWithFormat:@"%@",person.image.url];
                        
                    }
                    GPPSignIn *signIn = [GPPSignIn sharedInstance];
                    NSString  *message =[NSString stringWithFormat:@"%@ %@ %@",person.identifier,signIn.authentication.userEmail,person.name.givenName];
                    
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"My Test App"
                                                                    message:message
                                                                   delegate:self
                                                          cancelButtonTitle:@"Cancel"
                                                          otherButtonTitles:nil];
                  //  [alert show];
                    
               
                    
                    [[GPPSignIn sharedInstance] signOut];
                    
                  //  [self performSelector:@selector(finallyAddIndicator) withObject:nil afterDelay:0.0];
                    
                    [self performSelector:@selector(completeLoginCheck) withObject:nil afterDelay:0.5];
                    
                    
                }];
    }
    

}  //114884608068359752907

*/


-(void)viewWillAppear:(BOOL)animated{

    self.lblLineOne.alpha = 0.5;
    self.lblLineTwo.alpha = 0.5;
    
    
    UIImage *btnImage = [UIImage imageNamed:@"googleLogin.png"];
    [self.btnLoginWithGoogle setImage:btnImage forState:UIControlStateNormal];
    [self updateConstraints];
   
    
    myAppDelegate.LoginCheck = [NSUserDefaults standardUserDefaults];
    
  savedLoginId1 = [myAppDelegate.LoginCheck stringForKey:@"savingLoginId"];
  savedMailId1 = [myAppDelegate.LoginCheck stringForKey:@"savingMailId"];
    
    NSLog(@"saved mail in default is..%@", savedMailId1);
    NSLog(@"saved id in default is..%@", savedLoginId1);
     NSLog(@"saved id in default is..%f", self.consLblBuyTimeTop.constant);
    if (savedLoginId1.length>0)
    {
 [self performSelector:@selector(finallyAddIndicator) withObject:nil afterDelay:0.1];
    // [self completeLoginCheck];
        
     //   [GlobalFunction addIndicatorView];
//        self.btnLoginWithGoogle.hidden = true;
//        self.btnLoginWithFacebook.hidden = true;
//        self.lblLineOne.hidden = true;
//        self.lblLineTwo.hidden = true;
//        self.lblOr.hidden = true;
//        self.btnSkip.hidden = true;
//        self.btnTermsOfService.hidden = true;
//        self.lblLineUnderSkip.hidden = true;
//        self.lblLineUnderTnC.hidden = true;
        

        
        
        self.overlaySplashView.hidden = NO;
    }
    
//    if (IS_IPHONE_6)
//    {
//        UIGraphicsBeginImageContext(self.overlaySplashView.frame.size);
//        [[UIImage imageNamed:@"Group.png"] drawInRect:self.overlaySplashView.bounds];
//        UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
//        UIGraphicsEndImageContext();
//        self.overlaySplashView.backgroundColor = [UIColor colorWithPatternImage:image];
//    }
//    if (IS_IPHONE_6P)
//    {
//        
//        UIGraphicsBeginImageContext(self.overlaySplashView.frame.size);
//        [[UIImage imageNamed:@"Group6Plus.png"] drawInRect:self.overlaySplashView.bounds];
//        UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
//        UIGraphicsEndImageContext();
//        self.overlaySplashView.backgroundColor = [UIColor colorWithPatternImage:image];
//    }
//    if (IS_IPHONE_5)
//    {
//        UIGraphicsBeginImageContext(self.overlaySplashView.frame.size);
//        [[UIImage imageNamed:@"Group5.png"] drawInRect:self.overlaySplashView.bounds];
//        UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
//        UIGraphicsEndImageContext();
//        self.overlaySplashView.backgroundColor = [UIColor colorWithPatternImage:image];
//    }
//
    
    
}


-(void)viewDidAppear:(BOOL)animated
{
   //[self completeLoginCheck];
    if (savedLoginId1.length>0)
    {

    [self performSelector:@selector(completeLoginCheck) withObject:nil afterDelay:0.5];
        
    }
    
    NSLog(@"view height...%f",self.overlaySplashView.frame.size.height);
    NSLog(@"view width...%f",self.overlaySplashView.frame.size.width);
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)resignTextFields{
    [self.textFieldUsername resignFirstResponder];
    [self.textFieldPassword resignFirstResponder];
}

- (IBAction)notAMemberYetClicked:(id)sender {
    
    [self resignTextFields];
    
    myAppDelegate.registrationVCObj = nil;
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    myAppDelegate.registrationVCObj = (RegistrationViewController *)[storyboard instantiateViewControllerWithIdentifier:@"RegistrationViewController"];
    
    if (myAppDelegate.registrationVCObj) {
        [self presentViewController:myAppDelegate.registrationVCObj animated:YES completion:nil];
    }
        
}

- (IBAction)forgotPasswordClicked:(id)sender {
    
    [self resignTextFields];
    
    myAppDelegate.forgotPassVCObj = nil;
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    myAppDelegate.forgotPassVCObj = (ForgotPasswordViewController *)[storyboard instantiateViewControllerWithIdentifier:@"ForgotPasswordViewController"];
    
    if (myAppDelegate.forgotPassVCObj) {
        [self presentViewController:myAppDelegate.forgotPassVCObj animated:YES completion:nil];
    }
    
}

- (IBAction)loginWithFacebookClicked:(id)sender {
    
     myAppDelegate.isFromSkip = NO;
    
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    
    [login logOut];
    [FBSDKAccessToken setCurrentAccessToken:nil];
    [FBSDKProfile setCurrentProfile:nil];
    
    
    login.loginBehavior=FBSDKLoginBehaviorWeb;
    [login
     logInWithReadPermissions: @[@"public_profile",@"email"]
     fromViewController:self
     handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
         if (error) {
             NSLog(@"Process error");
             [[GlobalFunction shared] showAlertForMessage:[error localizedDescription]];
         } else if (result.isCancelled) {
             NSLog(@"Cancelled");
         } else {
             
             if ([FBSDKAccessToken currentAccessToken]) {
                 NSMutableDictionary* parameters = [NSMutableDictionary dictionary];
                 [parameters setValue:@"id,name,email,picture,first_name,last_name" forKey:@"fields"];
                 [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:parameters]
                  startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
                    //  ,photos,about,birthday,bio,first_name,gender,last_name,location,cover,education
                      if (!error) {
                          
                          NSLog(@"fetched user data ---- %@",result);
                          
                          NSDictionary *dictDataReg1 = [result objectForKey:@"picture"];
                          NSDictionary *dictDataReg2 = [dictDataReg1 objectForKey:@"data"];
                          
                          [self resignTextFields];
                          
                          socialEmail = [result valueForKey:@"email"];
                          picPath = [dictDataReg2 valueForKey:@"url"];
                          

                          if (![socialEmail isEqualToString:@""]) {
                              
                              //socialId = [result valueForKey:@"id"]; // comment by vs bcz use fbid as social id...
                              fbId = [result valueForKey:@"id"];
                              socialFlag = 1;
                              
                              socialUsername = [result valueForKey:@"name"];
                              
                              NSString *s=[result valueForKey:@""];
                              NSLog(@"%@",s);
                              
                              
                              
                              
//                              NSArray *usernameArr = [socialEmail componentsSeparatedByString:@"@"];
//                              
//                              socialUsername = [usernameArr objectAtIndex:0]; // These two lines comment by vs to add main full name in socialUsername variable 2 feb 17.
                              
                              
                              [self performSelector:@selector(finallyAddIndicator) withObject:nil afterDelay:0.0];
                              
                              [self performSelector:@selector(completeLoginCheck) withObject:nil afterDelay:0.5];
                              
                          }else{
                              
                              [[GlobalFunction shared] showAlertForMessage:@"Email id is required to login."];
                              
                          }

                      }else{
                          
                          [[GlobalFunction shared] showAlertForMessage:[error localizedDescription]];
                          
                      }
                  }];
             }
         }
     }];
    
}

- (IBAction)loginWithTwitterClicked:(id)sender {
    
     [GlobalFunction addIndicatorView];
  
     myAppDelegate.isFromSkip = NO;
    
    if (![[WebService shared] connected]) {
        
        [GlobalFunction removeIndicatorView];
        
        [[GlobalFunction shared] showAlertForMessage:@"Can't connect to internet, please check and try again."];
        
        return;
    }

    myAppDelegate.isGooglePressed = YES;
    
    [[GIDSignIn sharedInstance] signIn];

    return;
    
    TWTRSessionStore *store = [[Twitter sharedInstance] sessionStore];
    
    if (store) {
      
        NSString *userID = store.session.userID;
        
        if (userID) {
          
            [store logOutUserID:userID];

        }
        // If using the log in methods on the Twitter instance

    }
    
    [[Twitter sharedInstance] logInWithMethods:TWTRLoginMethodWebBased completion:^(TWTRSession *session, NSError *error) {
        
        if (session) {
            
            NSLog(@"signed in as %@", [session userName]);
            
            socialUsername = [session userName];
            socialFlag = 2;
            socialId = [session userID];
            
            TWTRAPIClient *client = [TWTRAPIClient clientWithCurrentUser];
            NSURLRequest *request = [client URLRequestWithMethod:@"GET"
                                                             URL:@"https://api.twitter.com/1.1/account/verify_credentials.json"
                                                      parameters:@{@"include_email": @"true", @"skip_status": @"true"}
                                                           error:nil];
            
            [client sendTwitterRequest:request completion:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
                
                if (!connectionError) {
                    
                    NSDictionary *jsonDict = nil;
                    if (data != nil) {
                        
                        jsonDict = [NSJSONSerialization JSONObjectWithData:data
                                                                   options:NSJSONReadingMutableContainers
                                                                     error:nil];
                        
                    }
                    
                    socialEmail = [jsonDict valueForKey:@"email"];
                    
                    if (![socialEmail isEqualToString:@""]) {
                        
                        [self performSelector:@selector(finallyAddIndicator) withObject:nil afterDelay:0.0];
                        
                        [self performSelector:@selector(completeLoginCheck) withObject:nil afterDelay:0.5];

                    }else{
                        
                        [[GlobalFunction shared] showAlertForMessage:@"Email id is required to login."];
                        
                    }

                }else{
                    
                    NSLog(@"error == %@",[connectionError localizedDescription]);
                    
//                    [[GlobalFunction shared] showAlertForMessage:[connectionError localizedDescription]];
                    
                }
                
            }];
            
        } else {
            NSLog(@"error: %@", [error localizedDescription]);
        }

    }];
    
}
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    NSURL *requestURL = [ request URL];
    
    if (navigationType == UIWebViewNavigationTypeLinkClicked)
    {
        // Call your method here
        return NO;
        
    }
    
    return YES;
}

- (void)signInWillDispatch:(GIDSignIn *)signIn error:(NSError *)error {
    [GlobalFunction removeIndicatorView];
}

// Present a view that prompts the user to sign in with Google
- (void)signIn:(GIDSignIn *)signIn
presentViewController:(UIViewController *)viewController {
    [self presentViewController:viewController animated:YES completion:nil];
}

// Dismiss the "Sign in with Google" view
- (void)signIn:(GIDSignIn *)signIn
dismissViewController:(UIViewController *)viewController {
    
    [GlobalFunction addIndicatorView];
    [self dismissViewControllerAnimated:YES completion:^ {
        
    }];
    
    
    
    
}


- (void)signIn:(GIDSignIn *)signIn didSignInForUser:(GIDGoogleUser *)user
     withError:(NSError *)error {
    
    
    if (error) {
        // Do some error handling here.
        [GlobalFunction removeIndicatorView];
        myAppDelegate.isGooglePressed = NO;
    } else {
    
         [GlobalFunction addIndicatorView];
    NSString *userId = user.userID;                  // For client-side use only!
    NSString *idToken = user.authentication.idToken; // Safe to send to the server
    NSString *fullName = user.profile.name;
    NSString *givenName = user.profile.givenName;
    NSString *familyName = user.profile.familyName;
    NSString *email = user.profile.email;
    NSURL *imageUrl = [user.profile imageURLWithDimension:60];
    
    NSData *data = [NSData dataWithContentsOfURL:imageUrl];
    UIImage *img = [[UIImage alloc] initWithData:data];
       
        
        
        socialUsername = fullName;
        socialFlag = 2;
        googleId = userId;
        
        socialEmail = email;
        picPath = [NSString stringWithFormat:@"%@",[user.profile imageURLWithDimension:60]];

        if(userId.length>0)
        {
            [[GIDSignIn sharedInstance] signOut];
            [self performSelector:@selector(completeLoginCheck) withObject:nil afterDelay:0.5];

        }
        else
        {
            if (error!= nil)
            {
                
            }
        }

    }
    
    
   
    
}


- (void)signIn:(GIDSignIn *)signIn
didDisconnectWithUser:(GIDGoogleUser *)user
     withError:(NSError *)error {
    
    [GlobalFunction removeIndicatorView];
    
    
    
    // Perform any operations when the user disconnects from app here.
    // ...
}





- (IBAction)loginClicked:(id)sender {
    
//    [self performSelector:@selector(finallyAddIndicator) withObject:nil afterDelay:0.0];
//    
//    [self resignTextFields];
//    
//    if ([[self.textFieldUsername.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""]) {
//        [[GlobalFunction shared] showAlertForMessage:@"Please enter a valid Username/Email address."];
//        
//        [GlobalFunction removeIndicatorView];
//        
//        return;
//    }
//    if ([[self.textFieldPassword.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""]) {
//        [[GlobalFunction shared] showAlertForMessage:@"Please enter password and try again."];
//        
//        [GlobalFunction removeIndicatorView];
//        
//        return;
//    }
    
    [self performSelector:@selector(completeLoginCheck) withObject:nil afterDelay:0.5];
    
}

-(void)completeLoginCheck{
    
//    if([[NSUserDefaults standardUserDefaults] valueForKey:@"isLogin"]){
//        
//    }
    
    float curLat = [GlobalFunction getCurrentLatitude];
    float curLong = [GlobalFunction getCurrentLongitude];
    
    NSString *LattitudeStr = [NSString stringWithFormat:@"%f", curLat];
    NSString *LongitudeStr = [NSString stringWithFormat:@"%f", curLong];
    
    NSLog(@"testBool = %@", keyIsLogin.description);
    
    
    if ([UserDefaults boolForKey:keyIsLogin]) {
        
         NSLog(@"testBool = %@", keyIsLogin.description);
        
        NSDictionary *dictionaryLogin = [UserDefaults objectForKey:keyUserloginDetailDictionary];
       // NSString *userName = [EncryptDecryptFile decrypt:[dictionaryLogin valueForKey:keyUsername]];
//        NSString *password = [EncryptDecryptFile decrypt:[dictionaryLogin valueForKey:keyPass]];
       // NSString *socialId_userdefaults = [EncryptDecryptFile decrypt:[dictionaryLogin valueForKey:keysocialId_fb]];
       // NSString *socialFlag_userdefaults = [EncryptDecryptFile decrypt:[dictionaryLogin valueForKey:keysocialFlag_fb]];
        
//        if ([socialFlag_userdefaults integerValue] == 0 && [socialFlag_userdefaults isEqualToString:[NSString stringWithFormat:@"%ld",(long)socialFlag]]) {
//            socialUsername = userName;
//            if (![self.textFieldUsername.text isEqualToString:userName]) {
//                socialUsername = @"";
//                [UserDefaults setBool:NO forKey:keyIsLogin];
//            }
//        }else{
//            if ([socialFlag_userdefaults isEqualToString:[NSString stringWithFormat:@"%ld",(long)socialFlag]] && [socialId isEqualToString:socialId_userdefaults]) {
//                
//            }else{
//                socialUsername = @"";
//                [UserDefaults setBool:NO forKey:keyIsLogin];
//            }
//        }
    }

  //  if (![UserDefaults boolForKey:keyIsLogin]) {
        
        if (![[WebService shared] connected]) {
            
            
            [GlobalFunction removeIndicatorView];
            
            
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"BUY TIMEE" message:@"Can't connect to internet, please check and try again." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:@"Retry", nil];
            
            alertView.tag = 999;
            [alertView show];

            return;
            
//            [GlobalFunction removeIndicatorView];
//            
//            [[GlobalFunction shared] showAlertForMessage:@"Can't connect to internet, please check and try again."];
//            
//            return;
        }
        
        if (socialFlag == 0) {
            
            NSLog(@"came fron skip button and social flag is..%ld", (long)socialFlag);
           // socialUsername = self.textFieldUsername.text;
            
        }else if (socialFlag == 1 || socialFlag == 2){
            
            self.textFieldUsername.text = @"";
            self.textFieldPassword.text = @"";
            
        }
    
    NSLog(@"came fron skip button and social flag is..%ld", (long)socialFlag);
    NSLog(@"came fron skip button and social flag is..%@", socialId);
    NSLog(@"came fron skip button and social flag is..%@", socialUsername);
    NSLog(@"came fron skip button and social flag is..%@", socialEmail);
    NSLog(@"came fron skip button and social flag is..%@", fbId);
    NSLog(@"came fron skip button and social flag is..%@", googleId);

    //getting an NSString
    
    NSString *savedLoginId = [myAppDelegate.LoginCheck stringForKey:@"savingLoginId"];
    NSString *savedMailId = [myAppDelegate.LoginCheck stringForKey:@"savingMailId"];
    NSString *savedFlagValue = [myAppDelegate.LoginCheck stringForKey:@"flagValue"];
    
    NSLog(@"saved mail in default is..%@", savedMailId);
    NSLog(@"saved id in default is..%@", savedLoginId);
    NSLog(@"saved flag in default is..%@", savedFlagValue);
    
    if (savedLoginId.length<=0)
    {
        
        NSString *savingLoginId = @"";
        NSString *savingMailId = @"";
        NSString *flagValue = @"";
        
        if (googleId.length <=0)
        {
            savingLoginId = fbId;
            savingMailId = socialEmail;
            flagValue = @"1";
        }
        else if(fbId.length <=0)
        {
            savingLoginId = googleId;
            savingMailId = socialEmail;
            flagValue = @"2";
        }
        
        // saving an NSString
        [myAppDelegate.LoginCheck setObject:savingLoginId forKey:@"savingLoginId"];
        [myAppDelegate.LoginCheck setObject:savingMailId forKey:@"savingMailId"];
        [myAppDelegate.LoginCheck setObject:flagValue forKey:@"flagValue"];
        
        [myAppDelegate.LoginCheck synchronize];
        
    }
    else
    {
        
    }
    

    
 NSString *timeZoneSeconds;
 #if TARGET_OS_SIMULATOR
    
//    bgTask=(UIBackgroundTaskIdentifier *)UIBackgroundTaskInvalid;
//    UIApplication *app=[UIApplication sharedApplication];
//    bgTask=(UIBackgroundTaskIdentifier *)[app beginBackgroundTaskWithExpirationHandler:^{}];
    
    
    timeZoneSeconds = [GlobalFunction getTimeZone];
    
    NSDictionary*dictReg;
    
    if (savedLoginId.length<=0)
    {
    
    if (socialUsername.length <= 0) {
        socialUsername = @"";
    }
    
       dictReg = [webServicesShared description:description picPath:picPath token:myAppDelegate.deviceTokenString flag:socialFlag fbId:fbId googleId:googleId fname:socialUsername lname:@"" emailId:socialEmail latitude:LattitudeStr longitude:LongitudeStr timeZone:timeZoneSeconds];
    }
    else
    {
        if ([savedFlagValue isEqualToString:@"1"])
        {
            fbId = savedLoginId;
            socialEmail = savedMailId;
            socialFlag = [savedFlagValue integerValue];
        }
        else
        {
            googleId = savedLoginId;
            socialEmail = savedMailId;
            socialFlag = [savedFlagValue integerValue];
        }
         dictReg = [webServicesShared description:description picPath:picPath token:myAppDelegate.deviceTokenString flag:socialFlag fbId:fbId googleId:googleId fname:socialUsername lname:@"" emailId:socialEmail latitude:LattitudeStr longitude:LongitudeStr timeZone:timeZoneSeconds];

    }

#else
    
     timeZoneSeconds = [GlobalFunction getTimeZone];
    
    NSDictionary*dictReg;
    
    if (savedLoginId.length<=0)
    {
        
        if (socialUsername.length <= 0) {
            socialUsername = @"";
        }
    
     dictReg = [webServicesShared description:description picPath:picPath token:myAppDelegate.deviceTokenString flag:socialFlag fbId:fbId googleId:googleId fname:socialUsername lname:@"" emailId:socialEmail latitude:LattitudeStr longitude:LongitudeStr timeZone:timeZoneSeconds];
        
    }
    else
    {
        if ([savedFlagValue isEqualToString:@"1"])
        {
            fbId = savedLoginId;
            socialEmail = savedMailId;
            socialFlag = [savedFlagValue integerValue];
        }
        else
        {
            googleId = savedLoginId;
            socialEmail = savedMailId;
            socialFlag = [savedFlagValue integerValue];
        }
        dictReg = [webServicesShared description:description picPath:picPath token:myAppDelegate.deviceTokenString flag:socialFlag fbId:fbId googleId:googleId fname:socialUsername lname:@"" emailId:socialEmail latitude:LattitudeStr longitude:LongitudeStr timeZone:timeZoneSeconds];
        
    }
#endif
        
        if (dictReg != nil) {
            
            if ([[EncryptDecryptFile decrypt:[dictReg valueForKey:@"responseCode"]] isEqualToString:@"200"]) {
                                
                NSDictionary *dictDataReg = [dictReg objectForKey:@"data"];
                
                NSString *reviewCount = [EncryptDecryptFile decrypt:[dictDataReg valueForKey:@"review"]];
                
                myAppDelegate.reviewCounter_ForHamburger = reviewCount;
                
            NSString *lastupdatetm = [EncryptDecryptFile decrypt:[dictDataReg valueForKey:@"lastupdatedTime"]];
                
                 NSLog(@"last updated time is...%@", lastupdatetm);
                
                NSString *braintreeStr = [EncryptDecryptFile decrypt:[dictDataReg valueForKey:@"clientToken"]];
                
                myAppDelegate.Authorize_String_BrainTree = braintreeStr;
                
             NSString *usertype1 = [EncryptDecryptFile decrypt:[dictDataReg valueForKey:@"userType"]];
                
        [UserDefaults setValue:[EncryptDecryptFile decrypt:[dictDataReg valueForKey:@"lastupdatedTime"]] forKey:keyLastModifyTime];
        [UserDefaults setValue:usertype1 forKey:keyUserType];
                
                if ([DatabaseClass updateDataBaseFromDictionary:dictDataReg]) {
                    
                    [self loginComplete];
                    
//                    if ([[EncryptDecryptFile decrypt:[dictReg valueForKey:@"login_type"]] isEqualToString:@"NORMAL"]) {
//                        
//                        [self loginComplete];
//
//                    }else if ([[EncryptDecryptFile decrypt:[dictReg valueForKey:@"login_type"]] isEqualToString:@"OTP"]) {
//                        
//                        [self handleCaseForUpdatePassword];
//                        
//                    }
                    
                }else{
                    
                    [GlobalFunction removeIndicatorView];
                    
                }
                
            }else{
                
                [GlobalFunction removeIndicatorView];
                [[GlobalFunction shared] showAlertForMessage:[EncryptDecryptFile decrypt:[dictReg valueForKey:@"responseMessage"]]];
                
//                self.btnLoginWithGoogle.hidden = false;
//                self.btnLoginWithFacebook.hidden = false;
//                self.lblLineOne.hidden = false;
//                self.lblLineTwo.hidden = false;
//                self.lblOr.hidden = false;
//                self.btnSkip.hidden = false;
//                self.btnTermsOfService.hidden = false;
//                self.lblLineUnderSkip.hidden = false;
//                self.lblLineUnderTnC.hidden = false;

                [myAppDelegate.vcObj logOutProcess];
               
                [UIView transitionWithView:self.overlaySplashView
                                  duration:0.4
                                   options:UIViewAnimationOptionTransitionCrossDissolve
                                animations:^{
                                    self.overlaySplashView.hidden = YES;
                                }
                                completion:NULL];
            }
            
        }
        else
        {
            
            [GlobalFunction removeIndicatorView];
            
            
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"BUY TIMEE" message:@"Server is down, please try again later." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:@"Retry", nil];
            
            alertView.tag = 999;
        
            [alertView show];

            
            
            
         //   [[GlobalFunction shared] showAlertForMessage:@"Server is down, please try again later."];
            
        }
        

    
}

-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex{
    
    if (alertView.tag == 999) {
        
        if (buttonIndex == [alertView cancelButtonIndex])
        {
//            self.btnLoginWithGoogle.hidden = false;
//            self.btnLoginWithFacebook.hidden = false;
//            self.lblLineOne.hidden = false;
//            self.lblLineTwo.hidden = false;
//            self.lblOr.hidden = false;
//            self.btnSkip.hidden = false;
//            self.btnTermsOfService.hidden = false;
//            self.lblLineUnderSkip.hidden = false;
//            self.lblLineUnderTnC.hidden = false;
            
            [myAppDelegate.vcObj logOutProcess];
            
            
            [UIView transitionWithView:self.overlaySplashView
                              duration:0.4
                               options:UIViewAnimationOptionTransitionCrossDissolve
                            animations:^{
                                self.overlaySplashView.hidden = YES;
                            }
                            completion:NULL];

            
        }else
        {
            [self performSelector:@selector(finallyAddIndicator) withObject:nil afterDelay:0.0];
            
            [self performSelector:@selector(completeLoginCheck) withObject:nil afterDelay:0.4];
           
        }
        
    }
}



-(void)finishLoading :(NSDictionary *)data
{
    
    
    
}

-(void)handleCaseForUpdatePassword{
    
    [GlobalFunction removeIndicatorView];

    NSMutableArray *arr = [DatabaseClass getDataFromUserData:[[NSString stringWithFormat:@"Select * from %@",tableUser] UTF8String]];
    
    myAppDelegate.userData = (user *)[arr firstObject];

    [self resignTextFields];
    
    myAppDelegate.resetPassVCObj = nil;
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    myAppDelegate.resetPassVCObj = (ResetPasswordViewController *)[storyboard instantiateViewControllerWithIdentifier:@"ResetPasswordViewController"];
    
    if (myAppDelegate.resetPassVCObj)
    {
        [self presentViewController:myAppDelegate.resetPassVCObj animated:YES completion:nil];
    }

}


-(void)loginComplete{
    
    NSString* final = @"Login SuccessFully";
    
    [Flurry logEvent:final];
    
    NSMutableArray *arr = [DatabaseClass getDataFromUserData:[[NSString stringWithFormat:@"Select * from %@",tableUser] UTF8String]];
    
    myAppDelegate.userData = (user *)[arr firstObject];

    [myAppDelegate.vcObj.tblViewSideBar layoutIfNeeded];
    [myAppDelegate.vcObj.tblViewSideBar reloadData];
    [myAppDelegate.vcObj.tblViewSideBar layoutIfNeeded];

    
    

    
    
    
    
    
    if (myAppDelegate.isFromSkip == NO) {
        
        NSDictionary *dictUserData = [NSDictionary dictionaryWithObjects:@[[EncryptDecryptFile EncryptIT:socialUsername],[EncryptDecryptFile EncryptIT:self.textFieldPassword.text],myAppDelegate.userData.userid,[EncryptDecryptFile EncryptIT:[NSString stringWithFormat:@"%ld",(long)socialFlag]],[EncryptDecryptFile EncryptIT:socialEmail],[EncryptDecryptFile EncryptIT:socialId]] forKeys:@[keyUsername,keyPass,keyUserId,keysocialFlag_fb,keysocialEmailId_fb,keysocialId_fb]];
        
        [UserDefaults setObject:dictUserData forKey:keyUserloginDetailDictionary];

        
    }
    
    
    [UserDefaults setBool:YES forKey:keyIsLogin];
    
    [UserDefaults synchronize];
    
    myAppDelegate.arrayTaskDetail = [DatabaseClass getDataFromTaskData:[[NSString stringWithFormat:@"Select * from %@",tableTaskDetail] UTF8String]];
    
    [myAppDelegate.arrayTaskDetail sortUsingDescriptors:
     @[
       [NSSortDescriptor sortDescriptorWithKey:@"taskid_Integer" ascending:NO],
       ]];
    
    myAppDelegate.arrayAcceptedTaskDetail = [DatabaseClass getDataFromAcceptedTaskDetail:[[NSString stringWithFormat:@"Select * from %@",tableAcceptedTask] UTF8String]];
    
    NSMutableArray *catArr = [DatabaseClass getDataFromCategoryData:[[NSString stringWithFormat:@"Select * from %@",tableCategory] UTF8String]];
    
    NSMutableArray *subCatArr = [DatabaseClass getDataFromSubCategoryData:[[NSString stringWithFormat:@"Select * from %@",tableSubCategory] UTF8String]];
    
    NSMutableArray *BusinessArr = [DatabaseClass getDataFromBusinessData:[[NSString stringWithFormat:@"Select * from %@",tableBusinessInfo] UTF8String]];
    
    myAppDelegate.arrayBusinessDetail = [[NSMutableArray alloc]init];
    
    for (BusinessInfo *businessObjs in BusinessArr) {
        
        if (![myAppDelegate.arrayBusinessDetail containsObject:businessObjs]) {
            [myAppDelegate.arrayBusinessDetail addObject:businessObjs];
        }
    }
    
NSLog(@"array count is...%lu",(unsigned long)myAppDelegate.arrayBusinessDetail.count);
    
    myAppDelegate.arrayCategoryDetail = [[NSMutableArray alloc] init];
    
    for (category *catObjs in catArr) {
        
        if (![myAppDelegate.arrayCategoryDetail containsObject:catObjs]) {
            [myAppDelegate.arrayCategoryDetail addObject:catObjs];
        }
        
        for (subCategory *objs in subCatArr) {
            
            if ([objs.categoryId isEqualToString:catObjs.categoryId]) {
                
                if (![myAppDelegate.arrayCategoryDetail containsObject:objs]) {
                    
                    [myAppDelegate.arrayCategoryDetail addObject:objs];
                    
                }
                
            }
            
        }
        //keyUserType
    }
    
    if (myAppDelegate.isFromSkip) {
        
//        myAppDelegate.isFromSkip = YES;
//        
//        myAppDelegate.stringNameOfChildVC = @"login_BTimee";
        
        
        myAppDelegate.isSideBarAccesible = true;
        myAppDelegate.vcObj.btnSideBarOpen.hidden = NO;
        myAppDelegate.vcObj.btnBack.hidden = YES;
        TaskTableViewCell *cell = (TaskTableViewCell *)[myAppDelegate.vcObj.tblViewSideBar cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
        
    //   [myAppDelegate.viewAllTaskVCObj completeAfterLoginWork];
        
        
        CATransition *transitionAnimation = [CATransition animation];
        [transitionAnimation setType:kCATransitionPush];
        [transitionAnimation setSubtype:kCATransitionFromRight];
        [transitionAnimation setDuration:0.3f];
        [transitionAnimation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
        [transitionAnimation setFillMode:kCAFillModeBoth];
        
        [myAppDelegate.vcObj.viewLoginContainer.layer addAnimation:transitionAnimation forKey:@"changeView"];
        
        for(UIView *view in myAppDelegate.vcObj.viewLoginContainer.subviews){
            
            [view removeFromSuperview];
            
        }
        for(UIView *view in myAppDelegate.vcObj.viewHolderWithHeader.subviews){
            
            [view removeFromSuperview];
            
        }
        
       [myAppDelegate.vcObj performSegueWithIdentifier:@"viewAllTask" sender:cell];
        [myAppDelegate.vcObj.viewLoginContainer setHidden:YES];
        
        myAppDelegate.loginVCObj = nil;

    }
    else
    {
    NSString*uType =[UserDefaults valueForKey:keyUserType];
    
   // NSString*uType = [dics11 valueForKey:@"userType"];
    
    
    if ([uType isEqualToString:@"0"])
    {
        
        NSLog(@"**** %@",myAppDelegate.userData.mobile);
        NSLog(@"**** %@",myAppDelegate.userData.tnc);

        
        
        if(myAppDelegate.userData.mobile.length>1 && [myAppDelegate.userData.tnc isEqualToString:@"1"])
        {
            NSLog(@"filled");
            myAppDelegate.isSideBarAccesible = true;
            myAppDelegate.vcObj.btnSideBarOpen.hidden = NO;
            myAppDelegate.vcObj.btnBack.hidden = YES;
            TaskTableViewCell *cell = (TaskTableViewCell *)[myAppDelegate.vcObj.tblViewSideBar cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
            [myAppDelegate.vcObj performSegueWithIdentifier:@"viewAllTask" sender:cell];
            [myAppDelegate.viewAllTaskVCObj completeAfterLoginWork];
        }
        
        else
        {
            NSLog(@"**** %@",myAppDelegate.userData.mobile);
            NSLog(@"**** %@",myAppDelegate.userData.tnc);
            
            [GlobalFunction removeIndicatorView];
            myAppDelegate.TandC_Buyee=@"home";
            myAppDelegate.isSideBarAccesible = true;
            [myAppDelegate.vcObj performSegueWithIdentifier:@"createBio" sender:myAppDelegate.vcObj.btnCreateBioSegue];
            myAppDelegate.vcObj.btnBack.hidden = YES;
            myAppDelegate.vcObj.btnSideBarOpen.hidden = NO;
        }

    }
    else{
       // myAppDelegate.isFromTnC = true;
        
        
        NSLog(@"**** %@",myAppDelegate.userData.mobile);
        NSLog(@"**** %@",myAppDelegate.userData.tnc);

            [GlobalFunction removeIndicatorView];
            myAppDelegate.TandC_Buyee=@"home";
            myAppDelegate.isSideBarAccesible = true;
            
            [myAppDelegate.vcObj performSegueWithIdentifier:@"createBio" sender:myAppDelegate.vcObj.btnCreateBioSegue];
            
            myAppDelegate.vcObj.btnBack.hidden = YES;
            myAppDelegate.vcObj.btnSideBarOpen.hidden = NO;

        
    }
   
//    if ([UserDefaults boolForKey:keyAgreeTnC]) {
//        
//        if (![UserDefaults boolForKey:keyBioFilled]) {
//            
//            myAppDelegate.isFromTnC = true;
//            
//            myAppDelegate.isSideBarAccesible = false;
//            
//            [myAppDelegate.vcObj performSegueWithIdentifier:@"createBio" sender:myAppDelegate.vcObj.btnCreateBioSegue];
//            
//            myAppDelegate.vcObj.btnBack.hidden = YES;
//            myAppDelegate.vcObj.btnSideBarOpen.hidden = YES;
//
//        }else{
//            
//            myAppDelegate.isSideBarAccesible = true;
//            
//            TaskTableViewCell *cell = (TaskTableViewCell *)[myAppDelegate.vcObj.tblViewSideBar cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
//            [myAppDelegate.vcObj performSegueWithIdentifier:@"viewAllTask" sender:cell];
//            
//            [myAppDelegate.viewAllTaskVCObj completeAfterLoginWork];
//
//        }
//
//    }else{
//        
////        myAppDelegate.isSideBarAccesible = false;
////        
////        TaskTableViewCell *cell = (TaskTableViewCell *)[myAppDelegate.vcObj.tblViewSideBar cellForRowAtIndexPath:[NSIndexPath indexPathForRow:5 inSection:0]];
////        [myAppDelegate.vcObj performSegueWithIdentifier:@"termsNCondition" sender:cell];
////        
//    }
    
    CATransition *transitionAnimation = [CATransition animation];
    [transitionAnimation setType:kCATransitionPush];
    [transitionAnimation setSubtype:kCATransitionFromRight];
    [transitionAnimation setDuration:0.3f];
    [transitionAnimation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    [transitionAnimation setFillMode:kCAFillModeBoth];
    
    [myAppDelegate.vcObj.viewLoginContainer.layer addAnimation:transitionAnimation forKey:@"changeView"];
    
    for(UIView *view in myAppDelegate.vcObj.viewLoginContainer.subviews){
        
        [view removeFromSuperview];
        
    }
    [myAppDelegate.vcObj.viewLoginContainer setHidden:YES];
    
    myAppDelegate.loginVCObj = nil;
    }
}

-(void) skipClicked:(id)sender
{
    
////
////    [self completeLoginCheck];
////    
////    return;
////
    [self finallyAddIndicator];
    myAppDelegate.isFromSkip = YES;
//    
    myAppDelegate.stringNameOfChildVC = @"login_BTimee";
    
  //  [myAppDelegate.vcObj logOutProcess];
    
    
    socialFlag = 0;
    socialEmail = @"";
    socialId = @"";
    fbId = @"";
    socialUsername = @"";
    googleId = @"";
    picPath = @"";
    description = @"";
    fname = @"";
    lname = @"";
//
//   // myAppDelegate.superViewClassName
//    
//    myAppDelegate.isSideBarAccesible = true;// or true
//    
//    TaskTableViewCell *cell = (TaskTableViewCell *)[myAppDelegate.vcObj.tblViewSideBar cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
//    
//    
//    
//    CATransition *transitionAnimation = [CATransition animation];
//    [transitionAnimation setType:kCATransitionPush];
//    [transitionAnimation setSubtype:kCATransitionFromRight];
//    [transitionAnimation setDuration:0.4f];
//    [transitionAnimation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
//    [transitionAnimation setFillMode:kCAFillModeBoth];
//    
//    [myAppDelegate.vcObj.viewLoginContainer.layer addAnimation:transitionAnimation forKey:@"changeView"];
//    
//    for(UIView *view in myAppDelegate.vcObj.viewLoginContainer.subviews){
//        
//        [view removeFromSuperview];
//        
//    }
//    
//    for(UIView *view in myAppDelegate.vcObj.viewHolderWithHeader.subviews){
//        
//        [view removeFromSuperview];
//        
//    }
//    [myAppDelegate.vcObj performSegueWithIdentifier:@"viewAllTask" sender:cell];
//    
//    [myAppDelegate.vcObj.viewLoginContainer setHidden:YES];
//    
//    myAppDelegate.loginVCObj = nil;
    
    [self performSelector:@selector(completeLoginCheck) withObject:nil afterDelay:0.1];

}




-(void)termsClicked:(id)sender
{
     myAppDelegate.isFromSkip = NO;
    
    myAppDelegate.stringNameOfChildVC = @"T&C_BTimee";
    
    myAppDelegate.isSideBarAccesible = false;
    
    TaskTableViewCell *cell = (TaskTableViewCell *)[myAppDelegate.vcObj.tblViewSideBar cellForRowAtIndexPath:[NSIndexPath indexPathForRow:5 inSection:0]];
    
    CATransition *transitionAnimation = [CATransition animation];
    [transitionAnimation setType:kCATransitionPush];
    [transitionAnimation setSubtype:kCATransitionFromRight];
    [transitionAnimation setDuration:0.3f];
    [transitionAnimation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    [transitionAnimation setFillMode:kCAFillModeBoth];
    
    [myAppDelegate.vcObj.viewLoginContainer.layer addAnimation:transitionAnimation forKey:@"changeView"];
    
    for(UIView *view in myAppDelegate.vcObj.viewLoginContainer.subviews){
        
        [view removeFromSuperview];
        
    }
    
    for(UIView *view in myAppDelegate.vcObj.viewHolderWithHeader.subviews){
        
        [view removeFromSuperview];
        
    }

    [myAppDelegate.vcObj performSegueWithIdentifier:@"termsNCondition" sender:cell];
    [myAppDelegate.vcObj.viewLoginContainer setHidden:YES];
    
    myAppDelegate.loginVCObj = nil;
    
    

}

#pragma mark - create accessory view
-(void)createInputAccessoryView{
    // Create the view that will play the part of the input accessory view.
    // Note that the frame width (third value in the CGRectMake method)
    // should change accordingly in landscape orientation. But we don’t care
    // about that now.
    inputAccView = [[UIView alloc] initWithFrame:CGRectMake(10.0, 0.0, self.view.frame.size.width, 40.0)];
    // Set the view’s background color. We’ ll set it here to gray. Use any color you want.
    [inputAccView setBackgroundColor:[UIColor lightGrayColor]];
    // We can play a little with transparency as well using the Alpha property. Normally
    // you can leave it unchanged.
    [inputAccView setAlpha: 0.8];
    // If you want you may set or change more properties (ex. Font, background image,e.t.c.).
    // For now, what we’ ve already done is just enough.
    // Let’s create our buttons now. First the previous button.
    btnPrev = [UIButton buttonWithType: UIButtonTypeCustom];
    // Set the button’ s frame. We will set their widths to 80px and height to 40px.
    [btnPrev setFrame: CGRectMake(0.0, 0.0, 80.0, 40.0)];
    // Title.
    [btnPrev setTitle: @"Previous" forState: UIControlStateNormal];
    // Background color.
    [btnPrev setBackgroundColor: [UIColor clearColor]];
    // You can set more properties if you need to.
    // With the following command we’ ll make the button to react in finger tapping. Note that the
    // gotoPrevTextfield method that is referred to the @selector is not yet created. We’ ll create it
    // (as well as the methods for the rest of our buttons) later.
    [btnPrev addTarget: self action: @selector(gotoPrevTextfield) forControlEvents: UIControlEventTouchUpInside];
    // Do the same for the two buttons left.
    btnNext = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnNext setFrame:CGRectMake(85.0f, 0.0f, 80.0f, 40.0f)];
    [btnNext setTitle:@"Next" forState:UIControlStateNormal];
    [btnNext setBackgroundColor:[UIColor clearColor]];
    [btnNext addTarget:self action:@selector(gotoNextTextfield) forControlEvents:UIControlEventTouchUpInside];
    btnDone = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnDone setFrame:CGRectMake(self.view.frame.size.width - 80.0f, 0.0f, 80.0f, 40.0f)];
    [btnDone setTitle:@"Done" forState:UIControlStateNormal];
    [btnDone setBackgroundColor:[UIColor clearColor]];
    [btnDone setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [btnDone addTarget:self action:@selector(doneTyping) forControlEvents:UIControlEventTouchUpInside];
    // Now that our buttons are ready we just have to add them to our view.
    [inputAccView addSubview:btnPrev];
    [inputAccView addSubview:btnNext];
    [inputAccView addSubview:btnDone];
}


-(void)gotoPrevTextfield{
    // If the active textfield is the first one, can't go to any previous
    // field so just return.
    if (txtActiveField == self.textFieldUsername) {
        
        [txtActiveField resignFirstResponder];
        
        if ([[self.textFieldPassword.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""] || [[self.textFieldUsername.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""]) {
            
            return;
            
        }
        
        [self loginClicked:self.btnLogin];

        return;
    }
    if (txtActiveField == self.textFieldPassword) {
        
        [self.textFieldPassword resignFirstResponder];
        [self.textFieldUsername becomeFirstResponder];
        
        return;
    }
    
}

-(void)gotoNextTextfield{
    // If the active textfield is the second one, there is no next so just return.
    if (txtActiveField == self.textFieldUsername) {
        
        [self.textFieldUsername resignFirstResponder];
        [self.textFieldPassword becomeFirstResponder];
        
        return;
    }
    if (txtActiveField == self.textFieldPassword) {
        
        [txtActiveField resignFirstResponder];
        
        if ([[self.textFieldPassword.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""] || [[self.textFieldUsername.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""]) {
            
            return;
            
        }
        
        [self loginClicked:self.btnLogin];

        return;
    }
    
}

-(void)doneTyping{
    // When the "done" button is tapped, the keyboard should go away.
    // That simply means that we just have to resign our first responder.
    [txtActiveField resignFirstResponder];
    
    if ([[self.textFieldPassword.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""] || [[self.textFieldUsername.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""]) {
        
        return;
        
    }
    
    [self loginClicked:self.btnLogin];
    
}

-(void)finallyAddIndicator{
    [GlobalFunction addIndicatorView];
}


#pragma mark - text field delegate
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    
    return YES;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    
    [self createInputAccessoryView];
    // Now add the view as an input accessory view to the selected textfield.
    [textField setInputAccessoryView:inputAccView];
    // Set the active field. We' ll need that if we want to move properly
    // between our textfields.
    txtActiveField = textField;
    
}

-(void)textFieldDidEndEditing:(UITextField *)textField{
    
    
}

-(void) updateConstraints
{
    [self.view layoutIfNeeded];
    _skipLblLineHeight.constant=1.5f;
    _termsLblLineHeight.constant=1.5f;

    if (IS_IPHONE_5)
    {
        self.consLblBuyTimeTop.constant = 107;
        self.consLblSignFacebookTop.constant = 102;
        
        self.consLineFirstLeading.constant = 41;
        self.consLineFirstTop.constant = 35;
         self.consLineSecondTop.constant = 35;
        self.consLblOrTop.constant = 19;
        self.consLineFirstWidth.constant = 104;
        self.consLineSecondWidth.constant = 104;
        self.consLblSignGoogleTop.constant = 30;
        self.consLineSecondLeading.constant = 173;
        self.consLblSkipTop.constant = 25;
        
        self.consBtnFBHeight.constant = 15;
        self.consBtnGoogleHeight.constant = 15;
        
        self.consBtnFBWidth.constant = 195;
        self.consBtnGoogleWidth.constant = 170;
        self.btnSkip.alpha = 0.7;
        
        
        self.skipLineLblLeft.constant=128;
        self.skipLineLblTop.constant=58;
        self.lblBuyTimee.font=[self.lblBuyTimee.font fontWithSize:23];
        
        self.termLineLblTop.constant=130;
        self.termLineLblLeft.constant=93;
        
        _skipLblLineHeight.constant=1.5f;
        _termsLblLineHeight.constant=1.5f;

    }
    
    if (IS_IPHONE_6P)
    {
        self.lblOr.font=[self.lblOr.font fontWithSize:23];
       // [self.btnLoginWithGoogle.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:21]];
       // [self.btnLoginWithFacebook.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:21]];
        [self.btnSkip.titleLabel setFont:[UIFont fontWithName:@"Lato-Light" size:25]];
        [self.btnTermsOfService.titleLabel setFont:[UIFont fontWithName:@"Lato-Light" size:15]];
        //Lato-Light
        self.consLblBuyTimeTop.constant = 144;
        self.consLblSignFacebookTop.constant = 141;
        
        self.consLineFirstLeading.constant = 53;
        self.consLineFirstTop.constant = 47;
        self.consLineSecondTop.constant = 47;
        self.consLblOrTop.constant = 30;
        self.consLineFirstWidth.constant = 136;
        self.consLineSecondWidth.constant = 136;
        self.consLblSignGoogleTop.constant = 38;
        self.consLineSecondLeading.constant = 225;
        self.consLblSkipTop.constant = 36;
        
        
        self.consBtnFBHeight.constant = 21;
        self.consBtnGoogleHeight.constant = 21;
        
        self.consBtnFBWidth.constant = 246;
        self.consBtnGoogleWidth.constant = 215;
        self.consBtnTermsBottomSpace.constant = 6;
        
    //*****
        
        self.skipLineLblLeft.constant=166;
        self.skipLineLblTop.constant=73;
        self.lblBuyTimee.font=[self.lblBuyTimee.font fontWithSize:31];
        self.termLineLblTop.constant=180;
        self.termLineLblLeft.constant=115;
        self.skipLineLblLineWidth.constant=42;
        self.termsLineLblWidth.constant=145;
        self.buyLblWidth.constant=230;
        
        
    }
    
    else if(IS_IPHONE_6)
    {
        _skipLblLineHeight.constant=1.5f;
        _termsLblLineHeight.constant=1.5f;

    }

    
    else if (IS_IPHONE_4_OR_LESS)
    {
        self.consLblBuyTimeTop.constant = 107;
        self.consLblSignFacebookTop.constant = 70;
        
        self.consLineFirstLeading.constant = 41;
        self.consLineFirstTop.constant = 35;
        self.consLineSecondTop.constant = 35;
        self.consLblOrTop.constant = 19;
        self.consLineFirstWidth.constant = 104;
        self.consLineSecondWidth.constant = 104;
        self.consLblSignGoogleTop.constant = 30;
        self.consLineSecondLeading.constant = 173;
        self.consLblSkipTop.constant = 25;
        
        self.consBtnFBHeight.constant = 15;
        self.consBtnGoogleHeight.constant = 15;
        
        self.consBtnFBWidth.constant = 195;
        self.consBtnGoogleWidth.constant = 170;
        self.btnSkip.alpha = 0.7;
        
        
        self.skipLineLblLeft.constant=128;
        self.skipLineLblTop.constant=58;
        self.lblBuyTimee.font=[self.lblBuyTimee.font fontWithSize:23];
        
        self.termLineLblTop.constant=73;
        self.termLineLblLeft.constant=93;
        
        _skipLblLineHeight.constant=1.5f;
        _termsLblLineHeight.constant=1.5f;

    }
    
    [self.view layoutIfNeeded];
}
@end
