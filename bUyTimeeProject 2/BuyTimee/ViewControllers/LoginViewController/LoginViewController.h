//
//  LoginViewController.h
//  BuyTimee
//
//  Created by User14 on 02/03/16.

//

#import <UIKit/UIKit.h>
//#import <GooglePlus/GooglePlus.h>
#import "Flurry.h"


@class GPPSignInButton;

@interface LoginViewController : UIViewController <UITextFieldDelegate>
{
     UIBackgroundTaskIdentifier *bgTask;
}
@property (weak, nonatomic) IBOutlet UIImageView *imgViewBackground;

@property (weak, nonatomic) IBOutlet UITextField *textFieldUsername;
@property (weak, nonatomic) IBOutlet UITextField *textFieldPassword;

@property (weak, nonatomic) IBOutlet UIButton *btnLogin;
@property (weak, nonatomic) IBOutlet UIButton *btnNotAMember;
@property (weak, nonatomic) IBOutlet UIButton *btnForgotPassword;
@property (weak, nonatomic) IBOutlet UIButton *btnLoginWithFacebook;
@property (weak, nonatomic) IBOutlet UIButton *btnLoginWithTwitter;

@property (weak, nonatomic) IBOutlet UIButton *btnLoginWithGoogle;

@property (weak, nonatomic) IBOutlet UIButton *btnSkip;
@property (weak, nonatomic) IBOutlet UIButton *btnTermsOfService;
- (IBAction)notAMemberYetClicked:(id)sender;

@property (retain, nonatomic) IBOutlet GPPSignInButton *signInButton;

- (IBAction)forgotPasswordClicked:(id)sender;

- (IBAction)loginWithFacebookClicked:(id)sender;

- (IBAction)loginWithTwitterClicked:(id)sender;

- (IBAction)loginClicked:(id)sender;

- (IBAction)skipClicked:(id)sender;

- (IBAction)termsClicked:(id)sender;
-(void)finishLoading :(NSDictionary *)data;


@property (nonatomic, retain) UITextField *txtActiveField;
@property (nonatomic, retain) UIView *inputAccView;
@property (nonatomic, retain) UIButton *btnDone;
@property (nonatomic, retain) UIButton *btnNext;
@property (nonatomic, retain) UIButton *btnPrev;

-(void)loginComplete;

// outlets

@property (weak, nonatomic) IBOutlet UILabel *lblBuyTimee;
@property (weak, nonatomic) IBOutlet UILabel *lblOr;
@property (weak, nonatomic) IBOutlet UILabel *lblLineTwo;

@property (weak, nonatomic) IBOutlet UILabel *lblLineOne;

// constraints by vs 27 jan

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consLblBuyTimeTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consLblSignFacebookTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consLblSignGoogleTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consLblSkipTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consLineFirstLeading;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consLineFirstTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consLblOrTop;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consLineSecondTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consLineSecondLeading;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consLineSecondWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consLineFirstWidth;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consBtnGoogleHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consBtnGoogleWidth;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consBtnFBHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consBtnFBWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consBtnTermsBottomSpace;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *skipLineLblLeft;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *skipLineLblTop;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *termLineLblTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *termLineLblLeft;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *skipLineLblLineWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *termsLineLblWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *buyLblWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *skipLblLineHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *termsLblLineHeight;

@property (weak, nonatomic) IBOutlet UILabel *lblLineUnderSkip;

@property (weak, nonatomic) IBOutlet UILabel *lblLineUnderTnC;


@property (weak, nonatomic) IBOutlet UIView *overlaySplashView;


@end
