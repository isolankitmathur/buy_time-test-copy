//
//  RegistrationViewController.m
//  HandApp
//
//  Created by  ~ on 26/02/16.
//  Copyright (c) 2016 . All rights reserved.
//

#import "RegistrationViewController.h"
#import "Constants.h"
#import "GlobalFunction.h"
#import "TaskTableViewCell.h"
#import "LoadingView.h"

@interface RegistrationViewController ()

- (IBAction)cancelBtnClicked:(id)sender;

@end

@implementation RegistrationViewController{
    
    LoadingView *loadingView;

}


@synthesize txtActiveField;
@synthesize inputAccView;
@synthesize btnDone;
@synthesize btnNext;
@synthesize btnPrev;

- (void)viewDidLoad {
    [super viewDidLoad];

    //set delegate
    self.textFieldEmail.delegate=self;
    self.textFieldPassword.delegate=self;
    self.textFieldRetypePassword.delegate=self;
    self.textFieldUsername.delegate=self;
    
    //set place holders
    UIFont *fnt = self.textFieldUsername.font;
    
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:@"Username*"
                                                                                         attributes:@{NSFontAttributeName: [fnt fontWithSize:20], NSForegroundColorAttributeName : [UIColor whiteColor]}];
    
    [attributedString setAttributes:@{NSFontAttributeName : [fnt fontWithSize:8], NSForegroundColorAttributeName : [UIColor whiteColor]
                                      , NSBaselineOffsetAttributeName : @0} range:NSMakeRange(8, 1)];

    
    self.textFieldUsername.attributedPlaceholder = attributedString;

    
    NSMutableAttributedString *attributedString2 = [[NSMutableAttributedString alloc] initWithString:@"Email*"
                                                                                         attributes:@{NSFontAttributeName: [fnt fontWithSize:20], NSForegroundColorAttributeName : [UIColor whiteColor]}];
    
    [attributedString2 setAttributes:@{NSFontAttributeName : [fnt fontWithSize:8], NSForegroundColorAttributeName : [UIColor whiteColor]
                                      , NSBaselineOffsetAttributeName : @0} range:NSMakeRange(5, 1)];
    
    self.textFieldEmail.attributedPlaceholder = attributedString2;


    NSMutableAttributedString *attributedString3 = [[NSMutableAttributedString alloc] initWithString:@"Password*"
                                                                                         attributes:@{NSFontAttributeName: [fnt fontWithSize:20], NSForegroundColorAttributeName : [UIColor whiteColor]}];
    
    [attributedString3 setAttributes:@{NSFontAttributeName : [fnt fontWithSize:8], NSForegroundColorAttributeName : [UIColor whiteColor]
                                      , NSBaselineOffsetAttributeName : @0} range:NSMakeRange(8, 1)];
    
    self.textFieldPassword.attributedPlaceholder = attributedString3;


    
    NSMutableAttributedString *attributedString4 = [[NSMutableAttributedString alloc] initWithString:@"Re-type Password*"
                                                                                         attributes:@{NSFontAttributeName: [fnt fontWithSize:20], NSForegroundColorAttributeName : [UIColor whiteColor]}];
    
    [attributedString4 setAttributes:@{NSFontAttributeName : [fnt fontWithSize:8], NSForegroundColorAttributeName : [UIColor whiteColor]
                                      , NSBaselineOffsetAttributeName : @0} range:NSMakeRange(16, 1)];

    
    self.textFieldRetypePassword.attributedPlaceholder = attributedString4;
    

    [self.view layoutIfNeeded];
        
    UIFont *font = self.textFieldEmail.font;
    
    
    if (IS_IPHONE_4_OR_LESS) {
        self.textFieldEmail.font = [font fontWithSize:14.0];
        self.textFieldPassword.font = [font fontWithSize:14.0];
        self.textFieldRetypePassword.font = [font fontWithSize:14.0];
        self.textFieldUsername.font = [font fontWithSize:14.0];
        
    }
    if (IS_IPHONE_5) {
        self.textFieldEmail.font = [font fontWithSize:15.0];
        self.textFieldPassword.font = [font fontWithSize:15.0];
        self.textFieldRetypePassword.font = [font fontWithSize:15.0];
        self.textFieldUsername.font = [font fontWithSize:15.0];
        
    }
    if (IS_IPHONE_6) {
        self.textFieldEmail.font = [font fontWithSize:16.0];
        self.textFieldPassword.font = [font fontWithSize:16.0];
        self.textFieldRetypePassword.font = [font fontWithSize:16.0];
        self.textFieldUsername.font = [font fontWithSize:16.0];
        
    }
    if (IS_IPHONE_6P) {
        self.textFieldEmail.font = [font fontWithSize:17.0];
        self.textFieldPassword.font = [font fontWithSize:17.0];
        self.textFieldRetypePassword.font = [font fontWithSize:17.0];
        self.textFieldUsername.font = [font fontWithSize:17.0];
        
    }

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - create accessory view
-(void)createInputAccessoryView{
    // Create the view that will play the part of the input accessory view.
    // Note that the frame width (third value in the CGRectMake method)
    // should change accordingly in landscape orientation. But we don’t care
    // about that now.
    inputAccView = [[UIView alloc] initWithFrame:CGRectMake(10.0, 0.0, self.view.frame.size.width, 40.0)];
    // Set the view’s background color. We’ ll set it here to gray. Use any color you want.
    [inputAccView setBackgroundColor:[UIColor lightGrayColor]];
    // We can play a little with transparency as well using the Alpha property. Normally
    // you can leave it unchanged.
    [inputAccView setAlpha: 0.8];
    // If you want you may set or change more properties (ex. Font, background image,e.t.c.).
    // For now, what we’ ve already done is just enough.
    // Let’s create our buttons now. First the previous button.
    btnPrev = [UIButton buttonWithType: UIButtonTypeCustom];
    // Set the button’ s frame. We will set their widths to 80px and height to 40px.
    [btnPrev setFrame: CGRectMake(0.0, 0.0, 80.0, 40.0)];
    // Title.
    [btnPrev setTitle: @"Previous" forState: UIControlStateNormal];
    // Background color.
    [btnPrev setBackgroundColor: [UIColor clearColor]];
    // You can set more properties if you need to.
    // With the following command we’ ll make the button to react in finger tapping. Note that the
    // gotoPrevTextfield method that is referred to the @selector is not yet created. We’ ll create it
    // (as well as the methods for the rest of our buttons) later.
    [btnPrev addTarget: self action: @selector(gotoPrevTextfield) forControlEvents: UIControlEventTouchUpInside];
    // Do the same for the two buttons left.
    btnNext = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnNext setFrame:CGRectMake(85.0f, 0.0f, 80.0f, 40.0f)];
    [btnNext setTitle:@"Next" forState:UIControlStateNormal];
    [btnNext setBackgroundColor:[UIColor clearColor]];
    [btnNext addTarget:self action:@selector(gotoNextTextfield) forControlEvents:UIControlEventTouchUpInside];
    btnDone = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnDone setFrame:CGRectMake(self.view.frame.size.width - 80.0f, 0.0f, 80.0f, 40.0f)];
    [btnDone setTitle:@"Done" forState:UIControlStateNormal];
    [btnDone setBackgroundColor:[UIColor clearColor]];
    [btnDone setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [btnDone addTarget:self action:@selector(doneTyping) forControlEvents:UIControlEventTouchUpInside];
    // Now that our buttons are ready we just have to add them to our view.
    [inputAccView addSubview:btnPrev];
    [inputAccView addSubview:btnNext];
    [inputAccView addSubview:btnDone];
}


-(void)gotoPrevTextfield{
    // If the active textfield is the first one, can't go to any previous
    // field so just return.
    
    if([self.textFieldRetypePassword isFirstResponder]){
        [self.textFieldRetypePassword resignFirstResponder];
        [self.textFieldPassword becomeFirstResponder];
    }
    else if([self.textFieldPassword isFirstResponder]){
        [self.textFieldPassword resignFirstResponder];
        [self.textFieldEmail becomeFirstResponder];
    }
    else if([self.textFieldEmail isFirstResponder]){
        [self.textFieldEmail resignFirstResponder];
        [self.textFieldUsername becomeFirstResponder];
    }
    else if([self.textFieldUsername isFirstResponder]){
        [self.textFieldUsername resignFirstResponder];
    }
    
}

-(void)gotoNextTextfield{
    // If the active textfield is the second one, there is no next so just return.
    if([self.textFieldUsername isFirstResponder]){
        [self.textFieldUsername resignFirstResponder];
        [self.textFieldEmail becomeFirstResponder];
    }
    else  if([self.textFieldEmail isFirstResponder]){
        [self.textFieldEmail resignFirstResponder];
        [self.textFieldPassword becomeFirstResponder];
    }
    else  if([self.textFieldPassword isFirstResponder]){
        [self.textFieldPassword resignFirstResponder];
        [self.textFieldRetypePassword becomeFirstResponder];
    }
    else  if([self.textFieldRetypePassword isFirstResponder]){
        [self.textFieldRetypePassword resignFirstResponder];
    }

}

-(void)doneTyping{
    // When the "done" button is tapped, the keyboard should go away.
    // That simply means that we just have to resign our first responder.
    [txtActiveField resignFirstResponder];
}


#pragma mark - text field delegate
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    
    return YES;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    
    [self createInputAccessoryView];
    // Now add the view as an input accessory view to the selected textfield.
    [textField setInputAccessoryView:inputAccView];
    // Set the active field. We' ll need that if we want to move properly
    // between our textfields.
    txtActiveField = textField;
    
    if(IS_IPHONE_4_OR_LESS || IS_IPHONE_5){
        
        if (self.txtActiveField==_textFieldUsername) {
            CGRect frame = self.contentView.frame;
            frame.origin.y = 0;
            
            [UIView animateWithDuration:0.3 animations:^{
                self.contentView.frame = frame;
            } completion:^(BOOL finished) {
                
            }];
            
        }
        if(textField==_textFieldEmail){
            
            CGRect frame = self.contentView.frame;
            frame.origin.y =-45;
            
            [UIView animateWithDuration:0.3 animations:^{
                self.contentView.frame = frame;
            } completion:^(BOOL finished) {
                
            }];
            
        }
        if(textField==_textFieldPassword){
            
            CGRect frame = self.contentView.frame;
            frame.origin.y =-100;
            
            [UIView animateWithDuration:0.3 animations:^{
                self.contentView.frame = frame;
            } completion:^(BOOL finished) {
                
            }];
            
        }
        if(textField==_textFieldRetypePassword){
            
            CGRect frame = self.contentView.frame;
            frame.origin.y =-120;
            
            [UIView animateWithDuration:0.3 animations:^{
                self.contentView.frame = frame;
            } completion:^(BOOL finished) {
                
            }];
            
        }
        
    }
    
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardWillHideNotification object:nil];
    
    [self.view endEditing:YES];
    return YES;
}



-(void)keyboardDidHide:(NSNotification *)notification
{
    
    CGRect frame = self.contentView.frame;
    frame.origin.y =0;
    
    [UIView animateWithDuration:0.10 animations:^{
        self.contentView.frame = frame;
    } completion:^(BOOL finished) {
        [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    }];
}

-(void)textFieldDidEndEditing:(UITextField *)textField{
    
    
}


- (IBAction)signUpButtonClicked:(id)sender {
    
    NSString *messageString = @"";
    
    if ([[self.textFieldUsername.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""]) {
        
        messageString = @"Username cannot be blank.";
        
        [[GlobalFunction shared] showAlertForMessage:messageString];

        return;
        
    }
    if ([[self.textFieldEmail.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""]) {
     
        messageString = @"Email cannot be blank.";
        
        [[GlobalFunction shared] showAlertForMessage:messageString];
        
        return;
        
    }
    if (![GlobalFunction validateEmail:self.textFieldEmail.text]) {
        
        messageString = @"Please enter a valid Email to continue.";
        
        [[GlobalFunction shared] showAlertForMessage:messageString];

        return;
    }
    if ([[self.textFieldPassword.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""]) {
        
        messageString = @"Password cannot be blank.";
        
        [[GlobalFunction shared] showAlertForMessage:messageString];

        return;
    }
    if (![self.textFieldPassword.text isEqualToString:self.textFieldRetypePassword.text]) {
        
        messageString = @"Both password did not match.";
        
        [[GlobalFunction shared] showAlertForMessage:messageString];

        return;
        
    }
    
    [self performSelector:@selector(addIndicator) withObject:nil afterDelay:0.0];

    [self performSelector:@selector(performWorkForRegister) withObject:nil afterDelay:0.5];
        
}

-(void)addIndicator{
    
    [GlobalFunction addIndicatorView];
    
}


-(void)performWorkForRegister{
    
    if (![[WebService shared] connected]) {
        
        [GlobalFunction removeIndicatorView];
        
        [[GlobalFunction shared] showAlertForMessage:@"Can't connect to internet, please check and try again."];
        
        return;
    }
    
#if TARGET_OS_SIMULATOR
    
    //Simulator
    NSDictionary *dictReg = [webServicesShared regisusername:self.textFieldUsername.text emailId:self.textFieldEmail.text password:self.textFieldPassword.text token:@""];
    
#else
    
    // Device
    NSDictionary *dictReg = [webServicesShared regisusername:self.textFieldUsername.text emailId:self.textFieldEmail.text password:self.textFieldPassword.text token:myAppDelegate.deviceTokenString];
    
#endif


    
    if (dictReg != nil) {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"BUY TIMEE" message:[EncryptDecryptFile decrypt:[dictReg valueForKey:@"responseMessage"]] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];

        if ([[EncryptDecryptFile decrypt:[dictReg valueForKey:@"responseCode"]] isEqualToString:@"200"]) {
            
            [self performSelector:@selector(removeIndicator) withObject:nil afterDelay:0.1];
            
            alert.tag = 10001;

        }else{
            
            [self performSelector:@selector(removeIndicator) withObject:nil afterDelay:0.1];

        }
        
        [alert show];

    }else{
        
        [GlobalFunction removeIndicatorView];
        
        [[GlobalFunction shared] showAlertForMessage:@"Server is down, please try again later."];

    }

}

-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex{
    
    if (alertView.tag == 10001) {
        
        if(buttonIndex == [alertView cancelButtonIndex]){
            
            [self dismissViewControllerAnimated:YES completion:nil];
            
        }
    }
}

-(void)removeIndicator{
    
    [GlobalFunction removeIndicatorView];
    
}


- (IBAction)cancelBtnClicked:(id)sender {
    
    [txtActiveField resignFirstResponder];
    
    [self dismissViewControllerAnimated:YES completion:nil];

}


@end
