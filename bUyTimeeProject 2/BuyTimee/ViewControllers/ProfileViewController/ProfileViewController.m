//
//  ProfileViewController.m
//  BuyTimee
//
//  Created by User14 on 14/03/16.
//  Copyright © 2016 User14. All rights reserved.
//

#import "ProfileViewController.h"
#import "GlobalFunction.h"
#import "Constants.h"
#import "FeedbackTableViewCell.h"
#import "TaskTableViewCell.h"

@interface ProfileViewController ()

@property (weak, nonatomic) IBOutlet UIButton *btnViewAllTask;

- (IBAction)openLocation:(id)sender;
- (IBAction)openMail:(id)sender;
- (IBAction)openContact:(id)sender;

- (IBAction)animateImageViewToFullScreen:(id)sender;

@end

@implementation ProfileViewController{
    NSMutableArray *heightArr;
    NSInteger noOfRating;
    NSMutableArray *ratingArray;
    NSMutableArray *namesArray;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    heightArr = [[NSMutableArray alloc]init];
    ratingArray = [[NSMutableArray alloc]init];
    
    if (myAppDelegate.isComingFromPushNotification) {
        
        if ([myAppDelegate.userData.userid isEqualToString:myAppDelegate.workerPushTaskDetail.userId_createdBy]) {
            
            ratingArray = myAppDelegate.workerPushTaskDetail.arrayRatings_createdBy;
            noOfRating = myAppDelegate.workerPushTaskDetail.avgStars_createdBy;
            
        }else{
            
            ratingArray = myAppDelegate.workerPushTaskDetail.arrayRatings_acceptedBy;
            noOfRating = myAppDelegate.workerPushTaskDetail.avgStars_acceptedBy;
            
        }

        //COMMENTED BY MITESH ON DATED 21-MAY-2016
//        if ([myAppDelegate.userPushOtherData.userid isEqualToString:myAppDelegate.userData.userid]) {
//            
//            ratingArray = [DatabaseClass getDataFromRatingTable:[[NSString stringWithFormat:@"Select * from %@",tableRating] UTF8String]];
//            noOfRating = [myAppDelegate.userData.avg_rating integerValue];
//            
//        }
        
        self.lblBioDescription.text = myAppDelegate.userPushOtherData.userDescription;
        self.lblUserAddress.text = myAppDelegate.userPushOtherData.location;
        self.lblUsername.text = myAppDelegate.userPushOtherData.username;
        self.imgViewProfilePicture.image = myAppDelegate.userPushOtherData.userImage;
        
    }else{
        
        if (myAppDelegate.isOfOtherUser) {
            
            if ([myAppDelegate.comingFromClass isEqualToString:[NSString stringWithFormat:@"%@",[ViewAllTaskViewController class]]]) {
                
                if (!myAppDelegate.isFirstOtherUser) {
                    
                    if ([myAppDelegate.userOtherData.userid isEqualToString:myAppDelegate.workerAllTaskDetail.userId_acceptedBy]) {
                        
                        ratingArray = myAppDelegate.workerAllTaskDetail.arrayRatings_acceptedBy;
                        noOfRating = myAppDelegate.workerAllTaskDetail.avgStars_acceptedBy;
                        myAppDelegate.isFirstOtherUser = true;
                        
                    }else{
                        
                        ratingArray = myAppDelegate.workerAllTaskDetail.arrayRatings_createdBy;
                        noOfRating = myAppDelegate.workerAllTaskDetail.avgStars_createdBy;
                        myAppDelegate.isFirstOtherUser = true;
                        
                    }
                    
                }else{
                    
                    if ([myAppDelegate.userOtherData.userid isEqualToString:myAppDelegate.workerOtherUserTaskDetail.userId_acceptedBy]) {
                        
                        ratingArray = myAppDelegate.workerOtherUserTaskDetail.arrayRatings_acceptedBy;
                        noOfRating = myAppDelegate.workerOtherUserTaskDetail.avgStars_acceptedBy;
                        
                    }else{
                        
                        ratingArray = myAppDelegate.workerOtherUserTaskDetail.arrayRatings_createdBy;
                        noOfRating = myAppDelegate.workerOtherUserTaskDetail.avgStars_createdBy;
                        
                    }
                    
                }
                
                
            }else if ([myAppDelegate.comingFromClass isEqualToString:[NSString stringWithFormat:@"%@",[TaskHistoryViewController class]]]){
                
                if (!myAppDelegate.isFirstOtherUser) {
                    
                    if ([myAppDelegate.userOtherData.userid isEqualToString:myAppDelegate.workerTaskDetail.userId_acceptedBy]) {
                        
                        ratingArray = myAppDelegate.workerTaskDetail.arrayRatings_acceptedBy;
                        noOfRating = myAppDelegate.workerTaskDetail.avgStars_acceptedBy;
                        myAppDelegate.isFirstOtherUser = true;
                        
                    }else{
                        
                        ratingArray = myAppDelegate.workerTaskDetail.arrayRatings_createdBy;
                        noOfRating = myAppDelegate.workerTaskDetail.avgStars_createdBy;
                        myAppDelegate.isFirstOtherUser = true;
                        
                    }
                    
                }else{
                    
                    if ([myAppDelegate.userOtherData.userid isEqualToString:myAppDelegate.workerOtherUserTaskDetail.userId_acceptedBy]) {
                        
                        ratingArray = myAppDelegate.workerOtherUserTaskDetail.arrayRatings_acceptedBy;
                        noOfRating = myAppDelegate.workerOtherUserTaskDetail.avgStars_acceptedBy;
                        
                    }else{
                        
                        ratingArray = myAppDelegate.workerOtherUserTaskDetail.arrayRatings_createdBy;
                        noOfRating = myAppDelegate.workerOtherUserTaskDetail.avgStars_createdBy;
                        
                    }
                    
                }
                
            }else if ([myAppDelegate.comingFromClass isEqualToString:[NSString stringWithFormat:@"%@",[ActiveTaskViewController class]]]){
                
                if (!myAppDelegate.isFirstOtherUser) {
                    
                    if ([myAppDelegate.userOtherData.userid isEqualToString:myAppDelegate.workerTaskDetail.userId_createdBy]) {
                        
                        ratingArray = myAppDelegate.workerTaskDetail.arrayRatings_createdBy;
                        noOfRating = myAppDelegate.workerTaskDetail.avgStars_createdBy;
                        myAppDelegate.isFirstOtherUser = true;
                        
                    }else{
                        
                        ratingArray = myAppDelegate.workerTaskDetail.arrayRatings_acceptedBy;
                        noOfRating = myAppDelegate.workerTaskDetail.avgStars_acceptedBy;
                        myAppDelegate.isFirstOtherUser = true;
                        
                    }
                    
                }else{
                    
                    if ([myAppDelegate.userOtherData.userid isEqualToString:myAppDelegate.workerOtherUserTaskDetail.userId_createdBy]) {
                        
                        ratingArray = myAppDelegate.workerOtherUserTaskDetail.arrayRatings_createdBy;
                        noOfRating = myAppDelegate.workerOtherUserTaskDetail.avgStars_createdBy;
                        
                    }else{
                        
                        ratingArray = myAppDelegate.workerOtherUserTaskDetail.arrayRatings_acceptedBy;
                        noOfRating = myAppDelegate.workerOtherUserTaskDetail.avgStars_acceptedBy;
                        
                    }
                    
                }
                
            }else{
                
                if ([myAppDelegate.userData.userid isEqualToString:myAppDelegate.workerTaskDetail.userId_createdBy]) {
                    
                    ratingArray = myAppDelegate.workerTaskDetail.arrayRatings_createdBy;
                    noOfRating = myAppDelegate.workerTaskDetail.avgStars_createdBy;
                    
                }else{
                    
                    ratingArray = myAppDelegate.workerTaskDetail.arrayRatings_acceptedBy;
                    noOfRating = myAppDelegate.workerTaskDetail.avgStars_acceptedBy;
                    
                }
                
            }
            
             //COMMENTED BY MITESH ON DATED 21-MAY-2016
//            if ([myAppDelegate.userOtherData.userid isEqualToString:myAppDelegate.userData.userid]) {
//                
//                ratingArray = [DatabaseClass getDataFromRatingTable:[[NSString stringWithFormat:@"Select * from %@",tableRating] UTF8String]];
//                noOfRating = [myAppDelegate.userData.avg_rating integerValue];
//                
//            }
            
            self.lblBioDescription.text = myAppDelegate.userOtherData.userDescription;
            self.lblUserAddress.text = myAppDelegate.userOtherData.location;
            self.lblUsername.text = myAppDelegate.userOtherData.username;
            self.imgViewProfilePicture.image = myAppDelegate.userOtherData.userImage;
            
        }else{
            
            ratingArray = [DatabaseClass getDataFromRatingTable:[[NSString stringWithFormat:@"Select * from %@",tableRating] UTF8String]];
            
            self.lblBioDescription.text = myAppDelegate.userData.userDescription;
            self.lblUserAddress.text = myAppDelegate.userData.location;
            self.lblUsername.text = myAppDelegate.userData.username;
            self.imgViewProfilePicture.image = myAppDelegate.userData.userImage;
            
            noOfRating = [myAppDelegate.userData.avg_rating integerValue];
            
        }

    }
    
    if (self.imgViewProfilePicture.image) {
        
    }else{
        self.imgViewProfilePicture.image = imageUserDefault;
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidAppear:(BOOL)animated{
    
    myAppDelegate.vcObj.lblTitleText.text = @"Profile";

    [self.view layoutIfNeeded];
    
    self.imgViewProfilePicture.layer.cornerRadius = self.imgViewProfilePicture.frame.size.height/2;
    self.imgViewProfilePicture.layer.masksToBounds = YES;
    
    CGFloat totalHeight = 35*ratingArray.count;
    
    for (rating *obj in ratingArray) {
        
        UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.tblFeedback.frame.size.width, 40)];
        
        lbl.text = obj.ratingdescription;
        
        lbl.lineBreakMode = NSLineBreakByClipping;
        
        lbl.font = [UIFont systemFontOfSize:16.0];
        
        lbl.numberOfLines = 0;
        
        CGFloat height = [GlobalFunction heightForWidth:self.tblFeedback.frame.size.width usingFont:lbl.font forLabel:lbl];
     
        NSString *heightString = [NSString stringWithFormat:@"%f",height];
        
        [heightArr addObject:heightString];
        
        totalHeight = totalHeight + height;
        
    }
    
    NSInteger count = 1;
    
    for (UIImageView *imgViews in self.imgStars) {
        
        if (count<=noOfRating) {
            
            imgViews.image = [UIImage imageNamed:@"redstar"];
            
            count = count + 1;
            
        }else{
            
            imgViews.image = [UIImage imageNamed:@"greystar"];

            count = count + 1;
        }
        
    }
    
    [self.view layoutIfNeeded];
    
    self.btnViewAllTask.imageEdgeInsets = UIEdgeInsetsMake(0,(self.btnViewAllTask.frame.size.width - self.btnViewAllTask.imageView.image.size.width), 0, 0);

    if (ratingArray.count>0) {
        self.constraintTblFeedbackHeight.constant = totalHeight;
    }else{
        self.constraintTblFeedbackHeight.constant = 0.0;
    }
    
    CGFloat r = [GlobalFunction heightForWidth:SCREEN_WIDTH-40 usingFont:self.lblBioDescription.font forLabel:self.lblBioDescription];
    
    if (r<40) {
    
        r = 40;
        
    }
    
    self.constraintLblDescriptionHeight.constant = r;
    
    [self.view layoutIfNeeded];

    [self.tblFeedback layoutIfNeeded];
    [self.tblFeedback reloadData];
    [self.tblFeedback layoutIfNeeded];
    
    [GlobalFunction removeIndicatorView];
    
}


- (IBAction)openTaskHistoryView:(id)sender {
    
    if (myAppDelegate.isComingFromPushNotification) {
        
        [myAppDelegate.taskDetailPushVCObj performSegueWithIdentifier:@"otherUserTaskHistory" sender:myAppDelegate.taskDetailPushVCObj.btnForTaskHistorySegue];

    }else{
    
        myAppDelegate.isSideBarAccesible = false;

        if (myAppDelegate.isOfOtherUser) {
            
            [myAppDelegate.taskDetailVCObj performSegueWithIdentifier:@"otherUserTaskHistory" sender:myAppDelegate.taskDetailVCObj.btnForTaskHistorySegue];

        }else{
            
            myAppDelegate.isFromProfile = true;
            
            TaskTableViewCell *cell = (TaskTableViewCell *)[myAppDelegate.vcObj.tblViewSideBar cellForRowAtIndexPath:[NSIndexPath indexPathForRow:4 inSection:0]];
            [myAppDelegate.vcObj performSegueWithIdentifier:@"taskhistory" sender:cell];
            
        }

    }

}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return heightArr.count;
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    FeedbackTableViewCell *cell = (FeedbackTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"feedback"];
    
    rating *rateClass = ratingArray[indexPath.row];
    
    cell.lblFeedbackDescription.text = rateClass.ratingdescription;
    cell.lblFeedbackUsername.text = rateClass.userName;
    
    UIFontDescriptor * fontDLBLTime = [cell.lblFeedbackUsername.font.fontDescriptor
                                       fontDescriptorWithSymbolicTraits:UIFontDescriptorTraitBold];
    [cell.lblFeedbackUsername setFont:[UIFont fontWithDescriptor:fontDLBLTime size:0]];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (tableView == self.tblFeedback) {
        
        // Remove seperator inset
        if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
            [cell setSeparatorInset:UIEdgeInsetsZero];
        }
        
        // Prevent the cell from inheriting the Table View's margin settings
        if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
            [cell setPreservesSuperviewLayoutMargins:NO];
        }
        
        // Explictly set your cell's layout margins
        if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
            [cell setLayoutMargins:UIEdgeInsetsZero];
        }
        
    }
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return [[heightArr objectAtIndex:indexPath.row] floatValue] + 35;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

}


- (IBAction)openLocation:(id)sender {
    
    CLLocationCoordinate2D coordinate;

    if (myAppDelegate.isComingFromPushNotification) {
        
        if ([myAppDelegate.userPushOtherData.location isEqualToString:@""]) {
            
            [[GlobalFunction shared] showAlertForMessage:@"There is no location available for this user to show."];
            
            return;
        }
        
        coordinate = [[GlobalFunction shared] getLocationFromAddressString:myAppDelegate.userPushOtherData.location];
        
        myAppDelegate.mapViewFromClass = @"Profile";
        
        [myAppDelegate.vcObj showRegionOnMapForCoordinates:[NSString stringWithFormat:@"%f,%f",coordinate.latitude,coordinate.longitude]];

    }else{
    
        if (myAppDelegate.isOfOtherUser) {
            
            if ([myAppDelegate.userOtherData.location isEqualToString:@""]) {
                
                [[GlobalFunction shared] showAlertForMessage:@"There is no location available for this user to show."];
                
                return;
            }
            
            coordinate = [[GlobalFunction shared] getLocationFromAddressString:myAppDelegate.userOtherData.location];
            
        }else{
            
            if ([myAppDelegate.userData.location isEqualToString:@""]) {
                
                [[GlobalFunction shared] showAlertForMessage:@"There is no location available for this user to show."];
                
                return;
            }
            
            coordinate = [[GlobalFunction shared] getLocationFromAddressString:myAppDelegate.userData.location];
            
        }
        
        myAppDelegate.mapViewFromClass = @"Profile";
        
        [myAppDelegate.vcObj showRegionOnMapForCoordinates:[NSString stringWithFormat:@"%f,%f",coordinate.latitude,coordinate.longitude]];

    }
    
}

- (IBAction)openMail:(id)sender {
    
    if (myAppDelegate.isComingFromPushNotification) {
        
        if ([myAppDelegate.userPushOtherData.emailId isEqualToString:@""]) {
            
            [[GlobalFunction shared] showAlertForMessage:@"There is no contact email available for this user."];
            
            return;
            
        }
        
        [myAppDelegate.vcObj sendMailToEmail:myAppDelegate.userPushOtherData.emailId];

    }else{
    
        if (myAppDelegate.isOfOtherUser) {
            
            if ([myAppDelegate.userOtherData.emailId isEqualToString:@""]) {
                
                [[GlobalFunction shared] showAlertForMessage:@"There is no contact email available for this user."];
                
                return;
                
            }
            
            [myAppDelegate.vcObj sendMailToEmail:myAppDelegate.userOtherData.emailId];
            
        }else{
            
            if ([myAppDelegate.userData.emailId isEqualToString:@""]) {
                
                [[GlobalFunction shared] showAlertForMessage:@"There is no contact email available for this user."];
                
                return;
            }
            
            [myAppDelegate.vcObj sendMailToEmail:myAppDelegate.userData.emailId];
            
        }

    }
    
}

- (IBAction)openContact:(id)sender {
    
    if (myAppDelegate.isComingFromPushNotification) {
        
        if ([myAppDelegate.userPushOtherData.mobile isEqualToString:@""]) {
            
            [[GlobalFunction shared] showAlertForMessage:@"There is no contact number available for this user."];
            
            return;
        }
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel://%@",myAppDelegate.userPushOtherData.mobile]]];

    }else{
    
        if (myAppDelegate.isOfOtherUser) {
            
            if ([myAppDelegate.userOtherData.mobile isEqualToString:@""]) {
                
                [[GlobalFunction shared] showAlertForMessage:@"There is no contact number available for this user."];
                
                return;
            }
            
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel://%@",myAppDelegate.userOtherData.mobile]]];
            
        }else{
            
            if ([myAppDelegate.userData.mobile isEqualToString:@""]) {
                
                [[GlobalFunction shared] showAlertForMessage:@"There is no contact number available for this user."];
                
                return;
            }
            
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel://%@",myAppDelegate.userData.mobile]]];
            
        }

    }
    
}

- (IBAction)animateImageViewToFullScreen:(id)sender {
    
    [GlobalFunction animateImageview:self.imgViewProfilePicture];

}


@end
