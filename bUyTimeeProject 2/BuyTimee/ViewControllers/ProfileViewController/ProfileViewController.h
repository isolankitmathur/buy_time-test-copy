//
//  ProfileViewController.h
//  BuyTimee
//
//  Created by User14 on 14/03/16.
//  Copyright © 2016 User14. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProfileViewController : UIViewController <UITableViewDataSource,UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *imgViewProfilePicture;
@property (weak, nonatomic) IBOutlet UILabel *lblUsername;
@property (weak, nonatomic) IBOutlet UILabel *lblUserAddress;
@property (strong, nonatomic) IBOutletCollection(UIImageView) NSArray *imgStars;
@property (weak, nonatomic) IBOutlet UILabel *lblBioDescription;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintLblDescriptionHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintViewCenterHolderHeight;

- (IBAction)openTaskHistoryView:(id)sender;

@property (weak, nonatomic) IBOutlet UITableView *tblFeedback;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintTblFeedbackHeight;

@end
