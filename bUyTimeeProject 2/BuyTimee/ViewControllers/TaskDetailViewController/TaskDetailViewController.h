//
//  TaskDetailViewController.h
//  BuyTimee
//
//  Created by User14 on 08/03/16.

//

#import <UIKit/UIKit.h>
#import "Flurry.h"

#import "BraintreeCore.h"

@interface TaskDetailViewController : UIViewController<UIScrollViewDelegate,UIGestureRecognizerDelegate>


@property (nonatomic, strong) BTAPIClient *braintreeClient;



@property (weak, nonatomic) IBOutlet UIScrollView *scrollViewHolder;

@property (weak, nonatomic) IBOutlet UILabel *lblTaskCompleted;
@property (weak, nonatomic) IBOutlet UILabel *lblTaskName;
@property (weak, nonatomic) IBOutlet UILabel *lblTaskPrice;
@property (weak, nonatomic) IBOutlet UILabel *lblTaskDate;
@property (weak, nonatomic) IBOutlet UILabel *lblTaskDescription;
@property (weak, nonatomic) IBOutlet UILabel *lblCreditCardEndingIn;
@property (weak, nonatomic) IBOutlet UILabel *lblCreditCardLast4DigitHolder;
@property (weak, nonatomic) IBOutlet UILabel *lblPaymentSentDate;
@property (weak, nonatomic) IBOutlet UILabel *lblUserName;
@property (weak, nonatomic) IBOutlet UILabel *lblUserAddress;
@property (weak, nonatomic) IBOutlet UILabel *lblAddress2;

@property (weak, nonatomic) IBOutlet UIView *viewDoneBtnEmployerHolder;
@property (weak, nonatomic) IBOutlet UIView *viewDoneBtnWorkerHolder;
@property (weak, nonatomic) IBOutlet UIView *viewAcceptBtnWorkerHolder;
@property (weak, nonatomic) IBOutlet UIView *viewYesNoBtnEmployerHolder;
@property (weak, nonatomic) IBOutlet UIView *viewCompleteIncompleteBtnEmployerHolder;

@property (weak, nonatomic) IBOutlet UIView *viewUserInfoHolder;
@property (weak, nonatomic) IBOutlet UIView *viewUserAdditionalInformation;
@property (weak, nonatomic) IBOutlet UIView *viewRelistPopHolder;
@property (weak, nonatomic) IBOutlet UIView *viewRatingHolder;
@property (weak, nonatomic) IBOutlet UIView *viewLblTaskCompletedHolder;
@property (weak, nonatomic) IBOutlet UIView *viewPaymentDetailHolder;
@property (weak, nonatomic) IBOutlet UIView *viewTaskDetailHolder;
@property (weak, nonatomic) IBOutlet UIView *viewRatingViewControllerHolder;

//for view heirarchy maintain
@property (weak, nonatomic) IBOutlet UIView *viewOtherUserProfileHolder;

//@property (strong, nonatomic) UIView *viewHolderForOtherUserProfile;
//@property (weak, nonatomic) IBOutlet UIView *viewOtherUserTaskHistoryHolder;
//@property (weak, nonatomic) IBOutlet UIView *viewOtherUserTaskDetailHolder;

@property (weak, nonatomic) IBOutlet UIButton *btnForTaskHistorySegue;
@property (weak, nonatomic) IBOutlet UIButton *btnForTaskDetailSegue;

@property (weak, nonatomic) IBOutlet UIImageView *imgViewUserProfilePicture;

@property (strong, nonatomic) IBOutletCollection(NSLayoutConstraint) NSArray *constraintViewBtnsHeight;

//@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintTopHolderViewHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintBottomHolderViewHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintLblTaskCompletedHolderView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintViewTaskPaymentDetailHolderHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintViewRatingHolderHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintViewUserAdditionalInformationHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintLblTaskDescriptionHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintViewLblTaskDescriptionHolderHeight;

- (IBAction)showUserLocation:(id)sender;
- (IBAction)sendEmailToUser:(id)sender;
- (IBAction)contactUser:(id)sender;
- (IBAction)submitFinalRating:(id)sender;

//rating methods

@property (weak, nonatomic) IBOutlet UIButton *btnAddRating;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *btnStars;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *btnAdditionalFeatures;

//btn actions
- (IBAction)taskCompleteClicked:(id)sender;
- (IBAction)taskInCompleteClicked:(id)sender;
- (IBAction)doneClicked_Employer:(id)sender;
- (IBAction)doneClicked_Worker:(id)sender;
- (IBAction)acceptTaskClicked_Worker:(id)sender;
- (IBAction)relistYesClicked:(id)sender;
- (IBAction)relistNoClicked:(id)sender;


@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *collectionVieTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *collectionViewHeight;


//Comment by kp



@property (weak, nonatomic) IBOutlet UIImageView *sliderImageView;
- (IBAction)buyNowBtnClciked:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *buyNowBtn;
@property (weak, nonatomic) IBOutlet UIButton *flagBtn;
- (IBAction)flagBtnClciked:(id)sender;
@property(nonatomic,strong)UISwipeGestureRecognizer*leftSwipe;
@property(nonatomic,strong)UISwipeGestureRecognizer*rightSwipe;


@property (weak, nonatomic) IBOutlet UIImageView *circleImage1;
@property (weak, nonatomic) IBOutlet UIImageView *circleImage2;
@property (weak, nonatomic) IBOutlet UIImageView *circleImage3;
@property (weak, nonatomic) IBOutlet UIImageView *circleImage4;
@property (weak, nonatomic) IBOutlet UIImageView *circleImage5;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *circleImageLeft;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *circleImageTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *circleImageHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *circleImageWidth;

@property (weak, nonatomic) IBOutlet UIView *firstTimeMentionView;
@property (weak, nonatomic) IBOutlet UIView *secondAddressMentionView;
@property (weak, nonatomic) IBOutlet UIView *thirdPostedByView;

@property (weak, nonatomic) IBOutlet UILabel *timeViewHeadingLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeViewDateTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeViewPriceLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeViewDetailLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *timeViewDetailLabelHeight;

@property (weak, nonatomic) IBOutlet UILabel *addressViewHeadingLabel;
@property (weak, nonatomic) IBOutlet UILabel *addressViewAddressLabel;
@property (weak, nonatomic) IBOutlet UILabel *addressViewDetailLabel;
@property (weak, nonatomic) IBOutlet UILabel *addressViewURLlbl;

- (IBAction)rateBtnClciked:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *rateBtn;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *firstTimeMentionViewHeight;


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *secondAddressMentionViewHeight;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewTopHolderHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewTopHolderWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *sliderImageViewHeight;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *rateBtnWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *rateBtnHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *buyNowBtnHeight;
@property (weak, nonatomic) IBOutlet UILabel *postedByViewNameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *postedByViewUserImage;
@property (weak, nonatomic) IBOutlet UILabel *postedByHeadingLabel;


@property (weak, nonatomic) IBOutlet UIImageView *star1;
@property (weak, nonatomic) IBOutlet UIImageView *star2;
@property (weak, nonatomic) IBOutlet UIImageView *star3;
@property (weak, nonatomic) IBOutlet UIImageView *star4;
@property (weak, nonatomic) IBOutlet UIImageView *star5;

@property(nonatomic,strong) NSMutableArray *imagesArray;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *circleImagesViewLeft;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *circleImagesViewTop;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *scrollViewHeight;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *buyNowBtnBottom;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *flagMainBtnWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *flagMianBtnHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *flagBtnHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *flagBtnWidth;
@property (weak, nonatomic) IBOutlet UIButton *flagTransBtn;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *flagBtnRight;

- (IBAction)flagTransBtnPressed:(id)sender;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *activityIndicatorTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *activityIndicatorLeft;


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *firstViewHeadingLblLeft;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *firstViewHeadingLblTop;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *firstViewDateLblLeft;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *firstViewDateLblTop;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *firstViewPriceLblTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *firstViewPriceLblRight;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *firstViewDEscriptionLblLeft;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *firstViewDEscriptionLblTop;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *secondViewHeadingLblLeft;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *secondViewHeadingLblTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *secondViewAddressLblLeft;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *secondViewAddressLblTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *secondViewURLLblLeft;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *secondViewURLLblTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *secondViewDetailLblLeft;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *secondViewDetailLblTop;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *postedByTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *postedByLeft;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imgViewTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imageViewWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imageViewHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *flagBtnTop;
@property (weak, nonatomic) IBOutlet UILabel *lineOneLabel;
@property (weak, nonatomic) IBOutlet UILabel *lineTwoLabel;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *postedByNameLblLeft;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *postedByNameLblTop;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *starOneTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *starOneLeft;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imageViewLeft;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *rateBtnTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *rateBtnRight;

@property (weak, nonatomic) IBOutlet UIView *blurview;
@property (weak, nonatomic) IBOutlet UIWebView *webViewForURLData;
@property (weak, nonatomic) IBOutlet UIButton *crossBtn;
- (IBAction)crossBtnClciked:(id)sender;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consBlurViewHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consWebViewHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consCrossBtnRight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *crossBtnHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *crossBtnWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *crossBtnTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *webVieTop;


@property (weak, nonatomic) IBOutlet UILabel *firstViewNewPriceLbl;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *conNewPriceLblWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *conNewPriceLblHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *conNewPriceLblTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *conNewPriceLblRight;


@property (weak, nonatomic) IBOutlet UILabel *firstViewDiscountlLbl;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consDiscountPriceLblWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consDiscountPriceLblHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consDiscountPriceLblTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consDiscountPriceLblRight;


@property (weak, nonatomic) IBOutlet UILabel *firstViewDiscountDescriptionLbl;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *conDiscountDescriptionLblLeft;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *conDiscountDescriptionLblTop;

@property (weak, nonatomic) IBOutlet UILabel *crossLineLbl;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *crossLineLblTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *crossLineLblWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *crossLineLblRight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consFirstViewHeadingLblWidth;









@end
