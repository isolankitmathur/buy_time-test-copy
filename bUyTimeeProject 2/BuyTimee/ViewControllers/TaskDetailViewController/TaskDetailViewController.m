//
//  TaskDetailViewController.m
//  BuyTimee
//
//  Created by User14 on 08/03/16.

//

#import "TaskDetailViewController.h"
#import "Constants.h"
#import "GlobalFunction.h"
#import "RatingViewController.h"
#import "TaskTableViewCell.h"
#import "WebService.h"
#import "subCategory.h"
#import "TaskDetailCollectionViewCell.h"
#import <EventKit/EventKit.h>
#import <CoreText/CoreText.h>

#import "BraintreePayPal.h"
#import "PPDataCollector.h"

@interface TaskDetailViewController ()<BTAppSwitchDelegate, BTViewControllerPresentingDelegate,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,UIWebViewDelegate>

//@property (nonatomic, strong) BTAPIClient *braintreeClient;
@property (nonatomic, strong) BTPayPalDriver *payPalDriver;

@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *btnCollection;
@property (weak, nonatomic) IBOutlet UIView *viewBottomHolder;
@property (weak, nonatomic) IBOutlet UILabel *lblRelist;
- (IBAction)openUserProfile:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lblCenterHolder;

@property (weak, nonatomic) IBOutlet UIView *viewBgForFullScreenImage;
@property (weak, nonatomic) IBOutlet UIView *viewImageViewAndActivityHolder;
@property (weak, nonatomic) IBOutlet UIImageView *imgViewTaskImagesHolder;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicatorTaskDetailImages;

- (IBAction)hideFullScreenImageView:(id)sender;
- (IBAction)swipeToRight:(id)sender;
- (IBAction)swipeToLeft:(id)sender;
- (IBAction)hideFullScreenImage:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *btnCloseFullView;


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintLblDateTrailing;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintLblDateWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintLblPriceWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintLblTaskNameWidth;
- (IBAction)removeRelistPopup:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *btnLocation;
@property (weak, nonatomic) IBOutlet UIButton *btnMail;
@property (weak, nonatomic) IBOutlet UIButton *btnContact;



@end

@implementation TaskDetailViewController{
    
    NSInteger noOfRating;
    NSMutableArray *arraryOfPics;
    
    task *taskDataObj;
    
    NSString *workerOrEmployer;
    
    NSString *emailIdToSend;
    NSString *telToCall;
    NSString *locationToShow;
    
    BOOL isViewDidLoad;
    
    NSInteger indexCounter;
    
    NSInteger maxCounter;
    
    NSString *afterWorkString;
    NSDictionary *afterWorkDict;
    
    NSInteger indexForTableRow;
    CGPoint centerTogoBack;
    
    BOOL isNoThanksClicked;
    
    
    //comment by KP
    
    int count;
    
    NSMutableArray *sliderImagesArrayDownloadImages;
    
    
     BOOL ispurchaseSuccessFull;
      BOOL eventAddedSuccessFully;
    
    int discVal;
    
    NSString *deviceData;
    NSString *nonce;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _blurview.alpha=0.0;
    _firstViewNewPriceLbl.hidden=YES;
    _firstViewDiscountlLbl.hidden=YES;
    _crossLineLbl.hidden=YES;

    
    
    
    
    
    

    myAppDelegate.vcObj.btnMapOnListView.hidden = true;//vs
    myAppDelegate.vcObj.BtnTransparentMap.hidden = true;//vs
    //comment by KP
    
    myAppDelegate.TandC_Buyee=@"home"; //by KP
    myAppDelegate.TandC_Buyee=@""; //by vs
    count=0;
    
    sliderImagesArrayDownloadImages=[[NSMutableArray alloc]init];
    
  //  self.postedByViewUserImage.layer.cornerRadius = self.postedByViewUserImage.frame.size.height/2;
  //  self.postedByViewUserImage.layer.masksToBounds = YES;

    [_sliderImageView setUserInteractionEnabled:YES];
    self.leftSwipe=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeLeft:)];
    
    self.leftSwipe.direction=UISwipeGestureRecognizerDirectionLeft;
    [self.sliderImageView addGestureRecognizer:self.leftSwipe];
    
    self.rightSwipe=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(rightSwipe:)];
    
    self.rightSwipe.direction=UISwipeGestureRecognizerDirectionRight;
    [self.sliderImageView addGestureRecognizer:self.rightSwipe];
    
    
    // self.buyNowBtn.layer.zPosition = 1;
    
    //    NSString *s=@"kdfkdfjkdfjkjfkdfkdfjkdfjkjf address...... ";
    //
    //    CGSize maximumLabelSize = CGSizeMake(350,9999);
    //
    //    CGSize expectedLabelSize = [s sizeWithFont:_addressViewDetailLabel.font
    //                             constrainedToSize:maximumLabelSize
    //                                 lineBreakMode:_addressViewDetailLabel.lineBreakMode];
    //
    //    CGRect newFrame = _timeViewDetailLabel.frame;
    //    newFrame.size.height = expectedLabelSize.height;
    //
    //    //_firstTimeMentionViewHeight.constant=newFrame.size.height +100;
    //    _secondAddressMentionViewHeight.constant=newFrame.size.height +110;
    //    _addressViewDetailLabel.text=s;
    //
    //
    //
    //    NSString *s1=@"kdfkdfjkdfjkjfkdfkdfjkdfjkjf kdfkdfjkdfjkjf dfjkkfdjfk  time...... ";
    //
    //    CGSize maximumLabelSize1 = CGSizeMake(350,9999);
    //
    //    CGSize expectedLabelSize1 = [s1 sizeWithFont:_timeViewDetailLabel.font
    //                               constrainedToSize:maximumLabelSize1
    //                                   lineBreakMode:_timeViewDetailLabel.lineBreakMode];
    //
    //    CGRect newFrame1 = _timeViewDetailLabel.frame;
    //    newFrame1.size.height = expectedLabelSize1.height;
    //
    //    //_firstTimeMentionViewHeight.constant=newFrame.size.height +100;
    //    _firstTimeMentionViewHeight.constant=newFrame1.size.height +90;
    //    _timeViewDetailLabel.text=s;
    //
    //    _viewTopHolderHeight.constant=newFrame.size.height +110 +newFrame1.size.height +100 +150;
    //
    
    
    
    
    
    
    // **********
    
    
    
    
    // Do any additional setup after loading the view.
    
    self.viewRelistPopHolder.hidden = YES;
    self.lblRelist.hidden = YES;
    
 //   [self.scrollViewHolder setBounces:false];
    
    [GlobalFunction createBorderforView:self.btnAddRating withWidth:0.0 cornerRadius:5.0 colorForBorder:[UIColor clearColor]];
    
   // _collectionView.delegate=self;
   // _collectionView.dataSource=self;
    
  //  [self.collectionView layoutIfNeeded];
  //  [self.collectionView reloadData];
   // [self.collectionView layoutIfNeeded];
    
    isViewDidLoad = YES;
    
    self.viewBgForFullScreenImage.hidden = YES;
    self.viewImageViewAndActivityHolder.hidden = YES;
    self.btnCloseFullView.hidden = YES;
    
    self.viewImageViewAndActivityHolder.backgroundColor = [UIColor clearColor];
    
    indexCounter = 0;
    
    //    myAppDelegate.viewAllTaskVCObj.btnTime.hidden=YES;
    //    myAppDelegate.viewAllTaskVCObj.btnPrice.hidden=YES;
    //   myAppDelegate.viewAllTaskVCObj.btnRadius.hidden=YES;
    
    //    if (sliderImagesArrayDownloadImages.count<=1) {
    //        for (int i=0; i<self.ArrayOfBlueCircle.count; i++) {
    //            UIImageView *imageview = [self.ArrayOfBlueCircle objectAtIndex:i];
    //            imageview.hidden=true;
    //
    //        }
    //    }
    //    else
    //    {
    //        for (int i=0; i<self.ArrayOfBlueCircle.count; i++) {
    //            UIImageView *imageview = [self.ArrayOfBlueCircle objectAtIndex:i];
    //            if (sliderImagesArrayDownloadImages.count) {
    //                imageview.hidden=false;
    //
    //            }
    //            else
    //            {
    //                imageview.hidden=true;
    //
    //            }
    //
    //        }
    //    }
    
    ispurchaseSuccessFull = false;
    eventAddedSuccessFully = false;

    
    NSLog(@"authorization code is...%@",myAppDelegate.Authorize_String_BrainTree);
    
    self.braintreeClient = [[BTAPIClient alloc] initWithAuthorization:myAppDelegate.Authorize_String_BrainTree];
    
    
}


-(void)appSwitcher:(id)appSwitcher didPerformSwitchToTarget:(BTAppSwitchTarget)target
{
    
}

- (void)startCheckout {
    
    // [GlobalFunction addIndicatorView];
    
    // Example: Initialize BTAPIClient, if you haven't already
    deviceData = @"";
    nonce = @"";
    
    deviceData = [PPDataCollector collectPayPalDeviceData];
    NSLog(@"Send this device data to your server: %@", deviceData);
    
    self.payPalDriver = [[BTPayPalDriver alloc] initWithAPIClient:self.braintreeClient];
    BTPayPalDriver *payPalDriver = [[BTPayPalDriver alloc] initWithAPIClient:self.braintreeClient];
    self.payPalDriver.viewControllerPresentingDelegate = myAppDelegate.vcObj;
    //self.payPalDriver.appSwitchDelegate = self; // Optional
    
    BTPayPalRequest *checkout = [[BTPayPalRequest alloc] init];
    
    //    [self.payPalDriver authorizeAccountWithAdditionalScopes:[NSSet setWithArray:@[@"address"]]
    //                                            completion:^(BTPayPalAccountNonce *tokenizedPayPalAccount, NSError *error) {
    //        if (tokenizedPayPalAccount) {
    //            BTPostalAddress *address = tokenizedPayPalAccount.billingAddress ?: tokenizedPayPalAccount.shippingAddress;
    //            NSLog(@"nonce is...%@",tokenizedPayPalAccount.nonce);
    //            NSLog(@"Address:\n%@\n%@\n%@ %@\n%@ %@", address.streetAddress, address.extendedAddress, address.locality,address.region, address.postalCode, address.countryCodeAlpha2);
    //        } else if (error) {
    //                            // Handle error
    //             NSLog(@"...Error occured...%@",error);
    //        } else {
    //                            // User canceled
    //            NSLog(@"...Use hit cancell button...");
    //        }
    //        }];
    
    
    checkout.billingAgreementDescription = @"Your agreement description";
    
   
    
    [self.payPalDriver requestBillingAgreement:checkout completion:^(BTPayPalAccountNonce * _Nullable tokenizedPayPalCheckout, NSError * _Nullable error) {
        if (error)
        {
            NSLog(@"error occuewd");
            [self errorShow:[NSString stringWithFormat:@"%@",error]];
            [GlobalFunction removeIndicatorView];
            
        } else if (tokenizedPayPalCheckout) {
            
            // [GlobalFunction addIndicatorView];
            
            nonce = tokenizedPayPalCheckout.nonce;
            
            NSLog(@"Got a nonce! %@",nonce);
            
            NSLog(@"Got a type is! %@", tokenizedPayPalCheckout.type);
            
            NSLog(@"Got a type is! %@", tokenizedPayPalCheckout.payerId);
            
            BTPostalAddress *address = tokenizedPayPalCheckout.billingAddress?: tokenizedPayPalCheckout.shippingAddress;
            NSLog(@"Billing address:\n%@\n%@\n%@ %@\n%@ %@", address.streetAddress, address.extendedAddress, address.locality, address.region, address.postalCode, address.countryCodeAlpha2);
            
            [GlobalFunction addIndicatorView];
            
            [self sendAuthorizationToServer:deviceData nonce:nonce];
            
        } else
        {
            NSLog(@"user clicked cancelled");
            // [self back];
            [GlobalFunction removeIndicatorView];

        }
    }];
    
    
    
    
}

-(void) errorShow:(NSString *)message
{
    UIAlertView *alertViewErr = [[UIAlertView alloc]initWithTitle:@"BUY TIMEE" message:message delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    
    [alertViewErr show];
    alertViewErr.tag = 1111;
}

-(void)viewWillDisappear:(BOOL)animated
{
    [GlobalFunction removeIndicatorView];
    
    
}

-(void)viewDidDisappear:(BOOL)animated
{
    // [GlobalFunction removeIndicatorView];
    
    NSLog(@"Send this device data to your server");
    
}

#pragma mark - BTViewControllerPresentingDelegate

// Required
- (void)paymentDriver:(id)paymentDriver
requestsPresentationOfViewController:(UIViewController *)viewController {
    [self presentViewController:viewController animated:YES completion:nil];
}

// Required
- (void)paymentDriver:(id)paymentDriver
requestsDismissalOfViewController:(UIViewController *)viewController {
    [viewController dismissViewControllerAnimated:YES completion:nil];
    
    [GlobalFunction addIndicatorView];
}

#pragma mark - BTAppSwitchDelegate

// Optional - display and hide loading indicator UI
- (void)appSwitcherWillPerformAppSwitch:(id)appSwitcher {
    [self showLoadingUI];
    
    // You may also want to subscribe to UIApplicationDidBecomeActiveNotification
    // to dismiss the UI when a customer manually switches back to your app since
    // the payment button completion block will not be invoked in that case (e.g.
    // customer switches back via iOS Task Manager)
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(hideLoadingUI:)
                                                 name:UIApplicationDidBecomeActiveNotification
                                               object:nil];
}

- (void)appSwitcherWillProcessPaymentInfo:(id)appSwitcher {
    [self hideLoadingUI:nil];
}

#pragma mark - Private methods

- (void)showLoadingUI {
    //...
}

- (void)hideLoadingUI:(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIApplicationDidBecomeActiveNotification
                                                  object:nil];
    //....
}




-(void)ShowImageIndicators{
    
    [_circleImage1 setHidden:YES];
    [_circleImage2 setHidden:YES];
    [_circleImage3 setHidden:YES];
    [_circleImage4 setHidden:YES];
    [_circleImage5 setHidden:YES];
    
    
    
    if (myAppDelegate.workerAllTaskDetail.taskDetailObj.taskImagesArray.count>0) {
        [_circleImage1 setHidden:YES];
        [_circleImage2 setHidden:YES];
        [_circleImage3 setHidden:YES];
        [_circleImage4 setHidden:YES];
        [_circleImage5 setHidden:YES];
        
        
        
        if (myAppDelegate.workerAllTaskDetail.taskDetailObj.taskImagesArray.count==1) {
            [_circleImage1 setHidden:YES];
            [_circleImage2 setHidden:YES];
            [_circleImage3 setHidden:YES];
            [_circleImage4 setHidden:YES];
            [_circleImage5 setHidden:YES];
        }else if (myAppDelegate.workerAllTaskDetail.taskDetailObj.taskImagesArray.count==2) {
            [_circleImage1 setHidden:NO];
            [_circleImage2 setHidden:NO];
        }else if (myAppDelegate.workerAllTaskDetail.taskDetailObj.taskImagesArray.count==3) {
            [_circleImage1 setHidden:NO];
            [_circleImage2 setHidden:NO];
            [_circleImage3 setHidden:NO];
        }else if (myAppDelegate.workerAllTaskDetail.taskDetailObj.taskImagesArray.count==4) {
            [_circleImage1 setHidden:NO];
            [_circleImage2 setHidden:NO];
            [_circleImage3 setHidden:NO];
            [_circleImage4 setHidden:NO];
        }else if (myAppDelegate.workerAllTaskDetail.taskDetailObj.taskImagesArray.count==5) {
            [_circleImage1 setHidden:NO];
            [_circleImage2 setHidden:NO];
            [_circleImage3 setHidden:NO];
            [_circleImage4 setHidden:NO];
            [_circleImage5 setHidden:NO];
        }
    }
    
    if (myAppDelegate.workerAllTaskDetail.taskDetailObj.taskImagesArray.count<=0)
    {
        [self removeSliderImageWithAnimation];
    }
    

    
}
-(void)swipeLeft:(UISwipeGestureRecognizer*)sender
{
    
    
    
    //    if (sliderImagesArrayDownloadImages.count-1==count) {
    //
    //    }
    //    else {
    //        count=count+1;
    //        CATransition *transitionAnimation = [CATransition animation];
    //        [transitionAnimation setType:kCATransitionPush];
    //        [transitionAnimation setSubtype:kCATransitionFromRight];
    //        [transitionAnimation setDuration:0.3f];
    //        [transitionAnimation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    //        [transitionAnimation setFillMode:kCAFillModeBoth];
    //        [self.sliderImageView.layer addAnimation:transitionAnimation forKey:nil];
    //
    //
    //
    //        for (int i = 0  ; i < sliderImagesArrayDownloadImages.count; i ++) {
    //
    //            UIImageView *imageview = [self.ArrayOfBlueCircle objectAtIndex:i];
    //
    //            if (i==count) {
    //                imageview.image=[UIImage imageNamed:@"circleFillImg.png"];
    //                UIImageView *imageview = [self.ArrayOfBlueCircle objectAtIndex:0];
    //                imageview.image=[UIImage imageNamed:@"circleImg"];
    //            }
    //            else {
    //                imageview.image=[UIImage imageNamed:@"circleImg"];
    //            }
    //
    //        }
    //
    //    }
    
    
    
    
    
    
    
    //    if(count==0)
    //    {
    
    NSLog(@"%lu",(unsigned long)sliderImagesArrayDownloadImages.count);
    
    NSLog(@"Count :%d", sliderImagesArrayDownloadImages.count-1);
    
    
    if(sliderImagesArrayDownloadImages.count==0)
    {
        
    }
    
    else
    {
        
        
        
        
        if (count<sliderImagesArrayDownloadImages.count-1) {
            
            
            count++;
            CATransition *animation = [CATransition animation];
            // [animation setDelegate:self];
            [animation setType:kCATransitionPush];
            [animation setSubtype:kCATransitionFromRight];
            [animation setDuration:0.40];
            [animation setTimingFunction:
             [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear]];
            [_sliderImageView.layer addAnimation:animation forKey:kCATransition];
            
            NSLog(@"%@",sliderImagesArrayDownloadImages);
            NSLog(@"%@",[sliderImagesArrayDownloadImages objectAtIndex:1]);
            
            _sliderImageView.image=[sliderImagesArrayDownloadImages objectAtIndex:count];
            
            
            NSLog(@"Count :%d", count);
            [_circleImage1 setImage:[UIImage imageNamed:@"circleImg"]];
            [_circleImage2 setImage:[UIImage imageNamed:@"circleImg"]];
            [_circleImage3 setImage:[UIImage imageNamed:@"circleImg"]];
            [_circleImage4 setImage:[UIImage imageNamed:@"circleImg"]];
            [_circleImage5 setImage:[UIImage imageNamed:@"circleImg"]];
            
            
            if (count==0) {
                [_circleImage1 setImage:[UIImage imageNamed:@"circleFillImg"]];
            }else if (count==1){
                [_circleImage2 setImage:[UIImage imageNamed:@"circleFillImg"]];
            }else if (count==2){
                [_circleImage3 setImage:[UIImage imageNamed:@"circleFillImg"]];
            }else if (count==3){
                [_circleImage4 setImage:[UIImage imageNamed:@"circleFillImg"]];
            }else if (count==4){
                [_circleImage5 setImage:[UIImage imageNamed:@"circleFillImg"]];
            }
            
        }
        
        
    }
    //   count=1;
    
    
    //}
    
    
    
    
    
    //    else if (count==1)
    //    {
    //        CATransition *animation = [CATransition animation];
    //        // [animation setDelegate:self];
    //        [animation setType:kCATransitionPush];
    //        [animation setSubtype:kCATransitionFromRight];
    //        [animation setDuration:0.40];
    //        [animation setTimingFunction:
    //         [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear]];
    //
    //        [_sliderImageView.layer addAnimation:animation forKey:kCATransition];
    //
    //        NSLog(@"%@",[sliderImagesArrayDownloadImages objectAtIndex:2]);
    //
    //        _sliderImageView.image=[sliderImagesArrayDownloadImages objectAtIndex:2];
    //
    //
    //        [_circleImage1 setImage:[UIImage imageNamed:@"circleImg"]];
    //        [_circleImage2 setImage:[UIImage imageNamed:@"circleImg"]];
    //        [_circleImage3 setImage:[UIImage imageNamed:@"circleFillImg"]];
    //        [_circleImage4 setImage:[UIImage imageNamed:@"circleImg"]];
    //        [_circleImage5 setImage:[UIImage imageNamed:@"circleImg"]];
    //
    //
    //
    //        count=2;
    //    }
    //    else if (count==2)
    //    {
    //        CATransition *animation = [CATransition animation];
    //        // [animation setDelegate:self];
    //        [animation setType:kCATransitionPush];
    //        [animation setSubtype:kCATransitionFromRight];
    //        [animation setDuration:0.40];
    //        [animation setTimingFunction:
    //         [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear]];
    //        [_sliderImageView.layer addAnimation:animation forKey:kCATransition];
    //
    //
    //        NSLog(@"%@",[sliderImagesArrayDownloadImages objectAtIndex:3]);
    //
    //        _sliderImageView.image=[sliderImagesArrayDownloadImages objectAtIndex:3];
    //
    //
    //        [_circleImage1 setImage:[UIImage imageNamed:@"circleImg"]];
    //        [_circleImage2 setImage:[UIImage imageNamed:@"circleImg"]];
    //        [_circleImage3 setImage:[UIImage imageNamed:@"circleImg"]];
    //        [_circleImage4 setImage:[UIImage imageNamed:@"circleFillImg"]];
    //        [_circleImage5 setImage:[UIImage imageNamed:@"circleImg"]];
    //
    //
    //
    //        count=3;
    //    }
    //    else if (count==3)
    //    {
    //        CATransition *animation = [CATransition animation];
    //        // [animation setDelegate:self];
    //        [animation setType:kCATransitionPush];
    //        [animation setSubtype:kCATransitionFromRight];
    //        [animation setDuration:0.40];
    //        [animation setTimingFunction:
    //         [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear]];
    //        [_sliderImageView.layer addAnimation:animation forKey:kCATransition];
    //
    //
    //        NSLog(@"%@",[sliderImagesArrayDownloadImages objectAtIndex:4]);
    //
    //        _sliderImageView.image=[sliderImagesArrayDownloadImages objectAtIndex:0];
    //
    //        [_circleImage1 setImage:[UIImage imageNamed:@"circleImg"]];
    //        [_circleImage2 setImage:[UIImage imageNamed:@"circleImg"]];
    //        [_circleImage3 setImage:[UIImage imageNamed:@"circleImg"]];
    //        [_circleImage4 setImage:[UIImage imageNamed:@"circleImg"]];
    //        [_circleImage5 setImage:[UIImage imageNamed:@"circleFillImg"]];
    //
    //
    //
    //        count=4;
    //    }
    
    
}




-(void)rightSwipe:(UISwipeGestureRecognizer*)sender
{
    
    
    
    if (count>0) {
        
        
        count--;
        CATransition *animation = [CATransition animation];
        // [animation setDelegate:self];
        [animation setType:kCATransitionPush];
        [animation setSubtype:kCATransitionFromLeft];
        [animation setDuration:0.40];
        [animation setTimingFunction:
         [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear]];
        [_sliderImageView.layer addAnimation:animation forKey:kCATransition];
        
        NSLog(@"%@",sliderImagesArrayDownloadImages);
        NSLog(@"%@",[sliderImagesArrayDownloadImages objectAtIndex:1]);
        
        _sliderImageView.image=[sliderImagesArrayDownloadImages objectAtIndex:count];
        
        
        NSLog(@"Count :%d", count);
        [_circleImage1 setImage:[UIImage imageNamed:@"circleImg"]];
        [_circleImage2 setImage:[UIImage imageNamed:@"circleImg"]];
        [_circleImage3 setImage:[UIImage imageNamed:@"circleImg"]];
        [_circleImage4 setImage:[UIImage imageNamed:@"circleImg"]];
        [_circleImage5 setImage:[UIImage imageNamed:@"circleImg"]];
        
        
        if (count==0) {
            [_circleImage1 setImage:[UIImage imageNamed:@"circleFillImg"]];
        }else if (count==1){
            [_circleImage2 setImage:[UIImage imageNamed:@"circleFillImg"]];
        }else if (count==2){
            [_circleImage3 setImage:[UIImage imageNamed:@"circleFillImg"]];
        }else if (count==3){
            [_circleImage4 setImage:[UIImage imageNamed:@"circleFillImg"]];
        }else if (count==4){
            [_circleImage5 setImage:[UIImage imageNamed:@"circleFillImg"]];
        }
        
    }
    
    
    return;
    
    if(count==4)
    {
        CATransition *animation = [CATransition animation];
        // [animation setDelegate:self];
        [animation setType:kCATransitionPush];
        [animation setSubtype:kCATransitionFromLeft];
        [animation setDuration:0.40];
        [animation setTimingFunction:
         [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear]];
        [_sliderImageView.layer addAnimation:animation forKey:kCATransition];
        
        _sliderImageView.image=[sliderImagesArrayDownloadImages objectAtIndex:3];
        
        [_circleImage1 setImage:[UIImage imageNamed:@"circleImg"]];
        [_circleImage2 setImage:[UIImage imageNamed:@"circleImg"]];
        [_circleImage3 setImage:[UIImage imageNamed:@"circleImg"]];
        [_circleImage4 setImage:[UIImage imageNamed:@"circleFillImg"]];
        [_circleImage5 setImage:[UIImage imageNamed:@"circleImg"]];
        
        
        count=3;
        
    }
    else if(count==3)
    {
        
        
        CATransition *animation = [CATransition animation];
        // [animation setDelegate:self];
        [animation setType:kCATransitionPush];
        [animation setSubtype:kCATransitionFromLeft];
        [animation setDuration:0.40];
        [animation setTimingFunction:
         [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear]];
        
        [_sliderImageView.layer addAnimation:animation forKey:kCATransition];
        
        _sliderImageView.image=[sliderImagesArrayDownloadImages objectAtIndex:2];
        
        [_circleImage1 setImage:[UIImage imageNamed:@"circleImg"]];
        [_circleImage2 setImage:[UIImage imageNamed:@"circleImg"]];
        [_circleImage3 setImage:[UIImage imageNamed:@"circleFillImg"]];
        [_circleImage4 setImage:[UIImage imageNamed:@"circleImg"]];
        [_circleImage5 setImage:[UIImage imageNamed:@"circleImg"]];
        
        
        
        count=2;
        
    }
    else if(count==2)
    {
        
        
        CATransition *animation = [CATransition animation];
        // [animation setDelegate:self];
        [animation setType:kCATransitionPush];
        [animation setSubtype:kCATransitionFromLeft];
        [animation setDuration:0.40];
        [animation setTimingFunction:
         [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear]];
        
        [_sliderImageView.layer addAnimation:animation forKey:kCATransition];
        _sliderImageView.image=[sliderImagesArrayDownloadImages objectAtIndex:1];
        
        
        [_circleImage1 setImage:[UIImage imageNamed:@"circleImg"]];
        [_circleImage2 setImage:[UIImage imageNamed:@"circleFillImg"]];
        [_circleImage3 setImage:[UIImage imageNamed:@"circleImg"]];
        [_circleImage4 setImage:[UIImage imageNamed:@"circleImg"]];
        
        
        
        count=1;
        
    }
    else if(count==1)
    {
        
        
        CATransition *animation = [CATransition animation];
        // [animation setDelegate:self];
        [animation setType:kCATransitionPush];
        [animation setSubtype:kCATransitionFromLeft];
        [animation setDuration:0.40];
        [animation setTimingFunction:
         [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear]];
        
        [_sliderImageView.layer addAnimation:animation forKey:kCATransition];
        
        _sliderImageView.image=[sliderImagesArrayDownloadImages objectAtIndex:4];
        
        [_circleImage1 setImage:[UIImage imageNamed:@"circleFillImg"]];
        [_circleImage2 setImage:[UIImage imageNamed:@"circleImg"]];
        [_circleImage3 setImage:[UIImage imageNamed:@"circleImg"]];
        [_circleImage4 setImage:[UIImage imageNamed:@"circleImg"]];
        
        count=0;
        
    }
    
    
    
}

-(void) viewWillAppear:(BOOL)animated
{
     [self updatecons];

    [self ShowImageIndicators];
    
    if (isViewDidLoad) {
        [self performWorkAfterUpdate];
        isViewDidLoad = false;
        
    }
    _rateBtn.hidden=YES;
    _flagBtn.hidden = NO;

}


-(void)viewDidAppear:(BOOL)animated{

    NSString* final = @"Reservation Detail page";
    
    [Flurry logEvent:final];
    
    [self downloadSliderImages];
    
   // _rateBtn.hidden = false;
}

-(void)updatecons{
    
    _buyNowBtn.backgroundColor=[UIColor colorWithRed:63.0/255.0f green:67.0/255.0f blue:70.0/255.0f alpha:1.0];
    _timeViewDetailLabel.textColor=[UIColor colorWithRed:0.0/255.0f green:0.0/255.0f blue:0.0/255.0f alpha:1.0];
    _addressViewDetailLabel.textColor=[UIColor colorWithRed:0.0/255.0f green:0.0/255.0f blue:0.0/255.0f alpha:1.0];
    
    
    _timeViewDateTimeLabel.textColor=[UIColor colorWithRed:63.0/255.0f green:67.0/255.0f blue:70.0/255.0f alpha:1.0];
    _addressViewAddressLabel.textColor=[UIColor colorWithRed:0.0/255.0f green:0.0/255.0f blue:0.0/255.0f alpha:1.0];
    
    
    _lineOneLabel.backgroundColor=[UIColor colorWithRed:77.0/255.0f green:96.0/255.0f blue:111.0/255.0f alpha:1.0];
    _lineTwoLabel.backgroundColor=[UIColor colorWithRed:77.0/255.0f green:96.0/255.0f blue:111.0/255.0f alpha:1.0];
    
    
    int imagesArrayCountVal=myAppDelegate.workerAllTaskDetail.taskDetailObj.taskImagesArray.count;
    
    NSLog(@"images array count value --- %d",imagesArrayCountVal);
    
    [self.view layoutIfNeeded];
    if(IS_IPHONE_6)
    {
        
        _circleImageHeight.constant=10;
        _circleImageWidth.constant=10;
        _circleImageTop.constant=150;
        _circleImageLeft.constant=145;
        _sliderImageViewHeight.constant=172;
        NSLog(@"%d",count);
        _buyNowBtnHeight.constant=45;
        _circleImagesViewLeft.constant=(375/2)-imagesArrayCountVal*(_circleImage1.frame.size.width)-10;
        NSLog(@"%f",_circleImagesViewLeft.constant);
        [self.buyNowBtn.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:19]];
        [_activityIndicator startAnimating];
        _activityIndicatorLeft.constant=180;
        _activityIndicatorTop.constant=90;
        
        [self.timeViewDateTimeLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:15]];
        [self.timeViewDetailLabel setFont:[UIFont fontWithName:@"Lato-Light" size:14.5]];
        [self.timeViewPriceLabel setFont:[UIFont fontWithName:@"Lato-Bold" size:15]];
        [self.firstViewDiscountDescriptionLbl setFont:[UIFont fontWithName:@"Lato-Light" size:14.5]];
        [self.addressViewHeadingLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:15]];
        [self.addressViewDetailLabel setFont:[UIFont fontWithName:@"Lato-Light" size:14.5]];
        [self.addressViewAddressLabel setFont:[UIFont fontWithName:@"Lato-Light" size:15]];
        [self.addressViewURLlbl setFont:[UIFont fontWithName:@"Lato-Light" size:15]];
        [self.postedByHeadingLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:16]];
        
        
        _firstViewHeadingLblTop.constant=6;
        _firstViewHeadingLblLeft.constant=10;
        _firstViewDateLblTop.constant=4;
        _firstViewDateLblLeft.constant=10;
        _firstViewPriceLblTop.constant=2;
        _firstViewPriceLblRight.constant=10;
        _firstViewDEscriptionLblLeft.constant=10;
        _firstViewDEscriptionLblTop.constant=10;
    
        _conDiscountDescriptionLblTop.constant=10;
        _conDiscountDescriptionLblLeft.constant=10;
        
        
        _conNewPriceLblRight.constant=0;
        _consFirstViewHeadingLblWidth.constant=340;
        
        _secondViewHeadingLblTop.constant=6;
        _secondViewAddressLblTop.constant=-3;
        _secondViewURLLblTop.constant=0;
        _secondViewHeadingLblLeft.constant=10;
        _secondViewAddressLblLeft.constant=10;
        _secondViewURLLblLeft.constant=10;
        _secondViewDetailLblLeft.constant=10;
        _secondViewDetailLblTop.constant=10;
        
        _rateBtnWidth.constant=40;
        _rateBtnHeight.constant=17;
        _rateBtnTop.constant=61;
        _rateBtnRight.constant=13;
        
        _postedByTop.constant=0;
        _postedByLeft.constant=11;
        
        _imgViewTop.constant=11;
        _imageViewWidth.constant=68;
        _imageViewHeight.constant=68;
        
        _flagBtnWidth.constant=15;
        _flagBtnHeight.constant=16;
        _flagBtnTop.constant=9;
        _flagBtnRight.constant=5;
        
        
        
        _postedByNameLblTop.constant=18;
        _postedByNameLblLeft.constant=10;
        
        _starOneTop.constant=4;
        _starOneLeft.constant=13;
        _imageViewLeft.constant=13;
        
        self.postedByViewUserImage.layer.cornerRadius = self.imageViewHeight.constant/2;//34;
        self.postedByViewUserImage.layer.masksToBounds = YES;
        [_buyNowBtn setTitleEdgeInsets:UIEdgeInsetsMake(-6,8,0,0)];
        
        _consBlurViewHeight.constant=667;
        _consWebViewHeight.constant=555;
      //  _crossBtnWidth.constant=30;
      //  _crossBtnHeight.constant=40;
        _crossBtnTop.constant=8;
       // _consCrossBtnRight.constant = 3;
        NSLog(@"right of cross btn..%f",_consCrossBtnRight.constant);
        
    }
    else if (IS_IPHONE_5 || IS_IPHONE_4_OR_LESS)
    {
        _conNewPriceLblRight.constant=0;
        _conNewPriceLblWidth.constant=52;
        _consDiscountPriceLblWidth.constant=40;
        
        _viewTopHolderWidth.constant=320;
        _sliderImageViewHeight.constant=147;
        _circleImagesViewLeft.constant=(_viewTopHolderWidth.constant/2)-imagesArrayCountVal*(_circleImage1.frame.size.width);
        NSLog(@"%f",_circleImagesViewLeft.constant);
        [_activityIndicator startAnimating];
        _circleImagesViewTop.constant=148;
        _buyNowBtnHeight.constant=38;
        
        [self.buyNowBtn.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:16]];
        
        [self.timeViewHeadingLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:14]];
        [self.timeViewDateTimeLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:13]];
        [self.timeViewDetailLabel setFont:[UIFont fontWithName:@"Lato-Light" size:12.5]];
        [self.timeViewPriceLabel setFont:[UIFont fontWithName:@"Lato-Bold" size:13]];
        [self.firstViewDiscountDescriptionLbl setFont:[UIFont fontWithName:@"Lato-Light" size:13]];

        [self.firstViewNewPriceLbl setFont:[UIFont fontWithName:@"Lato-Regular" size:14]];
        [self.firstViewDiscountlLbl setFont:[UIFont fontWithName:@"Lato-Regular" size:12]];

 
        
        
        [self.addressViewHeadingLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:14]];
        [self.addressViewDetailLabel setFont:[UIFont fontWithName:@"Lato-Light" size:12.5]];
        [self.addressViewAddressLabel setFont:[UIFont fontWithName:@"Lato-Light" size:13]];
        [self.addressViewURLlbl setFont:[UIFont fontWithName:@"Lato-Light" size:13]];
        [self.postedByHeadingLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:14]];
        [self.postedByViewNameLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:14]];
        
        
        _rateBtnWidth.constant=35;
        _rateBtnHeight.constant=15;
        _rateBtnTop.constant=50;
        _rateBtnRight.constant=11;
        
        _activityIndicatorLeft.constant=150;
        _activityIndicatorTop.constant=85;
        _buyNowBtnBottom.constant=60;
        
        _firstViewHeadingLblTop.constant=4;
        _firstViewHeadingLblLeft.constant=10;
        _firstViewDateLblTop.constant=0;
        _firstViewDateLblLeft.constant=10;
        _firstViewPriceLblTop.constant=0;
        _firstViewDEscriptionLblLeft.constant=10;
        _firstViewDEscriptionLblTop.constant=7;
        
        _conNewPriceLblTop.constant=4;
        _consDiscountPriceLblTop.constant=4;
        
        _conDiscountDescriptionLblLeft.constant=10;
        _conDiscountDescriptionLblTop.constant=7;
        
        _crossLineLblTop.constant=17;
        
        _consFirstViewHeadingLblWidth.constant=285;
        
        _consDiscountPriceLblRight.constant=8;
        
        _secondViewHeadingLblTop.constant=2;
        _secondViewAddressLblTop.constant=-4;
        _secondViewURLLblTop.constant=-4;
        _secondViewHeadingLblLeft.constant=10;
        _secondViewAddressLblLeft.constant=10;
        _secondViewURLLblLeft.constant=10;
        _secondViewDetailLblLeft.constant=10;
        _secondViewDetailLblTop.constant=6;
        
        _flagBtnWidth.constant=13;
        _flagBtnHeight.constant=13;
        _flagBtnTop.constant=7;
        _flagBtnRight.constant=5;
        
        
        _postedByNameLblTop.constant=10;
        _postedByNameLblLeft.constant=12;
        
        _postedByTop.constant=-2;
        _postedByLeft.constant=10;
        
        _starOneTop.constant=1;
        _starOneLeft.constant=7;
        _imageViewLeft.constant=15;
        
        _imageViewWidth.constant=58;
        _imageViewHeight.constant=58;
        _imgViewTop.constant=5;
        
        self.postedByViewUserImage.layer.cornerRadius = self.imageViewHeight.constant/2;
        self.postedByViewUserImage.layer.masksToBounds = YES;
        [_buyNowBtn setTitleEdgeInsets:UIEdgeInsetsMake(-5,7,0,0)];
        
        
        
        
        _consBlurViewHeight.constant=568;
        _consWebViewHeight.constant=468;
        _crossBtnWidth.constant=30;
        _crossBtnHeight.constant=30;
        _crossBtnTop.constant=7;
        _consCrossBtnRight.constant=2;
        

        
        
    }
    
    
    else if (IS_IPHONE_6P)
    {
        _conNewPriceLblRight.constant=0;

        _sliderImageViewHeight.constant=196;
        _viewTopHolderWidth.constant=414;
        _circleImagesViewLeft.constant=(_viewTopHolderWidth.constant/2)-imagesArrayCountVal*(_circleImage1.frame.size.width);
        
        [self.buyNowBtn.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:21]];
        _buyNowBtnHeight.constant=50;
        [_activityIndicator startAnimating];
        _activityIndicatorLeft.constant=190;
        _activityIndicatorTop.constant=95;
        
        [self.timeViewDateTimeLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:15]];
        [self.timeViewDetailLabel setFont:[UIFont fontWithName:@"Lato-Light" size:16]];
        [self.timeViewPriceLabel setFont:[UIFont fontWithName:@"Lato-Bold" size:15]];
        [self.firstViewDiscountDescriptionLbl setFont:[UIFont fontWithName:@"Lato-Light" size:16]];

        
        [self.addressViewHeadingLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:17]];
        [self.addressViewDetailLabel setFont:[UIFont fontWithName:@"Lato-Light" size:16]];
        [self.addressViewAddressLabel setFont:[UIFont fontWithName:@"Lato-Light" size:15]];
        [self.addressViewURLlbl setFont:[UIFont fontWithName:@"Lato-Light" size:15]];
        [self.postedByHeadingLabel setFont:[UIFont fontWithName:@"Lato-Light" size:16]];
        

        [self.firstViewNewPriceLbl setFont:[UIFont fontWithName:@"Lato-Regular" size:17]];
        [self.firstViewDiscountlLbl setFont:[UIFont fontWithName:@"Lato-Regular" size:17]];
        
        
        _firstViewHeadingLblTop.constant=6;
        _firstViewHeadingLblLeft.constant=10;
        _firstViewDateLblTop.constant=4;
        _firstViewDateLblLeft.constant=10;
        _firstViewPriceLblTop.constant=5;
        _firstViewPriceLblRight.constant=13;
        _firstViewDEscriptionLblLeft.constant=10;
        _firstViewDEscriptionLblTop.constant=5;
        
        _conNewPriceLblTop.constant=6;
        _consDiscountPriceLblTop.constant=6;
        
        _conDiscountDescriptionLblTop.constant=5;
        _conDiscountDescriptionLblLeft.constant=10;
        
        _crossLineLblTop.constant=18;
        _consFirstViewHeadingLblWidth.constant=379;

        
        _secondViewHeadingLblTop.constant=7;
        _secondViewAddressLblTop.constant=0;
        _secondViewURLLblTop.constant=0;
        _secondViewHeadingLblLeft.constant=10;
        _secondViewAddressLblLeft.constant=10;
        _secondViewURLLblLeft.constant=10;
        _secondViewDetailLblLeft.constant=10;
        _secondViewDetailLblTop.constant=13;
        
        _postedByTop.constant=2;
        _postedByLeft.constant=10;
        _imgViewTop.constant=6;
        _imageViewWidth.constant=84;
        _imageViewHeight.constant=84;
        
        _flagBtnWidth.constant=16;
        _flagBtnTop.constant=9;
        _flagBtnHeight.constant=17;
        _flagBtnRight.constant=6;
        
        _buyNowBtnBottom.constant=75;
        
        _postedByNameLblTop.constant=21;
        _postedByNameLblLeft.constant=7;
        
        _starOneTop.constant=10;
        _starOneLeft.constant=7;
        _imageViewLeft.constant=15;
        
        
        _rateBtnWidth.constant=45;
        _rateBtnHeight.constant=17;
        _rateBtnTop.constant=61;
        _rateBtnRight.constant=13;
        
        self.postedByViewUserImage.layer.cornerRadius = 42;
        self.postedByViewUserImage.layer.masksToBounds = YES;
        
        [_buyNowBtn setTitleEdgeInsets:UIEdgeInsetsMake(-6,8,0,0)];
        
        _consBlurViewHeight.constant=667;
        _consWebViewHeight.constant=618;
        _crossBtnWidth.constant=35;
        _crossBtnHeight.constant=35;
        _crossBtnTop.constant=5;
        _consCrossBtnRight.constant=2;
        

        
        
    }
    [self.view layoutIfNeeded];
}


-(void)performWorkAfterUpdate{
    
    [self.view layoutIfNeeded];
    
    if (!myAppDelegate.isComingFromPushNotification) {
        
        if (myAppDelegate.isOfOtherUser) {
            
            [self handleCasesForOtherUser];
        }else{
            [self handleCasesForSelf];
        }
        
    }else{
        
        [self handleCasesForSelf];
        
    }
    [self.view layoutIfNeeded];
    
    return;
    
    
    [self.view layoutIfNeeded];
    
#pragma mark - TODO: Check condition here
    self.constraintBottomHolderViewHeight.constant = self.viewRatingHolder.frame.size.height + self.viewUserAdditionalInformation.frame.size.height + self.viewUserInfoHolder.frame.size.height;
    
    if ([self.lblAddress2.text isEqualToString:@""] && [self.lblUserAddress.text isEqualToString:@""]) {
        self.lblCenterHolder.hidden = YES;
    }
    
    if (self.imgViewUserProfilePicture.image) {
        
    }else{
        self.imgViewUserProfilePicture.image = imageUserDefault;
    }
    
//    self.imgViewUserProfilePicture.layer.cornerRadius = self.imgViewUserProfilePicture.frame.size.height/2;
//    self.imgViewUserProfilePicture.layer.masksToBounds = YES;
    
    CGFloat height = [GlobalFunction heightForWidth:self.lblTaskDescription.frame.size.width usingFont:self.lblTaskDescription.font forLabel:self.lblTaskDescription];
    
    self.constraintLblTaskDescriptionHeight.constant = height;
    
    self.constraintViewLblTaskDescriptionHolderHeight.constant = height + 50;
    
    [self.view layoutIfNeeded];
    
    for (NSLayoutConstraint *constraint in self.constraintViewBtnsHeight) {
        
        if (IS_IPHONE_4_OR_LESS) {
            constraint.constant = 48;
        }
        if (IS_IPHONE_5) {
            constraint.constant = 58;
        }
        if (IS_IPHONE_6) {
            constraint.constant = 68;
        }
        if (IS_IPHONE_6P) {
            constraint.constant = 78;
        }
        
    }
    
    NSInteger count = 1;
    
    for (UIButton *btn in self.btnStars) {
        
        if (count<=noOfRating) {
            
            [btn setSelected:YES];
            
            count = count + 1;
            
        }else{
            
            [btn setSelected:NO];
            
            count = count + 1;
            
        }
        
    }
    
    for (UIButton *btn in self.btnCollection) {
        
        if (IS_IPHONE_4_OR_LESS) {
            [btn.titleLabel setFont:[UIFont systemFontOfSize:12.0]];
        }
        if (IS_IPHONE_5) {
            [btn.titleLabel setFont:[UIFont systemFontOfSize:15.0]];
        }
        if (IS_IPHONE_6) {
            [btn.titleLabel setFont:[UIFont systemFontOfSize:18.0]];
        }
        if (IS_IPHONE_6P) {
            [btn.titleLabel setFont:[UIFont systemFontOfSize:23.0]];
        }
        
    }
    
    
    CGRect r = [GlobalFunction rectForLabel:self.lblTaskName];
    CGRect r2 = [GlobalFunction rectForLabel:self.lblTaskPrice];
    CGRect r3 = [GlobalFunction rectForLabel:self.lblTaskDate];
    
    int roundedUp1 = ceil(r.size.width);
    int roundedUp2 = ceil(r2.size.width);
    int roundedUp3 = ceil(r3.size.width);
    
    //for adjusting task name
    CGFloat remainingWidth = SCREEN_WIDTH - 73 - roundedUp3;    //space for user image and its leading and trailing
    
    CGFloat remainingWidthAfterRemovingSeperatorLabelSpace = remainingWidth - 80;
    
    if (roundedUp2>80) {
        if (roundedUp1>remainingWidthAfterRemovingSeperatorLabelSpace) {
            self.constraintLblTaskNameWidth.constant = remainingWidthAfterRemovingSeperatorLabelSpace;
            self.constraintLblPriceWidth.constant = 80;
            self.constraintLblDateWidth.constant = roundedUp3;
            self.constraintLblDateTrailing.constant = 20;
        }else{
            remainingWidthAfterRemovingSeperatorLabelSpace = remainingWidthAfterRemovingSeperatorLabelSpace - roundedUp1;
            self.constraintLblTaskNameWidth.constant = roundedUp1;
            self.constraintLblDateWidth.constant = roundedUp3;
            remainingWidthAfterRemovingSeperatorLabelSpace = remainingWidthAfterRemovingSeperatorLabelSpace + 80;
            if (roundedUp2>remainingWidthAfterRemovingSeperatorLabelSpace) {
                self.constraintLblPriceWidth.constant = 80+remainingWidthAfterRemovingSeperatorLabelSpace;
                self.constraintLblDateTrailing.constant = 20;
            }else{
                self.constraintLblPriceWidth.constant = roundedUp2;
                remainingWidthAfterRemovingSeperatorLabelSpace = remainingWidthAfterRemovingSeperatorLabelSpace - roundedUp2;
                self.constraintLblDateTrailing.constant = 20+remainingWidthAfterRemovingSeperatorLabelSpace;
            }
        }
    }else{
        remainingWidthAfterRemovingSeperatorLabelSpace = remainingWidthAfterRemovingSeperatorLabelSpace + (80-roundedUp2);
        if (roundedUp1>remainingWidthAfterRemovingSeperatorLabelSpace) {
            self.constraintLblTaskNameWidth.constant = remainingWidthAfterRemovingSeperatorLabelSpace;
            self.constraintLblPriceWidth.constant = roundedUp2;
            self.constraintLblDateWidth.constant = roundedUp3;
            self.constraintLblDateTrailing.constant = 20;
        }else{
            remainingWidthAfterRemovingSeperatorLabelSpace = remainingWidthAfterRemovingSeperatorLabelSpace - roundedUp1;
            self.constraintLblTaskNameWidth.constant = roundedUp1;
            self.constraintLblDateWidth.constant = roundedUp3;
            self.constraintLblPriceWidth.constant = roundedUp2;
            self.constraintLblDateTrailing.constant = 20+remainingWidthAfterRemovingSeperatorLabelSpace;
        }
    }
    
    self.lblTaskName.lineBreakMode = NSLineBreakByTruncatingTail;
    self.lblTaskPrice.lineBreakMode = NSLineBreakByTruncatingTail;
    
    [self.collectionView layoutIfNeeded];
    [self.collectionView reloadData];
    [self.collectionView layoutIfNeeded];
    
    [self.view layoutIfNeeded];
    
    [self performSelector:@selector(removeIndicator) withObject:nil afterDelay:0.2];
    
}


#pragma mark - push notification handling for task detail data
-(void)handleWorkingOfPushNotification{
    
    noOfRating = [myAppDelegate.workerPushTaskDetail.avg_rating_acceptedBy integerValue];
    
    if (myAppDelegate.workerPushTaskDetail.isAlreadyRated) {
        self.btnAddRating.enabled = NO;
    }
    
    self.viewAcceptBtnWorkerHolder.hidden = true;
    self.viewYesNoBtnEmployerHolder.hidden = true;
    
    self.constraintViewTaskPaymentDetailHolderHeight.constant = 0;
    self.constraintViewUserAdditionalInformationHeight.constant = 63;
    self.constraintLblTaskCompletedHolderView.constant = 40;
    
    self.viewPaymentDetailHolder.hidden = YES;
    self.viewUserAdditionalInformation.hidden = NO;
    self.lblTaskCompleted.hidden = NO;
    self.viewLblTaskCompletedHolder.hidden = NO;
    
    for (int i = 0; i<myAppDelegate.arrayCategoryDetail.count; i++) {
        
        if ([[myAppDelegate.arrayCategoryDetail objectAtIndex:i] isKindOfClass:[subCategory class]]){
            
            subCategory *obj = [myAppDelegate.arrayCategoryDetail objectAtIndex:i];
            
            if ([obj.subCategoryId isEqualToString:myAppDelegate.workerPushTaskDetail.taskDetailObj.subcategoryId]) {
                
                self.lblAddress2.text = obj.name;
                
            }
            
        }
        
    }
    
    //task information
    self.lblTaskDescription.text = myAppDelegate.taskPushData.taskDescription;
    self.lblTaskName.text = myAppDelegate.taskPushData.title;
    
    if ([GlobalFunction checkIfContainFloat:myAppDelegate.taskPushData.price]) {
        self.lblTaskPrice.text = [NSString stringWithFormat:@"$%0.2f",myAppDelegate.taskPushData.price];
    }else{
        self.lblTaskPrice.text = [NSString stringWithFormat:@"$%0.0f",myAppDelegate.taskPushData.price];
    }
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:dateFormatFull];
    NSDate *date = [dateFormatter dateFromString:myAppDelegate.taskPushData.startDate];
    [dateFormatter setDateFormat:dateFormatDate];
    NSString *dateString = [dateFormatter stringFromDate:date];
    
    self.lblTaskDate.text = dateString;
    
    
    if ([myAppDelegate.userData.userid isEqualToString:myAppDelegate.workerPushTaskDetail.userId_acceptedBy]) {
        
        workerOrEmployer = @"Worker";
        
        noOfRating = myAppDelegate.workerPushTaskDetail.avgStars_createdBy;
        
        self.viewBottomHolder.backgroundColor = RGB(248, 248, 248);
        
        //other user information
        self.imgViewUserProfilePicture.image = myAppDelegate.workerPushTaskDetail.userImage_createdBy;
        self.lblUserName.text = myAppDelegate.workerPushTaskDetail.userName_createdBy;
        self.lblUserAddress.text = myAppDelegate.workerPushTaskDetail.locationName_createdBy;
        
        emailIdToSend = myAppDelegate.workerPushTaskDetail.emailId_createdBy;
        telToCall = myAppDelegate.workerPushTaskDetail.mobile_createdBy;
        locationToShow = myAppDelegate.workerPushTaskDetail.locationName_createdBy;
        
    }else if ([myAppDelegate.userData.userid isEqualToString:myAppDelegate.workerPushTaskDetail.userId_createdBy]){
        
        workerOrEmployer = @"Employer";
        
        noOfRating = myAppDelegate.workerPushTaskDetail.avgStars_acceptedBy;
        
        //other user information
        self.imgViewUserProfilePicture.image = myAppDelegate.workerPushTaskDetail.userImage_acceptedBy;
        self.lblUserName.text = myAppDelegate.workerPushTaskDetail.userName_acceptedBy;
        self.lblUserAddress.text = myAppDelegate.workerPushTaskDetail.locationName_acceptedBy;
        
        emailIdToSend = myAppDelegate.workerPushTaskDetail.emailId_acceptedBy;
        telToCall = myAppDelegate.workerPushTaskDetail.mobile_acceptedBy;
        locationToShow = myAppDelegate.workerPushTaskDetail.locationName_acceptedBy;
        
    }else{
        
        noOfRating = myAppDelegate.workerPushTaskDetail.avgStars_createdBy;
        
        self.viewBottomHolder.backgroundColor = RGB(248, 248, 248);
        
        //other user information
        self.imgViewUserProfilePicture.image = myAppDelegate.workerPushTaskDetail.userImage_createdBy;
        self.lblUserName.text = myAppDelegate.workerPushTaskDetail.userName_createdBy;
        self.lblUserAddress.text = myAppDelegate.workerPushTaskDetail.locationName_createdBy;
        
        emailIdToSend = myAppDelegate.workerPushTaskDetail.emailId_createdBy;
        telToCall = myAppDelegate.workerPushTaskDetail.mobile_createdBy;
        locationToShow = myAppDelegate.workerPushTaskDetail.locationName_createdBy;
        
    }
    
    if (myAppDelegate.isOfPushOtherUser) {
        
        self.btnLocation.enabled = NO;
        self.btnMail.enabled = NO;
        self.btnContact.enabled = NO;
        
        for (UIView *view in self.viewDoneBtnWorkerHolder.subviews) {
            if ([view isKindOfClass:[UIButton class]]) {
                UIButton *btn = (UIButton *)view;
                [btn setTitle:@"Done" forState:UIControlStateNormal];
            }
        }
        
        self.viewLblTaskCompletedHolder.hidden = true;
        self.viewCompleteIncompleteBtnEmployerHolder.hidden = true;
        
        self.constraintLblTaskCompletedHolderView.constant = 0;
        self.constraintViewRatingHolderHeight.constant = 0;
        
        self.lblTaskCompleted.hidden = YES;
        self.viewRatingHolder.hidden = YES;
        self.btnAddRating.hidden = YES;
        
        if ([myAppDelegate.userData.userid isEqualToString:myAppDelegate.workerPushTaskDetail.userId_acceptedBy]) {
            
            self.viewDoneBtnEmployerHolder.hidden = true;
            self.viewDoneBtnWorkerHolder.hidden = false;
            
            
        }else if ([myAppDelegate.userData.userid isEqualToString:myAppDelegate.workerPushTaskDetail.userId_createdBy]){
            
            self.viewDoneBtnEmployerHolder.hidden = false;
            self.viewDoneBtnWorkerHolder.hidden = true;
            
        }
        
    }else{
        
        if ([myAppDelegate.notificationType isEqualToString:@"101"] && [myAppDelegate.workerPushTaskDetail.taskDetailObj.status isEqualToString:@"1"]) {
            
            for (UIView *view in self.viewDoneBtnWorkerHolder.subviews) {
                if ([view isKindOfClass:[UIButton class]]) {
                    UIButton *btn = (UIButton *)view;
                    [btn setTitle:@"Task Accepted" forState:UIControlStateNormal];
                }
            }
            
            self.lblTaskCompleted.text = @"Task Accepted";
            
            self.viewDoneBtnEmployerHolder.hidden = true;
            
            self.constraintViewRatingHolderHeight.constant = 0;
            
            self.viewRatingHolder.hidden = YES;
            self.btnAddRating.hidden = YES;
            
            if ([myAppDelegate.userData.userid isEqualToString:myAppDelegate.workerPushTaskDetail.userId_acceptedBy]) {
                
                self.viewCompleteIncompleteBtnEmployerHolder.hidden = true;
                self.viewDoneBtnWorkerHolder.hidden = false;
                
            }else if ([myAppDelegate.userData.userid isEqualToString:myAppDelegate.workerPushTaskDetail.userId_createdBy]){
                
                self.viewCompleteIncompleteBtnEmployerHolder.hidden = false;
                self.viewDoneBtnWorkerHolder.hidden = true;
                
            }
            
        }else{
            
            for (UIView *view in self.viewDoneBtnWorkerHolder.subviews) {
                if ([view isKindOfClass:[UIButton class]]) {
                    UIButton *btn = (UIButton *)view;
                    [btn setTitle:@"Done" forState:UIControlStateNormal];
                }
            }
            
            self.lblTaskCompleted.text = @"Task Completed";
            
            self.viewCompleteIncompleteBtnEmployerHolder.hidden = true;
            
            if ([myAppDelegate.userData.userid isEqualToString:myAppDelegate.workerPushTaskDetail.userId_acceptedBy]) {
                
                self.viewDoneBtnEmployerHolder.hidden = true;
                self.viewDoneBtnWorkerHolder.hidden = false;
                
                self.constraintViewRatingHolderHeight.constant = 0;
                
                self.viewRatingHolder.hidden = YES;
                self.btnAddRating.hidden = YES;
                
            }else if ([myAppDelegate.userData.userid isEqualToString:myAppDelegate.workerPushTaskDetail.userId_createdBy]){
                
                self.viewDoneBtnEmployerHolder.hidden = false;
                self.viewDoneBtnWorkerHolder.hidden = true;
                
                self.constraintViewRatingHolderHeight.constant = 96;
                
                self.viewRatingHolder.hidden = NO;
                self.btnAddRating.hidden = NO;
                
            }
            
            if ([myAppDelegate.notificationType isEqualToString:@"102"] && [myAppDelegate.workerPushTaskDetail.taskDetailObj.status isEqualToString:@"3"]){
                
                NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
                [dateFormatter setDateFormat:dateFormatFull];
                NSDate *date = [dateFormatter dateFromString:myAppDelegate.taskData.lastModifyTime];
                [dateFormatter setDateFormat:@"dd-MM-YY"];
                NSString *dateString2 = [dateFormatter stringFromDate:date];
                
                if ([myAppDelegate.userData.userid isEqualToString:myAppDelegate.workerPushTaskDetail.userId_createdBy]) {
                    
                    //task information
                    self.lblCreditCardEndingIn.text = @"Credit Card Ending in:";
                    self.lblCreditCardLast4DigitHolder.text = [myAppDelegate.workerPushTaskDetail.cardDetail substringWithRange:NSMakeRange(12, 4)];
                    
                    self.lblPaymentSentDate.text = dateString2;
                    
                }else if ([myAppDelegate.userData.userid isEqualToString:myAppDelegate.workerPushTaskDetail.userId_acceptedBy]){
                    
                    //task information
                    self.lblCreditCardEndingIn.text = @"Credited to your account:";
                    self.lblCreditCardLast4DigitHolder.text = [NSString stringWithFormat:@"$%0.2f (Processing fees will apply)",myAppDelegate.workerPushTaskDetail.taskDetailObj.price];
                    
                    self.lblPaymentSentDate.text = dateString2;
                    
                }
                
                self.constraintViewUserAdditionalInformationHeight.constant = 63;
                self.constraintLblTaskCompletedHolderView.constant = 40;
                self.constraintViewTaskPaymentDetailHolderHeight.constant = 150;
                
                self.viewUserAdditionalInformation.hidden = NO;
                self.lblTaskCompleted.hidden = NO;
                self.viewLblTaskCompletedHolder.hidden = NO;
                self.viewPaymentDetailHolder.hidden = NO;
                
            }else{
                
                NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
                [dateFormatter setDateFormat:dateFormatFull];
                NSDate *date = [dateFormatter dateFromString:myAppDelegate.taskData.lastModifyTime];
                [dateFormatter setDateFormat:@"dd-MM-YY"];
                NSString *dateString2 = [dateFormatter stringFromDate:date];
                
                if ([myAppDelegate.userData.userid isEqualToString:myAppDelegate.workerPushTaskDetail.userId_createdBy]) {
                    
                    //task information
                    self.lblCreditCardEndingIn.text = @"Credit Card Ending in:";
                    self.lblCreditCardLast4DigitHolder.text = [myAppDelegate.workerPushTaskDetail.cardDetail substringWithRange:NSMakeRange(12, 4)];
                    
                    self.lblPaymentSentDate.text = dateString2;
                    
                }else if ([myAppDelegate.userData.userid isEqualToString:myAppDelegate.workerPushTaskDetail.userId_acceptedBy]){
                    
                    //task information
                    self.lblCreditCardEndingIn.text = @"Credited to your account:";
                    self.lblCreditCardLast4DigitHolder.text = [NSString stringWithFormat:@"$%0.2f (Processing fees will apply)",myAppDelegate.workerPushTaskDetail.taskDetailObj.price];
                    
                    self.lblPaymentSentDate.text = dateString2;
                    
                }
                
                self.constraintViewUserAdditionalInformationHeight.constant = 63;
                self.constraintLblTaskCompletedHolderView.constant = 40;
                self.constraintViewTaskPaymentDetailHolderHeight.constant = 150;
                
                self.viewUserAdditionalInformation.hidden = NO;
                self.lblTaskCompleted.hidden = NO;
                self.viewLblTaskCompletedHolder.hidden = NO;
                self.viewPaymentDetailHolder.hidden = NO;
                
            }
            
        }
        
    }
    
}


#pragma mark - handling of task active task detail data
-(void)handleWorkingOfTaskActiveData{
    
    for (UIView *view in self.viewDoneBtnWorkerHolder.subviews) {
        if ([view isKindOfClass:[UIButton class]]) {
            UIButton *btn = (UIButton *)view;
            [btn setTitle:@"Task Accepted" forState:UIControlStateNormal];
        }
    }
    
    if (myAppDelegate.workerTaskDetail.isAlreadyRated) {
        self.btnAddRating.enabled = NO;
    }
    
    self.lblTaskCompleted.text = @"Task Accepted";
    
    //fill task information
    self.lblTaskDescription.text = myAppDelegate.taskData.taskDescription;
    self.lblTaskName.text = myAppDelegate.taskData.title;
    
    if ([GlobalFunction checkIfContainFloat:myAppDelegate.taskData.price]) {
        self.lblTaskPrice.text = [NSString stringWithFormat:@"$%0.2f",myAppDelegate.taskData.price];
    }else{
        self.lblTaskPrice.text = [NSString stringWithFormat:@"$%0.0f",myAppDelegate.taskData.price];
    }
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:dateFormatFull];
    NSDate *date = [dateFormatter dateFromString:myAppDelegate.taskData.startDate];
    [dateFormatter setDateFormat:dateFormatDate];
    NSString *dateString = [dateFormatter stringFromDate:date];
    
    self.lblTaskDate.text = dateString;
    
    for (int i = 0; i<myAppDelegate.arrayCategoryDetail.count; i++) {
        
        if ([[myAppDelegate.arrayCategoryDetail objectAtIndex:i] isKindOfClass:[subCategory class]]){
            
            subCategory *obj = [myAppDelegate.arrayCategoryDetail objectAtIndex:i];
            
            if ([obj.subCategoryId isEqualToString:myAppDelegate.workerTaskDetail.taskDetailObj.subcategoryId]) {
                
                self.lblAddress2.text = obj.name;
                
            }
            
        }
        
    }
    
    //arrange view according to task user id
    if ([myAppDelegate.userData.userid isEqualToString:myAppDelegate.workerTaskDetail.userId_createdBy]) {
        
        workerOrEmployer = @"Employer";
        
        noOfRating = myAppDelegate.workerTaskDetail.avgStars_acceptedBy;
        
        //other user information
        self.imgViewUserProfilePicture.image = myAppDelegate.workerTaskDetail.userImage_acceptedBy;
        self.lblUserName.text = myAppDelegate.workerTaskDetail.userName_acceptedBy;
        self.lblUserAddress.text = myAppDelegate.workerTaskDetail.locationName_acceptedBy;
        
        emailIdToSend = myAppDelegate.workerTaskDetail.emailId_acceptedBy;
        telToCall = myAppDelegate.workerTaskDetail.mobile_acceptedBy;
        locationToShow = myAppDelegate.workerTaskDetail.locationName_acceptedBy;
        
    }else{
        
        workerOrEmployer = @"Worker";
        
        noOfRating = myAppDelegate.workerTaskDetail.avgStars_createdBy;
        
        self.viewBottomHolder.backgroundColor = RGB(248, 248, 248);
        
        //other user information
        self.imgViewUserProfilePicture.image = myAppDelegate.workerTaskDetail.userImage_createdBy;
        self.lblUserName.text = myAppDelegate.workerTaskDetail.userName_createdBy;
        self.lblUserAddress.text = myAppDelegate.workerTaskDetail.locationName_createdBy;
        
        emailIdToSend = myAppDelegate.workerTaskDetail.emailId_createdBy;
        telToCall = myAppDelegate.workerTaskDetail.mobile_createdBy;
        locationToShow = myAppDelegate.workerTaskDetail.locationName_createdBy;
        
    }
    
    self.viewAcceptBtnWorkerHolder.hidden = true;
    self.viewYesNoBtnEmployerHolder.hidden = true;
    
    self.constraintViewTaskPaymentDetailHolderHeight.constant = 0;
    self.constraintViewRatingHolderHeight.constant = 0;
    
    self.viewPaymentDetailHolder.hidden = YES;
    self.viewRatingHolder.hidden = YES;
    self.btnAddRating.hidden = YES;
    
    if (myAppDelegate.workerTaskDetail.isTaskAccepted) {
        
        self.viewDoneBtnEmployerHolder.hidden = true;
        
        self.constraintViewUserAdditionalInformationHeight.constant = 63;
        self.constraintLblTaskCompletedHolderView.constant = 40;
        
        self.viewUserAdditionalInformation.hidden = NO;
        self.lblTaskCompleted.hidden = NO;
        
        if ([myAppDelegate.userData.userid isEqualToString:myAppDelegate.workerTaskDetail.userId_createdBy]){
            
            self.viewCompleteIncompleteBtnEmployerHolder.hidden = false;
            self.viewDoneBtnWorkerHolder.hidden = true;
            
        }else{
            
            self.viewCompleteIncompleteBtnEmployerHolder.hidden = true;
            self.viewDoneBtnWorkerHolder.hidden = false;
            
        }
        
    }else{
        
        self.viewCompleteIncompleteBtnEmployerHolder.hidden = true;
        self.viewDoneBtnEmployerHolder.hidden = false;
        self.viewDoneBtnWorkerHolder.hidden = true;
        
        self.constraintViewUserAdditionalInformationHeight.constant = 0;
        self.constraintLblTaskCompletedHolderView.constant = 0;
        
        self.viewUserAdditionalInformation.hidden = YES;
        self.lblTaskCompleted.hidden = YES;
        
        self.btnLocation.enabled = NO;
        self.btnMail.enabled = NO;
        self.btnContact.enabled = NO;
        
    }
    
    if(myAppDelegate.workerTaskDetail.taskDetailObj.taskImagesArray.count>0){
        
        _collectionViewHeight.constant=60;
        
    }else{
        _collectionViewHeight.constant=0;
        
    }
    
    
}

#pragma mark - handling of task active task detail data
-(void)handleWorkingOfTaskHistoryData{
    
    for (UIView *view in self.viewDoneBtnWorkerHolder.subviews) {
        if ([view isKindOfClass:[UIButton class]]) {
            UIButton *btn = (UIButton *)view;
            [btn setTitle:@"Done" forState:UIControlStateNormal];
        }
    }
    
    if (myAppDelegate.workerTaskDetail.isAlreadyRated) {
        self.btnAddRating.enabled = NO;
    }
    
    //task information
    self.lblTaskDescription.text = myAppDelegate.taskData.taskDescription;
    self.lblTaskName.text = myAppDelegate.taskData.title;
    
    if ([GlobalFunction checkIfContainFloat:myAppDelegate.taskData.price]) {
        self.lblTaskPrice.text = [NSString stringWithFormat:@"$%0.2f",myAppDelegate.taskData.price];
    }else{
        self.lblTaskPrice.text = [NSString stringWithFormat:@"$%0.0f",myAppDelegate.taskData.price];
    }
    
    //task date
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:dateFormatFull];
    NSDate *date = [dateFormatter dateFromString:myAppDelegate.taskData.startDate];
    [dateFormatter setDateFormat:dateFormatDate];
    NSString *dateString = [dateFormatter stringFromDate:date];
    self.lblTaskDate.text = dateString;
    
    
    //payment transfer date
    NSDateFormatter *dateFormatter2 = [[NSDateFormatter alloc]init];
    [dateFormatter2 setDateFormat:dateFormatFull];
    NSDate *date2 = [dateFormatter2 dateFromString:myAppDelegate.taskData.lastModifyTime];
    [dateFormatter2 setDateFormat:@"dd-MM-YY"];
    NSString *dateString2 = [dateFormatter2 stringFromDate:date2];
    
    for (int i = 0; i<myAppDelegate.arrayCategoryDetail.count; i++) {
        
        if ([[myAppDelegate.arrayCategoryDetail objectAtIndex:i] isKindOfClass:[subCategory class]]){
            
            subCategory *obj = [myAppDelegate.arrayCategoryDetail objectAtIndex:i];
            
            if ([obj.subCategoryId isEqualToString:myAppDelegate.workerTaskDetail.taskDetailObj.subcategoryId]) {
                
                self.lblAddress2.text = obj.name;
                
            }
            
        }
        
    }
    
    self.viewAcceptBtnWorkerHolder.hidden = true;
    self.viewCompleteIncompleteBtnEmployerHolder.hidden = true;
    self.viewYesNoBtnEmployerHolder.hidden = true;
    
    self.constraintViewTaskPaymentDetailHolderHeight.constant = 150;
    self.viewPaymentDetailHolder.hidden = NO;
    
    self.constraintViewUserAdditionalInformationHeight.constant = 63;
    self.constraintLblTaskCompletedHolderView.constant = 40;
    
    self.viewUserAdditionalInformation.hidden = NO;
    self.viewLblTaskCompletedHolder.hidden = NO;
    
    if ([myAppDelegate.userData.userid isEqualToString:myAppDelegate.workerTaskDetail.userId_createdBy]){
        
        workerOrEmployer = @"Employer";
        
        noOfRating = myAppDelegate.workerTaskDetail.avgStars_acceptedBy;
        
        self.viewDoneBtnEmployerHolder.hidden = false;
        self.viewDoneBtnWorkerHolder.hidden = true;
        
        self.constraintViewRatingHolderHeight.constant = 96;
        
        self.viewRatingHolder.hidden = NO;
        self.btnAddRating.hidden = NO;
        
        //other user information
        self.imgViewUserProfilePicture.image = myAppDelegate.workerTaskDetail.userImage_acceptedBy;
        self.lblUserName.text = myAppDelegate.workerTaskDetail.userName_acceptedBy;
        self.lblUserAddress.text = myAppDelegate.workerTaskDetail.locationName_acceptedBy;
        
        emailIdToSend = myAppDelegate.workerTaskDetail.emailId_acceptedBy;
        telToCall = myAppDelegate.workerTaskDetail.mobile_acceptedBy;
        locationToShow  = myAppDelegate.workerTaskDetail.locationName_acceptedBy;
        
        //task information
        self.lblCreditCardEndingIn.text = @"Credit Card Ending in:";
        self.lblCreditCardLast4DigitHolder.text = [myAppDelegate.workerTaskDetail.cardDetail substringWithRange:NSMakeRange(12, 4)];
        self.lblPaymentSentDate.text = dateString2;
        
    }else{
        
        workerOrEmployer = @"Worker";
        
        noOfRating = myAppDelegate.workerTaskDetail.avgStars_createdBy;
        
        self.viewDoneBtnEmployerHolder.hidden = true;
        self.viewDoneBtnWorkerHolder.hidden = false;
        
        self.constraintViewRatingHolderHeight.constant = 0;
        
        self.viewRatingHolder.hidden = YES;
        self.btnAddRating.hidden = YES;
        
        self.viewBottomHolder.backgroundColor = RGB(248, 248, 248);
        
        //other user information
        self.imgViewUserProfilePicture.image = myAppDelegate.workerTaskDetail.userImage_createdBy;
        self.lblUserName.text = myAppDelegate.workerTaskDetail.userName_createdBy;
        self.lblUserAddress.text = myAppDelegate.workerTaskDetail.locationName_createdBy;
        
        emailIdToSend = myAppDelegate.workerTaskDetail.emailId_createdBy;
        telToCall = myAppDelegate.workerTaskDetail.mobile_createdBy;
        locationToShow  = myAppDelegate.workerTaskDetail.locationName_createdBy;
        
        if ([myAppDelegate.userData.userid isEqualToString:myAppDelegate.workerTaskDetail.userId_acceptedBy]) {
            
            //task information
            self.lblCreditCardEndingIn.text = @"Credited to your account:";
            self.lblCreditCardLast4DigitHolder.text = [NSString stringWithFormat:@"$%0.2f (Processing fees will apply)",myAppDelegate.workerTaskDetail.taskDetailObj.price];
            self.lblPaymentSentDate.text = dateString2;
            
        }else{
            
            self.viewPaymentDetailHolder.hidden = YES;
            self.constraintViewTaskPaymentDetailHolderHeight.constant = 0;
            
        }
        
    }
    
    if(myAppDelegate.workerTaskDetail.taskDetailObj.taskImagesArray.count>0){
        
        _collectionViewHeight.constant=60;
        
    }else{
        _collectionViewHeight.constant=0;
        
    }
    
}
-(void)removeSliderImageWithAnimation
{
    _sliderImageViewHeight.constant=0;
    
}


#pragma mark - handling of task active task detail data
-(void)handleWorkingOfViewAllTaskData{
    
    //task information
    self.lblTaskDescription.text = myAppDelegate.taskData.taskDescription;
    self.lblTaskName.text = myAppDelegate.taskData.title;
    
    //byKP    //************************************
    
    
    
//    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
//    [dateFormatter setDateFormat:dateFormatFull];
//    NSDate *date = [dateFormatter dateFromString:myAppDelegate.workerAllTaskDetail.taskDetailObj.startDate];
//    [dateFormatter setDateFormat:dateFormatDate];
//    NSString *dateString = [dateFormatter stringFromDate:date];
//    
//    
//    NSString *timeString = [GlobalFunction convertTimeFormat:myAppDelegate.workerAllTaskDetail.taskDetailObj.endDate];
//    
//    NSLog(@"time string is....%@",timeString);
//    
//    //        NSString *date =  self.objelements.date; taskObj.startDate
//    dateString = [GlobalFunction convertDateFormat:dateString];
//    
//    NSString *combineString = dateString;
//    //  combineString = [combineString stringByAppendingString:timeString];
//    NSString *str = [@[dateString, timeString] componentsJoinedByString:@" at "];
    
  //  _timeViewDateTimeLabel.text=str;

    
    NSLog(@"date from server is...%@",myAppDelegate.workerAllTaskDetail.taskDetailObj.startDate);
    NSLog(@"time from server is...%@",myAppDelegate.workerAllTaskDetail.taskDetailObj.endDate);
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:dateFormatFull];
    
    NSString *timeString = @"";
    NSString *dateString = @"";
    
    if ([myAppDelegate.workerAllTaskDetail.taskDetailObj.startDate isEqualToString:@"AnyDate"])
    {
        dateString = @"AnyDate";
    }
    else
    {
        NSDate *date = [dateFormatter dateFromString:myAppDelegate.workerAllTaskDetail.taskDetailObj.startDate];
        [dateFormatter setDateFormat:dateFormatDate];
        dateString = [dateFormatter stringFromDate:date];
        
        dateString = [GlobalFunction convertDateFormat:dateString];
    }
    
    if ([myAppDelegate.workerAllTaskDetail.taskDetailObj.endDate isEqualToString:@"AnyTime"])
    {
        timeString = @"AnyTime";
    }
    else
    {
        timeString = [GlobalFunction convertTimeFormat:myAppDelegate.workerAllTaskDetail.taskDetailObj.endDate];
        

    }
    
    NSLog(@"Date string is....%@",dateString);
    NSLog(@"time string is....%@",timeString);


    
    NSString *str = [@[dateString, timeString] componentsJoinedByString:@" at "];
    _timeViewDateTimeLabel.text=str;

    
    NSLog(@"%@",myAppDelegate.workerAllTaskDetail.taskDetailObj.taskImagesArray);
    NSLog(@"%@",myAppDelegate.workerAllTaskDetail.taskDetailObj.title);
    NSLog(@"%@",myAppDelegate.workerAllTaskDetail.taskDetailObj.startDate);
    NSLog(@"%@",myAppDelegate.workerAllTaskDetail.taskDetailObj.lastModifyTime);   //store date and time both....
    
    
    NSLog(@"%@",myAppDelegate.workerAllTaskDetail.taskDetailObj.taskDescription);
    NSLog(@"%f",myAppDelegate.workerAllTaskDetail.taskDetailObj.price);
    NSLog(@"%@ avgStars_createdBy---------------- ",myAppDelegate.workerAllTaskDetail.avg_rating_createdBy);
    
    
    if(myAppDelegate.workerAllTaskDetail.taskDetailObj.taskImagesArray.count==0)
    {
        _activityIndicator.hidden=YES;
        
    }
    
    //************************************
    
    NSLog(@"main discount value is...%@",myAppDelegate.workerAllTaskDetail.taskDetailObj.discountValue);
     NSLog(@"discount Discription value is...%@",myAppDelegate.workerAllTaskDetail.taskDetailObj.discountDiscription);
     NSLog(@"main discount value is...%f",myAppDelegate.workerAllTaskDetail.taskDetailObj.price);
    
    
    NSString *fees = @"";
    fees = myAppDelegate.workerAllTaskDetail.taskDetailObj.feeValue;
    
    NSString *priceVal;
    if ([GlobalFunction checkIfContainFloat:myAppDelegate.workerAllTaskDetail.taskDetailObj.price]) {
        priceVal=  [NSString stringWithFormat: @"%0.2f", myAppDelegate.workerAllTaskDetail.taskDetailObj.price];
        
    }else{
        priceVal=  [NSString stringWithFormat: @"%0.0f", myAppDelegate.workerAllTaskDetail.taskDetailObj.price];
    }

    int price = [priceVal intValue];
    int fee = [fees intValue];
    
    
    NSString *CalculatedString = [NSString stringWithFormat:@"%d", price-fee];
    CalculatedString = [@"$" stringByAppendingString:CalculatedString];

    
    NSString *prefixVal =@"$";
    NSString* finalPriceVal = [prefixVal stringByAppendingString:priceVal];
    
    
    
    discVal= [myAppDelegate.workerAllTaskDetail.taskDetailObj.discountValue intValue];
    int mainVal=myAppDelegate.workerAllTaskDetail.taskDetailObj.price;
    NSString *mainPrice=[NSString stringWithFormat:@"%d",mainVal-discVal];
    NSString *prefixVal11 =@"$";
    mainPrice = [prefixVal11 stringByAppendingString:mainPrice];


    if(discVal<=0)
    {
        if ([fees isEqualToString:@"0"])
        {
            
            _timeViewPriceLabel.textColor=[UIColor blackColor];
            _timeViewPriceLabel.text=finalPriceVal;
            
        }
        else
        {
            NSString *first = @"(+$";
            
            first = [first stringByAppendingString:fees];
            
            NSString *main = [first stringByAppendingString:@" fee)"];
            
            NSString *str1 = [@[CalculatedString, main]componentsJoinedByString:@" "];
            
            NSRange startRange = [str1 rangeOfString:@"("];
            NSRange endRange = NSMakeRange(startRange.location, str1.length  - startRange.location);
            
            NSMutableAttributedString * string = [[NSMutableAttributedString alloc] initWithString:str1];
            //  [string addAttribute:NSFontAttributeName
            //                 value:[UIFont fontWithName:@"Lato-Regular" size:10] range:endRange];
            
            
            
            //Red and large
            [string setAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"Lato-Regular" size:9.0f], NSForegroundColorAttributeName:[UIColor colorWithRed:63.0/255.0f green:67.0/255.0f blue:70.0/255.0f alpha:0.9]} range:endRange];
            
            //Rest of text -- just futura
            //   [hintText setAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"Futura-Medium" size:16.0f]} range:NSMakeRange(20, hintText.length - 20)];
            
            
            
            _timeViewPriceLabel.attributedText=string;

        }
        
    }
    
    else{
        _timeViewPriceLabel.textColor=[UIColor colorWithRed:214.0/255.0f green:52.0/255.0f blue:47.0/255.0f alpha:1.0];
      _timeViewPriceLabel.text=  [NSString stringWithFormat:@"$%@ off",myAppDelegate.workerAllTaskDetail.taskDetailObj.discountValue];
        
    }
    
    NSString *headingLabelText = [myAppDelegate.workerAllTaskDetail.taskDetailObj.title uppercaseString];
    
    
    _timeViewHeadingLabel.text=headingLabelText;
   // _timeViewDetailLabel.text=myAppDelegate.workerAllTaskDetail.taskDetailObj.taskDescription; // by vs 9 oct 17...
    
    
    _postedByViewNameLabel.text=myAppDelegate.workerAllTaskDetail.name_createdBy;
    
    _addressViewHeadingLabel.text = myAppDelegate.workerAllTaskDetail.name_Business;
    _addressViewAddressLabel.text = myAppDelegate.workerAllTaskDetail.address_Business;
    _addressViewDetailLabel.text = myAppDelegate.workerAllTaskDetail.services_Business;
    
    
    //*******************************************

    
    //hyper link work done by KP
    
    
   // myAppDelegate.workerAllTaskDetail.instaLink_Business=@"vs_suryawanshi";//@"https://www.instagram.com/vs_suryawanshi"
    
    if(myAppDelegate.workerAllTaskDetail.instaLink_Business.length<=0)
    {
        _addressViewURLlbl.text=myAppDelegate.workerAllTaskDetail.instaLink_Business;
    }
    else
    {
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:myAppDelegate.workerAllTaskDetail.instaLink_Business attributes:nil];
    NSRange linkRange = NSMakeRange(0, [myAppDelegate.workerAllTaskDetail.instaLink_Business length]); // for the word "link" in the string above
    NSDictionary *linkAttributes = @{ NSForegroundColorAttributeName : [UIColor colorWithRed:0.05 green:0.4 blue:0.65 alpha:1.0],
                                      NSUnderlineStyleAttributeName : @(NSUnderlineStyleSingle) };
    [attributedString setAttributes:linkAttributes range:linkRange];
    _addressViewURLlbl.attributedText = attributedString;
    
    
    _addressViewURLlbl.userInteractionEnabled = YES;
    [_addressViewURLlbl addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapOnLabel:)]];
        
    }

    //_addressViewURLlbl.text=myAppDelegate.workerAllTaskDetail.instaLink_Business;

    
    //*******************************************
    
    NSString *startBrace=@"  (";
    NSString *priceValWithBrace;
    NSString* buyNowBtnTitleWithPrice;
    if(discVal<=0)
    {
        priceValWithBrace = [startBrace stringByAppendingString:finalPriceVal];
        NSString *endBrace=@")";
        NSString *priceValWithBothBrace = [priceValWithBrace stringByAppendingString:endBrace];
        NSString *buyNowBtnTitle=@"Buy Now";
         buyNowBtnTitleWithPrice = [buyNowBtnTitle stringByAppendingString:priceValWithBothBrace];
        

    }
    else{
        // priceValWithBrace = [startBrace stringByAppendingString:mainPrice];
       // priceValWithBrace = [startBrace stringByAppendingString:@"$0"];
        
//            buyNowBtnTitleWithPrice = @"Buy Now";
        
       if(myAppDelegate.workerAllTaskDetail.purchaseID)
       {
           buyNowBtnTitleWithPrice = @"Reserved Discount";
       }
        else
        {
            buyNowBtnTitleWithPrice = @"Reserve Discount";
        }
    }
    
    if ([myAppDelegate.workerAllTaskDetail.taskDetailObj.soldStatus isEqualToString:@"1"])
    {
        [_buyNowBtn setTitle:@"SOLD OUT" forState:UIControlStateNormal];
        _buyNowBtn.userInteractionEnabled = NO;
    }
    else if ([myAppDelegate.workerAllTaskDetail.taskDetailObj.soldStatus isEqualToString:@"2"])
    {
        [_buyNowBtn setTitle:@"Not Available" forState:UIControlStateNormal];
        _buyNowBtn.userInteractionEnabled = NO;
    }
    else
    {
        [_buyNowBtn setTitle:buyNowBtnTitleWithPrice forState:UIControlStateNormal];
        if(myAppDelegate.workerAllTaskDetail.purchaseID)
        {
            _buyNowBtn.userInteractionEnabled = NO;
        }
        else
        {
            _buyNowBtn.userInteractionEnabled = YES;
        }
        
    }

    //******************************

    
    //******************************
    
    
   NSString *businessDescriptionText=myAppDelegate.workerAllTaskDetail.services_Business;
    
    
    self.addressViewDetailLabel.numberOfLines = 0;
    
    CGSize maximumLabelSize = CGSizeMake(350,9999);
    CGSize expectedLabelSize = [businessDescriptionText sizeWithFont:_addressViewDetailLabel.font
                                                   constrainedToSize:maximumLabelSize
                                                       lineBreakMode:_addressViewDetailLabel.lineBreakMode];
    
    CGRect newFrame;
    
    
    if(IS_IPHONE_6)
    {
        if(expectedLabelSize.height<=72)
        {
            
            NSLog(@"less then 72...");
            _secondAddressMentionViewHeight.constant=147;
        }
        
        else
        {
            newFrame = _addressViewDetailLabel.frame;
            newFrame.size.height = expectedLabelSize.height+20;
            NSLog(@"time frame ----- %f",newFrame.size.height);
            NSLog(@"address detial.....%@",_addressViewDetailLabel.text);
            _secondAddressMentionViewHeight.constant=newFrame.size.height + _addressViewDetailLabel.frame.origin.y;
        }
    }
    
    
    else if(IS_IPHONE_6P)
    {
        
        if(expectedLabelSize.height<=92)
        {
            
            NSLog(@"less then 92...");
            _secondAddressMentionViewHeight.constant=161;
        }
        
        else
        {
            newFrame = _addressViewDetailLabel.frame;
            newFrame.size.height = expectedLabelSize.height+15;
            NSLog(@"time frame ----- %f",newFrame.size.height);
            NSLog(@"address detial.....%@",_addressViewDetailLabel.text);
            _secondAddressMentionViewHeight.constant=newFrame.size.height + _addressViewDetailLabel.frame.origin.y;
        }
        
        
    }
    
    
    else if(IS_IPHONE_5)
    {
        
        if(expectedLabelSize.height<=65)
        {
            NSLog(@"less then 65...");
            _secondAddressMentionViewHeight.constant=125;
        }
        
        else
        {
            newFrame = _addressViewDetailLabel.frame;
            newFrame.size.height = expectedLabelSize.height+20;
            NSLog(@"time frame ----- %f",newFrame.size.height);
            NSLog(@"address detial.....%@",_addressViewDetailLabel.text);
            _secondAddressMentionViewHeight.constant=newFrame.size.height + _addressViewDetailLabel.frame.origin.y+28;
        }
        
    }
    
    
    else if (IS_IPHONE_4_OR_LESS)
    {
        if(expectedLabelSize.height<=65)
        {
            NSLog(@"less then 65...");
            _secondAddressMentionViewHeight.constant=125;
        }
        
        else
        {
            newFrame = _addressViewDetailLabel.frame;
            newFrame.size.height = expectedLabelSize.height+20;
            NSLog(@"time frame ----- %f",newFrame.size.height);
            NSLog(@"address detial.....%@",_addressViewDetailLabel.text);
            _secondAddressMentionViewHeight.constant=newFrame.size.height + _addressViewDetailLabel.frame.origin.y+33;
        }

    }
    
    _addressViewDetailLabel.text=businessDescriptionText;
    
    //******************************
    
    NSString *discountDescriptonText=myAppDelegate.workerAllTaskDetail.taskDetailObj.discountDiscription;


    self.firstViewDiscountDescriptionLbl.numberOfLines = 0;
    CGSize maximumLabelSizeDisc = CGSizeMake(350,9999);
    CGSize expectedLabelSizeDisc = [discountDescriptonText sizeWithFont:self.firstViewDiscountDescriptionLbl.font
                                                      constrainedToSize:maximumLabelSizeDisc
                                                          lineBreakMode:self.firstViewDiscountDescriptionLbl.lineBreakMode];
    
    CGRect newFrameDisc;
    newFrameDisc = self.firstViewDiscountDescriptionLbl.frame;
    
    if(expectedLabelSizeDisc.height<=18)
    {
        newFrameDisc.size.height = expectedLabelSizeDisc.height+23;
    }
    
    else
    {
        if(IS_IPHONE_6P)
            {
                newFrameDisc.size.height = expectedLabelSizeDisc.height+28;
            }
            
            
            else
            {
                newFrameDisc.size.height = expectedLabelSizeDisc.height+33;
            }
    }
    
    
    NSLog(@"time frame ----- %f",newFrameDisc.size.height);
    NSLog(@"address detial.....%@",self.firstViewDiscountDescriptionLbl.text);

    
    //*****************************
    
    NSString *detailText=myAppDelegate.workerAllTaskDetail.taskDetailObj.taskDescription;

    CGSize maximumLabelSize1 = CGSizeMake(350,9999);
    CGSize expectedLabelSize1 = [detailText sizeWithFont:_timeViewDetailLabel.font
                                       constrainedToSize:maximumLabelSize1
                                           lineBreakMode:_timeViewDetailLabel.lineBreakMode];
    NSLog(@"%f",expectedLabelSize1.height);
    NSLog(@"%f",self.firstTimeMentionView.frame.size.height);
    
    CGRect newFrame1;
    
    [self.view layoutIfNeeded];
    
    
    
    if(IS_IPHONE_6)
    {
        
//        if(expectedLabelSize1.height<=56)
//        {
//            NSLog(@"less then 56...");
//            _firstTimeMentionViewHeight.constant=112;
//            
//        }else{
            newFrame1 = _timeViewDetailLabel.frame;
            newFrame1.size.height = expectedLabelSize1.height+30;
            NSLog(@"address frame ----- %f",newFrame1.size.height);
        
            NSLog(@"disc frame ----- %f",newFrameDisc.size.height);
            NSLog(@"%@",discountDescriptonText);

        
        
        
//            _firstTimeMentionViewHeight.constant=newFrame1.size.height +_timeViewDetailLabel.frame.origin.y+newFrameDisc.size.height ;
        
        
        _firstTimeMentionViewHeight.constant=newFrame1.size.height +_firstViewDiscountDescriptionLbl.frame.origin.y+newFrameDisc.size.height;

      //  }
        
    }
    
    
    
    else if(IS_IPHONE_6P)
    {
        
//        if(expectedLabelSize1.height<=57)
//        {
//            NSLog(@"less then 57...");
//            _firstTimeMentionViewHeight.constant=122;
//            
//        }else{
            newFrame1 = _timeViewDetailLabel.frame;
        
        if(expectedLabelSize1.height<=16)
        {
            newFrame1.size.height = expectedLabelSize1.height+12;
        }
        else
        {
            newFrame1.size.height = expectedLabelSize1.height+17;
        }

        NSLog(@"address frame ----- %f",newFrame1.size.height);
        
        NSLog(@"%f",newFrame1.size.height);
        NSLog(@"%f",newFrameDisc.size.height);
        NSLog(@"%f",_timeViewDetailLabel.frame.origin.y);

        
            _firstTimeMentionViewHeight.constant=newFrame1.size.height+_firstViewDiscountDescriptionLbl.frame.origin.y +newFrameDisc.size.height;
      //  }

    }
    
    
    
    if(IS_IPHONE5  || IS_IPHONE_4_OR_LESS)
    {
        
        
//        if(expectedLabelSize1.height<=51)
//        {
//            NSLog(@"less then 51...");
//            _firstTimeMentionViewHeight.constant=97;
//            
//        }else{
        
        
            newFrame1 = _timeViewDetailLabel.frame;
        
           if(expectedLabelSize1.height<=16)
           {
               newFrame1.size.height = expectedLabelSize1.height+20;
           }
              else
           {
            newFrame1.size.height = expectedLabelSize1.height+30;
           }
        
            NSLog(@"address frame ----- %f",newFrame1.size.height);
            
            if (myAppDelegate.workerAllTaskDetail.taskDetailObj.taskImagesArray.count<=0)
            {
                _firstTimeMentionViewHeight.constant=newFrame1.size.height+_firstViewDiscountDescriptionLbl.frame.origin.y+newFrameDisc.size.height +25;
            }

            else{
                _firstTimeMentionViewHeight.constant=newFrame1.size.height+_firstViewDiscountDescriptionLbl.frame.origin.y+newFrameDisc.size.height +20;
            }
            
        
            
        //}
        
    }

    if(discVal <= 0)
    {
        
        if(detailText.length > 0)
        {
        
        NSMutableAttributedString *signUpString = [[NSMutableAttributedString alloc] initWithString:@"Description:"];
        
        detailText = [NSString stringWithFormat:@"%@%@",@" ",detailText];
        
        [signUpString appendAttributedString:[[NSAttributedString alloc] initWithString:detailText
                                                                             attributes:@{NSUnderlineStyleAttributeName: @(NSUnderlineStyleNone)}]];
        
        [signUpString addAttributes: @{NSUnderlineStyleAttributeName:[NSNumber numberWithInteger:NSUnderlineStyleSingle]} range:NSMakeRange(0,11)];
        
        UIFont *font_regular=[UIFont fontWithName:@"Lato-Regular" size:15.0f];
        
        [signUpString addAttribute:NSFontAttributeName value:font_regular range:NSMakeRange(0,12)];
        
        
        self.timeViewDetailLabel.attributedText = [signUpString copy];

        
       // _timeViewDetailLabel.text=detailText;
        
        
        
        
        self.firstViewDiscountDescriptionLbl.text=discountDescriptonText;
            
        }
    }
    else
    {
        if(detailText.length > 0)
        {

        NSMutableAttributedString *signUpString = [[NSMutableAttributedString alloc] initWithString:@"Description:"];
        
        detailText = [NSString stringWithFormat:@"%@%@",@" ",detailText];
        
        [signUpString appendAttributedString:[[NSAttributedString alloc] initWithString:detailText
                                                                             attributes:@{NSUnderlineStyleAttributeName: @(NSUnderlineStyleNone)}]];
        
        [signUpString addAttributes: @{NSUnderlineStyleAttributeName:[NSNumber numberWithInteger:NSUnderlineStyleSingle]} range:NSMakeRange(0,11)];
        
        UIFont *font_regular=[UIFont fontWithName:@"Lato-Regular" size:15.0f];
      
        
        [signUpString addAttribute:NSFontAttributeName value:font_regular range:NSMakeRange(0,12)];

        
        self.firstViewDiscountDescriptionLbl.attributedText = [signUpString copy];
        
        
        }
        
        if (discountDescriptonText.length > 0) {
            
       
        NSMutableAttributedString *signUpString1 = [[NSMutableAttributedString alloc] initWithString:@"Terms and conditions:"];
        
        discountDescriptonText = [NSString stringWithFormat:@"%@%@",@" ",discountDescriptonText];
        
    [signUpString1 appendAttributedString:[[NSAttributedString alloc] initWithString:discountDescriptonText
                                                                             attributes:@{NSUnderlineStyleAttributeName: @(NSUnderlineStyleNone)}]];

        
        [signUpString1 addAttributes: @{NSUnderlineStyleAttributeName:[NSNumber numberWithInteger:NSUnderlineStyleSingle]} range:NSMakeRange(0,21)];

        
            
            
//        NSString *infoString =@"I am Kirit Modi from Deesa.";
//        
//        NSMutableAttributedString *attString=[[NSMutableAttributedString alloc] initWithString:infoString];
        
        UIFont *font_regular1=[UIFont fontWithName:@"Lato-Regular" size:15.0f];
       // UIFont *font_bold=[UIFont fontWithName:@"Helvetica-Bold" size:20.0f];
        
//        [signUpString1 addAttribute:NSFontAttributeName value:font_regular range:NSMakeRange(0, 4)];
//        
//        [signUpString1 addAttribute:NSFontAttributeName value:font_bold range:NSMakeRange(5, 15)];
        
        [signUpString1 addAttribute:NSFontAttributeName value:font_regular1 range:NSMakeRange(0,22)];
        
       
       
        _timeViewDetailLabel.attributedText =  [signUpString1 copy];
        //self.firstViewDiscountDescriptionLbl.text=detailText; NSUnderlineStyleNone
    }
    
    }
    
    if(IS_IPHONE_6)
    {
        if(expectedLabelSize.height<=72 && expectedLabelSize1.height<=56 && expectedLabelSizeDisc.height<=5)
        {
            _scrollViewHolder.scrollEnabled=NO;
        }
        else{
            if (myAppDelegate.workerAllTaskDetail.taskDetailObj.taskImagesArray.count<=0)
            {
                NSInteger totalViewHeightExceptButton;
                NSInteger totalExpectedLabelSizeHeight;
                totalViewHeightExceptButton=self.view.frame.size.height-_buyNowBtn.frame.size.height;
                totalExpectedLabelSizeHeight=expectedLabelSize.height+ expectedLabelSize1.height+expectedLabelSizeDisc.height +_sliderImageView.frame.size.height+450;;
                NSLog(@"totalViewHeightExceptButton---- %ld",(long)totalViewHeightExceptButton);
                NSLog(@"totalExpectedLabelSizeHeight---- %ld",(long)totalExpectedLabelSizeHeight);
                
                if(totalExpectedLabelSizeHeight < totalViewHeightExceptButton)
                {
                    NSLog(@"SCROLL NOT ENABLED.....");
                    _scrollViewHolder.scrollEnabled=NO;
                }
                else
                {
                    NSLog(@"SCROLL ENABLED.....");
                    
                    
                    if(self.firstViewDiscountDescriptionLbl.text.length==0)
                    {
                        _viewTopHolderHeight.constant= expectedLabelSize.height+ expectedLabelSize1.height+500;
 
                    }
                    else{
                    _viewTopHolderHeight.constant= expectedLabelSize.height+ expectedLabelSize1.height+expectedLabelSizeDisc.height+500;
                    }
                }
            }
            else
            {
                
                _viewTopHolderHeight.constant= expectedLabelSize.height+ expectedLabelSize1.height+expectedLabelSizeDisc.height + _sliderImageView.frame.size.height +530;
                
            }
            
        }
    }
    
    
    else  if(IS_IPHONE_6P)
    {
        if(expectedLabelSize.height<=92 && expectedLabelSize1.height<=57 && expectedLabelSizeDisc.height<=5){
            _scrollViewHolder.scrollEnabled=NO;
        }
        else{
            
            
            
            if (myAppDelegate.workerAllTaskDetail.taskDetailObj.taskImagesArray.count<=0)
            {
                NSInteger totalViewHeightExceptButton;
                NSInteger totalExpectedLabelSizeHeight;
                totalViewHeightExceptButton=self.view.frame.size.height-_buyNowBtn.frame.size.height;
                totalExpectedLabelSizeHeight=expectedLabelSize.height+ expectedLabelSize1.height+expectedLabelSizeDisc.height +_sliderImageView.frame.size.height+520;;
                NSLog(@"totalViewHeightExceptButton---- %ld",(long)totalViewHeightExceptButton);
                NSLog(@"totalExpectedLabelSizeHeight---- %ld",(long)totalExpectedLabelSizeHeight);
                
                if(totalExpectedLabelSizeHeight < totalViewHeightExceptButton)
                {
                    NSLog(@"SCROLL NOT ENABLED.....");
                    _scrollViewHolder.scrollEnabled=NO;
                }
                else
                {
                    NSLog(@"SCROLL ENABLED.....");
                    
                    if(_firstViewDiscountDescriptionLbl.text.length==0)
                    {
                        _viewTopHolderHeight.constant= expectedLabelSize.height+ expectedLabelSize1.height+550;

                        
                    }
                    
                    else{
                        _viewTopHolderHeight.constant= expectedLabelSize.height+ expectedLabelSize1.height+expectedLabelSizeDisc.height+550;

                    }
                    
                    
                }
            }
            
            else{
                _viewTopHolderHeight.constant= expectedLabelSize.height+ expectedLabelSize1.height +expectedLabelSizeDisc.height + _sliderImageView.frame.size.height +550;
                
            }
            
            
        }
    }
    
    
    else  if(IS_IPHONE_5 || IS_IPHONE_4_OR_LESS)
    {
        if(expectedLabelSize.height<=65 && expectedLabelSize1.height<=51 && expectedLabelSizeDisc.height<=5){
            
            
            if(IS_IPHONE_4_OR_LESS)
            {
                _scrollViewHolder.scrollEnabled=YES;
                _viewTopHolderHeight.constant= expectedLabelSize.height+ expectedLabelSize1.height+expectedLabelSizeDisc.height+500;
            }
            
            else
            {
                _scrollViewHolder.scrollEnabled=NO;
            }
        }
        else{
            
            
            if (myAppDelegate.workerAllTaskDetail.taskDetailObj.taskImagesArray.count<=0)
            {
                NSInteger totalViewHeightExceptButton;
                NSInteger totalExpectedLabelSizeHeight;
                totalViewHeightExceptButton=self.view.frame.size.height-_buyNowBtn.frame.size.height;
                totalExpectedLabelSizeHeight=expectedLabelSize.height+ expectedLabelSize1.height+expectedLabelSizeDisc.height +_sliderImageView.frame.size.height+480;
                NSLog(@"totalViewHeightExceptButton---- %ld",(long)totalViewHeightExceptButton);
                NSLog(@"totalExpectedLabelSizeHeight---- %ld",(long)totalExpectedLabelSizeHeight);
                
                if(totalExpectedLabelSizeHeight < totalViewHeightExceptButton)
                {
                    NSLog(@"SCROLL NOT ENABLED.....");
                    _scrollViewHolder.scrollEnabled=NO;
                }
                else
                {
                    if(_firstViewDiscountDescriptionLbl.text.length==0)
                    {
                        NSLog(@"SCROLL ENABLED.....");
                        _viewTopHolderHeight.constant= expectedLabelSize.height+ expectedLabelSize1.height+500;

                    }
                    
                    else{
                        NSLog(@"SCROLL ENABLED.....");
                        _viewTopHolderHeight.constant= expectedLabelSize.height+ expectedLabelSize1.height+expectedLabelSizeDisc.height+500;

                    }
                    
                }
            }
            
            else{
                _viewTopHolderHeight.constant= expectedLabelSize.height+ expectedLabelSize1.height+expectedLabelSizeDisc.height + _sliderImageView.frame.size.height +480;
            }
        }
    }
    
    

    
    
    [self.view layoutIfNeeded];
    NSString *starVal=  [NSString stringWithFormat: @"%@", myAppDelegate.workerAllTaskDetail.avg_rating_createdBy];
    
    if([starVal isEqualToString:@"0"]){
        _star1.image=[UIImage imageNamed:@"greystar"];
        _star2.image=[UIImage imageNamed:@"greystar"];
        _star3.image=[UIImage imageNamed:@"greystar"];
        _star4.image=[UIImage imageNamed:@"greystar"];
        _star5.image=[UIImage imageNamed:@"greystar"];
    }
    else if([starVal isEqualToString:@"1"]){
        _star1.image=[UIImage imageNamed:@"redstar"];
        _star2.image=[UIImage imageNamed:@"greystar"];
        _star3.image=[UIImage imageNamed:@"greystar"];
        _star4.image=[UIImage imageNamed:@"greystar"];
        _star5.image=[UIImage imageNamed:@"greystar"];
        
    }
    else if([starVal isEqualToString:@"2"]){
        _star1.image=[UIImage imageNamed:@"redstar"];
        _star2.image=[UIImage imageNamed:@"redstar"];
        _star3.image=[UIImage imageNamed:@"greystar"];
        _star4.image=[UIImage imageNamed:@"greystar"];
        _star5.image=[UIImage imageNamed:@"greystar"];
        
    }
    else if([starVal isEqualToString:@"3"]){
        _star1.image=[UIImage imageNamed:@"redstar"];
        _star2.image=[UIImage imageNamed:@"redstar"];
        _star3.image=[UIImage imageNamed:@"redstar"];
        _star4.image=[UIImage imageNamed:@"greystar"];
        _star5.image=[UIImage imageNamed:@"greystar"];
        
    }
    else if([starVal isEqualToString:@"4"]){
        _star1.image=[UIImage imageNamed:@"redstar"];
        _star2.image=[UIImage imageNamed:@"redstar"];
        _star3.image=[UIImage imageNamed:@"redstar"];
        _star4.image=[UIImage imageNamed:@"redstar"];
        _star5.image=[UIImage imageNamed:@"greystar"];
        
    }
    else if([starVal isEqualToString:@"5"]){
        _star1.image=[UIImage imageNamed:@"redstar"];
        _star2.image=[UIImage imageNamed:@"redstar"];
        _star3.image=[UIImage imageNamed:@"redstar"];
        _star4.image=[UIImage imageNamed:@"redstar"];
        _star5.image=[UIImage imageNamed:@"redstar"];
        
    }
    
    NSLog(@"%@",myAppDelegate.workerAllTaskDetail.picPath_createdBy);
    
    
    if([myAppDelegate.workerAllTaskDetail.picPath_createdBy isEqualToString:@""])
    {
        NSLog(@"image not exist....");
        _postedByViewUserImage.image=imageUserDefault;
        
        
        
    }
    
    else
    {
        
        
        // NSString *prefixURL=@"http://192.168.5.134/buytimee_web/";
        //  NSString *finalURL=[prefixURL stringByAppendingString:myAppDelegate.workerAllTaskDetail.picPath_createdBy];
        
        NSString *fullPath=myAppDelegate.workerAllTaskDetail.picPath_createdBy;
        NSArray *parts = [fullPath componentsSeparatedByString:@"/"];
        NSString *imageName=[parts lastObject];
        NSArray *imageNameParts = [imageName componentsSeparatedByString:@"."];
        NSString *userImageName = [NSString stringWithFormat:@"%@",[imageNameParts firstObject]];
        NSString *userImageNameWithJPEG = [NSString stringWithFormat:@"%@.jpeg",[imageNameParts firstObject]];
        UIImage *img = [GlobalFunction loadImageOfLogoFromCacheFolder:userImageNameWithJPEG dirname:@"taskImages"];
        
        if (img)
        {
            _postedByViewUserImage.image = img;
            
        }
        else{
            
            NSString *UrlStr =fullPath;
            NSString* webStringURL = [UrlStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            
            
            
            //  NSString *fullPath=[NSString stringWithFormat:urlServer,mainPic];
            
            UIImage *img11 = [GlobalFunction getImageFromURL:webStringURL];
            
            if (img11) {
                
                _postedByViewUserImage.image = img11;
                [GlobalFunction saveimage:img11 imageName:userImageName dirname:@"taskImages"];
            }
            else
            {
                _postedByViewUserImage.image=imageUserDefault;
            }
            //
            //            dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH,  0ul);
            //            dispatch_async(queue, ^{
            //                //This is what you will load lazily
            //                NSURL *imageURL=[NSURL URLWithString:webStringURL];
            //
            //                NSData *images=[NSData dataWithContentsOfURL:imageURL];
            //
            //                UIImage *img = [UIImage imageWithData:images];
            //
            //                if (img) {
            //
            //                    [GlobalFunction saveimage:img imageName:userImageName dirname:@"taskImages"];
            //                    dispatch_sync(dispatch_get_main_queue(), ^{
            //                        _postedByViewUserImage.image = img;
            //                        if (!self.viewImageViewAndActivityHolder.hidden) {
            //                            _postedByViewUserImage.image = img;
            //                            self.activityIndicatorTaskDetailImages.hidden = YES;
            //                            [self.activityIndicatorTaskDetailImages stopAnimating];
            //                        }
            //                    });
            //                }
            //            });
        }
        
        
    }
    
    [GlobalFunction removeIndicatorView];
    
    
  //  [self downloadSliderImages];
    
    //  [self performSelector:@selector(downloadSliderImages) withObject:nil afterDelay:0.3];
    
    
    
    return;
    
    
    
    
    
    //************************************
    
    
    
    if ([GlobalFunction checkIfContainFloat:myAppDelegate.taskData.price]) {
        self.lblTaskPrice.text = [NSString stringWithFormat:@"$%0.2f",myAppDelegate.taskData.price];
    }else{
        self.lblTaskPrice.text = [NSString stringWithFormat:@"$%0.0f",myAppDelegate.taskData.price];
    }
    
    
    NSDateFormatter *dateFormatter11= [[NSDateFormatter alloc]init];
    [dateFormatter11 setDateFormat:dateFormatFull];
    NSDate *date1 = [dateFormatter11 dateFromString:myAppDelegate.taskData.startDate];
    [dateFormatter11 setDateFormat:dateFormatDate];
    NSString *dateString1 = [dateFormatter11 stringFromDate:date1];
    
    self.lblTaskDate.text = dateString1;
    
    for (int i = 0; i<myAppDelegate.arrayCategoryDetail.count; i++) {
        
        if ([[myAppDelegate.arrayCategoryDetail objectAtIndex:i] isKindOfClass:[subCategory class]]){
            
            subCategory *obj = [myAppDelegate.arrayCategoryDetail objectAtIndex:i];
            
            if ([obj.subCategoryId isEqualToString:myAppDelegate.workerAllTaskDetail.taskDetailObj.subcategoryId]) {
                
                self.lblAddress2.text = obj.name;
                
            }
            
        }
        
    }
    
    if (myAppDelegate.workerAllTaskDetail.isAlreadyRated) {
        self.btnAddRating.enabled = NO;
    }
    
    self.lblTaskCompleted.text = @"Task Accepted";
    
    self.viewAcceptBtnWorkerHolder.hidden = true;
    self.viewCompleteIncompleteBtnEmployerHolder.hidden = true;
    self.viewDoneBtnEmployerHolder.hidden = true;
    self.viewYesNoBtnEmployerHolder.hidden = true;
    self.viewDoneBtnWorkerHolder.hidden = false;
    
    self.constraintViewTaskPaymentDetailHolderHeight.constant = 0;
    self.constraintViewUserAdditionalInformationHeight.constant = 63;
    self.constraintViewRatingHolderHeight.constant = 0;
    self.constraintLblTaskCompletedHolderView.constant = 40;
    
    self.viewPaymentDetailHolder.hidden = YES;
    self.viewUserAdditionalInformation.hidden = NO;
    self.viewRatingHolder.hidden = YES;
    self.btnAddRating.hidden = YES;
    self.lblTaskCompleted.hidden = NO;
    
    noOfRating = myAppDelegate.workerAllTaskDetail.avgStars_createdBy;
    
    //other user information
    self.imgViewUserProfilePicture.image = myAppDelegate.workerAllTaskDetail.userImage_createdBy;
    self.lblUserName.text = myAppDelegate.workerAllTaskDetail.userName_createdBy;
    self.lblUserAddress.text = myAppDelegate.workerAllTaskDetail.locationName_createdBy;
    
    emailIdToSend = myAppDelegate.workerAllTaskDetail.emailId_createdBy;
    telToCall = myAppDelegate.workerAllTaskDetail.mobile_createdBy;
    locationToShow = myAppDelegate.workerAllTaskDetail.locationName_createdBy;
    
    if ([myAppDelegate.userData.userid isEqualToString:myAppDelegate.workerAllTaskDetail.userId_createdBy]){
        
        workerOrEmployer = @"Employer";
        
        if (myAppDelegate.workerAllTaskDetail.isTaskAccepted) {
            
            self.viewCompleteIncompleteBtnEmployerHolder.hidden = false;
            self.viewDoneBtnWorkerHolder.hidden = true;
            
            //other user information
            self.imgViewUserProfilePicture.image = myAppDelegate.workerAllTaskDetail.userImage_acceptedBy;
            self.lblUserName.text = myAppDelegate.workerAllTaskDetail.userName_acceptedBy;
            self.lblUserAddress.text = myAppDelegate.workerAllTaskDetail.locationName_acceptedBy;
            
            emailIdToSend = myAppDelegate.workerAllTaskDetail.emailId_acceptedBy;
            telToCall = myAppDelegate.workerAllTaskDetail.mobile_acceptedBy;
            locationToShow = myAppDelegate.workerAllTaskDetail.locationName_acceptedBy;
            
        }else{
            
            self.viewDoneBtnEmployerHolder.hidden = false;
            self.viewDoneBtnWorkerHolder.hidden = true;
            
            self.constraintViewUserAdditionalInformationHeight.constant = 0;
            self.constraintLblTaskCompletedHolderView.constant = 0;
            
            self.viewUserAdditionalInformation.hidden = YES;
            self.lblTaskCompleted.hidden = YES;
            
        }
        
    }else{
        
        workerOrEmployer = @"Worker";
        
        self.viewBottomHolder.backgroundColor = RGB(248, 248, 248);
        
        if ([myAppDelegate.userData.userid isEqualToString:myAppDelegate.workerAllTaskDetail.userId_acceptedBy]) {
            
            
        }else{
            
            self.btnLocation.enabled = NO;
            self.btnMail.enabled = NO;
            self.btnContact.enabled = NO;
            
        }
        
        if (myAppDelegate.workerAllTaskDetail.isTaskAccepted) {
            
            
        }else{
            
            self.viewAcceptBtnWorkerHolder.hidden = false;
            self.viewDoneBtnEmployerHolder.hidden = true;
            self.viewDoneBtnWorkerHolder.hidden = true;
            
            self.constraintLblTaskCompletedHolderView.constant = 0;
            self.constraintViewUserAdditionalInformationHeight.constant = 63;
            
            self.viewLblTaskCompletedHolder.hidden = YES;
            
        }
        
    }
    
    if(myAppDelegate.workerAllTaskDetail.taskDetailObj.taskImagesArray.count>0){
        
        _collectionViewHeight.constant=60;
        
    }else{
        _collectionViewHeight.constant=0;
    }
    
}

-(void)handleTapOnLabel:(UISwipeGestureRecognizer*)sender
{
    
    [GlobalFunction addIndicatorView];
    
    _webViewForURLData.delegate = self;
    
    
    [UIView animateWithDuration:0.5
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         
                         _blurview.alpha=1.0;
                         
                     } completion:^(BOOL finished) {
                         
                         
                     }];
    
    NSLog(@"hyper link tap .... ");
    
    //Create a URL object.
    
  //  NSString *mainURL = @"http://google.com"; https://www.instagram.com/
    
    
    NSArray *components = [_addressViewURLlbl.text componentsSeparatedByString:@"/"];
    NSString *userName = [components lastObject];
    NSString *prefix = @"https://www.instagram.com/";
    prefix = [prefix stringByAppendingString:userName];
    NSLog(@"%@", prefix);
    NSString *combined = [NSString stringWithFormat:@"%@%@", prefix,userName];
    NSLog(@"%@", combined);
    NSURL *url = [NSURL URLWithString: prefix];
   // NSURL *url = [NSURL URLWithString: mainURL];
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    [_webViewForURLData loadRequest:requestObj];
    

}
-(void)webViewDidFinishLoad:(UIWebView *)webView{
    [GlobalFunction removeIndicatorView];
}

-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    [GlobalFunction removeIndicatorView];
}


-(void)downloadSliderImages
{
    //  taskDetailObj.taskImagesArray
    
    if (myAppDelegate.workerAllTaskDetail.taskDetailObj.taskImagesArray.count>0)
        
    {
        
        NSLog(@"%lu",(unsigned long)myAppDelegate.workerAllTaskDetail.taskDetailObj.taskImagesArray.count);
        
        
        
        for(int i=0; i<myAppDelegate.workerAllTaskDetail.taskDetailObj.taskImagesArray.count;i++)
        {
            
            
            
            
            NSString *fullPath=[myAppDelegate.workerAllTaskDetail.taskDetailObj.taskImagesArray objectAtIndex:i];
            NSArray *parts = [fullPath componentsSeparatedByString:@"/"];
            NSString *imageName=[parts lastObject];
            NSArray *imageNameParts = [imageName componentsSeparatedByString:@"."];
            
            NSString *userImageName = [NSString stringWithFormat:@"%@_%@_%ld",[imageNameParts firstObject],myAppDelegate.workerAllTaskDetail.taskDetailObj.taskid,(long)i];
            NSString *userImageNameWithJPEG = [NSString stringWithFormat:@"%@_%@_%ld.jpeg",[imageNameParts firstObject],myAppDelegate.workerAllTaskDetail.taskDetailObj.taskid,(long)i];
            
            UIImage *img = [GlobalFunction loadImageOfLogoFromCacheFolder:userImageNameWithJPEG dirname:@"taskImages"];
            
            if (img) {
                
                [sliderImagesArrayDownloadImages addObject:img];
                
                _sliderImageView.image = img;
                
            }else{
                
                
                NSString *UrlStr =fullPath;
                
                NSString* webStringURL = [UrlStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                
                dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH,  0ul);
                dispatch_async(queue, ^{
                    //This is what you will load lazily
                    NSURL *imageURL=[NSURL URLWithString:webStringURL];
                    
                    NSData *images=[NSData dataWithContentsOfURL:imageURL];
                    
                    UIImage *img = [UIImage imageWithData:images];
                    
                    if (img) {
                        
                        [GlobalFunction saveimage:img imageName:userImageName dirname:@"taskImages"];
                        
                        dispatch_sync(dispatch_get_main_queue(), ^{
                            
                            [sliderImagesArrayDownloadImages addObject:img];
                            
                            _sliderImageView.image = img;
                            
                            if (!self.viewImageViewAndActivityHolder.hidden) {
                                
                                
                                //    [sliderImagesArrayDownloadImages addObject:img];
                                
                                self.sliderImageView.image = img;
                                self.activityIndicatorTaskDetailImages.hidden = YES;
                                [self.activityIndicatorTaskDetailImages stopAnimating];
                            }
                            
                            
                        });
                        
                    }
                    
                    
                });
                
            }
            
        }
        
        
        
        
    }
    
    else
        
        
    {
        
        
        
    }
}

-(void)handleCasesForSelf{
    
    if (myAppDelegate.isComingFromPushNotification) {
        
      //  [self handleWorkingOfPushNotification];
        
    }else{
        
        if ([myAppDelegate.comingFromClass isEqualToString:@"ActiveTaskViewController"]) {
            
         //   [self handleWorkingOfTaskActiveData];
            
        }
        else if ([myAppDelegate.comingFromClass isEqualToString:@"TaskHistoryViewController"]) {
            
         //   [self handleWorkingOfTaskHistoryData];
            
        }
        else if ([myAppDelegate.comingFromClass isEqualToString:@"ViewAllTaskViewController"]) {
            
            [self handleWorkingOfViewAllTaskData];
            
        }
        
    }
    
}


-(void)handleCasesForOtherUser{
    
    if (myAppDelegate.workerOtherUserTaskDetail.isAlreadyRated) {
        self.btnAddRating.enabled = NO;
    }
    
    for (UIView *view in self.viewDoneBtnWorkerHolder.subviews) {
        if ([view isKindOfClass:[UIButton class]]) {
            UIButton *btn = (UIButton *)view;
            [btn setTitle:@"Done" forState:UIControlStateNormal];
        }
    }
    
    self.btnLocation.enabled = NO;
    self.btnMail.enabled = NO;
    self.btnContact.enabled = NO;
    
    //task information
    self.lblTaskDescription.text = myAppDelegate.taskOtherData.taskDescription;
    self.lblTaskName.text = myAppDelegate.taskOtherData.title;
    
    if ([GlobalFunction checkIfContainFloat:myAppDelegate.taskOtherData.price]) {
        self.lblTaskPrice.text = [NSString stringWithFormat:@"$%0.2f",myAppDelegate.taskOtherData.price];
    }else{
        self.lblTaskPrice.text = [NSString stringWithFormat:@"$%0.0f",myAppDelegate.taskOtherData.price];
    }
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:dateFormatFull];
    NSDate *date = [dateFormatter dateFromString:myAppDelegate.taskOtherData.startDate];
    [dateFormatter setDateFormat:dateFormatDate];
    NSString *dateString = [dateFormatter stringFromDate:date];
    
    self.lblTaskDate.text = dateString;
    
    for (int i = 0; i<myAppDelegate.arrayCategoryDetail.count; i++) {
        
        if ([[myAppDelegate.arrayCategoryDetail objectAtIndex:i] isKindOfClass:[subCategory class]]){
            
            subCategory *obj = [myAppDelegate.arrayCategoryDetail objectAtIndex:i];
            
            if ([obj.subCategoryId isEqualToString:myAppDelegate.workerOtherUserTaskDetail.taskDetailObj.subcategoryId]) {
                
                self.lblAddress2.text = obj.name;
                
            }
            
        }
        
    }
    
    if (myAppDelegate.workerOtherUserTaskDetail.isTaskAccepted) {
        
        if ([myAppDelegate.userOtherData.userid isEqualToString:myAppDelegate.workerOtherUserTaskDetail.userId_createdBy]){
            
            workerOrEmployer = @"Employer";
            
            noOfRating = myAppDelegate.workerOtherUserTaskDetail.avgStars_acceptedBy;
            
            self.viewAcceptBtnWorkerHolder.hidden = true;
            self.viewCompleteIncompleteBtnEmployerHolder.hidden = true;
            self.viewDoneBtnEmployerHolder.hidden = false;
            self.viewYesNoBtnEmployerHolder.hidden = true;
            self.viewDoneBtnWorkerHolder.hidden = true;
            
            self.constraintLblTaskCompletedHolderView.constant = 40;
            self.constraintViewTaskPaymentDetailHolderHeight.constant = 0;
            self.constraintViewUserAdditionalInformationHeight.constant = 63;
            self.constraintViewRatingHolderHeight.constant = 0;
            
            if ([myAppDelegate.comingFromClass isEqualToString:[NSString stringWithFormat:@"%@",[TaskHistoryViewController class]]]) {
                self.constraintLblTaskCompletedHolderView.constant = 40;
            }else{
                self.constraintLblTaskCompletedHolderView.constant = 0;
            }
            
            //other user information
            self.imgViewUserProfilePicture.image = myAppDelegate.workerOtherUserTaskDetail.userImage_acceptedBy;
            self.lblUserName.text = myAppDelegate.workerOtherUserTaskDetail.userName_acceptedBy;
            self.lblUserAddress.text = myAppDelegate.workerOtherUserTaskDetail.locationName_acceptedBy;
            
            emailIdToSend = myAppDelegate.workerOtherUserTaskDetail.emailId_acceptedBy;
            telToCall = myAppDelegate.workerOtherUserTaskDetail.mobile_acceptedBy;
            locationToShow = myAppDelegate.workerOtherUserTaskDetail.locationName_acceptedBy;
            
        }else{
            
            workerOrEmployer = @"Worker";
            
            noOfRating = myAppDelegate.workerOtherUserTaskDetail.avgStars_createdBy;
            
            self.viewBottomHolder.backgroundColor = RGB(248, 248, 248);
            
            //other user information
            self.imgViewUserProfilePicture.image = myAppDelegate.workerOtherUserTaskDetail.userImage_createdBy;
            self.lblUserName.text = myAppDelegate.workerOtherUserTaskDetail.userName_createdBy;
            self.lblUserAddress.text = myAppDelegate.workerOtherUserTaskDetail.locationName_createdBy;
            
            emailIdToSend = myAppDelegate.workerOtherUserTaskDetail.emailId_createdBy;
            telToCall = myAppDelegate.workerOtherUserTaskDetail.mobile_createdBy;
            locationToShow = myAppDelegate.workerOtherUserTaskDetail.locationName_createdBy;
            
            self.viewAcceptBtnWorkerHolder.hidden = true;
            self.viewCompleteIncompleteBtnEmployerHolder.hidden = true;
            self.viewDoneBtnEmployerHolder.hidden = true;
            self.viewYesNoBtnEmployerHolder.hidden = true;
            self.viewDoneBtnWorkerHolder.hidden = false;
            
            self.constraintViewTaskPaymentDetailHolderHeight.constant = 0;
            self.constraintViewUserAdditionalInformationHeight.constant = 63;
            self.constraintViewRatingHolderHeight.constant = 0;
            
            if ([myAppDelegate.comingFromClass isEqualToString:[NSString stringWithFormat:@"%@",[TaskHistoryViewController class]]]) {
                self.constraintLblTaskCompletedHolderView.constant = 40;
            }else{
                self.constraintLblTaskCompletedHolderView.constant = 0;
            }
            
        }
        
        if ([myAppDelegate.comingFromClass isEqualToString:[NSString stringWithFormat:@"%@",[TaskHistoryViewController class]]]) {
            self.viewLblTaskCompletedHolder.hidden = NO;
        }else{
            self.viewLblTaskCompletedHolder.hidden = YES;
        }
        
        self.viewPaymentDetailHolder.hidden = YES;
        self.viewUserAdditionalInformation.hidden = NO;
        self.viewRatingHolder.hidden = YES;
        self.btnAddRating.hidden = YES;
        
    }else{
        
        workerOrEmployer = @"Employer";
        
        noOfRating = myAppDelegate.workerOtherUserTaskDetail.avgStars_createdBy;
        
        self.viewAcceptBtnWorkerHolder.hidden = true;
        self.viewCompleteIncompleteBtnEmployerHolder.hidden = true;
        self.viewDoneBtnEmployerHolder.hidden = false;
        self.viewYesNoBtnEmployerHolder.hidden = true;
        self.viewDoneBtnWorkerHolder.hidden = true;
        
        self.constraintLblTaskCompletedHolderView.constant = 0;
        self.constraintViewTaskPaymentDetailHolderHeight.constant = 0;
        self.constraintViewUserAdditionalInformationHeight.constant = 0;
        self.constraintViewRatingHolderHeight.constant = 0;
        
        self.viewBottomHolder.backgroundColor = RGB(248, 248, 248);
        
        //other user information
        if ([myAppDelegate.userOtherData.userid isEqualToString:myAppDelegate.userData.userid]) {
            
            self.imgViewUserProfilePicture.image = myAppDelegate.userData.userImage;
            self.lblUserName.text = myAppDelegate.userData.username;
            self.lblUserAddress.text = myAppDelegate.userData.location;
            
            emailIdToSend = myAppDelegate.userData.emailId;
            telToCall = myAppDelegate.userData.mobile;
            locationToShow = myAppDelegate.userData.location;
            
        }else{
            
            self.imgViewUserProfilePicture.image = myAppDelegate.userOtherData.userImage;
            self.lblUserName.text = myAppDelegate.userOtherData.username;
            self.lblUserAddress.text = myAppDelegate.userOtherData.location;
            
            emailIdToSend = myAppDelegate.userOtherData.emailId;
            telToCall = myAppDelegate.userOtherData.mobile;
            locationToShow = myAppDelegate.userOtherData.location;
            
        }
        
        self.viewLblTaskCompletedHolder.hidden = YES;
        self.viewPaymentDetailHolder.hidden = YES;
        self.viewUserAdditionalInformation.hidden = YES;
        self.viewRatingHolder.hidden = YES;
        self.btnAddRating.hidden = YES;
        
    }
    
    if(myAppDelegate.workerOtherUserTaskDetail.taskDetailObj.taskImagesArray.count>0){
        
        _collectionViewHeight.constant=60;
        
    }else{
        
        _collectionViewHeight.constant=0;
        
    }
    
    
}


-(void)removeIndicator{
    
    [GlobalFunction removeIndicatorView];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if ([segue.identifier isEqualToString:@"rating"]) {
        
        if (!myAppDelegate.isComingFromPushNotification) {
            myAppDelegate.vcObj.btnSideBarOpen.hidden = YES;
            myAppDelegate.vcObj.btnBack.hidden = NO;
        }
        
        myAppDelegate.stringNameOfChildVC = [NSString stringWithFormat:@"%@",[self class]];
        
        myAppDelegate.ratingVCObj = nil;
        
        myAppDelegate.ratingVCObj = (RatingViewController *)segue.destinationViewController;
        
    }
    if ([segue.identifier isEqualToString:@"otherUserProfile"]) {
        
        if (myAppDelegate.isComingFromPushNotification) {
            
            myAppDelegate.isOfPushOtherUser = true;
            
            myAppDelegate.userPushOtherData = nil;
            myAppDelegate.userPushOtherData = [[user alloc] init];
            
            if (myAppDelegate.workerPushTaskDetail.isTaskAccepted && [myAppDelegate.workerPushTaskDetail.userId_createdBy isEqualToString:myAppDelegate.userData.userid]) {
                
                myAppDelegate.userPushOtherData.userid = myAppDelegate.workerPushTaskDetail.userId_acceptedBy;
                myAppDelegate.userPushOtherData.username = myAppDelegate.workerPushTaskDetail.userName_acceptedBy;
                myAppDelegate.userPushOtherData.userDescription = myAppDelegate.workerPushTaskDetail.description_acceptedBy;
                myAppDelegate.userPushOtherData.emailId = myAppDelegate.workerPushTaskDetail.emailId_acceptedBy;
                myAppDelegate.userPushOtherData.location = myAppDelegate.workerPushTaskDetail.locationName_acceptedBy;
                myAppDelegate.userPushOtherData.mobile = myAppDelegate.workerPushTaskDetail.mobile_acceptedBy;
                myAppDelegate.userPushOtherData.picpath = myAppDelegate.workerPushTaskDetail.picPath_acceptedBy;
                myAppDelegate.userPushOtherData.userImage = myAppDelegate.workerPushTaskDetail.userImage_acceptedBy;
                myAppDelegate.userPushOtherData.avg_rating = myAppDelegate.workerPushTaskDetail.avg_rating_acceptedBy;
                
                
            }else{
                
                myAppDelegate.userPushOtherData.userid = myAppDelegate.workerPushTaskDetail.userId_createdBy;
                myAppDelegate.userPushOtherData.username = myAppDelegate.workerPushTaskDetail.userName_createdBy;
                myAppDelegate.userPushOtherData.userDescription = myAppDelegate.workerPushTaskDetail.description_createdBy;
                myAppDelegate.userPushOtherData.emailId = myAppDelegate.workerPushTaskDetail.emailId_createdBy;
                myAppDelegate.userPushOtherData.location = myAppDelegate.workerPushTaskDetail.locationName_createdBy;
                myAppDelegate.userPushOtherData.mobile = myAppDelegate.workerPushTaskDetail.mobile_createdBy;
                myAppDelegate.userPushOtherData.picpath = myAppDelegate.workerPushTaskDetail.picPath_createdBy;
                myAppDelegate.userPushOtherData.userImage = myAppDelegate.workerPushTaskDetail.userImage_createdBy;
                myAppDelegate.userPushOtherData.avg_rating = myAppDelegate.workerPushTaskDetail.avg_rating_createdBy;
                
                
            }
            
            myAppDelegate.profilePushOtherUserVCObj = nil;
            
            myAppDelegate.profilePushOtherUserVCObj = (ProfileViewController *)segue.destinationViewController;
            
        }else{
            
            myAppDelegate.vcObj.btnSideBarOpen.hidden = YES;
            
            myAppDelegate.vcObj.btnBack.hidden = NO;
            
            myAppDelegate.stringNameOfChildVC = [NSString stringWithFormat:@"%@",[self class]];
            
            if (!myAppDelegate.userOtherData) {
                myAppDelegate.userOtherData = nil;
                myAppDelegate.userOtherData = [[user alloc]init];
            }
            
            if ([myAppDelegate.comingFromClass isEqualToString:@"ViewAllTaskViewController"]) {
                
                if (myAppDelegate.isOfOtherUser) {
                    
                    if (myAppDelegate.workerOtherUserTaskDetail.isTaskAccepted) {
                        
                        if ([myAppDelegate.userOtherData.userid isEqualToString:myAppDelegate.workerOtherUserTaskDetail.userId_acceptedBy]) {
                            
                            myAppDelegate.userOtherData = nil;
                            myAppDelegate.userOtherData = [[user alloc]init];
                            
                            myAppDelegate.userOtherData.userid = myAppDelegate.workerOtherUserTaskDetail.userId_createdBy;
                            myAppDelegate.userOtherData.username = myAppDelegate.workerOtherUserTaskDetail.userName_createdBy;
                            myAppDelegate.userOtherData.userDescription = myAppDelegate.workerOtherUserTaskDetail.description_createdBy;
                            myAppDelegate.userOtherData.emailId = myAppDelegate.workerOtherUserTaskDetail.emailId_createdBy;
                            myAppDelegate.userOtherData.location = myAppDelegate.workerOtherUserTaskDetail.locationName_createdBy;
                            myAppDelegate.userOtherData.mobile = myAppDelegate.workerOtherUserTaskDetail.mobile_createdBy;
                            myAppDelegate.userOtherData.picpath = myAppDelegate.workerOtherUserTaskDetail.picPath_createdBy;
                            myAppDelegate.userOtherData.userImage = myAppDelegate.workerOtherUserTaskDetail.userImage_createdBy;
                            myAppDelegate.userOtherData.avg_rating = myAppDelegate.workerOtherUserTaskDetail.avg_rating_createdBy;
                            
                        }else if ([myAppDelegate.userOtherData.userid isEqualToString:myAppDelegate.workerOtherUserTaskDetail.userId_createdBy]){
                            
                            myAppDelegate.userOtherData = nil;
                            myAppDelegate.userOtherData = [[user alloc]init];
                            
                            myAppDelegate.userOtherData.userid = myAppDelegate.workerOtherUserTaskDetail.userId_acceptedBy;
                            myAppDelegate.userOtherData.username = myAppDelegate.workerOtherUserTaskDetail.userName_acceptedBy;
                            myAppDelegate.userOtherData.userDescription = myAppDelegate.workerOtherUserTaskDetail.description_acceptedBy;
                            myAppDelegate.userOtherData.emailId = myAppDelegate.workerOtherUserTaskDetail.emailId_acceptedBy;
                            myAppDelegate.userOtherData.location = myAppDelegate.workerOtherUserTaskDetail.locationName_acceptedBy;
                            myAppDelegate.userOtherData.mobile = myAppDelegate.workerOtherUserTaskDetail.mobile_acceptedBy;
                            myAppDelegate.userOtherData.picpath = myAppDelegate.workerOtherUserTaskDetail.picPath_acceptedBy;
                            myAppDelegate.userOtherData.userImage = myAppDelegate.workerOtherUserTaskDetail.userImage_acceptedBy;
                            myAppDelegate.userOtherData.avg_rating = myAppDelegate.workerOtherUserTaskDetail.avg_rating_acceptedBy;
                            
                        }else{
                            
                            myAppDelegate.userOtherData = nil;
                            myAppDelegate.userOtherData = [[user alloc]init];
                            
                            myAppDelegate.userOtherData.userid = myAppDelegate.workerOtherUserTaskDetail.userId_createdBy;
                            myAppDelegate.userOtherData.username = myAppDelegate.workerOtherUserTaskDetail.userName_createdBy;
                            myAppDelegate.userOtherData.userDescription = myAppDelegate.workerOtherUserTaskDetail.description_createdBy;
                            myAppDelegate.userOtherData.emailId = myAppDelegate.workerOtherUserTaskDetail.emailId_createdBy;
                            myAppDelegate.userOtherData.location = myAppDelegate.workerOtherUserTaskDetail.locationName_createdBy;
                            myAppDelegate.userOtherData.mobile = myAppDelegate.workerOtherUserTaskDetail.mobile_createdBy;
                            myAppDelegate.userOtherData.picpath = myAppDelegate.workerOtherUserTaskDetail.picPath_createdBy;
                            myAppDelegate.userOtherData.userImage = myAppDelegate.workerOtherUserTaskDetail.userImage_createdBy;
                            myAppDelegate.userOtherData.avg_rating = myAppDelegate.workerOtherUserTaskDetail.avg_rating_createdBy;
                            
                        }
                        
                    }else{
                        
                        myAppDelegate.userOtherData = nil;
                        myAppDelegate.userOtherData = [[user alloc]init];
                        
                        myAppDelegate.userOtherData.userid = myAppDelegate.workerOtherUserTaskDetail.userId_createdBy;
                        myAppDelegate.userOtherData.username = myAppDelegate.workerOtherUserTaskDetail.userName_createdBy;
                        myAppDelegate.userOtherData.userDescription = myAppDelegate.workerOtherUserTaskDetail.description_createdBy;
                        myAppDelegate.userOtherData.emailId = myAppDelegate.workerOtherUserTaskDetail.emailId_createdBy;
                        myAppDelegate.userOtherData.location = myAppDelegate.workerOtherUserTaskDetail.locationName_createdBy;
                        myAppDelegate.userOtherData.mobile = myAppDelegate.workerOtherUserTaskDetail.mobile_createdBy;
                        myAppDelegate.userOtherData.picpath = myAppDelegate.workerOtherUserTaskDetail.picPath_createdBy;
                        myAppDelegate.userOtherData.userImage = myAppDelegate.workerOtherUserTaskDetail.userImage_createdBy;
                        myAppDelegate.userOtherData.avg_rating = myAppDelegate.workerOtherUserTaskDetail.avg_rating_createdBy;
                        
                    }
                    
                }else{
                    
                    if (myAppDelegate.workerAllTaskDetail.isTaskAccepted) {
                        
                        if ([myAppDelegate.userData.userid isEqualToString:myAppDelegate.workerAllTaskDetail.userId_acceptedBy]) {
                            
                            myAppDelegate.userOtherData = nil;
                            myAppDelegate.userOtherData = [[user alloc]init];
                            
                            myAppDelegate.userOtherData.userid = myAppDelegate.workerAllTaskDetail.userId_createdBy;
                            myAppDelegate.userOtherData.username = myAppDelegate.workerAllTaskDetail.userName_createdBy;
                            myAppDelegate.userOtherData.userDescription = myAppDelegate.workerAllTaskDetail.description_createdBy;
                            myAppDelegate.userOtherData.emailId = myAppDelegate.workerAllTaskDetail.emailId_createdBy;
                            myAppDelegate.userOtherData.location = myAppDelegate.workerAllTaskDetail.locationName_createdBy;
                            myAppDelegate.userOtherData.mobile = myAppDelegate.workerAllTaskDetail.mobile_createdBy;
                            myAppDelegate.userOtherData.picpath = myAppDelegate.workerAllTaskDetail.picPath_createdBy;
                            myAppDelegate.userOtherData.userImage = myAppDelegate.workerAllTaskDetail.userImage_createdBy;
                            myAppDelegate.userOtherData.avg_rating = myAppDelegate.workerAllTaskDetail.avg_rating_createdBy;
                            
                        }else if([myAppDelegate.userData.userid isEqualToString:myAppDelegate.workerAllTaskDetail.userId_createdBy]){
                            
                            myAppDelegate.userOtherData = nil;
                            myAppDelegate.userOtherData = [[user alloc]init];
                            
                            myAppDelegate.userOtherData.userid = myAppDelegate.workerAllTaskDetail.userId_acceptedBy;
                            myAppDelegate.userOtherData.username = myAppDelegate.workerAllTaskDetail.userName_acceptedBy;
                            myAppDelegate.userOtherData.userDescription = myAppDelegate.workerAllTaskDetail.description_acceptedBy;
                            myAppDelegate.userOtherData.emailId = myAppDelegate.workerAllTaskDetail.emailId_acceptedBy;
                            myAppDelegate.userOtherData.location = myAppDelegate.workerAllTaskDetail.locationName_acceptedBy;
                            myAppDelegate.userOtherData.mobile = myAppDelegate.workerAllTaskDetail.mobile_acceptedBy;
                            myAppDelegate.userOtherData.picpath = myAppDelegate.workerAllTaskDetail.picPath_acceptedBy;
                            myAppDelegate.userOtherData.userImage = myAppDelegate.workerAllTaskDetail.userImage_acceptedBy;
                            myAppDelegate.userOtherData.avg_rating = myAppDelegate.workerAllTaskDetail.avg_rating_acceptedBy;
                            
                        }
                        
                    }else{
                        
                        myAppDelegate.userOtherData = nil;
                        myAppDelegate.userOtherData = [[user alloc]init];
                        
                        myAppDelegate.userOtherData.userid = myAppDelegate.workerAllTaskDetail.userId_createdBy;
                        myAppDelegate.userOtherData.username = myAppDelegate.workerAllTaskDetail.userName_createdBy;
                        myAppDelegate.userOtherData.userDescription = myAppDelegate.workerAllTaskDetail.description_createdBy;
                        myAppDelegate.userOtherData.emailId = myAppDelegate.workerAllTaskDetail.emailId_createdBy;
                        myAppDelegate.userOtherData.location = myAppDelegate.workerAllTaskDetail.locationName_createdBy;
                        myAppDelegate.userOtherData.mobile = myAppDelegate.workerAllTaskDetail.mobile_createdBy;
                        myAppDelegate.userOtherData.picpath = myAppDelegate.workerAllTaskDetail.picPath_createdBy;
                        myAppDelegate.userOtherData.userImage = myAppDelegate.workerAllTaskDetail.userImage_createdBy;
                        myAppDelegate.userOtherData.avg_rating = myAppDelegate.workerAllTaskDetail.avg_rating_createdBy;
                        
                    }
                    
                }
                
            }else{
                
                if (myAppDelegate.isOfOtherUser) {
                    
                    if (myAppDelegate.workerOtherUserTaskDetail.isTaskAccepted) {
                        
                        if ([myAppDelegate.userOtherData.userid isEqualToString:myAppDelegate.workerOtherUserTaskDetail.userId_acceptedBy]) {
                            
                            myAppDelegate.userOtherData = nil;
                            myAppDelegate.userOtherData = [[user alloc]init];
                            
                            myAppDelegate.userOtherData.userid = myAppDelegate.workerOtherUserTaskDetail.userId_createdBy;
                            myAppDelegate.userOtherData.username = myAppDelegate.workerOtherUserTaskDetail.userName_createdBy;
                            myAppDelegate.userOtherData.userDescription = myAppDelegate.workerOtherUserTaskDetail.description_createdBy;
                            myAppDelegate.userOtherData.emailId = myAppDelegate.workerOtherUserTaskDetail.emailId_createdBy;
                            myAppDelegate.userOtherData.location = myAppDelegate.workerOtherUserTaskDetail.locationName_createdBy;
                            myAppDelegate.userOtherData.mobile = myAppDelegate.workerOtherUserTaskDetail.mobile_createdBy;
                            myAppDelegate.userOtherData.picpath = myAppDelegate.workerOtherUserTaskDetail.picPath_createdBy;
                            myAppDelegate.userOtherData.userImage = myAppDelegate.workerOtherUserTaskDetail.userImage_createdBy;
                            myAppDelegate.userOtherData.avg_rating = myAppDelegate.workerOtherUserTaskDetail.avg_rating_createdBy;
                            
                        }else if ([myAppDelegate.userOtherData.userid isEqualToString:myAppDelegate.workerOtherUserTaskDetail.userId_createdBy]){
                            
                            myAppDelegate.userOtherData = nil;
                            myAppDelegate.userOtherData = [[user alloc]init];
                            
                            myAppDelegate.userOtherData.userid = myAppDelegate.workerOtherUserTaskDetail.userId_acceptedBy;
                            myAppDelegate.userOtherData.username = myAppDelegate.workerOtherUserTaskDetail.userName_acceptedBy;
                            myAppDelegate.userOtherData.userDescription = myAppDelegate.workerOtherUserTaskDetail.description_acceptedBy;
                            myAppDelegate.userOtherData.emailId = myAppDelegate.workerOtherUserTaskDetail.emailId_acceptedBy;
                            myAppDelegate.userOtherData.location = myAppDelegate.workerOtherUserTaskDetail.locationName_acceptedBy;
                            myAppDelegate.userOtherData.mobile = myAppDelegate.workerOtherUserTaskDetail.mobile_acceptedBy;
                            myAppDelegate.userOtherData.picpath = myAppDelegate.workerOtherUserTaskDetail.picPath_acceptedBy;
                            myAppDelegate.userOtherData.userImage = myAppDelegate.workerOtherUserTaskDetail.userImage_acceptedBy;
                            myAppDelegate.userOtherData.avg_rating = myAppDelegate.workerOtherUserTaskDetail.avg_rating_acceptedBy;
                            
                        }else{
                            
                            myAppDelegate.userOtherData = nil;
                            myAppDelegate.userOtherData = [[user alloc]init];
                            
                            myAppDelegate.userOtherData.userid = myAppDelegate.workerOtherUserTaskDetail.userId_createdBy;
                            myAppDelegate.userOtherData.username = myAppDelegate.workerOtherUserTaskDetail.userName_createdBy;
                            myAppDelegate.userOtherData.userDescription = myAppDelegate.workerOtherUserTaskDetail.description_createdBy;
                            myAppDelegate.userOtherData.emailId = myAppDelegate.workerOtherUserTaskDetail.emailId_createdBy;
                            myAppDelegate.userOtherData.location = myAppDelegate.workerOtherUserTaskDetail.locationName_createdBy;
                            myAppDelegate.userOtherData.mobile = myAppDelegate.workerOtherUserTaskDetail.mobile_createdBy;
                            myAppDelegate.userOtherData.picpath = myAppDelegate.workerOtherUserTaskDetail.picPath_createdBy;
                            myAppDelegate.userOtherData.userImage = myAppDelegate.workerOtherUserTaskDetail.userImage_createdBy;
                            myAppDelegate.userOtherData.avg_rating = myAppDelegate.workerOtherUserTaskDetail.avg_rating_createdBy;
                            
                        }
                        
                    }else{
                        
                        myAppDelegate.userOtherData = nil;
                        myAppDelegate.userOtherData = [[user alloc]init];
                        
                        myAppDelegate.userOtherData.userid = myAppDelegate.workerOtherUserTaskDetail.userId_createdBy;
                        myAppDelegate.userOtherData.username = myAppDelegate.workerOtherUserTaskDetail.userName_createdBy;
                        myAppDelegate.userOtherData.userDescription = myAppDelegate.workerOtherUserTaskDetail.description_createdBy;
                        myAppDelegate.userOtherData.emailId = myAppDelegate.workerOtherUserTaskDetail.emailId_createdBy;
                        myAppDelegate.userOtherData.location = myAppDelegate.workerOtherUserTaskDetail.locationName_createdBy;
                        myAppDelegate.userOtherData.mobile = myAppDelegate.workerOtherUserTaskDetail.mobile_createdBy;
                        myAppDelegate.userOtherData.picpath = myAppDelegate.workerOtherUserTaskDetail.picPath_createdBy;
                        myAppDelegate.userOtherData.userImage = myAppDelegate.workerOtherUserTaskDetail.userImage_createdBy;
                        myAppDelegate.userOtherData.avg_rating = myAppDelegate.workerOtherUserTaskDetail.avg_rating_createdBy;
                        
                    }
                    
                    
                }else{
                    
                    if (myAppDelegate.workerTaskDetail.isTaskAccepted) {
                        
                        if ([myAppDelegate.userData.userid isEqualToString:myAppDelegate.workerTaskDetail.userId_acceptedBy]) {
                            
                            myAppDelegate.userOtherData = nil;
                            myAppDelegate.userOtherData = [[user alloc]init];
                            
                            myAppDelegate.userOtherData.userid = myAppDelegate.workerTaskDetail.userId_createdBy;
                            myAppDelegate.userOtherData.username = myAppDelegate.workerTaskDetail.userName_createdBy;
                            myAppDelegate.userOtherData.userDescription = myAppDelegate.workerTaskDetail.description_createdBy;
                            myAppDelegate.userOtherData.emailId = myAppDelegate.workerTaskDetail.emailId_createdBy;
                            myAppDelegate.userOtherData.location = myAppDelegate.workerTaskDetail.locationName_createdBy;
                            myAppDelegate.userOtherData.mobile = myAppDelegate.workerTaskDetail.mobile_createdBy;
                            myAppDelegate.userOtherData.picpath = myAppDelegate.workerTaskDetail.picPath_createdBy;
                            myAppDelegate.userOtherData.userImage = myAppDelegate.workerTaskDetail.userImage_createdBy;
                            myAppDelegate.userOtherData.avg_rating = myAppDelegate.workerTaskDetail.avg_rating_createdBy;
                            
                        }else if ([myAppDelegate.userData.userid isEqualToString:myAppDelegate.workerTaskDetail.userId_createdBy]){
                            
                            myAppDelegate.userOtherData = nil;
                            myAppDelegate.userOtherData = [[user alloc]init];
                            
                            myAppDelegate.userOtherData.userid = myAppDelegate.workerTaskDetail.userId_acceptedBy;
                            myAppDelegate.userOtherData.username = myAppDelegate.workerTaskDetail.userName_acceptedBy;
                            myAppDelegate.userOtherData.userDescription = myAppDelegate.workerTaskDetail.description_acceptedBy;
                            myAppDelegate.userOtherData.emailId = myAppDelegate.workerTaskDetail.emailId_acceptedBy;
                            myAppDelegate.userOtherData.location = myAppDelegate.workerTaskDetail.locationName_acceptedBy;
                            myAppDelegate.userOtherData.mobile = myAppDelegate.workerTaskDetail.mobile_acceptedBy;
                            myAppDelegate.userOtherData.picpath = myAppDelegate.workerTaskDetail.picPath_acceptedBy;
                            myAppDelegate.userOtherData.userImage = myAppDelegate.workerTaskDetail.userImage_acceptedBy;
                            myAppDelegate.userOtherData.avg_rating = myAppDelegate.workerTaskDetail.avg_rating_acceptedBy;
                            
                        }
                        
                    }else{
                        
                        myAppDelegate.userOtherData = nil;
                        myAppDelegate.userOtherData = [[user alloc]init];
                        
                        myAppDelegate.userOtherData.userid = myAppDelegate.workerTaskDetail.userId_createdBy;
                        myAppDelegate.userOtherData.username = myAppDelegate.workerTaskDetail.userName_createdBy;
                        myAppDelegate.userOtherData.userDescription = myAppDelegate.workerTaskDetail.description_createdBy;
                        myAppDelegate.userOtherData.emailId = myAppDelegate.workerTaskDetail.emailId_createdBy;
                        myAppDelegate.userOtherData.location = myAppDelegate.workerTaskDetail.locationName_createdBy;
                        myAppDelegate.userOtherData.mobile = myAppDelegate.workerTaskDetail.mobile_createdBy;
                        myAppDelegate.userOtherData.picpath = myAppDelegate.workerTaskDetail.picPath_createdBy;
                        myAppDelegate.userOtherData.userImage = myAppDelegate.workerTaskDetail.userImage_createdBy;
                        myAppDelegate.userOtherData.avg_rating = myAppDelegate.workerTaskDetail.avg_rating_createdBy;
                        
                    }
                    
                }
                
            }
            
            myAppDelegate.isOfOtherUser = true;
            
            myAppDelegate.profileOtherUserVCObj = nil;
            
            myAppDelegate.profileOtherUserVCObj = (ProfileViewController *)segue.destinationViewController;
            
        }
        
    }
    if ([segue.identifier isEqualToString:@"otherUserTaskHistory"]) {
        
        if (myAppDelegate.isComingFromPushNotification) {
            
            myAppDelegate.taskHistoryPushOtherUserVCObj = nil;
            
            myAppDelegate.taskHistoryPushOtherUserVCObj = (TaskHistoryViewController *)segue.destinationViewController;
            
        }else{
            
            myAppDelegate.isOfOtherUserTaskHistory = YES;
            
            myAppDelegate.taskHistoryOtherUserVCObj = nil;
            
            myAppDelegate.taskHistoryOtherUserVCObj = (TaskHistoryViewController *)segue.destinationViewController;
            
        }
        
    }
    if ([segue.identifier isEqualToString:@"otherUserTaskDetail"]) {
        
        if (myAppDelegate.isComingFromPushNotification) {
            
            myAppDelegate.taskDetailOtherPushVCObj = nil;
            
            myAppDelegate.taskDetailOtherPushVCObj = (TaskDetailViewController *)segue.destinationViewController;
            
        }else{
            
            myAppDelegate.isOfOtherUserTaskDetail = YES;
            
            myAppDelegate.taskDetailOtherUserVCObj = nil;
            
            myAppDelegate.taskDetailOtherUserVCObj = (TaskDetailViewController *)segue.destinationViewController;
            
        }
        
    }
    
}

-(BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender{
    
    //    if ([identifier isEqualToString:@"otherUserProfile"]) {
    //
    //        if (myAppDelegate.isOfOtherUserTaskDetail) {
    //            return NO;
    //        }
    //
    //    }
    
    return YES;
}



-(void)updateAfter:(NSString *)afterwork andDict:(NSDictionary *)dict{
    
    afterWorkString = [NSString stringWithFormat:@"%@",afterwork];
    afterWorkDict = dict;
    
    if ([[GlobalFunction shared] callUpdateWebservice]) {
        
        [GlobalFunction removeIndicatorView];
        
        if ([afterWorkString isEqualToString:@"Accept task"]) {
            
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"BUY TIMEE" message:@"Task accepted successfully." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            
            alertView.tag = 998;
            
            [alertView show];
            
        }else if ([afterWorkString isEqualToString:@"Task complete"]){
            
            [self performTaskCompleteWorkFor:workerOrEmployer];
            
        }else if ([afterWorkString isEqualToString:@"Relist YES"] || [afterWorkString isEqualToString:@"Relist NO"]){
            
            NSString *messageString = @"";
            
            if ([afterWorkString isEqualToString:@"Relist YES"]) {
                messageString = @"Task relisted successfully.";
            }else if ([afterWorkString isEqualToString:@"Relist NO"]){
                messageString = @"Task marked as incomplete successfully.";
            }
            
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"BUY TIMEE" message:messageString delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            
            alertView.tag = 998;
            
            [alertView show];
            
        }
        
    }else{
        
        [GlobalFunction removeIndicatorView];
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"BUY TIMEE" message:@"Failed to update, please try again." delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Retry", nil];
        
        alertView.tag = 999;
        
        [alertView show];
        
    }
    
}

-(void)updateWebServiceHandling{
    
    if (![self checkForInternet]) {
        
        [GlobalFunction removeIndicatorView];
        
        if ([afterWorkString isEqualToString:@"Accept task"]) {
            
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"BUY TIMEE" message:@"Task accepted successfully." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            
            alertView.tag = 998;
            
            [alertView show];
            
        }else if ([afterWorkString isEqualToString:@"Task complete"]){
            
            [self performTaskCompleteWorkFor:workerOrEmployer];
            
        }else if ([afterWorkString isEqualToString:@"Relist YES"] || [afterWorkString isEqualToString:@"Relist NO"]){
            
            NSString *messageString = @"";
            
            if ([afterWorkString isEqualToString:@"Relist YES"]) {
                messageString = @"Task relisted successfully.";
            }else if ([afterWorkString isEqualToString:@"Relist NO"]){
                messageString = @"Task marked as incomplete successfully.";
            }
            
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"BUY TIMEE" message:messageString delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            
            alertView.tag = 998;
            
            [alertView show];
            
        }
        
        return;
    }
    
    if ([[GlobalFunction shared] callUpdateWebservice]) {
        
        [GlobalFunction removeIndicatorView];
        
        if ([afterWorkString isEqualToString:@"Accept task"]) {
            
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"BUY TIMEE" message:@"Task accepted successfully." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            
            alertView.tag = 998;
            
            [alertView show];
            
        }else if ([afterWorkString isEqualToString:@"Task complete"]){
            
            [self performTaskCompleteWorkFor:workerOrEmployer];
            
        }else if ([afterWorkString isEqualToString:@"Relist YES"] || [afterWorkString isEqualToString:@"Relist NO"]){
            
            NSString *messageString = @"";
            
            if ([afterWorkString isEqualToString:@"Relist YES"]) {
                messageString = @"Task relisted successfully.";
            }else if ([afterWorkString isEqualToString:@"Relist NO"]){
                messageString = @"Task marked as incomplete successfully.";
            }
            
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"BUY TIMEE" message:messageString delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            
            alertView.tag = 998;
            
            [alertView show];
            
        }
        
    }else{
        
        [GlobalFunction removeIndicatorView];
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"BUY TIMEE" message:@"Failed to update, please try again." delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Retry", nil];
        
        alertView.tag = 999;
        
        [alertView show];
        
    }
    
}

-(BOOL)checkForInternet{
    
    BOOL isConnected = true;
    
    if (![[WebService shared] connected]) {
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"BUY TIMEE" message:@"Can't connect to internet, update not successful. Please check your internet connection then try again." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        [alertView show];
        
        isConnected = false;
        
    }
    
    return isConnected;
}


- (IBAction)taskCompleteClicked:(id)sender {
    
    [GlobalFunction addIndicatorView];
    
    [self performSelector:@selector(taskCompleteWork) withObject:nil afterDelay:0.2];
    
}

-(void)taskCompleteWork{
    
    if (![[WebService shared] connected]) {
        
        [GlobalFunction removeIndicatorView];
        
        [[GlobalFunction shared] showAlertForMessage:@"Can't connect to internet, please check and try again."];
        
        return;
    }
    
    NSDictionary *dictTaskInfoResponse;
    
    if (myAppDelegate.isComingFromPushNotification) {
        
        dictTaskInfoResponse = [[WebService shared] employeeStatusTaskid:myAppDelegate.taskPushData.taskid status:@"3"];
        
    }else{
        
        dictTaskInfoResponse = [[WebService shared] employeeStatusTaskid:myAppDelegate.taskData.taskid status:@"3"];
        
    }
    
    if (dictTaskInfoResponse) {
        
        if ([[EncryptDecryptFile decrypt:[dictTaskInfoResponse valueForKey:@"responseCode"]] isEqualToString:@"200"]) {
            
            NSLog(@"response message == %@",[EncryptDecryptFile decrypt:[dictTaskInfoResponse valueForKey:@"responseMessage"]]);
            
            [self updateAfter:@"Task complete" andDict:dictTaskInfoResponse];
            
        }else{
            
            [GlobalFunction removeIndicatorView];
            
            [[GlobalFunction shared] showAlertForMessage:[EncryptDecryptFile decrypt:[dictTaskInfoResponse valueForKey:@"responseMessage"]]];
            
        }
        
    }else{
        
        [GlobalFunction removeIndicatorView];
        
        [[GlobalFunction shared] showAlertForMessage:@"Server is down, please try again later."];
        
    }
    
}

-(void)performTaskCompleteWorkFor:(NSString *)userType{
    
    for (UIView *view in self.viewDoneBtnWorkerHolder.subviews) {
        if ([view isKindOfClass:[UIButton class]]) {
            UIButton *btn = (UIButton *)view;
            [btn setTitle:@"Done" forState:UIControlStateNormal];
        }
    }
    
    [self.view layoutIfNeeded];
    
    self.lblTaskCompleted.text = @"Task Completed";
    
    if ([userType isEqualToString:@"Worker"]) {
        
        self.viewAcceptBtnWorkerHolder.hidden = true;
        self.viewCompleteIncompleteBtnEmployerHolder.hidden = true;
        self.viewDoneBtnEmployerHolder.hidden = true;
        self.viewYesNoBtnEmployerHolder.hidden = true;
        self.viewDoneBtnWorkerHolder.hidden = false;
        
        self.constraintLblTaskCompletedHolderView.constant = 40;
        self.constraintViewTaskPaymentDetailHolderHeight.constant = 0;
        self.constraintViewUserAdditionalInformationHeight.constant = 63;
        self.constraintViewRatingHolderHeight.constant = 0;
        
        self.viewPaymentDetailHolder.hidden = YES;
        self.viewUserAdditionalInformation.hidden = NO;
        self.viewRatingHolder.hidden = YES;
        self.btnAddRating.hidden = YES;
        self.viewLblTaskCompletedHolder.hidden = NO;
        self.lblTaskCompleted.hidden = NO;
        
        //        //task information
        //        self.lblCreditCardEndingIn.text = @"Credited to your account:";
        //        self.lblCreditCardLast4DigitHolder.text = [NSString stringWithFormat:@"$%0.2f",myAppDelegate.workerTaskDetail.taskDetailObj.price];
        //
        //        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
        //        [dateFormatter setDateFormat:dateFormatFull];
        //#pragma mark - TODO: update date here
        //        NSDate *date = [dateFormatter dateFromString:myAppDelegate.taskData.startDate];
        //        [dateFormatter setDateFormat:@"dd-MM-YY"];
        //        NSString *dateString = [dateFormatter stringFromDate:date];
        //
        //        self.lblPaymentSentDate.text = dateString;
        
    }else if ([userType isEqualToString:@"Employer"]){
        
        self.viewAcceptBtnWorkerHolder.hidden = true;
        self.viewCompleteIncompleteBtnEmployerHolder.hidden = true;
        self.viewDoneBtnEmployerHolder.hidden = false;
        self.viewYesNoBtnEmployerHolder.hidden = true;
        self.viewDoneBtnWorkerHolder.hidden = true;
        
        self.constraintViewTaskPaymentDetailHolderHeight.constant = 0;
        self.constraintViewUserAdditionalInformationHeight.constant = 63;
        self.constraintViewRatingHolderHeight.constant = 96;
        self.constraintLblTaskCompletedHolderView.constant = 40;
        
        self.viewLblTaskCompletedHolder.hidden = NO;
        self.viewPaymentDetailHolder.hidden = YES;
        self.viewUserAdditionalInformation.hidden = NO;
        self.viewRatingHolder.hidden = NO;
        self.btnAddRating.hidden = NO;
        self.lblTaskCompleted.hidden = NO;
        
        self.viewBottomHolder.backgroundColor = RGB(248, 248, 248);
        
        //        //task information
        //        self.lblCreditCardEndingIn.text = @"Credit Card Ending in:";
        //        if (![myAppDelegate.workerTaskDetail.cardDetail isEqualToString:@""]) {
        //            self.lblCreditCardLast4DigitHolder.hidden = YES;
        //        }else{
        //            self.lblCreditCardLast4DigitHolder.text = [myAppDelegate.workerTaskDetail.cardDetail substringWithRange:NSMakeRange(12, 4)];
        //        }
        //
        //        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
        //        [dateFormatter setDateFormat:dateFormatFull];
        //#pragma mark - TODO: update date here
        //        NSDate *date = [dateFormatter dateFromString:myAppDelegate.taskData.startDate];
        //        [dateFormatter setDateFormat:@"dd-MM-YY"];
        //        NSString *dateString = [dateFormatter stringFromDate:date];
        //
        //        self.lblPaymentSentDate.text = dateString;
        
    }
    
    [self.view layoutIfNeeded];
    
    self.constraintBottomHolderViewHeight.constant = self.viewRatingHolder.frame.size.height + self.viewUserAdditionalInformation.frame.size.height + self.viewUserInfoHolder.frame.size.height;
    
    [UIView animateWithDuration:0.4 animations:^{
        [self.view layoutIfNeeded];
    }completion:^(BOOL finished) {
        [GlobalFunction removeIndicatorView];
    }];
    
}


- (IBAction)taskInCompleteClicked:(id)sender {
    
    [self.viewRelistPopHolder setHidden:NO];
    self.lblRelist.hidden = NO;
    
    self.viewAcceptBtnWorkerHolder.hidden = true;
    self.viewCompleteIncompleteBtnEmployerHolder.hidden = true;
    self.viewDoneBtnEmployerHolder.hidden = true;
    self.viewYesNoBtnEmployerHolder.hidden = false;
    self.viewDoneBtnWorkerHolder.hidden = true;
    
}

- (IBAction)doneClicked_Employer:(id)sender {
    
    [self performSegue];
    
}

- (IBAction)doneClicked_Worker:(id)sender {
    
    [self performSegue];
    
}

- (IBAction)acceptTaskClicked_Worker:(id)sender {
    
    if (myAppDelegate.isBankInfoFilled) {
        
        [GlobalFunction addIndicatorView];
        
        [self performSelector:@selector(acceptTask) withObject:nil afterDelay:0.5];
        
    }else{
        
        if (isNoThanksClicked) {
            
            [GlobalFunction addIndicatorView];
            
            [self performSelector:@selector(acceptTask) withObject:nil afterDelay:0.5];
            
        }else{
            
            [GlobalFunction removeIndicatorView];
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"WOULD YOU LIKE PEOPLE TO PAY YOU?" message:@"In order to receive money via BUY TIMEE, we'll need to connect your Stripe account." delegate:self cancelButtonTitle:@"No Thanks" otherButtonTitles:@"Connect Stripe Account", nil];
            
            alert.tag = 1009;
            
            [alert show];
            
        }
        
    }
    
}

-(void)acceptTask{
    
    if (![[WebService shared] connected]) {
        
        [GlobalFunction removeIndicatorView];
        
        [[GlobalFunction shared] showAlertForMessage:@"Can't connect to internet, please check and try again."];
        
        return;
    }
    
    NSDictionary *dict = [[WebService shared] acceptTaskTaskid:myAppDelegate.taskData.taskid userid:myAppDelegate.userData.userid];
    
    if (dict != nil) {
        
        if ([[EncryptDecryptFile decrypt:[dict valueForKey:@"responseCode"]] isEqualToString:@"200"]) {
            
            [self updateAfter:@"Accept task" andDict:dict];
            
        }else{
            
            [GlobalFunction removeIndicatorView];
            
            [[GlobalFunction shared] showAlertForMessage:[EncryptDecryptFile decrypt:[dict valueForKey:@"responseMessage"]]];
            
        }
        
    }else{
        
        [GlobalFunction removeIndicatorView];
        
        [[GlobalFunction shared] showAlertForMessage:@"Server is down, please try again later."];
        
    }
    
}

- (IBAction)relistYesClicked:(id)sender {
    
    [GlobalFunction addIndicatorView];
    
    [self performSelector:@selector(doRelistYesWork) withObject:nil afterDelay:0.2];
}

-(void)doRelistYesWork{
    
    if (![[WebService shared] connected]) {
        
        [GlobalFunction removeIndicatorView];
        
        [[GlobalFunction shared] showAlertForMessage:@"Can't connect to internet, please check and try again."];
        
        return;
    }
    
    NSDictionary *dictTaskInfoResponse;
    
    if (myAppDelegate.isComingFromPushNotification) {
        dictTaskInfoResponse = [[WebService shared] employeeStatusTaskid:myAppDelegate.taskPushData.taskid status:@"0"];
    }else{
        dictTaskInfoResponse = [[WebService shared] employeeStatusTaskid:myAppDelegate.taskData.taskid status:@"0"];
    }
    
    if (dictTaskInfoResponse) {
        
        if ([[EncryptDecryptFile decrypt:[dictTaskInfoResponse valueForKey:@"responseCode"]] isEqualToString:@"200"]) {
            
            NSLog(@"response message == %@",[EncryptDecryptFile decrypt:[dictTaskInfoResponse valueForKey:@"responseMessage"]]);
            
            [self updateAfter:@"Relist YES" andDict:dictTaskInfoResponse];
            
        }else{
            
            [GlobalFunction removeIndicatorView];
            
            [[GlobalFunction shared] showAlertForMessage:[EncryptDecryptFile decrypt:[dictTaskInfoResponse valueForKey:@"responseMessage"]]];
            
        }
        
    }else{
        
        [GlobalFunction removeIndicatorView];
        
        [[GlobalFunction shared] showAlertForMessage:@"Server is down, please try again later."];
        
    }
    
}

- (IBAction)relistNoClicked:(id)sender {
    
    [GlobalFunction addIndicatorView];
    
    [self performSelector:@selector(relistNoStatusUpdate) withObject:nil afterDelay:0.5];
    
}

-(void)relistNoStatusUpdate{
    
    if (![[WebService shared] connected]) {
        
        [GlobalFunction removeIndicatorView];
        
        [[GlobalFunction shared] showAlertForMessage:@"Can't connect to internet, please check and try again."];
        
        return;
    }
    
    NSDictionary *dictTaskInfoResponse;
    
    if (myAppDelegate.isComingFromPushNotification) {
        dictTaskInfoResponse = [[WebService shared] employeeStatusTaskid:myAppDelegate.taskPushData.taskid status:@"4"];
    }else{
        dictTaskInfoResponse = [[WebService shared] employeeStatusTaskid:myAppDelegate.taskData.taskid status:@"4"];
    }
    
    if (dictTaskInfoResponse) {
        
        if ([[EncryptDecryptFile decrypt:[dictTaskInfoResponse valueForKey:@"responseCode"]] isEqualToString:@"200"]) {
            
            [self.viewRelistPopHolder setHidden:YES];
            self.lblRelist.hidden = YES;
            self.viewAcceptBtnWorkerHolder.hidden = true;
            self.viewCompleteIncompleteBtnEmployerHolder.hidden = false;
            self.viewDoneBtnEmployerHolder.hidden = true;
            self.viewYesNoBtnEmployerHolder.hidden = true;
            self.viewDoneBtnWorkerHolder.hidden = true;
            
            [self updateAfter:@"Relist NO" andDict:dictTaskInfoResponse];
            
        }else{
            
            [GlobalFunction removeIndicatorView];
            
            [[GlobalFunction shared] showAlertForMessage:[EncryptDecryptFile decrypt:[dictTaskInfoResponse valueForKey:@"responseMessage"]]];
            
        }
        
    }else{
        
        [GlobalFunction removeIndicatorView];
        
        [[GlobalFunction shared] showAlertForMessage:@"Server is down, please try again later."];
        
    }
    
}

- (IBAction)showUserLocation:(id)sender {
    
    if ([locationToShow isEqualToString:@""]) {
        
        [[GlobalFunction shared] showAlertForMessage:@"There is no location available for this user to show."];
        
        return;
        
        
    }
    
    myAppDelegate.mapViewFromClass = @"TaskDetail";
    
    CLLocationCoordinate2D coordinate = [[GlobalFunction shared] getLocationFromAddressString:locationToShow];
    
    [myAppDelegate.vcObj showRegionOnMapForCoordinates:[NSString stringWithFormat:@"%f,%f",coordinate.latitude,coordinate.longitude]];
    
}

- (IBAction)sendEmailToUser:(id)sender {
    
    if ([emailIdToSend isEqualToString:@""]) {
        
        [[GlobalFunction shared] showAlertForMessage:@"There is no contact email available for this user."];
        
        return;
    }
    
    [myAppDelegate.vcObj sendMailToEmail:emailIdToSend];
    
}

- (IBAction)contactUser:(id)sender {
    
    if ([telToCall isEqualToString:@""]) {
        
        [[GlobalFunction shared] showAlertForMessage:@"There is no contact number available for this user."];
        
        return;
    }
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel://%@",telToCall]]];
    
}

- (IBAction)submitFinalRating:(id)sender {
    
    
}


-(void)performSegue{
    
    if (myAppDelegate.isComingFromPushNotification) {
        [myAppDelegate.vcObj backFromPushNotificationView:myAppDelegate.vcObj.btnBackForPushNotificationView];
    }else{
        [myAppDelegate.vcObj backToMainView:myAppDelegate.vcObj.btnBack];
    }
    
    [GlobalFunction removeIndicatorView];
    
}


#pragma mark - collection view delegates
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    if (myAppDelegate.isComingFromPushNotification) {
        
        maxCounter = myAppDelegate.workerPushTaskDetail.taskDetailObj.taskImagesArray.count;
        
        return maxCounter;
        
    }else{
        
        if (myAppDelegate.isOfOtherUser) {
            
            maxCounter = myAppDelegate.workerOtherUserTaskDetail.taskDetailObj.taskImagesArray.count;
            
            return myAppDelegate.workerOtherUserTaskDetail.taskDetailObj.taskImagesArray.count;
            
        }else{
            
            if([myAppDelegate.comingFromClass isEqualToString:@"ViewAllTaskViewController"]){
                
                maxCounter = myAppDelegate.workerAllTaskDetail.taskDetailObj.taskImagesArray.count;
                
                return myAppDelegate.workerAllTaskDetail.taskDetailObj.taskImagesArray.count;
            }else{
                
                maxCounter = myAppDelegate.workerTaskDetail.taskDetailObj.taskImagesArray.count;
                
                return myAppDelegate.workerTaskDetail.taskDetailObj.taskImagesArray.count;
            }
        }
        
    }
    
    return 0;
}

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    // Adjust cell size for orientation
    return CGSizeMake(((SCREEN_WIDTH-40)/3) - 5,60);
    
}



- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    TaskDetailCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"TaskCell" forIndexPath:indexPath];
    
    cell.cellimageView.tag = indexPath.row;
    cell.cellimageView.clipsToBounds = YES;
    
    //************ Download user image *************************************************************************
    
    if (myAppDelegate.isComingFromPushNotification) {
        
        if (myAppDelegate.workerPushTaskDetail.taskDetailObj.imagesArray.count>0) {
            
            cell.acitivityIndicator.hidden=true;
            [cell.acitivityIndicator stopAnimating];
            
            cell.cellimageView.image = [myAppDelegate.workerPushTaskDetail.taskDetailObj.imagesArray objectAtIndex:indexPath.row];
            
            
        }else{
            
            NSString *fullPath=[myAppDelegate.workerPushTaskDetail.taskDetailObj.taskImagesArray objectAtIndex:indexPath.row];
            NSArray *parts = [fullPath componentsSeparatedByString:@"/"];
            NSString *imageName=[parts lastObject];
            NSArray *imageNameParts = [imageName componentsSeparatedByString:@"."];
            
            NSString *userImageName = [NSString stringWithFormat:@"%@_%@_%ld",[imageNameParts firstObject],myAppDelegate.workerPushTaskDetail.taskDetailObj.taskid,(long)indexPath.row];
            NSString *userImageNameWithJPEG = [NSString stringWithFormat:@"%@_%@_%ld.jpeg",[imageNameParts firstObject],myAppDelegate.workerPushTaskDetail.taskDetailObj.taskid,(long)indexPath.row];
            
            UIImage *img = [GlobalFunction loadImageOfLogoFromCacheFolder:userImageNameWithJPEG dirname:@"taskImages"];
            
            if (img) {
                
                cell.acitivityIndicator.hidden=true;
                [cell.acitivityIndicator stopAnimating];
                cell.cellimageView.image = img;
                
            }else{
                
                cell.acitivityIndicator.hidden=false;
                [cell.acitivityIndicator startAnimating];
                
                NSString *UrlStr =fullPath;
                
                NSString* webStringURL = [UrlStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                
                dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH,  0ul);
                dispatch_async(queue, ^{
                    //This is what you will load lazily
                    NSURL *imageURL=[NSURL URLWithString:webStringURL];
                    
                    NSData *images=[NSData dataWithContentsOfURL:imageURL];
                    
                    UIImage *img = [UIImage imageWithData:images];
                    
                    if (img) {
                        
                        [GlobalFunction saveimage:img imageName:userImageName dirname:@"taskImages"];
                        
                        dispatch_sync(dispatch_get_main_queue(), ^{
                            
                            TaskDetailCollectionViewCell *updateCell = (TaskDetailCollectionViewCell *)[collectionView cellForItemAtIndexPath:indexPath];
                            UIImageView *view = (UIImageView*)[updateCell viewWithTag:indexPath.row];
                            view.clipsToBounds=YES;
                            //                            UIImage *img = [UIImage imageWithData:images];
                            
                            updateCell.cellimageView.image = img;
                            
                            if (!self.viewImageViewAndActivityHolder.hidden) {
                                self.imgViewTaskImagesHolder.image = img;
                                self.activityIndicatorTaskDetailImages.hidden = YES;
                                [self.activityIndicatorTaskDetailImages stopAnimating];
                            }
                            
                            updateCell.acitivityIndicator.hidden=true;
                            [updateCell.acitivityIndicator stopAnimating];
                            
                        });
                        
                    }
                    
                });
                
            }
            
        }
        
    }else{
        
        if (myAppDelegate.isOfOtherUser) {
            
            if (myAppDelegate.workerOtherUserTaskDetail.taskDetailObj.imagesArray.count>0) {
                
                cell.acitivityIndicator.hidden=true;
                [cell.acitivityIndicator stopAnimating];
                
                cell.cellimageView.image = [myAppDelegate.workerOtherUserTaskDetail.taskDetailObj.imagesArray objectAtIndex:indexPath.row];
                
                
            }else{
                
                NSString *fullPath=[myAppDelegate.workerOtherUserTaskDetail.taskDetailObj.taskImagesArray objectAtIndex:indexPath.row];
                NSArray *parts = [fullPath componentsSeparatedByString:@"/"];
                NSString *imageName=[parts lastObject];
                NSArray *imageNameParts = [imageName componentsSeparatedByString:@"."];
                
                NSString *userImageName = [NSString stringWithFormat:@"%@_%@_%ld",[imageNameParts firstObject],myAppDelegate.workerOtherUserTaskDetail.taskDetailObj.taskid,(long)indexPath.row];
                NSString *userImageNameWithJPEG = [NSString stringWithFormat:@"%@_%@_%ld.jpeg",[imageNameParts firstObject],myAppDelegate.workerOtherUserTaskDetail.taskDetailObj.taskid,(long)indexPath.row];
                
                UIImage *img = [GlobalFunction loadImageOfLogoFromCacheFolder:userImageNameWithJPEG dirname:@"taskImages"];
                
                if (img) {
                    
                    cell.acitivityIndicator.hidden=true;
                    [cell.acitivityIndicator stopAnimating];
                    cell.cellimageView.image = img;
                    
                }else{
                    
                    cell.acitivityIndicator.hidden=false;
                    [cell.acitivityIndicator startAnimating];
                    
                    NSString *UrlStr =fullPath;
                    
                    NSString* webStringURL = [UrlStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                    
                    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH,  0ul);
                    dispatch_async(queue, ^{
                        //This is what you will load lazily
                        NSURL *imageURL=[NSURL URLWithString:webStringURL];
                        
                        NSData *images=[NSData dataWithContentsOfURL:imageURL];
                        
                        UIImage *img = [UIImage imageWithData:images];
                        
                        if (img) {
                            
                            [GlobalFunction saveimage:img imageName:userImageName dirname:@"taskImages"];
                            
                            dispatch_sync(dispatch_get_main_queue(), ^{
                                
                                TaskDetailCollectionViewCell *updateCell = (TaskDetailCollectionViewCell *)[collectionView cellForItemAtIndexPath:indexPath];
                                UIImageView *view = (UIImageView*)[updateCell viewWithTag:indexPath.row];
                                view.clipsToBounds=YES;
                                //                            UIImage *img = [UIImage imageWithData:images];
                                
                                updateCell.cellimageView.image = img;
                                
                                if (!self.viewImageViewAndActivityHolder.hidden) {
                                    self.imgViewTaskImagesHolder.image = img;
                                    self.activityIndicatorTaskDetailImages.hidden = YES;
                                    [self.activityIndicatorTaskDetailImages stopAnimating];
                                }
                                
                                updateCell.acitivityIndicator.hidden=true;
                                [updateCell.acitivityIndicator stopAnimating];
                                
                            });
                            
                        }
                        
                        
                    });
                    
                }
                
            }
            
            
        }else{
            
            if([myAppDelegate.comingFromClass isEqualToString:@"ViewAllTaskViewController"])
                
            {
                
                if (myAppDelegate.workerAllTaskDetail.taskDetailObj.imagesArray.count>0) {
                    
                    cell.acitivityIndicator.hidden=true;
                    [cell.acitivityIndicator stopAnimating];
                    
                    cell.cellimageView.image = [myAppDelegate.workerAllTaskDetail.taskDetailObj.imagesArray objectAtIndex:indexPath.row];
                    
                }else{
                    
                    NSString *fullPath=[myAppDelegate.workerAllTaskDetail.taskDetailObj.taskImagesArray objectAtIndex:indexPath.row];
                    NSArray *parts = [fullPath componentsSeparatedByString:@"/"];
                    NSString *imageName=[parts lastObject];
                    NSArray *imageNameParts = [imageName componentsSeparatedByString:@"."];
                    
                    NSString *userImageName = [NSString stringWithFormat:@"%@_%@_%ld",[imageNameParts firstObject],myAppDelegate.workerAllTaskDetail.taskDetailObj.taskid,(long)indexPath.row];
                    NSString *userImageNameWithJPEG = [NSString stringWithFormat:@"%@_%@_%ld.jpeg",[imageNameParts firstObject],myAppDelegate.workerAllTaskDetail.taskDetailObj.taskid,(long)indexPath.row];
                    
                    UIImage *img = [GlobalFunction loadImageOfLogoFromCacheFolder:userImageNameWithJPEG dirname:@"taskImages"];
                    
                    if (img) {
                        
                        cell.acitivityIndicator.hidden=true;
                        [cell.acitivityIndicator stopAnimating];
                        cell.cellimageView.image = img;
                        
                    }else{
                        
                        cell.acitivityIndicator.hidden=false;
                        [cell.acitivityIndicator startAnimating];
                        
                        NSString *UrlStr =fullPath;
                        
                        NSString* webStringURL = [UrlStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                        
                        dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH,  0ul);
                        dispatch_async(queue, ^{
                            //This is what you will load lazily
                            NSURL *imageURL=[NSURL URLWithString:webStringURL];
                            
                            NSData *images=[NSData dataWithContentsOfURL:imageURL];
                            
                            UIImage *img = [UIImage imageWithData:images];
                            
                            if (img) {
                                
                                [GlobalFunction saveimage:img imageName:userImageName dirname:@"taskImages"];
                                
                                dispatch_sync(dispatch_get_main_queue(), ^{
                                    
                                    TaskDetailCollectionViewCell *updateCell = (TaskDetailCollectionViewCell *)[collectionView cellForItemAtIndexPath:indexPath];
                                    UIImageView *view = (UIImageView*)[updateCell viewWithTag:indexPath.row];
                                    view.clipsToBounds=YES;
                                    
                                    updateCell.cellimageView.image = img;
                                    
                                    if (!self.viewImageViewAndActivityHolder.hidden) {
                                        self.imgViewTaskImagesHolder.image = img;
                                        self.activityIndicatorTaskDetailImages.hidden = YES;
                                        [self.activityIndicatorTaskDetailImages stopAnimating];
                                    }
                                    
                                    updateCell.acitivityIndicator.hidden=true;
                                    [updateCell.acitivityIndicator stopAnimating];
                                    
                                });
                                
                            }
                            
                            
                        });
                        
                    }
                    
                }
                
                
            }else{
                
                if (myAppDelegate.workerTaskDetail.taskDetailObj.imagesArray.count>0) {
                    
                    cell.acitivityIndicator.hidden=true;
                    [cell.acitivityIndicator stopAnimating];
                    
                    cell.cellimageView.image = [myAppDelegate.workerTaskDetail.taskDetailObj.imagesArray objectAtIndex:indexPath.row];
                    
                }else{
                    
                    NSString *fullPath=[myAppDelegate.workerTaskDetail.taskDetailObj.taskImagesArray objectAtIndex:indexPath.row];
                    NSArray *parts = [fullPath componentsSeparatedByString:@"/"];
                    NSString *imageName=[parts lastObject];
                    NSArray *imageNameParts = [imageName componentsSeparatedByString:@"."];
                    
                    NSString *userImageName = [NSString stringWithFormat:@"%@_%@_%ld",[imageNameParts firstObject],myAppDelegate.workerTaskDetail.taskDetailObj.taskid,(long)indexPath.row];
                    NSString *userImageNameWithJPEG = [NSString stringWithFormat:@"%@_%@_%ld.jpeg",[imageNameParts firstObject],myAppDelegate.workerTaskDetail.taskDetailObj.taskid,(long)indexPath.row];
                    
                    UIImage *img = [GlobalFunction loadImageOfLogoFromCacheFolder:userImageNameWithJPEG dirname:@"taskImages"];
                    
                    if (img) {
                        
                        cell.acitivityIndicator.hidden=true;
                        [cell.acitivityIndicator stopAnimating];
                        cell.cellimageView.image = img;
                        
                    }else{
                        
                        cell.acitivityIndicator.hidden=false;
                        [cell.acitivityIndicator startAnimating];
                        
                        NSString *UrlStr =fullPath;
                        
                        NSString* webStringURL = [UrlStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                        
                        dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH,  0ul);
                        dispatch_async(queue, ^{
                            //This is what you will load lazily
                            NSURL *imageURL=[NSURL URLWithString:webStringURL];
                            
                            NSData *images=[NSData dataWithContentsOfURL:imageURL];
                            
                            UIImage *img = [UIImage imageWithData:images];
                            
                            if (img) {
                                
                                [GlobalFunction saveimage:img imageName:userImageName dirname:@"taskImages"];
                                
                                dispatch_sync(dispatch_get_main_queue(), ^{
                                    
                                    TaskDetailCollectionViewCell *updateCell = (TaskDetailCollectionViewCell *)[collectionView cellForItemAtIndexPath:indexPath];
                                    UIImageView *view = (UIImageView*)[updateCell viewWithTag:indexPath.row];
                                    view.clipsToBounds=YES;
                                    
                                    updateCell.cellimageView.image = img;
                                    
                                    if (!self.viewImageViewAndActivityHolder.hidden) {
                                        self.imgViewTaskImagesHolder.image = img;
                                        self.activityIndicatorTaskDetailImages.hidden = YES;
                                        [self.activityIndicatorTaskDetailImages stopAnimating];
                                    }
                                    
                                    updateCell.acitivityIndicator.hidden=true;
                                    [updateCell.acitivityIndicator stopAnimating];
                                    
                                });
                                
                            }
                            
                            
                        });
                        
                    }
                    
                }
                
            }
            
        }
        
    }
    
    //*****************************************************************************
    
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    self.imgViewTaskImagesHolder.contentMode = UIViewContentModeScaleAspectFill;
    self.imgViewTaskImagesHolder.clipsToBounds = YES;
    
    indexCounter = indexPath.row;
    
    [self showPopupForIndexpath:indexPath];
    
}

- (IBAction)openUserProfile:(id)sender {
    
    
}



#pragma mark - pop up handling
-(void)showPopupForIndexpath:(NSIndexPath *)iPath{
    
    [self.view layoutIfNeeded];
    
    indexForTableRow = iPath.row;
    
    TaskDetailCollectionViewCell *tCell = (TaskDetailCollectionViewCell *)[self.collectionView cellForItemAtIndexPath:iPath];
    
    CGPoint center = [[self.collectionView cellForItemAtIndexPath:iPath] convertPoint:tCell.cellimageView.center toView:self.collectionView];
    
    CGPoint center2 = [self.collectionView convertPoint:center toView:nil];
    
    center2.y = center2.y - 72; // - self.scrollViewHolder.contentOffset.y;
    
    centerTogoBack = center2;
    
    self.viewImageViewAndActivityHolder.hidden = NO;
    
    self.viewBgForFullScreenImage.hidden = NO;
    
    self.viewBgForFullScreenImage.alpha = 0.1;
    
    self.viewImageViewAndActivityHolder.transform = CGAffineTransformMakeScale(0.1, 0.1);
    
    self.viewImageViewAndActivityHolder.alpha = 0.1;
    
    self.viewImageViewAndActivityHolder.center = centerTogoBack;
    
    [UIView animateWithDuration:0.5
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         
                         self.viewImageViewAndActivityHolder.alpha = 1.0;
                         
                         self.viewBgForFullScreenImage.alpha = 0.8;
                         
                         self.viewImageViewAndActivityHolder.transform = CGAffineTransformMakeScale(1.0, 1.0);
                         
                         CGPoint centerOfView = self.view.center;
                         
                         centerOfView.y = centerOfView.y - (72/2);
                         
                         self.viewImageViewAndActivityHolder.center = centerOfView;
                         
                         [self setImageToImageViewForIndex:indexCounter];
                         
                     } completion:^(BOOL finished) {
                         
                         
                     }];
    
    self.btnCloseFullView.hidden = NO;
}

#pragma mark - popup tap handling

//************************************************


- (IBAction)hideFullScreenImageView:(id)sender {
    
    self.viewImageViewAndActivityHolder.transform = CGAffineTransformMakeScale(1.0, 1.0);
    self.viewImageViewAndActivityHolder.alpha = 1.0;
    
    [UIView animateWithDuration:0.5
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         
                         self.viewImageViewAndActivityHolder.alpha = 0.1;
                         
                         self.viewBgForFullScreenImage.alpha = 0.1;
                         
                         self.viewImageViewAndActivityHolder.transform = CGAffineTransformMakeScale(0.1, 0.1);
                         
                         self.viewImageViewAndActivityHolder.center = centerTogoBack;
                         
                     } completion:^(BOOL finished) {
                         
                         self.viewBgForFullScreenImage.hidden = YES;
                         self.viewImageViewAndActivityHolder.hidden = YES;
                         self.btnCloseFullView.hidden = YES;
                         
                     }];
    
    
    indexCounter = 0;
    
}

- (IBAction)swipeToRight:(id)sender {
    
    [GlobalFunction SwipeLeft:self.imgViewTaskImagesHolder];
    
    //NSLog(@"pics count---%lu",(unsigned long)pics.count);
    
    
    indexCounter = indexCounter - 1;
    
    centerTogoBack.x = centerTogoBack.x - (((SCREEN_WIDTH-36)/3)-3);
    
    if(indexCounter>=0){
        
        [self setImageToImageViewForIndex:indexCounter];
        
    }else{
        
        centerTogoBack.x = centerTogoBack.x + (((SCREEN_WIDTH-36)/3)-3);
        
        self.viewImageViewAndActivityHolder.transform = CGAffineTransformMakeScale(1.0, 1.0);
        self.viewImageViewAndActivityHolder.alpha = 1.0;
        
        [UIView animateWithDuration:0.5
                              delay:0.0
                            options:UIViewAnimationOptionCurveEaseOut
                         animations:^{
                             
                             self.viewImageViewAndActivityHolder.alpha = 0.1;
                             
                             self.viewBgForFullScreenImage.alpha = 0.1;
                             
                             self.viewImageViewAndActivityHolder.transform = CGAffineTransformMakeScale(0.1, 0.1);
                             
                             self.viewImageViewAndActivityHolder.center = centerTogoBack;
                             
                         } completion:^(BOOL finished) {
                             
                             self.viewBgForFullScreenImage.hidden = YES;
                             self.viewImageViewAndActivityHolder.hidden = YES;
                             self.btnCloseFullView.hidden = YES;
                             
                         }];
        
        
        indexCounter = 0;
    }
    
}

- (IBAction)swipeToLeft:(id)sender {
    
    [GlobalFunction SwipeRight:self.imgViewTaskImagesHolder];
    
    //NSLog(@"pics count---%lu",(unsigned long)pics.count);
    
    indexCounter = indexCounter + 1;
    
    centerTogoBack.x = centerTogoBack.x + (((SCREEN_WIDTH-36)/3)-3);
    
    if(indexCounter<maxCounter){
        
        [self setImageToImageViewForIndex:indexCounter];
        
    }else{
        centerTogoBack.x = centerTogoBack.x - (((SCREEN_WIDTH-36)/3)-3);
        
        self.viewImageViewAndActivityHolder.transform = CGAffineTransformMakeScale(1.0, 1.0);
        self.viewImageViewAndActivityHolder.alpha = 1.0;
        
        [UIView animateWithDuration:0.5
                              delay:0.0
                            options:UIViewAnimationOptionCurveEaseOut
                         animations:^{
                             
                             self.viewImageViewAndActivityHolder.alpha = 0.1;
                             
                             self.viewBgForFullScreenImage.alpha = 0.1;
                             
                             self.viewImageViewAndActivityHolder.transform = CGAffineTransformMakeScale(0.1, 0.1);
                             
                             self.viewImageViewAndActivityHolder.center = centerTogoBack;
                             
                         } completion:^(BOOL finished) {
                             
                             self.viewBgForFullScreenImage.hidden = YES;
                             self.viewImageViewAndActivityHolder.hidden = YES;
                             self.btnCloseFullView.hidden = YES;
                             
                         }];
        
        
        indexCounter = 0;
    }
    
}

-(void)setImageToImageViewForIndex:(NSInteger)index{
    
    if (myAppDelegate.isComingFromPushNotification) {
        
        if (myAppDelegate.workerPushTaskDetail.taskDetailObj.imagesArray.count>0) {
            self.imgViewTaskImagesHolder.image =  [myAppDelegate.workerPushTaskDetail.taskDetailObj.imagesArray objectAtIndex:index];
            [self.activityIndicatorTaskDetailImages stopAnimating];
            self.activityIndicatorTaskDetailImages.hidden = YES;
        }else{
            NSString *fullPath=[myAppDelegate.workerPushTaskDetail.taskDetailObj.taskImagesArray objectAtIndex:index];
            NSArray *parts = [fullPath componentsSeparatedByString:@"/"];
            NSString *imageName=[parts lastObject];
            NSArray *imageNameParts = [imageName componentsSeparatedByString:@"."];
            
            NSString *userImageNameWithJPEG = [NSString stringWithFormat:@"%@_%@_%ld.jpeg",[imageNameParts firstObject],myAppDelegate.workerPushTaskDetail.taskDetailObj.taskid,(long)index];
            
            UIImage *img = [GlobalFunction loadImageOfLogoFromCacheFolder:userImageNameWithJPEG dirname:@"taskImages"];
            
            if (img) {
                
                self.imgViewTaskImagesHolder.image = img;
                [self.activityIndicatorTaskDetailImages stopAnimating];
                self.activityIndicatorTaskDetailImages.hidden = YES;
                
            }else{
                
                
            }
            
        }
        
    }else{
        
        if (myAppDelegate.isOfOtherUser) {
            
            if (myAppDelegate.workerOtherUserTaskDetail.taskDetailObj.imagesArray.count>0) {
                self.imgViewTaskImagesHolder.image =  [myAppDelegate.workerOtherUserTaskDetail.taskDetailObj.imagesArray objectAtIndex:index];
                [self.activityIndicatorTaskDetailImages stopAnimating];
                self.activityIndicatorTaskDetailImages.hidden = YES;
            }else{
                NSString *fullPath=[myAppDelegate.workerOtherUserTaskDetail.taskDetailObj.taskImagesArray objectAtIndex:index];
                NSArray *parts = [fullPath componentsSeparatedByString:@"/"];
                NSString *imageName=[parts lastObject];
                NSArray *imageNameParts = [imageName componentsSeparatedByString:@"."];
                
                NSString *userImageNameWithJPEG = [NSString stringWithFormat:@"%@_%@_%ld.jpeg",[imageNameParts firstObject],myAppDelegate.workerOtherUserTaskDetail.taskDetailObj.taskid,(long)index];
                
                UIImage *img = [GlobalFunction loadImageOfLogoFromCacheFolder:userImageNameWithJPEG dirname:@"taskImages"];
                
                if (img) {
                    
                    self.imgViewTaskImagesHolder.image = img;
                    [self.activityIndicatorTaskDetailImages stopAnimating];
                    self.activityIndicatorTaskDetailImages.hidden = YES;
                    
                }else{
                    
                    
                }
                
            }
        }else{
            
            if([myAppDelegate.comingFromClass isEqualToString:@"ViewAllTaskViewController"]){
                
                if (myAppDelegate.workerAllTaskDetail.taskDetailObj.imagesArray.count>0) {
                    
                    self.imgViewTaskImagesHolder.image =  [myAppDelegate.workerAllTaskDetail.taskDetailObj.imagesArray objectAtIndex:index];
                    [self.activityIndicatorTaskDetailImages stopAnimating];
                    self.activityIndicatorTaskDetailImages.hidden = YES;
                }
                else
                    
                {
                    NSString *fullPath=[myAppDelegate.workerAllTaskDetail.taskDetailObj.taskImagesArray objectAtIndex:index];
                    NSArray *parts = [fullPath componentsSeparatedByString:@"/"];
                    NSString *imageName=[parts lastObject];
                    NSArray *imageNameParts = [imageName componentsSeparatedByString:@"."];
                    
                    NSString *userImageNameWithJPEG = [NSString stringWithFormat:@"%@_%@_%ld.jpeg",[imageNameParts firstObject],myAppDelegate.workerAllTaskDetail.taskDetailObj.taskid,(long)index];
                    
                    UIImage *img = [GlobalFunction loadImageOfLogoFromCacheFolder:userImageNameWithJPEG dirname:@"taskImages"];
                    
                    if (img) {
                        self.imgViewTaskImagesHolder.image = img;
                        [self.activityIndicatorTaskDetailImages stopAnimating];
                        self.activityIndicatorTaskDetailImages.hidden = YES;
                    }else{
                    }
                    
                }
                
                
            }else{
                
                if (myAppDelegate.workerTaskDetail.taskDetailObj.imagesArray.count>0) {
                    self.imgViewTaskImagesHolder.image =  [myAppDelegate.workerTaskDetail.taskDetailObj.imagesArray objectAtIndex:index];
                    [self.activityIndicatorTaskDetailImages stopAnimating];
                    self.activityIndicatorTaskDetailImages.hidden = YES;
                }else{
                    
                    NSString *fullPath=[myAppDelegate.workerTaskDetail.taskDetailObj.taskImagesArray objectAtIndex:index];
                    NSArray *parts = [fullPath componentsSeparatedByString:@"/"];
                    NSString *imageName=[parts lastObject];
                    NSArray *imageNameParts = [imageName componentsSeparatedByString:@"."];
                    
                    NSString *userImageNameWithJPEG = [NSString stringWithFormat:@"%@_%@_%ld.jpeg",[imageNameParts firstObject],myAppDelegate.workerTaskDetail.taskDetailObj.taskid,(long)index];
                    
                    UIImage *img = [GlobalFunction loadImageOfLogoFromCacheFolder:userImageNameWithJPEG dirname:@"taskImages"];
                    
                    if (img) {
                        self.imgViewTaskImagesHolder.image = img;
                        [self.activityIndicatorTaskDetailImages stopAnimating];
                        self.activityIndicatorTaskDetailImages.hidden = YES;
                    }else{
                        
                    }
                    
                }
                
            }
            
        }
        
    }
    
}

- (IBAction)hideFullScreenImage:(id)sender {
    
    self.viewImageViewAndActivityHolder.transform = CGAffineTransformMakeScale(1.0, 1.0);
    self.viewImageViewAndActivityHolder.alpha = 1.0;
    
    [UIView animateWithDuration:0.5
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         
                         self.viewImageViewAndActivityHolder.alpha = 0.1;
                         
                         self.viewBgForFullScreenImage.alpha = 0.1;
                         
                         self.viewImageViewAndActivityHolder.transform = CGAffineTransformMakeScale(0.1, 0.1);
                         
                         self.viewImageViewAndActivityHolder.center = centerTogoBack;
                         
                     } completion:^(BOOL finished) {
                         
                         self.viewBgForFullScreenImage.hidden = YES;
                         self.viewImageViewAndActivityHolder.hidden = YES;
                         self.btnCloseFullView.hidden = YES;
                         
                     }];
    
    
    indexCounter = 0;
    
}

- (IBAction)removeRelistPopup:(id)sender {
    
    [self.viewRelistPopHolder setHidden:YES];
    self.lblRelist.hidden = YES;
    self.viewAcceptBtnWorkerHolder.hidden = true;
    self.viewCompleteIncompleteBtnEmployerHolder.hidden = false;
    self.viewDoneBtnEmployerHolder.hidden = true;
    self.viewYesNoBtnEmployerHolder.hidden = true;
    self.viewDoneBtnWorkerHolder.hidden = true;
    
}

- (IBAction)buyNowBtnClciked:(id)sender {
  
 [GlobalFunction addIndicatorView];
    

// [myAppDelegate.vcObj performSegueWithIdentifier:@"rateViewController" sender:_rateBtn];

    
 [self performSelector:@selector(checkPurchaseConditions) withObject:nil afterDelay:0.3];
    
    
    
    
//    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"BuyTimee" message:@"hello" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
//    
//    alertView.tag = 10111;
//    
//    [alertView show];

    
    
    
    
    
    
    
    
    
    //    if (myAppDelegate.isFromSkip) {
//        
//    
//        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"BuyTimee" message:@"You need to login first." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
//        
//        alertView.tag = 010;
//        
//        [alertView show];
//        
//        return;
//    }
//    
//    
//    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"BuyTimee" message:@"Are you sure to buy this reservation?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Buy", nil];
//    
//    alert.tag = 201;
//    
//    [alert show];
    
}

-(void)checkPurchaseConditions
{
    if (myAppDelegate.isFromSkip) {
        
        [GlobalFunction removeIndicatorView];
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"BUY TIMEE" message:@"You need to login first." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:@"Login",nil];
        
        alertView.tag = 010;
        
        [alertView show];
        
        return;
    }
    if(discVal>0)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"BUY TIMEE" message:@"Are you sure to buy this discount?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Buy", nil];
        
        alert.tag = 201;
        
        [alert show];
    }
    else
    {
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"BUY TIMEE" message:@"Are you sure to buy this appointment?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Buy", nil];
    
       // old one.. Are you sure to buy this reservation?
        
    alert.tag = 201;
    
    [alert show];
        
    }
}

-(void)purchaseNow
{
  
    
    if(discVal>0)
    {
          NSLog(@"Payment condition not checked because discount is available");
    }
    else
    {
        if([myAppDelegate.selectedPaymentTypeFromServer isEqualToString:@"0"])
        {
            
            [GlobalFunction removeIndicatorView];
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"BUY TIMEE" message:@"You need to choose your payment option on profile page, before purchase any appointment.." delegate:self cancelButtonTitle:@"Later" otherButtonTitles:@"Choose payment method.",nil];
            
            // old one.. You need to add your Cashout with PayPal information before creating the reservation.
            
             alert.tag = 1020;
            
            [alert show];
            
            return;
        }
       else if([myAppDelegate.selectedPaymentTypeFromServer isEqualToString:@"1"])
        {
            
            if (myAppDelegate.isPaymentFilled == false) {
                
                // [self closeSideBar:self.btnSideBarClose];
                
                  [GlobalFunction removeIndicatorView];
                
                 UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"BUY TIMEE" message:@"You need to fill your credit card information first, before purchase any appointment." delegate:self cancelButtonTitle:@"Later" otherButtonTitles:@"Go to fill credit card information", nil];
                
                // old one.. You need to add your Pay with PayPal information before purchasing the reservation.
                
                alert.tag = 10190;
                
                [alert show];
                
                return;
            }
        }
        else if([myAppDelegate.selectedPaymentTypeFromServer isEqualToString:@"2"])
        {
            if (myAppDelegate.isPaymentFilled == false) {
                
                // [self closeSideBar:self.btnSideBarClose];
                
                  [GlobalFunction removeIndicatorView];
                
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"BUY TIMEE" message:@"You need to add your Pay with PayPal information before purchase any appointment." delegate:self cancelButtonTitle:@"Later" otherButtonTitles:@"Pay with PayPal", nil];
                
                alert.tag = 1019;
                
                [alert show];
                
                return;
            }
            
        }


    }
   
//    NSDictionary *buyDict = [[WebService shared] purchaseReservationid:myAppDelegate.workerAllTaskDetail.taskDetailObj.taskid userid:myAppDelegate.workerAllTaskDetail.taskDetailObj.userid];
    
    NSString *timeZoneSeconds = [GlobalFunction getTimeZone];
    
    NSDictionary *buyDict = [[WebService shared] purchaseReservationid:myAppDelegate.workerAllTaskDetail.taskDetailObj.taskid userid:myAppDelegate.userData.userid LastModifyTime:[UserDefaults valueForKey:keyLastModifyTime] timeZone:timeZoneSeconds];   //ModifyTime:[UserDefaults valueForKey:keyLastModifyTime]
    
    
    if (buyDict) {
        
       NSLog(@"response --- %@",[EncryptDecryptFile decrypt:[buyDict valueForKey:@"responseMessage"]]);
        
        NSLog(@"response --- %@",[EncryptDecryptFile decrypt:[buyDict valueForKey:@"responseCode"]]);
        
        if ([[EncryptDecryptFile decrypt:[buyDict valueForKey:@"responseCode"]] isEqualToString:@"200"]) {
            
            
            NSDictionary *dictFromResponse = [buyDict objectForKey:@"data"];
            
            
            
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"BUY TIMEE" message:[EncryptDecryptFile decrypt:[buyDict valueForKey:@"responseMessage"]] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            
            alertView.tag = 10111;
            
            [alertView show];
            
            return;
            
        }
        else if ([[EncryptDecryptFile decrypt:[buyDict valueForKey:@"responseCode"]] isEqualToString:@"300"])
        {
            [GlobalFunction removeIndicatorView];
            
            UIAlertView *alertBuy=[[UIAlertView alloc] initWithTitle:@"BUY TIMEE" message:@"It looks like something has changed, please go back to listing to see updates." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            
            alertBuy.tag = 10101;
            [alertBuy show];
            return;

        }
        else{
            
            [[GlobalFunction shared] showAlertForMessage:[EncryptDecryptFile decrypt:[buyDict valueForKey:@"responseMessage"]]];
            
            [GlobalFunction removeIndicatorView];
            
        }
    }
    else
    {
        [[GlobalFunction shared] showAlertForMessage:@"Server is down, please try again later."];
        
        [GlobalFunction removeIndicatorView];
    }
    
    

}

-(void)callReportReservationAPI
{
    NSDictionary *dictMain = [[WebService shared] reportTaskid:myAppDelegate.workerAllTaskDetail.taskDetailObj.taskid userid:myAppDelegate.userData.userid];
    // [[GlobalFunction shared] callUpdateWebservice]
    
    if (dictMain) {
        
        NSLog(@"response --- %@",[EncryptDecryptFile decrypt:[dictMain valueForKey:@"responseMessage"]]);
        
        NSLog(@"response --- %@",[EncryptDecryptFile decrypt:[dictMain valueForKey:@"responseCode"]]);
        
        if ([[EncryptDecryptFile decrypt:[dictMain valueForKey:@"responseCode"]] isEqualToString:@"200"]) {
            
            [self bioUpdatedSuccefully1:[EncryptDecryptFile decrypt:[dictMain valueForKey:@"responseMessage"]]];
            
            NSString *userId = @"";
            userId =myAppDelegate.userData.userid;
            
            NSString* myLogEvent = @"";
            
            myLogEvent = myAppDelegate.workerAllTaskDetail.taskDetailObj.taskid ;
            
            // Capture author info &  user status
            NSDictionary *articleParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                           userId, @"UserId",
                                           myLogEvent, @"ReservationId",
                                           nil];
            
            
            [Flurry logEvent:@"Reservation Reported" withParameters:articleParams];
            NSLog(@"Dics is...%@",articleParams);
        }
        else
        {
            [[GlobalFunction shared] showAlertForMessage:[EncryptDecryptFile decrypt:[dictMain valueForKey:@"responseMessage"]]];
            
            [GlobalFunction removeIndicatorView];
            
        }
    }
    else
    {
        [[GlobalFunction shared] showAlertForMessage:@"Server is down, please try again later."];
        
        [GlobalFunction removeIndicatorView];
    }
}

- (IBAction)flagBtnClciked:(id)sender {
    
    if (!ispurchaseSuccessFull)
    {
        [GlobalFunction addIndicatorView];
        
      [self performSelector:@selector(callReportReservationAPI) withObject:nil afterDelay:0.2];
        
        eventAddedSuccessFully = false;
    }
    else
    {
        
        
        [GlobalFunction addIndicatorView];
        [self performSelector:@selector(eventHandling) withObject:nil afterDelay:0.1];
        
        
}
    
}

-(void)eventHandling
{
    NSLog(@"....%@",myAppDelegate.workerAllTaskDetail.taskDetailObj.startDate);
    NSLog(@"....%@",myAppDelegate.workerAllTaskDetail.taskDetailObj.title);
    NSLog(@"....%@",myAppDelegate.workerAllTaskDetail.taskDetailObj.locationName);
    NSLog(@"....%@",myAppDelegate.workerAllTaskDetail.taskDetailObj.endDate);
    
    if (eventAddedSuccessFully)
    {
       
        [GlobalFunction removeIndicatorView];
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"BUY TIMEE" message:@"You have already added this event in calendar." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        
        [alert show];
        return;
    }
    
    else
    {
    
    
    // NSString *dateTimeString = [NSString stringWithFormat:@"%@",myAppDelegate.workerAllTaskDetail.taskDetailObj.startDate];
    NSString *dateTimeString = [NSString stringWithFormat:@"%@%@%@",myAppDelegate.workerAllTaskDetail.taskDetailObj.startDate,@" ",myAppDelegate.workerAllTaskDetail.taskDetailObj.endDate];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
    NSDate *dateFromString = [[NSDate alloc] init];
    // voila!
    dateFromString = [dateFormatter dateFromString:dateTimeString];
    
    
    EKEventStore *store = [EKEventStore new];
    [store requestAccessToEntityType:EKEntityTypeEvent completion:^(BOOL granted, NSError *error) {
        
        if (!granted) {
            
            [GlobalFunction removeIndicatorView];
            
            
            return; }
        EKEvent *event = [EKEvent eventWithEventStore:store];
        
        event.title = myAppDelegate.workerAllTaskDetail.taskDetailObj.title;
        event.location = myAppDelegate.workerAllTaskDetail.taskDetailObj.locationName;
        event.startDate = dateFromString; //[[NSDate date] dateByAddingTimeInterval:60*2]; //today
        event.endDate =  [event.startDate dateByAddingTimeInterval:60*60];  //set 1 hour meeting
        event.calendar = [store defaultCalendarForNewEvents];
        NSError *err = nil;
        
        NSTimeInterval interval = 600* -1;
        EKAlarm *alarm = [EKAlarm alarmWithRelativeOffset:interval];
        [event addAlarm:alarm];
        //  [store saveEvent:event span:EKSpanThisEvent commit:YES error:&err];
        [store saveEvent:event span:EKSpanThisEvent commit:YES error:&err];
        //  self.savedEventId = event.eventIdentifier;  //save the event id if you want to access this later
        
        NSLog(@"Event identifier ---- %@",event.eventIdentifier);
        
        
        [GlobalFunction removeIndicatorView];
        
        eventAddedSuccessFully = true;
        
        
        
        // UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"BuyTimee" message:@"Event successfully added." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        //      [alert show];
        
        
    }];
    
    
    
    
    UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"BUY TIMEE" message:@"Event successfully added to your calendar." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    
    [alert show];
    
}
    
}

-(void)bioUpdatedSuccefully1:(NSString *)response{
    
    [GlobalFunction removeIndicatorView];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"BUY TIMEE" message:response delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    
    // alert.tag = 10001;
    
    [alert show];
    
}

- (IBAction)rateBtnClciked:(id)sender {
    
    [myAppDelegate.vcObj performSegueWithIdentifier:@"rateViewController" sender:_rateBtn];
    
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex{
    
    if (alertView.tag == 999) {
        
        if (buttonIndex == [alertView cancelButtonIndex]) {
            
            if ([afterWorkString isEqualToString:@"Accept task"]) {
                
                [self performSelector:@selector(performSegue) withObject:nil afterDelay:0.1];
                
                [[GlobalFunction shared] showAlertForMessage:@"Task accepted successfully."];
                
            }else if ([afterWorkString isEqualToString:@"Task complete"]){
                
                [self performTaskCompleteWorkFor:workerOrEmployer];
                
            }else if ([afterWorkString isEqualToString:@"Relist YES"] || [afterWorkString isEqualToString:@"Relist NO"]){
                
                NSString *messageString = @"";
                
                if ([afterWorkString isEqualToString:@"Relist YES"]) {
                    messageString = @"Task relisted successfully.";
                }else if ([afterWorkString isEqualToString:@"Relist NO"]){
                    messageString = @"Task marked as incomplete successfully.";
                }
                
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"BUY TIMEE" message:messageString delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                
                alertView.tag = 998;
                
                [alertView show];
                
            }
            
        }else{
            
            [GlobalFunction addIndicatorView];
            
            [self performSelector:@selector(updateWebServiceHandling) withObject:nil afterDelay:0.2];
            
        }
        
    }else if (alertView.tag == 998){
        
        if (buttonIndex == [alertView cancelButtonIndex]) {
            [self performSelector:@selector(performSegue) withObject:nil afterDelay:0.1];
        }
        
    }else if (alertView.tag == 1009){
        
        if (buttonIndex == [alertView cancelButtonIndex]) {
            
            return;
            isNoThanksClicked = true;
            
        }else{
            
            //  [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://connect.stripe.com/oauth/authorize?response_type=code&client_id=ca_8OAG2bzlaujVom8ENyjTHymERuAJajIT&scope=read_write"]]];
            
            //               [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://connect.stripe.com/oauth/authorize?response_type=code&client_id=ca_8OAG50wWYZgh78OmpC57dZNCiICFJhqm&scope=read_write"]]];    //live  old
            
            
            
            
            
            
            
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://connect.stripe.com/oauth/authorize?response_type=code&client_id=ca_8ihA2QrB60eKiyHjBtUE8DNtDf9uOrhY&scope=read_write"]]];    //live
            
            
            
            
        }
        
    }
    else if (alertView.tag == 10111){
        
        if (buttonIndex == [alertView cancelButtonIndex]) {
            
            
            [GlobalFunction removeIndicatorView];
//            [UIView transitionWithView:_buyNowBtn
//                              duration:2.4
//                               options:UIViewAnimationOptionCurveEaseOut
//                            animations:^{
//                                //_buyNowBtn.hidden=YES;
//                                _buyNowBtnHeight.constant=0;
//                                UIImage *calenderImage=[UIImage imageNamed:@"calenderImg"];//@"calenderImg"
//                                _rateBtn.hidden=NO;
//                                // [_flagBtn setBackgroundImage:calenderImage forState:UIControlStateNormal];
//                                [_flagBtn setImage:calenderImage forState:UIControlStateNormal];
//                                //  _flagBtn.hidden = YES;
//                                ispurchaseSuccessFull = true;
//                                
//                                _flagBtnWidth.constant=28;
//                                _flagBtnHeight.constant=22;
//                                
//                            }
//                            completion:NULL];
            

            
            
            CATransition *animation = [CATransition animation];
            animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
            animation.type = kCATransitionFade;
            animation.duration = 0.85;
            [_flagBtn.layer addAnimation:animation forKey:@"kCATransitionFade"];
            [_rateBtn.layer addAnimation:animation forKey:@"kCATransitionFade"];
            [_buyNowBtn.layer addAnimation:animation forKey:@"kCATransitionFade"];



                                            _buyNowBtnHeight.constant=0;
                                            UIImage *calenderImage=[UIImage imageNamed:@"calenderImg"];//@"calenderImg"
                                            _rateBtn.hidden=NO;
                                            [_flagBtn setImage:calenderImage forState:UIControlStateNormal];
                                            ispurchaseSuccessFull = true;
                                            _flagBtnWidth.constant=28;
                                            _flagBtnHeight.constant=22;
            
            NSString *userId = @"";
            userId =myAppDelegate.userData.userid;
            
            NSString* myLogEvent = @"";
            myLogEvent = myAppDelegate.workerAllTaskDetail.taskDetailObj.taskid;
            //
            //            NSString* final = [NSString stringWithFormat:@"%@%@", myLogEvent,myAppDelegate.userData.userid];
            //            [Flurry logEvent:final];
            
            NSDictionary *articleParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                           @"UserId", userId,
                                           @"ReservationId", myLogEvent,
                                           nil];
            
            [Flurry logEvent:@"Reservation purchased Successfully" withParameters:articleParams];
        }
    }
    else if (alertView.tag == 1019){
        
        if (buttonIndex == [alertView cancelButtonIndex]) {
 
        }
        else
        {
            // Comment below code bcz of add paypal integration by VS 12 July 18...
            
//            myAppDelegate.isSideBarAccesible = false;
//
//            myAppDelegate.isFromEdit = false;
//            // myAppDelegate.superViewClassName =@"new_create_bio";
//          //  [self performSegueWithIdentifier:@"paymentInfo" sender:myAppDelegate.vcObj.btnPaymentInfo];
//
//
//          [myAppDelegate.vcObj performSegueWithIdentifier:@"paymentInfo" sender:myAppDelegate.vcObj.btnPaymentInfo];
            
            //.... end code swich to card screen....
            
            [GlobalFunction addIndicatorView];
            
            [self startCheckout];
            
            
        }
    }
    else if (alertView.tag == 10190){
        
        if (buttonIndex == [alertView cancelButtonIndex]) {
            
        }
        else
        {
            // Comment below code bcz of add paypal integration by VS 12 July 18...
            
                        myAppDelegate.isSideBarAccesible = false;
            
                        myAppDelegate.isFromEdit = false;
                        // myAppDelegate.superViewClassName =@"new_create_bio";
                      //  [self performSegueWithIdentifier:@"paymentInfo" sender:myAppDelegate.vcObj.btnPaymentInfo];
            
            
                      [myAppDelegate.vcObj performSegueWithIdentifier:@"paymentInfo" sender:myAppDelegate.vcObj.btnPaymentInfo];
            
            //.... end code swich to card screen....
            
//            [GlobalFunction addIndicatorView];
//
//            [self startCheckout];
//
            
        }
    }
    else if (alertView.tag == 1020){
        
        if (buttonIndex == [alertView cancelButtonIndex]) {
            
        }
        else
        {

                myAppDelegate.isSideBarAccesible = true;
            
                [myAppDelegate.vcObj performSegueWithIdentifier:@"createBio" sender:myAppDelegate.vcObj.btnCreateBioSegue];
            
            
            
        }
    }
    else if (alertView.tag == 201){
        
        if (buttonIndex == [alertView cancelButtonIndex]) {
            
            [GlobalFunction removeIndicatorView];
        }
        else
        {
          //  [GlobalFunction addIndicatorView];
            
            [self purchaseNow];
            
        }
    }
    
    else if (alertView.tag == 010){
        
        if (buttonIndex == [alertView cancelButtonIndex]) {
            
            // [GlobalFunction removeIndicatorView];
        }
        else
        {
            
            {
                
                
                
                CATransition *transitionAnimation = [CATransition animation];
                [transitionAnimation setType:kCATransitionPush];
                [transitionAnimation setSubtype:kCATransitionFromLeft];
                [transitionAnimation setDuration:0.3f];
                [transitionAnimation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
                [transitionAnimation setFillMode:kCAFillModeBoth];
                
                
                
                //  [myAppDelegate.activeTaskVCObj.viewTaskDetailHolder.layer addAnimation:transitionAnimation forKey:@"changeView"];
                
                [myAppDelegate.vcObj.viewLoginContainer.layer addAnimation:transitionAnimation forKey:@"changeView"];
                
                
                for(UIView *view in myAppDelegate.vcObj.viewLoginContainer.subviews){
                    
                    [view removeFromSuperview];
                    
                }
                
                
                for(UIView *view in myAppDelegate.vcObj.viewHolderWithHeader.subviews){
                    
                    [view removeFromSuperview];
                    
                }
                
                [myAppDelegate.vcObj performSegueWithIdentifier:@"login" sender:myAppDelegate.loginVCObj];
                myAppDelegate.vcObj.btnBack.hidden = YES;
                myAppDelegate.vcObj.btnSideBarOpen.hidden = YES;
                // myAppDelegate.vcObj = nil;
                myAppDelegate.termsNconditionVCObj = nil;
                myAppDelegate.stringNameOfChildVC = @"";
                myAppDelegate.TandC_Buyee = @"";
                return;
                
                //            myAppDelegate.stringNameOfChildVC = @"BackToLogin";
                //
                //            [self backToMainView:nil];
                
            }
        }
    }
    
    
    else if (alertView.tag == 10101){
        
        if (buttonIndex == [alertView cancelButtonIndex]) {
            
            [myAppDelegate.vcObj backToMainView:nil];
        }
        else
        {
            //  [GlobalFunction addIndicatorView];
            
           // [self purchaseNow];
            
        }
    }

    
    
    
}




- (IBAction)flagTransBtnPressed:(id)sender {
    
    [self flagBtnClciked:self.flagTransBtn];
}
- (IBAction)crossBtnClciked:(id)sender {
    [UIView animateWithDuration:0.5
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         
                         _blurview.alpha=0.0;
                         
                     } completion:^(BOOL finished) {
                         
                         
                     }];

    [_webViewForURLData stopLoading];

}

- (void)sendAuthorizationToServer:(NSString *)dataid nonce:(NSString *)nonce
{
    // Send the entire authorization reponse
    
    // NSDictionary* projectDictionary = [authorization objectForKey:@"response"];
    //  projectDictionary1 = [projectDictionary objectForKey:@"code"];
    
    
    // metadataID = [PayPalMobile clientMetadataID];
    
    
    //   NSLog(@"meta data id is...%@",metadataID);
    //   NSLog(@"code is...%@",projectDictionary1);
    
    //  [self dismissViewControllerAnimated:YES completion:nil];
    //  backFromPayPal = true;
    //  [self back];
    // return;
    
    // [GlobalFunction addIndicatorView];
    
    [self performSelector:@selector(callAPI) withObject:nil afterDelay:0.1];
    
    
}

-(void)callAPI
{
    if (![[WebService shared] connected]) {
        
        [GlobalFunction removeIndicatorView];
        
        [[GlobalFunction shared] showAlertForMessage:@"Can't connect to internet, please check and try again."];
        
        return;
    }
    
    //return;
    NSDictionary *dictPaymentResponse = [[WebService shared] addPayPalUserid:myAppDelegate.userData.userid metaDataId:deviceData codeString:nonce];
    
    if (dictPaymentResponse)
    {
        NSLog(@"response --- %@",[EncryptDecryptFile decrypt:[dictPaymentResponse valueForKey:@"responseMessage"]]);
        
        if ([[EncryptDecryptFile decrypt:[dictPaymentResponse valueForKey:@"responseCode"]] isEqualToString:@"200"])
        {
            myAppDelegate.isPaymentFilled = true;
            myAppDelegate.selectedPaymentTypeFromServer = @"2";
            myAppDelegate.ispayWithPayPallFilled = true;
            myAppDelegate.isStripeCardInfoFilled = false;
            



            [self updatePaymentInfoSuccess:[EncryptDecryptFile decrypt:[dictPaymentResponse valueForKey:@"responseMessage"]]];
        }
        else{
            
            //  [[GlobalFunction shared] showAlertForMessage:[EncryptDecryptFile decrypt:[dictPaymentResponse valueForKey:@"responseMessage"]]];
            [self errorShow:[EncryptDecryptFile decrypt:[dictPaymentResponse valueForKey:@"responseMessage"]]];
            [GlobalFunction removeIndicatorView];
        }
        
    }
    else
    {
        [self errorShow:@"Server is down, please try again later."];
        
        // [[GlobalFunction shared] showAlertForMessage:@"Server is down, please try again later."];
        
        [GlobalFunction removeIndicatorView];
        
    }
    
}

-(void)updatePaymentInfoSuccess:(NSString *)response{
    
    [GlobalFunction removeIndicatorView];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"BUY TIMEE" message:response delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    
    // alert.tag = 10001;
    
    // alert.tag = 1111;
    
    
    [alert show];
    
}



@end
