//
//  TaskDetailCollectionViewCell.h
//  BuyTimee
//
//  Created by User14 on 03/04/16.
//  Copyright © 2016 User14. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TaskDetailCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *cellimageView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *acitivityIndicator;

@end
