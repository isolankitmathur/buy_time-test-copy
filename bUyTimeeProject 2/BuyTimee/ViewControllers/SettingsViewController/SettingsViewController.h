//
//  SettingsViewController.h
//  BuyTimee
//
//  Created by User14 on 15/03/16.
//  Copyright © 2016 User14. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingsViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIView *viewTopHolder;
@property (weak, nonatomic) IBOutlet UIView *viewCreditCardInfoHolder;

//outlets for user info
@property (weak, nonatomic) IBOutlet UIImageView *imgViewProfilePicture;
@property (weak, nonatomic) IBOutlet UILabel *lblUsername;
@property (weak, nonatomic) IBOutlet UILabel *lblUserAddress1;

- (IBAction)openMap:(id)sender;
- (IBAction)call:(id)sender;
- (IBAction)mail:(id)sender;


//outlets for credit card info
@property (weak, nonatomic) IBOutlet UILabel *lblNameOnCreditCard;
@property (weak, nonatomic) IBOutlet UIImageView *imgCardType;
@property (weak, nonatomic) IBOutlet UILabel *lblCreditCardNo;
@property (weak, nonatomic) IBOutlet UILabel *lblExpireyDate;
@property (weak, nonatomic) IBOutlet UILabel *lblCVVNo;

- (IBAction)editPaymentInfo:(id)sender;

-(void)updateUserData;

@end
