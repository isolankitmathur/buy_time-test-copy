//
//  SettingsViewController.m
//  BuyTimee
//
//  Created by User14 on 15/03/16.
//  Copyright © 2016 User14. All rights reserved.
//

#import "SettingsViewController.h"
#import "GlobalFunction.h"
#import "Constants.h"

@interface SettingsViewController ()

@property (weak, nonatomic) IBOutlet UIView *viewNoCreditCardAvailable;
@property (weak, nonatomic) IBOutlet UIView *viewNoSavedBankInfoAvailablr;
- (IBAction)animateImageToFullScreen:(id)sender;

@end

@implementation SettingsViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [GlobalFunction createBorderforView:self.viewCreditCardInfoHolder withWidth:0.5 cornerRadius:0.0 colorForBorder:RGB(151, 151, 151)];
    
    self.imgViewProfilePicture.userInteractionEnabled = true;
    
    [self updateUserData];
    
}

-(void)viewDidAppear:(BOOL)animated{
    
    [self updateUserData];
    
    [GlobalFunction removeIndicatorView];

}

-(void)updateUserData{
    
    self.viewNoCreditCardAvailable.hidden = YES;
    self.viewNoSavedBankInfoAvailablr.hidden = YES;
    
    if (myAppDelegate.userData) {
        
        if (myAppDelegate.userData.paymentData) {
            
            NSMutableArray *array = [NSMutableArray array];
            int j = 0;
            for (int i = 0; i < 4; i++) {
                NSString *ch = [myAppDelegate.userData.paymentData.card_number substringWithRange:NSMakeRange(j, 4)];
                j = j+4;
                [array addObject:ch];
            }
            
            //            self.lblCreditCardNo.text = [NSString stringWithFormat:@"%@-%@-%@-%@",[array objectAtIndex:0],[array objectAtIndex:1],[array objectAtIndex:2],[array objectAtIndex:3]];
            //            self.lblCVVNo.text = myAppDelegate.userData.paymentData.cvv_number;
            
            self.lblCreditCardNo.text = [NSString stringWithFormat:@"XXXX-XXXX-XXXX-%@",[array objectAtIndex:3]];

            self.lblExpireyDate.text = [NSString stringWithFormat:@"%@-%@",myAppDelegate.userData.paymentData.expiry_month,[myAppDelegate.userData.paymentData.expiry_year substringWithRange:NSMakeRange(2, 2)]];
            
            self.lblNameOnCreditCard.text = myAppDelegate.userData.paymentData.card_holder_name;
            
            if ([myAppDelegate.userData.paymentData.payment_method isEqualToString:@"0"]) {
                self.imgCardType.image = imageVisa;
            }else if ([myAppDelegate.userData.paymentData.payment_method isEqualToString:@"1"]) {
                self.imgCardType.image = imageMasterCard;
            }else if ([myAppDelegate.userData.paymentData.payment_method isEqualToString:@"2"]) {
                self.imgCardType.image = imageDiscover;
            }else if ([myAppDelegate.userData.paymentData.payment_method isEqualToString:@"3"]) {
                self.imgCardType.image = imageAmericanExpress;
            }
            
        }else{
            self.viewNoCreditCardAvailable.hidden = NO;
        }
        
        self.lblUserAddress1.text = myAppDelegate.userData.location;
        self.lblUsername.text = myAppDelegate.userData.username;
        
        if (myAppDelegate.userData.userImage) {
            self.imgViewProfilePicture.image = myAppDelegate.userData.userImage;
        }else{
            self.imgViewProfilePicture.image = imageUserDefault;
        }
        
        self.imgViewProfilePicture.layer.cornerRadius = self.imgViewProfilePicture.frame.size.height/2;
        self.imgViewProfilePicture.layer.masksToBounds = YES;
        
        if ([myAppDelegate.userData.username isEqualToString:@""]) {
            
        }
        
    }

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    
    myAppDelegate.vcObj.lblTitleText.text = @"Settings";

}


- (IBAction)openMap:(id)sender {
    
    if ([myAppDelegate.userData.location isEqualToString:@""]) {
        
        [[GlobalFunction shared] showAlertForMessage:@"There is no location available for this user to show."];
        
        return;
    }
    
    myAppDelegate.mapViewFromClass = @"Settings";
    
    CLLocationCoordinate2D coordinate = [[GlobalFunction shared] getLocationFromAddressString:myAppDelegate.userData.location];
    
    [myAppDelegate.vcObj showRegionOnMapForCoordinates:[NSString stringWithFormat:@"%f,%f",coordinate.latitude,coordinate.longitude]];
    
}

- (IBAction)call:(id)sender {
    
    if ([myAppDelegate.userData.mobile isEqualToString:@""]) {
        
        [[GlobalFunction shared] showAlertForMessage:@"There is no contact number available for this user."];
        
        return;
    }

    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel://%@",myAppDelegate.userData.mobile]]];

}

- (IBAction)mail:(id)sender {
    
    if ([myAppDelegate.userData.emailId isEqualToString:@""]) {
        
        [[GlobalFunction shared] showAlertForMessage:@"There is no contact email available for this user."];
        
        return;
    }
    
    [myAppDelegate.vcObj sendMailToEmail:myAppDelegate.userData.emailId];
    
}

- (IBAction)editPaymentInfo:(id)sender {
    
    myAppDelegate.isFromEdit = YES;
    
    if ([sender tag] == 1) {
        
        myAppDelegate.isSideBarAccesible = true;
        
        [myAppDelegate.vcObj performSegueWithIdentifier:@"paymentInfo" sender:myAppDelegate.vcObj.btnPaymentInfo];

    }else if ([sender tag] == 2){
        
        myAppDelegate.isSideBarAccesible = true;

        [myAppDelegate.vcObj performSegueWithIdentifier:@"bankinfo" sender:myAppDelegate.vcObj.btnBankInfo];

    }else if ([sender tag] == 3){
        
        myAppDelegate.isSideBarAccesible = false;

        [myAppDelegate.vcObj performSegueWithIdentifier:@"createBio" sender:myAppDelegate.vcObj.btnCreateBioSegue];

    }
    
}



- (IBAction)animateImageToFullScreen:(id)sender {

    [GlobalFunction animateImageview:self.imgViewProfilePicture];

}

@end
