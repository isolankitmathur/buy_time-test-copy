//
//  TaskHistoryViewController.h
//  HandApp
//


#import <UIKit/UIKit.h>
#import "Flurry.h"

@interface TaskHistoryViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UIView *viewTaskDetailHolder;
@property (weak, nonatomic) IBOutlet UITableView *tblTaskHistory;

-(void)reloadTable;

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *searchTxtFldTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *searchTxtFldHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lineLabelTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableViewHeight;
@property (weak, nonatomic) IBOutlet UITextField *searchTxtFld;

@property (weak, nonatomic) IBOutlet UIButton *allBtn;
@property (weak, nonatomic) IBOutlet UIButton *boughtBtn;
@property (weak, nonatomic) IBOutlet UIButton *soldBtn;
@property (weak, nonatomic) IBOutlet UIButton *listedBtn;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *allBtnWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *allBtnHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *allBtnBottom;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *boughtBtnHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *boughtBtnWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *boughtBtnBottom;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *soldBtnWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *soldBtnHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *soldBtnBottom;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *listedBtnWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *listedBtnHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *listedBtnBottom;

- (IBAction)allBtnClciked:(id)sender;
- (IBAction)boughtBtnClicked:(id)sender;
- (IBAction)soldBtnClciked:(id)sender;
- (IBAction)listedBtnClciked:(id)sender;
- (IBAction)requestedBtnClicked:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *btnRequested;

@property (nonatomic, retain) UITextField *txtActiveField;
@property (nonatomic, retain) UIView *inputAccView;
@property (nonatomic, retain) UIView *DatePickerInputAccView;
@property (nonatomic, retain) UIButton *btnDone;

@property (weak, nonatomic) IBOutlet UILabel *lineLbl;

- (IBAction)reviewBtnClciked:(id)sender;

@property (weak, nonatomic) IBOutlet UIView *btnBackgroundView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *btnBackgroundViewBottom;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *btnBackgroundViewHeight;
@property (weak, nonatomic) IBOutlet UILabel *lblNoTaskHistoryInfoAvailable;


@property (weak, nonatomic) IBOutlet UIView *reviewDetailView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consLblNoHistoryTop;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consBtnRequestedHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consBtnRequestedWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consBtnRequestedLeading;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consBtnRequestedBottom;



@end
