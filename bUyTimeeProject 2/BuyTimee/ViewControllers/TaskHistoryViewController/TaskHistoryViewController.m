//
//  TaskHistoryViewController.m
//  HandApp
//


#import "TaskHistoryViewController.h"
#import "TaskDetailTableViewCell.h"
#import "Constants.h"
#import "task.h"
#import "GlobalFunction.h"
#import "subCategory.h"
#import "myActivityTableViewCell.h"
#import "TaskTableViewCell.h"

@interface TaskHistoryViewController ()<UIGestureRecognizerDelegate>

//@property (weak, nonatomic) IBOutlet UILabel *lblNoTaskHistoryInfoAvailable;
- (IBAction)animateImageViewToFullScreen:(id)sender;
@property (strong, nonatomic) IBOutlet UITapGestureRecognizer *imgViewTapGesture;

@end

@implementation TaskHistoryViewController{
    
    NSMutableArray *arrayOfTaskWithStatus3;
    NSMutableArray *arrayOfAllHistoryData;
    BOOL isFromSearchResult;
    NSString *compareReviewStatus;
    
    BOOL isDeleteResponseYes;
    BOOL isEditTapped;
    NSString *taskId_ForDeleteReservation;
    
    NSString *requestStatus;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    
    
    //    _reviewDetailView.hidden=YES;
    
    myAppDelegate.isFromHistory_ForEdit = NO;
    
    myActivityTableViewCell *cell;
    
    cell.buttonForReviewDetail.hidden=NO;
    
    
    myAppDelegate.vcObj.btnMapOnListView.hidden = true;//vs
    myAppDelegate.vcObj.BtnTransparentMap.hidden = true;//vs
    
    
    NSAttributedString *str5 = [[NSAttributedString alloc] initWithString:@"Search" attributes:@{ NSForegroundColorAttributeName :[UIColor colorWithRed:111.0/255.0f green:119.0/255.0f blue:125.0/255.0f alpha:1.0],
                                                                                                  }];
    _searchTxtFld.attributedPlaceholder = str5;
    UIView *v1;
    self.searchTxtFld.delegate = self;
    
    //  self.searchTxtFld.enablesReturnKeyAutomatically = true;
    //  self.searchTxtFld.returnKeyType = UIReturnKeyDone;
    
    
    
    
    if(IS_IPHONE_6)
    {
        v1=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 13, 18)];
        
    }
    
    else if(IS_IPHONE_6P)
        
    {
        
        v1=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 13, 18)];
        
    }
    
    else if(IS_IPHONE_5 || IS_IPHONE_4_OR_LESS){
        
        v1=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 8, 18)];
        
        
    }
    
    self.searchTxtFld.leftView = v1;
    self.searchTxtFld.leftViewMode = UITextFieldViewModeAlways;
    [self addInputViewToTextField:self.searchTxtFld];
    
    
    [self.tableView setSeparatorColor:[UIColor colorWithRed:196.0/255.0f green:196.0/255.0f blue:196.0/255.0f alpha:1.0]];
    
    
    _tableView.delegate=self;
    _tableView.dataSource=self;
    
    [self.allBtn setSelected:YES];
    
    //    self.tblTaskHistory.delegate = self;
    //    self.tblTaskHistory.dataSource = self;
    //
    //    [self.tblTaskHistory layoutIfNeeded];
    //    [self.tblTaskHistory reloadData];
    //    [self.tblTaskHistory layoutIfNeeded];
    
    [self.tableView layoutIfNeeded];
    [self.tableView reloadData];
    [self.tableView layoutIfNeeded];
    
    // [self.tblTaskHistory setAllowsSelection:YES];
    // [self.tableView setAllowsSelection:YES];
    //self.tableView.backgroundColor = [UIColor clearColor];
    self.lblNoTaskHistoryInfoAvailable.text = @"No appointments are available currently.";
    
    // old one.. No reservations are available currently.
    
    self.lblNoTaskHistoryInfoAvailable.hidden = YES;
    // Do any additional setup after loading the view.
}

- (void)addInputViewToTextField:(UITextField *)textField{
    
    [self createInputAccessoryView];
    
    [textField setInputAccessoryView:_inputAccView];
    
}


-(void)createInputAccessoryView{
    // Create the view that will play the part of the input accessory view.
    // Note that the frame width (third value in the CGRectMake method)
    // should change accordingly in landscape orientation. But we don’t care
    // about that now.
    _inputAccView = [[UIView alloc] initWithFrame:CGRectMake(10.0, 0.0, self.view.frame.size.width, 40.0)];
    // Set the view’s background color. We’ ll set it here to gray. Use any color you want.
    [_inputAccView setBackgroundColor:[UIColor lightGrayColor]];
    // We can play a little with transparency as well using the Alpha property. Normally
    // you can leave it unchanged.
    [_inputAccView setAlpha: 0.8];
    // If you want you may set or change more properties (ex. Font, background image,e.t.c.).
    // For now, what we’ ve already done is just enough.
    // Let’s create our buttons now. First the previous button.
    // (as well as the methods for the rest of our buttons) later.
    _btnDone = [UIButton buttonWithType:UIButtonTypeCustom];
    [_btnDone setFrame:CGRectMake(self.view.frame.size.width - 80.0f, 0.0f, 80.0f, 40.0f)];
    [_btnDone setTitle:@"Done" forState:UIControlStateNormal];
    [_btnDone setBackgroundColor:[UIColor clearColor]];
    [_btnDone setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [_btnDone addTarget:self action:@selector(doneTyping) forControlEvents:UIControlEventTouchUpInside];
    // Now that our buttons are ready we just have to add them to our view.
    [_inputAccView addSubview:_btnDone];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return YES;
}


-(void)doneTyping
{
    [_searchTxtFld resignFirstResponder];
    
    
}

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    
    
    
    _txtActiveField = textField;
    
    [self createInputAccessoryView];
    [textField setInputAccessoryView:_inputAccView];
    
    
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    self.searchTxtFld.enablesReturnKeyAutomatically = true;
    
    NSString *value = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    if (![value isEqualToString:@""]) {
        // self.placeholder.hidden=true;
        [self filterContentForSearchText:value];//
        // scope:[[self.searchDisplayController.searchBar scopeButtonTitles]
        //      objectAtIndex:[self.searchDisplayController.searchBar
        //                    selectedScopeButtonIndex]]];
        
        isFromSearchResult = YES;
    }
    else{
        
        isFromSearchResult = NO;
        //  arrayOfTaskWithStatus3 = [arrayOfAllHistoryData mutableCopy];
        //  [self.tableView reloadData];
        
        [self performSelector:@selector(reloadTable) withObject:nil afterDelay:0.1];
    }
    
    // [_myContractTbleView reloadData];
    
    
    
    
    return true;
    
}
- (void)filterContentForSearchText:(NSString*)searchText //scope:(NSString*)scope
{
    
    
    NSPredicate *predicateTitle = [NSPredicate predicateWithFormat:@"title contains[cd] %@", searchText];
    NSPredicate *predicateLocation = [NSPredicate predicateWithFormat:@"locationName contains[cd] %@", searchText];
    //   NSPredicate *predicatename = [NSPredicate predicateWithFormat:@"creator_name contains[cd] %@", searchText];
    //  NSPredicate *predicateAcceptorname = [NSPredicate predicateWithFormat:@"acceptor_name contains[cd] %@", searchText];
    
    NSArray *subPredicates = [NSArray arrayWithObjects:predicateTitle, predicateLocation,nil];
    NSPredicate *orPredicate = [NSCompoundPredicate orPredicateWithSubpredicates:subPredicates];
    
    //searchResults = [arrUserData filteredArrayUsingPredicate:orPredicate];
    
    
    //  NSPredicate *predicateFName = [NSPredicate predicateWithFormat:@"SELF contains[cd] %@", searchText];
    
    arrayOfTaskWithStatus3 = [arrayOfAllHistoryData filteredArrayUsingPredicate:orPredicate];
    
    if (arrayOfTaskWithStatus3.count>0) {
        
    }else{
        arrayOfTaskWithStatus3 = [[NSArray alloc] init];
    }
    
    
    
    
    [self performSelector:@selector(reloadTable) withObject:nil afterDelay:0.1];
    
    
    //
    //    [self.tableView layoutIfNeeded];
    //    [self.tableView reloadData];
    //    [self.tableView layoutIfNeeded];
}


-(void)viewWillAppear:(BOOL)animated
{
    [self manageConstraints];
}

-(void)viewDidAppear:(BOOL)animated{
    
    
    NSString* final = @"Reservation history page";
    
    [Flurry logEvent:final];
    
    if (myAppDelegate.isComingBackAfterMapOpen) {
        
        myAppDelegate.isComingBackAfterMapOpen = false;
        
    }else{
        
        //  myAppDelegate.vcObj.lblTitleText.text = @"B U Y  T I M E E";
        
        //  [GlobalFunction addIndicatorView];
        
        // [self performSelector:@selector(performUpdateWork) withObject:nil afterDelay:0.2];
        
    }
    
    
    
    [self performSelector:@selector(performWebWork) withObject:nil afterDelay:0.2];
    
    
    if (myAppDelegate.isFromPushNotification)
    {
        [self performSelector:@selector(fetchTaskInfo) withObject:nil afterDelay:0.2];
    }
    
    
}

-(void)performWebWork
{
    if (![self checkForInternet]) {
        
        
        [[GlobalFunction shared] showAlertForMessage:@"Can't connect to internet, please check and try again."];
        
        [self performSelector:@selector(reloadTable) withObject:nil afterDelay:0.2];
        
        return;
    }
    
    else
    {
        [GlobalFunction addIndicatorView];
        
        NSString *timeZoneSeconds = [GlobalFunction getTimeZone];
        
        
        NSDictionary *reservationData = [[WebService shared] reservationHistoryUserid:myAppDelegate.userData.userid timeZone:timeZoneSeconds];
        // NSDictionary *reservationData = [[WebService shared] reservationHistoryUserid:@"5" timeZone:timeZoneSeconds];
        
        
        if (reservationData != nil) {
            
            if ([[EncryptDecryptFile decrypt:[reservationData valueForKey:@"responseCode"]] isEqualToString:@"200"]) {
                
                
                NSDictionary *dictDataReg = [reservationData objectForKey:@"data"];
                
                NSString *reviewCount1 = [EncryptDecryptFile decrypt:[dictDataReg valueForKey:@"review"]];
                
                myAppDelegate.reviewCounter_ForHamburger = reviewCount1;
                
                [myAppDelegate.vcObj.tblViewSideBar reloadData]; // for update review counter in hamburger...
                
                [self manageDataFromServer:dictDataReg];
                
                NSLog(@"count of all array is...%lu",(unsigned long)arrayOfAllHistoryData.count);
                [self performSelector:@selector(reloadTable) withObject:nil afterDelay:0.2];
            }
            else{
                
                [GlobalFunction removeIndicatorView];
                
                [[GlobalFunction shared] showAlertForMessage:[EncryptDecryptFile decrypt:[reservationData valueForKey:@"responseMessage"]]];
                [self performSelector:@selector(reloadTable) withObject:nil afterDelay:0.2];
            }
            
        }
        else{
            
            [GlobalFunction removeIndicatorView];
            
            [[GlobalFunction shared] showAlertForMessage:@"Server is down, please try again later."];
            [self performSelector:@selector(reloadTable) withObject:nil afterDelay:0.2];
            
        }
    }
    
}

-(void)manageDataFromServer:(NSDictionary *)dict;
{
    arrayOfAllHistoryData = [[NSMutableArray alloc]init];
    
    NSArray *arrayTerms= [dict valueForKey:@"reservation"];
    
    //  NSDictionary *dictTemp0=[arrayTerms objectAtIndex:0];
    NSDictionary *dictTemp0=[arrayTerms mutableCopy];
    
    NSMutableArray *array1 = [[NSMutableArray alloc]init];
    
    task *data;
    
    // NSDictionary *dictTemp1=[ParsingClass parseTaskDict:dict];
    
    for (id main in dictTemp0)
    {
        data=[[task alloc]init];
        data.taskid = [EncryptDecryptFile decrypt:[main valueForKey:@"reservationId"]];
        data.taskid_Integer = [data.taskid integerValue];
        
        data.startDate = [EncryptDecryptFile decrypt:[main valueForKey:@"startDate"]];
        
        
        data.picpath = [EncryptDecryptFile decrypt:[main valueForKey:@"picPath"]];
        data.locationName = [EncryptDecryptFile decrypt:[main valueForKey:@"locationName"]];
        data.endDate = [EncryptDecryptFile decrypt:[main valueForKey:@"startTime"]];
        
        data.title = [EncryptDecryptFile decrypt:[main valueForKey:@"title"]];
        
        NSString *str = [EncryptDecryptFile decrypt: [main valueForKey:@"price"]];
        data.discountValue = [EncryptDecryptFile decrypt:[main valueForKey:@"discount_status"]];
        data.price = [str floatValue];
        
        data.reservationType_ForHistory = [EncryptDecryptFile decrypt: [main valueForKey:@"reservationType"]];
        NSLog(@"reservationType_ForHistory is %@",data.reservationType_ForHistory);
        data.reservationStatus_ForHistory = [EncryptDecryptFile decrypt: [main valueForKey:@"reservationStatus"]];
        NSLog(@"reservationStatus_ForHistory is %@",data.reservationStatus_ForHistory);
        
        
        
        data.reviewStatus_ForHistory = [EncryptDecryptFile decrypt: [main valueForKey:@"reviewStatus"]];
        data.editStatus = [EncryptDecryptFile decrypt: [main valueForKey:@"editStatus"]];
        data.feeValue = [EncryptDecryptFile decrypt: [main valueForKey:@"fee"]];
        
        NSString* purchaseID  =  [main valueForKey:@"purchaseId"];
        if(purchaseID)
        {
            NSString *dPurchaseID = [EncryptDecryptFile decrypt:purchaseID];
            if(![dPurchaseID isEqual:@""])
            {
                data.purchaseID = dPurchaseID;
                NSLog(@"purchaseID is this %@",dPurchaseID);
            }
            
            
        }
        
        //data.reviewStatus_ForHistory = @"2";
        NSLog(@"review status manage data from server is...%@",data.reviewStatus_ForHistory);
        NSLog(@"edit status manage data from server is...%@",data.reviewStatus_ForHistory);
        NSLog(@"Fee value is...%@",data.feeValue);
        
        [array1 addObject:data];
    }
    
    arrayOfAllHistoryData = array1;
}


-(void)performUpdateWork{
    
    if (![self checkForInternet]) {
        
        [self performSelector:@selector(reloadTable) withObject:nil afterDelay:0.2];
        
        return;
    }
    
    if ([[GlobalFunction shared] callUpdateWebservice]) {
        
        [self performSelector:@selector(reloadTable) withObject:nil afterDelay:0.2];
        
    }else{
        
        [GlobalFunction removeIndicatorView];
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"BUY TIMEE" message:@"Failed to update, please try again." delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Retry", nil];
        
        alertView.tag = 999;
        
        [alertView show];
        
    }
    
}

-(void)updateWebServiceHandling{
    
    if (![self checkForInternet]) {
        
        [self performSelector:@selector(reloadTable) withObject:nil afterDelay:0.2];
        
        return;
    }
    
    if ([[GlobalFunction shared] callUpdateWebservice]) {
        
        [self performSelector:@selector(reloadTable) withObject:nil afterDelay:0.2];
        
    }else{
        
        [GlobalFunction removeIndicatorView];
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"BUY TIMEE" message:@"Failed to update, please try again." delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Retry", nil];
        
        alertView.tag = 999;
        
        [alertView show];
        
    }
    
}

-(BOOL)checkForInternet{
    
    BOOL isConnected = true;
    
    if (![[WebService shared] connected]) {
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"BUY TIMEE" message:@"Can't connect to internet, update not successful. Please check your internet connection then try again." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        [alertView show];
        
        isConnected = false;
        
    }
    
    return isConnected;
}

-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex{
    
    if (alertView.tag == 999) {
        
        if (buttonIndex == [alertView cancelButtonIndex]) {
            
            [self performSelector:@selector(reloadTable) withObject:nil afterDelay:0.2];
            
        }else{
            
            [GlobalFunction addIndicatorView];
            
            [self performSelector:@selector(updateWebServiceHandling) withObject:nil afterDelay:0.2];
            
        }
        
    }
    
}


-(void)reloadTable{
    
    if (!(arrayOfAllHistoryData.count>0)) {
        
        //self.tblTaskHistory.hidden = YES;
        self.tableView.hidden = YES;
        self.lblNoTaskHistoryInfoAvailable.hidden = NO;
        self.lblNoTaskHistoryInfoAvailable.alpha = 0.0;
        [UIView animateWithDuration:0.3 animations:^{
            self.lblNoTaskHistoryInfoAvailable.alpha = 1.0;
        }];
        
    }else{
        
        if (myAppDelegate.isComingFromPushNotification) {
            
            // temprary comment by vs for handle work of history. we will uncommrnt it when work in pushnotification.
            //arrayOfTaskWithStatus3 = [GlobalFunction filterTaskForUserID:myAppDelegate.userPushOtherData.userid taskStatus:TMTaskHistory];
            
        }else{
            
            if (myAppDelegate.isOfOtherUser) {
                arrayOfTaskWithStatus3 = [GlobalFunction filterTaskForUserID:myAppDelegate.userOtherData.userid taskStatus:TMTaskHistory];
            }else{
                //arrayOfTaskWithStatus3 = [GlobalFunction filterTaskForUserID:myAppDelegate.userData.userid taskStatus:TMTaskHistory];
                
                if (isFromSearchResult) {
                    if (self.allBtn.selected) {
                        arrayOfTaskWithStatus3 =  [self filterTaskFortaskStatussearch:@"5"];
                    }
                    else if (self.soldBtn.selected)
                    {
                        arrayOfTaskWithStatus3 =  [self filterTaskFortaskStatussearch:@"2"];
                    }
                    else if (self.boughtBtn.selected)
                    {
                        arrayOfTaskWithStatus3 =  [self filterTaskFortaskStatussearch:@"1"];
                    }
                    else if (self.listedBtn.selected)
                    {
                        arrayOfTaskWithStatus3 =  [self filterTaskFortaskStatussearch:@"0"];
                    }
                    else if (self.btnRequested.selected)
                    {
                        arrayOfTaskWithStatus3 =  [self filterTaskFortaskStatus:@"63"];
                    }
                    
                }
                else
                {
                    
                    if (self.allBtn.selected) {
                        //  arrayOfTaskWithStatus3 =  arrayOfAllHistoryData;
                        arrayOfTaskWithStatus3 =  [self filterTaskFortaskStatus:@"5"];
                    }
                    else if (self.soldBtn.selected)
                    {
                        arrayOfTaskWithStatus3 =  [self filterTaskFortaskStatus:@"2"];
                    }
                    else if (self.boughtBtn.selected)
                    {
                        arrayOfTaskWithStatus3 =  [self filterTaskFortaskStatus:@"1"];
                    }
                    else if (self.listedBtn.selected)
                    {
                        arrayOfTaskWithStatus3 =  [self filterTaskFortaskStatus:@"0"];
                    }
                    else if (self.btnRequested.selected)
                    {
                        arrayOfTaskWithStatus3 =  [self filterTaskFortaskStatus:@"63"];
                    }
                }
            }
        }
        
        //        [self.tblTaskHistory layoutIfNeeded];
        //        [self.tblTaskHistory reloadData];
        //        [self.tblTaskHistory layoutIfNeeded];
        [self.tableView layoutIfNeeded];
        [self.tableView reloadData];
        [self.tableView layoutIfNeeded];
        if (arrayOfTaskWithStatus3.count>0) {
            // self.tblTaskHistory.hidden = NO;
            self.tableView.hidden = NO;
            self.lblNoTaskHistoryInfoAvailable.hidden = YES;
            
        }else{
            // self.tblTaskHistory.hidden = YES;
            self.tableView.hidden = YES;
            self.lblNoTaskHistoryInfoAvailable.hidden = NO;
            self.lblNoTaskHistoryInfoAvailable.alpha = 0.0;
            [UIView animateWithDuration:0.3 animations:^{
                self.lblNoTaskHistoryInfoAvailable.alpha = 1.0;
            }];
            
        }
    }
    
    //    [self.tblTaskHistory layoutIfNeeded];
    //    [self.tblTaskHistory reloadData];
    //    [self.tblTaskHistory layoutIfNeeded];
    [self.tableView layoutIfNeeded];
    [self.tableView reloadData];
    [self.tableView layoutIfNeeded];
    [GlobalFunction removeIndicatorView];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return arrayOfTaskWithStatus3.count;
    
    // return 5;
    
}

-(void)SetColor:(myActivityTableViewCell *)cell{
    
    NSString *strMain2=cell.lblPrice.text;
    
    
    NSRange startRange = [strMain2 rangeOfString:@"$"];
    NSRange endRange = NSMakeRange(startRange.location, strMain2.length  - startRange.location);
    
    NSMutableAttributedString * string = [[NSMutableAttributedString alloc] initWithString:strMain2];
    [string addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:214/255.0 green:52/255.0 blue:47/255.0 alpha:1.0] range:endRange];
    cell.lblPrice.attributedText = string;
    
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    myActivityTableViewCell *cell = (myActivityTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"cell"];
    
    task *taskObj = (task *)[arrayOfTaskWithStatus3 objectAtIndex:indexPath.row];
    
    //    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    //    NSDate *date = [dateFormatter dateFromString:taskObj.startDate];
    //    [dateFormatter setDateFormat:@"YY/MM/dd"];
    //    NSString *dateString = [dateFormatter stringFromDate:date];
    
    
    [cell.reviewBtn setUserInteractionEnabled:NO];
    cell.nameLbl.text = taskObj.title;
    
    
    cell.dateLbl.text = taskObj.startDate;
    
    
    
    NSString *locationName = taskObj.locationName;
    
    NSString *priceMain;
    
    NSLog(@"discount status is...%@",taskObj.discountValue);
    NSLog(@"discount status is...%f",taskObj.price);
    NSLog(@"fee value is...%@",taskObj.feeValue);
    if (IS_IPHONE_5 || IS_IPHONE_4_OR_LESS)
    {
        [cell.lblPrice setFont:[UIFont fontWithName:@"Lato-Regular" size:13]];
    }
    
    
    
    if([taskObj.discountValue isEqualToString:@"1"])
    {
        // cell.lblTaskPrice.textColor = [UIColor colorWithRed:214/255.0 green:52/255.0 blue:47/255.0 alpha:1.0];
        
        if ([GlobalFunction checkIfContainFloat:taskObj.price]) {
            priceMain =  [NSString stringWithFormat:@"$%0.2f off",taskObj.price];
        }else{
            priceMain = [NSString stringWithFormat:@"$%0.0f off",taskObj.price];
        }
        //        NSString *strMain2 = [@[locationName, priceMain] componentsJoinedByString:@"  "];
        //  cell.locationLbl.text=strMain2;
        cell.locationLbl.text=locationName;
        //        cell.lblPrice.text = priceMain;
        
        NSRange startRange = [priceMain rangeOfString:@"$"];
        NSRange endRange = NSMakeRange(startRange.location, priceMain.length  - startRange.location);
        
        NSMutableAttributedString * string = [[NSMutableAttributedString alloc] initWithString:priceMain];
        [string addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:214/255.0 green:52/255.0 blue:47/255.0 alpha:1.0] range:endRange];
        cell.lblPrice.attributedText = string;
        
        //        [self performSelector:@selector(SetColor:) withObject:cell afterDelay:0.001];
    }
    else
    {
        NSString *addStr = taskObj.feeValue;
        
        if ([addStr isEqualToString:@"0"])
        {
            if ([GlobalFunction checkIfContainFloat:taskObj.price]) {
                priceMain =  [NSString stringWithFormat:@"$%0.2f",taskObj.price];
            }else{
                priceMain = [NSString stringWithFormat:@"$%0.0f",taskObj.price];
            }
            
            cell.locationLbl.text=locationName;
            
            
            NSAttributedString *priceText = [[NSAttributedString alloc] initWithString:priceMain
                                                                            attributes:@{ NSForegroundColorAttributeName: [UIColor colorWithRed:63.0/255.0f green:67.0/255.0f blue:70.0/255.0f alpha:0.9],NSFontAttributeName: [UIFont fontWithName:@"Lato-Regular" size:10.0f] }];
            
            if ([taskObj.reservationType_ForHistory isEqualToString:@"63"])
            {
                NSString *space = @" ";
                space = [space stringByAppendingString:priceMain];
                cell.lblPrice.text = space;
                //cell.lblPrice.backgroundColor = [UIColor lightGrayColor];
                //cell.locationLbl.backgroundColor = [UIColor greenColor];
                
                
            }
            else
            {
                cell.lblPrice.attributedText = priceText;
            }
        }
        else
        {
            //            if ([GlobalFunction checkIfContainFloat:taskObj.price]) {
            //                priceMain =  [NSString stringWithFormat:@"$%0.2f",taskObj.price];
            //            }else{
            //                priceMain = [NSString stringWithFormat:@"$%0.0f",taskObj.price];
            //            }
            //
            //            cell.locationLbl.text=locationName;
            //            cell.lblPrice.text = priceMain;
            
            if ([GlobalFunction checkIfContainFloat:taskObj.price]) {
                priceMain =  [NSString stringWithFormat:@"$%0.2f",taskObj.price];
            }else{
                priceMain = [NSString stringWithFormat:@"$%0.0f",taskObj.price];
            }
            NSString *strMain2 = [@[priceMain,addStr] componentsJoinedByString:@" "];
            
            NSRange startRange = [strMain2 rangeOfString:@"("];
            NSRange endRange = NSMakeRange(startRange.location, strMain2.length  - startRange.location);
            
            NSMutableAttributedString * string = [[NSMutableAttributedString alloc] initWithString:strMain2];
            
            [string setAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"Lato-Regular" size:10.0f], NSForegroundColorAttributeName:[UIColor colorWithRed:63.0/255.0f green:67.0/255.0f blue:70.0/255.0f alpha:0.9]} range:endRange];
            
            //        NSMutableAttributedString * string = [[NSMutableAttributedString alloc] initWithString:strMain2];
            //        [string addAttribute:NSFontAttributeName
            //                       value:[UIFont fontWithName:@"Lato-Regular" size:11] range:endRange];
            
            
            cell.locationLbl.text=locationName;
            
            
            cell.lblPrice.attributedText = string;
            
        }
        
    }
    //    if ([taskObj.reservationType_ForHistory isEqualToString:@"6"])
    //    {
    //         cell.lblPrice.text = @"";
    //    }
    
    cell.purchasedLbl.text = taskObj.reservationStatus_ForHistory;
    
    NSLog(@"Review status is...%@",taskObj.reviewStatus_ForHistory);
    
    if ([taskObj.reviewStatus_ForHistory isEqualToString:@"1"]) {
        cell.reviewBtn.hidden = false;
        cell.reviewLine.hidden = false;
        
        //        if ([GlobalFunction checkIfContainFloat:taskObj.price]) {
        //            priceMain =  [NSString stringWithFormat:@"$%0.2f off",taskObj.price];
        //        }else{
        //            priceMain = [NSString stringWithFormat:@"$%0.0f off",taskObj.price];
        //        }
        //        cell.lblPrice.text = priceMain;
        
        
        
    }
    else
    {
        
        
        cell.reviewBtn .hidden = true;
        cell.reviewLine.hidden = true;
    }
    
    
    cell.UserImgView.tag = indexPath.row;
    cell.UserImgView.image = imageUserDefault;
    
    //adjust radius tap gesture
    self.imgViewTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(animateImageViewToFullScreen:)];
    self.imgViewTapGesture.delegate = self;
    self.imgViewTapGesture.numberOfTapsRequired = 1;
    [cell.UserImgView addGestureRecognizer:self.imgViewTapGesture];
    
    cell.UserImgView.layer.cornerRadius = cell.UserImgView.frame.size.height/2;
    cell.UserImgView.layer.masksToBounds = YES;
    
    NSLog(@"************* ID: %@ Pic:%@", taskObj.taskid, taskObj.picpath);
    if ([taskObj.picpath isEqualToString:@""]) {
        
        cell.UserImgView.image = imageUserDefault;
        
        if(taskObj.userImage){
            //cell.imgViewUserProfile.image = taskObj.userImage;
        }else{
            //cell.imgViewUserProfile.image = imageUserDefault;
        }
    }else{
        if (taskObj.userImage) {
            // cell.UserImgView.image = taskObj.userImage;
        }else{
            
            NSArray *components = [taskObj.picpath componentsSeparatedByString:@"/"];
            NSString *imgNameWithJPEG = [components lastObject];
            imgNameWithJPEG = [imgNameWithJPEG stringByAppendingString:@".jpeg"];
            NSArray *componentsWithDots = [imgNameWithJPEG componentsSeparatedByString:@"."];
            if (componentsWithDots.count>2) {
                imgNameWithJPEG = [NSString stringWithFormat:@"%@.%@",[componentsWithDots firstObject],[componentsWithDots lastObject]];
            }
            
            UIImage *img = [GlobalFunction loadImageOfLogoFromCacheFolder:imgNameWithJPEG dirname:@"user"];
            
            if (img) {
                //taskObj.userImage = img;
                cell.UserImgView.image = img;
            }else{
                
                NSString *image = taskObj.picpath;
                NSString *fullPath=[NSString stringWithFormat:urlServer,image];
                
                // fullPath=image;
                NSString* webStringURL = [fullPath stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                
                dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH,  0ul);
                dispatch_async(queue, ^{
                    //This is what you will load lazily
                    NSURL *imageURL=[NSURL URLWithString:webStringURL];
                    
                    NSData *images=[NSData dataWithContentsOfURL:imageURL];
                    
                    // NSLog(<#NSString * _Nonnull format, ...#>)
                    NSArray *componentsArr = [image componentsSeparatedByString:@"/"];
                    
                    
                    NSString *imgNameWithJPEG = [componentsArr lastObject];
                    
                    NSArray *componentsArr2 = [imgNameWithJPEG componentsSeparatedByString:@"."];
                    
                    NSString *imgNameWithoutJPEG = [componentsArr2 firstObject];
                    
                    UIImage *img = [UIImage imageWithData:images];
                    
                    if (img) {
                        
                        dispatch_sync(dispatch_get_main_queue(), ^{
                            
                            UIImage *img = [UIImage imageWithData:images];
                            cell.UserImgView.image=img;
                            [GlobalFunction saveimage:img imageName:imgNameWithoutJPEG dirname:@"user"];
                            
                        });
                        
                    }
                    
                });
                
            }
            
        }
        
    }
    
    //*****************************************************************************
    
    
    
    
    
    
    
    
    
    
    cell.nameLbl.textColor=[UIColor colorWithRed:63.0/255.0f green:67.0/255.0f blue:70.0/255.0f alpha:1.0];
    cell.locationLbl.textColor=[UIColor colorWithRed:63.0/255.0f green:67.0/255.0f blue:70.0/255.0f alpha:1.0];
    cell.dateLbl.textColor=[UIColor colorWithRed:63.0/255.0f green:67.0/255.0f blue:70.0/255.0f alpha:1.0];
    cell.purchasedLbl.textColor=[UIColor colorWithRed:111.0/255.0f green:119.0/255.0f blue:125.0/255.0f alpha:1.0];
    
    //   cell.locationLbl.backgroundColor = [UIColor redColor];
    //   cell.lblPrice.backgroundColor = [UIColor greenColor];
    
    
    
    if(IS_IPHONE_6P)
    {
        
        cell.userProfileLeft.constant=10;
        cell.userProfileTop.constant=18;
        cell.userProfileHeight.constant=80;
        cell.userProfileWidth.constant=80;
        cell.nameLblLeft.constant=20;
        cell.nameLblTop.constant=13;
        cell.locationTop.constant=15;
        cell.locationLblLeft.constant=20;
        cell.consLblPriceTop.constant = 15;
        cell.locationLblWidth.constant =100; //120;
        cell.lblPriceWidth.constant = 88;//94;
        cell.purchaseLblLeft.constant=20;
        cell.purchaseLblTop.constant=15;
        cell.purchaseLblWidth.constant=200;
        cell.rightArrowTop.constant=20;
        cell.dateLblTop.constant=14;
        cell.dateLblRight.constant=4;
        cell.dateLblWidth.constant=68;
        cell.reviewBtnRight.constant=13;
        cell.reviewLineRight.constant=16;
        cell.reviewLineWidth.constant=55;
        
        //        [cell.nameLbl setFont:[UIFont fontWithName:@"Lato-Regular" size:20]];
        //        [cell.locationLbl setFont:[UIFont fontWithName:@"Lato-Regular" size:18]];
        //        [cell.purchasedLbl setFont:[UIFont fontWithName:@"Lato-Regular" size:18]];
        //        [cell.dateLbl setFont:[UIFont fontWithName:@"Lato-Regular" size:16]];
        //        [cell.reviewBtn.titleLabel setFont:[UIFont fontWithName:@"Lato-Bold" size:18]];
        //        [cell.lblPrice setFont:[UIFont fontWithName:@"Lato-Regular" size:18]];
        
        
        [cell.nameLbl setFont:[UIFont fontWithName:@"Lato-Regular" size:16]];
        [cell.locationLbl setFont:[UIFont fontWithName:@"Lato-Regular" size:15]];
        [cell.purchasedLbl setFont:[UIFont fontWithName:@"Lato-Regular" size:15]];
        [cell.dateLbl setFont:[UIFont fontWithName:@"Lato-Regular" size:13]];
        [cell.reviewBtn.titleLabel setFont:[UIFont fontWithName:@"Lato-Bold" size:15]];
        [cell.lblPrice setFont:[UIFont fontWithName:@"Lato-Regular" size:16]];
        
        cell.nameLblWidth.constant = 260;
        
        
        
    }
    
    
    else if (IS_IPHONE_5 || IS_IPHONE_4_OR_LESS)
    {
        cell.userProfileLeft.constant=6;
        cell.userProfileTop.constant=11;
        cell.userProfileHeight.constant=63;
        cell.userProfileWidth.constant=63;
        cell.nameLblLeft.constant=13;
        cell.nameLblTop.constant=6;
        [cell.nameLbl setFont:[UIFont fontWithName:@"Lato-Regular" size:15]];
        cell.nameLblWidth.constant = 192;
        cell.locationTop.constant=7;
        cell.consLblPriceTop.constant = 8;
        cell.locationLblLeft.constant=13;
        cell.locationLblWidth.constant = 75;
        cell.lblPriceWidth.constant = 75;
        [cell.locationLbl setFont:[UIFont fontWithName:@"Lato-Regular" size:14]];
        cell.purchaseLblLeft.constant=15;
        cell.purchaseLblTop.constant=7;
        [cell.purchasedLbl setFont:[UIFont fontWithName:@"Lato-Regular" size:14]];
        cell.rightArrowTop.constant=11;
        cell.rightArrowRight.constant=2;
        cell.dateLblTop.constant=8;
        [cell.dateLbl setFont:[UIFont fontWithName:@"Lato-Regular" size:12]];
        cell.dateLblRight.constant=0;
        cell.dateLblWidth.constant=55;
        [cell.reviewBtn.titleLabel setFont:[UIFont fontWithName:@"Lato-Bold" size:14]];
        cell.reviewBtnRight.constant=3;
        cell.reviewLineRight.constant=10;
        cell.reviewLineWidth.constant=45;
        cell.reviewBtnTop.constant=7;
        cell.reviewLineTop.constant=-3;
        
    }
    
    
    else if (IS_IPHONE_6)
    {
        [cell.nameLbl setFont:[UIFont fontWithName:@"Lato-Regular" size:15]];
        [cell.locationLbl setFont:[UIFont fontWithName:@"Lato-Regular" size:14]];
        [cell.purchasedLbl setFont:[UIFont fontWithName:@"Lato-Regular" size:14]];
        [cell.dateLbl setFont:[UIFont fontWithName:@"Lato-Regular" size:12]];
        [cell.reviewBtn.titleLabel setFont:[UIFont fontWithName:@"Lato-Bold" size:14]];
        [cell.lblPrice setFont:[UIFont fontWithName:@"Lato-Regular" size:15]];
        cell.nameLblWidth.constant = 240;
        
    }
    
    
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (tableView == self.tblTaskHistory) {
        
        // Remove seperator inset
        if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
            [cell setSeparatorInset:UIEdgeInsetsZero];
        }
        
        // Prevent the cell from inheriting the Table View's margin settings
        if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
            [cell setPreservesSuperviewLayoutMargins:NO];
        }
        
        // Explictly set your cell's layout margins
        if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
            [cell setLayoutMargins:UIEdgeInsetsZero];
        }
        
    }
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    if(IS_IPHONE_5 || IS_IPHONE_4_OR_LESS)
    {
        return 100;
    }
    else{
        return 117;
        
    }
    
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [GlobalFunction addIndicatorView];
    
    
    
    //    if (myAppDelegate.isComingFromPushNotification) {
    //
    //        myAppDelegate.taskPushData = nil;
    //
    //        myAppDelegate.taskPushData = [arrayOfTaskWithStatus3 objectAtIndex:indexPath.row];
    //
    //    }else{
    //
    //        if (myAppDelegate.isOfOtherUser) {
    //
    //            myAppDelegate.taskOtherData = nil;
    //
    //            myAppDelegate.taskOtherData = [arrayOfTaskWithStatus3 objectAtIndex:indexPath.row];
    //
    //        }else{
    //
    //            myAppDelegate.taskData = nil;
    //
    //            myAppDelegate.taskData = [arrayOfTaskWithStatus3 objectAtIndex:indexPath.row];
    //
    //        }
    //
    //    }
    
    
    
    //    myActivityTableViewCell *cell = (myActivityTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"cell"];
    //
    //    [self performSegueWithIdentifier:@"reviewdetail" sender:cell.buttonForReviewDetail];
    
    
    [self doneTyping];
    
    myAppDelegate.taskData = nil;
    
    myAppDelegate.taskData = [arrayOfTaskWithStatus3 objectAtIndex:indexPath.row];
    
    
    NSLog(@"taskData purchaseID :- %@",myAppDelegate.taskData.purchaseID);
    
    compareReviewStatus = @"";
    
    compareReviewStatus = myAppDelegate.taskData.reviewStatus_ForHistory;
    NSLog(@"reservationType is..%@",myAppDelegate.taskData.reservationType_ForHistory);
    
    myAppDelegate.reservationBoughtStatus_ForShare = @"";
    myAppDelegate.reservationBoughtStatus_ForShare = myAppDelegate.taskData.reservationType_ForHistory;
    
    NSLog(@"review status is..%@",compareReviewStatus);
    
    requestStatus = myAppDelegate.taskData.reservationType_ForHistory;
    
    if (![requestStatus isEqualToString:@"63"])
    {
        requestStatus = @"";
    }
    
    [self performSelector:@selector(fetchTaskInfo) withObject:nil afterDelay:0.2];
    
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    //Obviously, if this returns no, the edit option won't even populate
    return YES;
}
-(NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    task *taskObj1 = (task *)[arrayOfTaskWithStatus3 objectAtIndex:indexPath.row];
    
    UITableViewRowAction *Delete = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:@"Delete" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath)
                                    {
                                        NSLog(@"Delete work call");
                                        // Delete something here
                                        
                                        
                                        taskId_ForDeleteReservation = @"";
                                        taskId_ForDeleteReservation = taskObj1.taskid;
                                        NSLog(@"Review status is...%@",taskObj1.reviewStatus_ForHistory);
                                        
                                        if ([taskObj1.reviewStatus_ForHistory isEqualToString:@"0"]) {
                                            
                                            [GlobalFunction addIndicatorView];
                                            // [self performSelector:@selector(finallyAddIndicator11) withObject:nil afterDelay:0.1];
                                            
                                            //  isDeleteResponseYes = [self callDeleteApi:indexPath];
                                            
                                            [self performSelector:@selector(callDeleteApi:) withObject:indexPath afterDelay:0.1];
                                            
                                            //        if (isDeleteResponseYes == true) {
                                            //
                                            //            if (editingStyle == UITableViewCellEditingStyleDelete)
                                            //            {
                                            //
                                            //                        task *data=[arrayOfTaskWithStatus3 objectAtIndex:indexPath.row];
                                            //                        [arrayOfAllHistoryData removeObject:data];
                                            //                        [arrayOfTaskWithStatus3 removeObject:data];
                                            //                //[arrayOfAllHistoryData removeObjectAtIndex:indexPath.row];
                                            //                // Delete the row from the data source
                                            //                [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
                                            //
                                            //                [self performSelector:@selector(reloadTable) withObject:nil afterDelay:0.2];
                                            //            }
                                            //
                                            //        }
                                            
                                        }
                                        else
                                        {
                                            [self.tableView setEditing:FALSE];
                                            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"BUY TIMEE" message:@"You can not delete this appointment." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                                            
                                            // old one..You can not delete this reservation.
                                            
                                            [alert show];
                                            
                                            
                                        }
                                        
                                    }];
    
    Delete.backgroundColor = [UIColor redColor];
    
    NSString *edit = @"";
    edit = taskObj1.editStatus;
    
    if ([edit isEqualToString:@"1"])
    {
        UITableViewRowAction *Edit = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:@" Edit " handler:^(UITableViewRowAction *action, NSIndexPath *indexPath)
                                      {
                                          NSLog(@"edit work call");
                                          //Just as an example :
                                          
                                          [GlobalFunction addIndicatorView];
                                          
                                          isEditTapped = YES;
                                          
                                          myAppDelegate.taskData = nil;
                                          
                                          myAppDelegate.taskData = [arrayOfTaskWithStatus3 objectAtIndex:indexPath.row];
                                          [self performSelector:@selector(fetchTaskInfo) withObject:nil afterDelay:0.2];
                                      }];
        Edit.backgroundColor = [UIColor colorWithRed:0.63 green:0.67 blue:0.70 alpha:1];
        
        return @[Delete, Edit];
        
    }
    
    
    return @[Delete]; //array with all the buttons you want. 1,2,3, etc...
    
}
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    //    task *taskObj1 = (task *)[arrayOfTaskWithStatus3 objectAtIndex:indexPath.row];
    //
    //    taskId_ForDeleteReservation = @"";
    //    taskId_ForDeleteReservation = taskObj1.taskid;
    //       NSLog(@"Review status is...%@",taskObj1.reviewStatus_ForHistory);
    //
    //    if ([taskObj1.reviewStatus_ForHistory isEqualToString:@"0"]) {
    //
    //         [GlobalFunction addIndicatorView];
    //       // [self performSelector:@selector(finallyAddIndicator11) withObject:nil afterDelay:0.1];
    //
    //      //  isDeleteResponseYes = [self callDeleteApi:indexPath];
    //
    //       [self performSelector:@selector(callDeleteApi:) withObject:indexPath afterDelay:0.1];
    //
    ////        if (isDeleteResponseYes == true) {
    ////
    ////            if (editingStyle == UITableViewCellEditingStyleDelete)
    ////            {
    ////
    ////                        task *data=[arrayOfTaskWithStatus3 objectAtIndex:indexPath.row];
    ////                        [arrayOfAllHistoryData removeObject:data];
    ////                        [arrayOfTaskWithStatus3 removeObject:data];
    ////                //[arrayOfAllHistoryData removeObjectAtIndex:indexPath.row];
    ////                // Delete the row from the data source
    ////                [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
    ////
    ////                [self performSelector:@selector(reloadTable) withObject:nil afterDelay:0.2];
    ////            }
    ////
    ////        }
    //
    //    }
    //    else
    //    {
    //        [self.tableView setEditing:FALSE];
    //        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"BuyTimee" message:@"You can not delete this reservation." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    //
    //        [alert show];
    //
    //
    //    }
    
}
-(void)finallyAddIndicator11{
    
    [GlobalFunction addIndicatorView];
}

-(void)callDeleteApi:(NSIndexPath *)indexPath
{
    
    
    if (![self checkForInternet]) {
        [self.tableView setEditing:FALSE];
        [GlobalFunction removeIndicatorView];
        
        [[GlobalFunction shared] showAlertForMessage:@"Can't connect to internet, please check and try again."];
        
        return;
        // return false;
    }
    
    else
    {
        
        
        NSDictionary *reservationData = [[WebService shared] deleteReservationTaskid:taskId_ForDeleteReservation userid:myAppDelegate.userData.userid];
        
        // NSDictionary *reservationData = [[WebService shared] deleteReservationTaskid:taskId_ForDeleteReservation userid:@"5"];
        
        if (reservationData != nil) {
            
            if ([[EncryptDecryptFile decrypt:[reservationData valueForKey:@"responseCode"]] isEqualToString:@"200"]) {
                
                //    NSDictionary *dictDataReg = [reservationData objectForKey:@"data"];
                
                [self bioUpdatedSuccefully:[EncryptDecryptFile decrypt:[reservationData valueForKey:@"responseMessage"]]];
                
                NSLog(@"count of all array is...%lu",(unsigned long)arrayOfAllHistoryData.count);
                
                isDeleteResponseYes = true;
                
                NSString* final = @"Reservation deleted successfully";
                
                [Flurry logEvent:final];
                
                if (isDeleteResponseYes == true) {
                    
                    
                    
                    task *data=[arrayOfTaskWithStatus3 objectAtIndex:indexPath.row];
                    [arrayOfAllHistoryData removeObject:data];
                    [arrayOfTaskWithStatus3 removeObject:data];
                    //[arrayOfAllHistoryData removeObjectAtIndex:indexPath.row];
                    // Delete the row from the data source
                    [self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
                    
                    [self performSelector:@selector(reloadTable) withObject:nil afterDelay:0.2];
                    
                    
                }
                
                
                // return true;
                
            }
            else{
                [self.tableView setEditing:FALSE];
                [GlobalFunction removeIndicatorView];
                
                [[GlobalFunction shared] showAlertForMessage:[EncryptDecryptFile decrypt:[reservationData valueForKey:@"responseMessage"]]];
                
                isDeleteResponseYes = false;
                // return false;
            }
            
        }
        else{
            [self.tableView setEditing:FALSE];
            [GlobalFunction removeIndicatorView];
            
            [[GlobalFunction shared] showAlertForMessage:@"Server is down, please try again later."];
            
            isDeleteResponseYes = false;
            // return false;
        }
    }
    
    
}
-(void)fetchTaskInfo{
    
    if (![[WebService shared] connected]) {
        
        [GlobalFunction removeIndicatorView];
        
        [[GlobalFunction shared] showAlertForMessage:@"Can't connect to internet, please check and try again."];
        
        return;
    }
    
    
    
    NSString *timeZoneSeconds = [GlobalFunction getTimeZone];
    
    NSDictionary *dictTaskInfoResponse;
    
    if (myAppDelegate.isFromPushNotification) {
        
        //  dictTaskInfoResponse = [[WebService shared] viewTaskInfoTaskid:myAppDelegate.taskPushData.taskid userid:myAppDelegate.userData.userid timeZone:timeZoneSeconds];
        
        dictTaskInfoResponse = [[WebService shared] viewTaskInfoTaskid:myAppDelegate.reservationId_FromPush userid:myAppDelegate.userData.userid timeZone:timeZoneSeconds view:@""];
        
    }else{
        
        if (myAppDelegate.isOfOtherUser)
        {
            if (isEditTapped == YES)
            {
                
                dictTaskInfoResponse = [[WebService shared] viewTaskInfoTaskid:myAppDelegate.taskOtherData.taskid userid:myAppDelegate.userData.userid timeZone:timeZoneSeconds view:@"1"];
            }
            else
            {
                dictTaskInfoResponse = [[WebService shared] viewTaskInfoTaskid:myAppDelegate.taskOtherData.taskid userid:myAppDelegate.userData.userid timeZone:timeZoneSeconds view:requestStatus];
            }
        }else{
            
            if (isEditTapped == YES)
            {
                dictTaskInfoResponse = [[WebService shared] viewTaskInfoTaskid:myAppDelegate.taskData.taskid userid:myAppDelegate.userData.userid timeZone:timeZoneSeconds view:@"1"];
            }
            else
            {
                dictTaskInfoResponse = [[WebService shared] viewTaskInfoTaskid:myAppDelegate.taskData.taskid userid:myAppDelegate.userData.userid timeZone:timeZoneSeconds view:requestStatus];
            }
        }
        
    }
    
    if (dictTaskInfoResponse) {
        
        if ([[EncryptDecryptFile decrypt:[dictTaskInfoResponse valueForKey:@"responseCode"]] isEqualToString:@"200"]) {
            
            NSLog(@"response message == %@",[EncryptDecryptFile decrypt:[dictTaskInfoResponse valueForKey:@"responseMessage"]]);
            
            NSDictionary *dictDataReg = [dictTaskInfoResponse objectForKey:@"data"];
            
            if (dictDataReg != nil) {
                
                if (myAppDelegate.isComingFromPushNotification) {
                    
                    myAppDelegate.workerPushTaskDetail = nil;
                    myAppDelegate.workerPushTaskDetail = [[WorkerEmployerTaskDetail alloc]init];
                    
                    [self fillWorkerTaskDataForWorkerTaskDataObj:myAppDelegate.workerPushTaskDetail fromDictionary:dictDataReg andUserData:myAppDelegate.userPushOtherData];
                    
                }else{
                    
                    if (myAppDelegate.isOfOtherUser) {
                        
                        myAppDelegate.workerOtherUserTaskDetail = nil;
                        myAppDelegate.workerOtherUserTaskDetail = [[WorkerEmployerTaskDetail alloc]init];
                        
                        [self fillWorkerTaskDataForWorkerTaskDataObj:myAppDelegate.workerOtherUserTaskDetail fromDictionary:dictDataReg andUserData:myAppDelegate.userOtherData];
                        
                    }else{
                        
                        myAppDelegate.workerTaskDetail = nil;
                        myAppDelegate.workerTaskDetail = [[WorkerEmployerTaskDetail alloc]init];
                        
                        [self fillWorkerTaskDataForWorkerTaskDataObj:myAppDelegate.workerTaskDetail fromDictionary:dictDataReg andUserData:myAppDelegate.userData];
                        
                    }
                    
                }
                
                if (myAppDelegate.isFromPushNotification)
                {
                    [self performSelector:@selector(performSegueWorkForNotification) withObject:nil afterDelay:0.3];
                }
                else
                {
                    [self performSelector:@selector(performSegueForTaskDetail) withObject:nil afterDelay:0.5];
                }
                
            }else{
                
                [GlobalFunction removeIndicatorView];
                
                [[GlobalFunction shared] showAlertForMessage:@"Server is down, please try again later."];
                
                myAppDelegate.isFromPushNotification = false;
                isEditTapped = NO;
            }
            
            
        }else{
            
            [GlobalFunction removeIndicatorView];
            
            [[GlobalFunction shared] showAlertForMessage:[EncryptDecryptFile decrypt:[dictTaskInfoResponse valueForKey:@"responseMessage"]]];
            myAppDelegate.isFromPushNotification = false;
            isEditTapped = NO;
        }
        
    }else{
        
        [GlobalFunction removeIndicatorView];
        
        [[GlobalFunction shared] showAlertForMessage:@"Server is down, please try again later."];
        myAppDelegate.isFromPushNotification = false;
        isEditTapped = NO;
    }
    
}

-(void)bioUpdatedSuccefully:(NSString *)response{
    
    [GlobalFunction removeIndicatorView];
    
    NSLog(@"successful .......");
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"BUY TIMEE" message:response delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    
    [alert show];
    
}

-(void)performSegueWorkForNotification
{
    myActivityTableViewCell *cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:[arrayOfTaskWithStatus3 indexOfObject:myAppDelegate.taskData] inSection:0]];
    if ([myAppDelegate.notificationType_FromPush isEqualToString:@"102"])
    {
        [myAppDelegate.vcObj performSegueWithIdentifier:@"review" sender:cell];
    }
    else
    {
        [self performSegueWithIdentifier:@"reviewdetail" sender:cell.buttonForReviewDetail];
    }
    
}

-(void)fillWorkerTaskDataForWorkerTaskDataObj:(WorkerEmployerTaskDetail *)obj fromDictionary:(NSDictionary *)dict andUserData:(user *)userObj{
    
    NSDictionary *dictTemp0 =nil;
    
    // NSString *mainn = [EncryptDecryptFile decrypt:[dict valueForKey:@"Accepted By"]];
    
    dictTemp0= [dict objectForKey:@"Accepted By"];
    
    
    
    if (dictTemp0==nil) {
        // obj.isTaskAccepted = false;
    }else{
        // obj.isTaskAccepted = true;
        //    obj.userId_acceptedBy = [EncryptDecryptFile decrypt:[dictTemp0 valueForKey:@"userId"]];
        //  obj.emailId_acceptedBy = [EncryptDecryptFile decrypt:[dictTemp0 valueForKey:@"emailId"]];
        obj.picPath_acceptedBy = [EncryptDecryptFile decrypt:[dictTemp0 valueForKey:@"picPath"]];
    }
    
    
    NSDictionary *dictTemp3 =nil;
    dictTemp3= [dict objectForKey:@"Reservation Detail"];
    
    if (dictTemp3==nil) {
        
    }else{
        
        
        obj.taskDetailObj = [[task alloc]init];
        
        obj.taskDetailObj.userid = [EncryptDecryptFile decrypt:[dictTemp3 valueForKey:@"userId"]];
        obj.taskDetailObj.categoryId = [EncryptDecryptFile decrypt:[dictTemp3 valueForKey:@"categoryId"]];
        obj.taskDetailObj.paymentMethod = [EncryptDecryptFile decrypt:[dictTemp3 valueForKey:@"paymentMethod"]];
        
        obj.taskDetailObj.title = [EncryptDecryptFile decrypt:[dictTemp3 valueForKey:@"title"]];
        obj.taskDetailObj.taskid = [EncryptDecryptFile decrypt:[dictTemp3 valueForKey:@"reservationId"]];
        //obj.taskDetailObj.subcategoryId = [EncryptDecryptFile decrypt:[dictTemp3 valueForKey:@"subcategoryId"]];
        obj.taskDetailObj.status = [EncryptDecryptFile decrypt:[dictTemp3 valueForKey:@"status"]];
        NSLog(@"status is this %@",obj.taskDetailObj.status);
        obj.taskDetailObj.startDate = [EncryptDecryptFile decrypt:[dictTemp3 valueForKey:@"startDate"]];
        obj.taskDetailObj.price = [[EncryptDecryptFile decrypt:[dictTemp3 valueForKey:@"actualPrice"]] floatValue];
        obj.taskDetailObj.longitude = [[EncryptDecryptFile decrypt:[dictTemp3 valueForKey:@"longitude"]] floatValue];
        obj.taskDetailObj.locationName = [EncryptDecryptFile decrypt:[dictTemp3 valueForKey:@"locationName"]];
        obj.taskDetailObj.latitude = [[EncryptDecryptFile decrypt:[dictTemp3 valueForKey:@"latitude"]] floatValue];
        obj.taskDetailObj.endDate = [EncryptDecryptFile decrypt:[dictTemp3 valueForKey:@"startTime"]];
        obj.taskDetailObj.taskDescription = [EncryptDecryptFile decrypt:[dictTemp3 valueForKey:@"description"]];
        obj.taskDetailObj.lastModifyTime = [EncryptDecryptFile decrypt:[dictTemp3 valueForKey:@"lastModifyTime"]];
        
        obj.taskDetailObj.discountValue = [EncryptDecryptFile decrypt:[dictTemp3 valueForKey:@"discount"]];
        obj.taskDetailObj.discountDescription = [EncryptDecryptFile decrypt:[dictTemp3 valueForKey:@"discountDescription"]];
        obj.taskDetailObj.quantityOfDiscount = [EncryptDecryptFile decrypt:[dictTemp3 valueForKey:@"totalUsers"]];
        obj.taskDetailObj.feeValue = [EncryptDecryptFile decrypt:[dictTemp3 valueForKey:@"fee"]];
        
        obj.taskDetailObj.soldStatus = [EncryptDecryptFile decrypt:[dictTemp3 valueForKey:@"soldStatus"]];
        obj.taskDetailObj.priceNewVal = [EncryptDecryptFile decrypt:[dictTemp3 valueForKey:@"price"]];// by kp
        obj.taskDetailObj.requestAppointmentStatus = [EncryptDecryptFile decrypt:[dictTemp3 valueForKey:@"requestAppointmentStatus"]];
        
        //********* Rohitash Prajapati ***********//
        //        NSString* purchaseID  =  [dictTemp3 valueForKey:@"purchaseId"];
        //        if(purchaseID)
        //        {
        //            obj.taskDetailObj.purchaseID = [EncryptDecryptFile decrypt:purchaseID];
        //        }
        
        NSLog(@"purchase id is this %@",myAppDelegate.taskData.purchaseID);
        NSString *purchaseID = myAppDelegate.taskData.purchaseID;
        if(purchaseID)
        {
            obj.taskDetailObj.purchaseID = purchaseID;
        }
        
        
        
        
        
        //        NSString *data = [dictTemp3 valueForKey:@"reservationType"];
        //        NSLog(@"reservationType :- %@",[EncryptDecryptFile decrypt: [dictTemp3 valueForKey:@"reservationType"]]);
        //        obj.taskDetailObj.reservationType_ForHistory = [EncryptDecryptFile decrypt: [dictTemp3 valueForKey:@"reservationType"]];
        
        
        
        
        obj.taskDetailObj.taskImagesArray = [[NSMutableArray alloc] init];
        
        NSLog(@"%@",obj.taskDetailObj.soldStatus);
        NSLog(@"%@",obj.taskDetailObj.priceNewVal);  //by kp
        
        NSLog(@"%@",obj.taskDetailObj.userid);
        NSLog(@"%@",obj.taskDetailObj.taskid);
        
        
        
        NSLog(@"task id after fech data in history class..%@",obj.taskDetailObj.taskid);
        NSLog(@"task id after fech data in history class..%@",obj.taskDetailObj.title);
        //  myAppDelegate.workerTaskDetail_taskDetailObj_taskid = obj.taskDetailObj.taskid; //update string for rate view.
        //  myAppDelegate.workerTaskDetail_taskDetailObj_taskName = obj.taskDetailObj.title;
        
        NSArray *newArray= [dict objectForKey:@"reservation_images"];
        for (NSDictionary *dicts in newArray) {
            
            NSString *imageName=[EncryptDecryptFile decrypt:[dicts valueForKey:@"reservationImg"]];
            
            if(imageName.length<=0)
            {
                
            }
            else
            {
                [obj.taskDetailObj.taskImagesArray addObject:[EncryptDecryptFile decrypt:[dicts valueForKey:@"reservationImg"]]];
            }
            
        }
        
    }
    
    
    
    NSDictionary *dictTemp2 =nil;
    dictTemp2= [dict objectForKey:@"Created By"];
    if (dictTemp2==nil) {
        
    }else
    {
        
        obj.userId_createdBy = [EncryptDecryptFile decrypt:[dictTemp2 valueForKey:@"userId"]];
        obj.emailId_createdBy = [EncryptDecryptFile decrypt:[dictTemp2 valueForKey:@"emailId"]];
        obj.picPath_createdBy = [EncryptDecryptFile decrypt:[dictTemp2 valueForKey:@"picPath"]];
        obj.name_createdBy = [EncryptDecryptFile decrypt:[dictTemp2 valueForKey:@"name"]];
        NSLog(@"%@",obj.picPath_createdBy);
        NSLog(@"%@",obj.userId_createdBy);
        
        obj.avg_rating_createdBy = [EncryptDecryptFile decrypt:[dictTemp2 valueForKey:@"avgRating"]];
        //  myAppDelegate.workerTaskDetail_userId_createdBy = obj.userId_createdBy;// update string for rate view.
        //  myAppDelegate.workerTaskDetail_name_createdBy = obj.userId_createdBy;// update string for rate view.
    }
    
    NSDictionary *dictTemp5 =nil;
    dictTemp5= [dict objectForKey:@"Business"];
    if (dictTemp5==nil) {
        
    }else
    {
        
        obj.name_Business = [EncryptDecryptFile decrypt:[dictTemp5 valueForKey:@"name"]];
        obj.address_Business = [EncryptDecryptFile decrypt:[dictTemp5 valueForKey:@"address"]];
        obj.yelpProfile_Business = [EncryptDecryptFile decrypt:[dictTemp5 valueForKey:@"yelpProfile"]];
        obj.services_Business = [EncryptDecryptFile decrypt:[dictTemp5 valueForKey:@"services"]];
        obj.instaLink_Business = [EncryptDecryptFile decrypt:[dictTemp5 valueForKey:@"instaLink"]];
        
    }
    
    return;
    ///img work by vs
    
    
    
    
    //    NSArray *arrayTerms= [dict valueForKey:@"Accepted By"];
    //    if ([[arrayTerms objectAtIndex:0] isKindOfClass:[NSString class]]) {
    //
    //        obj.isTaskAccepted = false;
    //
    //    }else if ([[arrayTerms objectAtIndex:0] isKindOfClass:[NSDictionary class]]){
    //
    //        obj.isTaskAccepted = true;
    //
    //        NSDictionary *dictTemp0=[arrayTerms objectAtIndex:0];
    //
    //        obj.userId_acceptedBy = [EncryptDecryptFile decrypt:[dictTemp0 valueForKey:@"userId"]];
    //        obj.userName_acceptedBy = [EncryptDecryptFile decrypt:[dictTemp0 valueForKey:@"userName"]];
    //        obj.description_acceptedBy = [EncryptDecryptFile decrypt:[dictTemp0 valueForKey:@"description"]];
    //        obj.emailId_acceptedBy = [EncryptDecryptFile decrypt:[dictTemp0 valueForKey:@"emailId"]];
    //        obj.latitude_acceptedBy = [[EncryptDecryptFile decrypt:[dictTemp0 valueForKey:@"latitude"]] floatValue];
    //        obj.longitude_acceptedBy = [[EncryptDecryptFile decrypt:[dictTemp0 valueForKey:@"longitude"]] floatValue];
    //        obj.locationName_acceptedBy = [EncryptDecryptFile decrypt:[dictTemp0 valueForKey:@"location"]];
    //        obj.mobile_acceptedBy = [EncryptDecryptFile decrypt:[dictTemp0 valueForKey:@"mobile"]];
    //        obj.picPath_acceptedBy = [EncryptDecryptFile decrypt:[dictTemp0 valueForKey:@"picPath"]];
    //        obj.avg_rating_acceptedBy = [EncryptDecryptFile decrypt:[dictTemp0 valueForKey:@"avg_rating"]];
    //
    //    }
    //
    //    //task accepted by rating detail
    //    obj.arrayRatings_acceptedBy = [[NSMutableArray alloc]init];
    //    arrayTerms= [dict valueForKey:@"Worker Rating Detail"];
    //    if ([[arrayTerms objectAtIndex:0] isKindOfClass:[NSString class]]) {
    //
    //        obj.rating_acceptedBy = [EncryptDecryptFile decrypt:[arrayTerms objectAtIndex:0]];
    //
    //    }else if ([[arrayTerms objectAtIndex:0] isKindOfClass:[NSDictionary class]]){
    //
    //        NSInteger noOfStars = 0;
    //        for (NSDictionary *dicts in arrayTerms) {
    //
    //            NSString *stars = [EncryptDecryptFile decrypt:[dicts valueForKey:@"ratingStar"]];
    //            noOfStars = noOfStars + [stars integerValue];
    //
    //            obj.ratingObj = nil;
    //            obj.ratingObj = [[rating alloc]init];
    //
    //            obj.ratingObj.ratingdescription = [EncryptDecryptFile decrypt:[dicts valueForKey:@"description"]];
    //            obj.ratingObj.employerId = [EncryptDecryptFile decrypt:[dicts valueForKey:@"employerId"]];
    //            obj.ratingObj.ratingId = [EncryptDecryptFile decrypt:[dicts valueForKey:@"ratingId"]];
    //            obj.ratingObj.ratingStar = [EncryptDecryptFile decrypt:[dicts valueForKey:@"ratingStar"]];
    //            obj.ratingObj.taskId = [EncryptDecryptFile decrypt:[dicts valueForKey:@"taskId"]];
    //            obj.ratingObj.workerId = [EncryptDecryptFile decrypt:[dicts valueForKey:@"workerId"]];
    //            obj.ratingObj.userName = [EncryptDecryptFile decrypt:[dicts valueForKey:@"userName"]];
    //
    //            [obj.arrayRatings_acceptedBy addObject:obj.ratingObj];
    //        }
    //        CGFloat avg = noOfStars/arrayTerms.count;
    //        obj.avgStars_acceptedBy = avg;
    //    }else if ([[arrayTerms objectAtIndex:0] isKindOfClass:[NSArray class]]){
    //
    //        NSArray *arrRatings = [arrayTerms objectAtIndex:0];
    //
    //        NSInteger noOfStars = 0;
    //
    //        for (NSDictionary *dicts in arrRatings) {
    //
    //            NSString *stars = [EncryptDecryptFile decrypt:[dicts valueForKey:@"ratingStar"]];
    //            noOfStars = noOfStars + [stars integerValue];
    //
    //            obj.ratingObj = nil;
    //            obj.ratingObj = [[rating alloc]init];
    //
    //            obj.ratingObj.ratingdescription = [EncryptDecryptFile decrypt:[dicts valueForKey:@"description"]];
    //            obj.ratingObj.employerId = [EncryptDecryptFile decrypt:[dicts valueForKey:@"employerId"]];
    //            obj.ratingObj.ratingId = [EncryptDecryptFile decrypt:[dicts valueForKey:@"ratingId"]];
    //            obj.ratingObj.ratingStar = [EncryptDecryptFile decrypt:[dicts valueForKey:@"ratingStar"]];
    //            obj.ratingObj.taskId = [EncryptDecryptFile decrypt:[dicts valueForKey:@"taskId"]];
    //            obj.ratingObj.workerId = [EncryptDecryptFile decrypt:[dicts valueForKey:@"workerId"]];
    //            obj.ratingObj.userName = [EncryptDecryptFile decrypt:[dicts valueForKey:@"userName"]];
    //
    //            [obj.arrayRatings_acceptedBy addObject:obj.ratingObj];
    //        }
    //
    //        CGFloat avg = noOfStars/arrayTerms.count;
    //        obj.avgStars_acceptedBy = avg;
    //
    //    }
    //
    //    //task created by rating detail
    //    obj.arrayRatings_createdBy = [[NSMutableArray alloc]init];
    //    arrayTerms= [dict valueForKey:@"Created By Rating Detail"];
    //
    //    if ([[arrayTerms objectAtIndex:0] isKindOfClass:[NSString class]]) {
    //
    //        obj.rating_createdBy = [EncryptDecryptFile decrypt:[arrayTerms objectAtIndex:0]];
    //
    //    }else if ([[arrayTerms objectAtIndex:0] isKindOfClass:[NSDictionary class]]){
    //
    //        NSInteger noOfStars = 0;
    //        for (NSDictionary *dicts in arrayTerms) {
    //
    //            NSString *stars = [EncryptDecryptFile decrypt:[dicts valueForKey:@"ratingStar"]];
    //            noOfStars = noOfStars + [stars integerValue];
    //
    //            obj.ratingObj = nil;
    //            obj.ratingObj = [[rating alloc]init];
    //
    //            obj.ratingObj.ratingdescription = [EncryptDecryptFile decrypt:[dicts valueForKey:@"description"]];
    //            obj.ratingObj.employerId = [EncryptDecryptFile decrypt:[dicts valueForKey:@"employerId"]];
    //            obj.ratingObj.ratingId = [EncryptDecryptFile decrypt:[dicts valueForKey:@"ratingId"]];
    //            obj.ratingObj.ratingStar = [EncryptDecryptFile decrypt:[dicts valueForKey:@"ratingStar"]];
    //            obj.ratingObj.taskId = [EncryptDecryptFile decrypt:[dicts valueForKey:@"taskId"]];
    //            obj.ratingObj.workerId = [EncryptDecryptFile decrypt:[dicts valueForKey:@"workerId"]];
    //            obj.ratingObj.userName = [EncryptDecryptFile decrypt:[dicts valueForKey:@"userName"]];
    //
    //            [obj.arrayRatings_createdBy addObject:obj.ratingObj];
    //        }
    //
    //        CGFloat avg = noOfStars/arrayTerms.count;
    //        obj.avgStars_createdBy = avg;
    //
    //    }else if ([[arrayTerms objectAtIndex:0] isKindOfClass:[NSArray class]]){
    //
    //        NSArray *arrRatings = [arrayTerms objectAtIndex:0];
    //
    //        NSInteger noOfStars = 0;
    //
    //        for (NSDictionary *dicts in arrRatings) {
    //
    //            NSString *stars = [EncryptDecryptFile decrypt:[dicts valueForKey:@"ratingStar"]];
    //            noOfStars = noOfStars + [stars integerValue];
    //
    //            obj.ratingObj = nil;
    //            obj.ratingObj = [[rating alloc]init];
    //
    //            obj.ratingObj.ratingdescription = [EncryptDecryptFile decrypt:[dicts valueForKey:@"description"]];
    //            obj.ratingObj.employerId = [EncryptDecryptFile decrypt:[dicts valueForKey:@"employerId"]];
    //            obj.ratingObj.ratingId = [EncryptDecryptFile decrypt:[dicts valueForKey:@"ratingId"]];
    //            obj.ratingObj.ratingStar = [EncryptDecryptFile decrypt:[dicts valueForKey:@"ratingStar"]];
    //            obj.ratingObj.taskId = [EncryptDecryptFile decrypt:[dicts valueForKey:@"taskId"]];
    //            obj.ratingObj.workerId = [EncryptDecryptFile decrypt:[dicts valueForKey:@"workerId"]];
    //            obj.ratingObj.userName = [EncryptDecryptFile decrypt:[dicts valueForKey:@"userName"]];
    //
    //            [obj.arrayRatings_createdBy addObject:obj.ratingObj];
    //        }
    //
    //        CGFloat avg = noOfStars/arrayTerms.count;
    //        obj.avgStars_createdBy = avg;
    //
    //    }
    //
    //
    //    arrayTerms= [dict valueForKey:@"Card Detail"];
    //    if ([[arrayTerms objectAtIndex:0] isKindOfClass:[NSString class]]) {
    //        obj.cardDetail = [EncryptDecryptFile decrypt:[arrayTerms objectAtIndex:0]];
    //    }else if ([[arrayTerms objectAtIndex:0] isKindOfClass:[NSDictionary class]]){
    //        obj.cardDetail = [EncryptDecryptFile decrypt:[[arrayTerms objectAtIndex:0] valueForKey:@"card_number"]];
    //    }else if ([[arrayTerms objectAtIndex:0] isKindOfClass:[NSArray class]]){
    //        obj.cardDetail = [EncryptDecryptFile decrypt:[[arrayTerms objectAtIndex:0] objectAtIndex:0]];
    //    }
    //
    //    arrayTerms= [dict valueForKey:@"Created By"];
    //    NSDictionary *dictTemp2=[arrayTerms objectAtIndex:0];
    //
    //    obj.userId_createdBy = [EncryptDecryptFile decrypt:[dictTemp2 valueForKey:@"userId"]];
    //    obj.userName_createdBy = [EncryptDecryptFile decrypt:[dictTemp2 valueForKey:@"userName"]];
    //    obj.description_createdBy = [EncryptDecryptFile decrypt:[dictTemp2 valueForKey:@"description"]];
    //    obj.emailId_createdBy = [EncryptDecryptFile decrypt:[dictTemp2 valueForKey:@"emailId"]];
    //    obj.latitude_createdBy = [[EncryptDecryptFile decrypt:[dictTemp2 valueForKey:@"latitude"]] floatValue];
    //    obj.longitude_createdBy = [[EncryptDecryptFile decrypt:[dictTemp2 valueForKey:@"longitude"]] floatValue];
    //    obj.locationName_createdBy = [EncryptDecryptFile decrypt:[dictTemp2 valueForKey:@"location"]];
    //    obj.mobile_createdBy = [EncryptDecryptFile decrypt:[dictTemp2 valueForKey:@"mobile"]];
    //    obj.picPath_createdBy = [EncryptDecryptFile decrypt:[dictTemp2 valueForKey:@"picPath"]];
    //    obj.avg_rating_createdBy = [EncryptDecryptFile decrypt:[dictTemp2 valueForKey:@"avg_rating"]];
    //
    //    arrayTerms= [dict valueForKey:@"Task Detail"];
    //    NSDictionary *dictTemp3=[arrayTerms objectAtIndex:0];
    //
    //    obj.taskDetailObj = [[task alloc]init];
    //
    //    obj.taskDetailObj.userid = [EncryptDecryptFile decrypt:[dictTemp3 valueForKey:@"userId"]];
    //    obj.taskDetailObj.title = [EncryptDecryptFile decrypt:[dictTemp3 valueForKey:@"title"]];
    //    obj.taskDetailObj.taskid = [EncryptDecryptFile decrypt:[dictTemp3 valueForKey:@"taskId"]];
    //    obj.taskDetailObj.subcategoryId = [EncryptDecryptFile decrypt:[dictTemp3 valueForKey:@"subcategoryId"]];
    //    obj.taskDetailObj.status = [EncryptDecryptFile decrypt:[dictTemp3 valueForKey:@"status"]];
    //    obj.taskDetailObj.startDate = [EncryptDecryptFile decrypt:[dictTemp3 valueForKey:@"startDate"]];
    //    obj.taskDetailObj.price = [[EncryptDecryptFile decrypt:[dictTemp3 valueForKey:@"price"]] floatValue];
    //    obj.taskDetailObj.longitude = [[EncryptDecryptFile decrypt:[dictTemp3 valueForKey:@"longitude"]] floatValue];
    //    obj.taskDetailObj.locationName = [EncryptDecryptFile decrypt:[dictTemp3 valueForKey:@"locationName"]];
    //    obj.taskDetailObj.latitude = [[EncryptDecryptFile decrypt:[dictTemp3 valueForKey:@"latitude"]] floatValue];
    //    obj.taskDetailObj.endDate = [EncryptDecryptFile decrypt:[dictTemp3 valueForKey:@"endDate"]];
    //    obj.taskDetailObj.taskDescription = [EncryptDecryptFile decrypt:[dictTemp3 valueForKey:@"description"]];
    //    obj.taskDetailObj.lastModifyTime = [EncryptDecryptFile decrypt:[dictTemp3 valueForKey:@"lastModifyTime"]];
    //    obj.taskDetailObj.taskImagesArray = [[NSMutableArray alloc] init];
    //    obj.taskDetailObj.imagesArray = [[NSMutableArray alloc]init];
    //
    //    for (rating *ratingObjs in obj.arrayRatings_acceptedBy) {
    //
    //        if ([ratingObjs.taskId isEqualToString:[NSString stringWithFormat:@"%@",obj.taskDetailObj.taskid]]) {
    //
    //            obj.isAlreadyRated = true;
    //
    //            break;
    //
    //        }
    //
    //    }
    //
    //    arrayTerms= [dict valueForKey:@"taskImage"];
    //    NSArray *newArray = [arrayTerms objectAtIndex:0];
    //
    //    int i = 0;
    //
    //    for (NSDictionary *dicts in newArray) {
    //
    //        NSArray *componentsArray = [[EncryptDecryptFile decrypt:[dicts valueForKey:@"taskImg"]] componentsSeparatedByString:@"http://192.168.5.134/handapp/"];
    //
    //        if (componentsArray.count>1) {
    //            [obj.taskDetailObj.taskImagesArray addObject:[NSString stringWithFormat:urlServer,[componentsArray objectAtIndex:1]]];
    //        }else{
    //            [obj.taskDetailObj.taskImagesArray addObject:[NSString stringWithFormat:urlServer,[componentsArray objectAtIndex:0]]];
    //        }
    //
    //        NSString *fullPath=[obj.taskDetailObj.taskImagesArray objectAtIndex:i];
    //        NSArray *parts = [fullPath componentsSeparatedByString:@"/"];
    //        NSString *imageName=[parts lastObject];
    //        NSArray *imageNameParts = [imageName componentsSeparatedByString:@"."];
    //
    //        NSString *userImageNameWithJPEG = [NSString stringWithFormat:@"%@_%@_%d.jpeg",[imageNameParts firstObject],obj.taskDetailObj.taskid,i];
    //
    //        UIImage *img = [GlobalFunction loadImageOfLogoFromCacheFolder:userImageNameWithJPEG dirname:@"taskImages"];
    //
    //        if (img) {
    //
    //            [obj.taskDetailObj.imagesArray addObject:img];
    //
    //        }
    //
    //        i = i+1;
    //    }
    //
    //    if (obj.taskDetailObj.taskImagesArray.count != obj.taskDetailObj.imagesArray.count) {
    //        [obj.taskDetailObj.imagesArray removeAllObjects];
    //    }
    //
    //
    //    if ([userObj.userid isEqualToString:obj.userId_acceptedBy]) {
    //
    //        if ([obj.picPath_createdBy isEqualToString:@""]) {
    //            obj.userImage_createdBy = imageUserDefault;
    //        }else{
    //            NSArray *components = [obj.picPath_createdBy componentsSeparatedByString:@"/"];
    //
    //            NSString *imgNameWithJPEG = [components lastObject];
    //
    //
    //            NSArray *componentsWithDots = [imgNameWithJPEG componentsSeparatedByString:@"."];
    //
    //            if (componentsWithDots.count>2) {
    //
    //                imgNameWithJPEG = [NSString stringWithFormat:@"%@.%@",[componentsWithDots firstObject],[componentsWithDots lastObject]];
    //
    //            }
    //
    //            UIImage *img = [GlobalFunction loadImageOfLogoFromCacheFolder:imgNameWithJPEG dirname:@"user"];
    //
    //            if (img) {
    //                obj.userImage_createdBy = img;
    //            }else{
    //                UIImage *img2 = [GlobalFunction getImageFromURL:[NSString stringWithFormat:urlServer,obj.picPath_createdBy]];
    //
    //                if (img2) {
    //                    obj.userImage_createdBy = img2;
    //
    //                    NSArray *components2 = [imgNameWithJPEG componentsSeparatedByString:@"."];
    //
    //                    NSString *imgNameWithoutJPEG = [components2 firstObject];
    //
    //                    [GlobalFunction saveimage:img2 imageName:imgNameWithoutJPEG dirname:@"user"];
    //
    //                }else{
    //                    obj.userImage_createdBy = imageUserDefault;
    //                }
    //            }
    //        }
    //    }else if ([userObj.userid isEqualToString:obj.userId_createdBy]){
    //
    //        if ([obj.picPath_acceptedBy isEqualToString:@""]) {
    //            obj.userImage_acceptedBy = imageUserDefault;
    //        }else{
    //            NSArray *components = [obj.picPath_acceptedBy componentsSeparatedByString:@"/"];
    //
    //            NSString *imgNameWithJPEG = [components lastObject];
    //
    //
    //            NSArray *componentsWithDots = [imgNameWithJPEG componentsSeparatedByString:@"."];
    //
    //            if (componentsWithDots.count>2) {
    //
    //                imgNameWithJPEG = [NSString stringWithFormat:@"%@.%@",[componentsWithDots firstObject],[componentsWithDots lastObject]];
    //
    //            }
    //
    //            UIImage *img = [GlobalFunction loadImageOfLogoFromCacheFolder:imgNameWithJPEG dirname:@"user"];
    //
    //            if (img) {
    //                obj.userImage_acceptedBy = img;
    //            }else{
    //                UIImage *img2 = [GlobalFunction getImageFromURL:[NSString stringWithFormat:urlServer,obj.picPath_acceptedBy]];
    //
    //                if (img2) {
    //                    obj.userImage_acceptedBy = img2;
    //
    //                    NSArray *components2 = [imgNameWithJPEG componentsSeparatedByString:@"."];
    //
    //                    NSString *imgNameWithoutJPEG = [components2 firstObject];
    //
    //                    [GlobalFunction saveimage:img2 imageName:imgNameWithoutJPEG dirname:@"user"];
    //
    //                }else{
    //                    obj.userImage_acceptedBy = imageUserDefault;
    //                }
    //            }
    //        }
    //    }
}


-(void)performSegueForTaskDetail{
    
    //    if (myAppDelegate.isComingFromPushNotification) {
    //
    //        [myAppDelegate.taskDetailPushVCObj performSegueWithIdentifier:@"otherUserTaskDetail" sender:myAppDelegate.taskDetailPushVCObj.btnForTaskHistorySegue];
    //
    //    }else{
    //
    //        myAppDelegate.isSideBarAccesible = false;
    //
    //        if (myAppDelegate.isOfOtherUser) {
    //
    //            [myAppDelegate.taskDetailVCObj performSegueWithIdentifier:@"otherUserTaskDetail" sender:myAppDelegate.taskDetailVCObj.btnForTaskHistorySegue];
    //
    //        }else{
    //
    //            TaskDetailTableViewCell *cell = [self.tblTaskHistory cellForRowAtIndexPath:[NSIndexPath indexPathForRow:[arrayOfTaskWithStatus3 indexOfObject:myAppDelegate.taskData] inSection:0]];
    //            [self performSegueWithIdentifier:@"taskHistoryDetail" sender:cell];
    //
    //        }
    //
    //    }
    
    if (isEditTapped == YES)
    {
        
        if ([myAppDelegate.taskData.editStatus isEqualToString:@"1"])
        {
            myAppDelegate.isFromHistory_ForEdit = YES;
            isEditTapped = NO;
            // [GlobalFunction addIndicatorView];
            
            TaskTableViewCell *cell = (TaskTableViewCell *)[myAppDelegate.vcObj.tblViewSideBar cellForRowAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:0]];
            [myAppDelegate.vcObj performSegueWithIdentifier:@"createTask" sender:cell];
            
            
        }
        
        
    }
    else
    {
        isEditTapped = NO;
        myActivityTableViewCell *cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:[arrayOfTaskWithStatus3 indexOfObject:myAppDelegate.taskData] inSection:0]];
        if ([compareReviewStatus isEqualToString:@"1"])
        {
            [myAppDelegate.vcObj performSegueWithIdentifier:@"review" sender:cell];
        }
        else
        {
            [self performSegueWithIdentifier:@"reviewdetail" sender:cell.buttonForReviewDetail];
        }
        
    }
    
    
    
}

//-(BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender{
//
//    if ([identifier isEqualToString:@"taskHistoryDetail"]) {
//        return NO;
//    }
//    return YES;
//}
//
//-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
//
//    [GlobalFunction addIndicatorView];
//
//    if ([segue.identifier isEqualToString:@"taskHistoryDetail"]) {
//
//        myAppDelegate.vcObj.btnSideBarOpen.hidden = YES;
//        myAppDelegate.vcObj.btnBack.hidden = NO;
//
//        if (myAppDelegate.isFromProfile) {
//            myAppDelegate.stringNameOfChildVC = @"ViewController";
//        }else{
//            myAppDelegate.stringNameOfChildVC = [NSString stringWithFormat:@"%@",[self class]];
//        }
//
//        myAppDelegate.superViewClassName = @"";
//
//        myAppDelegate.taskDetailVCObj = nil;
//
//        myAppDelegate.taskDetailVCObj = (TaskDetailViewController *)segue.destinationViewController;
//
//    }
//}
//



-(BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{
    
    if ([identifier isEqualToString:@"reviewdetail"]) {
        
    }
    return YES;
    
}



-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    // [GlobalFunction addIndicatorView];
    
    myAppDelegate.superViewClassName = @"TaskHistoryViewController";
    
    if ([segue.identifier isEqualToString:@"reviewdetail"]) {
        
        //    myAppDelegate.TandC_Buyee=@"review_rejection";
        //   myAppDelegate.superViewClassName = @"";
        
        myAppDelegate.vcObj.btnBack.hidden = NO;
        myAppDelegate.vcObj.btnSideBarOpen.hidden = YES;
        myAppDelegate.reviewDetailVcObj = nil;
        myAppDelegate.reviewDetailVcObj = (ReivewDetailViewController *)segue.destinationViewController;
        
        
    }
    
}



- (IBAction)animateImageViewToFullScreen:(id)sender {
    
    //    TaskDetailTableViewCell *tCell = [self.tblTaskHistory cellForRowAtIndexPath:[NSIndexPath indexPathForRow:[[sender view] tag] inSection:0]];
    //
    //    [GlobalFunction animateImageview:tCell.imgViewUserProfile];
    
    [_txtActiveField resignFirstResponder];
    
    myActivityTableViewCell *tCell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:[[sender view] tag] inSection:0]];
    
    [GlobalFunction animateImageview:tCell.UserImgView];
    
}

- (IBAction)allBtnClciked:(id)sender {
    
    
    
    [self.listedBtn setSelected:NO];
    [self.boughtBtn setSelected:NO];
    [self.allBtn setSelected:YES];
    [self.soldBtn setSelected:NO];
    [self.btnRequested setSelected:NO];
    
    [self.allBtn setTitleColor:[UIColor colorWithRed:63.0/255.0f green:67.0/255.0f blue:70.0/255.0f alpha:1.0] forState:UIControlStateNormal];
    _allBtn.backgroundColor=[UIColor colorWithRed:255.0/255.0f green:255.0/255.0f blue:255.0/255.0f alpha:1.0];
    
    
    [self.boughtBtn setTitleColor:[UIColor colorWithRed:255.0/255.0f green:255.0/255.0f blue:255.0/255.0f alpha:1.0] forState:UIControlStateNormal];
    _boughtBtn.backgroundColor=[UIColor colorWithRed:63.0/255.0f green:67.0/255.0f blue:70.0/255.0f alpha:1.0];
    
    
    [self.soldBtn setTitleColor:[UIColor colorWithRed:255.0/255.0f green:255.0/255.0f blue:255.0/255.0f alpha:1.0] forState:UIControlStateNormal];
    _soldBtn.backgroundColor=[UIColor colorWithRed:63.0/255.0f green:67.0/255.0f blue:70.0/255.0f alpha:1.0];
    
    
    [self.listedBtn setTitleColor:[UIColor colorWithRed:255.0/255.0f green:255.0/255.0f blue:255.0/255.0f alpha:1.0] forState:UIControlStateNormal];
    _listedBtn.backgroundColor=[UIColor colorWithRed:63.0/255.0f green:67.0/255.0f blue:70.0/255.0f alpha:1.0];
    
    [self.btnRequested setTitleColor:[UIColor colorWithRed:255.0/255.0f green:255.0/255.0f blue:255.0/255.0f alpha:1.0] forState:UIControlStateNormal];
    _btnRequested.backgroundColor=[UIColor colorWithRed:63.0/255.0f green:67.0/255.0f blue:70.0/255.0f alpha:1.0];
    
    
    [self performSelector:@selector(reloadTable) withObject:nil afterDelay:0.2];
    
}

- (IBAction)boughtBtnClicked:(id)sender {
    
    
    
    [self.listedBtn setSelected:NO];
    [self.boughtBtn setSelected:YES];
    [self.allBtn setSelected:NO];
    [self.soldBtn setSelected:NO];
    [self.btnRequested setSelected:NO];
    
    
    [self.boughtBtn setTitleColor:[UIColor colorWithRed:63.0/255.0f green:67.0/255.0f blue:70.0/255.0f alpha:1.0] forState:UIControlStateNormal];
    _boughtBtn.backgroundColor=[UIColor colorWithRed:255.0/255.0f green:255.0/255.0f blue:255.0/255.0f alpha:1.0];
    
    [self.allBtn setTitleColor:[UIColor colorWithRed:255.0/255.0f green:255.0/255.0f blue:255.0/255.0f alpha:1.0] forState:UIControlStateNormal];
    _allBtn.backgroundColor=[UIColor colorWithRed:63.0/255.0f green:67.0/255.0f blue:70.0/255.0f alpha:1.0];
    
    
    [self.soldBtn setTitleColor:[UIColor colorWithRed:255.0/255.0f green:255.0/255.0f blue:255.0/255.0f alpha:1.0] forState:UIControlStateNormal];
    _soldBtn.backgroundColor=[UIColor colorWithRed:63.0/255.0f green:67.0/255.0f blue:70.0/255.0f alpha:1.0];
    
    
    [self.listedBtn setTitleColor:[UIColor colorWithRed:255.0/255.0f green:255.0/255.0f blue:255.0/255.0f alpha:1.0] forState:UIControlStateNormal];
    _listedBtn.backgroundColor=[UIColor colorWithRed:63.0/255.0f green:67.0/255.0f blue:70.0/255.0f alpha:1.0];
    
    [self.btnRequested setTitleColor:[UIColor colorWithRed:255.0/255.0f green:255.0/255.0f blue:255.0/255.0f alpha:1.0] forState:UIControlStateNormal];
    _btnRequested.backgroundColor=[UIColor colorWithRed:63.0/255.0f green:67.0/255.0f blue:70.0/255.0f alpha:1.0];
    
    
    //    [self.tableView layoutIfNeeded];
    //    [self.tableView reloadData];
    //    [self.tableView layoutIfNeeded];
    [self performSelector:@selector(reloadTable) withObject:nil afterDelay:0.2];
    
}

- (IBAction)soldBtnClciked:(id)sender {
    
    
    [self.listedBtn setSelected:NO];
    [self.boughtBtn setSelected:NO];
    [self.allBtn setSelected:NO];
    [self.soldBtn setSelected:YES];
    
    [self.soldBtn setTitleColor:[UIColor colorWithRed:63.0/255.0f green:67.0/255.0f blue:70.0/255.0f alpha:1.0] forState:UIControlStateNormal];
    _soldBtn.backgroundColor=[UIColor colorWithRed:255.0/255.0f green:255.0/255.0f blue:255.0/255.0f alpha:1.0];
    
    [self.allBtn setTitleColor:[UIColor colorWithRed:255.0/255.0f green:255.0/255.0f blue:255.0/255.0f alpha:1.0] forState:UIControlStateNormal];
    _allBtn.backgroundColor=[UIColor colorWithRed:63.0/255.0f green:67.0/255.0f blue:70.0/255.0f alpha:1.0];
    
    [self.boughtBtn setTitleColor:[UIColor colorWithRed:255.0/255.0f green:255.0/255.0f blue:255.0/255.0f alpha:1.0] forState:UIControlStateNormal];
    _boughtBtn.backgroundColor=[UIColor colorWithRed:63.0/255.0f green:67.0/255.0f blue:70.0/255.0f alpha:1.0];
    
    
    [self.listedBtn setTitleColor:[UIColor colorWithRed:255.0/255.0f green:255.0/255.0f blue:255.0/255.0f alpha:1.0] forState:UIControlStateNormal];
    _listedBtn.backgroundColor=[UIColor colorWithRed:63.0/255.0f green:67.0/255.0f blue:70.0/255.0f alpha:1.0];
    
    [self.btnRequested setTitleColor:[UIColor colorWithRed:255.0/255.0f green:255.0/255.0f blue:255.0/255.0f alpha:1.0] forState:UIControlStateNormal];
    _btnRequested.backgroundColor=[UIColor colorWithRed:63.0/255.0f green:67.0/255.0f blue:70.0/255.0f alpha:1.0];
    
    [self performSelector:@selector(reloadTable) withObject:nil afterDelay:0.2];
    
    //    [self.tableView layoutIfNeeded];
    //    [self.tableView reloadData];
    //    [self.tableView layoutIfNeeded];
    
}

- (IBAction)listedBtnClciked:(id)sender {
    
    [self.listedBtn setSelected:YES];
    [self.boughtBtn setSelected:NO];
    [self.allBtn setSelected:NO];
    [self.soldBtn setSelected:NO];
    [self.btnRequested setSelected:NO];
    
    [self.listedBtn setTitleColor:[UIColor colorWithRed:63.0/255.0f green:67.0/255.0f blue:70.0/255.0f alpha:1.0] forState:UIControlStateNormal];
    _listedBtn.backgroundColor=[UIColor colorWithRed:255.0/255.0f green:255.0/255.0f blue:255.0/255.0f alpha:1.0];
    [self.allBtn setTitleColor:[UIColor colorWithRed:255.0/255.0f green:255.0/255.0f blue:255.0/255.0f alpha:1.0] forState:UIControlStateNormal];
    _allBtn.backgroundColor=[UIColor colorWithRed:63.0/255.0f green:67.0/255.0f blue:70.0/255.0f alpha:1.0];
    
    [self.boughtBtn setTitleColor:[UIColor colorWithRed:255.0/255.0f green:255.0/255.0f blue:255.0/255.0f alpha:1.0] forState:UIControlStateNormal];
    _boughtBtn.backgroundColor=[UIColor colorWithRed:63.0/255.0f green:67.0/255.0f blue:70.0/255.0f alpha:1.0];
    
    
    [self.soldBtn setTitleColor:[UIColor colorWithRed:255.0/255.0f green:255.0/255.0f blue:255.0/255.0f alpha:1.0] forState:UIControlStateNormal];
    _soldBtn.backgroundColor=[UIColor colorWithRed:63.0/255.0f green:67.0/255.0f blue:70.0/255.0f alpha:1.0];
    [self.btnRequested setTitleColor:[UIColor colorWithRed:255.0/255.0f green:255.0/255.0f blue:255.0/255.0f alpha:1.0] forState:UIControlStateNormal];
    _btnRequested.backgroundColor=[UIColor colorWithRed:63.0/255.0f green:67.0/255.0f blue:70.0/255.0f alpha:1.0];
    
    //
    //    [self.tableView layoutIfNeeded];
    //    [self.tableView reloadData];
    //    [self.tableView layoutIfNeeded];
    [self performSelector:@selector(reloadTable) withObject:nil afterDelay:0.2];
    
}

- (IBAction)requestedBtnClicked:(id)sender
{
    [self.listedBtn setSelected:NO];
    [self.boughtBtn setSelected:NO];
    [self.allBtn setSelected:NO];
    [self.soldBtn setSelected:NO];
    [self.btnRequested setSelected:YES];
    
    [self.soldBtn setTitleColor:[UIColor colorWithRed:255.0/255.0f green:255.0/255.0f blue:255.0/255.0f alpha:1.0] forState:UIControlStateNormal];
    _soldBtn.backgroundColor=[UIColor colorWithRed:63.0/255.0f green:67.0/255.0f blue:70.0/255.0f alpha:1.0];
    
    [self.allBtn setTitleColor:[UIColor colorWithRed:255.0/255.0f green:255.0/255.0f blue:255.0/255.0f alpha:1.0] forState:UIControlStateNormal];
    _allBtn.backgroundColor=[UIColor colorWithRed:63.0/255.0f green:67.0/255.0f blue:70.0/255.0f alpha:1.0];
    
    [self.boughtBtn setTitleColor:[UIColor colorWithRed:255.0/255.0f green:255.0/255.0f blue:255.0/255.0f alpha:1.0] forState:UIControlStateNormal];
    _boughtBtn.backgroundColor=[UIColor colorWithRed:63.0/255.0f green:67.0/255.0f blue:70.0/255.0f alpha:1.0];
    
    
    [self.listedBtn setTitleColor:[UIColor colorWithRed:255.0/255.0f green:255.0/255.0f blue:255.0/255.0f alpha:1.0] forState:UIControlStateNormal];
    _listedBtn.backgroundColor=[UIColor colorWithRed:63.0/255.0f green:67.0/255.0f blue:70.0/255.0f alpha:1.0];
    [self.btnRequested setTitleColor:[UIColor colorWithRed:63.0/255.0f green:67.0/255.0f blue:70.0/255.0f alpha:1.0] forState:UIControlStateNormal];
    _btnRequested.backgroundColor=[UIColor colorWithRed:255.0/255.0f green:255.0/255.0f blue:255.0/255.0f alpha:1.0];
    
    [self performSelector:@selector(reloadTable) withObject:nil afterDelay:0.2];
}

- (IBAction)reviewBtnClciked:(id)sender {
}
-(void)manageConstraints
{
    self.consLblNoHistoryTop.constant = -50;
    
    _allBtn.backgroundColor=[UIColor whiteColor];
    [self.allBtn setTitleColor:[UIColor colorWithRed:63.0/255.0f green:67.0/255.0f blue:70.0/255.0f alpha:1.0] forState:UIControlStateNormal];
    _boughtBtn.backgroundColor=[UIColor colorWithRed:63.0/255.0f green:67.0/255.0f blue:70.0/255.0f alpha:1.0];
    _soldBtn.backgroundColor=[UIColor colorWithRed:63.0/255.0f green:67.0/255.0f blue:70.0/255.0f alpha:1.0];
    _listedBtn.backgroundColor=[UIColor colorWithRed:63.0/255.0f green:67.0/255.0f blue:70.0/255.0f alpha:1.0];
    _btnRequested.backgroundColor=[UIColor colorWithRed:63.0/255.0f green:67.0/255.0f blue:70.0/255.0f alpha:1.0];
    _searchTxtFld.backgroundColor=[UIColor colorWithRed:244.0/255.0f green:244.0/255.0f blue:244.0/255.0f alpha:1.0];
    
    _lineLbl.backgroundColor=[UIColor colorWithRed:196.0/255.0f green:196.0/255.0f blue:196.0/255.0f alpha:1.0];
    
    
    _btnBackgroundView.backgroundColor=[UIColor colorWithRed:90.0/255.0f green:93.0/255.0f blue:95.0/255.0f alpha:1.0];
    
    
    
    
    [self.view layoutIfNeeded];
    
    if(IS_IPHONE_6P)
    {
        
        _searchTxtFldHeight.constant=48;
        [self.searchTxtFld setFont:[UIFont fontWithName:@"Lato-Regular" size:20]];
        
        _allBtnBottom.constant=76;
        _boughtBtnBottom.constant=76;
        _soldBtnBottom.constant=76;
        _listedBtnBottom.constant=76;
        _consBtnRequestedBottom.constant = 76;
        
        _allBtnHeight.constant=47;
        _boughtBtnHeight.constant=47;
        _soldBtnHeight.constant=47;
        _listedBtnHeight.constant=47;
        _consBtnRequestedHeight.constant = 47;
        
        
        _btnBackgroundViewBottom.constant=76;
        _btnBackgroundViewHeight.constant=47;
        
        
        _allBtnWidth.constant=82;
        _boughtBtnWidth.constant=82;
        _soldBtnWidth.constant=82;
        _listedBtnWidth.constant=82;
        _consBtnRequestedWidth.constant = 82;
        
        _tableViewHeight.constant=560;
        _allBtnWidth.constant = 82;
        
    }
    
    else if (IS_IPHONE_5)
    {
        
        _searchTxtFldHeight.constant=36;
        [self.searchTxtFld setFont:[UIFont fontWithName:@"Lato-Regular" size:17]];
        
        _allBtnBottom.constant=60;
        _boughtBtnBottom.constant=60;
        _soldBtnBottom.constant=60;
        _listedBtnBottom.constant=60;
        self.consBtnRequestedBottom.constant = 60;
        
        _allBtnHeight.constant=36;
        _boughtBtnHeight.constant=36;
        _soldBtnHeight.constant=36;
        _listedBtnHeight.constant=36;
        self.consBtnRequestedHeight.constant = 36;
        
        
        _btnBackgroundViewBottom.constant=60;
        _btnBackgroundViewHeight.constant=36;
        
        
        _allBtnWidth.constant=61;
        _boughtBtnWidth.constant=62;
        _soldBtnWidth.constant=62;
        _listedBtnWidth.constant=62;
        self.consBtnRequestedWidth.constant = 70;
        
        
        _tableViewHeight.constant=445;
        
        [self.allBtn.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:13]];
        
        [self.boughtBtn.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:13]];
        
        [self.soldBtn.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:13]];
        
        [self.listedBtn.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:13]];
        
        [self.btnRequested.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:13]];
        
        
        
    }
    
    
    else if (IS_IPHONE_4_OR_LESS)
    {
        _searchTxtFldHeight.constant=36;
        [self.searchTxtFld setFont:[UIFont fontWithName:@"Lato-Regular" size:17]];
        _allBtnBottom.constant=60;
        _boughtBtnBottom.constant=60;
        _soldBtnBottom.constant=60;
        _listedBtnBottom.constant=60;
        self.consBtnRequestedBottom.constant = 60;
        
        _allBtnHeight.constant=36;
        _boughtBtnHeight.constant=36;
        _soldBtnHeight.constant=36;
        _listedBtnHeight.constant=36;
        self.consBtnRequestedHeight.constant = 36;
        _btnBackgroundViewBottom.constant=60;
        _btnBackgroundViewHeight.constant=36;
        _allBtnWidth.constant=61;
        _boughtBtnWidth.constant=62;
        _soldBtnWidth.constant=62;
        _listedBtnWidth.constant=62;
        self.consBtnRequestedWidth.constant = 70;
        _tableViewHeight.constant=345;
        [self.allBtn.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:13]];
        [self.boughtBtn.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:13]];
        [self.soldBtn.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:13]];
        [self.listedBtn.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:13]];
        [self.btnRequested.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:13]];
    }
    
    [self.view layoutIfNeeded];
    
}



-(NSArray *)filterTaskFortaskStatus:(NSString *)taskStatus{
    
    NSArray *arrayFiltered;
    
    NSLog(@"count of array detail in arrayOfAllHistoryData is....%lu",(unsigned long)arrayOfAllHistoryData.count);
    
    if ([taskStatus isEqualToString: @"0"] || [taskStatus isEqualToString:@"1"] || [taskStatus isEqualToString:@"2"] || [taskStatus isEqualToString:@"63"]) {
        
        
        
        NSPredicate *statusPredicate = [NSPredicate predicateWithFormat:@"SELF.reservationType_ForHistory MATCHES[cd] %@",taskStatus];
        arrayFiltered = [arrayOfAllHistoryData filteredArrayUsingPredicate:statusPredicate];
        
#pragma mark - for showing task only of other users not self in view all task list
        //        NSPredicate *userIdPredicate = [NSPredicate predicateWithFormat:@"SELF.userid MATCHES[cd] %@",userID];
        //        NSArray *userIDFilteredArray = [arrayFiltered filteredArrayUsingPredicate:userIdPredicate];
        //
        //        NSMutableArray *copyOfArrayFiltered = [arrayFiltered mutableCopy];
        //
        //        [copyOfArrayFiltered removeObjectsInArray:userIDFilteredArray];
        //
        //        arrayFiltered = nil;
        //
        //        arrayFiltered = copyOfArrayFiltered;
        
    }
    else
    {
        arrayFiltered = arrayOfAllHistoryData;
    }
    NSMutableArray *array = [arrayFiltered mutableCopy];
    
    [array sortUsingDescriptors:
     @[
       [NSSortDescriptor sortDescriptorWithKey:@"taskid_Integer" ascending:NO],
       ]];
    
    arrayFiltered = array;
    
    //    for (task *obj in arrayFiltered) {
    //        NSLog(@"filtered tast id === %@ and task status === %@ and created by id ==`= %@",obj.taskid,obj.status,obj.userid);
    //    }
    
    return arrayFiltered;
    
}






-(NSArray *)filterTaskFortaskStatussearch:(NSString *)taskStatus{
    
    NSArray *arrayFiltered;
    
    NSLog(@"count of array detail in filterTaskForUserID is....%lu",(unsigned long)arrayOfAllHistoryData.count);
    
    NSString *searchTexts = self.searchTxtFld.text;
    NSMutableArray *predicateArray = [[NSMutableArray alloc] init];
    NSPredicate *orPredicate;
    
    NSPredicate *temp;
    NSPredicate *predicateTitle;
    NSArray *subPredicates;
    if ([taskStatus isEqualToString: @"0"] || [taskStatus isEqualToString:@"1"] || [taskStatus isEqualToString:@"2"]) {
        
        NSPredicate *statusPredicate = [NSPredicate predicateWithFormat:@"SELF.reservationType_ForHistory MATCHES[cd] %@",taskStatus];
        
        // [predicateArray addObject:statusPredicate];
        // arrayFiltered = [arrayOfTaskWithStatus3 filteredArrayUsingPredicate:statusPredicate];
        
        
        NSPredicate *predicateLocation;
        
        NSMutableArray *subPredicates1=[[NSMutableArray alloc] init];
        
        
        if (searchTexts.length<=0)
        {
            subPredicates = [NSArray arrayWithObjects:statusPredicate,nil];
        }
        else
        {
            //        NSPredicate *predicateTitle = [NSPredicate predicateWithFormat:@"title contains[cd] %@",searchTexts];
            //        NSPredicate *predicateLocation = [NSPredicate predicateWithFormat:@"locationName contains[cd] %@",searchTexts];
            
            predicateTitle = [NSPredicate predicateWithFormat:@"title contains[cd] %@ or locationName contains[cd] %@",searchTexts, searchTexts];
            //predicateLocation = [NSPredicate predicateWithFormat:@"locationName contains[cd] %@",searchTexts];
            //subPredicates = [NSArray arrayWithObjects:statusPredicate,predicateTitle,nil];
            
            
            [subPredicates1 addObject:predicateTitle];
            [subPredicates1 addObject:statusPredicate];
            
            temp=[NSCompoundPredicate andPredicateWithSubpredicates:subPredicates1];
            
            arrayFiltered = [arrayOfAllHistoryData filteredArrayUsingPredicate:temp];
            //   [predicateArray addObject:predicateTitle];
            //   [predicateArray addObject:predicateLocation];
            //subPredicates = [NSArray arrayWithObjects:statusPredicate,predicateTitle,nil];
        }
        
        //           temp=[NSCompoundPredicate andPredicateWithSubpredicates:statusPredicate];
        
        //subPredicates = [NSArray arrayWithObjects:predicateTitle, predicateLocation,nil];
        // orPredicate = [NSCompoundPredicate orPredicateWithSubpredicates:subPredicates];
    }else{
        NSMutableArray *subPredicates1=[[NSMutableArray alloc] init];
        predicateTitle = [NSPredicate predicateWithFormat:@"title contains[cd] %@ or locationName contains[cd] %@",searchTexts, searchTexts];
        [subPredicates1 addObject:predicateTitle];
        
        arrayFiltered = [arrayOfAllHistoryData filteredArrayUsingPredicate:predicateTitle];
        
        
    }
    
    //arrayFiltered = [arrayOfAllHistoryData filteredArrayUsingPredicate:temp];
    
    //arrayFiltered = [arrayOfAllHistoryData filteredArrayUsingPredicate:orPredicate];
    //
    //    NSArray *subPredicates = [NSArray arrayWithArray:predicateArray];
    //    NSPredicate *orPredicate = [NSCompoundPredicate andPredicateWithSubpredicates:subPredicates];
    //    arrayFiltered = [arrayOfAllHistoryData filteredArrayUsingPredicate:orPredicate];
    NSMutableArray *array = [arrayFiltered mutableCopy];
    
    [array sortUsingDescriptors:
     @[
       [NSSortDescriptor sortDescriptorWithKey:@"taskid_Integer" ascending:NO],
       ]];
    
    arrayFiltered = array;
    
    //    for (task *obj in arrayFiltered) {
    //        NSLog(@"filtered tast id === %@ and task status === %@ and created by id ==`= %@",obj.taskid,obj.status,obj.userid);
    //    }
    
    return arrayFiltered;
    
}





@end

