//
//  myActivityTableViewCell.h
//  BuyTimee
//
//  Created by mitesh on 28/03/17.
//  Copyright © 2017 User14. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface myActivityTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *userProfileLeft;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *userProfileTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *userProfileHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *userProfileWidth;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *nameLblLeft;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *nameLblTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *nameLblWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *nameLblHeight;
@property (weak, nonatomic) IBOutlet UILabel *nameLbl;

@property (weak, nonatomic) IBOutlet UILabel *lblPrice;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lblPriceHeight;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lblPriceWidth;


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *locationTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *locationLblLeft;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *locationLblHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *locationLblWidth;
@property (weak, nonatomic) IBOutlet UILabel *locationLbl;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *purchaseLblLeft;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *purchaseLblTop;
@property (weak, nonatomic) IBOutlet UILabel *purchasedLbl;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *purchaseLblWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *purchaseLblHeight;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *dateLblTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *dateLblRight;
@property (weak, nonatomic) IBOutlet UILabel *dateLbl;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *dateLblHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *dateLblWidth;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *rightArrowTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *rightArrowRight;
@property (weak, nonatomic) IBOutlet UIImageView *rightArrow;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *rightArrowHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *rightArrowWidth;

@property (weak, nonatomic) IBOutlet UIButton *reviewBtn;
@property (weak, nonatomic) IBOutlet UILabel *reviewLine;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *reviewBtnTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *reviewBtnRight;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *reviewBtnWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *reviewBtnHeight;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *reviewLineTop;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *reviewLineRight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *reviewLineWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *reviewLineHeight;
@property (weak, nonatomic) IBOutlet UIImageView *UserImgView;
@property (weak, nonatomic) IBOutlet UIButton *buttonForReviewDetail;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consLblPriceTop;




@end
