//
//  ResetPasswordViewController.m
//  BuyTimee
//
//  Created by apple on 21/03/16.
//  Copyright © 2016 User14. All rights reserved.
//

#import "ResetPasswordViewController.h"
#import "GlobalFunction.h"
#import "Constants.h"
#import "LoadingView.h"

@interface ResetPasswordViewController ()

- (IBAction)cancelBtnClicked:(id)sender;
- (IBAction)updatePasswordClicked:(id)sender;

@end

@implementation ResetPasswordViewController{
    
    LoadingView *loadingView;
    
}


@synthesize txtActiveField;
@synthesize inputAccView;
@synthesize btnDone;
@synthesize btnNext;
@synthesize btnPrev;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    //self.scrollView.contentSize = CGSizeMake(0, 100);
    
    
    
    self.textFieldOldPassword.delegate=self;
    self.textFieldNewPassword.delegate=self;
    self.textFiledConfirmPassword.delegate=self;
    
    NSAttributedString *str1 = [[NSAttributedString alloc] initWithString:@"Old Password" attributes:@{ NSForegroundColorAttributeName : [UIColor whiteColor],
                                                                                                        }];
    self.textFieldOldPassword.attributedPlaceholder = str1;
    
    
    NSAttributedString *str2 = [[NSAttributedString alloc] initWithString:@"New Password" attributes:@{ NSForegroundColorAttributeName : [UIColor whiteColor],
                                                                                                        }];
    self.textFieldNewPassword.attributedPlaceholder = str2;
    
    
    NSAttributedString *str3 = [[NSAttributedString alloc] initWithString:@"Confirm Password" attributes:@{ NSForegroundColorAttributeName : [UIColor whiteColor],
                                                                                                            }];
    self.textFiledConfirmPassword.attributedPlaceholder = str3;
    
    self.textFieldOldPassword.secureTextEntry = true;
    
    UIFont *font = self.textFieldNewPassword.font;
    
    if (IS_IPHONE_4_OR_LESS) {
        self.textFieldOldPassword.font = [font fontWithSize:14.0];
        self.textFieldNewPassword.font = [font fontWithSize:14.0];
        self.textFiledConfirmPassword.font = [font fontWithSize:14.0];
        
    }
    if (IS_IPHONE_5) {
        self.textFieldOldPassword.font = [font fontWithSize:15.0];
        self.textFieldNewPassword.font = [font fontWithSize:15.0];
        self.textFiledConfirmPassword.font = [font fontWithSize:15.0];
        
    }
    if (IS_IPHONE_6) {
        self.textFieldOldPassword.font = [font fontWithSize:16.0];
        self.textFieldNewPassword.font = [font fontWithSize:16.0];
        self.textFiledConfirmPassword.font = [font fontWithSize:16.0];
        
    }
    if (IS_IPHONE_6P) {
        self.textFieldOldPassword.font = [font fontWithSize:17.0];
        self.textFieldNewPassword.font = [font fontWithSize:17.0];
        self.textFiledConfirmPassword.font = [font fontWithSize:17.0];
        
    }
    
}


-(void)resignTextFields{
    [self.textFieldOldPassword resignFirstResponder];
    [self.textFieldNewPassword resignFirstResponder];
    [self.textFiledConfirmPassword resignFirstResponder];
}

#pragma mark - create accessory view
-(void)createInputAccessoryView{
    // Create the view that will play the part of the input accessory view.
    // Note that the frame width (third value in the CGRectMake method)
    // should change accordingly in landscape orientation. But we don’t care
    // about that now.
    inputAccView = [[UIView alloc] initWithFrame:CGRectMake(10.0, 0.0, self.view.frame.size.width, 40.0)];
    // Set the view’s background color. We’ ll set it here to gray. Use any color you want.
    [inputAccView setBackgroundColor:[UIColor lightGrayColor]];
    // We can play a little with transparency as well using the Alpha property. Normally
    // you can leave it unchanged.
    [inputAccView setAlpha: 0.8];
    // If you want you may set or change more properties (ex. Font, background image,e.t.c.).
    // For now, what we’ ve already done is just enough.
    // Let’s create our buttons now. First the previous button.
    btnPrev = [UIButton buttonWithType: UIButtonTypeCustom];
    // Set the button’ s frame. We will set their widths to 80px and height to 40px.
    [btnPrev setFrame: CGRectMake(0.0, 0.0, 80.0, 40.0)];
    // Title.
    [btnPrev setTitle: @"Previous" forState: UIControlStateNormal];
    // Background color.
    [btnPrev setBackgroundColor: [UIColor clearColor]];
    // You can set more properties if you need to.
    // With the following command we’ ll make the button to react in finger tapping. Note that the
    // gotoPrevTextfield method that is referred to the @selector is not yet created. We’ ll create it
    // (as well as the methods for the rest of our buttons) later.
    [btnPrev addTarget: self action: @selector(gotoPrevTextfield) forControlEvents: UIControlEventTouchUpInside];
    // Do the same for the two buttons left.
    btnNext = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnNext setFrame:CGRectMake(85.0f, 0.0f, 80.0f, 40.0f)];
    [btnNext setTitle:@"Next" forState:UIControlStateNormal];
    [btnNext setBackgroundColor:[UIColor clearColor]];
    [btnNext addTarget:self action:@selector(gotoNextTextfield) forControlEvents:UIControlEventTouchUpInside];
    btnDone = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnDone setFrame:CGRectMake(self.view.frame.size.width - 80.0f, 0.0f, 80.0f, 40.0f)];
    [btnDone setTitle:@"Done" forState:UIControlStateNormal];
    [btnDone setBackgroundColor:[UIColor clearColor]];
    [btnDone setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [btnDone addTarget:self action:@selector(doneTyping) forControlEvents:UIControlEventTouchUpInside];
    // Now that our buttons are ready we just have to add them to our view.
    [inputAccView addSubview:btnPrev];
    [inputAccView addSubview:btnNext];
    [inputAccView addSubview:btnDone];
}


-(void)gotoPrevTextfield{
    // If the active textfield is the first one, can't go to any previous
    // field so just return.
    if (txtActiveField == self.textFieldOldPassword) {
        [txtActiveField resignFirstResponder];
        return;
    }
    if (txtActiveField == self.textFieldNewPassword) {
        
        [self.textFieldNewPassword resignFirstResponder];
        [self.textFieldOldPassword becomeFirstResponder];
        
        
        return;
    }
    
    if (txtActiveField == self.textFiledConfirmPassword) {
        
        [self.textFiledConfirmPassword resignFirstResponder];
        [self.textFieldNewPassword becomeFirstResponder];
        
        return;
    }
    
}


-(void)gotoNextTextfield{
    // If the active textfield is the second one, there is no next so just return.
    if (txtActiveField == self.textFieldOldPassword) {
        
        [self.textFieldOldPassword resignFirstResponder];
        [self.textFieldNewPassword becomeFirstResponder];
        return;
    }
    if (txtActiveField == self.textFieldNewPassword) {
        
        [self.textFieldNewPassword resignFirstResponder];
        [self.textFiledConfirmPassword becomeFirstResponder];
        return;
    }
    
    if (txtActiveField == self.textFiledConfirmPassword) {
        
        [self.textFieldNewPassword resignFirstResponder];
        [txtActiveField resignFirstResponder];
        return;
    }
    
}


-(void)doneTyping{
    // When the "done" button is tapped, the keyboard should go away.
    // That simply means that we just have to resign our first responder.
    [txtActiveField resignFirstResponder];
}


-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    
    return YES;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    
    [self createInputAccessoryView];
    // Now add the view as an input accessory view to the selected textfield.
    [textField setInputAccessoryView:inputAccView];
    // Set the active field. We' ll need that if we want to move properly
    // between our textfields.
    txtActiveField = textField;
    
    
    if(IS_IPHONE_4_OR_LESS){
        if (self.txtActiveField==_textFieldOldPassword) {
            CGRect frame = self.contentView.frame;
            frame.origin.y = self.contentViewTop.constant;
            
            [UIView animateWithDuration:0.3 animations:^{
                self.contentView.frame = frame;
            } completion:^(BOOL finished) {
                
            }];
            
        }
        if(textField==_textFieldNewPassword){
            
            CGRect frame = self.contentView.frame;
            frame.origin.y =-45;
            
            [UIView animateWithDuration:0.3 animations:^{
                self.contentView.frame = frame;
            } completion:^(BOOL finished) {
                
            }];
            
        }
        if(textField==_textFiledConfirmPassword){
            
            CGRect frame = self.contentView.frame;
            frame.origin.y =-100;
            
            [UIView animateWithDuration:0.3 animations:^{
                self.contentView.frame = frame;
            } completion:^(BOOL finished) {
                
            }];
        }
    }
    
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardWillHideNotification object:nil];
    
    [self.view endEditing:YES];
    return YES;
}



-(void)keyboardDidHide:(NSNotification *)notification{
    
    [UIView animateWithDuration:0.10 animations:^{
        [self.contentView setFrame:CGRectMake(0,self.contentViewTop.constant, self.contentView.frame.size.width, self.contentView.frame.size.height)];
    } completion:^(BOOL finished) {
        [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    }];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)cancelBtnClicked:(id)sender {
    
    [txtActiveField resignFirstResponder];
    
    [self dismissViewControllerAnimated:YES completion:nil];

}

-(void)addIndicator{
    
    [GlobalFunction addIndicatorView];
    
}


-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex{
    
    if (alertView.tag == 10001) {
        
        if(buttonIndex == [alertView cancelButtonIndex]){
            
            [GlobalFunction addIndicatorView];
            
            myAppDelegate.loginVCObj.textFieldPassword.text = self.textFieldNewPassword.text;
            
            [self dismissViewControllerAnimated:YES completion:nil];
            
            [myAppDelegate.loginVCObj loginComplete];
            
        }
    }
}

-(void)removeIndicator{
    
    [GlobalFunction removeIndicatorView];
    
}


- (IBAction)updatePasswordClicked:(id)sender {
    
    if ([[self.textFieldOldPassword.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""]) {
        
        [[GlobalFunction shared] showAlertForMessage:@"OTP cannot be blank."];
        
        return;
    }
    if ([[self.textFieldNewPassword.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""]) {
        
        [[GlobalFunction shared] showAlertForMessage:@"Password cannot be blank."];
        
        return;
    }
    if ([[self.textFiledConfirmPassword.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""]) {
        
        [[GlobalFunction shared] showAlertForMessage:@"Confirm password cannot be blank."];
        
        return;
    }
    
    if (![self.textFieldNewPassword.text isEqualToString:self.textFiledConfirmPassword.text]) {
        
        [[GlobalFunction shared] showAlertForMessage:@"Confirm password does not match."];
        
        return;

    }

    [self performSelector:@selector(addIndicator) withObject:nil afterDelay:0.0];
    
    [self performSelector:@selector(performWorkForResetPassword) withObject:nil afterDelay:0.5];

}

-(void)performWorkForResetPassword{
    
    if (![[WebService shared] connected]) {
        
        [GlobalFunction removeIndicatorView];
        
        [[GlobalFunction shared] showAlertForMessage:@"Can't connect to internet, please check and try again."];
        
        return;
        
    }
    
    
    NSDictionary *dictReg = [webServicesShared resetPasswordForUserId:myAppDelegate.userData.userid newPassword:self.textFieldNewPassword.text otp:self.textFieldOldPassword.text];
    
    if (dictReg != nil) {
        
        if ([[EncryptDecryptFile decrypt:[dictReg valueForKey:@"responseCode"]] isEqualToString:@"200"]) {
            
            [self removeIndicator];
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"BUY TIMEE" message:[EncryptDecryptFile decrypt:[dictReg valueForKey:@"responseMessage"]] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            
            alert.tag = 10001;
            
            [alert show];
            
        }else{
            
            [self removeIndicator];
            
            [[GlobalFunction shared] showAlertForMessage:[EncryptDecryptFile decrypt:[dictReg valueForKey:@"responseMessage"]]];
            
        }
        
    }else{
        
        [self removeIndicator];
        
        [[GlobalFunction shared] showAlertForMessage:@"Server is down, please try again later."];
        
    }
    
}


@end
