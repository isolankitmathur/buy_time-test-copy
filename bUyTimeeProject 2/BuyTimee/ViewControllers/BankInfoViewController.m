//
//  BankInfoViewController.m
//  BuyTimee
//
//  Created by User38 on 07/02/17.
//  Copyright © 2017 User14. All rights reserved.
//

#import "BankInfoViewController.h"
#import "Constants.h"
#import "GlobalFunction.h"
#import "WebService.h"
#import <AVFoundation/AVFoundation.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import <Photos/Photos.h>
#import "SelectCategoryTableViewCell.h"
#import "category.h"

#import "GooglePlacesData.h"

#import <GooglePlaces/GooglePlaces.h>


@import GooglePlaces;
@import GooglePlacePicker;
@import GoogleMaps;

@interface BankInfoViewController ()<UIImagePickerControllerDelegate,UIActionSheetDelegate,UIAlertViewDelegate,CLLocationManagerDelegate, GMSAutocompleteTableDataSourceDelegate, GMSAutocompleteFetcherDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *imgViewUsersPicture;
@end

@implementation BankInfoViewController
{
    // google place api work...
    
    GMSPlacesClient *_placesClient;
    CLLocationManager *locationManager;
    GMSPlacePicker *_placePicker;
    
    GMSAutocompleteTableDataSource *_tableDataSource;
    UISearchController *_searchBar;
    
    UISearchDisplayController *_searchDisplayController;
    
    GMSAutocompleteFetcher *_fetcher;
}
@synthesize btnDone,btnNext,btnPrev,btnSave,inputAccView,txtActiveField;

BOOL isPictureAvailabe;
BOOL isOldPictureAvailable;


BOOL isOpened;

NSMutableArray *catAry;

category *selectedCategoryObj;


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    catAry = [DatabaseClass getDataFromCategoryData:[[NSString stringWithFormat:@"Select * from %@",tableCategory] UTF8String]];
    isOpened = false;
    self.tbleView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    self.tbleView.hidden=YES;
    [GlobalFunction createBorderforView:self.tbleView withWidth:1.0 cornerRadius:0.0 colorForBorder:RGB(151, 151, 151)];
    
    self.blackTransparentView.hidden = true;
    self.blackTransparentView.alpha = 0;
    
    myAppDelegate.vcObj.btnMapOnListView.hidden = true;//vs
    myAppDelegate.vcObj.BtnTransparentMap.hidden = true;//vs
    
    self.LogoHolderView.backgroundColor = [UIColor whiteColor];
    self.textFieldUsername.delegate=self;
    self.textFieldAdd.delegate=self;
    self.textFieldYelp.delegate=self;
    self.textFieldInsta.delegate=self;
    self.textFieldServices.delegate=self;
    
    NSAttributedString *star = [[NSAttributedString alloc] initWithString:@"*" attributes:@{ NSForegroundColorAttributeName :[UIColor redColor],
                                                                                             }];
    
    NSMutableAttributedString *str1 = [[NSMutableAttributedString alloc] initWithString:@"Business Name" attributes:@{ NSForegroundColorAttributeName :[UIColor colorWithRed:143.0/255.0f green:143.0/255.0f blue:143.0/255.0f alpha:1.0],
                                                                                                       }];
    [str1 appendAttributedString:star];
     self.textFieldUsername.attributedPlaceholder = str1;
    
    NSMutableAttributedString *str2 = [[NSMutableAttributedString alloc] initWithString:@"Location" attributes:@{ NSForegroundColorAttributeName :[UIColor colorWithRed:143.0/255.0f green:143.0/255.0f blue:143.0/255.0f alpha:1.0],
                                                                                                         }];
    [str2 appendAttributedString:star];
    self.textFieldAdd.attributedPlaceholder = str2;
    
    NSAttributedString *str3 = [[NSAttributedString alloc] initWithString:@"Yelp Profile" attributes:@{ NSForegroundColorAttributeName :[UIColor colorWithRed:143.0/255.0f green:143.0/255.0f blue:143.0/255.0f alpha:1.0],
                                                                                                   }];
    self.textFieldYelp.attributedPlaceholder = str3;
    
    NSAttributedString *str4 = [[NSAttributedString alloc] initWithString:@"Instagram(username)" attributes:@{ NSForegroundColorAttributeName :[UIColor colorWithRed:143.0/255.0f green:143.0/255.0f blue:143.0/255.0f alpha:1.0],
                                                                                                        }];
    self.textFieldInsta.attributedPlaceholder = str4;
    
    NSMutableAttributedString *str5 = [[NSMutableAttributedString alloc] initWithString:@"Services" attributes:@{ NSForegroundColorAttributeName :[UIColor colorWithRed:143.0/255.0f green:143.0/255.0f blue:143.0/255.0f alpha:1.0],
                                                                                                     }];
    [str5 appendAttributedString:star];
    self.textFieldServices.attributedPlaceholder = str5;
    
    
   
    
    NSMutableAttributedString *catName = [[NSMutableAttributedString alloc] initWithString:@"Choose Category" attributes:@{ NSForegroundColorAttributeName :[UIColor colorWithRed:63.0/255.0f green:67.0/255.0f blue:70.0/255.0f alpha:1.0],
                                                                                                                          }];
    [catName appendAttributedString:star];
    
    [_chooseBtn setAttributedTitle:catName forState:UIControlStateNormal];
    
    [self updateConstraints];

    
    // Google api work...
    
    
    
    
}

-(void) viewDidAppear:(BOOL)animated
{
    NSString* final = @"Vendor profile page";
    
    [Flurry logEvent:final];

    NSLog(@"possition of save btn y is..%f",self.btnSave.frame.origin.y);
    NSLog(@"height of save btn y is..%f",self.btnSave.frame.size.height);
    
    NSLog(@"possition of cncl btn y is..%f",_btnCancel.frame.origin.y);
    NSLog(@"height of save cncl y is..%f",_btnCancel.frame.size.height);
    

    if (_imagePickFromAlbum==YES)
    {
        _imagePickFromAlbum=NO;
        [GlobalFunction removeIndicatorView];
    }
    else
    {
        [self getVendorData];
    }
    
//   // self.condTableViewTop.constant = -100;
//    if(IS_IPHONE_4_OR_LESS)
//    {
//        self.tbleViewHeight.constant = 28*catAry.count;
//
//    }
//
//    else
//    {
//        self.tbleViewHeight.constant = 40*catAry.count;
//
//    }
}


-(NSString*)categoryNameByCategoryID:(NSString*)categoryID
{
    for (category * cat in catAry) {
            if ([cat.categoryId isEqualToString:categoryID]) {
                return cat.name;
            }
        
    }
    return nil;
}

-(void)getVendorData
{
    
    if (![[WebService shared] connected]) {
        
        [GlobalFunction removeIndicatorView];
        
        [[GlobalFunction shared] showAlertForMessage:@"Can't connect to internet, please check and try again."];
        
        return;
    }
    
    NSDictionary*dictReg = [webServicesShared viewVendorUserid:myAppDelegate.userData.userid];
    
    
  //  NSLog(@"response --- %@",[EncryptDecryptFile decrypt:[dictReg valueForKey:@"responseMessage"]]);
    
  //  NSLog(@"response --- %@",[EncryptDecryptFile decrypt:[dictReg valueForKey:@"responseCode"]]);
    
    if (dictReg != nil)
    {
        
    
    
    if ([[EncryptDecryptFile decrypt:[dictReg valueForKey:@"responseCode"]] isEqualToString:@"200"]) {
        
        NSArray *dictDataReg = [dictReg objectForKey:@"data"];
        
        
        if (dictDataReg) {
   
            self.textFieldUsername.text = [EncryptDecryptFile decrypt:[dictDataReg valueForKey:@"name"]];
            
            self.textFieldAdd.text = [EncryptDecryptFile decrypt:[dictDataReg valueForKey:@"address"]];
            self.textFieldYelp.text = [EncryptDecryptFile decrypt:[dictDataReg valueForKey:@"yelpProfile"]];
            self.textFieldInsta.text = [EncryptDecryptFile decrypt:[dictDataReg valueForKey:@"instaLink"]];
            self.textFieldServices.text = [EncryptDecryptFile decrypt:[dictDataReg valueForKey:@"services"]];
            
            
            NSString * catID = [EncryptDecryptFile decrypt:[dictDataReg valueForKey:@"categoryId"]];
            NSString *categoryName = [self categoryNameByCategoryID:[EncryptDecryptFile decrypt:[dictDataReg valueForKey:@"categoryId"]]];
            
            NSMutableAttributedString *FinalCategoryName = [[NSMutableAttributedString alloc] initWithString:categoryName];
            
            NSAttributedString *star = [[NSAttributedString alloc] initWithString:@"*" attributes:@{ NSForegroundColorAttributeName :[UIColor redColor],
                                                                                                     }];
            
            NSMutableAttributedString *catName = [[NSMutableAttributedString alloc] initWithString:@"Choose Category" attributes:@{ NSForegroundColorAttributeName :[UIColor colorWithRed:63.0/255.0f green:67.0/255.0f blue:70.0/255.0f alpha:1.0],
                                                                                                                               }];
            [catName appendAttributedString:star];
            
            
            if(!categoryName)
            {
                [_chooseBtn setAttributedTitle:catName forState:UIControlStateNormal];
            }
            else
            {
                [_chooseBtn setAttributedTitle:FinalCategoryName forState:UIControlStateNormal];
               //[_chooseBtn setTitle:categoryName forState:UIControlStateNormal];
            }
            
            
            NSString *mainPic = [EncryptDecryptFile decrypt:[dictDataReg valueForKey:@"picPath"]];
            
            if (![mainPic isEqualToString:@""])
            {
                NSArray *components = [mainPic componentsSeparatedByString:@"/"];
                NSString *imgNameWithJPEG = [components lastObject];
                imgNameWithJPEG = [imgNameWithJPEG stringByAppendingString:@".jpeg"];
                NSArray *componentsWithDots = [imgNameWithJPEG componentsSeparatedByString:@"."];
                if (componentsWithDots.count>2) {
                    imgNameWithJPEG = [NSString stringWithFormat:@"%@.%@",[componentsWithDots firstObject],[componentsWithDots lastObject]];
                }
           
               UIImage *img = [GlobalFunction loadImageOfLogoFromCacheFolder:imgNameWithJPEG dirname:@"vender"];
                if (img) {
                    
                    self.imgViewUsersPicture.image = img;
                    self.imgViewUsersPicture.layer.cornerRadius = self.imgViewUsersPicture.frame.size.height/2;
                    self.imgViewUsersPicture.layer.masksToBounds = YES;
                    isPictureAvailabe = YES;
                }
                else
                {
                    
                   NSString *fullPath=[NSString stringWithFormat:urlServer,mainPic];
                    
                   UIImage *img11 = [GlobalFunction getImageFromURL:fullPath];
                    
                    self.imgViewUsersPicture.image = img11;
                    
                    NSLog(@"%f",_imgViewUsersPicture.frame.size.height);
                    NSLog(@"%f",_imgViewUsersPicture.frame.size.width);

                    
                    
                    
                    self.imgViewUsersPicture.layer.cornerRadius = self.imgViewUsersPicture.frame.size.height/2;
                    self.imgViewUsersPicture.layer.masksToBounds = YES;
                    
                    NSLog(@"%f",_imgViewUsersPicture.frame.size.height);
                    NSLog(@"%f",_imgViewUsersPicture.frame.size.width);

                    
                    
                    isPictureAvailabe = YES;

                }

            }
            
        }
    }

    else
    {
        [GlobalFunction removeIndicatorView];
    }
}
    else
    {
    [GlobalFunction removeIndicatorView];
    [[GlobalFunction shared] showAlertForMessage:@"Server is down, please try again later."];
    }
     [GlobalFunction removeIndicatorView];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    [self updateConstraints];
    
    [GlobalFunction addIndicatorView];
   
    

}

-(void) updateConstraints
{
    [self.view layoutIfNeeded];
    
    self.tbleViewHeight.constant = 0;

    
     self.consBtnCancelLeading.constant = 0.5f;
    _btnCancel.backgroundColor=[UIColor colorWithRed:63.0/255.0f green:67.0/255.0f blue:70.0/255.0f alpha:1.0];
    btnSave.backgroundColor=[UIColor colorWithRed:63.0/255.0f green:67.0/255.0f blue:70.0/255.0f alpha:1.0];
    self.insideViewOfBlurView.layer.cornerRadius = 5.0;
    
    if (IS_IPHONE_5)
    {
        _alertViewLeft.constant=35;
        _alertViewWidth.constant=250;
        _alertViewHeight.constant=120;
        _alertViewTop.constant=150;
        self.nsConsHelpTop.constant = 11;
        
        self.textFldOneLeading.constant = 22;
        self.textFldTwoLeading.constant = 22;
        self.textFldThreeLeading.constant = 22;
        self.textFldFourLeading.constant = 22;
        self.textFldFiveLeading.constant = 22;
        self.chooseBtnLeading.constant = 22;

        
        
        [self.textFieldUsername setFont:[UIFont fontWithName:@"Lato-Regular" size:15]];
        [self.textFieldAdd setFont:[UIFont fontWithName:@"Lato-Regular" size:15]];
        [self.textFieldYelp setFont:[UIFont fontWithName:@"Lato-Regular" size:15]];
        [self.textFieldInsta setFont:[UIFont fontWithName:@"Lato-Regular" size:15]];
        [self.textFieldServices setFont:[UIFont fontWithName:@"Lato-Regular" size:15]];
       
        
         [self.btnSave.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:17]];
         [self.btnCancel.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:17]];
          [self.lblUploadLogo setFont:[UIFont fontWithName:@"Lato-Regular" size:15]];
        
        [self.chooseBtn.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:15]];
        [_chooseBtn setImageEdgeInsets:UIEdgeInsetsMake(0, 268, 0, 0)];
        
        self.consUpperviewHeight.constant = 135;
        self.consBtnTransTop.constant = 5;
        self.consLogoImgTop.constant = 8;
        self.consUploadLogoLblTop.constant = 15;
        //self.consTextFldViewTop.constant = 10;
        self.consTextFieldssTopSpace.constant =130;
        
        
        self.consTextNameTop.constant = 16;
        self.consTextAddressTop.constant = 16;
        self.consTextYelpTop.constant = 16;
        self.consTextInstaTop.constant = 16;
        self.consTextServicesTop.constant = 16;
        self.chooseBtnTop.constant = 16;
        
        self.lineOneTop.constant = 6;
        self.lineTwoTop.constant = 6;
        self.lineThreeTop.constant = 6;
        self.lineFourTop.constant = 6;
        self.lineFiveTop.constant = 6;
        self.chooseBtnLineTop.constant = 6;
        
        
        _textFldViewHeight.constant=380;
        self.consImgViewLogoWidth.constant = 56;
        self.consImgViewLogoHeight.constant = 55;
        self.consUploadLogoLblTop.constant=19;
        self.consImgViewLeading.constant = 132;
        self.consUploadLogoLeading.constant = 108;
        self.consLogoImgTop.constant = 23;
        self.consBtnTransparentLeading.constant = 108;
        
        
        _consBtnSaveBottom.constant=60;
        _consBtnCancelBottom.constant=60;
        self.consBtnSaveWidth.constant = 160;
        self.consBtnCancelWidth.constant = 160;
        self.btnsUnderViewBottom.constant = 60;
        self.btnsUnderViewHeight.constant = 40;
        self.consBtnSaveHeight.constant = 40;
        self.consBtnCancelHeight.constant = 40;

//        self.consLogoImgTop.constant = 32;
//        self.consImgViewLogoWidth.constant = 76;
//        self.consImgViewLogoHeight.constant = 75;
//        self.consImgViewLeading.constant = 169;
//        self.consUploadLogoLeading.constant = 151;
//        self.consUploadLogoLblTop.constant = 25;
        NSLog(@"update constraint ....");
        
    }
    if (IS_IPHONE_6)
    {
        self.nsConsHelpLeading.constant = 5;
        self.consTextNameTop.constant = 11;
//        _alertViewLeft.constant=35;
//        _alertViewWidth.constant=300;
//        _alertViewHeight.constant=160;
        
        self.consTextAddressTop.constant = 24;
        
        self.consTextYelpTop.constant = 23;
        self.consTextInstaTop.constant = 24;
         self.consTextServicesTop.constant = 24;
        self.chooseBtnTop.constant = 23;
        
        self.lineTwoTop.constant = 8;
        self.lineThreeTop.constant = 9;
        self.lineFourTop.constant = 9;
        self.lineFiveTop.constant = 9;
        self.chooseBtnLineTop.constant = 9;
        
        
        _textFldViewHeight.constant=500;
        
        self.consBtnSaveHeight.constant = 45;
        self.consBtnCancelHeight.constant = 45;
        _consBtnSaveBottom.constant=72;
        _consBtnCancelBottom.constant=72;
        
        [self.btnSave.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:19]];
        [self.btnCancel.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:19]];
        self.btnsUnderViewBottom.constant = 72;
        self.btnsUnderViewHeight.constant = 45;

    }
    if (IS_IPHONE_6P)
    {
        self.nsConsHelpTop.constant = 17;
        self.nsConsHelpLeading.constant = 6;
        
        
        _alertViewTop.constant=230;
        _alertViewLeft.constant=88;
        
        self.textFldOneLeading.constant = 28;
        self.textFldTwoLeading.constant = 29;
        self.textFldThreeLeading.constant = 29;
        self.textFldFourLeading.constant = 29;
        self.textFldFiveLeading.constant = 29;
        self.chooseBtnLeading.constant = 29;
        
        [self.textFieldUsername setFont:[UIFont fontWithName:@"Lato-Regular" size:19]];
        [self.textFieldAdd setFont:[UIFont fontWithName:@"Lato-Regular" size:19]];
        [self.textFieldYelp setFont:[UIFont fontWithName:@"Lato-Regular" size:19]];
        [self.textFieldInsta setFont:[UIFont fontWithName:@"Lato-Regular" size:19]];
        [self.textFieldServices setFont:[UIFont fontWithName:@"Lato-Regular" size:19]];
        [self.chooseBtn.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:19]];
        
        [_chooseBtn setImageEdgeInsets:UIEdgeInsetsMake(0, 353, 0, 0)];
        
        [self.btnSave.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:21]];
        [self.btnCancel.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:21]];

         [self.lblUploadLogo setFont:[UIFont fontWithName:@"Lato-Regular" size:18]];
        self.consUpperviewHeight.constant = 170;
        self.consLogoImgTop.constant = 32;
        self.consImgViewLogoWidth.constant = 76;
        self.consImgViewLogoHeight.constant = 75;
        self.consImgViewLeading.constant = 169;
        self.consUploadLogoLeading.constant = 151;
        self.consBtnTransparentLeading.constant = 151;
        self.consBtnTransTop.constant = 28;
        self.consUploadLogoLblTop.constant = 25;
        

        self.consTextNameTop.constant = 33;
        self.consTextAddressTop.constant = 30;
        self.consTextYelpTop.constant = 29;
        self.consTextInstaTop.constant = 30;
        self.consTextServicesTop.constant = 30;
        self.chooseBtnTop.constant = 30;
//        self.chooseBtnTop.constant = 26;
        
        self.lineOneTop.constant = 11;
        self.lineTwoTop.constant = 11;
        self.lineThreeTop.constant = 12;
        self.lineFourTop.constant = 11;
        self.lineFiveTop.constant = 11;
        self.chooseBtnLineTop.constant = 11;
        
        
         self.consBtnSaveWidth.constant = 207;
         self.consBtnCancelWidth.constant = 207;
         _textFldViewHeight.constant=500;
        
        self.consBtnSaveHeight.constant = 50;
        self.consBtnCancelHeight.constant = 50;
        _consBtnSaveBottom.constant=75;
        _consBtnCancelBottom.constant=75;
        _textFldViewHeight.constant=700;

        self.btnsUnderViewBottom.constant = 75;
        self.btnsUnderViewHeight.constant = 50;


    }
    
    
    if(IS_IPHONE_4_OR_LESS)
    {
        _alertViewLeft.constant=35;
        _alertViewWidth.constant=250;
        _alertViewHeight.constant=120;
        _alertViewTop.constant=150;
        self.nsConsHelpTop.constant = 5;
        self.nsConsHelpLeading.constant=8;
        
        
        
        self.textFldOneLeading.constant = 22;
        self.textFldTwoLeading.constant = 22;
        self.textFldThreeLeading.constant = 22;
        self.textFldFourLeading.constant = 22;
        self.textFldFiveLeading.constant = 22;
        self.chooseBtnLeading.constant = 22;
        
        [self.textFieldUsername setFont:[UIFont fontWithName:@"Lato-Regular" size:15]];
        [self.textFieldAdd setFont:[UIFont fontWithName:@"Lato-Regular" size:15]];
        [self.textFieldYelp setFont:[UIFont fontWithName:@"Lato-Regular" size:15]];
        [self.textFieldInsta setFont:[UIFont fontWithName:@"Lato-Regular" size:15]];
        [self.textFieldServices setFont:[UIFont fontWithName:@"Lato-Regular" size:15]];
        [self.btnSave.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:17]];
        [self.btnCancel.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:17]];
        [self.lblUploadLogo setFont:[UIFont fontWithName:@"Lato-Regular" size:15]];
        
        [self.chooseBtn.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:15]];
        [_chooseBtn setImageEdgeInsets:UIEdgeInsetsMake(0, 268, 0, 0)];
        
        self.consUpperviewHeight.constant = 135;
        self.consBtnTransTop.constant = 5;
        self.consLogoImgTop.constant = 8;
        self.consUploadLogoLblTop.constant = 15;
        //self.consTextFldViewTop.constant = 10;
        self.consTextFieldssTopSpace.constant =130;
        
        
        self.consTextNameTop.constant = 16;
        self.consTextAddressTop.constant = 16;
        self.consTextYelpTop.constant = 16;
        self.consTextInstaTop.constant = 16;
        self.consTextServicesTop.constant = 16;
        self.chooseBtnTop.constant = 16;
        
        self.lineOneTop.constant = 6;
        self.lineTwoTop.constant = 6;
        self.lineThreeTop.constant = 6;
        self.lineFourTop.constant = 6;
        self.lineFiveTop.constant = 6;
        self.chooseBtnLineTop.constant = 6;
        

        
        _consTxtFlsUserNameHeight.constant=20;
        _consTxtFldYulpHeight.constant=20;
        _consTxtFldInstaHeight.constant=20;
        _consTxtFldAdressHeight.constant=20;
        _consTxtFldServiceHeight.constant=20;
        
        
        _textFldViewHeight.constant=380;
        self.consImgViewLogoWidth.constant = 56;
        self.consImgViewLogoHeight.constant = 55;
        self.consUploadLogoLblTop.constant=19;
        self.consImgViewLeading.constant = 132;
        self.consUploadLogoLeading.constant = 108;
        self.consLogoImgTop.constant = 23;
        self.consBtnTransparentLeading.constant = 108;
        
        
        _consBtnSaveBottom.constant=60;
        _consBtnCancelBottom.constant=60;
        self.consBtnSaveWidth.constant = 160;
        self.consBtnCancelWidth.constant = 160;
        self.btnsUnderViewBottom.constant = 60;
        self.btnsUnderViewHeight.constant = 40;
        self.consBtnSaveHeight.constant = 40;
        self.consBtnCancelHeight.constant = 40;
        
    }
    
    
    
    [self.view layoutIfNeeded];
}


#pragma mark - create accessory view
-(void)createInputAccessoryView{
    // Create the view that will play the part of the input accessory view.
    // Note that the frame width (third value in the CGRectMake method)
    // should change accordingly in landscape orientation. But we don’t care
    // about that now.
    inputAccView = [[UIView alloc] initWithFrame:CGRectMake(10.0, 0.0, self.view.frame.size.width, 40.0)];
    // Set the view’s background color. We’ ll set it here to gray. Use any color you want.
    [inputAccView setBackgroundColor:[UIColor lightGrayColor]];
    // We can play a little with transparency as well using the Alpha property. Normally
    // you can leave it unchanged.
    [inputAccView setAlpha: 0.8];
    // If you want you may set or change more properties (ex. Font, background image,e.t.c.).
    // For now, what we’ ve already done is just enough.
    // Let’s create our buttons now. First the previous button.
    btnPrev = [UIButton buttonWithType: UIButtonTypeCustom];
    // Set the button’ s frame. We will set their widths to 80px and height to 40px.
    [btnPrev setFrame: CGRectMake(0.0, 0.0, 80.0, 40.0)];
    // Title.
    [btnPrev setTitle: @"Previous" forState: UIControlStateNormal];
    // Background color.
    [btnPrev setBackgroundColor: [UIColor clearColor]];
    // You can set more properties if you need to.
    // With the following command we’ ll make the button to react in finger tapping. Note that the
    // gotoPrevTextfield method that is referred to the @selector is not yet created. We’ ll create it
    // (as well as the methods for the rest of our buttons) later.
    [btnPrev addTarget: self action: @selector(gotoPrevTextfield) forControlEvents: UIControlEventTouchUpInside];
    // Do the same for the two buttons left.
    btnNext = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnNext setFrame:CGRectMake(85.0f, 0.0f, 80.0f, 40.0f)];
    [btnNext setTitle:@"Next" forState:UIControlStateNormal];
    [btnNext setBackgroundColor:[UIColor clearColor]];
    [btnNext addTarget:self action:@selector(gotoNextTextfield) forControlEvents:UIControlEventTouchUpInside];
    btnDone = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnDone setFrame:CGRectMake(self.view.frame.size.width - 80.0f, 0.0f, 80.0f, 40.0f)];
    [btnDone setTitle:@"Done" forState:UIControlStateNormal];
    [btnDone setBackgroundColor:[UIColor clearColor]];
    [btnDone setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [btnDone addTarget:self action:@selector(doneTyping) forControlEvents:UIControlEventTouchUpInside];
    // Now that our buttons are ready we just have to add them to our view.
    [inputAccView addSubview:btnPrev];
    [inputAccView addSubview:btnNext];
    [inputAccView addSubview:btnDone];
}


-(void)gotoPrevTextfield{
    // If the active textfield is the first one, can't go to any previous
    // field so just return.
    if (txtActiveField == self.textFieldUsername) {

        CGRect frame = self.textFldView.frame;
        if (IS_IPHONE_5 || IS_IPHONE_4_OR_LESS)
        {
            self.consTextFieldssTopSpace.constant = 135;
        }
        if (IS_IPHONE_6)
        {
            self.consTextFieldssTopSpace.constant = 176;
        }
        if (IS_IPHONE_6P)
        {
            self.consTextFieldssTopSpace.constant = 176;
        }
        
        float x= self.consTextFieldssTopSpace.constant;
        frame.origin.y = x;
        [UIView animateWithDuration:0.3 animations:^{
            
            self.textFldView.frame = frame;
            
        } completion:^(BOOL finished) {
            
        }];
        
        
        [txtActiveField resignFirstResponder];
        return;

    }
    if (txtActiveField == self.textFieldAdd) {
        
        if (IS_IPHONE_5 || IS_IPHONE_4_OR_LESS)
        {
            CGRect frame = self.textFldView.frame;
            // frame.origin.y = -50;
            
            
            self.consTextFieldssTopSpace.constant = 128;
            
            float x= self.consTextFieldssTopSpace.constant;
            frame.origin.y = x;
            
            
            [UIView animateWithDuration:0.3 animations:^{
                self.textFldView.frame = frame;
            } completion:^(BOOL finished) {
                
            }];

        }
        else
        {
        
        CGRect frame = self.textFldView.frame;
        // frame.origin.y = -50;
        
        
        self.consTextFieldssTopSpace.constant = 145;
        
        float x= self.consTextFieldssTopSpace.constant;
        frame.origin.y = x;
        
        
        [UIView animateWithDuration:0.3 animations:^{
            self.textFldView.frame = frame;
        } completion:^(BOOL finished) {
            
        }];
        
        }
        [self.textFieldAdd resignFirstResponder];
        [self.textFieldUsername becomeFirstResponder];
        return;
    }
    if (txtActiveField == self.textFieldYelp) {
        
        CGRect frame = self.textFldView.frame;
       // frame.origin.y = -50;
        
        
        self.consTextFieldssTopSpace.constant = 145;
        
        float x= self.consTextFieldssTopSpace.constant;
        frame.origin.y = x;

        
        [UIView animateWithDuration:0.3 animations:^{
            self.textFldView.frame = frame;
        } completion:^(BOOL finished) {
            
        }];
        
        [self.textFieldYelp resignFirstResponder];
        [self.textFieldAdd becomeFirstResponder];
        return;
    }
        if (txtActiveField == self.textFieldInsta) {
    
            CGRect frame = self.textFldView.frame;
          //  frame.origin.y = -80;
            
            self.consTextFieldssTopSpace.constant = 60;
            
            float x= self.consTextFieldssTopSpace.constant;
            frame.origin.y = x;
            

    
            [UIView animateWithDuration:0.3 animations:^{
                self.textFldView.frame = frame;
            } completion:^(BOOL finished) {
    
            }];
    
            [self.textFieldInsta resignFirstResponder];
            [self.textFieldYelp becomeFirstResponder];
            return;
        }
    if (txtActiveField == self.textFieldServices) {
        
        
        CGRect frame = self.textFldView.frame;
      //  frame.origin.y = -80;
        
        self.consTextFieldssTopSpace.constant = 100;
        
        float x= self.consTextFieldssTopSpace.constant;
        frame.origin.y = x;
        

        
        [UIView animateWithDuration:0.3 animations:^{
            self.textFldView.frame = frame;
        } completion:^(BOOL finished) {
            
        }];
        
        
        //
        //        CGRect frame = self.textFldView.frame;
        //        frame.origin.y = -110;
        //
        //        [UIView animateWithDuration:0.3 animations:^{
        //            self.textFldView.frame = frame;
        //        } completion:^(BOOL finished) {
        //
        //        }];
        
        [self.textFieldServices resignFirstResponder];
        [self.textFieldInsta becomeFirstResponder];
        return;
    }
    
}

-(void)gotoNextTextfield{
    // If the active textfield is the second one, there is no next so just return.
    if (txtActiveField == self.textFieldUsername) {
        
        CGRect frame = self.textFldView.frame;
       // frame.origin.y = -50;
        
        if(IS_IPHONE_4_OR_LESS)
        {
            self.consTextFieldssTopSpace.constant =-500;
        }
        else
        {
            self.consTextFieldssTopSpace.constant = 150;
        }
        
        
        float x= self.consTextFieldssTopSpace.constant;
        frame.origin.y = x;
        
        [UIView animateWithDuration:0.3 animations:^{
            
            self.textFldView.frame = frame;
         
            
    
        } completion:^(BOOL finished) {
            
        }];
        
        [self.textFieldUsername resignFirstResponder];
        [self.textFieldAdd becomeFirstResponder];
        return;
    }
    if (txtActiveField == self.textFieldAdd) {
        
        CGRect frame = self.textFldView.frame;
        //frame.origin.y = -80;
        
        self.consTextFieldssTopSpace.constant = 100;//50;
        
        float x= self.consTextFieldssTopSpace.constant;
        frame.origin.y = x;
        
        
        [UIView animateWithDuration:0.3 animations:^{

           self.textFldView.frame = frame;
            
        } completion:^(BOOL finished) {
            
        }];
        
        [self.textFieldAdd resignFirstResponder];
        [self.textFieldYelp becomeFirstResponder];
        return;
    }
    if (txtActiveField == self.textFieldYelp) {
        
        CGRect frame = self.textFldView.frame;
       // frame.origin.y = -110;
        
         self.consTextFieldssTopSpace.constant = 60;
        float x= self.consTextFieldssTopSpace.constant;
        frame.origin.y = x;
        
        [UIView animateWithDuration:0.3 animations:^{
            
           self.textFldView.frame = frame;
         
        } completion:^(BOOL finished) {
            
        }];
        
        [self.textFieldYelp resignFirstResponder];
        [self.textFieldInsta becomeFirstResponder];
        return;
    }
        if (txtActiveField == self.textFieldInsta) {
    
            CGRect frame = self.textFldView.frame;
            
              self.consTextFieldssTopSpace.constant = 10;
           // frame.origin.y = -140;
    
            float x= self.consTextFieldssTopSpace.constant;
            frame.origin.y = x;
            
            [UIView animateWithDuration:0.3 animations:^{
                
               self.textFldView.frame = frame;
                
            } completion:^(BOOL finished) {
    
            }];
    
            [self.textFieldInsta resignFirstResponder];
            [self.textFieldServices becomeFirstResponder];
            return;
        }
    if (txtActiveField == self.textFieldServices) {
        
        CGRect frame = self.textFldView.frame;
        //frame.origin.y = 0;
        
        if (IS_IPHONE_5 || IS_IPHONE_4_OR_LESS)
        {
            self.consTextFieldssTopSpace.constant = 135;
        }
        if (IS_IPHONE_6)
        {
            self.consTextFieldssTopSpace.constant = 176;
        }
        if (IS_IPHONE_6P)
        {
             self.consTextFieldssTopSpace.constant = 176;
        }
        
        float x= self.consTextFieldssTopSpace.constant;
        frame.origin.y = x;
        [UIView animateWithDuration:0.3 animations:^{
            
            self.textFldView.frame = frame;
            
        } completion:^(BOOL finished) {
            
        }];
        
        
        [txtActiveField resignFirstResponder];
        return;
    }
    
}

-(void)doneTyping{
    // When the "done" button is tapped, the keyboard should go away.
    // That simply means that we just have to resign our first responder.
    
  
    
  //  myAppDelegate.createBioVCObj.ConsGreyLineTop.constant=200;

    
    CGRect frame = self.textFldView.frame;
   // frame.origin.y = 0;
    
    if (IS_IPHONE_5 || IS_IPHONE_4_OR_LESS)
    {
        self.consTextFieldssTopSpace.constant = 135;
    }
    if (IS_IPHONE_6)
    {
        self.consTextFieldssTopSpace.constant = 176;
    }
    if (IS_IPHONE_6P)
    {
        self.consTextFieldssTopSpace.constant = 176;
    }

    float x= self.consTextFieldssTopSpace.constant;
    frame.origin.y = x;
    
    [UIView animateWithDuration:0.3 animations:^{

        self.textFldView.frame = frame;
        
    } completion:^(BOOL finished) {
        
    }];
    
    [txtActiveField resignFirstResponder];
  //   myAppDelegate.createBioVCObj.ConsGreyLineTop.constant=100;

}


#pragma mark - text field delegate
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    
//    CGRect frame = self.textFldView.frame;
//    frame.origin.y = 0;
    self.consTextFldViewTop.constant = 0;
    self.consTextFieldssTopSpace.constant =90;

    
    [UIView animateWithDuration:0.3 animations:^{
      //  self.textFldView.frame = frame;
       // [self doneTyping];
    } completion:^(BOOL finished) {
        
    }];
    
    [textField resignFirstResponder];
    
    
    return YES;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    
    [self createInputAccessoryView];
    // Now add the view as an input accessory view to the selected textfield.
    [textField setInputAccessoryView:inputAccView];
    // Set the active field. We' ll need that if we want to move properly
    // between our textfields.
    txtActiveField = textField;
    
    if (txtActiveField == self.textFieldUsername) {
        

    }
    if (txtActiveField == self.textFieldAdd) {
        
     
        
        if(IS_IPHONE_4_OR_LESS)
        {
            CGRect frame = self.textFldView.frame;
            self.consTextFieldssTopSpace.constant = 40;//60;
            float x= self.consTextFieldssTopSpace.constant;
            frame.origin.y = x;
            [UIView animateWithDuration:0.3 animations:^{
                
                self.textFldView.frame = frame;
                
            } completion:^(BOOL finished) {
                
            }];

        }
        else
        {
            CGRect frame = self.textFldView.frame;
            self.consTextFieldssTopSpace.constant = 100;//60;
            float x= self.consTextFieldssTopSpace.constant;
            frame.origin.y = x;
            [UIView animateWithDuration:0.3 animations:^{
                
                self.textFldView.frame = frame;
                
            } completion:^(BOOL finished) {
                
            }];

        }
        
        
    }
    if (txtActiveField == self.textFieldYelp) {
        
        
        if(IS_IPHONE_4_OR_LESS)
        {
            CGRect frame = self.textFldView.frame;
            
            self.consTextFieldssTopSpace.constant = 10;//90;
            // frame.origin.y = -140;
            
            float x= self.consTextFieldssTopSpace.constant;
            frame.origin.y = x;
            
            [UIView animateWithDuration:0.3 animations:^{
                
                self.textFldView.frame = frame;
                
            } completion:^(BOOL finished) {
                
            }];

        }
        
        else
        {
            CGRect frame = self.textFldView.frame;
            
            self.consTextFieldssTopSpace.constant = 52;//90;
            // frame.origin.y = -140;
            
            float x= self.consTextFieldssTopSpace.constant;
            frame.origin.y = x;
            
            [UIView animateWithDuration:0.3 animations:^{
                
                self.textFldView.frame = frame;
                
            } completion:^(BOOL finished) {
                
            }];

        }
        
        
        
    }
        if (txtActiveField == self.textFieldInsta) {
    
            
            
            if(IS_IPHONE_4_OR_LESS)
            {
                CGRect frame = self.textFldView.frame;
                
                self.consTextFieldssTopSpace.constant = -40;//50;
                // frame.origin.y = -140;
                
                float x= self.consTextFieldssTopSpace.constant;
                frame.origin.y = x;
                
                [UIView animateWithDuration:0.3 animations:^{
                    
                    self.textFldView.frame = frame;
                    
                } completion:^(BOOL finished) {
                    
                }];

            }
            else
            {
                CGRect frame = self.textFldView.frame;
                
                self.consTextFieldssTopSpace.constant = 2;//50;
                // frame.origin.y = -140;
                
                float x= self.consTextFieldssTopSpace.constant;
                frame.origin.y = x;
                
                [UIView animateWithDuration:0.3 animations:^{
                    
                    self.textFldView.frame = frame;
                    
                } completion:^(BOOL finished) {
                    
                }];
            }
    
        }
    if (txtActiveField == self.textFieldServices) {
        
        CGRect frame = self.textFldView.frame;
        
        if (IS_IPHONE_5) {
           self.consTextFieldssTopSpace.constant = -70;
        }
        if (IS_IPHONE_6) {
            self.consTextFieldssTopSpace.constant = -25;
        }
        if (IS_IPHONE_6P) {
            self.consTextFieldssTopSpace.constant = -25;
        }
        if(IS_IPHONE_4_OR_LESS)
        {
            self.textFieldServices.autocorrectionType = UITextAutocorrectionTypeNo;
            self.consTextFieldssTopSpace.constant = -75;
        }
        //self.consTextFieldssTopSpace.constant = -95;
        // frame.origin.y = -140;
        
        float x= self.consTextFieldssTopSpace.constant;
        frame.origin.y = x;
        
        [UIView animateWithDuration:0.3 animations:^{
            
            self.textFldView.frame = frame;
            
        } completion:^(BOOL finished) {
            
        }];
}
}

-(void)textFieldDidEndEditing:(UITextField *)textField{
    
   // [txtActiveField resignFirstResponder];
   [self doneTyping];
    
}


- (IBAction)uploadPicture:(id)sender {
    
    [txtActiveField resignFirstResponder];
    
    [self Actionperform];
    
}

-(void)Actionperform{
    
    UIActionSheet *popup = [[UIActionSheet alloc] initWithTitle:@"Select Option:" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:
                            @"Select Existing",
                            @"Take Photo", nil];
    popup.tag = 1;
    
    [popup showInView:[UIApplication sharedApplication].keyWindow];
    
}

- (void)actionSheet:(UIActionSheet *)popup didDismissWithButtonIndex:(NSInteger)buttonIndex {
    
    if (popup.tag == 1) {
        
        if (buttonIndex == 0) {
            
            if (IS_OS_8_OR_LATER) {
                
                PHAuthorizationStatus status = [PHPhotoLibrary authorizationStatus];
                
                if (status == PHAuthorizationStatusAuthorized) {
                    
                    // Access has been granted.
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        [self openPhotos :(int)buttonIndex];
                        
                    });
                    
                }else if (status == PHAuthorizationStatusDenied) {
                    
                    // Access has been denied.
                    [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
                        
                        if (status == PHAuthorizationStatusAuthorized) {
                            
                            // Access has been granted.
                            dispatch_async(dispatch_get_main_queue(), ^{
                                
                                [self openPhotos :(int)buttonIndex];
                                
                            });
                            
                            
                        }else {
                            
                            // Access has been denied.
                            dispatch_async(dispatch_get_main_queue(), ^{
                                
                                [self photosDenied];
                                
                            });
                        }
                    }];
                    
                }else if (status == PHAuthorizationStatusNotDetermined) {
                    
                    // Access has not been determined.
                    [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
                        
                        if (status == PHAuthorizationStatusAuthorized) {
                            
                            // Access has been granted.
                            dispatch_async(dispatch_get_main_queue(), ^{
                                
                                [self openPhotos :(int)buttonIndex];
                                
                            });
                            
                        }else {
                            // Access has been denied.
                            dispatch_async(dispatch_get_main_queue(), ^{
                                
                                [self photosDenied];
                                
                            });
                        }
                    }];
                    
                }else if (status == PHAuthorizationStatusRestricted) {
                    
                    // Restricted access - normally won't happen.
                    
                }
                
            }else{
                
                ALAuthorizationStatus statusAsset = [ALAssetsLibrary authorizationStatus];
                switch (statusAsset) {
                    case ALAuthorizationStatusNotDetermined: {
                        // not determined
                        
                        ALAssetsLibrary *lib = [[ALAssetsLibrary alloc] init];
                        [lib enumerateGroupsWithTypes:ALAssetsGroupSavedPhotos usingBlock:^(ALAssetsGroup *group, BOOL *stop) {
                            
                            // Access has been granted.
                            dispatch_async(dispatch_get_main_queue(), ^{
                                
                                [self openPhotos :(int)buttonIndex];
                                
                            });
                            
                        } failureBlock:^(NSError *error) {
                            if (error.code == ALAssetsLibraryAccessUserDeniedError) {
                                NSLog(@"user denied access, code: %zd", error.code);
                            } else {
                                NSLog(@"Other error code: %zd", error.code);
                            }
                            
                            // Access has been denied.
                            dispatch_async(dispatch_get_main_queue(), ^{
                                
                                [self photosDenied];
                                
                            });
                            
                        }];
                        
                        break;
                    }
                    case ALAuthorizationStatusRestricted: {
                        // restricted
                        break;
                    }
                    case ALAuthorizationStatusDenied: {
                        
                        // denied
                        
                        ALAssetsLibrary *lib = [[ALAssetsLibrary alloc] init];
                        [lib enumerateGroupsWithTypes:ALAssetsGroupSavedPhotos usingBlock:^(ALAssetsGroup *group, BOOL *stop) {
                            // Access has been granted.
                            dispatch_async(dispatch_get_main_queue(), ^{
                                
                                [self openPhotos :(int)buttonIndex];
                                
                            });
                            
                        } failureBlock:^(NSError *error) {
                            if (error.code == ALAssetsLibraryAccessUserDeniedError) {
                                NSLog(@"user denied access, code: %zd", error.code);
                            } else {
                                NSLog(@"Other error code: %zd", error.code);
                            }
                            
                            // Access has been denied.
                            dispatch_async(dispatch_get_main_queue(), ^{
                                
                                [self photosDenied];
                                
                            });
                            
                        }];
                        
                        break;
                    }
                    case ALAuthorizationStatusAuthorized: {
                        // authorized
                        
                        // Access has been granted.
                        dispatch_async(dispatch_get_main_queue(), ^{
                            
                            [self openPhotos :(int)buttonIndex];
                            
                        });
                        
                        break;
                    }
                    default: {
                        break;
                    }
                }
                
            }
            
            
        }else if(buttonIndex == 1){
            
            // For check camera availability
            if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
                
                AVAuthorizationStatus status = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
                
                if(status == AVAuthorizationStatusAuthorized) { // authorized
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        [self openPhotos:(int)buttonIndex];
                        
                    });
                }
                else if(status == AVAuthorizationStatusDenied){ // denied
                    
                    [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
                        
                        if(granted){ // Access has been granted ..do something
                            
                            dispatch_async(dispatch_get_main_queue(), ^{
                                
                                [self openPhotos:(int)buttonIndex];
                                
                            });
                            
                            
                        } else { // Access denied ..do something
                            
                            dispatch_async(dispatch_get_main_queue(), ^{
                                
                                [self camDenied];
                                
                            });
                            
                        }
                    }];
                    
                }
                else if(status == AVAuthorizationStatusRestricted){ // restricted
                    
                    [[GlobalFunction shared] showAlertForMessage:@"Camera uses in application is restricted, please check and try again."];
                    
                }
                else if(status == AVAuthorizationStatusNotDetermined){ // not determined
                    
                    [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
                        
                        if(granted){ // Access has been granted ..do something
                            
                            dispatch_async(dispatch_get_main_queue(), ^{
                                
                                [self openPhotos:(int)buttonIndex];
                                
                            });
                            
                        } else { // Access denied ..do something
                            
                            dispatch_async(dispatch_get_main_queue(), ^{
                                
                                [self camDenied];
                                
                            });
                            
                        }
                    }];
                }
                
            }else{
                
                [[GlobalFunction shared] showAlertForMessage:@"There is no camera available on device."];
                
            }
            
        }
        
    }
    
}

-(void)photosDenied{
    
    NSString *alertText;
    NSString *alertButton;
    
    alertText = @"BUY TIMEE wants access to photos for getting images to provide better user client interaction.";
    
    alertButton = @"Go";
    
    UIAlertView *alert = [[UIAlertView alloc]
                          initWithTitle:@"BUY TIMEE"
                          message:alertText
                          delegate:self
                          cancelButtonTitle:alertButton
                          otherButtonTitles:nil];
    alert.tag = 3491832;
    [alert show];
    
}

- (void)camDenied{
    
    NSString *alertText;
    NSString *alertButton;
    
    alertText = @"BUY TIMEE wants access to camera for getting images to provide better user client interaction.";
    
    alertButton = @"Go";
    
    UIAlertView *alert = [[UIAlertView alloc]
                          initWithTitle:@"BuyTimee"
                          message:alertText
                          delegate:self
                          cancelButtonTitle:alertButton
                          otherButtonTitles:nil];
    
    alert.tag = 3491832;
    [alert show];
}


-(void)openPhotos :(int)index{
    
    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
    
    if (index==0) {
        imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    }
    else{
        imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
        
        if([UIImagePickerController isCameraDeviceAvailable: UIImagePickerControllerCameraDeviceFront]){
            // do something
            imagePickerController.cameraDevice = UIImagePickerControllerCameraDeviceFront;
        }
        
    }
    
    imagePickerController.delegate = (id)self;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        //Code that presents or dismisses a view controller here
        [myAppDelegate.window.rootViewController presentViewController:imagePickerController animated:YES completion:nil];
        
    });
    
}


// This method is called when an image has been chosen from the library or taken from the camera.
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    
    _imagePickFromAlbum=YES;
    
    //You can retrieve the actual UIImage
    UIImage *image = [info valueForKey:UIImagePickerControllerOriginalImage];
    UIImage *flippedImage;
    if (picker.sourceType == UIImagePickerControllerSourceTypeCamera) {
        
        if (picker.cameraDevice == UIImagePickerControllerCameraDeviceFront) {
            
            flippedImage = [UIImage imageWithCGImage:image.CGImage scale:image.scale orientation:UIImageOrientationLeftMirrored];
            
        }else{
            
            flippedImage = image;
            
        }
        
    }else{
        
        flippedImage = image;
        
    }
    NSLog(@"%f",_imgViewUsersPicture.frame.size.height);
    NSLog(@"%f",_imgViewUsersPicture.frame.size.width);

    
    self.imgViewUsersPicture.image = flippedImage;
    
    self.imgViewUsersPicture.layer.cornerRadius = self.imgViewUsersPicture.frame.size.height/2;
    self.imgViewUsersPicture.layer.masksToBounds = YES;
    NSLog(@"%f",_imgViewUsersPicture.frame.size.height);
    NSLog(@"%f",_imgViewUsersPicture.frame.size.width);

    self.imgViewUsersPicture.hidden = NO;
    
    isPictureAvailabe = YES;
    
    [picker dismissViewControllerAnimated:YES completion:nil];
    
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    
    [picker dismissViewControllerAnimated:YES completion:nil];
    
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    [GlobalFunction addIndicatorView];
    
    if ([segue.identifier isEqualToString:@"BankInfo"]) {
        
        //        myAppDelegate.vcObj.btnSideBarOpen.hidden = YES;
        //        myAppDelegate.vcObj.btnBack.hidden = NO;
        //
        //        if (myAppDelegate.isFromProfile) {
        //            myAppDelegate.stringNameOfChildVC = @"ViewController";
        //        }else{
        //            myAppDelegate.stringNameOfChildVC = [NSString stringWithFormat:@"%@",[self class]];
        //        }
        //
        
        
        myAppDelegate.superViewClassName = @"";
        
        myAppDelegate.bankInfObj = nil;
        
        myAppDelegate.bankInfObj = (BankInfoViewController *)segue.destinationViewController;
        
        
    }
}
-(void)saveProfile:(id)sender
{
    
    
    if ([[self.textFieldUsername.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""]) {
        
        [[GlobalFunction shared] showAlertForMessage:@"Business name required."];
        
        return;
        
    }
    
    if (!selectedCategoryObj) {
        
        [[GlobalFunction shared] showAlertForMessage:@"Please select a category to continue."];
        return;
        
    }
    
    if ([[self.textFieldAdd.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""]) {
        
        [[GlobalFunction shared] showAlertForMessage:@"Address can't be blank."];
        
        return;
        
    }
    
    if ([[self.textFieldServices.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""]) {
        
        [[GlobalFunction shared] showAlertForMessage:@"Services can't be blank."];
        
        return;
        
    }
    
    
    
    
    
    
    [GlobalFunction addIndicatorView];
    
    [self performSelector:@selector(updateBioToServer1) withObject:nil afterDelay:0.5];
}

-(void)updateBioToServer1{
    
    if (![[WebService shared] connected]) {
        
        [GlobalFunction removeIndicatorView];
        
        [[GlobalFunction shared] showAlertForMessage:@"Can't connect to internet, please check and try again."];
        
        return;
    }
    
  //  CLLocationCoordinate2D coordinate = [[GlobalFunction shared] getLocationFromAddressString:self.textFieldEmail.text];
    
    NSString *imageName1 = [NSString stringWithFormat:@"userProfilePicture_%@_%@.jpeg",myAppDelegate.userData.userid,TimeStamp];
    
  // imageName1 = @"main11.jpeg";
    
   NSDictionary *dictBioResponse;
    
    if (isPictureAvailabe) {
        
        
        dictBioResponse = [[WebService shared]updateVendorProfileuserId:myAppDelegate.userData.userid name:self.textFieldUsername.text andUserImage:self.imgViewUsersPicture.image andImageName:imageName1 instaLink:self.textFieldInsta.text yelpProfile:self.textFieldYelp.text address:self.textFieldAdd.text services:self.textFieldServices.text categoryId:selectedCategoryObj.categoryId];
        

    }else if(isOldPictureAvailable){
        
        
                dictBioResponse = [[WebService shared]updateVendorProfileuserId:myAppDelegate.userData.userid name:self.textFieldUsername.text andUserImage:myAppDelegate.userData.userImage andImageName:myAppDelegate.userData.userImageName instaLink:self.textFieldInsta.text yelpProfile:self.textFieldYelp.text address:self.textFieldAdd.text services:self.textFieldServices.text categoryId:selectedCategoryObj.categoryId];
        
    }else{
        
        
        dictBioResponse = [[WebService shared]updateVendorProfileuserId:myAppDelegate.userData.userid name:self.textFieldUsername.text andUserImage:nil andImageName:@"" instaLink:self.textFieldInsta.text yelpProfile:self.textFieldYelp.text address:self.textFieldAdd.text services:self.textFieldServices.text categoryId:selectedCategoryObj.categoryId];

        
    }
    
    if (dictBioResponse) {
        
      //  NSLog(@"response --- %@",[EncryptDecryptFile decrypt:[dictBioResponse valueForKey:@"responseMessage"]]);
        
      //  NSLog(@"response --- %@",[EncryptDecryptFile decrypt:[dictBioResponse valueForKey:@"responseCode"]]);
        
        if ([[EncryptDecryptFile decrypt:[dictBioResponse valueForKey:@"responseCode"]] isEqualToString:@"200"]) {
            
            NSArray *dictDataReg = [dictBioResponse objectForKey:@"data"];
            
            NSDictionary *dictFromResponse = [dictBioResponse objectForKey:@"data"];
             NSLog(@"response --- %@",[EncryptDecryptFile decrypt:[dictDataReg valueForKey:@"picPath"]]);
            
            if (dictDataReg) {
                
                
                NSMutableDictionary *dictTemp0 = [[NSMutableDictionary alloc]init];
                
                [dictTemp0 setValue:[dictDataReg valueForKey:@"businessId"] forKey:@"businessId"];
                [dictTemp0 setValue:[dictDataReg valueForKey:@"name"] forKey:@"name"];
                [dictTemp0 setValue:[dictDataReg valueForKey:@"address"] forKey:@"address"];
                

                
                
                if ([[GlobalFunction shared] callUpdateWebservice]) {
                    
                    
                    
                    NSLog(@"%lu",(unsigned long)myAppDelegate.arrayBusinessDetail.count);
                    
                    
                    NSDictionary *businessId = [dictBioResponse valueForKey:@"data"];
                    
                    
                    
                    myAppDelegate.userData.BusinessUserId = [EncryptDecryptFile decrypt:[businessId valueForKey:@"businessId"]];
                    
                    
                    
                    NSLog(@"businessId id === %@",myAppDelegate.userData.BusinessUserId);
                    NSLog(@"businessId id === %@",[EncryptDecryptFile decrypt:[businessId valueForKey:@"userId"]]);
                    
                    
                  //    [DatabaseClass updateBusinessIdInDataBase:[EncryptDecryptFile EncryptIT:myAppDelegate.userData.BusinessUserId] forUserId:[businessId valueForKey:@"userId"]];
                    
                    [DatabaseClass updateBusinessIdInDataBase:[EncryptDecryptFile EncryptIT:@"1"] forUserId:[EncryptDecryptFile EncryptIT:myAppDelegate.userData.BusinessUserId]];
                    
                    
              //      myAppDelegate.arrayBusinessDetail = [DatabaseClass getDataFromBusinessData:[[NSString stringWithFormat:@"Select * from %@",tableBusinessInfo] UTF8String]];
                    
                    
                    //    NSMutableArray *arr = [DatabaseClass getDataFromUserData:[[NSString stringWithFormat:@"Select * from %@",tableUser] UTF8String]];
                    //
                    //    myAppDelegate.userData = (user *)[arr firstObject];
                    
                    
                    NSLog(@"%lu",(unsigned long)myAppDelegate.arrayBusinessDetail.count);
 
                }
                else
                {
                    
                    [GlobalFunction removeIndicatorView];
                    
                    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"BUY TIMEE" message:@"Failed to update, please try again." delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Retry", nil];
                    
                    alertView.tag = 999;
                    
                    [alertView show];
                    
                }

//                
//                NSString *query;
//                
//                if(dictTemp0.count>0)
//                {
////                    
////       query = nil;
////       query = [NSString stringWithFormat:@"delete from %@ where businessId = '%@'",tableBusinessInfo,[dictTemp0 valueForKey:@"businessId"]];
////                    
////                 //   NSString *query = [NSString stringWithFormat:@"delete from %@",tableBusinessInfo];
////                    
////                    [DatabaseClass executeQuery:query];
////                    
////                    
////                    query = nil;
////                    query=[NSString stringWithFormat:@"insert into %@ values(?,?,?)",tableBusinessInfo];
////                    [DatabaseClass saveBusinessDataFromVendor:[query UTF8String] fromDict:dictTemp0];
////                    myAppDelegate.arrayBusinessDetail = [DatabaseClass getDataFromBusinessData:[[NSString stringWithFormat:@"Select * from %@",tableBusinessInfo] UTF8String]];
//                    
//    
//                    NSLog(@"%lu",(unsigned long)myAppDelegate.arrayBusinessDetail.count);
//                    
//    
//                    NSDictionary *businessId = [dictBioResponse valueForKey:@"data"];
//                    
//                    
//                    
//                    myAppDelegate.userData.BusinessUserId = [EncryptDecryptFile decrypt:[businessId valueForKey:@"businessId"]];
//                    
//                    
//                    
//                    NSLog(@"businessId id === %@",myAppDelegate.userData.BusinessUserId);
//                    
//                    
//                    
//                    
//                    [DatabaseClass updateBusinessIdInDataBase:[EncryptDecryptFile EncryptIT:@"1"] forUserId:[EncryptDecryptFile EncryptIT:myAppDelegate.userData.BusinessUserId]];
//                    
//                    myAppDelegate.arrayBusinessDetail = [DatabaseClass getDataFromBusinessData:[[NSString stringWithFormat:@"Select * from %@",tableBusinessInfo] UTF8String]];
//
//                    NSLog(@"%lu",(unsigned long)myAppDelegate.arrayBusinessDetail.count);
//                    
//    
//                }
//                

                if (isPictureAvailabe) {
                    
                    [GlobalFunction removeImage:imageName1 dirname:@"vender"];
                    
                   // myAppDelegate.userData.userImageName = imageName1;
                    
                    NSArray *components = [imageName1 componentsSeparatedByString:@"."];
                    
                    imageName1 = [components firstObject];
                    
                    [GlobalFunction saveimage:self.imgViewUsersPicture.image imageName:imageName1 dirname:@"vender"];
                    isPictureAvailabe = NO;
                    
                }
                
                
          //      [GlobalFunction saveimage:self.imgViewUsersPicture.image imageName:imageName1 dirname:@"vender"];
                
                
                [self bioUpdatedSuccefully:[EncryptDecryptFile decrypt:[dictBioResponse valueForKey:@"responseMessage"]]];
                
            }else{
                
                [[GlobalFunction shared] showAlertForMessage:[EncryptDecryptFile decrypt:[dictBioResponse valueForKey:@"responseMessage"]]];
                
                [GlobalFunction removeIndicatorView];
                
            }
            
            
        }else{
            
            [[GlobalFunction shared] showAlertForMessage:[EncryptDecryptFile decrypt:[dictBioResponse valueForKey:@"responseMessage"]]];
            
            [GlobalFunction removeIndicatorView];
            
        }
        
    }else{
        
        [[GlobalFunction shared] showAlertForMessage:@"Server is down, please try again later."];
        
        [GlobalFunction removeIndicatorView];
        
    }
    
    
}

-(void)performUpdate
{
        
        if (![self checkForInternet]) {
            
            // [self performSelector:@selector(reloadTables) withObject:nil afterDelay:0.01];
            
            return;
        }
        
        if ([[GlobalFunction shared] callUpdateWebservice]) {
            
            
        }else{
            
            [GlobalFunction removeIndicatorView];
            
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Buy Timee" message:@"Failed to update, please try again." delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Retry", nil];
            
            alertView.tag = 999;
            
            [alertView show];
            
        }
}

-(BOOL)checkForInternet{
    
    BOOL isConnected = true;
    
    if (![[WebService shared] connected]) {
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"BUY TIMEE" message:@"Can't connect to internet, update not successful. Please check your internet connection then try again." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        [alertView show];
        
        isConnected = false;
        
    }
    
    return isConnected;
}

-(void)bioUpdatedSuccefully:(NSString *)response{
    
    [GlobalFunction removeIndicatorView];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"BUY TIMEE" message:response delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    
    alert.tag = 10001;
    
    [alert show];
    
}

-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex{
    
    if (alertView.tag == 10001) {
        
        if(buttonIndex == [alertView cancelButtonIndex]){
            
            
            [self cancelBtnPressed:nil];
            
           //[self goBack];
            
        }
    }else if (alertView.tag == 3491832){
        
        NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
        [[UIApplication sharedApplication] openURL:url];
        
    }else if (alertView.tag == 12001){
        
        if (buttonIndex == [alertView cancelButtonIndex]) {
            
        }else{
            
            [GlobalFunction addIndicatorView];
            
        //    [self performSelector:@selector(disconnectStripeAccount) withObject:nil afterDelay:0.5];
            
        }
        
    }
    else if (alertView.tag == 999) {
        
        if (buttonIndex == [alertView cancelButtonIndex]) {
            
            //  [self performSelector:@selector(reloadTable) withObject:nil afterDelay:0.2];
            
        }else{
            
            [GlobalFunction addIndicatorView];
            
            [self performSelector:@selector(updateWebServiceHandling) withObject:nil afterDelay:0.2];
            
        }
        
    }
}

-(void)updateWebServiceHandling{
    
    if (![self checkForInternet]) {
        
       
        
        return;
    }
    
    if ([[GlobalFunction shared] callUpdateWebservice]) {
        
       
        
    }else{
        
        [GlobalFunction removeIndicatorView];
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"BUY TIMEE" message:@"Failed to update, please try again." delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Retry", nil];
        
        alertView.tag = 999;
        
        [alertView show];
        
    }
    
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)cancelBtnPressed:(id)sender {
    
    [myAppDelegate.vcObj backToMainView:_cancelPressed];
    
}
- (IBAction)locationHelpPressed:(id)sender
{
     [txtActiveField resignFirstResponder];
    
    
    
    [UIView transitionWithView:self.locationHelpButton
                      duration:0.4
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^{
                        self.blackTransparentView.hidden = false;
                        self.blackTransparentView.alpha = 1.0;

                    }
                    completion:NULL];

    
    
}
- (IBAction)OkBtnPressed:(id)sender
{
    
    [UIView animateWithDuration:0.3 animations:^{
 self.blackTransparentView.alpha = 0;
    } completion: ^(BOOL finished) {//creates a variable (BOOL) called "finished" that is set to *YES* when animation IS completed.
         self.blackTransparentView.hidden = true;//if animation is finished ("finished" == *YES*), then hidden = "finished" ... (aka hidden = *YES*)
    }];

    
//    
//    self.blackTransparentView.hidden = true;
//    self.blackTransparentView.alpha = 0;
    
}

//********************Rohitash Prajapati*******************//

#pragma mark - TableView Delegate and Datasource


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return catAry.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 40;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *MyIdentifier = @"CategoryCell";
    
    SelectCategoryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    
    
    
    if (cell == nil){
        cell = [[SelectCategoryTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                                  reuseIdentifier:MyIdentifier];
    }

        category *catObj = [catAry objectAtIndex:indexPath.row];
        
        
        NSLog(@"%@",catAry);
        
        
        cell.cellLbl.text = catObj.name;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    selectedCategoryObj = [catAry objectAtIndex:indexPath.row];
    NSMutableAttributedString *dateString1 = [[NSMutableAttributedString alloc] initWithString:selectedCategoryObj.name];

  //  [_chooseBtn setTitle:selectedCategoryObj.name forState:UIControlStateNormal];
     [_chooseBtn setAttributedTitle:dateString1 forState:UIControlStateNormal];
    [self closeTable];
    
}


-(void)openTable{
    
    [self.tbleView layoutIfNeeded];
//    [self.tbleView reloadData];
    [self.tbleView layoutIfNeeded];
    
    //   [txtActiveField resignFirstResponder];
    
//    self.viewTableTapHandler.hidden = false;
      _tbleView.hidden = NO;
    [self.view layoutIfNeeded];
    
        NSInteger count = 0;
        if (catAry.count>5) {
            count = 5;
        }else{
            count = catAry.count;
        }


    if(IS_IPHONE_4_OR_LESS)
    {
        //[self.tbleView layoutIfNeeded];
        [self.tbleView reloadData];

        self.tbleViewHeight.constant = 28*count;

    }

    else
    {
       //[self.tbleView layoutIfNeeded];
        [self.tbleView reloadData];

        self.tbleViewHeight.constant = 40*count;

    }
    
    
    
  
    
    [UIView animateWithDuration:0.5f
                          delay:0.0f
                        options:UIViewAnimationOptionTransitionFlipFromTop | UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         [self.view layoutIfNeeded];
                         
                         isOpened = true;
                         
                     }completion:NULL];
    
}

-(void)closeTable{
    
    [self.view layoutIfNeeded];
    
    self.tbleViewHeight.constant = 0;
    
    [UIView animateWithDuration:0.5 delay:0.0 options:UIViewAnimationOptionTransitionFlipFromBottom | UIViewAnimationOptionCurveEaseInOut animations:^{
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished) {
        _tbleView.hidden = YES;
        isOpened = false;
    }];
    
}






- (IBAction)chooseCatBtnClicked:(id)sender
{
    [self.view endEditing:YES];
    if(isOpened)
    {
        [self closeTable];
    }
    else
    {
        [self openTable];
    }
}
- (IBAction)transparentBtnClicked:(id)sender {
}
@end
