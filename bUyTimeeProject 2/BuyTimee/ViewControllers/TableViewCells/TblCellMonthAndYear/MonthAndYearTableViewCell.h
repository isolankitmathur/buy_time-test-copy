//
//  MonthAndYearTableViewCell.h
//  BuyTimee
//
//  Created by User14 on 07/03/16.

//

#import <UIKit/UIKit.h>

@interface MonthAndYearTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblMonthAndYear;

@end
