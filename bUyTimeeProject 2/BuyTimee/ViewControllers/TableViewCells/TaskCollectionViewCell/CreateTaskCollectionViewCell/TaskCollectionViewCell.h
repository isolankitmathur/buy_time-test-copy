//
//  TaskCollectionViewCell.h
//  BuyTimee
//
//  Created by User14 on 01/04/16.
//  Copyright © 2016 User14. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TaskCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imgViewTaskImage;
@property (weak, nonatomic) IBOutlet UIButton *btnDeleteImage;

@end
