//
//  TaskDetailTableViewCell.m
//  BuyTimee
//
//  Created by User14 on 03/03/16.

//

#import "TaskDetailTableViewCell.h"

@implementation TaskDetailTableViewCell

- (void)awakeFromNib {
    
    self.imgViewUserProfile.layer.cornerRadius = self.imgViewUserProfile.frame.size.width/2;
    self.imgViewUserProfile.layer.masksToBounds = YES;
    self.imgViewUserProfile.layer.shouldRasterize = YES;
    self.imgViewUserProfile.contentMode = UIViewContentModeScaleAspectFill;
    // Initialization code
    self.imgViewUserProfile.userInteractionEnabled = YES;
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
