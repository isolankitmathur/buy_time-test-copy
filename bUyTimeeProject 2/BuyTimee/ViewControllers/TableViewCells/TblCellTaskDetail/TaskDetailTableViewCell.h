//
//  TaskDetailTableViewCell.h
//  BuyTimee
//
//  Created by User14 on 03/03/16.

//

#import <UIKit/UIKit.h>

@interface TaskDetailTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imgViewUserProfile;
@property (weak, nonatomic) IBOutlet UILabel *lblTaskName;
@property (weak, nonatomic) IBOutlet UILabel *lblTaskPrice;
@property (weak, nonatomic) IBOutlet UILabel *lblTaskDate;
@property (weak, nonatomic) IBOutlet UILabel *lblAddress;
@property (weak, nonatomic) IBOutlet UILabel *lblTaskEmployerName;
@property (weak, nonatomic) IBOutlet UILabel *lblAt;

@property (weak, nonatomic) IBOutlet UIImageView *imgViewRightArrow;


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintLblDateWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintLblPriceWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintLblDateTrailingSpace;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintLblTaskNameWidth;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintLblUserNameTrailingSpace;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintLblUserNameWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintLblAddressWidth;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consTaskNameWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consLblAddressWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consLblDateWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consLblTaskNameTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consImageViewRightArrowTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consCellImageViewWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consCellImageViewHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consLabelTaskPriceBottom;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consImageViewTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consLblTaskNmaeLeft;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consLblTaskDateLeft;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consImageviewLeft;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consLblAtLeft;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consTaskPriceRight;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consLblAtTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consLblDataTop;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consLblAddressTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consImgRightArrowWIdth;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consImgRightArrowHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consAddressLblLeft;
@property (weak, nonatomic) IBOutlet UILabel *labelUnderlineCell;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consLblTaskPriceWidth;

@property (weak, nonatomic) IBOutlet UILabel *lblFeePrice;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consLblFeePriceTrailing;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consLblFeePriceBottom;

@end
