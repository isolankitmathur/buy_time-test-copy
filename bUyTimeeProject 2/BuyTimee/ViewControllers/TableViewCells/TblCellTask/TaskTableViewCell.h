//
//  TaskTableViewCell.h
//  BuyTimee
//
//  Created by User14 on 29/02/16.

//

#import <UIKit/UIKit.h>

@interface TaskTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imgViewIcon;
@property (weak, nonatomic) IBOutlet UILabel *lblList;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cellTaskImgViewIconLeft;

@end
