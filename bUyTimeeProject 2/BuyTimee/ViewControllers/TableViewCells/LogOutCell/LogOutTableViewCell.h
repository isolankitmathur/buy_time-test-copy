//
//  LogOutTableViewCell.h
//  BuyTimee
//
//  Created by User38 on 14/06/17.
//  Copyright © 2017 User14. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LogOutTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imgViewIcon1;

@property (weak, nonatomic) IBOutlet UILabel *lblList1;

@end
