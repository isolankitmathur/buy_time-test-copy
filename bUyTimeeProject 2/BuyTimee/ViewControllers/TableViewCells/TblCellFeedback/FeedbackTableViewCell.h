//
//  FeedbackTableViewCell.h
//  BuyTimee
//
//  Created by User14 on 14/03/16.
//  Copyright © 2016 User14. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FeedbackTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblFeedbackUsername;
@property (weak, nonatomic) IBOutlet UILabel *lblFeedbackDescription;

@end
