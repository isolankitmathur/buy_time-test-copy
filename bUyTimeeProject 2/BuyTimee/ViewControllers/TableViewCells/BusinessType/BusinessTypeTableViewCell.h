//
//  BusinessTypeTableViewCell.h
//  BuyTimee
//
//  Created by User38 on 20/06/17.
//  Copyright © 2017 User14. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BusinessTypeTableViewCell : UITableViewCell


@property (weak, nonatomic) IBOutlet UILabel *lblBusinessType;


@end
