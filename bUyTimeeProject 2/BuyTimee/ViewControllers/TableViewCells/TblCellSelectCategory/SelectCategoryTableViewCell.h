//
//  SelectCategoryTableViewCell.h
//  BuyTimee
//
//  Created by User14 on 08/03/16.

//

#import <UIKit/UIKit.h>

@interface SelectCategoryTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *cellLbl;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cellLabelHeight;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cellLabelWidth;
@end
