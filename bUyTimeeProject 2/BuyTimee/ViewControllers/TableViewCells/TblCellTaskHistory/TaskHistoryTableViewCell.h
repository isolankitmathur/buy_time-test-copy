//
//  TaskHistoryTableViewCell.h
//  BuyTimee
//
//  Created by User14 on 29/02/16.

//

#import <UIKit/UIKit.h>

@interface TaskHistoryTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imgViewIcon;
@property (weak, nonatomic) IBOutlet UILabel *lblList;
@property (weak, nonatomic) IBOutlet UIView *BgRedView;
@property (weak, nonatomic) IBOutlet UILabel *lblReviewNo;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consBgRedViewTop;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consBgRedViewHeight;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consBgRedViewWidth;

@end
