//
//  CategoryTableViewCell.h
//  BuyTimee
//
//  Created by User14 on 29/02/16.

//

#import <UIKit/UIKit.h>

@interface CategoryTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblCategory;
@property (weak, nonatomic) IBOutlet UIImageView *imgViewCheckMark;

@end
