//
//  BioInfoTableViewCell.h
//  BuyTimee
//
//  Created by User14 on 29/02/16.

//

#import <UIKit/UIKit.h>

@interface BioInfoTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imgViewUserImage;
@property (weak, nonatomic) IBOutlet UILabel *lblUserName;
@property (weak, nonatomic) IBOutlet UILabel *lblUserAddress;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cellBioImgViewTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cellBioImgViewHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cellBioImgViewWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cellBioLblUserNameTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cellBioLblUserNameLeft;

@end
