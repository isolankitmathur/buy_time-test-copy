//
//  RatingViewController.m
//  HandApp
//
//  Created by  ~ on 04/03/16.
//  Copyright (c) 2016 . All rights reserved.
//

#import "RatingViewController.h"
#import "GlobalFunction.h"
#import "Constants.h"
#import "TaskTableViewCell.h"
#import "subCategory.h"
#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
@interface RatingViewController ()
{
    BOOL settingText;

}
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *btnStarCollection;

@property (weak, nonatomic) IBOutlet UIButton *btnAddComment;
@property (weak, nonatomic) IBOutlet UIImageView *imgViewUser;

@property (weak, nonatomic) IBOutlet UILabel *lblCenter;
- (IBAction)animateImageViewToFullScreen:(id)sender;

@end

@implementation RatingViewController



- (void)viewDidLoad {
    
    [super viewDidLoad];
    [self addInputViewToTextField:self.reviewTextView];

    

    [[self.reviewTextView layer] setBorderColor:[[UIColor colorWithRed:196.0/255.0f green:196.0/255.0f blue:196.0/255.0f alpha:1.0] CGColor]];
    
    
    
    
    [[self.reviewTextView layer] setBorderWidth:0.3];
    [[self.reviewTextView layer] setCornerRadius:2];
    
    
    
    [self.reviewTextView setDelegate:self];
   
    
    myAppDelegate.TandC_Buyee=@"home"; //by vs for maintain filter after back btn on details and set string name here for go back on home screen.
   // myAppDelegate.vcObj.lblTitleText.text = @"B U Y  T I M E E";
    
    
    _reviewTextView.textColor = [UIColor colorWithRed:196.0/255.0f green:196.0/255.0f blue:196.0/255.0f alpha:1.0];
    ;
    
//    NSString *mainStr = @"Write review*";
//
//    NSMutableAttributedString * string = [[NSMutableAttributedString alloc]initWithString:@"Write review*"];
//
//    NSArray *words=[mainStr componentsSeparatedByString:@" "];
//
//    for (NSString *word in words) {
//        if ([word isEqualToString:@"*"]) {
//            NSRange range=[mainStr rangeOfString:word];
//            [string addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:range];
//        }
//    }
//   // [self.lblThanks setAttributedText:string];
//
//
//    [self.reviewTextView setAttributedText:string];
   // _reviewTextView.attributedText = string;

    
    
    
    NSAttributedString *star = [[NSAttributedString alloc] initWithString:@"*" attributes:@{ NSForegroundColorAttributeName :[UIColor redColor],
                                                                                             }];
    NSMutableAttributedString *strQuantityTitle = [[NSMutableAttributedString alloc] initWithString:@"Write review" attributes:@{ NSForegroundColorAttributeName : [UIColor colorWithRed:143.0/255.0f green:143.0/255.0f blue:143.0/255.0f alpha:1.0],
                                                                                                                              }];
    [strQuantityTitle appendAttributedString:star];
    [self.reviewTextView setAttributedText:strQuantityTitle];
    
    if(IS_IPHONE_6)
    {
        
        [self.reviewTextView setTextContainerInset:UIEdgeInsetsMake(13,6, 0, 0)];    //top,left,bottom,right
        [self.btnAddComment setTitleEdgeInsets:UIEdgeInsetsMake(0, 2, 0, 0)];
    }
    
    else if (IS_IPHONE_6P)
    {
        [self.reviewTextView setTextContainerInset:UIEdgeInsetsMake(13,5, 0, 0)];    //top,left,bottom,right
        [self.btnAddComment setTitleEdgeInsets:UIEdgeInsetsMake(-1, 2, 0, 0)];
        
    }
    
    else if (IS_IPHONE5)
    {
        [self.reviewTextView setTextContainerInset:UIEdgeInsetsMake(12,4, 0, 0)];    //top,left,bottom,right
        [self.btnAddComment setTitleEdgeInsets:UIEdgeInsetsMake(-2, 1, 0, 0)];

        
    }

    
    
    /*
    
    if (myAppDelegate.isComingFromPushNotification) {
        
        self.imgViewUser.image = myAppDelegate.workerPushTaskDetail.userImage_acceptedBy;
        self.lblUserAddress.text = myAppDelegate.workerPushTaskDetail.locationName_acceptedBy;
        
        for (int i = 0; i<myAppDelegate.arrayCategoryDetail.count; i++) {
            
            if ([[myAppDelegate.arrayCategoryDetail objectAtIndex:i] isKindOfClass:[subCategory class]]){
                
                subCategory *obj = [myAppDelegate.arrayCategoryDetail objectAtIndex:i];
                
                if ([obj.subCategoryId isEqualToString:myAppDelegate.workerPushTaskDetail.taskDetailObj.subcategoryId]) {
                    
                    self.lblUserLastAddress.text = obj.name;
                    
                }
                
            }
            
        }
        
        self.nameLbl.text = myAppDelegate.workerPushTaskDetail.userName_acceptedBy;
        
        if ([self.lblUserAddress.text isEqualToString:@""] && [self.lblUserLastAddress.text isEqualToString:@""]) {
            self.lblCenter.hidden = YES;
        }

    }else{
        
        self.imgViewUser.image = myAppDelegate.workerTaskDetail.userImage_acceptedBy;
        self.lblUserAddress.text = myAppDelegate.workerTaskDetail.locationName_acceptedBy;
        
        for (int i = 0; i<myAppDelegate.arrayCategoryDetail.count; i++) {
            
            if ([[myAppDelegate.arrayCategoryDetail objectAtIndex:i] isKindOfClass:[subCategory class]]){
                
                subCategory *obj = [myAppDelegate.arrayCategoryDetail objectAtIndex:i];
                
                if ([obj.subCategoryId isEqualToString:myAppDelegate.workerTaskDetail.taskDetailObj.subcategoryId]) {
                    
                    self.lblUserLastAddress.text = obj.name;
                    
                }
                
            }
            
        }
        
        self.nameLbl.text = myAppDelegate.workerTaskDetail.userName_acceptedBy;
        
        if ([self.lblUserAddress.text isEqualToString:@""] && [self.lblUserLastAddress.text isEqualToString:@""]) {
            self.lblCenter.hidden = YES;
        }

    }
    
    */
    
}

- (void)addInputViewToTextField:(UITextView *)textField{
    
    [self createInputAccessoryView];
    
    [textField setInputAccessoryView:_inputAccView];
    
}



-(void)createInputAccessoryView{
    // Create the view that will play the part of the input accessory view.
    // Note that the frame width (third value in the CGRectMake method)
    // should change accordingly in landscape orientation. But we don’t care
    // about that now.
    _inputAccView = [[UIView alloc] initWithFrame:CGRectMake(10.0, 0.0, self.view.frame.size.width, 40.0)];
    // Set the view’s background color. We’ ll set it here to gray. Use any color you want.
    [_inputAccView setBackgroundColor:[UIColor lightGrayColor]];
    // We can play a little with transparency as well using the Alpha property. Normally
    // you can leave it unchanged.
    [_inputAccView setAlpha: 0.8];
    // If you want you may set or change more properties (ex. Font, background image,e.t.c.).
    // For now, what we’ ve already done is just enough.
    // Let’s create our buttons now. First the previous button.
    // (as well as the methods for the rest of our buttons) later.
    _btnDone = [UIButton buttonWithType:UIButtonTypeCustom];
    [_btnDone setFrame:CGRectMake(self.view.frame.size.width - 80.0f, 0.0f, 80.0f, 40.0f)];
    [_btnDone setTitle:@"Done" forState:UIControlStateNormal];
    [_btnDone setBackgroundColor:[UIColor clearColor]];
    [_btnDone setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [_btnDone addTarget:self action:@selector(doneTyping) forControlEvents:UIControlEventTouchUpInside];
    // Now that our buttons are ready we just have to add them to our view.
    [_inputAccView addSubview:_btnDone];
}

-(void)doneTyping
    {
        [UIView animateWithDuration:0.5
                              delay:0.0
                            options:UIViewAnimationOptionCurveEaseOut
                         animations:^{
                             
                             [_reviewTextView resignFirstResponder];
                             
                         } completion:^(BOOL finished) {
                             
                             
                         }];
        
 
    }


-(void)viewDidAppear:(BOOL)animated{
    
    
    NSString* final = @"Reservation Rate page";
    
    [Flurry logEvent:final];
    
    
    self.headingLabel.text = [myAppDelegate.workerTaskDetail_taskDetailObj_taskName uppercaseString];
    self.nameLbl.text = myAppDelegate.workerTaskDetail_name_createdBy;
    
    _btnAddComment.backgroundColor=[UIColor colorWithRed:63.0/255.0f green:67.0/255.0f blue:70.0/255.0f alpha:1.0];
    
    _headingLabel.textColor=[UIColor colorWithRed:0.0/255.0f green:0.0/255.0f blue:0.0/255.0f alpha:1.0];
    _nameLbl.textColor=[UIColor colorWithRed:0.0/255.0f green:0.0/255.0f blue:0.0/255.0f alpha:1.0];
    _lineLbl.backgroundColor=[UIColor colorWithRed:77.0/255.0f green:96.0/255.0f blue:111.0/255.0f alpha:1.0];
    _addYourRatingLabel.textColor=[UIColor colorWithRed:0.0/255.0f green:0.0/255.0f blue:0.0/255.0f alpha:1.0];


    NSString *fullPath=myAppDelegate.workerAllTaskDetail.picPath_createdBy;
    NSArray *parts = [fullPath componentsSeparatedByString:@"/"];
    NSString *imageName=[parts lastObject];
    NSArray *imageNameParts = [imageName componentsSeparatedByString:@"."];
   // NSString *userImageName = [NSString stringWithFormat:@"%@",[imageNameParts firstObject]];
    NSString *userImageNameWithJPEG = [NSString stringWithFormat:@"%@.jpeg",[imageNameParts firstObject]];
    UIImage *img = [GlobalFunction loadImageOfLogoFromCacheFolder:userImageNameWithJPEG dirname:@"taskImages"];
    if (img)
    {
        self.imgViewUser.image = img;
        
    }
    else
    {
        self.imgViewUser.image=imageUserDefault;
    }
    
    [self.view layoutIfNeeded];
   // _reviewTextView.delegate=self;

     [self.reviewTextView setDelegate:self];

    if (IS_IPHONE_5) {
        
        self.constraintBtnCommentHeight.constant = 40;
        _textViewHeight.constant=221;
        _commentBoxImageHeight.constant=226;
         [self.headingLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:14.5]];
         [self.addYourRatingLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:14]];
         [self.nameLbl setFont:[UIFont fontWithName:@"Lato-Regular" size:14]];
         [self.reviewTextView setFont:[UIFont fontWithName:@"Lato-Regular" size:14]];
         _imgViewWidth.constant=60;
        _imgViewHeight.constant=60;
       
        _headingLabelTop.constant=12;
        _nameLabelTop.constant=12;
        _lineTop.constant=8;
        _addYourRatingTop.constant=8;
        _addYourRatingLblLeft.constant=19;
        _starImageTop.constant=6;
        _starImageLeft.constant=0;
        _starTwoLeft.constant=-1;
        _starThreeLeft.constant=-1;
        _starFourLeft.constant=-1;
        _starFiveLeft.constant=-1;
        _iamgeViewTop.constant=10;
        _commentImageViewTop.constant=7;
        _textViewTop.constant=15;
        [self.btnAddComment.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:16]];
        _reviewTxtViewLeft.constant=15;
        
        self.imgViewUser.layer.cornerRadius = 30;
        self.imgViewUser.layer.masksToBounds = YES;
        //self.imgViewUser.clipsToBounds = YES;
        
    }
    if (IS_IPHONE_6) {
        
        
        self.constraintBtnCommentHeight.constant = 45;
        _textViewHeight.constant=260;
        _headingLabelTop.constant=15;
        _imgViewHeight.constant=72;
        _nameLabelTop.constant=16;
        _lineTop.constant=12;
        _addYourRatingTop.constant=12;
        
        [self.addYourRatingLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:16]];
        [self.nameLbl setFont:[UIFont fontWithName:@"Lato-Regular" size:16]];
        _starImageTop.constant=11;
        _starImageLeft.constant=3;
        _headingLabelLeft.constant=11;
        _iamgeViewTop.constant=13;
        _reviewTxtViewLeft.constant=19;
        _addYourRatingLblLeft.constant=23;
        _savePublishBtnTop.constant=22;
        [self.btnAddComment.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:19]];
        [self.addYourRatingLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:16]];
        [self.reviewTextView setFont:[UIFont fontWithName:@"Lato-Regular" size:16]];

 //       _imgViewHeight.constant=65;
//        _imgViewWidth.constant=66;
        
        self.imgViewUser.layer.cornerRadius =34.5;
        self.imgViewUser.layer.masksToBounds = YES;


    }
    if (IS_IPHONE_6P) {
        
        [self.btnAddComment.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:21]];
        self.constraintBtnCommentHeight.constant = 50;
        _textViewHeight.constant=291;
        _iamgeViewTop.constant=17;
        _headingLabelTop.constant=21;
        _imgViewHeight.constant=80;
        _imgViewWidth.constant=80;
        _nameLabelTop.constant=20;
        _lineTop.constant=12;
        _addYourRatingTop.constant=15;
        _addYourRatingLblLeft.constant=23;
        
        [self.addYourRatingLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:18]];
        _starImageTop.constant=14;
        _starImageLeft.constant=3;

        [self.headingLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:19]];
        [self.reviewTextView setFont:[UIFont fontWithName:@"Lato-Regular" size:18]];
        _textViewTop.constant=23;
        _reviewTxtViewLeft.constant=22;
        
        
        self.imgViewUser.layer.cornerRadius =self.imgViewHeight.constant/2;
        self.imgViewUser.layer.masksToBounds = YES;
        
    }
    
    
    if(IS_IPHONE_4_OR_LESS)
    {
        self.constraintBtnCommentHeight.constant = 40;
        _textViewHeight.constant=135;
        _commentBoxImageHeight.constant=140;
        [self.headingLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:14.5]];
        [self.addYourRatingLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:14]];
        [self.nameLbl setFont:[UIFont fontWithName:@"Lato-Regular" size:14]];
        [self.reviewTextView setFont:[UIFont fontWithName:@"Lato-Regular" size:14]];
        _imgViewWidth.constant=60;
        _imgViewHeight.constant=60;
        
        _headingLabelTop.constant=12;
        _nameLabelTop.constant=12;
        _lineTop.constant=8;
        _addYourRatingTop.constant=8;
        _addYourRatingLblLeft.constant=19;
        _starImageTop.constant=6;
        _starImageLeft.constant=0;
        _starTwoLeft.constant=-1;
        _starThreeLeft.constant=-1;
        _starFourLeft.constant=-1;
        _starFiveLeft.constant=-1;
        _iamgeViewTop.constant=10;
        _commentImageViewTop.constant=7;
        _textViewTop.constant=15;
        [self.btnAddComment.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:16]];
        _reviewTxtViewLeft.constant=15;
        
        self.imgViewUser.layer.cornerRadius = 30;
        self.imgViewUser.layer.masksToBounds = YES;
        //self.imgViewUser.clipsToBounds = YES;

    }
    
    
    [self.view layoutIfNeeded];
    
}

- (void)didReceiveMemoryWarning{
    
    [super didReceiveMemoryWarning];
    
}



- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    
    
    NSLog(@"range change");
    
    
    NSAttributedString *star = [[NSAttributedString alloc] initWithString:@"*" attributes:@{ NSForegroundColorAttributeName :[UIColor redColor],
                                                                                             }];
    NSMutableAttributedString *strQuantityTitle = [[NSMutableAttributedString alloc] initWithString:@"Write review" attributes:@{ NSForegroundColorAttributeName : [UIColor colorWithRed:196.0/255.0f green:196.0/255.0f blue:196.0/255.0f alpha:1.0],
                                                                                                                                  }];
    [strQuantityTitle appendAttributedString:star];
   // [self.reviewTextView setAttributedText:strQuantityTitle];
    
      NSString *value = [textView.text stringByReplacingCharactersInRange:range withString:text];
    
    
    
    if ([_reviewTextView.text isEqualToString:@"Write review*"]) {
        _reviewTextView.text = @"";
        _reviewTextView.textColor=[UIColor colorWithRed:63.0/255.0f green:67.0/255.0f blue:70.0/255.0f alpha:1.0];

    }
    else if (value.length==0)
    {
//        _reviewTextView.text = @"Write review";
//        _reviewTextView.textColor=[UIColor colorWithRed:196.0/255.0f green:196.0/255.0f blue:196.0/255.0f alpha:1.0];
        [self.reviewTextView setAttributedText:strQuantityTitle];
        [_reviewTextView resignFirstResponder];
        
        
        return NO;
    }
    else if (value.length>0)
    {
        _reviewTextView.textColor=[UIColor colorWithRed:63.0/255.0f green:67.0/255.0f blue:70.0/255.0f alpha:1.0];

    }
    
    
    
    
    return YES;
    
    
    if (_reviewTextView.text.length > 1 && [_reviewTextView.text isEqualToString:@"Write review"]) {
        _reviewTextView.text = @"";
        _reviewTextView.textColor = [UIColor colorWithRed:63.0/255.0f green:67.0/255.0f blue:70.0/255.0f alpha:1.0];
        
    }
    if ([text isEqualToString:@"\n"]) {
        
        CGRect frame = self.view.frame;
        frame.origin.y = 0;
        
        [UIView animateWithDuration:0.3 animations:^{
            self.view.frame = frame;
        } completion:^(BOOL finished) {
            
        }];
        
        [_reviewTextView resignFirstResponder];
        return NO;
    }
    return YES;
    
}


- (IBAction)starBtn1Clicked:(id)sender {
    
    [self manageRatings:[sender tag]];
    
}

- (IBAction)starBtn2Clicked:(id)sender {
    
    [self manageRatings:[sender tag]];
    
}

- (IBAction)starBtn3Clicked:(id)sender {
    
    [self manageRatings:[sender tag]];
    
}

- (IBAction)starBtn4Clicked:(id)sender {
    
    [self manageRatings:[sender tag]];
    
}

- (IBAction)starBtn5Clicked:(id)sender {
    
    [self manageRatings:[sender tag]];
    
}


-(void)manageRatings:(NSInteger)tag{
    
    for (UIButton *btn in self.btnStarCollection) {
        
        if (btn.tag <= tag) {
            
            if (btn.tag == tag) {
                
                if (!btn.isSelected) {
                    
                    [UIView transitionWithView:btn
                                      duration:0.5
                                       options:UIViewAnimationOptionTransitionFlipFromLeft
                                    animations:^{            [btn setSelected:YES]; }
                                    completion:nil];
                    
                }else{
                    
                    [UIView transitionWithView:btn
                                      duration:0.5
                                       options:UIViewAnimationOptionTransitionFlipFromRight
                                    animations:^{            [btn setSelected:NO]; }
                                    completion:nil];
                    
                }
                
            }else{
                
                [UIView transitionWithView:btn
                                  duration:0.5
                                   options:UIViewAnimationOptionTransitionFlipFromLeft
                                animations:^{            [btn setSelected:YES]; }
                                completion:nil];
            }
            
        }else{
            
            [UIView transitionWithView:btn
                              duration:0.5
                               options:UIViewAnimationOptionTransitionFlipFromRight
                            animations:^{            [btn setSelected:NO]; }
                            completion:nil];
            
        }
        
    }
    
}

-(NSInteger)fetchNoOfRatings{
    
    NSInteger count = 0;
    
    for (UIButton *btn in self.btnStarCollection) {
        
        if (btn.selected) {
            
            count = count + 1;
            
        }
        
    }
    
    return count;
    
}



- (IBAction)commentClicked:(id)sender {
        
    [_reviewTextView resignFirstResponder];
    
    [GlobalFunction addIndicatorView];
    
    [self performSelector:@selector(submitRatings) withObject:nil afterDelay:0.3];

    
    
//    myAppDelegate.isSideBarAccesible = true;
//    
//    TaskTableViewCell *cell = (TaskTableViewCell *)[myAppDelegate.vcObj.tblViewSideBar cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
//    [myAppDelegate.vcObj performSegueWithIdentifier:@"viewAllTask" sender:cell];

    
}


#pragma mark - todo update working here
-(void)submitRatings{
    
    if([_reviewTextView.text isEqualToString:@"Write review*"]){

        [GlobalFunction removeIndicatorView];

        [[GlobalFunction shared] showAlertForMessage:@"Please add review to rate."];
        
        return;
        
    }
    if ([[_reviewTextView.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@"Write review*"]) {
        
        [GlobalFunction removeIndicatorView];

        [[GlobalFunction shared] showAlertForMessage:@"Please add review to rate."];
        
        return;

    }

    
    if (![[WebService shared] connected]) {
        
        [GlobalFunction removeIndicatorView];
        
        [[GlobalFunction shared] showAlertForMessage:@"Can't connect to internet, please check and try again."];
        
        return;
    }

    NSDictionary *dictRatingResponse;
    
////    if (myAppDelegate.isComingFromPushNotification) {
////        
////        dictRatingResponse = [[WebService shared] addRatingUserid:myAppDelegate.workerPushTaskDetail.userId_acceptedBy ratingStar:[NSString stringWithFormat:@"%ld",(long)[self fetchNoOfRatings]] description:_reviewTextView.text taskid:myAppDelegate.taskPushData.taskid employerId:myAppDelegate.workerPushTaskDetail.userId_createdBy];
////
////    }else{
//    
//        dictRatingResponse = [[WebService shared] addRatingUserid:myAppDelegate.workerTaskDetail.userId_acceptedBy ratingStar:[NSString stringWithFormat:@"%ld",(long)[self fetchNoOfRatings]] description:_reviewTextView.text taskid:myAppDelegate.taskData.taskid employerId:myAppDelegate.workerTaskDetail.userId_createdBy];
//
//  //  }
    NSLog(@"main user id is...%@",myAppDelegate.userData.userid);
    dictRatingResponse = [[WebService shared] addRatingUserid:myAppDelegate.userData.userid ratingStar:[NSString stringWithFormat:@"%ld",(long)[self fetchNoOfRatings]] description:_reviewTextView.text taskid:myAppDelegate.workerTaskDetail_taskDetailObj_taskid employerId:myAppDelegate.workerTaskDetail_userId_createdBy];

    
    if (dictRatingResponse) {
        
        NSLog(@"response --- %@",[EncryptDecryptFile decrypt:[dictRatingResponse valueForKey:@"responseMessage"]]);
        
        if ([[EncryptDecryptFile decrypt:[dictRatingResponse valueForKey:@"responseCode"]] isEqualToString:@"200"]) {
            
            NSString* final = @"Reservation rated successfully";
            
            [Flurry logEvent:final];
            
            [self submitRatingSuccessfull:[EncryptDecryptFile decrypt:[dictRatingResponse valueForKey:@"responseMessage"]]];
            
        }else{
            
            [[GlobalFunction shared] showAlertForMessage:[EncryptDecryptFile decrypt:[dictRatingResponse valueForKey:@"responseMessage"]]];
            
            [GlobalFunction removeIndicatorView];
        }
        
    }else{
        
        [[GlobalFunction shared] showAlertForMessage:@"Server is down, please try again later."];
        
        [GlobalFunction removeIndicatorView];
        
    }
    
}

-(void)submitRatingSuccessfull:(NSString *)response{
    
    [GlobalFunction removeIndicatorView];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"BUY TIMEE" message:response delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    
    alert.tag = 10001;
    
    [alert show];
    
}

-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex{
    
    if (alertView.tag == 10001) {
        
         if(buttonIndex == [alertView cancelButtonIndex]){
             
                 myAppDelegate.isSideBarAccesible = true;
             
//                 TaskTableViewCell *cell = (TaskTableViewCell *)[myAppDelegate.vcObj.tblViewSideBar cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
//                 [myAppDelegate.vcObj performSegueWithIdentifier:@"viewAllTask" sender:cell];
             
             TaskTableViewCell *cell = (TaskTableViewCell *)[myAppDelegate.vcObj.tblViewSideBar cellForRowAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:0]];
             [myAppDelegate.vcObj performSegueWithIdentifier:@"taskhistory" sender:cell];

         }
        
        
//        if(buttonIndex == [alertView cancelButtonIndex]){
//            
//            if (myAppDelegate.isComingFromPushNotification) {
//                
//                [myAppDelegate.vcObj backFromPushNotificationView:myAppDelegate.vcObj.btnBackForPushNotificationView];
//                
////                [myAppDelegate.vcObj backToMainView:myAppDelegate.vcObj.btnBack];
//                
//            }else{
//                
////                myAppDelegate.isSideBarAccesible = true;
////                
////                myAppDelegate.isComingFromRating = true;
////                
////                TaskTableViewCell *cell = (TaskTableViewCell *)[myAppDelegate.vcObj.tblViewSideBar cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
////                [myAppDelegate.vcObj performSegueWithIdentifier:@"taskhistory" sender:cell];
//
//            }
//        }
    }
}



-(void)textViewDidBeginEditing:(UITextView *)textView{
    
    
    
    _txtActiveField = textView;
    
    
    
    [self createInputAccessoryView];
    [_txtActiveField setInputAccessoryView:_inputAccView];
    

   if(IS_IPHONE_4_OR_LESS)
   {
       CGRect frame = self.view.frame;
       frame.origin.y = -190;
       
       [UIView animateWithDuration:0.3 animations:^{
           self.view.frame = frame;
       } completion:^(BOOL finished) {
           if([_reviewTextView.text isEqualToString:@"Write review*"]){
           }
       }];

   }
    
    else
    {
        CGRect frame = self.view.frame;
        frame.origin.y = -150;
        
        [UIView animateWithDuration:0.3 animations:^{
            self.view.frame = frame;
        } completion:^(BOOL finished) {
            if([_reviewTextView.text isEqualToString:@"Write review*"]){
            }
        }];

    }
    
    
    NSLog(@"begin editing,,,");
    

}


- (void)textViewDidChange:(UITextView *)textView{
    
    
    return;
    
    
    if (_reviewTextView.text.length != 0 && [[_reviewTextView.text substringFromIndex:1] isEqualToString:@"Write review"]){
        _reviewTextView.text = [_reviewTextView.text substringToIndex:1];
        _reviewTextView.textColor = [UIColor blackColor]; //optional
        
    }

    
//    
//    if (_reviewTextView.text.length != 0 && [[_reviewTextView.text substringFromIndex:1] isEqualToString:@"Write review"] && [_reviewTextView.textColor isEqual:[UIColor lightGrayColor]]){
//        _reviewTextView.text = [_reviewTextView.text substringToIndex:1];
//        _reviewTextView.textColor = [UIColor blackColor]; //optional
//        
//    }
    else if(_reviewTextView.text.length == 0){
        _reviewTextView.text = @"Write review";
        _reviewTextView.textColor = [UIColor lightGrayColor];
        [_reviewTextView setSelectedRange:NSMakeRange(0, 0)];
    }
    
    
    CGRect line = [_reviewTextView caretRectForPosition:
                   _reviewTextView.selectedTextRange.start];
    CGFloat overflow = line.origin.y + line.size.height
    - ( _reviewTextView.contentOffset.y + _reviewTextView.bounds.size.height
       - _reviewTextView.contentInset.bottom - _reviewTextView.contentInset.top );
    if ( overflow > 0 ) {
        // We are at the bottom of the visible text and introduced a line feed, scroll down (iOS 7 does not do it)
        // Scroll caret to visible area
        CGPoint offset = _reviewTextView.contentOffset;
        offset.y += overflow + 7; // leave 7 pixels margin
        // Cannot animate with setContentOffset:animated: or caret will not appear
        [UIView animateWithDuration:.2 animations:^{
            [_reviewTextView setContentOffset:offset];
        }];
    }

}

-(void)textViewDidEndEditing:(UITextView *)textView{
    
    [_reviewTextView resignFirstResponder];
    
    CGRect frame = self.view.frame;
    frame.origin.y = 0;
    
    NSAttributedString *star = [[NSAttributedString alloc] initWithString:@"*" attributes:@{ NSForegroundColorAttributeName :[UIColor redColor],
                                                                                             }];
    NSMutableAttributedString *strQuantityTitle = [[NSMutableAttributedString alloc] initWithString:@"Write review" attributes:@{ NSForegroundColorAttributeName : [UIColor colorWithRed:196.0/255.0f green:196.0/255.0f blue:196.0/255.0f alpha:1.0],
                                                                                                                                  }];
    [strQuantityTitle appendAttributedString:star];
  //  [self.reviewTextView setAttributedText:strQuantityTitle];
    
    [UIView animateWithDuration:0.3 animations:^{
        self.view.frame = frame;
    } completion:^(BOOL finished) {
        if ([_reviewTextView.text isEqualToString:@""]) {
           // [_reviewTextView.text = @"Write review";
           // _reviewTextView.textColor = [UIColor lightGrayColor]; //optional
            [_reviewTextView setAttributedText:strQuantityTitle];
        }
    }];

}


- (IBAction)animateImageViewToFullScreen:(id)sender {
    
    [GlobalFunction animateImageview:self.imgViewUser];

}

@end
