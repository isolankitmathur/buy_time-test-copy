//
//  RatingViewController.h
//  HandApp
//
//  Created by  ~ on 04/03/16.
//  Copyright (c) 2016 . All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Flurry.h"


@interface RatingViewController : UIViewController<UITextViewDelegate>

@property (weak, nonatomic) IBOutlet UILabel *nameLbl;
@property (weak, nonatomic) IBOutlet UILabel *lblUserAddress;
@property (weak, nonatomic) IBOutlet UILabel *lblUserLastAddress;

- (IBAction)starBtn1Clicked:(id)sender;
- (IBAction)starBtn2Clicked:(id)sender;
- (IBAction)starBtn3Clicked:(id)sender;
- (IBAction)starBtn4Clicked:(id)sender;
- (IBAction)starBtn5Clicked:(id)sender;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintBtnCommentHeight;

- (IBAction)commentClicked:(id)sender;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *savePublishBtnTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *textViewHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *commentBoxImageHeight;
@property (weak, nonatomic) IBOutlet UILabel *headingLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *iamgeViewTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *nameLabelTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lineTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *addYourRatingTop;
@property (weak, nonatomic) IBOutlet UILabel *addYourRatingLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *starImageTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *commentImageViewTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *textViewTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *headingLabelTop;

@property (weak, nonatomic) IBOutlet UITextView *reviewTextView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imgViewHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imgViewWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *starImageLeft;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *headingLabelLeft;


@property (nonatomic, retain) UITextView *txtActiveField;
@property (nonatomic, retain) UIView *inputAccView;
@property (nonatomic, retain) UIView *DatePickerInputAccView;
@property (nonatomic, retain) UIButton *btnDone;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lineLblHeight;
@property (weak, nonatomic) IBOutlet UILabel *lineLbl;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *reviewTxtViewLeft;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *addYourRatingLblLeft;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *starTwoLeft;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *starThreeLeft;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *starFourLeft;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *starFiveLeft;



@end
