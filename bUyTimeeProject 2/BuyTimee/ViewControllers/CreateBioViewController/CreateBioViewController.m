//
//  CreateBioViewController.m
//  HandApp
//


#import "CreateBioViewController.h"
#import "PaymentInfoViewController.h"
#import "Constants.h"
#import "TaskTableViewCell.h"
#import "GlobalFunction.h"
#import "BioInfoTableViewCell.h"
#import <AVFoundation/AVFoundation.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import <Photos/Photos.h>
#import "BankInfoViewController.h"
#import "SetUpBankViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "cashOutPayPalViewController.h"


#import "TermsAndCondition.h"

#import "BraintreePayPal.h"
#import "PPDataCollector.h"

@interface CreateBioViewController () <BTAppSwitchDelegate, BTViewControllerPresentingDelegate, UIImagePickerControllerDelegate,UIActionSheetDelegate,UIAlertViewDelegate>

//@property (nonatomic, strong) BTAPIClient *braintreeClient;
@property (nonatomic, strong) BTPayPalDriver *payPalDriver;


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintSaveBioBtnHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consUploadPhotoTop;

@property (weak, nonatomic) IBOutlet UIImageView *imgViewUsersPicture;
@end

@implementation CreateBioViewController
{
    BOOL isPictureAvailabe;
    BOOL isOldPictureAvailable;
    
    NSString *deviceData;
    NSString *nonce;
    NSString *buttonClicked;
    BOOL isFromPaymentOption;
}

@synthesize txtActiveField;
@synthesize inputAccView;
@synthesize btnDone;
@synthesize btnNext;
@synthesize btnPrev;
@synthesize btnAddBankInfo;

- (void)viewDidLoad {
    [super viewDidLoad];
   myAppDelegate.TandC_Buyee = @"CreteBio";
    myAppDelegate.vcObj.btnMapOnListView.hidden = true;//vs
    myAppDelegate.vcObj.BtnTransparentMap.hidden = true;//vs
    
    
  //  myAppDelegate.createBioVCObj.consGreyLineLbl.hidden=NO;
   _consGreyLineLbl.backgroundColor = [UIColor clearColor];
    myAppDelegate.NavController=self.navigationController;
    
    
    //*************
    //web view work by KP
    
    
    
    
    
    
    
    // Do any additional setup after loading the view.
    
    self.textFieldDescription.delegate=self;
    self.textFieldEmail.delegate=self;
    self.textFieldLocation.delegate=self;
    self.textFieldPhone.delegate=self;
    self.textFieldUsername.delegate=self;
    
   // myAppDelegate.vcObj.lblTitleText.text = @"B U Y  T I M E E";
    
    
    
    //**************** Manage text fields placeholder and font
    NSAttributedString *star = [[NSAttributedString alloc] initWithString:@"*" attributes:@{ NSForegroundColorAttributeName :[UIColor redColor],
                                                                                             }];
    
    
    NSMutableAttributedString *str1 = [[NSMutableAttributedString alloc] initWithString:@"Name" attributes:@{ NSForegroundColorAttributeName :[UIColor colorWithRed:143.0/255.0f green:143.0/255.0f blue:143.0/255.0f alpha:1.0],
                                                                                                         }];
    
    [str1 appendAttributedString: star];
    self.textFieldUsername.attributedPlaceholder = str1;
    
//    NSAttributedString *str2 = [[NSAttributedString alloc] initWithString:@"Instagram" attributes:@{ NSForegroundColorAttributeName :[UIColor colorWithRed:143.0/255.0f green:143.0/255.0f blue:143.0/255.0f alpha:1.0],
//                                                                                                }];
//    self.textFieldLocation.attributedPlaceholder = str2;
    
    NSMutableAttributedString *str3 = [[NSMutableAttributedString alloc] initWithString:@"Location" attributes:@{ NSForegroundColorAttributeName :[UIColor colorWithRed:143.0/255.0f green:143.0/255.0f blue:143.0/255.0f alpha:1.0],
                                                                                                    }];
   [str3 appendAttributedString: star];
    self.textFieldEmail.attributedPlaceholder = str3;

    NSAttributedString *str4 = [[NSAttributedString alloc] initWithString:@"About Me" attributes:@{ NSForegroundColorAttributeName :[UIColor colorWithRed:143.0/255.0f green:143.0/255.0f blue:143.0/255.0f alpha:1.0],
                                                                                                     }];
    self.textFieldDescription.attributedPlaceholder = str4;
    
    
    NSMutableAttributedString *str5 = [[NSMutableAttributedString alloc] initWithString:@"Contact Number" attributes:@{ NSForegroundColorAttributeName :[UIColor colorWithRed:143.0/255.0f green:143.0/255.0f blue:143.0/255.0f alpha:1.0],
                                                                                                    }];
     [str5 appendAttributedString: star];
    self.textFieldPhone.attributedPlaceholder = str5;
    
    
    
    
    if (IS_IPHONE_4_OR_LESS) {
        [self.btnSave.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:12]];
        
        [self.btnCancel.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:12]];
//        [self.btnSave.titleLabel setFont:[UIFont systemFontOfSize:12.0]];
//        [self.btnCancel.titleLabel setFont:[UIFont systemFontOfSize:12.0]];
    }
    if (IS_IPHONE_5) {
        [self.btnSave.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:17]];
        
        [self.btnCancel.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:17]];
        
//        [self.btnSave.titleLabel setFont:[UIFont systemFontOfSize:15.0]];
//        [self.btnCancel.titleLabel setFont:[UIFont systemFontOfSize:15.0]];
    }
    if (IS_IPHONE_6) {
        
        [self.btnSave.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:19]];
        
        [self.btnCancel.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:19]];
        
        
        
        
//        [self.btnSave.titleLabel setFont:[UIFont systemFontOfSize:19.0]];
//        [self.btnCancel.titleLabel setFont:[UIFont systemFontOfSize:19.0]];
    }
    if (IS_IPHONE_6P) {
        
        [self.btnSave.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:21]];
        [self.btnCancel.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:21]];
        _constraintSaveBioBtnHeight.constant=53;
        
//        [self.btnSave.titleLabel setFont:[UIFont systemFontOfSize:23.0]];
//        [self.btnCancel.titleLabel setFont:[UIFont systemFontOfSize:23.0]];
    }
//
    
   //**************** Manage text fields placeholder and font end
    
    
    self.textFieldUsername.text = myAppDelegate.userData.username;
   // self.textFieldPhone.text = myAppDelegate.userData.mobile;
    self.textFieldLocation.text = myAppDelegate.userData.instaLink;
    self.textFieldEmail.text = myAppDelegate.userData.location;
    self.textFieldDescription.text = myAppDelegate.userData.userDescription;
    
    
    if([myAppDelegate.userData.mobile isEqualToString:@"0"])
    {
        
    }
    else
    {
        self.textFieldPhone.text=myAppDelegate.userData.mobile;
    }
    
    
    
    
    
    
    
   // self.textFieldUsername.enabled = NO;
   // self.textFieldUsername.userInteractionEnabled = NO;
    
    
    self.textFieldPhone.enabled = YES;
    self.textFieldPhone.userInteractionEnabled = YES;

    if (myAppDelegate.userData.userImage) {
        if ([myAppDelegate.userData.userImageName isEqualToString:@""]) {
            isOldPictureAvailable = false;
            isPictureAvailabe = false;
        }else{
            isOldPictureAvailable = true;
            
            
            self.imgViewUsersPicture.image = myAppDelegate.userData.userImage;
            
           if(IS_IPHONE_6)
           {
           self.imgViewUsersPicture.layer.cornerRadius = self.imgViewUsersPicture.frame.size.height/2;
            self.imgViewUsersPicture.layer.masksToBounds = YES;
           }
            
                NSLog(@"%f size of images--- ", _imgViewUsersPicture.frame.size.width);
                NSLog(@"%f size of images--- ", _imgViewUsersPicture.frame.size.height);

            
            
            
                  [self manageConstraint];
            
          
            
                NSLog(@"%f size of images--- ", _imgViewUsersPicture.frame.size.width);
                NSLog(@"%f size of images--- ", _imgViewUsersPicture.frame.size.height);

                 [GlobalFunction removeIndicatorView];

            
            
            
        }
    }else{
        isOldPictureAvailable = false;
        isPictureAvailabe = false;
    }
    
    if (myAppDelegate.isBankInfoFilled) {
        // [self.btnAddBankInfo setTitle:@"Disconnect from stripe" forState:UIControlStateNormal];
    }else{
        
    }
    
    NSLog(@"authorization code is...%@",myAppDelegate.Authorize_String_BrainTree);
    
    self.braintreeClient = [[BTAPIClient alloc] initWithAuthorization:myAppDelegate.Authorize_String_BrainTree];
//    
//    
//    UIButton *customPayPalButton = [[UIButton alloc] initWithFrame:CGRectMake(0, -10, 280, 30)];
//    customPayPalButton.backgroundColor = [UIColor redColor];
//    [customPayPalButton addTarget:self
//                           action:@selector(customPayPalButtonTapped:)
//                 forControlEvents:UIControlEventTouchUpInside];
//    [self.btnAddPaymentInfo addSubview:customPayPalButton];
    
    self.cashOutBtn.hidden = true;
  //  self.paymentOptionView.hidden = true;
    

//    self.paymentOptionView.backgroundColor = [UIColor whiteColor];
//    self.paymentOptionView.layer.cornerRadius = 4.0;
//    self.paymentOptionView.layer.borderColor = [UIColor grayColor].CGColor;
//    self.paymentOptionView.layer.borderWidth = 2.0f;
//    
//    self.circleImgViewPaypal.layer.cornerRadius = 10;
//    
//    self.circleImgViewPaypal.layer.masksToBounds = YES;
//    
//    self.circleImgViewBank.layer.cornerRadius = 10;
//    
//    self.circleImgViewBank.layer.masksToBounds = YES;
//    
//    self.circleImgViewBank.backgroundColor = [UIColor colorWithRed:63.0/255.0f green:67.0/255.0f blue:70.0/255.0f alpha:1.0];
//    self.circleImgViewPaypal.backgroundColor = [UIColor colorWithRed:63.0/255.0f green:67.0/255.0f blue:70.0/255.0f alpha:1.0];
//    
//    self.circleImgViewBank.hidden = YES;
//    self.circleImgViewPaypal.hidden = YES;
    
    buttonClicked = @"";
    [self.btnRadioStripe setBackgroundImage:[UIImage imageNamed:@"radioButton.png"] forState:UIControlStateNormal];
    [self.btnRadioPayPal setBackgroundImage:[UIImage imageNamed:@"radioButton.png"] forState:UIControlStateNormal];
    
   
    self.lblOptionPayPal.userInteractionEnabled = YES;
    UITapGestureRecognizer *tapGestureLblPaypal = \
    [[UITapGestureRecognizer alloc]
     initWithTarget:self action:@selector(tapOnPayPalLbl:)];
    [self.lblOptionPayPal addGestureRecognizer:tapGestureLblPaypal];
    
    self.lblOptionStripe.userInteractionEnabled = YES;
    UITapGestureRecognizer *tapGestureLblStripe = \
    [[UITapGestureRecognizer alloc]
     initWithTarget:self action:@selector(tapOnStripeLbl:)];
    [self.lblOptionStripe addGestureRecognizer:tapGestureLblStripe];
}


-(void)appSwitcher:(id)appSwitcher didPerformSwitchToTarget:(BTAppSwitchTarget)target
{
    
}

- (IBAction)customPayPalButtonTapped:(id)sender {
    
    
//    BTPayPalDriver *payPalDriver = [[BTPayPalDriver alloc] initWithAPIClient:self.braintreeClient];
//    payPalDriver.viewControllerPresentingDelegate = self;
//    payPalDriver.appSwitchDelegate = self; // Optional
//
//
//
//    BTPayPalRequest *checkout = [[BTPayPalRequest alloc] init];
//    checkout.billingAgreementDescription = @"Your agreement description";
//    //checkout.billingAgreementDescription = [NSURL URLWithString:@"https://www.paypal.com/webapps/mpp/ua/useragreement-full"];
//    [payPalDriver requestBillingAgreement:checkout completion:^(BTPayPalAccountNonce * _Nullable tokenizedPayPalCheckout, NSError * _Nullable error) {
//        if (error) {
//            // self.progressBlock(error.localizedDescription);
//        } else if (tokenizedPayPalCheckout) {
//            NSLog(@"Got a nonce! %@", tokenizedPayPalCheckout.nonce);
//
//            NSLog(@"Got a type is! %@", tokenizedPayPalCheckout.type);
//
//            NSLog(@"Got a type is! %@", tokenizedPayPalCheckout.payerId);
//
//            BTPostalAddress *address = tokenizedPayPalCheckout.billingAddress?: tokenizedPayPalCheckout.shippingAddress;
//            NSLog(@"Billing address:\n%@\n%@\n%@ %@\n%@ %@", address.streetAddress, address.extendedAddress, address.locality, address.region, address.postalCode, address.countryCodeAlpha2);
//            // self.completionBlock(tokenizedPayPalCheckout);
//        } else {
//            //            self.progressBlock(@"Cancelled");
//        }
//    }];
//
//
//
//    NSString *deviceData = [PPDataCollector collectPayPalDeviceData];
//    NSLog(@"Send this device data to your server: %@", deviceData);
//
//

}


- (void)startCheckout {
    
   // [GlobalFunction addIndicatorView];
    
    // Example: Initialize BTAPIClient, if you haven't already
    deviceData = @"";
    nonce = @"";
    
    deviceData = [PPDataCollector collectPayPalDeviceData];
    NSLog(@"Send this device data to your server: %@", deviceData);
    
    self.payPalDriver = [[BTPayPalDriver alloc] initWithAPIClient:self.braintreeClient];
    BTPayPalDriver *payPalDriver = [[BTPayPalDriver alloc] initWithAPIClient:self.braintreeClient];
    self.payPalDriver.viewControllerPresentingDelegate = myAppDelegate.vcObj;
    //self.payPalDriver.appSwitchDelegate = self; // Optional
    
    BTPayPalRequest *checkout = [[BTPayPalRequest alloc] init];
    
    //    [self.payPalDriver authorizeAccountWithAdditionalScopes:[NSSet setWithArray:@[@"address"]]
    //                                            completion:^(BTPayPalAccountNonce *tokenizedPayPalAccount, NSError *error) {
    //        if (tokenizedPayPalAccount) {
    //            BTPostalAddress *address = tokenizedPayPalAccount.billingAddress ?: tokenizedPayPalAccount.shippingAddress;
    //            NSLog(@"nonce is...%@",tokenizedPayPalAccount.nonce);
    //            NSLog(@"Address:\n%@\n%@\n%@ %@\n%@ %@", address.streetAddress, address.extendedAddress, address.locality,address.region, address.postalCode, address.countryCodeAlpha2);
    //        } else if (error) {
    //                            // Handle error
    //             NSLog(@"...Error occured...%@",error);
    //        } else {
    //                            // User canceled
    //            NSLog(@"...Use hit cancell button...");
    //        }
    //        }];
    
    
    checkout.billingAgreementDescription = @"Your agreement description";
   // checkout.displayName = @"sdsdsdsdsdsdsdsdsdsd";
   // checkout.description; //= @"ddgdgdadjavdhjv jhdvhjav jdvjhavajd jhdvhjas";
    
    
    [self.payPalDriver requestBillingAgreement:checkout completion:^(BTPayPalAccountNonce * _Nullable tokenizedPayPalCheckout, NSError * _Nullable error) {
        if (error)
        {
            NSLog(@"error occuewd");
            [self errorShow:[NSString stringWithFormat:@"%@",error]];
            [GlobalFunction removeIndicatorView];
            
        } else if (tokenizedPayPalCheckout) {
            
           // [GlobalFunction addIndicatorView];
            
            nonce = tokenizedPayPalCheckout.nonce;
            
            NSLog(@"Got a nonce! %@",nonce);
            
            NSLog(@"Got a type is! %@", tokenizedPayPalCheckout.type);
            
            NSLog(@"Got a type is! %@", tokenizedPayPalCheckout.payerId);
            
            BTPostalAddress *address = tokenizedPayPalCheckout.billingAddress?: tokenizedPayPalCheckout.shippingAddress;
            NSLog(@"Billing address:\n%@\n%@\n%@ %@\n%@ %@", address.streetAddress, address.extendedAddress, address.locality, address.region, address.postalCode, address.countryCodeAlpha2);
            
           [GlobalFunction addIndicatorView];
            
           [self sendAuthorizationToServer:deviceData nonce:nonce];
            
        } else
        {
            NSLog(@"user clicked cancelled");
           // [self back];
            [GlobalFunction removeIndicatorView];
//            myAppDelegate.selectedPaymentTypeFromServer = @"2";
//
//            myAppDelegate.ispayWithPayPallFilled = true;
//            myAppDelegate.isStripeCardInfoFilled = false;
        }
    }];
    
    
    
    
}

-(void) errorShow:(NSString *)message
{
    UIAlertView *alertViewErr = [[UIAlertView alloc]initWithTitle:@"BUY TIMEE" message:message delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    
    [alertViewErr show];
    alertViewErr.tag = 1111;
}

-(void)viewWillDisappear:(BOOL)animated
{
    [GlobalFunction removeIndicatorView];
    
    
}

-(void)viewDidDisappear:(BOOL)animated
{
   // [GlobalFunction removeIndicatorView];
    
    NSLog(@"Send this device data to your server");
    
}

#pragma mark - BTViewControllerPresentingDelegate

// Required
- (void)paymentDriver:(id)paymentDriver
requestsPresentationOfViewController:(UIViewController *)viewController {
    [self presentViewController:viewController animated:YES completion:nil];
}

// Required
- (void)paymentDriver:(id)paymentDriver
requestsDismissalOfViewController:(UIViewController *)viewController {
    [viewController dismissViewControllerAnimated:YES completion:nil];
    
    [GlobalFunction addIndicatorView];
}

#pragma mark - BTAppSwitchDelegate

// Optional - display and hide loading indicator UI
- (void)appSwitcherWillPerformAppSwitch:(id)appSwitcher {
    [self showLoadingUI];
    
    // You may also want to subscribe to UIApplicationDidBecomeActiveNotification
    // to dismiss the UI when a customer manually switches back to your app since
    // the payment button completion block will not be invoked in that case (e.g.
    // customer switches back via iOS Task Manager)
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(hideLoadingUI:)
                                                 name:UIApplicationDidBecomeActiveNotification
                                               object:nil];
}

- (void)appSwitcherWillProcessPaymentInfo:(id)appSwitcher {
    [self hideLoadingUI:nil];
}

#pragma mark - Private methods

- (void)showLoadingUI {
    //...
}

- (void)hideLoadingUI:(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIApplicationDidBecomeActiveNotification
                                                  object:nil];
    //....
}

-(void)webViewDidFinishLoad:(UIWebView *)webView{
    [GlobalFunction removeIndicatorView];
}

-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    [GlobalFunction removeIndicatorView];
}


-(void)viewWillAppear:(BOOL)animated

{
  //  myAppDelegate.createBioVCObj.consGreyLineLbl.hidden=NO;
_consGreyLineLbl.backgroundColor = [UIColor clearColor];
    
    
}

-(void)setRadioButtonImages
{
    
    if([myAppDelegate.selectedPaymentTypeFromServer isEqualToString:@"1"])
    {
        [self.btnRadioStripe setBackgroundImage:[UIImage imageNamed:@"radioSelectedButton.png"] forState:UIControlStateNormal];
         [self.btnRadioPayPal setBackgroundImage:[UIImage imageNamed:@"radioButton.png"] forState:UIControlStateNormal];
    }
    else if ([myAppDelegate.selectedPaymentTypeFromServer isEqualToString:@"2"])
    {
        [self.btnRadioPayPal setBackgroundImage:[UIImage imageNamed:@"radioSelectedButton.png"] forState:UIControlStateNormal];
         [self.btnRadioStripe setBackgroundImage:[UIImage imageNamed:@"radioButton.png"] forState:UIControlStateNormal];
    }
    
}

-(void)viewDidAppear:(BOOL)animated{
    
    
    NSString* final = @"User Profile page";
    
    [Flurry logEvent:final];
    
    
    [self setRadioButtonImages];
    
     myAppDelegate.isFromCreateBioOnTnC = true;
    
    [self manageConstraint];
    
    NSLog(@"%@",myAppDelegate.userData.tnc);
    if([myAppDelegate.userData.tnc isEqualToString:@"0"])
    {
        
        myAppDelegate.isTermsAndPolicyViewOpen=YES;
        
        NSMutableArray *TandCarr = [DatabaseClass getDataFromTnCData:[[NSString stringWithFormat:@"Select * from %@",tableTnC] UTF8String]];
        myAppDelegate.termsAndConditionData = (TermsAndCondition *)[TandCarr firstObject];
        myAppDelegate.termsAndConditionData.termsAndCondition=[EncryptDecryptFile decrypt:myAppDelegate.termsAndConditionData.termsAndCondition];
        NSLog(@"%@",myAppDelegate.termsAndConditionData.termsAndCondition);
        
        NSString *myDescriptionHTML = myAppDelegate.termsAndConditionData.termsAndCondition;
        UIFont *font;
        
        if(IS_IPHONE_6 || IS_IPHONE_6P)
        {
            font = [UIFont fontWithName:@"Lato-Light" size:14];
        }
        
        else if (IS_IPHONE_5 || IS_IPHONE_4_OR_LESS)
        {
            font = [UIFont fontWithName:@"Lato-Light" size:14];
        }
        
        
        myDescriptionHTML = [NSString stringWithFormat:@"<span style=\"font-family: %@; font-size: %i\">%@</span>",
                             font.fontName,
                             (int) font.pointSize,
                             myDescriptionHTML];
        
        [_termsAndPolicyWebView loadHTMLString:myDescriptionHTML baseURL:nil];
        
        _termsAndPolicyWebView.delegate=self;
        
        
        [UIView transitionWithView:self.locationHelpButton
                          duration:0.4
                           options:UIViewAnimationOptionTransitionCrossDissolve
                        animations:^{
                            self.blackTransparentView.hidden = false;
                            self.blackTransparentView.alpha = 1.0;
                        }
                        completion:NULL];

        
    }
    
    else
    {
        [UIView transitionWithView:self.locationHelpButton
                          duration:0.4
                           options:UIViewAnimationOptionTransitionCrossDissolve
                        animations:^{
                            self.blackTransparentView.hidden = true;
                            self.blackTransparentView.alpha =0;
                        }
                        completion:NULL];
        
    }

//
//    
//        NSMutableDictionary *dic=[[NSMutableDictionary alloc] init];
//     [dic setValue:myAppDelegate.userData.username forKey:@"username"];
//     [dic setValue:myAppDelegate.userData.instaLink forKey:@"instaLink"];
//     [dic setValue:myAppDelegate.userData.location forKey:@"location"];
//     [dic setValue:myAppDelegate.userData.userDescription forKey:@"aboutme"];
//     
//     
//     [UserDefaults setObject:dic forKey:keyUserloginDetailDictionary];
//     [UserDefaults synchronize];
//
//     
//     NSMutableArray *arr1 = [UserDefaults objectForKey:@"UserInfoArray"];
//     myAppDelegate.userData = [arr1 objectAtIndex:0];
//     
//     self.textFieldUsername.text = myAppDelegate.userData.username;
//     self.textFieldPhone.text = myAppDelegate.userData.mobile;
//     self.textFieldLocation.text = myAppDelegate.userData.emailId;
//     self.textFieldEmail.text = myAppDelegate.userData.location;
//     self.textFieldDescription.text = myAppDelegate.userData.userDescription;
//
//     
 
    
    
    
//    NSDictionary *dic=[UserDefaults objectForKey:keyUserloginDetailDictionary];
//    
//   // self.textFieldUsername.text = [dic valueForKey:@"username"];
//    
//    self.textFieldLocation.text = [dic valueForKey:@"instaLink"];
//    self.textFieldEmail.text = [dic valueForKey:@"location"];
//    self.textFieldDescription.text = [dic valueForKey:@"aboutme"];

    
//    if(myAppDelegate.bankInfObj.imagePickFromAlbum==YES)
//    {
//        
//    }
//    
//    
//    
//    else{
//    
//    [self manageConstraint];
//    
//    self.imgViewUsersPicture.layer.cornerRadius = self.imgViewUsersPicture.frame.size.height/2;
//    self.imgViewUsersPicture.layer.masksToBounds = YES;
//    
//    [GlobalFunction removeIndicatorView];
//    
//    NSLog(@"%f size of images--- ", _imgViewUsersPicture.frame.size.width);
//    NSLog(@"%f size of images--- ", _imgViewUsersPicture.frame.size.height);
//    }
    
    
}

-(void)manageConstraint{
    
    _consGreyLineLbl.backgroundColor = [UIColor clearColor];
    
    _consGreyLineLbl.hidden=YES;

    _btnSave.backgroundColor=[UIColor colorWithRed:63.0/255.0f green:67.0/255.0f blue:70.0/255.0f alpha:1.0];
    _btnCancel.backgroundColor=[UIColor colorWithRed:63.0/255.0f green:67.0/255.0f blue:70.0/255.0f alpha:1.0];

    _btn_Accept.backgroundColor=[UIColor colorWithRed:63.0/255.0f green:67.0/255.0f blue:70.0/255.0f alpha:1.0];
    _btn_Cancel.backgroundColor=[UIColor colorWithRed:63.0/255.0f green:67.0/255.0f blue:70.0/255.0f alpha:1.0];

    

    
    [self.view layoutIfNeeded];
    
//    self.consPhoneTextFldHeight.constant = 0;
//    self.consLblPhoneHeight.constant = 0;
//    self.textFieldPhone.hidden = YES;

    
    self.consBtnCancelLeading.constant = 0.4f;
    
    self.insideViewOfBlurView.layer.cornerRadius = 5.0;
    
    
    if (IS_IPHONE_4_OR_LESS) {
        
        self.constraintLblAddBankInfoBarBottomSpace.constant = 10;
        self.constraintLblDescriptionBarBottomSpace.constant = 10;
        self.constraintLblEmailBarBottomSpace.constant = 10;
        self.constraintLblLocationBarBottomSpace.constant = 10;
        self.constraintLblPhoneBarBottomSpace.constant = 10;
        self.constraintLblUsernameBarBottomSpace.constant = 10;
        self.constraintSaveBioBtnHeight.constant = 40;
        
        
        
        
        //*******************
        _consGreyLineLbl.backgroundColor = [UIColor clearColor];
        _consGreyLineLbl.hidden=YES;
        
        _alertViewLeft.constant=35;
        _alertViewWidth.constant=250;
        _alertViewHeight.constant=120;
        _alertViewTop.constant=150;
        
        
        self.consBtnCancelLeading.constant = 0.5f;
        [self.textFieldUsername setFont:[UIFont fontWithName:@"Lato-Regular" size:12]];
        [self.textFieldEmail setFont:[UIFont fontWithName:@"Lato-Regular" size:12]];
        [self.textFieldDescription setFont:[UIFont fontWithName:@"Lato-Regular" size:12]];
        [self.textFieldLocation setFont:[UIFont fontWithName:@"Lato-Regular" size:12]];
        [self.textFieldPhone setFont:[UIFont fontWithName:@"Lato-Regular" size:12]];
        
        
        [self.btnSave.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:15]];
        [self.btnCancel.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:15]];
        
        [self.lblUploadPhoto setFont:[UIFont fontWithName:@"Lato-Regular" size:12]];
        
        [self.btnAddBankInfo.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:12]];
        [self.btnAddPaymentInfo.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:12]];
        [self.btnVendor.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:15]];
        
        self.userNameTxtFldHeight.constant = 30;
        _textFieldPhone.hidden=NO;
        self.consPhoneTextFldHeight.constant=20;
        
        self.nsConsHelpTop.constant = 4;
        self.consHelpBtnWidth.constant=20;
        self.consHelpBtnHeight.constant=20;
        
        
        
        self.consUserImgViewTop.constant = 27;
        self.consUserImgViewWidth.constant = 56;
        self.consUserImgViewHeight.constant = 57;
        self.consUserImgViewLeading.constant = 132;
        self.consLblUploadPhotoLeading.constant = 99;
        self.consBtnTransparentUploadPhotoLeading.constant = 100;
        self.consBtnTransparentUploadPhotoTop.constant = 5;
        self.consLblUploadPhotoTop.constant = 9;
        
        self.constraintUserNameTextFieldTopSpace.constant=11;
        
        self.consUpperHolderViewHeight.constant=138;
        
        
        _consSaveBtnBottom.constant=59;
        _consCancelBtnBottom.constant=59;
        
        //*****
        //
        
        
        
        _line1Top.constant=2;//5
        _line2Top.constant=5;
        _line3Top.constant=4;
        _line4Top.constant=4;
        _line5Top.constant=7;
        _line6Top.constant=9;
        _line7Top.constant=9;
        
        
        _locationTxtFldTop.constant=6;
        _instagramTxtFldTop.constant=3;
        _aboutMeTop.constant=6;
        
        
        _phoneTxtFldTop.constant=4;
        
        _addPaymntTop.constant=11;
        _addBankTop.constant=8;
        
        
        
        
        self.consLblUserUnderLineLeading.constant = 18;
        self.consLblUserUnderLineTrailing.constant = 18;
        //self.vendorLineLblLeft.constant = 95;
        self.consBtnVendorLeading.constant = 116;
        self.vendorLineLblWidth.constant = 86;
        self.consUserNameTxtFldLeading.constant = 21;
        //    self.consUserNameTxtFldTrailing.constant = 18;
        
//        _vendorBtnBottomConstant.constant=12;
//        _vendorLineBottom.constant=16;
//        self.consUnderButtonsLineTop.constant = 16;
        
        _vendorBtnBottomConstant.constant=30;
        _vendorLineBottom.constant=28;
        self.consUnderButtonsLineTop.constant = 28;
        
        _vendorLineLblLeft.constant=101;
        _btnVendorHeight.constant=30;
        
        //_ConsGreyLineTop.constant=451;
        
        self.consUnderButtonsLineHeight.constant = 40;
        
        
                
        self.btnAddPaymentInfo.imageEdgeInsets = UIEdgeInsetsMake(0, (self.btnAddPaymentInfo.frame.size.width - [self.btnAddPaymentInfo imageForState:UIControlStateNormal].size.width+13), 0, -5);

        self.btnAddBankInfo.imageEdgeInsets = UIEdgeInsetsMake(0, (self.btnAddBankInfo.frame.size.width - [self.btnAddBankInfo imageForState:UIControlStateNormal].size.width+13), 0, -5);
        
        //top,left,bottom,right
        
        self.imgViewUsersPicture.layer.cornerRadius = self.imgViewUsersPicture.frame.size.height/2;
        self.imgViewUsersPicture.layer.masksToBounds = YES;
        
        _consTermsAndPolicyViewBottom.constant=70;
        _consTermsAndPolicyWebViewHeight.constant=250;
        _btnAcceptHeight.constant=40;
        _btnAcceptWidth.constant=144.5;
        _consLineLblHeight.constant=40;

        
        [_btn_Accept.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:17]];
        [_btn_Cancel.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:17]];
        
        
        [self.lblChooseOptionPayment setFont:[UIFont fontWithName:@"Lato-Regular" size:13]];
        [self.lblOptionPayPal setFont:[UIFont fontWithName:@"Lato-Regular" size:15]];
        [self.lblOptionStripe setFont:[UIFont fontWithName:@"Lato-Regular" size:15]];
        _consGreyLineLbl.backgroundColor = [UIColor clearColor];
        self.consLblPaypalTrailing.constant = 15;
        self.consRadioBtnSecondTrailing.constant = 5;
        self.consLblStripeLeading.constant = 6;
        self.consRadioBtnFirstLeading.constant = 24;


    }
    if (IS_IPHONE_5 ) {
        
        
        [self.lblChooseOptionPayment setFont:[UIFont fontWithName:@"Lato-Regular" size:13]];
        [self.lblOptionPayPal setFont:[UIFont fontWithName:@"Lato-Regular" size:15]];
        [self.lblOptionStripe setFont:[UIFont fontWithName:@"Lato-Regular" size:15]];
        _consGreyLineLbl.backgroundColor = [UIColor clearColor];
        self.consLblPaypalTrailing.constant = 15;
        self.consRadioBtnSecondTrailing.constant = 5;
        self.consLblStripeLeading.constant = 6;
        self.consRadioBtnFirstLeading.constant = 24;
        
        
        
        
        
        _consGreyLineLbl.hidden=YES;
        
        _alertViewLeft.constant=35;
        _alertViewWidth.constant=250;
        _alertViewHeight.constant=120;
        _alertViewTop.constant=150;
        
        
//        self.constraintLblAddBankInfoBarBottomSpace.constant = 15;
//        self.constraintLblDescriptionBarBottomSpace.constant = 15;
//        
//        self.constraintLblEmailBarBottomSpace.constant = 15;
//        self.constraintLblLocationBarBottomSpace.constant = -8;
//        self.constraintLblPhoneBarBottomSpace.constant = 12;
//        self.constraintLblUsernameBarBottomSpace.constant = 12;
        
        self.consBtnCancelLeading.constant = 0.5f;
        [self.textFieldUsername setFont:[UIFont fontWithName:@"Lato-Regular" size:15]];
        [self.textFieldEmail setFont:[UIFont fontWithName:@"Lato-Regular" size:15]];
        [self.textFieldDescription setFont:[UIFont fontWithName:@"Lato-Regular" size:15]];
        [self.textFieldLocation setFont:[UIFont fontWithName:@"Lato-Regular" size:15]];
        [self.textFieldPhone setFont:[UIFont fontWithName:@"Lato-Regular" size:15]];

        
        [self.btnSave.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:17]];
        [self.btnCancel.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:17]];
        
        [self.lblUploadPhoto setFont:[UIFont fontWithName:@"Lato-Regular" size:14]];
        
        [self.btnAddBankInfo.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:15]];
        [self.btnAddPaymentInfo.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:15]];
        [self.btnVendor.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:15]];
        
        self.userNameTxtFldHeight.constant = 30;
        _textFieldPhone.hidden=NO;
        self.consPhoneTextFldHeight.constant=30;
        

        self.nsConsHelpTop.constant = 9;

        
        
        
        
        self.consUserImgViewTop.constant = 27;
        self.consUserImgViewWidth.constant = 56;
        self.consUserImgViewHeight.constant = 57;
        self.consUserImgViewLeading.constant = 132;
        self.consLblUploadPhotoLeading.constant = 99;
        self.consBtnTransparentUploadPhotoLeading.constant = 100;
        self.consBtnTransparentUploadPhotoTop.constant = 5;
        self.consLblUploadPhotoTop.constant = 9;

        self.constraintSaveBioBtnHeight.constant = 40;
        self.constraintUserNameTextFieldTopSpace.constant=11;
        
        self.consUpperHolderViewHeight.constant=142;
        
        
        _consSaveBtnBottom.constant=59;
        _consCancelBtnBottom.constant=59;
        
        //*****
//        
        _line1Top.constant=5;
        _line2Top.constant=5;
        _line3Top.constant=4;
        _line4Top.constant=4;
        _line5Top.constant=7;
        _line6Top.constant=9;
        _line7Top.constant=9;
        
        
        _locationTxtFldTop.constant=6;
        _instagramTxtFldTop.constant=3;
        _aboutMeTop.constant=6;
        
        
        _phoneTxtFldTop.constant=4;
        
        _addPaymntTop.constant=11;
        _addBankTop.constant=8;
        
        
        
      
        self.consLblUserUnderLineLeading.constant = 18;
        self.consLblUserUnderLineTrailing.constant = 18;
        //self.vendorLineLblLeft.constant = 95;
        self.consBtnVendorLeading.constant = 116;
        self.vendorLineLblWidth.constant = 86;
        self.consUserNameTxtFldLeading.constant = 21;
    //    self.consUserNameTxtFldTrailing.constant = 18;
 
//        _vendorBtnBottomConstant.constant=12;
//        _vendorLineBottom.constant=16;
//        self.consUnderButtonsLineTop.constant = 16;
        
        _vendorBtnBottomConstant.constant= 22;//30;
        _vendorLineBottom.constant=24;//32;
        self.consUnderButtonsLineTop.constant = 24;//32;
        
        _vendorLineLblLeft.constant=101;
        _btnVendorHeight.constant=30;
        
        //_ConsGreyLineTop.constant=451;
   
        self.consUnderButtonsLineHeight.constant = 40;
        
        
        
        self.btnAddBankInfo.imageEdgeInsets = UIEdgeInsetsMake(7, (self.btnAddBankInfo.frame.size.width - [self.btnAddBankInfo imageForState:UIControlStateNormal].size.width+13), 7, -5);

        self.btnAddPaymentInfo.imageEdgeInsets = UIEdgeInsetsMake(7, (self.btnAddPaymentInfo.frame.size.width - [self.btnAddPaymentInfo imageForState:UIControlStateNormal].size.width+13), 7, -5);
      
            self.imgViewUsersPicture.layer.cornerRadius = self.imgViewUsersPicture.frame.size.height/2;
            self.imgViewUsersPicture.layer.masksToBounds = YES;
        
        
        _consTermsAndPolicyViewBottom.constant=100;
        _consTermsAndPolicyWebViewHeight.constant=312;
        _btnAcceptHeight.constant=40;
        _btnAcceptWidth.constant=144.5;
        _consLineLblHeight.constant=40;
        
        
        [_btn_Accept.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:17]];
        [_btn_Cancel.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:17]];
        
        self.btnAddStripeDetail.imageEdgeInsets = UIEdgeInsetsMake(2,270,2,2);
        [self.btnAddStripeDetail.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:15]];
        
        self.nsconsAddStripeDetailUnderLineBottom.constant = 30;

    }
    if (IS_IPHONE_6) {
        
        self.nsConsHelpTop.constant = 14;
        self.nsConsHelpLeading.constant = 10;
        
        
        
        _consGreyLineLbl.backgroundColor = [UIColor clearColor];
        _consGreyLineLbl.hidden=YES;
        self.consBtnCancelLeading.constant = 0.5f;
        
        self.userNameTxtFldHeight.constant = 30;
        self.consPhoneTextFldHeight.constant=30;
        _textFieldPhone.hidden=NO;
//        
//        
//        
//        
        self.constraintUserNameTextFieldTopSpace.constant=31;
        
         self.constraintSaveBioBtnHeight.constant = 45;
        
        self.consUnderButtonsLineHeight.constant = 45;
       // self.btnAddBankInfo.hidden = false;
       // self.cashOutBtn.hidden = true;
  //       self.consUnderButtonsLineTop.constant = 20;
 //       _vendorBtnBottomConstant.constant=26;
 //       _vendorLineBottom.constant=20; //old constrtaints before hide bank info brn by vs 16/8/18.
        _vendorBtnBottomConstant.constant = 40;
        _vendorLineBottom.constant=32;
        self.consUnderButtonsLineTop.constant = 32;

        self.consBtnVendorLeading.constant = 125;
        self.vendorLineLblLeft.constant = 121;
        self.vendorLineLblWidth.constant = 100;
        
        _line1Top.constant=8;
        _line2Top.constant=8;
        _line3Top.constant=6;
        _line4Top.constant=8;
//        _line5Top.constant=6;
//        _line6Top.constant=13;
//        _line7Top.constant=14;
//
//        _locationTxtFldTop.constant=13;
//        _instagramTxtFldTop.constant=11;
//        _aboutMeTop.constant=-8;
//        
//        
//        
        _addPaymntTop.constant=14;
        _addBankTop.constant=13;
       
        self.btnAddBankInfo.imageEdgeInsets = UIEdgeInsetsMake(7, (self.btnAddBankInfo.frame.size.width - [self.btnAddBankInfo imageForState:UIControlStateNormal].size.width+7), 5, -2);
        self.btnAddPaymentInfo.imageEdgeInsets = UIEdgeInsetsMake(8, (self.btnAddPaymentInfo.frame.size.width - [self.btnAddPaymentInfo imageForState:UIControlStateNormal].size.width+7), 4, -2);
        
        
        
        _consTermsAndPolicyViewBottom.constant=100;
        _consTermsAndPolicyWebViewHeight.constant=405;
        _btnAcceptHeight.constant=45;
        [_btn_Accept.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:19]];
        [_btn_Cancel.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:19]];

        _btnAcceptWidth.constant=172;
        _consLineLblHeight.constant=45;

        self.nsconsAddStripeDetailUnderLineBottom.constant = 24;

    }
    if (IS_IPHONE_6P) {
        
        
        
        [self.lblChooseOptionPayment setFont:[UIFont fontWithName:@"Lato-Regular" size:15]];
        [self.lblOptionPayPal setFont:[UIFont fontWithName:@"Lato-Regular" size:18]];
        [self.lblOptionStripe setFont:[UIFont fontWithName:@"Lato-Regular" size:18]];
        _consGreyLineLbl.backgroundColor = [UIColor clearColor];
        self.consPaymentOPtionViewHeight.constant = 85;
        self.consRadioBtnFirstTop.constant = 42;
        self.consLblStripeTop.constant = 13;
        self.consRadioBtnSecondTop.constant = 42;
        self.consLblPaypalTop.constant = 13;
        self.consLblChooseOptionLeading.constant = 49;
//        self.consLblPaypalTrailing.constant = 15;
//        self.consRadioBtnSecondTrailing.constant = 5;
//        self.consLblStripeLeading.constant = 6;
//        self.consRadioBtnFirstLeading.constant = 24;
        
        
        
        
        self.nsConsHelpTop.constant = 13;
        self.nsConsHelpLeading.constant = 11;
        
        _alertViewTop.constant=230;
        _alertViewLeft.constant=88;
        
        _consGreyLineLbl.backgroundColor = [UIColor clearColor];
        _consGreyLineLbl.hidden=YES;
//        self.constraintLblAddBankInfoBarBottomSpace.constant = 25;
//        self.constraintLblDescriptionBarBottomSpace.constant = 25;
//        self.constraintLblEmailBarBottomSpace.constant = 25;
//        self.constraintLblLocationBarBottomSpace.constant = -8;
//        self.constraintLblPhoneBarBottomSpace.constant = 25;
//        self.constraintLblUsernameBarBottomSpace.constant = 25;
      //  self.constraintSaveBioBtnHeight.constant = 78;
        [self.textFieldUsername setFont:[UIFont fontWithName:@"Lato-Regular" size:19]];
        [self.textFieldEmail setFont:[UIFont fontWithName:@"Lato-Regular" size:19]];
        [self.textFieldDescription setFont:[UIFont fontWithName:@"Lato-Regular" size:19]];
        [self.textFieldLocation setFont:[UIFont fontWithName:@"Lato-Regular" size:19]];
        [self.textFieldPhone setFont:[UIFont fontWithName:@"Lato-Regular" size:19]];


        [self.btnSave.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:21]];
        [self.btnCancel.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:21]];
        
        [self.lblUploadPhoto setFont:[UIFont fontWithName:@"Lato-Regular" size:19]];
        
        [self.btnAddBankInfo.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:19]];
        [self.btnAddPaymentInfo.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:19]];
        [self.btnVendor.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:19]];
      //  [self.btnSave setTitleEdgeInsets:UIEdgeInsetsMake(10.0f, 10.0f, 0.0f, 0.0f)];
       // [self.btnCancel setTitleEdgeInsets:UIEdgeInsetsMake(10.0f, 10.0f, 0.0f, 0.0f)];
       
        self.consUpperHolderViewHeight.constant=170;
        self.userNameTxtFldHeight.constant = 30;
        self.constraintUserNameTextFieldTopSpace.constant=33;
        _textFieldPhone.hidden=NO;
        self.consPhoneTextFldHeight.constant=30;

        
        self.consUserImgViewTop.constant = 36;
        self.consUserImgViewWidth.constant = 74;
        self.consUserImgViewHeight.constant = 75;
        self.consUserImgViewLeading.constant = 171;
        self.consLblUploadPhotoLeading.constant = 147;
        self.consBtnTransparentUploadPhotoLeading.constant = 147;
        self.consBtnTransparentUploadPhotoTop.constant = 28;
        self.consLblUploadPhotoTop.constant = 19;
        
        _vendorLineLblLeft.constant=133;
      //  _vendorBtnBottomConstant.constant=29;
      //  _vendorLineBottom.constant=22;
      //    self.consUnderButtonsLineTop.constant = 22;
        
        _vendorBtnBottomConstant.constant=40;//46;
        _vendorLineBottom.constant=31;//37;
        self.consUnderButtonsLineTop.constant = 31;//37;
        
        self.vendorLineLblLeft.constant = 131;
        self.consBtnVendorLeading.constant = 149;
        self.vendorLineLblWidth.constant = 111;
        self.consUserNameTxtFldLeading.constant = 26;

        _locationTxtFldTop.constant=10;
        _instagramTxtFldTop.constant=10;
        _aboutMeTop.constant=10;
        _phoneTxtFldTop.constant=10;
        
        
        
        _addPaymntTop.constant=19;
        _addBankTop.constant=16;
//        _ConsGreyLineTop.constant=614;
        
//        _line1Top.constant=12;
//        _line2Top.constant=10;
//        _line3Top.constant=8;
//        //   _line4Top.constant=10;
//        _line5Top.constant=8;
//        _line6Top.constant=14;
//        _line7Top.constant=17;
        

        
        
        self.constraintSaveBioBtnHeight.constant = 50;

        self.consUnderButtonsLineHeight.constant = 53;
      

        _consSaveBtnBottom.constant=76;
        _consCancelBtnBottom.constant=76;
        
        
        self.btnAddBankInfo.imageEdgeInsets = UIEdgeInsetsMake(4, (self.btnAddBankInfo.frame.size.width - [self.btnAddBankInfo imageForState:UIControlStateNormal].size.width-3), 4, -4);
        
        self.btnAddPaymentInfo.imageEdgeInsets = UIEdgeInsetsMake(4, (self.btnAddPaymentInfo.frame.size.width - [self.btnAddPaymentInfo imageForState:UIControlStateNormal].size.width-2), 3, -4);
        
        self.imgViewUsersPicture.layer.cornerRadius = self.imgViewUsersPicture.frame.size.height/2;
        self.imgViewUsersPicture.layer.masksToBounds = YES;

        
        _consTermsAndPolicyViewBottom.constant=100;
        _consTermsAndPolicyWebViewHeight.constant=460;
        _btnAcceptHeight.constant=50;
        _btnAcceptWidth.constant=193;
        
        _btnAcceptWidth.constant=191.5;
        _consLineLblHeight.constant=50;

        
        [_btn_Accept.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:21]];
        [_btn_Cancel.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:21]];
        
//         self.btnAddStripeDetail.imageEdgeInsets = UIEdgeInsetsMake(0, (self.btnAddStripeDetail.frame.size.width - [self.btnAddStripeDetail imageForState:UIControlStateNormal].size.width-2), 0, 8);
        
        self.btnAddStripeDetail.imageEdgeInsets = UIEdgeInsetsMake(0,358,0,3);
         [self.btnAddStripeDetail.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:19]];

        self.nsconsAddStripeDetailUnderLineBottom.constant = 33;

    }
    
    
  
    CGRect frameRect = self.textFieldLocation.frame;
    frameRect.size.height = 0;
    self.textFieldLocation.frame = frameRect;
    self.instagramTxtFldTop.constant = 0;
    self.textFieldLocation.hidden = true;
   // self.textFieldLocation.backgroundColor = [UIColor greenColor];
    self.line3Top.constant = -36;
   // self.viewHolder.backgroundColor = [UIColor grayColor];
    [self.view layoutIfNeeded];
    
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - create accessory view
-(void)createInputAccessoryView{
    // Create the view that will play the part of the input accessory view.
    // Note that the frame width (third value in the CGRectMake method)
    // should change accordingly in landscape orientation. But we don’t care
    // about that now.
    inputAccView = [[UIView alloc] initWithFrame:CGRectMake(10.0, 0.0, self.view.frame.size.width, 40.0)];
    // Set the view’s background color. We’ ll set it here to gray. Use any color you want.
    [inputAccView setBackgroundColor:[UIColor lightGrayColor]];
    // We can play a little with transparency as well using the Alpha property. Normally
    // you can leave it unchanged.
    [inputAccView setAlpha: 0.8];
    // If you want you may set or change more properties (ex. Font, background image,e.t.c.).
    // For now, what we’ ve already done is just enough.
    // Let’s create our buttons now. First the previous button.
    btnPrev = [UIButton buttonWithType: UIButtonTypeCustom];
    // Set the button’ s frame. We will set their widths to 80px and height to 40px.
    [btnPrev setFrame: CGRectMake(0.0, 0.0, 80.0, 40.0)];
    // Title.
    [btnPrev setTitle: @"Previous" forState: UIControlStateNormal];
    // Background color.
    [btnPrev setBackgroundColor: [UIColor clearColor]];
    // You can set more properties if you need to.
    // With the following command we’ ll make the button to react in finger tapping. Note that the
    // gotoPrevTextfield method that is referred to the @selector is not yet created. We’ ll create it
    // (as well as the methods for the rest of our buttons) later.
    [btnPrev addTarget: self action: @selector(gotoPrevTextfield) forControlEvents: UIControlEventTouchUpInside];
    // Do the same for the two buttons left.
    btnNext = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnNext setFrame:CGRectMake(85.0f, 0.0f, 80.0f, 40.0f)];
    [btnNext setTitle:@"Next" forState:UIControlStateNormal];
    [btnNext setBackgroundColor:[UIColor clearColor]];
    [btnNext addTarget:self action:@selector(gotoNextTextfield) forControlEvents:UIControlEventTouchUpInside];
    btnDone = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnDone setFrame:CGRectMake(self.view.frame.size.width - 80.0f, 0.0f, 80.0f, 40.0f)];
    [btnDone setTitle:@"Done" forState:UIControlStateNormal];
    [btnDone setBackgroundColor:[UIColor clearColor]];
    [btnDone setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [btnDone addTarget:self action:@selector(doneTyping) forControlEvents:UIControlEventTouchUpInside];
    // Now that our buttons are ready we just have to add them to our view.
    [inputAccView addSubview:btnPrev];
    [inputAccView addSubview:btnNext];
    [inputAccView addSubview:btnDone];
}


-(void)gotoPrevTextfield{
    // If the active textfield is the first one, can't go to any previous
    // field so just return.
    if (txtActiveField == self.textFieldUsername) {
        [txtActiveField resignFirstResponder];
        return;
    }
    if (txtActiveField == self.textFieldEmail) {
        
        CGRect frame = self.viewHolder.frame;
        frame.origin.y = 0;
        
        [UIView animateWithDuration:0.3 animations:^{
            self.viewHolder.frame = frame;
        } completion:^(BOOL finished) {
            
        }];
        
        [self.textFieldEmail resignFirstResponder];
        [self.textFieldUsername becomeFirstResponder];
        return;
    }
    if (txtActiveField == self.textFieldLocation) {
        
//        CGRect frame = self.viewHolder.frame;
//        frame.origin.y = -50;
//
//        [UIView animateWithDuration:0.3 animations:^{
//            self.viewHolder.frame = frame;
//        } completion:^(BOOL finished) {
//
//        }];
//
//        [self.textFieldLocation resignFirstResponder];
//        [self.textFieldEmail becomeFirstResponder];
//        return;
    }
    if (txtActiveField == self.textFieldPhone) {
        
        CGRect frame = self.viewHolder.frame;
        frame.origin.y = -80;
        
        [UIView animateWithDuration:0.3 animations:^{
            self.viewHolder.frame = frame;
        } completion:^(BOOL finished) {
            
        }];
        
        [self.textFieldPhone resignFirstResponder];
        [self.textFieldEmail becomeFirstResponder];
        return;
    }
    
    
    if (txtActiveField == self.textFieldDescription) {
        
        
        CGRect frame = self.viewHolder.frame;
        frame.origin.y = -80;
        
        [UIView animateWithDuration:0.3 animations:^{
            self.viewHolder.frame = frame;
        } completion:^(BOOL finished) {
            
        }];

        
//        
//        CGRect frame = self.viewHolder.frame;
//        frame.origin.y = -110;
//        
//        [UIView animateWithDuration:0.3 animations:^{
//            self.viewHolder.frame = frame;
//        } completion:^(BOOL finished) {
//            
//        }];
        
        [self.textFieldDescription resignFirstResponder];
        [self.textFieldPhone becomeFirstResponder];
        return;
    }
    
}

-(void)gotoNextTextfield{
    // If the active textfield is the second one, there is no next so just return.
    if (txtActiveField == self.textFieldUsername) {
        
//        CGRect frame1 = self.upperHolderView.frame;
//        frame1.origin.y = -50;
        
        CGRect frame = self.viewHolder.frame;
        frame.origin.y = -25;

        
        [UIView animateWithDuration:0.3 animations:^{
//             self.upperHolderView.frame = frame1;
           
//            [self.view layoutIfNeeded];
//            self.consUpperHolderViewTop.constant = -60;
//            [self.view layoutIfNeeded];
            
            self.viewHolder.frame = frame;
           
        } completion:^(BOOL finished) {
            
        }];
        
        [self.textFieldUsername resignFirstResponder];
        [self.textFieldEmail becomeFirstResponder];
        return;
    }
    if (txtActiveField == self.textFieldEmail)
    
    {
        
//
//        if(IS_IPHONE_4_OR_LESS)
//        {
//            CGRect frame1 = self.upperHolderView.frame;
//            frame1.origin.y =-100;
//
//            CGRect frame = self.viewHolder.frame;
//            frame.origin.y = -100;
//
//            [UIView animateWithDuration:0.3 animations:^{
//                self.viewHolder.frame = frame;
//
//            } completion:^(BOOL finished) {
//
//            }];
//
//        }
//
//        else
//        {
//            CGRect frame1 = self.upperHolderView.frame;
//            frame1.origin.y = -80;
//
//            CGRect frame = self.viewHolder.frame;
//            frame.origin.y = -80;
//
//
//            [UIView animateWithDuration:0.3 animations:^{
//                self.viewHolder.frame = frame;
//
//            } completion:^(BOOL finished) {
//
//            }];
//
//        }
//
//
//
//
//
//
//
//
//        [self.textFieldEmail resignFirstResponder];
//        [self.textFieldLocation becomeFirstResponder];
//        return;
    }
    if (txtActiveField == self.textFieldEmail) {
        
        CGRect frame1 = self.upperHolderView.frame;
        frame1.origin.y = -60;//-110;
        
        CGRect frame = self.viewHolder.frame;
        frame.origin.y = -60;//-110;
 
        
        
        [UIView animateWithDuration:0.3 animations:^{
            
//            [self.view layoutIfNeeded];
//            self.consUpperHolderViewTop.constant = -110;
//            [self.view layoutIfNeeded];
//            

            
           // self.upperHolderView.frame = frame1;
            
            self.viewHolder.frame = frame;
            
        } completion:^(BOOL finished) {
            
        }];
        
        [self.textFieldLocation resignFirstResponder];
        [self.textFieldPhone becomeFirstResponder];
        return;
    }
    if (txtActiveField == self.textFieldPhone) {
        
        CGRect frame = self.viewHolder.frame;
        frame.origin.y = -110;
        
        [UIView animateWithDuration:0.3 animations:^{
            self.viewHolder.frame = frame;
        } completion:^(BOOL finished) {
            
        }];
        
        [self.textFieldPhone resignFirstResponder];
        [self.textFieldDescription becomeFirstResponder];
        return;
    }
    if (txtActiveField == self.textFieldDescription) {
        
        CGRect frame1 = self.upperHolderView.frame;
        frame1.origin.y = 0;
        
        CGRect frame = self.viewHolder.frame;
        frame.origin.y = 0;
           [UIView animateWithDuration:0.3 animations:^{
           //  self.upperHolderView.frame = frame1;
//               
//               [self.view layoutIfNeeded];
//               self.consUpperHolderViewTop.constant = 0;
//               [self.view layoutIfNeeded];
               

            self.viewHolder.frame = frame;
           
        } completion:^(BOOL finished) {
            
        }];
        
        
        [txtActiveField resignFirstResponder];
        return;
    }
    
}

-(void)doneTyping{
    // When the "done" button is tapped, the keyboard should go away.
    // That simply means that we just have to resign our first responder.
    
    CGRect frame = self.viewHolder.frame;
    frame.origin.y = 0;
    
    [UIView animateWithDuration:0.3 animations:^{
        
//        [self.view layoutIfNeeded];
//        self.consUpperHolderViewTop.constant = 0;
//        [self.view layoutIfNeeded];
//
        
        self.viewHolder.frame = frame;
        
    } completion:^(BOOL finished) {
        
    }];
    
    [txtActiveField resignFirstResponder];
}


#pragma mark - text field delegate
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    CGRect frame = self.viewHolder.frame;
    frame.origin.y = 0;
    
    [UIView animateWithDuration:0.3 animations:^{
        self.viewHolder.frame = frame;
    } completion:^(BOOL finished) {
        
    }];
    
    [textField resignFirstResponder];
    
    return YES;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    
    [self createInputAccessoryView];
    // Now add the view as an input accessory view to the selected textfield.
    [textField setInputAccessoryView:inputAccView];
    // Set the active field. We' ll need that if we want to move properly
    // between our textfields.
    txtActiveField = textField;
    
    if (txtActiveField == self.textFieldUsername) {
        
    }
    if (txtActiveField == self.textFieldEmail)    //location
    {
        
        if(IS_IPHONE_4_OR_LESS)
        {
            CGRect frame1 = self.upperHolderView.frame;
            frame1.origin.y = 0;
            
            
            CGRect frame = self.viewHolder.frame;
            frame.origin.y = -25;
            
            [UIView animateWithDuration:0.3 animations:^{
                //  self.upperHolderView.frame = frame1;
                self.viewHolder.frame = frame;
            } completion:^(BOOL finished) {
                
            }];

        }
        
        else
        {
            CGRect frame1 = self.upperHolderView.frame;
            frame1.origin.y = -5;
            
            
            CGRect frame = self.viewHolder.frame;
            frame.origin.y = -25;
            
            [UIView animateWithDuration:0.3 animations:^{
                //  self.upperHolderView.frame = frame1;
                self.viewHolder.frame = frame;
            } completion:^(BOOL finished) {
                
            }];

        }
        
        
        
        
        
    }
    if (txtActiveField == self.textFieldLocation)  //instagram
    {
        
        
//        if(IS_IPHONE_4_OR_LESS)
//        {
//            CGRect frame1 = self.upperHolderView.frame;
//            frame1.origin.y = -20;
//
//            CGRect frame = self.viewHolder.frame;
//            frame.origin.y = -50;
//
//            [UIView animateWithDuration:0.3 animations:^{
//
//                //  self.upperHolderView.frame = frame1;
//                self.viewHolder.frame = frame;
//
//            } completion:^(BOOL finished) {
//
//            }];
//
//        }
//
//        else
//        {
//            CGRect frame1 = self.upperHolderView.frame;
//            frame1.origin.y = -15;
//
//            CGRect frame = self.viewHolder.frame;
//            frame.origin.y = -80;
//
//            [UIView animateWithDuration:0.3 animations:^{
//
//                //  self.upperHolderView.frame = frame1;
//                self.viewHolder.frame = frame;
//
//            } completion:^(BOOL finished) {
//
//            }];
//
//        }
//
//
//
//
    }
    if (txtActiveField == self.textFieldPhone)
    {
     
        
        if(IS_IPHONE_4_OR_LESS)
        {
            CGRect frame = self.viewHolder.frame;
            frame.origin.y = -80;
            
            [UIView animateWithDuration:0.3 animations:^{
                self.viewHolder.frame = frame;
            } completion:^(BOOL finished) {
                
            }];

        }
        
        else
        {
            CGRect frame = self.viewHolder.frame;
            frame.origin.y = -60;
            
            [UIView animateWithDuration:0.3 animations:^{
                self.viewHolder.frame = frame;
            } completion:^(BOOL finished) {
                
            }];

            
        }
        
        
        
        
 }
    
    if (txtActiveField == self.textFieldDescription) {
        
        
        if(IS_IPHONE_4_OR_LESS)
        {
            
            _textFieldDescription.autocorrectionType = UITextAutocorrectionTypeNo;
            CGRect frame1 = self.upperHolderView.frame;
            frame1.origin.y = 0;
            
            
            CGRect frame = self.viewHolder.frame;
            frame.origin.y = -110;
            
            [UIView animateWithDuration:0.3 animations:^{
                //  self.upperHolderView.frame = frame1;
                self.viewHolder.frame = frame;
            } completion:^(BOOL finished) {
                
            }];
  
        }
        
        else
        {
            CGRect frame1 = self.upperHolderView.frame;
            
            frame1.origin.y = -15;
            
            
            CGRect frame = self.viewHolder.frame;
            frame.origin.y = -110;
            
            [UIView animateWithDuration:0.3 animations:^{
                //  self.upperHolderView.frame = frame1;
                self.viewHolder.frame = frame;
            } completion:^(BOOL finished) {
                
            }];
            
        }
        
        
    }
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    
}


- (IBAction)uploadPicture:(id)sender {
    
    [txtActiveField resignFirstResponder];
    
    [self Actionperform];
    
}

-(void)goToBankScreen
{
    
    [self getBankInfoClicked:btnAddBankInfo];
    
    return;
    
    myAppDelegate.isSideBarAccesible = false;
    
    myAppDelegate.vcObj.btnSideBarOpen.hidden = YES;
    
    myAppDelegate.vcObj.btnBack.hidden = NO;
    
    
//    myAppDelegate.TandC_Buyee = @"SetUpBankViewController";
//
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    SetUpBankViewController *objSetUpBank= (SetUpBankViewController *)[storyboard instantiateViewControllerWithIdentifier:@"SetUpBankViewController"];
//
    
    [myAppDelegate.NavController pushViewController:objSetUpBank animated:YES];
    
   // [self performSegueWithIdentifier:@"BankInfoFromRoot" sender:nil];//myAppDelegate.vcObj.btnBankInfo

    
  //  [self shouldPerformSegueWithIdentifier:@"setupbank" sender:self];
    
  //  [self prepareForSegue:<#(nonnull UIStoryboardSegue *)#> sender:<#(nullable id)#>]
    
     //[self shouldPerformSegueWithIdentifier:@"setupbank" sender:nil];
    [self.mainUpperHolderVIew setHidden:NO];

    UIStoryboardSegue* aSegue = [[UIStoryboardSegue alloc] initWithIdentifier:@"setupbank" source:self destination:objSetUpBank];
    [self prepareForSegue:aSegue sender:btnAddBankInfo];
   // [aSegue perform];
}


-(void)showActionSheet
{
    UIAlertController * view = [UIAlertController
                                alertControllerWithTitle:@"BUY TIMEE "
                                message:@"How do you want to use PayPal?"
                                preferredStyle:UIAlertControllerStyleActionSheet];
    
    
    UIAlertAction* paymentInfo = [UIAlertAction
                             actionWithTitle:@"Pay with PayPal"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 
                                 [GlobalFunction addIndicatorView];
                                 
                                 [self startCheckout];
                                 //Do some thing here
                                 [view dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
    UIAlertAction* bankInfo = [UIAlertAction
                              actionWithTitle:@"Cashout with PayPal"
                              style:UIAlertActionStyleDefault
                              handler:^(UIAlertAction * action)
                              {
                                  NSLog(@"Add paypal id select");
                                  
                                 
                                  [view dismissViewControllerAnimated:NO completion:nil];

                                  
                                //  [self getBankInfoClicked:nil];
                                  [self performSegueWithIdentifier:@"cashOutPayPal" sender:self];

                  // [self performSelector:@selector(goToBankScreen) withObject:nil afterDelay:0.5];
                                  
                              }];
    UIAlertAction* cancel = [UIAlertAction
                              actionWithTitle:@"Cancel"
                              style:UIAlertActionStyleCancel
                              handler:^(UIAlertAction * action)
                              {
                                 // [view dismissViewControllerAnimated:YES completion:nil];
                                  
                              }];
   
    CGFloat left = 0.0;
    
    
    UIImage *topoImage = [UIImage imageNamed:@"rightCheckRecovered.png"];
    CGFloat alertViewPadding = 22.0;
if (IS_IPHONE_6)
{
    left = -view.view.frame.size.width/2 - topoImage.size.width - alertViewPadding-60;
    
}
if (IS_IPHONE_6P)
{
    left = -view.view.frame.size.width/2 - topoImage.size.width - alertViewPadding-76;
        
}
if (IS_IPHONE_5)
{
    left = -view.view.frame.size.width/2 - topoImage.size.width - alertViewPadding-40;
        
}
if (IS_IPHONE_4_OR_LESS)
{
    left = -view.view.frame.size.width/2 - topoImage.size.width - alertViewPadding-40;
    
}
    UIImage *centeredTopoImage = [[topoImage imageWithAlignmentRectInsets:UIEdgeInsetsMake(0,left,0,0) ]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
  //  [paymentInfo setValue:centeredTopoImage forKey:@"image"];
    
    UIColor *color = [UIColor colorWithRed:63.0/255.0f green:67.0/255.0f blue:70.0/255.0f alpha:1.0]; // select needed color
    NSString *string = @"Buy Timee"; // the string to colorize
    NSDictionary *attrs = @{ NSForegroundColorAttributeName : color };
    NSAttributedString *attrStr = [[NSAttributedString alloc] initWithString:string attributes:attrs];

//    NSMutableAttributedString *hogan = [[NSMutableAttributedString alloc] initWithString:@"BuyTimee"];
//    [hogan addAttribute:NSForegroundColorAttributeName
//                  value:[UIColor colorWithRed:143.0/255.0f green:143.0/255.0f blue:143.0/255.0f alpha:1.0]];
    
    [view setValue:attrStr forKey:@"attributedTitle"];
    
    if ([myAppDelegate.selectedPaymentTypeFromServer isEqualToString:@"2"])
    {
       
            if (myAppDelegate.ispayWithPayPallFilled == true)
            {
                [paymentInfo setValue:centeredTopoImage forKey:@"image"];
        
            }
            if (myAppDelegate.iscashOutPayPallFilled == true)
            {
                [bankInfo setValue:centeredTopoImage forKey:@"image"];
        
            }
        
        
        
//        if (myAppDelegate.isPaymentFilled == true)
//        {
//            [paymentInfo setValue:centeredTopoImage forKey:@"image"];
//
//        }
//        if (myAppDelegate.isBankDetailFilled == true)
//        {
//            [bankInfo setValue:centeredTopoImage forKey:@"image"];
//
//        }
        
    }


    
    
    
    
    
    [view addAction:paymentInfo];
    [view addAction:bankInfo];
    [view addAction:cancel];
    [self presentViewController:view animated:YES completion:nil];
}

-(void)showActionSheetForBank
{
    UIAlertController * view = [UIAlertController
                                alertControllerWithTitle:@"BUY TIMEE "
                                message:@"How do you want to use Stripe?"
                                preferredStyle:UIAlertControllerStyleActionSheet];
    
    
    UIAlertAction* paymentInfo = [UIAlertAction
                                  actionWithTitle:@"Add your card details"
                                  style:UIAlertActionStyleDefault
                                  handler:^(UIAlertAction * action)
                                  {
                                      
                                    
                                      [view dismissViewControllerAnimated:YES completion:nil];
                                      
                                      myAppDelegate.isFromEdit = false;
                                      
                                      [myAppDelegate.vcObj performSegueWithIdentifier:@"paymentInfo" sender:myAppDelegate.vcObj.btnPaymentInfo];
                                      
                                      myAppDelegate.isSideBarAccesible = false;
                                      
                                  }];
    UIAlertAction* bankInfo = [UIAlertAction
                               actionWithTitle:@"Add your bank details"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   NSLog(@"Add paypal id select");
                                   
                                   
                                   [view dismissViewControllerAnimated:NO completion:nil];
                                   
                                   
                                   //  [self getBankInfoClicked:nil];
                                  // isFromPaymentOption = false;
                                   [self performSegueWithIdentifier:@"setupbank" sender:self];
                                   
                                   // [self performSelector:@selector(goToBankScreen) withObject:nil afterDelay:0.5];
                                   
                               }];
    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:@"Cancel"
                             style:UIAlertActionStyleCancel
                             handler:^(UIAlertAction * action)
                             {
                                 // [view dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
    
    CGFloat left = 0.0;
    
    
    UIImage *topoImage = [UIImage imageNamed:@"rightCheckRecovered.png"];
    CGFloat alertViewPadding = 22.0;
    if (IS_IPHONE_6)
    {
        left = -view.view.frame.size.width/2 - topoImage.size.width - alertViewPadding-60;
        
    }
    if (IS_IPHONE_6P)
    {
        left = -view.view.frame.size.width/2 - topoImage.size.width - alertViewPadding-76;
        
    }
    if (IS_IPHONE_5)
    {
        left = -view.view.frame.size.width/2 - topoImage.size.width - alertViewPadding-40;
        
    }
    if (IS_IPHONE_4_OR_LESS)
    {
        left = -view.view.frame.size.width/2 - topoImage.size.width - alertViewPadding-40;
        
    }
    UIImage *centeredTopoImage = [[topoImage imageWithAlignmentRectInsets:UIEdgeInsetsMake(0,left,0,0) ]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    //  [paymentInfo setValue:centeredTopoImage forKey:@"image"];
    
    UIColor *color = [UIColor colorWithRed:63.0/255.0f green:67.0/255.0f blue:70.0/255.0f alpha:1.0]; // select needed color
    NSString *string = @"Buy Timee"; // the string to colorize
    NSDictionary *attrs = @{ NSForegroundColorAttributeName : color };
    NSAttributedString *attrStr = [[NSAttributedString alloc] initWithString:string attributes:attrs];
    
    //    NSMutableAttributedString *hogan = [[NSMutableAttributedString alloc] initWithString:@"BuyTimee"];
    //    [hogan addAttribute:NSForegroundColorAttributeName
    //                  value:[UIColor colorWithRed:143.0/255.0f green:143.0/255.0f blue:143.0/255.0f alpha:1.0]];
    
    [view setValue:attrStr forKey:@"attributedTitle"];
    
    
    if ([myAppDelegate.selectedPaymentTypeFromServer isEqualToString:@"1"])
    {
        
            if (myAppDelegate.isStripeBankInfoFilled == true)
            {
                [bankInfo setValue:centeredTopoImage forKey:@"image"];

            }
            if (myAppDelegate.isStripeCardInfoFilled == true)
            {
                [paymentInfo setValue:centeredTopoImage forKey:@"image"];

            }
        
        
//    if (myAppDelegate.isPaymentFilled == true)
//    {
//        [paymentInfo setValue:centeredTopoImage forKey:@"image"];
//
//    }
//    if (myAppDelegate.isBankDetailFilled == true)
//    {
//        [bankInfo setValue:centeredTopoImage forKey:@"image"];
//
//    }
    }
 
    
    
    
    
    
    [view addAction:paymentInfo];
    [view addAction:bankInfo];
    [view addAction:cancel];
    [self presentViewController:view animated:YES completion:nil];
}

- (IBAction)getPaymentInfoClicked:(id)sender {
    
    
//    [self showActionSheet];
//    
//    return;
//    UIActionSheet *popup = [[UIActionSheet alloc] initWithTitle:@"Select Option:" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:
//                            @"Add Payment Info.",
//                            @"Add PayPal Info.", nil];
//
//
// //   [[[popup valueForKey:@"Add Payment Info."] objectAtIndex:0] setImage:[UIImage imageNamed:@"Checked"] forState:UIControlStateNormal];
//
//   // [popup set]
// 
//    popup.tag = 111;
//
//    [popup showInView:[UIApplication sharedApplication].keyWindow];
//    
//    return;
//    
//    [GlobalFunction addIndicatorView];
//
//    [self startCheckout];
//    return;
    
  
    [UIView transitionWithView:self.blackTransparentView
                      duration:0.4
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^{
                        self.paymentOptionView.hidden = NO;
                        self.blackTransparentView.hidden = NO;
                        self.blackTransparentView.alpha = 1.0;
                        self.termsAndPolicyView.hidden = YES;
                        self.insideViewOfBlurView.hidden = YES;
                    }
                    completion:NULL];
    
    return;
    
     [self performSegueWithIdentifier:@"setupbank" sender:self];
    return;
    
    myAppDelegate.isFromEdit = false;
    
    [myAppDelegate.vcObj performSegueWithIdentifier:@"paymentInfo" sender:myAppDelegate.vcObj.btnPaymentInfo];
    
    myAppDelegate.isSideBarAccesible = false;
    
    
    
}

- (IBAction)getBankInfoClicked:(id)sender {
   
   // isFromPaymentOption = true;
    
    if([buttonClicked isEqualToString:@"paypal"])
    {
         [self performSelector:@selector(showActionSheet) withObject:nil afterDelay:0.1];
    }
    else if([buttonClicked isEqualToString:@"bank"])
    {
         [self performSelector:@selector(showActionSheetForBank) withObject:nil afterDelay:0.1];

    }
    
    
    return;
    [self shouldPerformSegueWithIdentifier:@"setupbank" sender:sender];
    return;
    myAppDelegate.isSideBarAccesible = false;
    
    myAppDelegate.vcObj.btnSideBarOpen.hidden = YES;
    
    myAppDelegate.vcObj.btnBack.hidden = NO;
    
  //  myAppDelegate.TandC_Buyee = @"";
    
    myAppDelegate.TandC_Buyee = @"SetUpBankViewController";
  //  myAppDelegate.vcObj.sideBarBackBtnTop.constant=10;

    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    SetUpBankViewController *objSetUpBank= (SetUpBankViewController *)[storyboard instantiateViewControllerWithIdentifier:@"SetUpBankViewController"];
    
    
    [myAppDelegate.NavController pushViewController:objSetUpBank animated:YES];
    
    
    return;
    
    /*
    if (myAppDelegate.isBankInfoFilled) {
        
        UIAlertView *alertDisconnectWithStripe = [[UIAlertView alloc] initWithTitle:@"Buy Timee" message:@"Are you sure, you want to disconnect your stripe account?" delegate:self cancelButtonTitle:@"N0" otherButtonTitles:@"Yes",nil];
        
        alertDisconnectWithStripe.tag = 12001;
        
        [alertDisconnectWithStripe show];
         
         
         
        
    }else{
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://connect.stripe.com/oauth/authorize?response_type=code&client_id=ca_8OAG2bzlaujVom8ENyjTHymERuAJajIT&scope=read_write"]]];
        
    }
     */
    
//        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://connect.stripe.com/oauth/authorize?response_type=code&client_id=ca_8OAG50wWYZgh78OmpC57dZNCiICFJhqm&scope=read_write"]]];    //live  old

    
    
    
  //  [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://connect.stripe.com/oauth/authorize?response_type=code&client_id=ca_8ihA2QrB60eKiyHjBtUE8DNtDf9uOrhY&scope=read_write"]]];    //live
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://connect.stripe.com/oauth/authorize?response_type=code&client_id=ca_8pQgnRmat8ScuAFaSPgLP4PENzWwO4vR&scope=read_write"]]];    // change id given by ishan & edited by vs on 27/12/2016...(johnjdixson stripe acc)
    
    
    
//    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://connect.stripe.com/oauth/authorize?response_type=code&client_id=ca_8OAG2bzlaujVom8ENyjTHymERuAJajIT&scope=read_write"]]];
//    
//    
    
}



- (IBAction)saveBioClicked:(id)sender {
    
    //    if ([[self.textFieldUsername.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""]) {
    //
    //        [[GlobalFunction shared] showAlertForMessage:@"Name field cannot be blank."];
    //
    //        return;
    //
    //    }
//        if ([[self.textFieldEmail.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""]) {
//    
//            [[GlobalFunction shared] showAlertForMessage:@"Location field cannot be blank."];
//    
//            return;
//    
//        }
//    
//    if ([[self.textFieldLocation.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""]) {
//        
//        [[GlobalFunction shared] showAlertForMessage:@"Instagram field cannot be blank."];
//        
//        return;
//        
//    }
//    
//        if ([[self.textFieldDescription.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""]) {
//    
//            [[GlobalFunction shared] showAlertForMessage:@"About me field cannot be blank."];
//    
//            return;
//    
//        }
    
//    if (![GlobalFunction validateEmail:self.textFieldLocation.text]) {
//        
//        [[GlobalFunction shared] showAlertForMessage:@"Please enter valid email."];
//        
//        return;
//        
//    }
    
    if ([[self.textFieldUsername.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""]) {
        
        [GlobalFunction removeIndicatorView];
        
        [[GlobalFunction shared] showAlertForMessage:@"Name cannot be blank."];
        
        return;
        
    }
    if ([[self.textFieldEmail.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""]) {
        
        [GlobalFunction removeIndicatorView];
        
        [[GlobalFunction shared] showAlertForMessage:@"Location cannot be blank."];
        
        return;
        
    }
    if([self.textFieldPhone.text isEqualToString:@""])
    {
        [[GlobalFunction shared] showAlertForMessage:@"Please enter valid contact no."];
        return;
    }
    
//    NSString *phoneNumber = self.textFieldPhone.text;
//    NSString *phoneRegex = @"[123456789][0-9]{9}([0-9]{0})?";
//    NSPredicate *test = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", phoneRegex];
//    BOOL matches = [test evaluateWithObject:phoneNumber];
//
//        if(matches==NO)
//        {
//           [[GlobalFunction shared] showAlertForMessage:@"Please enter valid 10 digit contact no."];
//
//            return;
//        }
    
    
    if(self.textFieldPhone.text.length<=15)
    {
        printf("less than 15 length");
    }
    else
    {
        [[GlobalFunction shared] showAlertForMessage:@"Please enter valid contact no."];
        printf("not less than");
        return;
    }

    
    
    
//    NSCharacterSet *numbersOnly = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
//    NSCharacterSet *characterSetFromTextField = [NSCharacterSet characterSetWithCharactersInString:_textFieldPhone.text];
//
//    BOOL stringIsValid = [numbersOnly isSupersetOfSet:characterSetFromTextField];
//    if(stringIsValid)
//    {
//        if(self.textFieldPhone.text.length<=15)
//        {
//            printf("less than 15 length");
//        }
//        else
//        {
//            [[GlobalFunction shared] showAlertForMessage:@"Please enter valid contact no."];
//            printf("not less than");
//            return;
//        }
//    }
//    else
//    {
//        [[GlobalFunction shared] showAlertForMessage:@"Please enter valid contact no."];
//
//        return;
//
//    }

    
    
    
    [GlobalFunction addIndicatorView];
    [self performSelector:@selector(updateBioToServer) withObject:nil afterDelay:0.5];
    
}


//- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
//{
//
//    double lati = newLocation.coordinate.latitude;
//    _geocoder_latitude_str= [NSString stringWithFormat:@"%.4f",lati];
//    [curent_lat_ary addObject:_geocoder_latitude_str];
//    double longi = newLocation.coordinate.longitude;
//    _geocoder_longitude_str=[NSString stringWithFormat:@"%.4f",longi];
//    [cureent_log_ary addObject:_geocoder_longitude_str];
//    NSUserDefaults *current_lat=[NSUserDefaults standardUserDefaults];
//    [current_lat setObject:_geocoder_latitude_str forKey:@"current_lat"];
//    [current_lat setObject:_geocoder_longitude_str forKey:@"current_long"];
//    [locationManager stopUpdatingLocation];
//
//    CLGeocoder *reverseGeocoder = [[CLGeocoder alloc] init];
//
//    [reverseGeocoder reverseGeocodeLocation:newLocation completionHandler:^(NSArray *placemarks, NSError *error) {
//        // NSLog(@"Received placemarks: %@", placemarks);
//
//        CLPlacemark *myPlacemark = [placemarks objectAtIndex:0];
//        NSString *countryCode = myPlacemark.ISOcountryCode;
//        NSString *countryName = myPlacemark.country;
//        NSString *cityName= myPlacemark.subAdministrativeArea;
//        NSLog(@"My country code: %@ and countryName: %@ MyCity: %@", countryCode, countryName, cityName);
//    }];
//}


#pragma mark - todo image work


-(void)updateBioToServer{
    
    if (![[WebService shared] connected]) {
        
        [GlobalFunction removeIndicatorView];
        
        [[GlobalFunction shared] showAlertForMessage:@"Can't connect to internet, please check and try again."];
        
        return;
    }
    
    CLLocationCoordinate2D coordinate = [[GlobalFunction shared] getLocationFromAddressString:self.textFieldEmail.text];
    
    NSString *imageName1 = [NSString stringWithFormat:@"userProfilePicture_%@_%@.jpeg",myAppDelegate.userData.userid,TimeStamp];
    
   // imageName1 = @"main.jpeg";
    
    NSDictionary *dictBioResponse;
    
//    myAppDelegate.userData.userid;
//    
//    NSString *mainUID = myAppDelegate.userData.userid;
//    
//    if (mainUID.length <= 0) {
//        
//        mainUID = @"1";
//    }
//    
    if (isPictureAvailabe) {
        
        
     //   myAppDelegate.userData.userid
        
//        dictBioResponse = [[WebService shared]updateProfileuserId:myAppDelegate.userData.userid name:self.textFieldUsername.text description:self.textFieldDescription.text latitude:[NSString stringWithFormat:@"%f",coordinate.latitude] longitude:[NSString stringWithFormat:@"%f",coordinate.longitude] andUserImage:self.imgViewUsersPicture.image andImageName:imageName1 instaLink:self.textFieldLocation.text location:self.textFieldEmail.text];

        
        
        dictBioResponse = [[WebService shared]newUpdateProfileuserId:myAppDelegate.userData.userid name:self.textFieldUsername.text description:self.textFieldDescription.text latitude:[NSString stringWithFormat:@"%f",coordinate.latitude] longitude:[NSString stringWithFormat:@"%f",coordinate.longitude] andUserImage:self.imgViewUsersPicture.image andImageName:imageName1 instaLink:@"" location:self.textFieldEmail.text mobile:self.textFieldPhone.text];
        
        
        
        
        
    }else if(isOldPictureAvailable){
        
        
//         dictBioResponse = [[WebService shared]updateProfileuserId:myAppDelegate.userData.userid name:self.textFieldUsername.text description:self.textFieldDescription.text latitude:[NSString stringWithFormat:@"%f",coordinate.latitude] longitude:[NSString stringWithFormat:@"%f",coordinate.longitude] andUserImage:myAppDelegate.userData.userImage andImageName:imageName1 instaLink:self.textFieldLocation.text location:self.textFieldEmail.text];

        
        dictBioResponse = [[WebService  shared]newUpdateProfileuserId:myAppDelegate.userData.userid name:self.textFieldUsername.text description:self.textFieldDescription.text latitude:[NSString stringWithFormat:@"%f",coordinate.latitude] longitude:[NSString stringWithFormat:@"%f",coordinate.longitude] andUserImage:myAppDelegate.userData.userImage andImageName:imageName1 instaLink:@"" location:self.textFieldEmail.text mobile:self.textFieldPhone.text];
        
    }else{
        
        
//        dictBioResponse = [[WebService shared]updateProfileuserId:myAppDelegate.userData.userid name:self.textFieldUsername.text description:self.textFieldDescription.text latitude:[NSString stringWithFormat:@"%f",coordinate.latitude] longitude:[NSString stringWithFormat:@"%f",coordinate.longitude] andUserImage:nil andImageName:@"" instaLink:self.textFieldLocation.text location:self.textFieldEmail.text];

        
        dictBioResponse = [[WebService shared]newUpdateProfileuserId:myAppDelegate.userData.userid name:self.textFieldUsername.text description:self.textFieldDescription.text latitude:[NSString stringWithFormat:@"%f",coordinate.latitude] longitude:[NSString stringWithFormat:@"%f",coordinate.longitude] andUserImage:nil andImageName:@"" instaLink:@"" location:self.textFieldEmail.text mobile:self.textFieldPhone.text];
                
    }
    
    if (dictBioResponse) {
        
        NSLog(@"response --- %@",[EncryptDecryptFile decrypt:[dictBioResponse valueForKey:@"responseMessage"]]);
        
        NSLog(@"response --- %@",[EncryptDecryptFile decrypt:[dictBioResponse valueForKey:@"responseCode"]]);
        
        if ([[EncryptDecryptFile decrypt:[dictBioResponse valueForKey:@"responseCode"]] isEqualToString:@"200"]) {
            
            NSArray *dictDataReg = [dictBioResponse objectForKey:@"data"];
            
            NSDictionary *dictFromResponse = [dictBioResponse objectForKey:@"data"];
            
  
            if (dictDataReg) {
                
                
              // NSDictionary *dictFromResponse = [[NSDictionary alloc]init];
            // NSDictionary *dictFromResponse  = [dictDataReg firstObject];
                
                NSMutableDictionary *dictTemp0 = [[NSMutableDictionary alloc]init];
                
                [dictTemp0 setValue:[dictFromResponse valueForKey:@"userId"] forKey:@"userId"];
                [dictTemp0 setValue:[dictFromResponse valueForKey:@"name"] forKey:@"name"];
                [dictTemp0 setValue:[dictFromResponse valueForKey:@"emailId"] forKey:@"emailId"];
                [dictTemp0 setValue:[dictFromResponse valueForKey:@"instaLink"] forKey:@"instaLink"];
                [dictTemp0 setValue:[dictFromResponse valueForKey:@"location"] forKey:@"location"];
                [dictTemp0 setValue:[dictFromResponse valueForKey:@"description"] forKey:@"description"];
                [dictTemp0 setValue:[dictFromResponse valueForKey:@"picPath"] forKey:@"picPath"];
                [dictTemp0 setValue:[dictFromResponse valueForKey:@"avgRating"] forKey:@"avgRating"];
                [dictTemp0 setValue:[dictFromResponse valueForKey:@"customerId"] forKey:@"customerId"];
                [dictTemp0 setValue:[dictFromResponse valueForKey:@"stripeId"] forKey:@"stripeId"];
                [dictTemp0 setValue:[dictFromResponse valueForKey:@"businessId"] forKey:@"businessId"];
                [dictTemp0 setValue:[dictFromResponse valueForKey:@"mobile"] forKey:@"mobile"];
                [dictTemp0 setValue:[dictFromResponse valueForKey:@"tnc"] forKey:@"tnc"];
                [dictTemp0 setValue:[dictFromResponse valueForKey:@"paymentType"] forKey:@"paymentType"];
                [dictTemp0 setValue:[dictFromResponse valueForKey:@"account"] forKey:@"account"];
                
                NSString *query = [NSString stringWithFormat:@"delete from %@",tableUser];
                [DatabaseClass executeQuery:query];
                
                if (isPictureAvailabe) {
                    
                    [GlobalFunction removeImage:myAppDelegate.userData.userImageName dirname:@"user"];
                    
                    myAppDelegate.userData.userImageName = imageName1;
                    
                    NSArray *components = [myAppDelegate.userData.userImageName componentsSeparatedByString:@"."];
                    
                    imageName1 = [components firstObject];
                    
                    [GlobalFunction saveimage:self.imgViewUsersPicture.image imageName:imageName1 dirname:@"user"];
                    
                    
                }else if (isOldPictureAvailable){
                    
                    
                }else{
                    
                    
                }
                
                query=[NSString stringWithFormat:@"insert into %@ values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",tableUser];
                [DatabaseClass saveUserData:[query UTF8String] fromDict:dictTemp0];
                
                NSMutableArray *arr = [DatabaseClass getDataFromUserData:[[NSString stringWithFormat:@"Select * from %@",tableUser] UTF8String]];
                
                myAppDelegate.userData = nil;
                myAppDelegate.userData = (user *)[arr firstObject];
               // [UserDefaults setObject:arr forKey:@"UserInfoArray"];
                
                NSMutableDictionary *dic=[[NSMutableDictionary alloc] init];
                [dic setValue:myAppDelegate.userData.username forKey:@"username"];
                [dic setValue:myAppDelegate.userData.instaLink forKey:@"instaLink"];
                [dic setValue:myAppDelegate.userData.location forKey:@"location"];
                [dic setValue:myAppDelegate.userData.userDescription forKey:@"aboutme"];
                [dic setValue:myAppDelegate.userData.mobile forKey:@"mobie"];

                NSLog(@"%@",myAppDelegate.userData.mobile);
                               
                
                
//                NSDictionary *userDict = [UserDefaults objectForKey:keyUserloginDetailDictionary];
//                
//                NSDictionary *dictUserData = [NSDictionary dictionaryWithObjects:@[[EncryptDecryptFile EncryptIT:self.textFieldUsername.text],[userDict valueForKey:keyPass],myAppDelegate.userData.userid,[userDict valueForKey:keysocialFlag_fb],[userDict valueForKey:keysocialEmailId_fb],[userDict valueForKey:keysocialId_fb]] forKeys:@[keyUsername,keyPass,keyUserId,keysocialFlag_fb,keysocialEmailId_fb,keysocialId_fb]];
                
                
                [UserDefaults setObject:dic forKey:keyUserloginDetailDictionary];
                [UserDefaults synchronize];
                
                [myAppDelegate.vcObj.tblViewSideBar reloadData];
                
                [self bioUpdatedSuccefully:[EncryptDecryptFile decrypt:[dictBioResponse valueForKey:@"responseMessage"]]];
                
            }else{
                
                [[GlobalFunction shared] showAlertForMessage:[EncryptDecryptFile decrypt:[dictBioResponse valueForKey:@"responseMessage"]]];
                
                [GlobalFunction removeIndicatorView];
                
            }
            
            
        }else{
            
            [[GlobalFunction shared] showAlertForMessage:[EncryptDecryptFile decrypt:[dictBioResponse valueForKey:@"responseMessage"]]];
            
            [GlobalFunction removeIndicatorView];
            
        }
        
    }else{
        
        [[GlobalFunction shared] showAlertForMessage:@"Server is down, please try again later."];
        
        [GlobalFunction removeIndicatorView];
        
    }
    
    
}

-(void)bioUpdatedSuccefully:(NSString *)response{
    
    [GlobalFunction removeIndicatorView];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Buy Timee" message:response delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    
    alert.tag = 10001;
    
    [alert show];
    
}

-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex{
    
    if (alertView.tag == 10001) {
        
        if(buttonIndex == [alertView cancelButtonIndex]){
            
           [self goBack];
            
        }
    }else if (alertView.tag == 3491832){
        
        NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
        [[UIApplication sharedApplication] openURL:url];
        
    }else if (alertView.tag == 12001){
        
        if (buttonIndex == [alertView cancelButtonIndex]) {
            
        }else{
            
            [GlobalFunction addIndicatorView];
            
            [self performSelector:@selector(disconnectStripeAccount) withObject:nil afterDelay:0.5];
            
        }
        
    }
    
}

-(void)disconnectStripeAccount{
    
    NSDictionary *dicts = [webServicesShared removeStripeForUserid:myAppDelegate.userData.userid];
    
    if (dicts) {
        
        NSLog(@"response --- %@",[EncryptDecryptFile decrypt:[dicts valueForKey:@"responseMessage"]]);
        
        if ([[EncryptDecryptFile decrypt:[dicts valueForKey:@"responseCode"]] isEqualToString:@"200"]) {
            
            [DatabaseClass updateStripeIdInDataBase:[EncryptDecryptFile EncryptIT:@""] forUserId:[EncryptDecryptFile EncryptIT:myAppDelegate.userData.userid]];
            
            NSMutableArray *arr = [DatabaseClass getDataFromUserData:[[NSString stringWithFormat:@"Select * from %@",tableUser] UTF8String]];
            
            myAppDelegate.userData = nil;
            myAppDelegate.userData = (user *)[arr firstObject];
            
           // [self.btnAddBankInfo setTitle:@"Add Bank Info." forState:UIControlStateNormal];
            [self.btnAddBankInfo setTitle:@"Add Paypal Info." forState:UIControlStateNormal]; // by vs on 05June18
            
            [GlobalFunction removeIndicatorView];
            
            [[GlobalFunction shared] showAlertForMessage:[EncryptDecryptFile decrypt:[dicts valueForKey:@"responseMessage"]]];
            
        }else{
            
            [GlobalFunction removeIndicatorView];
            
            [[GlobalFunction shared] showAlertForMessage:[EncryptDecryptFile decrypt:[dicts valueForKey:@"responseMessage"]]];
            
        }
        
    }else{
        
        [GlobalFunction removeIndicatorView];
        
        [[GlobalFunction shared] showAlertForMessage:@"Server is down, please try again later."];
        
    }
    
}

-(void)goBack{
    
    TaskTableViewCell *cell = (TaskTableViewCell *)[myAppDelegate.vcObj.tblViewSideBar cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
    
    myAppDelegate.isSideBarAccesible = true;
    
    [myAppDelegate.vcObj performSegueWithIdentifier:@"viewAllTask" sender:cell];
    
  //  myAppDelegate.isFromTnC = false;
    
    [myAppDelegate.viewAllTaskVCObj completeAfterLoginWork];
    
//    if (myAppDelegate.isFromTnC) {
//        
//        if (![UserDefaults boolForKey:keyBioFilled]) {
//            
//            [UserDefaults setBool:YES forKey:keyBioFilled];
//            [UserDefaults synchronize];
//            
//        }
//        
//        TaskTableViewCell *cell = (TaskTableViewCell *)[myAppDelegate.vcObj.tblViewSideBar cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
//        
//        myAppDelegate.isSideBarAccesible = true;
//        
//        [myAppDelegate.vcObj performSegueWithIdentifier:@"viewAllTask" sender:cell];
//        
//        myAppDelegate.isFromTnC = false;
//        
//        [myAppDelegate.viewAllTaskVCObj completeAfterLoginWork];
//        
//    }else{
//        
//        if (myAppDelegate.isFromEdit) {
//            
//            [myAppDelegate.settingsVCObj updateUserData];
//            
//            myAppDelegate.isFromEdit = false;
//            
//            myAppDelegate.isComingBackToSettings = true;
//            
//            myAppDelegate.isSideBarAccesible = true;
//            
//            TaskTableViewCell *cell = (TaskTableViewCell *)[myAppDelegate.vcObj.tblViewSideBar cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
//            [myAppDelegate.vcObj performSegueWithIdentifier:@"settings" sender:cell];
//            
//            myAppDelegate.createBioVCObj = nil;
//            
//        }else{
//            
//        }
//        
//    }
    
}


- (IBAction)cancelBioClicked:(id)sender {
    
    
    NSLog(@"%@",myAppDelegate.userData.mobile);
    
    if(myAppDelegate.userData.mobile.length>1)
    {
        [self goBack];
    }

    else
    {
//        [[GlobalFunction shared] showAlertForMessage:@"Please enter valid 10 digit contact no."];
        
        
        [myAppDelegate.vcObj logOutProcess];
        
        CATransition *transitionAnimation = [CATransition animation];
        [transitionAnimation setType:kCATransitionPush];
        [transitionAnimation setSubtype:kCATransitionFromLeft];
        [transitionAnimation setDuration:0.4f];
        [transitionAnimation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
        [transitionAnimation setFillMode:kCAFillModeBoth];
        
        
        
        //  [myAppDelegate.activeTaskVCObj.viewTaskDetailHolder.layer addAnimation:transitionAnimation forKey:@"changeView"];
        
        [myAppDelegate.vcObj.viewLoginContainer.layer addAnimation:transitionAnimation forKey:@"changeView"];
        
        
        for(UIView *view in myAppDelegate.vcObj.viewLoginContainer.subviews){
            
            [view removeFromSuperview];
            
        }
        
        
        for(UIView *view in myAppDelegate.vcObj.viewHolderWithHeader.subviews){
            
            [view removeFromSuperview];
            
        }
        
        [myAppDelegate.vcObj performSegueWithIdentifier:@"login" sender:myAppDelegate.loginVCObj];
        myAppDelegate.vcObj.btnBack.hidden = YES;
        myAppDelegate.vcObj.btnSideBarOpen.hidden = YES;
        // myAppDelegate.vcObj = nil;
        myAppDelegate.viewAllTaskVCObj = nil;
        myAppDelegate.stringNameOfChildVC = @"";

        
    }
}




-(void)Actionperform{
    
    UIActionSheet *popup = [[UIActionSheet alloc] initWithTitle:@"Select Option:" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:
                            @"Select Existing",
                            @"Take Photo", nil];
    popup.tag = 1;
    
    [popup showInView:[UIApplication sharedApplication].keyWindow];
    
}

- (void)actionSheet:(UIActionSheet *)popup didDismissWithButtonIndex:(NSInteger)buttonIndex {
    
    if (popup.tag == 1) {
        
        if (buttonIndex == 0) {
            
            if (IS_OS_8_OR_LATER) {
                
                PHAuthorizationStatus status = [PHPhotoLibrary authorizationStatus];
                
                if (status == PHAuthorizationStatusAuthorized) {
                    
                    // Access has been granted.
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        [self openPhotos :(int)buttonIndex];
                        
                    });
                    
                }else if (status == PHAuthorizationStatusDenied) {
                    
                    // Access has been denied.
                    [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
                        
                        if (status == PHAuthorizationStatusAuthorized) {
                            
                            // Access has been granted.
                            dispatch_async(dispatch_get_main_queue(), ^{
                                
                                [self openPhotos :(int)buttonIndex];
                                
                            });
                            
                            
                        }else {
                            
                            // Access has been denied.
                            dispatch_async(dispatch_get_main_queue(), ^{
                                
                                [self photosDenied];
                                
                            });
                        }
                    }];
                    
                }else if (status == PHAuthorizationStatusNotDetermined) {
                    
                    // Access has not been determined.
                    [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
                        
                        if (status == PHAuthorizationStatusAuthorized) {
                            
                            // Access has been granted.
                            dispatch_async(dispatch_get_main_queue(), ^{
                                
                                [self openPhotos :(int)buttonIndex];
                                
                            });
                            
                        }else {
                            // Access has been denied.
                            dispatch_async(dispatch_get_main_queue(), ^{
                                
                                [self photosDenied];
                                
                            });
                        }
                    }];
                    
                }else if (status == PHAuthorizationStatusRestricted) {
                    
                    // Restricted access - normally won't happen.
                    
                }
                
            }else{
                
                ALAuthorizationStatus statusAsset = [ALAssetsLibrary authorizationStatus];
                switch (statusAsset) {
                    case ALAuthorizationStatusNotDetermined: {
                        // not determined
                        
                        ALAssetsLibrary *lib = [[ALAssetsLibrary alloc] init];
                        [lib enumerateGroupsWithTypes:ALAssetsGroupSavedPhotos usingBlock:^(ALAssetsGroup *group, BOOL *stop) {
                            
                            // Access has been granted.
                            dispatch_async(dispatch_get_main_queue(), ^{
                                
                                [self openPhotos :(int)buttonIndex];
                                
                            });
                            
                        } failureBlock:^(NSError *error) {
                            if (error.code == ALAssetsLibraryAccessUserDeniedError) {
                                NSLog(@"user denied access, code: %zd", error.code);
                            } else {
                                NSLog(@"Other error code: %zd", error.code);
                            }
                            
                            // Access has been denied.
                            dispatch_async(dispatch_get_main_queue(), ^{
                                
                                [self photosDenied];
                                
                            });
                            
                        }];
                        
                        break;
                    }
                    case ALAuthorizationStatusRestricted: {
                        // restricted
                        break;
                    }
                    case ALAuthorizationStatusDenied: {
                        
                        // denied
                        
                        ALAssetsLibrary *lib = [[ALAssetsLibrary alloc] init];
                        [lib enumerateGroupsWithTypes:ALAssetsGroupSavedPhotos usingBlock:^(ALAssetsGroup *group, BOOL *stop) {
                            // Access has been granted.
                            dispatch_async(dispatch_get_main_queue(), ^{
                                
                                [self openPhotos :(int)buttonIndex];
                                
                            });
                            
                        } failureBlock:^(NSError *error) {
                            if (error.code == ALAssetsLibraryAccessUserDeniedError) {
                                NSLog(@"user denied access, code: %zd", error.code);
                            } else {
                                NSLog(@"Other error code: %zd", error.code);
                            }
                            
                            // Access has been denied.
                            dispatch_async(dispatch_get_main_queue(), ^{
                                
                                [self photosDenied];
                                
                            });
                            
                        }];
                        
                        break;
                    }
                    case ALAuthorizationStatusAuthorized: {
                        // authorized
                        
                        // Access has been granted.
                        dispatch_async(dispatch_get_main_queue(), ^{
                            
                            [self openPhotos :(int)buttonIndex];
                            
                        });
                        
                        break;
                    }
                    default: {
                        break;
                    }
                }
                
            }
            
            
        }else if(buttonIndex == 1){
            
            // For check camera availability
            if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
                
                AVAuthorizationStatus status = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
                
                if(status == AVAuthorizationStatusAuthorized) { // authorized
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        [self openPhotos:(int)buttonIndex];
                        
                    });
                }
                else if(status == AVAuthorizationStatusDenied){ // denied
                    
                    [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
                        
                        if(granted){ // Access has been granted ..do something
                            
                            dispatch_async(dispatch_get_main_queue(), ^{
                                
                                [self openPhotos:(int)buttonIndex];
                                
                            });
                            
                            
                        } else { // Access denied ..do something
                            
                            dispatch_async(dispatch_get_main_queue(), ^{
                                
                                [self camDenied];
                                
                            });
                            
                        }
                    }];
                    
                }
                else if(status == AVAuthorizationStatusRestricted){ // restricted
                    
                    [[GlobalFunction shared] showAlertForMessage:@"Camera uses in application is restricted, please check and try again."];
                    
                }
                else if(status == AVAuthorizationStatusNotDetermined){ // not determined
                    
                    [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
                        
                        if(granted){ // Access has been granted ..do something
                            
                            dispatch_async(dispatch_get_main_queue(), ^{
                                
                                [self openPhotos:(int)buttonIndex];
                                
                            });
                            
                        } else { // Access denied ..do something
                            
                            dispatch_async(dispatch_get_main_queue(), ^{
                                
                                [self camDenied];
                                
                            });
                            
                        }
                    }];
                }
                
            }else{
                
                [[GlobalFunction shared] showAlertForMessage:@"There is no camera available on device."];
                
            }
            
        }
        
    }
    
   else if (popup.tag == 111)
   {
        
        if (buttonIndex == 0)
        {
            NSLog(@"Add paypal select");
            [GlobalFunction addIndicatorView];
            
            [self startCheckout];
        }
        else if (buttonIndex == 1)
        {
            NSLog(@"Add paypal id select");
        
            [self performSegueWithIdentifier:@"setupbank" sender:self];
            

        }
   }
    
}

-(void)photosDenied{
    
    NSString *alertText;
    NSString *alertButton;
    
    alertText = @"BUY TIMEE wants access to photos for getting images to provide better user client interaction.";
    
    alertButton = @"Go";
    
    UIAlertView *alert = [[UIAlertView alloc]
                          initWithTitle:@"BUY TIMEE"
                          message:alertText
                          delegate:self
                          cancelButtonTitle:alertButton
                          otherButtonTitles:nil];
    alert.tag = 3491832;
    [alert show];
    
}

- (void)camDenied{
    
    NSString *alertText;
    NSString *alertButton;
    
    alertText = @"BUY TIMEE wants access to camera for getting images to provide better user client interaction.";
    
    alertButton = @"Go";
    
    UIAlertView *alert = [[UIAlertView alloc]
                          initWithTitle:@"BUY TIMEE"
                          message:alertText
                          delegate:self
                          cancelButtonTitle:alertButton
                          otherButtonTitles:nil];
    
    alert.tag = 3491832;
    [alert show];
}


-(void)openPhotos :(int)index{
    
    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
    
    if (index==0) {
        imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    }
    else{
        imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
        
        if([UIImagePickerController isCameraDeviceAvailable: UIImagePickerControllerCameraDeviceFront]){
            // do something
            imagePickerController.cameraDevice = UIImagePickerControllerCameraDeviceFront;
        }
        
    }
    
    imagePickerController.delegate = (id)self;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        //Code that presents or dismisses a view controller here
        [myAppDelegate.window.rootViewController presentViewController:imagePickerController animated:YES completion:nil];
        
    });
    
}


// This method is called when an image has been chosen from the library or taken from the camera.
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    
    
    myAppDelegate.bankInfObj.imagePickFromAlbum=NO;
    
    
    //You can retrieve the actual UIImage
    UIImage *image = [info valueForKey:UIImagePickerControllerOriginalImage];
    UIImage *flippedImage;
    if (picker.sourceType == UIImagePickerControllerSourceTypeCamera) {
        
        if (picker.cameraDevice == UIImagePickerControllerCameraDeviceFront) {
            
            flippedImage = [UIImage imageWithCGImage:image.CGImage scale:image.scale orientation:UIImageOrientationLeftMirrored];
            
        }else{
            
            flippedImage = image;
            
        }
        
    }else{
        
        flippedImage = image;
        
    }
    
    
    self.imgViewUsersPicture.image = flippedImage;
    
    self.imgViewUsersPicture.layer.cornerRadius = self.imgViewUsersPicture.frame.size.height/2;
    self.imgViewUsersPicture.layer.masksToBounds = YES;
    
    self.imgViewUsersPicture.hidden = NO;
    
    isPictureAvailabe = YES;
    
    [picker dismissViewControllerAnimated:YES completion:nil];
    
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    
    [picker dismissViewControllerAnimated:YES completion:nil];
    
}

-(BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{
    
  
    
//     if ([identifier isEqualToString:@"setupbank"])
//     {
//         if (isFromPaymentOption == true)
//         {
//             return NO;
//         }
//     }
    
    if ([identifier isEqualToString:@"bankInfoVC"]) {
        
        
        
        
        if([myAppDelegate.selectedPaymentTypeFromServer isEqualToString:@"0"])
        {
            
            [GlobalFunction removeIndicatorView];
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"BUY TIMEE" message:@"You need to choose your payment option, before adding vendor profile." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            
            // old one.. You need to add your Cashout with PayPal information before creating the reservation.
            
            // alert.tag = 1009;
            
            [alert show];
            
            return NO;
        }
        else if([myAppDelegate.selectedPaymentTypeFromServer isEqualToString:@"2"]) {
            
            if(myAppDelegate.isBankDetailFilled == false)
            {
            
            [GlobalFunction removeIndicatorView];
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"BUY TIMEE" message:@"You need to add your Cashout with PayPal information before adding vendor profile." delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:nil];
            
            //alert.tag = 1009;
            
            [alert show];
            
            return NO;
            }
        }
            else if([myAppDelegate.selectedPaymentTypeFromServer isEqualToString:@"1"])
            {
                if(myAppDelegate.isBankDetailFilled == false)
                {
                    
                [GlobalFunction removeIndicatorView];
                
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"BUY TIMEE" message:@"You need to add your bank information before adding vendor profile." delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:nil];
                
               // alert.tag = 1009;
                
                [alert show];
                
                return NO;
            }
        
            }
    }
    return YES;
    
}

 


-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
   // [GlobalFunction addIndicatorView];
    
   
     myAppDelegate.superViewClassName = @"new_create_bio";
    
    if ([segue.identifier isEqualToString:@"BankInfo"]) {
        
//        myAppDelegate.vcObj.btnSideBarOpen.hidden = YES;
//        myAppDelegate.vcObj.btnBack.hidden = NO;
//        
//        if (myAppDelegate.isFromProfile) {
//            myAppDelegate.stringNameOfChildVC = @"ViewController";
//        }else{
//            myAppDelegate.stringNameOfChildVC = [NSString stringWithFormat:@"%@",[self class]];
//        }
//
       
        
        myAppDelegate.superViewClassName = @"";
        
        myAppDelegate.bankInfObj = nil;
        
        myAppDelegate.bankInfObj = (BankInfoViewController *)segue.destinationViewController;
        
    }
    else if([segue.identifier isEqualToString:@"bankInfoVC"])
    {
       
        
        myAppDelegate.vcObj.btnBack.hidden = NO;
       myAppDelegate.vcObj.btnSideBarOpen.hidden = YES;
        myAppDelegate.bankInfObj = nil;
        
        myAppDelegate.bankInfObj = (BankInfoViewController *)segue.destinationViewController;

    }
    else if([segue.identifier isEqualToString:@"setupbank"])
    {
          myAppDelegate.Address_FromBioScreen = self.textFieldEmail.text;
        
        myAppDelegate.vcObj.btnBack.hidden =  NO;
        myAppDelegate.vcObj.btnSideBarOpen.hidden = YES;
        myAppDelegate.setUpBankInfoInfObj = nil;
        
        myAppDelegate.setUpBankInfoInfObj = (SetUpBankViewController *)segue.destinationViewController;

    }
    else if([segue.identifier isEqualToString:@"cashOutPayPal"])
    {
       // myAppDelegate.Address_FromBioScreen = self.textFieldEmail.text;
        
        myAppDelegate.vcObj.btnBack.hidden =  NO;
        myAppDelegate.vcObj.btnSideBarOpen.hidden = YES;
        myAppDelegate.cashOutPayPalVCObj = nil;
        
        myAppDelegate.cashOutPayPalVCObj = (cashOutPayPalViewController *)segue.destinationViewController;
        
    }
}

- (IBAction)vendorBtnClicked:(id)sender {
    
    

    
    
    
  //  myAppDelegate.vcObj.sideBarBackBtnTop.constant=20;
    
    myAppDelegate.isSideBarAccesible = false;
    
    myAppDelegate.vcObj.btnSideBarOpen.hidden = YES;
    
    myAppDelegate.vcObj.btnBack.hidden = NO;
    
 //   myAppDelegate.TandC_Buyee = @"";
    
    myAppDelegate.TandC_Buyee = @"BankInfoViewController";
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    BankInfoViewController *objBank= (BankInfoViewController *)[storyboard instantiateViewControllerWithIdentifier:@"BankInfoViewController"];
    
    
    [myAppDelegate.NavController pushViewController:objBank animated:NO];
    
    
    // [myAppDelegate.bankInfObj performSegueWithIdentifier:@"BankInfo" sender:self]; //BankInfo  //navigate to next screen
}
- (IBAction)locationHelpPressed:(id)sender
{
    
    _termsAndPolicyView.hidden=YES;
    
     [txtActiveField resignFirstResponder];
    
    
//    [UIView animateWithDuration:0.5 animations:^{
//       self.blackTransparentView.hidden = false;
//    } completion: ^(BOOL finished) {
//        self.blackTransparentView.alpha = 1.0;
//    }];
    
    [UIView transitionWithView:self.locationHelpButton
                      duration:0.4
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^{
                        self.blackTransparentView.hidden = false;
                        self.blackTransparentView.alpha = 1.0;
                    }
                    completion:NULL];
   // self.blackTransparentView.hidden = false;
   // self.blackTransparentView.alpha = 1.0;
    
}
- (IBAction)OkBtnPressed:(id)sender
{
    
    [UIView animateWithDuration:0.3 animations:^{
        self.blackTransparentView.alpha = 0;
    } completion: ^(BOOL finished) {//creates a variable (BOOL) called "finished" that is set to *YES* when animation IS completed.
        self.blackTransparentView.hidden = true;//if animation is finished ("finished" == *YES*), then hidden = "finished" ... (aka hidden = *YES*)
    }];
    
    
  //  self.blackTransparentView.hidden = true;
   // self.blackTransparentView.alpha = 0;
}//
- (IBAction)btnAcceptClciked:(id)sender {
    
    
            [GlobalFunction addIndicatorView];

    
    
        [self performSelector:@selector(showIndicator) withObject:nil afterDelay:0.2];
    
    
}

-(void)showIndicator
{
    NSDictionary*dictReg = [webServicesShared termsAndPolicyUserId:myAppDelegate.userData.userid tnc:@"1"];
    NSArray *arrayTerms;
    NSString * result = @"";
    NSString *myDescriptionHTML = @"";;
    
    if (dictReg != nil) {
        
        if ([[EncryptDecryptFile decrypt:[dictReg valueForKey:@"responseCode"]] isEqualToString:@"200"]) {
            
            NSDictionary *dictDataReg = [dictReg objectForKey:@"data"];
            NSString *result= [dictDataReg valueForKey:@"description"];
            
            NSLog(@"%@",result);
            
            result=[EncryptDecryptFile decrypt:result];
            
            NSLog(@"%@",result);
            
            
            if([result isEqualToString:@"1"])
            {
                //                        [myAppDelegate.userData.tnc isEqualToString:@"1"];
                
                myAppDelegate.userData.tnc=@"1";
                
            }
            
            [GlobalFunction removeIndicatorView];
            myAppDelegate.isTermsAndPolicyViewOpen=NO;
            [UIView animateWithDuration:0.4 animations:^{
                self.blackTransparentView.alpha = 0;
            } completion: ^(BOOL finished) {
                self.blackTransparentView.hidden = true;
            }];
            
        }
        
        
        else
        {
            
        }
    }
    
}

- (IBAction)btnCancelClicked:(id)sender {
    [myAppDelegate.vcObj logOutProcess];
    
    CATransition *transitionAnimation = [CATransition animation];
    [transitionAnimation setType:kCATransitionPush];
    [transitionAnimation setSubtype:kCATransitionFromLeft];
    [transitionAnimation setDuration:0.4f];
    [transitionAnimation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    [transitionAnimation setFillMode:kCAFillModeBoth];
    
    
    
    //  [myAppDelegate.activeTaskVCObj.viewTaskDetailHolder.layer addAnimation:transitionAnimation forKey:@"changeView"];
    
    [myAppDelegate.vcObj.viewLoginContainer.layer addAnimation:transitionAnimation forKey:@"changeView"];
    
    
    for(UIView *view in myAppDelegate.vcObj.viewLoginContainer.subviews){
        
        [view removeFromSuperview];
        
    }
    
    
    for(UIView *view in myAppDelegate.vcObj.viewHolderWithHeader.subviews){
        
        [view removeFromSuperview];
        
    }
    
    [myAppDelegate.vcObj performSegueWithIdentifier:@"login" sender:myAppDelegate.loginVCObj];
    myAppDelegate.vcObj.btnBack.hidden = YES;
    myAppDelegate.vcObj.btnSideBarOpen.hidden = YES;
    // myAppDelegate.vcObj = nil;
    myAppDelegate.viewAllTaskVCObj = nil;
    myAppDelegate.stringNameOfChildVC = @"";
    myAppDelegate.TandC_Buyee =@"";
    myAppDelegate.isFromCreateBioOnTnC = false;

}

- (void)sendAuthorizationToServer:(NSString *)dataid nonce:(NSString *)nonce
{
    // Send the entire authorization reponse
    
    // NSDictionary* projectDictionary = [authorization objectForKey:@"response"];
    //  projectDictionary1 = [projectDictionary objectForKey:@"code"];
    
    
    // metadataID = [PayPalMobile clientMetadataID];
    
    
 //   NSLog(@"meta data id is...%@",metadataID);
 //   NSLog(@"code is...%@",projectDictionary1);
    
    //  [self dismissViewControllerAnimated:YES completion:nil];
    //  backFromPayPal = true;
    //  [self back];
    // return;
    
   // [GlobalFunction addIndicatorView];

    [self performSelector:@selector(callAPI) withObject:nil afterDelay:0.1];
    
    
}

-(void)callAPI
{
    if (![[WebService shared] connected]) {
        
        [GlobalFunction removeIndicatorView];
        
        [[GlobalFunction shared] showAlertForMessage:@"Can't connect to internet, please check and try again."];
        
        return;
    }
    
    //return;
    NSDictionary *dictPaymentResponse = [[WebService shared] addPayPalUserid:myAppDelegate.userData.userid metaDataId:deviceData codeString:nonce];
    
    if (dictPaymentResponse)
    {
        NSLog(@"response --- %@",[EncryptDecryptFile decrypt:[dictPaymentResponse valueForKey:@"responseMessage"]]);
        
        if ([[EncryptDecryptFile decrypt:[dictPaymentResponse valueForKey:@"responseCode"]] isEqualToString:@"200"])
        {
            myAppDelegate.isPaymentFilled = true;
            myAppDelegate.selectedPaymentTypeFromServer = @"2";
            myAppDelegate.ispayWithPayPallFilled = true;
            myAppDelegate.isStripeCardInfoFilled = false;
            [self setRadioButtonImages];



            [self updatePaymentInfoSuccess:[EncryptDecryptFile decrypt:[dictPaymentResponse valueForKey:@"responseMessage"]]];
        }
        else{
            
            //  [[GlobalFunction shared] showAlertForMessage:[EncryptDecryptFile decrypt:[dictPaymentResponse valueForKey:@"responseMessage"]]];
            [self errorShow:[EncryptDecryptFile decrypt:[dictPaymentResponse valueForKey:@"responseMessage"]]];
            [GlobalFunction removeIndicatorView];
        }
        
    }
    else
    {
        [self errorShow:@"Server is down, please try again later."];
        
        // [[GlobalFunction shared] showAlertForMessage:@"Server is down, please try again later."];
        
        [GlobalFunction removeIndicatorView];
        
    }

}

-(void)updatePaymentInfoSuccess:(NSString *)response
{
    
    [GlobalFunction removeIndicatorView];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"BUY TIMEE" message:response delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    
    // alert.tag = 10001;
    
   // alert.tag = 1111;
    
    
    [alert show];
    
}


- (IBAction)cashOutClicked:(id)sender
{
    
}
- (IBAction)payWithPayPalClicked:(id)sender
{
 
    buttonClicked = @"paypal";
    
//    self.circleImgViewBank.hidden = YES;
//    self.circleImgViewPaypal.hidden = NO;
    
   [self okButtonClickedOnChoosePayOption:nil];
    [self performSelector:@selector(showActionSheet) withObject:nil afterDelay:0.4];
   // [self showActionSheet];
    
    
//    [UIView transitionWithView:self.btnAddBankInfo
//                      duration:0.4
//                       options:UIViewAnimationOptionTransitionCrossDissolve
//                    animations:^{
//                        [self.view layoutIfNeeded];
//                     
//                        
//                        self.consUnderButtonsLineTop.constant = 20;
//                        _vendorBtnBottomConstant.constant=26;
//                        _vendorLineBottom.constant=20; //old constrtaints before hide bank info brn by vs 16/8/18.
//                        
//                        [self.view layoutIfNeeded];
//                    }
//                    completion:^(BOOL finished)
//               {
//                   [UIView transitionWithView:self.btnAddBankInfo
//                                     duration:0.4
//                                      options:UIViewAnimationOptionTransitionCrossDissolve
//                                   animations:^{
//                                       self.btnAddBankInfo.hidden = false;
//                                       self.lblLineUnderAddBankBtn.hidden = false;
//                                       [self.btnAddBankInfo setTitle:@"Add Your PayPal Info." forState:UIControlStateNormal];
//
//                                   } completion:NULL];
//
//                  
//                    }];
//
    
}
- (IBAction)okButtonClickedOnChoosePayOption:(id)sender
{
    [UIView transitionWithView:self.blackTransparentView
                      duration:0.4
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^{
                        self.paymentOptionView.hidden = YES;
                        self.blackTransparentView.hidden = YES;
                        self.blackTransparentView.alpha = 1.0;
                        self.termsAndPolicyView.hidden = NO;
                        self.insideViewOfBlurView.hidden = NO;
                    }
                    completion:NULL];
    
    //return;
}
-(void)hidePaymentView
{
    
}
- (IBAction)payWithBankClicked:(id)sender
{
     buttonClicked = @"bank";
    
  //  self.circleImgViewBank.hidden = NO;
  //  self.circleImgViewPaypal.hidden = YES;
     [self okButtonClickedOnChoosePayOption:nil];
    
//    [UIView transitionWithView:self.btnAddBankInfo
//                      duration:0.4
//                       options:UIViewAnimationOptionTransitionCrossDissolve
//                    animations:^{
//                      [self.view layoutIfNeeded];
//                        self.consUnderButtonsLineTop.constant = 20;
//                        _vendorBtnBottomConstant.constant=26;
//                        _vendorLineBottom.constant=20; //old constrtaints before hide bank info brn by vs 16/8/18.
//                         [self.view layoutIfNeeded];
//                    }
//                    completion:^(BOOL finished)
//     {
//         [UIView transitionWithView:self.btnAddBankInfo
//                           duration:0.4
//                            options:UIViewAnimationOptionTransitionCrossDissolve
//                         animations:^{
//                             self.btnAddBankInfo.hidden = false;
//                             self.lblLineUnderAddBankBtn.hidden = false;
//                             [self.btnAddBankInfo setTitle:@"Add Your Bnak Info." forState:UIControlStateNormal];
//                             
//                         } completion:NULL];
//
//     }];
    [self performSelector:@selector(showActionSheetForBank) withObject:nil afterDelay:0.4];

}
- (IBAction)stripeRadioClicked:(id)sender
{
//     myAppDelegate.selectedPaymentTypeFromServer = @"1";
//
//    if (myAppDelegate.isFromTapPayPalRadioBtnOnCreateBio == true)
//    {
//        myAppDelegate.isPaymentFilled = false;
//        myAppDelegate.isBankDetailFilled = false;
//        myAppDelegate.isFromTapPayPalRadioBtnOnCreateBio = false;
//
//    }
//    myAppDelegate.isFromTapStripeRadioBtnOnCreateBio = true;
    
    //radioSelectedButton.png
//    [self.btnRadioStripe setBackgroundImage:[UIImage imageNamed:@"radioSelectedButton.png"] forState:UIControlStateNormal];
//     [self.btnRadioPayPal setBackgroundImage:[UIImage imageNamed:@"radioButton.png"] forState:UIControlStateNormal];
    
    [self performSelector:@selector(showActionSheetForBank) withObject:nil afterDelay:0.1];

}

- (IBAction)paypalRadioClicked:(id)sender
{
//     myAppDelegate.selectedPaymentTypeFromServer = @"2";
//
//    if (myAppDelegate.isFromTapStripeRadioBtnOnCreateBio == true)
//    {
//        myAppDelegate.isPaymentFilled = false;
//        myAppDelegate.isBankDetailFilled = false;
//        myAppDelegate.isFromTapStripeRadioBtnOnCreateBio = false;
//
//    }
//
//    myAppDelegate.isFromTapPayPalRadioBtnOnCreateBio = true;
    
//    [self.btnRadioStripe setBackgroundImage:[UIImage imageNamed:@"radioButton.png"] forState:UIControlStateNormal];
//    [self.btnRadioPayPal setBackgroundImage:[UIImage imageNamed:@"radioSelectedButton.png"] forState:UIControlStateNormal];
    [self performSelector:@selector(showActionSheet) withObject:nil afterDelay:0.1];
}
-(void)tapOnPayPalLbl:(UITapGestureRecognizer *)tapGesture
{
    [self paypalRadioClicked:nil];
}
-(void)tapOnStripeLbl:(UITapGestureRecognizer *)tapGesture
{
    [self stripeRadioClicked:nil];
}
@end
