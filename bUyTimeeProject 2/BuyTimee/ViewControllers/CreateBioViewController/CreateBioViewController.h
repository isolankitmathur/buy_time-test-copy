//
//  CreateBioViewController.h
//  HandApp
//


#import <UIKit/UIKit.h>
#import "Flurry.h"
#import "BraintreeCore.h"
@interface CreateBioViewController : UIViewController<UITextFieldDelegate,UIWebViewDelegate>

@property (nonatomic, strong) BTAPIClient *braintreeClient;

@property (weak, nonatomic) IBOutlet UIButton *btnUploadPhoto;
@property (strong, nonatomic) IBOutlet UIButton *btnAddPaymentInfo;
@property (weak, nonatomic) IBOutlet UIButton *btnAddBankInfo;
@property (weak, nonatomic) IBOutlet UIButton *btnSave;
@property (weak, nonatomic) IBOutlet UIButton *btnCancel;

@property (weak, nonatomic) IBOutlet UITextField *textFieldUsername;
@property (weak, nonatomic) IBOutlet UITextField *textFieldEmail;       //text field location typo error
@property (weak, nonatomic) IBOutlet UITextField *textFieldLocation;    //text field email type error
@property (weak, nonatomic) IBOutlet UITextField *textFieldPhone;
@property (weak, nonatomic) IBOutlet UITextField *textFieldDescription;

@property (weak, nonatomic) IBOutlet UIView *viewHolder;

@property (weak, nonatomic) IBOutlet UIView *upperHolderView;


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintUserNameTextFieldTopSpace;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintLblUsernameBarBottomSpace;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintLblEmailBarBottomSpace;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintLblLocationBarBottomSpace;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintLblPhoneBarBottomSpace;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintLblDescriptionBarBottomSpace;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintLblAddBankInfoBarBottomSpace;

- (IBAction)uploadPicture:(id)sender;
- (IBAction)getPaymentInfoClicked:(id)sender;
- (IBAction)saveBioClicked:(id)sender;
- (IBAction)cancelBioClicked:(id)sender;
- (IBAction)getBankInfoClicked:(id)sender;


@property (nonatomic, retain) UITextField *txtActiveField;
@property (nonatomic, retain) UIView *inputAccView;
@property (nonatomic, retain) UIButton *btnDone;
@property (nonatomic, retain) UIButton *btnNext;
@property (nonatomic, retain) UIButton *btnPrev;

@property (nonatomic, assign)BOOL isPaymentFilled;
@property (nonatomic, assign)BOOL isBankInfoFilled;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consLblPhoneHeight;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consPhoneTextFldHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consBtnCancelLeading;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consBtnSaveLeading;

@property (weak, nonatomic) IBOutlet UIButton *btnVendor;
- (IBAction)vendorBtnClicked:(id)sender;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *btnVendorWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *btnVendorHeight;



@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consUpperHolderViewTop;
@property (weak, nonatomic) IBOutlet UIView *mainUpperHolderVIew;



@property (weak, nonatomic) IBOutlet NSLayoutConstraint *vendorBtnBottomConstant;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *vendorLineBottom;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *userNameTxtFldHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *vendorLineLblLeft;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *vendorLineLblWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *txtFldHeight;





@property (weak, nonatomic) IBOutlet NSLayoutConstraint *userTxtFldTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *line1Top;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *locationTxtFldTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *line2Top;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *instagramTxtFldTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *line3Top;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *phoneTxtFldTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *line4Top;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *aboutMeTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *line5Top;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *addPaymntTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *line6Top;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *addBankTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *line7Top;




@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consUpperHolderViewHeight;

@property (weak, nonatomic) IBOutlet UILabel *userNameLine;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consUploadBtnHeight;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *ConsGreyLineTop;

@property (weak, nonatomic) IBOutlet UILabel *consGreyLineLbl;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consImageViewUserPicWidth;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consImageViewUserPicHeight;


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consSaveBtnBottom;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consCancelBtnBottom;


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consBtnVendorLeading;
//*****************************


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consUnderButtonsLineHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consUnderButtonsLineTop;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consBtnTransparentUploadPhotoTop;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consBtnTransparentUploadPhotoLeading;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consUserImgViewWidth;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consUserImgViewHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consUserImgViewTop;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consUserImgViewLeading;


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consLblUploadPhotoLeading;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consLblUploadPhotoTop;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consUserNameTxtFldLeading;
@property (weak, nonatomic) IBOutlet UILabel *lblUploadPhoto;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consLblUserUnderLineLeading;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consLblUserUnderLineTrailing;


// New black view trans. properties by VS 19 june 17
@property (weak, nonatomic) IBOutlet UIView *blackTransparentView;
@property (weak, nonatomic) IBOutlet UIView *insideViewOfBlurView;
@property (weak, nonatomic) IBOutlet UIButton *OkButton;
- (IBAction)OkBtnPressed:(id)sender;

@property (weak, nonatomic) IBOutlet UILabel *lblBlurView;

@property (weak, nonatomic) IBOutlet UIButton *locationHelpButton;

- (IBAction)locationHelpPressed:(id)sender;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *nsConsHelpTop;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *nsConsHelpLeading;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *nsconsInsideBlurViewTop;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *alertViewTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *alertViewLeft;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *alertViewWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *alertViewHeight;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *consHelpBtnWidth;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *consHelpBtnHeight;

@property (strong, nonatomic) IBOutlet UIView *termsAndPolicyView;
@property (strong, nonatomic) IBOutlet UIWebView *termsAndPolicyWebView;
@property (strong, nonatomic) IBOutlet UIButton *btn_Accept;
@property (strong, nonatomic) IBOutlet UIButton *btn_Cancel;
- (IBAction)btnAcceptClciked:(id)sender;
- (IBAction)btnCancelClicked:(id)sender;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *consTermsAndPolicyViewBottom;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *consTermsAndPolicyWebViewHeight;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *btnAcceptWidth;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *btnAcceptHeight;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *btnAcceptBottom;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *btnCancelBottom;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *consLineLblBottom;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *consLineLblHeight;



@property (weak, nonatomic) IBOutlet UIButton *cashOutBtn;

- (IBAction)cashOutClicked:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *paymentOptionView;

@property (weak, nonatomic) IBOutlet UIButton *btnContinueWithPaypal;
- (IBAction)payWithPayPalClicked:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnContinueWithBank;

- (IBAction)payWithBankClicked:(id)sender;

//@property (weak, nonatomic) IBOutlet UIImageView *circleImgViewPaypal;
//@property (weak, nonatomic) IBOutlet UIImageView *circleImgViewBank;

- (IBAction)okButtonClickedOnChoosePayOption:(id)sender;

@property (weak, nonatomic) IBOutlet UILabel *lblLineUnderAddBankBtn;


@property (weak, nonatomic) IBOutlet UIView *paymentsOptionsView;

@property (weak, nonatomic) IBOutlet UILabel *lblOptionStripe;

@property (weak, nonatomic) IBOutlet UILabel *lblOptionPayPal;

@property (weak, nonatomic) IBOutlet UILabel *lblChooseOptionPayment;

@property (weak, nonatomic) IBOutlet UIButton *btnRadioStripe;

@property (weak, nonatomic) IBOutlet UIButton *btnRadioPayPal;

- (IBAction)stripeRadioClicked:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *btnAddStripeDetail;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *nsconsAddStripeDetailUnderLineBottom;

- (IBAction)paypalRadioClicked:(id)sender;


-(void)setRadioButtonImages;


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consRadioBtnFirstLeading;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consRadioBtnFirstTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consRadioBtnFirstWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consRadioBtnFirstHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consLblStripeLeading;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consLblStripeTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consLblPaypalTrailing;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consLblPaypalTop;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consRadioBtnSecondHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consRadioBtnSecondWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consRadioBtnSecondTrailing;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consPaymentOPtionViewHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consRadioBtnSecondTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consLblChooseOptionLeading;

@end


