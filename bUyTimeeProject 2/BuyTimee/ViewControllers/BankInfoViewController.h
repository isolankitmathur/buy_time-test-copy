//
//  BankInfoViewController.h
//  BuyTimee
//
//  Created by User38 on 07/02/17.
//  Copyright © 2017 User14. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Flurry.h"


@interface BankInfoViewController : UIViewController<UITextFieldDelegate>


@property (nonatomic, retain) UITextField *txtActiveField;
@property (nonatomic, retain) UIView *inputAccView;
@property (nonatomic, retain) UIButton *btnDone;
@property (nonatomic, retain) UIButton *btnNext;
@property (nonatomic, retain) UIButton *btnPrev;


@property (weak, nonatomic) IBOutlet UIView *LogoHolderView;

@property (weak, nonatomic) IBOutlet UITextField *textFieldUsername;
@property (weak, nonatomic) IBOutlet UITextField *textFieldAdd;       //text field location typo error
@property (weak, nonatomic) IBOutlet UITextField *textFieldYelp;    //text field email type error
@property (weak, nonatomic) IBOutlet UITextField *textFieldInsta;
@property (weak, nonatomic) IBOutlet UITextField *textFieldServices;

@property (weak, nonatomic) IBOutlet UIView *textFldView;

@property (weak, nonatomic) IBOutlet UIButton *btnSave;

@property (weak, nonatomic) IBOutlet UIButton *btnCancel;


@property (weak, nonatomic) IBOutlet UILabel *lblUploadLogo;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consUploadLogoLeading;


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consImgViewLogoWidth;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consImgViewLogoHeight;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consBtnSaveBottom;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consBtnCancelBottom;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consBtnSaveHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consBtnSaveWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consBtnCancelHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consBtnCancelWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consBtnCancelLeading;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consBtnTransTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consLogoImgTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consImgViewLeading;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consTextNameTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consBtnTransparentLeading;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consTextAddressTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consTextYelpTop;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consTextInstaTop;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consTextServicesTop;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *TextFldViewTop;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consUpperviewHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consTextFldViewTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consTextFieldssTopSpace;

- (IBAction)uploadPicture:(id)sender;
- (IBAction)saveProfile:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *cancelPressed;
- (IBAction)cancelBtnPressed:(id)sender;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consUploadLogoLblTop;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consImageLogoBtnWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consImageLogoBtnHeight;



@property(nonatomic,assign)BOOL imagePickFromAlbum;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *textFldViewHeight;

@property (weak, nonatomic) IBOutlet UIView *underButtonsViewForLine;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *btnsUnderViewHeight;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *btnsUnderViewBottom;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lineOneTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lineTwoTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lineThreeTop;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lineFourTop;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lineFiveTop;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *textFldOneLeading;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *textFldFiveLeading;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *textFldTwoLeading;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *textFldThreeLeading;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *textFldFourLeading;

// New black view trans. properties by VS 19 june 17
@property (weak, nonatomic) IBOutlet UIView *blackTransparentView;
@property (weak, nonatomic) IBOutlet UIView *insideViewOfBlurView;
@property (weak, nonatomic) IBOutlet UIButton *OkButton;

- (IBAction)OkBtnPressed:(id)sender;



@property (weak, nonatomic) IBOutlet UILabel *lblBlurView;

@property (weak, nonatomic) IBOutlet UIButton *locationHelpButton;
- (IBAction)locationHelpPressed:(id)sender;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *nsConsHelpTop;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *nsConsHelpLeading;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *nsconsInsideBlurViewTop;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *alertViewTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *alertViewLeft;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *alertViewWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *alertViewHeight;

@property (weak, nonatomic) IBOutlet UIButton *chooseBtn;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *chooseBtnTrailing;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *chooseBtnLeading;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *chooseBtnHeight;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *chooseBtnBottom;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *chooseBtnTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *chooseBtnLineTop;

@property (weak, nonatomic) IBOutlet UITableView *tbleView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tbleViewHeight;

- (IBAction)chooseCatBtnClicked:(id)sender;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *consTxtFlsUserNameHeight;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *consTxtFldAdressHeight;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *consTxtFldYulpHeight;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *consTxtFldInstaHeight;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *consTxtFldServiceHeight;



@property (weak, nonatomic) IBOutlet NSLayoutConstraint *condTableViewTop;

/// by vs 22 nov 18...

@property (weak, nonatomic) IBOutlet UIButton *btnTransparent;
- (IBAction)transparentBtnClicked:(id)sender;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consBtnTransparentTop;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consBtnTransLeading;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consBtnTransHeight;


@end
