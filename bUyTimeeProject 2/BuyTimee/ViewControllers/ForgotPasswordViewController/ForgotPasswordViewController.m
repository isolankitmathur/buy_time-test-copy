//
//  ForgotPasswordViewController.m
//  HandApp
//
//  Created by  ~ on 26/02/16.
//  Copyright (c) 2016 . All rights reserved.
//

#import "ForgotPasswordViewController.h"
#import "Constants.h"
#import "GlobalFunction.h"
#import "LoadingView.h"

@interface ForgotPasswordViewController ()

- (IBAction)cancelBtnClicked:(id)sender;
@end

@implementation ForgotPasswordViewController{
    
    LoadingView *loadingView;
    
}


@synthesize txtActiveField;
@synthesize inputAccView;
@synthesize btnDone;
@synthesize btnNext;
@synthesize btnPrev;

- (void)viewDidLoad{
    
    [super viewDidLoad];
    
    self.textFieldForgotPassword.delegate = self;
    
    //set place holders
    
    UIFont *fnt = self.textFieldForgotPassword.font;
    
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:@"Enter Email*"
                                                                                         attributes:@{NSFontAttributeName: [fnt fontWithSize:20], NSForegroundColorAttributeName : [UIColor whiteColor]}];
    
    [attributedString setAttributes:@{NSFontAttributeName : [fnt fontWithSize:8], NSForegroundColorAttributeName : [UIColor whiteColor]
                                      , NSBaselineOffsetAttributeName : @0} range:NSMakeRange(11, 1)];
    
    
//    NSAttributedString *str1 = [[NSAttributedString alloc] initWithString:@"Enter Email" attributes:@{ NSForegroundColorAttributeName : [UIColor whiteColor],
//                                                                                                    }];
   
    self.textFieldForgotPassword.attributedPlaceholder = attributedString;
    
    UIFont *font = self.textFieldForgotPassword.font;
    
    
    if (IS_IPHONE_4_OR_LESS){
        
        self.lblForgotPasswordText.font = [font fontWithSize:12.0];
        self.textFieldForgotPassword.font = [font fontWithSize:12.0];
        
    }
    if (IS_IPHONE5){
        
        self.lblForgotPasswordText.font = [font fontWithSize:14.0];
        self.textFieldForgotPassword.font = [font fontWithSize:14.0];
        
    }
    if (IS_IPHONE_6){
        
        self.lblForgotPasswordText.font = [font fontWithSize:16.0];
        self.textFieldForgotPassword.font = [font fontWithSize:16.0];
    }
    if (IS_IPHONE_6P){
        
        self.lblForgotPasswordText.font = [font fontWithSize:18.0];
        self.textFieldForgotPassword.font = [font fontWithSize:18.0];
    }
    
}

- (void)didReceiveMemoryWarning{
    [super didReceiveMemoryWarning];
}


- (IBAction)forgotPasswordSubmitClicked:(id)sender {
    
    
    if ([[self.textFieldForgotPassword.text stringByReplacingOccurrencesOfString:@" " withString:@""] isEqualToString:@""]) {
        
        [[GlobalFunction shared] showAlertForMessage:@"Please enter Email address."];
        
        return;
    }
    
    if (![GlobalFunction validateEmail:self.textFieldForgotPassword.text]) {
        
        [[GlobalFunction shared] showAlertForMessage:@"Please enter valid Email address."];
        
        return;
    }

    [self performSelector:@selector(addIndicator) withObject:nil afterDelay:0.0];
    
    [self performSelector:@selector(performWorkForForgotPassword) withObject:nil afterDelay:0.5];
    
}

-(void)performWorkForForgotPassword{
    
    if (![[WebService shared] connected]) {
        
        [GlobalFunction removeIndicatorView];
        
        [[GlobalFunction shared] showAlertForMessage:@"Can't connect to internet, please check and try again."];
        
        return;
        
    }

    
    NSDictionary *dictReg = [webServicesShared forgotemailId:self.textFieldForgotPassword.text];
    
    if (dictReg != nil) {
        
        if ([[EncryptDecryptFile decrypt:[dictReg valueForKey:@"responseCode"]] isEqualToString:@"200"]) {
        
            [GlobalFunction removeIndicatorView];

            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"BUY TIMEE" message:[EncryptDecryptFile decrypt:[dictReg valueForKey:@"responseMessage"]] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            
            alert.tag = 10001;
            
            [alert show];
            
        }else{
            
            [GlobalFunction removeIndicatorView];

            [[GlobalFunction shared] showAlertForMessage:[EncryptDecryptFile decrypt:[dictReg valueForKey:@"responseMessage"]]];

        }
        
    }else{
        
        [GlobalFunction removeIndicatorView];
        
        [[GlobalFunction shared] showAlertForMessage:@"Server is down, please try again later."];

    }

}

-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex{
    
    if (alertView.tag == 10001) {
        
        if(buttonIndex == [alertView cancelButtonIndex]){
            
            [self dismissViewControllerAnimated:YES completion:nil];
            
        }
    }
}


-(void)addIndicator{
    
    [GlobalFunction addIndicatorView];
    
}


#pragma mark - create accessory view
-(void)createInputAccessoryView{
    // Create the view that will play the part of the input accessory view.
    // Note that the frame width (third value in the CGRectMake method)
    // should change accordingly in landscape orientation. But we don’t care
    // about that now.
    inputAccView = [[UIView alloc] initWithFrame:CGRectMake(10.0, 0.0, self.view.frame.size.width, 40.0)];
    // Set the view’s background color. We’ ll set it here to gray. Use any color you want.
    [inputAccView setBackgroundColor:[UIColor lightGrayColor]];
    // We can play a little with transparency as well using the Alpha property. Normally
    // you can leave it unchanged.
    [inputAccView setAlpha: 0.8];
    // If you want you may set or change more properties (ex. Font, background image,e.t.c.).
    // For now, what we’ ve already done is just enough.
    // Let’s create our buttons now. First the previous button.
    btnPrev = [UIButton buttonWithType: UIButtonTypeCustom];
    // Set the button’ s frame. We will set their widths to 80px and height to 40px.
    [btnPrev setFrame: CGRectMake(0.0, 0.0, 80.0, 40.0)];
    // Title.
    [btnPrev setTitle: @"Previous" forState: UIControlStateNormal];
    // Background color.
    [btnPrev setBackgroundColor: [UIColor clearColor]];
    // You can set more properties if you need to.
    // With the following command we’ ll make the button to react in finger tapping. Note that the
    // gotoPrevTextfield method that is referred to the @selector is not yet created. We’ ll create it
    // (as well as the methods for the rest of our buttons) later.
    [btnPrev addTarget: self action: @selector(gotoPrevTextfield) forControlEvents: UIControlEventTouchUpInside];
    // Do the same for the two buttons left.
    btnNext = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnNext setFrame:CGRectMake(85.0f, 0.0f, 80.0f, 40.0f)];
    [btnNext setTitle:@"Next" forState:UIControlStateNormal];
    [btnNext setBackgroundColor:[UIColor clearColor]];
    [btnNext addTarget:self action:@selector(gotoNextTextfield) forControlEvents:UIControlEventTouchUpInside];
    btnDone = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnDone setFrame:CGRectMake(self.view.frame.size.width - 80.0f, 0.0f, 80.0f, 40.0f)];
    [btnDone setTitle:@"Done" forState:UIControlStateNormal];
    [btnDone setBackgroundColor:[UIColor clearColor]];
    [btnDone setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [btnDone addTarget:self action:@selector(doneTyping) forControlEvents:UIControlEventTouchUpInside];
    // Now that our buttons are ready we just have to add them to our view.
    [inputAccView addSubview:btnPrev];
    [inputAccView addSubview:btnNext];
    [inputAccView addSubview:btnDone];
}


-(void)gotoPrevTextfield{
    // If the active textfield is the first one, can't go to any previous
    // field so just return.
    [txtActiveField resignFirstResponder];
    
}

-(void)gotoNextTextfield{
    // If the active textfield is the second one, there is no next so just return.
    [txtActiveField resignFirstResponder];
    
}

-(void)doneTyping{
    // When the "done" button is tapped, the keyboard should go away.
    // That simply means that we just have to resign our first responder.
    [txtActiveField resignFirstResponder];
}


#pragma mark - text field delegate
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    
    return YES;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    
    [self createInputAccessoryView];
    // Now add the view as an input accessory view to the selected textfield.
    [textField setInputAccessoryView:inputAccView];
    // Set the active field. We' ll need that if we want to move properly
    // between our textfields.
    txtActiveField = textField;
    
    if(IS_IPHONE_4_OR_LESS){
        
        if(textField==_textFieldForgotPassword){
            
            CGRect frame = self.contentView.frame;
            frame.origin.y =-10;
            
            [UIView animateWithDuration:0.5 animations:^{
                self.contentView.frame = frame;
            } completion:^(BOOL finished) {
                
            }];
            
        }
    }

    
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardWillHideNotification object:nil];
    
    [self.view endEditing:YES];
    return YES;
}

-(void)keyboardDidHide:(NSNotification *)notification{
    
    [UIView animateWithDuration:0.3 animations:^{
        [self.contentView setFrame:CGRectMake(0,self.contentViewTop.constant, self.contentView.frame.size.width, self.contentView.frame.size.height)];
    } completion:^(BOOL finished) {
        [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    }];
    
}


-(void)textFieldDidEndEditing:(UITextField *)textField{
    
}


- (IBAction)cancelBtnClicked:(id)sender {

    [txtActiveField resignFirstResponder];
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

@end
