//
//  ForgotPasswordViewController.h
//  HandApp
//
//  Created by  ~ on 26/02/16.
//  Copyright (c) 2016 . All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ForgotPasswordViewController : UIViewController<UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UILabel *lblForgotPasswordText;

@property (weak, nonatomic) IBOutlet UITextField *textFieldForgotPassword;

- (IBAction)forgotPasswordSubmitClicked:(id)sender;

@property (nonatomic, retain) UITextField *txtActiveField;
@property (nonatomic, retain) UIView *inputAccView;
@property (nonatomic, retain) UIButton *btnDone;
@property (nonatomic, retain) UIButton *btnNext;
@property (nonatomic, retain) UIButton *btnPrev;

@property (weak, nonatomic) IBOutlet UIImageView *imgViewBackground;

@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentViewTop;

@end
