//
//  PaymentInfoViewController.m
//  HandApp
//
//  Created by  ~ on 01/03/16.
//  Copyright (c) 2016 . All rights reserved.
//

#import "PaymentInfoViewController.h"
#import "Constants.h"
#import "MonthAndYearTableViewCell.h"
#import "GlobalFunction.h"
#import "TaskTableViewCell.h"


#import "BraintreePayPal.h"
#import "PPDataCollector.h"

#import <QuartzCore/QuartzCore.h>
#import <SafariServices/SafariServices.h>

@interface PaymentInfoViewController () <BTAppSwitchDelegate, BTViewControllerPresentingDelegate,UITextFieldDelegate,UITableViewDataSource,UITableViewDelegate,UIGestureRecognizerDelegate>


//@property (nonatomic, strong) BTAPIClient *braintreeClient;
@property (nonatomic, strong) BTPayPalDriver *payPalDriver;


//@property(nonatomic, strong) PayPalConfiguration *payPalconfig;

@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *btnCreditCardCollection;


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintViewCreditCardHolderHeight;  //123
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintBtnVisaBottomSpace;  //21
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintLblChoosePaymentTopSpace;    //19


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintLogoBottomSpace;     //24
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintLogoTopSpaceConstraintTopSpace;  //43
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintViewLogoHolderHeight;    //162
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintLblPaymentInfoBottomSpace;   //28


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintTextFeildHeightMaxValue;     //40

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintTextFieldMonthBottomSpace;   //172
@property (weak, nonatomic) IBOutlet UIButton *btnSavePaymentInfo;
@property (weak, nonatomic) IBOutlet UIView *viewBgHolder;
@property (weak, nonatomic) IBOutlet UIImageView *imgViewCVVInfo;
- (IBAction)closeCVVInfo:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnCloseCVV;
@property (weak, nonatomic) IBOutlet UIButton *btnInfo;

- (IBAction)backToSuperView:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *viewCardAndTextFieldHolder;

@end

@implementation PaymentInfoViewController{
    
    NSArray *monthArray;
    NSMutableArray *yearArray;
    BOOL isMonthOrYear;
    CGPoint centerToGoBack;
    BOOL isTableOpened;
    UIButton *btnPrevSelected;
    
    BOOL AmericanCardBtnClicked;
    NSString *mainCard;
    
     BOOL backFromPayPal;
    NSString* projectDictionary1;
    NSString *metadataID;
    NSString *deviceData;
    NSString *nonce;
}

@synthesize txtActiveField;
@synthesize inputAccView;
@synthesize btnDone;
@synthesize btnNext;
@synthesize btnPrev;

- (void)viewDidLoad{
    [super viewDidLoad];
    
     myAppDelegate.vcObj.btnMapOnListView.hidden = true;//vs
    myAppDelegate.vcObj.BtnTransparentMap.hidden = true;//vs

    mainCard = @"";
    
    monthArray = [NSArray arrayWithObjects:@"01",@"02",@"03",@"04",@"05",@"06",@"07",@"08",@"09",@"10",@"11",@"12", nil];
    
    yearArray = [[NSMutableArray alloc]init];
    
    NSInteger year = 2016;
    
    for (int i = 0;i<50; i++) {
        
        [yearArray addObject:[NSString stringWithFormat:@"%ld",(long)year++]];
        
    }
    
    self.viewBgHolder.hidden = YES;
    self.imgViewCVVInfo.hidden = YES;
    self.btnCloseCVV.hidden = YES;
    
    self.tableViewMonth.delegate = self;
    self.tableViewMonth.dataSource = self;
    
    self.tableViewYear.delegate = self;
    self.tableViewYear.dataSource = self;
    
    self.textFieldCardDigit1To4.delegate = self;
    self.textFieldCardDigit5To8.delegate = self;
    self.textFieldCardDigit9To12.delegate = self;
    self.textFieldCardDigit13To16.delegate = self;
    self.textFieldCVVNO.delegate = self;
    self.textFieldExpirationDateMM.delegate = self;
    self.textFieldExpirationDateYYYY.delegate = self;
    self.textFieldNameOnCard.delegate = self;
    
    self.viewBGTapHandler.hidden = true;
    self.viewBGTapHandlerInCardsView.hidden = true;
    
    UILabel * leftView8 = [[UILabel alloc] initWithFrame:CGRectMake(10,0,5,self.textFieldNameOnCard.frame.size.height)];
    leftView8.backgroundColor = [UIColor clearColor];
    
    self.textFieldNameOnCard.leftView = leftView8;
    self.textFieldNameOnCard.leftViewMode = UITextFieldViewModeAlways;
    self.textFieldNameOnCard.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    
//    [GlobalFunction createBorderforView:self.textFieldCardDigit1To4 withWidth:1.0 cornerRadius:3.0 colorForBorder:[UIColor colorWithRed:151.0/255.0 green:151.0/255.0 blue:151.0/255.0 alpha:1.0]];
//    [GlobalFunction createBorderforView:self.textFieldCardDigit5To8 withWidth:1.0 cornerRadius:3.0 colorForBorder:[UIColor colorWithRed:151.0/255.0 green:151.0/255.0 blue:151.0/255.0 alpha:1.0]];
//    [GlobalFunction createBorderforView:self.textFieldCardDigit9To12 withWidth:1.0 cornerRadius:3.0 colorForBorder:[UIColor colorWithRed:151.0/255.0 green:151.0/255.0 blue:151.0/255.0 alpha:1.0]];
//    [GlobalFunction createBorderforView:self.textFieldCardDigit13To16 withWidth:1.0 cornerRadius:3.0 colorForBorder:[UIColor colorWithRed:151.0/255.0 green:151.0/255.0 blue:151.0/255.0 alpha:1.0]];
//    [GlobalFunction createBorderforView:self.textFieldCVVNO withWidth:1.0 cornerRadius:3.0 colorForBorder:[UIColor colorWithRed:151.0/255.0 green:151.0/255.0 blue:151.0/255.0 alpha:1.0]];
//    [GlobalFunction createBorderforView:self.textFieldNameOnCard withWidth:1.0 cornerRadius:3.0 colorForBorder:[UIColor colorWithRed:151.0/255.0 green:151.0/255.0 blue:151.0/255.0 alpha:1.0]];
    [GlobalFunction createBorderforView:self.viewHolderForExpirationDateYear withWidth:1.0 cornerRadius:3.0 colorForBorder:[UIColor colorWithRed:151.0/255.0 green:151.0/255.0 blue:151.0/255.0 alpha:1.0]];
    [GlobalFunction createBorderforView:self.viewHolderForTableExpirationDateMonth withWidth:1.0 cornerRadius:3.0 colorForBorder:[UIColor colorWithRed:151.0/255.0 green:151.0/255.0 blue:151.0/255.0 alpha:1.0]];
    
    
    if (IS_IPHONE_4_OR_LESS) {
        [self.btnSavePaymentInfo.titleLabel setFont:[UIFont systemFontOfSize:16.0]]; //12
    }
    if (IS_IPHONE_5) {
        [self.btnSavePaymentInfo.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:16]];
    }
    if (IS_IPHONE_6) {
        [self.btnSavePaymentInfo.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:19]];
    }
    if (IS_IPHONE_6P) {
        
        [self.btnSavePaymentInfo.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:21]];

    }
    
    NSLog(@"card no length is is...%lu",(unsigned long)[myAppDelegate.userData.paymentData.card_number length]);
    
    NSLog(@"card no is...%@",myAppDelegate.userData.paymentData.card_number);
    
    if (myAppDelegate.userData.paymentData) {
        
        NSUInteger length = [myAppDelegate.userData.paymentData.card_number length];
        
        NSMutableArray *array = [NSMutableArray array];
        int j = 0;
        for (int i = 0; i < 4; i++) {
            NSString *ch;
            if (length == 15 && j == 12)
            {
                ch = [myAppDelegate.userData.paymentData.card_number substringWithRange:NSMakeRange(j, 3)];
            }
            else
            {
            ch = [myAppDelegate.userData.paymentData.card_number substringWithRange:NSMakeRange(j, 4)];
            }
            
            j = j+4;
            
//            if (length == 15 && j == 12) {
//                
//                j = j-1;
//            }
            
        
            [array addObject:ch];
            
        }
        self.textFieldCardDigit1To4.text = [array objectAtIndex:0];
        self.textFieldCardDigit5To8.text = [array objectAtIndex:1];
        self.textFieldCardDigit9To12.text = [array objectAtIndex:2];
        self.textFieldCardDigit13To16.text = [array objectAtIndex:3];

        
//        self.textFieldCVVNO.text = myAppDelegate.userData.paymentData.cvv_number;
        
        NSLog(@"value of month is... is.... %@",myAppDelegate.userData.paymentData.expiry_month);
        
        self.textFieldExpirationDateYYYY.text = myAppDelegate.userData.paymentData.expiry_year;
        self.textFieldExpirationDateMM.text = myAppDelegate.userData.paymentData.expiry_month;

        self.lblMonth.hidden = true;
        self.imgViewArrow.hidden = true;
        
        self.viewMonthLabelHolder.backgroundColor = [UIColor clearColor];
        self.viewHolderForTableExpirationDateMonth.backgroundColor = [UIColor clearColor];
        
        self.lblYear.hidden = true;
        self.imgArrowYear.hidden = true;
        
        self.viewHolderForExpirationDateYear.backgroundColor = [UIColor clearColor];
        self.viewYearLabelHolder.backgroundColor = [UIColor clearColor];

        self.textFieldNameOnCard.text = myAppDelegate.userData.paymentData.card_holder_name;
        
        if ([myAppDelegate.userData.paymentData.payment_method isEqualToString:@"0"]) {
            [[self.btnCreditCardCollection objectAtIndex:0] setSelected:YES];
        }else if ([myAppDelegate.userData.paymentData.payment_method isEqualToString:@"1"]) {
            [[self.btnCreditCardCollection objectAtIndex:1] setSelected:YES];
        }else if ([myAppDelegate.userData.paymentData.payment_method isEqualToString:@"2"]) {
            [[self.btnCreditCardCollection objectAtIndex:2] setSelected:YES];
        }else if ([myAppDelegate.userData.paymentData.payment_method isEqualToString:@"3"]) {
            [[self.btnCreditCardCollection objectAtIndex:3] setSelected:YES];
            AmericanCardBtnClicked = YES;
        }
        
    }

    //scan view tap
    UITapGestureRecognizer *scanViewTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showScanView:)];
    scanViewTap.delegate = self;
    scanViewTap.numberOfTapsRequired = 1;
    [self.viewBgHolder addGestureRecognizer:scanViewTap];
    
    
   
    
    //........ PayPal future payment integeration....
    
   // [PayPalMobile preconnectWithEnvironment:PayPalEnvironmentProduction]; //
    
//     [PayPalMobile preconnectWithEnvironment:PayPalEnvironmentSandbox];
//    
//    _payPalconfig = [[PayPalConfiguration alloc]init];
//    
//    _payPalconfig.acceptCreditCards = YES;
//    _payPalconfig.merchantName = @"Apple corp.";
//    
//    _payPalconfig.merchantPrivacyPolicyURL = [NSURL URLWithString:@"https://www.paypal.com/webapps/mpp/ua/privacy-full"];
//    _payPalconfig.merchantUserAgreementURL = [NSURL URLWithString:@"https://www.paypal.com/webapps/mpp/ua/useragreement-full"];
//    
//    
//    //   _payPalconfig.merchantPrivacyPolicyURL = [NSURL URLWithString:@"https://www.omega.supreme.example/privacy"];
//    //  _payPalconfig.merchantUserAgreementURL = [NSURL URLWithString:@"https://www.omega.supreme.example/user_agreement"];
//    
//        _payPalconfig.languageOrLocale = [NSLocale preferredLanguages][0];
//    
//        //   _payPalconfig.payPalShippingAddressOption = PayPalShippingAddressOptionPayPal;
//    
//        _payPalconfig.payPalShippingAddressOption = PayPalShippingAddressOptionNone;
//    
//    _payPalconfig.rememberUser = YES;
//        _environment = kPayPalEnvironment;
//    
//        NSLog(@"pay pal sdk is...%@",[PayPalMobile libraryVersion]);
//    
//    
//    
//    _payPalconfig.languageOrLocale = [NSLocale preferredLanguages][0];
//    
//    //   _payPalconfig.payPalShippingAddressOption = PayPalShippingAddressOptionPayPal;
//    
//    _payPalconfig.payPalShippingAddressOption = PayPalShippingAddressOptionNone;
//    
//    
//    _environment = kPayPalEnvironment;
//    
//    NSLog(@"pay pal sdk is...%@",[PayPalMobile libraryVersion]);

    
    
    
   
    
    
    
   //........................... end integeration..........
    
    
    // below commented work of paypal and card option work on 12 June 18....bcz according to new approach only paypal work not use stripe.. // VS
    
//    self.ImageViewUnderPayPalView.alpha = 0.98f;
//    
//    self.SubViewUnderPayPalView.layer.cornerRadius = 5;
//    self.SubViewUnderPayPalView.layer.masksToBounds = true;
//    
//    self.SubViewUnderPayPalView.layer.borderWidth = 1.0f;
//    self.SubViewUnderPayPalView.layer.borderColor = [UIColor colorWithRed:63/255.0 green:67/255.0 blue:70/255.0 alpha:1.0].CGColor;
//    
//    self.cardImageView.layer.cornerRadius = 5.0f;
//    
//    
//    
//    NSMutableAttributedString *attString =
//    [[NSMutableAttributedString alloc]
//     initWithString: @"Continue with PayPal"];
//    
//    [attString addAttribute: NSFontAttributeName
//                      value: [UIFont fontWithName:@"Lato-Regular" size:18]
//                      range: NSMakeRange(0,13)];
//    
//    
//    [attString addAttribute: NSFontAttributeName
//                      value:  [UIFont fontWithName:@"Lato-Regular" size:23]
//                      range: NSMakeRange(14,6)];
//
//    [attString addAttribute: NSForegroundColorAttributeName
//                      value: [UIColor colorWithRed:63/255.0 green:67/255.0 blue:70/255.0 alpha:1.0]
//                      range: NSMakeRange(0,20)];
//    
//    [self.BtnContinueWithPayPal setAttributedTitle:attString forState:UIControlStateNormal];
//    
//    NSMutableAttributedString *attString1 =
//    [[NSMutableAttributedString alloc]
//     initWithString: @"Continue with Card"];
//    
//    [attString1 addAttribute: NSFontAttributeName
//                      value: [UIFont fontWithName:@"Lato-Regular" size:18]
//                      range: NSMakeRange(0,13)];
//    
//    
//    [attString1 addAttribute: NSFontAttributeName
//                      value:  [UIFont fontWithName:@"Lato-Regular" size:23]
//                      range: NSMakeRange(14,4)];
//    
//    [attString1 addAttribute: NSForegroundColorAttributeName
//                      value: [UIColor colorWithRed:63/255.0 green:67/255.0 blue:70/255.0 alpha:1.0]
//                      range: NSMakeRange(0,18)];
//    
//    [self.BtnContinueWithCard setAttributedTitle:attString1 forState:UIControlStateNormal];

   //   [myAppDelegate.vcObj startCheckout];
    
//    self.braintreeClient = [[BTAPIClient alloc] initWithAuthorization:@"eyJ2ZXJzaW9uIjoyLCJhdXRob3JpemF0aW9uRmluZ2VycHJpbnQiOiI3MjUxODQ2ZTE3YTFkOGIyYjI1OWRjMTBjYmU5NTUyYzNmNmRhNDQ3ODg4MzQ5ZDg4YjI3OTg1YTRhZGQ3NTZhfGNsaWVudF9pZD1jbGllbnRfaWQkc2FuZGJveCQ0ZHByYmZjNnBoNTk1Y2NqXHUwMDI2Y3JlYXRlZF9hdD0yMDE4LTA3LTA5VDA2OjA4OjQyLjI5NjExMzg3NyswMDAwXHUwMDI2bWVyY2hhbnRfaWQ9NHB0eXBjYjZxNGI1MnFkZCIsImNvbmZpZ1VybCI6Imh0dHBzOi8vYXBpLnNhbmRib3guYnJhaW50cmVlZ2F0ZXdheS5jb206NDQzL21lcmNoYW50cy80cHR5cGNiNnE0YjUycWRkL2NsaWVudF9hcGkvdjEvY29uZmlndXJhdGlvbiIsImNoYWxsZW5nZXMiOltdLCJlbnZpcm9ubWVudCI6InNhbmRib3giLCJjbGllbnRBcGlVcmwiOiJodHRwczovL2FwaS5zYW5kYm94LmJyYWludHJlZWdhdGV3YXkuY29tOjQ0My9tZXJjaGFudHMvNHB0eXBjYjZxNGI1MnFkZC9jbGllbnRfYXBpIiwiYXNzZXRzVXJsIjoiaHR0cHM6Ly9hc3NldHMuYnJhaW50cmVlZ2F0ZXdheS5jb20iLCJhdXRoVXJsIjoiaHR0cHM6Ly9hdXRoLnZlbm1vLnNhbmRib3guYnJhaW50cmVlZ2F0ZXdheS5jb20iLCJhbmFseXRpY3MiOnsidXJsIjoiaHR0cHM6Ly9vcmlnaW4tYW5hbHl0aWNzLXNhbmQuc2FuZGJveC5icmFpbnRyZWUtYXBpLmNvbS80cHR5cGNiNnE0YjUycWRkIn0sInRocmVlRFNlY3VyZUVuYWJsZWQiOmZhbHNlLCJwYXlwYWxFbmFibGVkIjp0cnVlLCJwYXlwYWwiOnsiZGlzcGxheU5hbWUiOiJ0ZXN0IGZhY2lsaXRhdG9yJ3MgVGVzdCBTdG9yZSIsImNsaWVudElkIjoiQVFoZW5UVW55SnhwWVV0VE9ITWxEZDdQOTloYkwzSXhibGlqVTk5MGNmczJuMGEzU2lFbTVOWXNiZjA1dUZ4WVltdF9CY2w2UENZVWo3SnYiLCJwcml2YWN5VXJsIjoiaHR0cHM6Ly9leGFtcGxlLmNvbSIsInVzZXJBZ3JlZW1lbnRVcmwiOiJodHRwczovL2V4YW1wbGUuY29tIiwiYmFzZVVybCI6Imh0dHBzOi8vYXNzZXRzLmJyYWludHJlZWdhdGV3YXkuY29tIiwiYXNzZXRzVXJsIjoiaHR0cHM6Ly9jaGVja291dC5wYXlwYWwuY29tIiwiZGlyZWN0QmFzZVVybCI6bnVsbCwiYWxsb3dIdHRwIjp0cnVlLCJlbnZpcm9ubWVudE5vTmV0d29yayI6ZmFsc2UsImVudmlyb25tZW50Ijoib2ZmbGluZSIsInVudmV0dGVkTWVyY2hhbnQiOmZhbHNlLCJicmFpbnRyZWVDbGllbnRJZCI6Im1hc3RlcmNsaWVudDMiLCJiaWxsaW5nQWdyZWVtZW50c0VuYWJsZWQiOnRydWUsIm1lcmNoYW50QWNjb3VudElkIjoiVVNEIiwiY3VycmVuY3lJc29Db2RlIjoiVVNEIn0sIm1lcmNoYW50SWQiOiI0cHR5cGNiNnE0YjUycWRkIiwidmVubW8iOiJvZmYifQ=="];
//    
//    NSLog(@"authorization code is...%@",myAppDelegate.Authorize_String_BrainTree);
//    
//        self.braintreeClient = [[BTAPIClient alloc] initWithAuthorization:myAppDelegate.Authorize_String_BrainTree];
  //  [GlobalFunction addIndicatorView];
    
  //  [self startCheckout];
   
  //  [self performSelectorOnMainThread:@selector(startCheckout) withObject:nil waitUntilDone:YES];

    

}

-(void)viewWillDisappear:(BOOL)animated
{
    [GlobalFunction removeIndicatorView];


}

-(void)viewDidDisappear:(BOOL)animated
{
    [GlobalFunction removeIndicatorView];

    NSLog(@"Send this device data to your server");
    
}

- (void)startCheckout {
    
    // Example: Initialize BTAPIClient, if you haven't already
    deviceData = @"";
    nonce = @"";
    
    deviceData = [PPDataCollector collectPayPalDeviceData];
    NSLog(@"Send this device data to your server: %@", deviceData);
    
    self.payPalDriver = [[BTPayPalDriver alloc] initWithAPIClient:self.braintreeClient];
    BTPayPalDriver *payPalDriver = [[BTPayPalDriver alloc] initWithAPIClient:self.braintreeClient];
    self.payPalDriver.viewControllerPresentingDelegate = myAppDelegate.vcObj;
    //self.payPalDriver.appSwitchDelegate = self; // Optional
    
    BTPayPalRequest *checkout = [[BTPayPalRequest alloc] init];
    
//    [self.payPalDriver authorizeAccountWithAdditionalScopes:[NSSet setWithArray:@[@"address"]]
//                                            completion:^(BTPayPalAccountNonce *tokenizedPayPalAccount, NSError *error) {
//        if (tokenizedPayPalAccount) {
//            BTPostalAddress *address = tokenizedPayPalAccount.billingAddress ?: tokenizedPayPalAccount.shippingAddress;
//            NSLog(@"nonce is...%@",tokenizedPayPalAccount.nonce);
//            NSLog(@"Address:\n%@\n%@\n%@ %@\n%@ %@", address.streetAddress, address.extendedAddress, address.locality,address.region, address.postalCode, address.countryCodeAlpha2);
//        } else if (error) {
//                            // Handle error
//             NSLog(@"...Error occured...%@",error);
//        } else {
//                            // User canceled
//            NSLog(@"...Use hit cancell button...");
//        }
//        }];
    
    
    checkout.billingAgreementDescription = @"Your agreement description";
   
    [self.payPalDriver requestBillingAgreement:checkout completion:^(BTPayPalAccountNonce * _Nullable tokenizedPayPalCheckout, NSError * _Nullable error) {
        if (error)
        {
            NSLog(@"error occuewd");
            [self errorShow:[NSString stringWithFormat:@"%@",error]];
            
        } else if (tokenizedPayPalCheckout) {
            
            nonce = tokenizedPayPalCheckout.nonce;
            
            NSLog(@"Got a nonce! %@",nonce);

            NSLog(@"Got a type is! %@", tokenizedPayPalCheckout.type);

            NSLog(@"Got a type is! %@", tokenizedPayPalCheckout.payerId);

            BTPostalAddress *address = tokenizedPayPalCheckout.billingAddress?: tokenizedPayPalCheckout.shippingAddress;
            NSLog(@"Billing address:\n%@\n%@\n%@ %@\n%@ %@", address.streetAddress, address.extendedAddress, address.locality, address.region, address.postalCode, address.countryCodeAlpha2);
            [GlobalFunction addIndicatorView];
            
            [self sendAuthorizationToServer:deviceData nonce:nonce];
            
        } else
        {
            NSLog(@"user clicked cancelled");
             [self back];
        }
    }];
    
    
   
    
}

-(void) errorShow:(NSString *)message
{
     UIAlertView *alertViewErr = [[UIAlertView alloc]initWithTitle:@"BUY TIMEE" message:message delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    
    [alertViewErr show];
    alertViewErr.tag = 1111;
}

- (void)appSwitcher:(id)appSwitcher didPerformSwitchToTarget:(BTAppSwitchTarget)target
{
    
}


#pragma mark - BTViewControllerPresentingDelegate

// Required
- (void)paymentDriver:(id)paymentDriver
requestsPresentationOfViewController:(UIViewController *)viewController {
    [self presentViewController:viewController animated:YES completion:nil];
}

// Required
- (void)paymentDriver:(id)paymentDriver
requestsDismissalOfViewController:(UIViewController *)viewController {
    [viewController dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - BTAppSwitchDelegate

// Optional - display and hide loading indicator UI
- (void)appSwitcherWillPerformAppSwitch:(id)appSwitcher {
    [self showLoadingUI];
    
    // You may also want to subscribe to UIApplicationDidBecomeActiveNotification
    // to dismiss the UI when a customer manually switches back to your app since
    // the payment button completion block will not be invoked in that case (e.g.
    // customer switches back via iOS Task Manager)
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(hideLoadingUI:)
                                                 name:UIApplicationDidBecomeActiveNotification
                                               object:nil];
}

- (void)appSwitcherWillProcessPaymentInfo:(id)appSwitcher {
    [self hideLoadingUI:nil];
}

#pragma mark - Private methods

- (void)showLoadingUI {
    //...
}

- (void)hideLoadingUI:(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIApplicationDidBecomeActiveNotification
                                                  object:nil];
    //....
}

-(void)viewWillAppear:(BOOL)animated
{
   // [GlobalFunction addIndicatorView];
    
    // Do any additional setup after loading the view, typically from a nib.
    
//    [PayPalMobile preconnectWithEnvironment:PayPalEnvironmentSandbox]; //
//    
//    // [PayPalMobile preconnectWithEnvironment:PayPalEnvironmentNoNetwork];
//    
//    _payPalconfig = [[PayPalConfiguration alloc]init];
//    
//    _payPalconfig.acceptCreditCards = YES;
//    _payPalconfig.merchantName = @"Apple corp.";
//    
//    _payPalconfig.merchantPrivacyPolicyURL = [NSURL URLWithString:@"https://www.paypal.com/webapps/mpp/ua/privacy-full"];
//    _payPalconfig.merchantUserAgreementURL = [NSURL URLWithString:@"https://www.paypal.com/webapps/mpp/ua/useragreement-full"];
//    
//    
//    //   _payPalconfig.merchantPrivacyPolicyURL = [NSURL URLWithString:@"https://www.omega.supreme.example/privacy"];
//    //  _payPalconfig.merchantUserAgreementURL = [NSURL URLWithString:@"https://www.omega.supreme.example/user_agreement"];
//    
//    
//    _payPalconfig.languageOrLocale = [NSLocale preferredLanguages][0];
//    
//    //   _payPalconfig.payPalShippingAddressOption = PayPalShippingAddressOptionPayPal;
//    
//    _payPalconfig.payPalShippingAddressOption = PayPalShippingAddressOptionNone;
//    
//    
//    _environment = kPayPalEnvironment;
//    
//    NSLog(@"pay pal sdk is...%@",[PayPalMobile libraryVersion]);
    

}
//- (void)setPayPalEnvironment:(NSString *)environment {
//   // self.environment = environment;
//   // [PayPalMobile preconnectWithEnvironment:environment];
////    
////    
//    // Choose whichever scope-values apply in your case. See `PayPalOAuthScopes.h` for a complete list of available scope-values.
//    NSSet *scopeValues = [NSSet setWithArray:@[kPayPalOAuth2ScopeOpenId, kPayPalOAuth2ScopeEmail, kPayPalOAuth2ScopeAddress, kPayPalOAuth2ScopePhone]];
//    
//    PayPalProfileSharingViewController *psViewController;
//    psViewController = [[PayPalProfileSharingViewController alloc] initWithScopeValues:scopeValues
//                                                                         configuration:self.payPalconfig
//                                                                              delegate:self];
//    
//    // Present the PayPalProfileSharingViewController
//    [self presentViewController:psViewController animated:YES completion:nil];
//    
//    
//}

//-(void)StartFuturePaymentWork
//{
//    metadataID = @"";
//    projectDictionary1 = @"";
//    
//
//    PayPalFuturePaymentViewController *fpViewController;
//    fpViewController = [[PayPalFuturePaymentViewController alloc] initWithConfiguration:self.payPalconfig
//                                                                               delegate:self];
//    
//    // Present the PayPalFuturePaymentViewController
//    [self presentViewController:fpViewController animated:YES completion:nil];
//    
//
//}

#pragma mark - PayPalProfileSharingDelegate methods

//- (void)userDidCancelPayPalProfileSharingViewController:(PayPalProfileSharingViewController *)profileSharingViewController {
//    // User cancelled login. Dismiss the PayPalProfileSharingViewController, breathe deeply.
//    [self dismissViewControllerAnimated:YES completion:nil];
//}
//
//- (void)payPalProfileSharingViewController:(PayPalProfileSharingViewController *)profileSharingViewController
//             userDidLogInWithAuthorization:(NSDictionary *)profileSharingAuthorization {
//    // The user has successfully logged into PayPal, and has consented to profile sharing.
//    
//    // Your code must now send the authorization response to your server.
//    [self sendAuthorizationToServer:profileSharingAuthorization];
//    
//    // Be sure to dismiss the PayPalProfileSharingViewController.
//    [self dismissViewControllerAnimated:YES completion:nil];
//}


-(void)showScanView:(UIGestureRecognizer *)gesture{
    
    [self closeCVVInfo:self.btnCloseCVV];
    
}

-(void)viewDidAppear:(BOOL)animated{
    
 // [GlobalFunction addIndicatorView];
    
// [self performSelectorOnMainThread:@selector(startCheckout) withObject:nil waitUntilDone:YES];
 //   return;
    if (! backFromPayPal)
    {
        //[self StartFuturePaymentWork];
        // [self setPayPalEnvironment:@"g"];
    }
  
    
    NSString* final = @"Payment Information page";
    
    [Flurry logEvent:final];
    
    [self.view layoutIfNeeded];
    _btnSavePaymentInfo.backgroundColor=[UIColor colorWithRed:63.0/255.0f green:67.0/255.0f blue:70.0/255.0f alpha:1.0];
    
    NSAttributedString *str1 = [[NSAttributedString alloc] initWithString:@"Name" attributes:@{ NSForegroundColorAttributeName :[UIColor colorWithRed:143.0/255.0f green:143.0/255.0f blue:143.0/255.0f alpha:1.0],
                                                                                                       }];
    _textFieldNameOnCard.attributedPlaceholder = str1;

    
    NSAttributedString *str2 = [[NSAttributedString alloc] initWithString:@"XXXX" attributes:@{ NSForegroundColorAttributeName :[UIColor colorWithRed:143.0/255.0f green:143.0/255.0f blue:143.0/255.0f alpha:1.0],
                                                                                                }];
    _textFieldCardDigit1To4.attributedPlaceholder = str2;

    NSAttributedString *str3 = [[NSAttributedString alloc] initWithString:@"XXXX" attributes:@{ NSForegroundColorAttributeName :[UIColor colorWithRed:143.0/255.0f green:143.0/255.0f blue:143.0/255.0f alpha:1.0],
                                                                                                }];
    _textFieldCardDigit5To8.attributedPlaceholder = str3;

    
    
    NSAttributedString *str4 = [[NSAttributedString alloc] initWithString:@"XXXX" attributes:@{ NSForegroundColorAttributeName :[UIColor colorWithRed:143.0/255.0f green:143.0/255.0f blue:143.0/255.0f alpha:1.0],
                                                                                                }];
    _textFieldCardDigit9To12.attributedPlaceholder = str4;

    
    NSAttributedString *str5 = [[NSAttributedString alloc] initWithString:@"XXXX" attributes:@{ NSForegroundColorAttributeName :[UIColor colorWithRed:143.0/255.0f green:143.0/255.0f blue:143.0/255.0f alpha:1.0],
                                                                                                }];
    _textFieldCardDigit13To16.attributedPlaceholder = str5;
    
    NSAttributedString *star = [[NSAttributedString alloc] initWithString:@"*" attributes:@{ NSForegroundColorAttributeName :[UIColor redColor],
                                                                                             }];
    NSMutableAttributedString *str6 = [[NSMutableAttributedString alloc] initWithString:@"CVV/CVV2" attributes:@{ NSForegroundColorAttributeName :[UIColor colorWithRed:143.0/255.0f green:143.0/255.0f blue:143.0/255.0f alpha:1.0],
                                                                                                }];
    [str6 appendAttributedString:star];
    
    _textFieldCVVNO.attributedPlaceholder = str6;
    
//    NSAttributedString *star = [[NSAttributedString alloc] initWithString:@"*" attributes:@{ NSForegroundColorAttributeName :[UIColor redColor],
//                                                                                             }];
    
    NSMutableAttributedString *str11 = [[NSMutableAttributedString alloc] initWithString:@"Choose your payment method" attributes:@{ NSForegroundColorAttributeName :[UIColor colorWithRed:143.0/255.0f green:143.0/255.0f blue:143.0/255.0f alpha:1.0],
                                                                                                                                    }];
    
    NSMutableAttributedString *str22 = [[NSMutableAttributedString alloc] initWithString:@"Full name on card" attributes:@{ NSForegroundColorAttributeName :[UIColor colorWithRed:143.0/255.0f green:143.0/255.0f blue:143.0/255.0f alpha:1.0],
                                                                                                                           }];
    
    NSMutableAttributedString *str33 = [[NSMutableAttributedString alloc] initWithString:@"Expiry Date" attributes:@{ NSForegroundColorAttributeName :[UIColor colorWithRed:143.0/255.0f green:143.0/255.0f blue:143.0/255.0f alpha:1.0],
                                                                                                                     }];
    NSMutableAttributedString *str44 = [[NSMutableAttributedString alloc] initWithString:@"Card Number (16 digits on the front of card)" attributes:@{ NSForegroundColorAttributeName :[UIColor colorWithRed:143.0/255.0f green:143.0/255.0f blue:143.0/255.0f alpha:1.0],
                                                                                                                                                      }];
    
    [str11 appendAttributedString: star];
    [str22 appendAttributedString: star];
    
    [str33 appendAttributedString: star];
    
    [str44 appendAttributedString: star];
    
    
    self.lblChooseYourPaymentMethod.attributedText = str11;
    self.lblFullNameOnCard.attributedText = str22;
    self.lblExpiryDate.attributedText = str33;
    self.lblCardNo.attributedText = str44;
    

    if (IS_IPHONE_4_OR_LESS) {
        
        self.constraintBtnSavePaymentBtnInfoHeight.constant = 48;
        
        self.constraintBtnVisaBottomSpace.constant = 7;
        self.constraintLblChoosePaymentTopSpace.constant = 7;
        self.constraintViewCreditCardHolderHeight.constant = 97;
        
        self.constraintLogoTopSpaceConstraintTopSpace.constant = 30;
        self.constraintLogoBottomSpace.constant = 10;
        self.constraintLblPaymentInfoBottomSpace.constant = 10;
        self.constraintViewLogoHolderHeight.constant = 117;
        
        self.constraintTextFeildHeightMaxValue.constant = 30;
        self.constraintTextFieldHeight.constant = 20;
        
        self.constraintViewPaymentInfoHolderTopSpace.constant = 15;
        self.constraintTextFieldMonthBottomSpace.constant = 15;
        
        self.LblYouNeedToAdd.font=[self.LblYouNeedToAdd.font fontWithSize:14];
        self.buyTimeLbl.font=[self.buyTimeLbl.font fontWithSize:19];
        
        _buyTimeLabelLeft.constant=20;
        _buyTimeLblTop.constant=17;
        
        _backBtnTop.constant=10;
        
        _headerUnderLineTop.constant=3;
        
        _headingLblTop.constant=75;
        _headingLblLeft.constant=10;
        _headingLblHeight.constant=50;
        
        //** by kp
        
        _consLblYouNeedTop.constant=70;
        _consGrayViewTop.constant=10;
    
        
        
    }
    if (IS_IPHONE_5) {
        
        self.LblYouNeedToAdd.font=[self.LblYouNeedToAdd.font fontWithSize:14];
        self.buyTimeLbl.font=[self.buyTimeLbl.font fontWithSize:19];
//
//        self.consImageViewYouNeedLeading.constant = 39;
//        self.consImageViewYouNeedWidth.constant = 240;
//        self.consImageViewYouNeedHeight.constant = 36;
//        self.consImageViewYouNeedTop.constant = 133;
        
        self.constraintBtnSavePaymentBtnInfoHeight.constant = 37;
        
        self.constraintBtnVisaBottomSpace.constant = 10;
        self.constraintLblChoosePaymentTopSpace.constant = 10;
        self.constraintViewCreditCardHolderHeight.constant = 103;
        
//        self.constraintLogoTopSpaceConstraintTopSpace.constant = 40;
//        self.constraintLogoBottomSpace.constant = 15;
//        self.constraintLblPaymentInfoBottomSpace.constant = 15;
        self.constraintViewLogoHolderHeight.constant = 120;//137;
        
        self.constraintTextFeildHeightMaxValue.constant = 30;
        self.constraintTextFieldHeight.constant = 28;
        
        self.constraintViewPaymentInfoHolderTopSpace.constant = 20;
        self.constraintTextFieldMonthBottomSpace.constant = 25;//20;
        
        
        _buyTimeLabelLeft.constant=20;
        _buyTimeLblTop.constant=17;
        
        _backBtnTop.constant=10;
        
        _headerUnderLineTop.constant=3;
        
        _headingLblTop.constant=75;
        _headingLblLeft.constant=10;
        _headingLblHeight.constant=50;
        
        
        
    }
    if (IS_IPHONE_6) {
        
        self.constraintBtnSavePaymentBtnInfoHeight.constant =45;
        
        self.constraintBtnVisaBottomSpace.constant = 21;
        self.constraintLblChoosePaymentTopSpace.constant = 19;
        self.constraintViewCreditCardHolderHeight.constant = 123;
        
        self.constraintLogoTopSpaceConstraintTopSpace.constant = 50;
        self.constraintLogoBottomSpace.constant = 24;
        self.constraintLblPaymentInfoBottomSpace.constant = 28;
        self.constraintViewLogoHolderHeight.constant = 150;//169;
        
        self.constraintTextFeildHeightMaxValue.constant = 35;
        self.constraintTextFieldHeight.constant = 30;
        
        self.constraintViewPaymentInfoHolderTopSpace.constant = 30;
        self.constraintTextFieldMonthBottomSpace.constant = 40;
        
        _buyTimeLblTop.constant=1;
        _backBtnTop.constant=15;
        _headerUnderLineTop.constant=14;
        _buyTimeLabelLeft.constant=47;
        
        self.buyTimeLbl.font=[self.buyTimeLbl.font fontWithSize:22];
        
        self.LblYouNeedToAdd.font=[self.LblYouNeedToAdd.font fontWithSize:16];


        _headingLblTop.constant=91;
        _headingLblLeft.constant=15;
        _headingLblHeight.constant=50;
        
        

        
        
        
    }
    
    
    
    
    if (IS_IPHONE_6P) {
        
        
        self.constraintBtnSavePaymentBtnInfoHeight.constant = 50;
        
        self.constraintBtnVisaBottomSpace.constant = 21;
        self.constraintLblChoosePaymentTopSpace.constant = 19;
        self.constraintViewCreditCardHolderHeight.constant = 123;//123;
        
        self.constraintLogoTopSpaceConstraintTopSpace.constant = 50;
        self.constraintLogoBottomSpace.constant = 24;
        self.constraintLblPaymentInfoBottomSpace.constant = 28;
        self.constraintViewLogoHolderHeight.constant = 145;//169;
        
        self.constraintTextFeildHeightMaxValue.constant = 40;
        self.constraintTextFieldHeight.constant = 35;
        
        self.constraintViewPaymentInfoHolderTopSpace.constant = 60;
        self.constraintTextFieldMonthBottomSpace.constant = 70;
        _buyTimeLblTop.constant=9;
        _backBtnTop.constant=14;
        _headerUnderLineTop.constant=22;
        _buyTimeLabelLeft.constant=61;
        
        self.buyTimeLbl.font=[self.buyTimeLbl.font fontWithSize:24];
        
        self.LblYouNeedToAdd.font=[self.LblYouNeedToAdd.font fontWithSize:19];
        
        
        _headingLblTop.constant=102;
        _headingLblLeft.constant=12;
        _headingLblHeight.constant=50;
        
        _buyTimeLblWidth.constant=190;
        
        
        
        
        

    }
    
    [self.view layoutIfNeeded];
    
    self.imgViewCVVInfo.transform = CGAffineTransformMakeScale(0.0, 0.0);
    
    centerToGoBack = [self.viewTextFieldHolder convertPoint:self.btnInfo.center toView:nil];
    
    [self.tableViewMonth layoutIfNeeded];
    [self.tableViewMonth reloadData];
    [self.tableViewMonth layoutIfNeeded];
    
    [self.tableViewYear layoutIfNeeded];
    [self.tableViewYear reloadData];
    [self.tableViewYear layoutIfNeeded];
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if (tableView == self.tableViewMonth) {
        return 12;
    }
    else if (tableView == self.tableViewYear) {
        return 50;
    }
    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    MonthAndYearTableViewCell *cell;
    
    cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    
    if (tableView == self.tableViewMonth) {
        
        cell.lblMonthAndYear.text = [monthArray objectAtIndex:indexPath.row];
        
    }else if(tableView == self.tableViewYear){
        
        cell.lblMonthAndYear.text = [yearArray objectAtIndex:indexPath.row];
    }
    
    return cell;
    
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    
    
    
    
    

   if (tableView == self.tableViewMonth) {
       
        self.textFieldExpirationDateMM.text = [monthArray objectAtIndex:indexPath.row];
        self.lblMonth.hidden = true;
        self.imgViewArrow.hidden = true;
        self.viewMonthLabelHolder.backgroundColor = [UIColor clearColor];
        self.viewHolderForTableExpirationDateMonth.backgroundColor = [UIColor clearColor];
        [self closeMonthTableView];
       
    }
    
    
    
    else if(tableView == self.tableViewYear)
    
    {
        self.textFieldExpirationDateYYYY.text = [yearArray objectAtIndex:indexPath.row];
        self.lblYear.hidden = true;
        self.imgArrowYear.hidden = true;
        self.viewHolderForExpirationDateYear.backgroundColor = [UIColor clearColor];
        self.viewYearLabelHolder.backgroundColor = [UIColor clearColor];
        [self closeYearTableView];
        
        
    }
    
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (tableView == self.tableViewMonth) {
        
        return self.textFieldExpirationDateMM.frame.size.height;
        
    }else if(tableView == self.tableViewYear){
        
        return self.textFieldExpirationDateYYYY.frame.size.height;
        
    }
    
    return 46;
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(BOOL)prefersStatusBarHidden{
    return YES;
}


-(void)switchRadioBtn:(id)sender{
    
    NSInteger tag = 0;
    
    if ([sender isKindOfClass:[UIButton class]]) {
        tag = [sender tag];
    }else{
        tag = [[sender view] tag];
    }
    
    for (UIButton *btnCreditCard in self.btnCreditCardCollection) {
        
        if (tag == btnCreditCard.tag) {
            
            [UIView transitionWithView:btnCreditCard duration:0.3 options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
                [btnCreditCard setSelected:YES];
            } completion:^(BOOL finished) {
                
            }];
            
        }else{
            
            if (btnPrevSelected == btnCreditCard) {
                
                [UIView transitionWithView:btnCreditCard duration:0.3 options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
                    [btnCreditCard setSelected:NO];
                } completion:^(BOOL finished) {
                    
                }];
                
            }else{
                
                [btnCreditCard setSelected:NO];
                
            }
            
        }
        
    }
    
}

-(void)setPrevSelectedBtn:(id)sender{
    
    NSInteger tag = 0;
    
    if ([sender isKindOfClass:[UIButton class]]) {
        tag = [sender tag];
    }else{
        tag = [[sender view] tag];
    }
    
    for (UIButton *btnCreditCard in self.btnCreditCardCollection) {
        
        if (tag == btnCreditCard.tag) {
            
            btnPrevSelected = btnCreditCard;
            
        }
    }
    
}


- (IBAction)visaSelected:(id)sender {
    
    AmericanCardBtnClicked=NO;
    
    [self switchRadioBtn:sender];
    
    [self setPrevSelectedBtn:sender];
    
}

- (IBAction)masterCardSelected:(id)sender {
    AmericanCardBtnClicked=NO;

    
    [self switchRadioBtn:sender];
    
    [self setPrevSelectedBtn:sender];
    
}

- (IBAction)discoverCardSelected:(id)sender {
    
    AmericanCardBtnClicked=NO;
    
    
    [self switchRadioBtn:sender];
    
    [self setPrevSelectedBtn:sender];
    
}

- (IBAction)americanExpressSelected:(id)sender {
    
    AmericanCardBtnClicked=YES;

    
    [self switchRadioBtn:sender];
    
    [self setPrevSelectedBtn:sender];
    
}

- (IBAction)savePaymentInfo:(id)sender {
    
    if (isTableOpened) {
        
        if (isMonthOrYear) {
            [self closeMonthTableView];
        }else{
            [self closeYearTableView];
        }
        
    }else{
        
        if (![self isCreditCardTypeSelected]) {
            
            [[GlobalFunction shared] showAlertForMessage:@"Please select card type first."];
            
            return;
        }
        
        if ([self.textFieldNameOnCard.text isEqualToString:@""]) {
            
            [[GlobalFunction shared] showAlertForMessage:@"Card Holder name can't be blank."];
            
            return;
            
        }
        
        NSString *creditCardString = [self.textFieldCardDigit1To4.text stringByAppendingString:[self.textFieldCardDigit5To8.text stringByAppendingString:[self.textFieldCardDigit9To12.text stringByAppendingString:self.textFieldCardDigit13To16.text]]];
        
        creditCardString = [creditCardString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        
        //comment by kp
        
        if ([creditCardString length] <=0) {
            
            
            [[GlobalFunction shared] showAlertForMessage:@"Credit card number is not valid."];
            
            return;
        }
        
        
//        if(AmericanCardBtnClicked==YES)
//        {
//            if ([creditCardString length] <15) {
//                [[GlobalFunction shared] showAlertForMessage:@"Please enter valid credit card number."];
//                return;
//            }
//            if ([creditCardString length] == 16) {
//                [[GlobalFunction shared] showAlertForMessage:@"Please enter valid credit card number."];
//                return;
//            }
//        }
        
        
        
        if(AmericanCardBtnClicked==YES)
        {
            if ([creditCardString length] <15 || [creditCardString length] == 16) {
                [[GlobalFunction shared] showAlertForMessage:@"Invalid credit card number. Please try again."];
                return;
            }
            if ([self.textFieldCVVNO.text length] <4) {
                [[GlobalFunction shared] showAlertForMessage:@"Please enter valid cvv number."];
                return;
            }
        }

        
        
        else{
            
            if ([creditCardString length] <16) {
                [[GlobalFunction shared] showAlertForMessage:@"Invalid credit card number. Please try again."];
                return;
            }
            if ([self.textFieldCVVNO.text length] !=3) {
                [[GlobalFunction shared] showAlertForMessage:@"Please enter valid cvv number."];
                return;
            }
            
        }
        
        if ([self.textFieldExpirationDateMM.text isEqualToString:@""]) {
            
            [[GlobalFunction shared] showAlertForMessage:@"Please select card expiry month."];
            
            return;
            
        }
        
        if ([self.textFieldExpirationDateYYYY.text isEqualToString:@""]) {
            
            [[GlobalFunction shared] showAlertForMessage:@"Please select card expiry year."];
            
            return;
        }
        
        if ([[self.textFieldCVVNO.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""]) {
            
            [[GlobalFunction shared] showAlertForMessage:@"Please enter 'CVV/CVV2' number."];
            
            return;
        }
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyyy"];
        NSString *yearString = [formatter stringFromDate:[NSDate date]];
        NSLog(@" cuurent year string.... %@ ",yearString);
        NSLog(@"selected year string.... %@ ",self.textFieldExpirationDateYYYY.text);

        
        NSDate *currentDate = [NSDate date];
        NSCalendar* calendar = [NSCalendar currentCalendar];
        NSDateComponents* components = [calendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:currentDate];
        
        NSString *monthString;
        
        if([components month]<10)
        {
            
            NSString *prefix=@"0";
            NSString *monthVal=[@([components month]) stringValue];
            monthString = [prefix stringByAppendingString:monthVal];
        }
        
        else{
            monthString=[@([components month]) stringValue];
        }
         
        NSLog(@"%@, current month string...",monthString);
        NSLog(@"%@, selected month string...",self.textFieldExpirationDateMM.text);
         
        if ( ([self.textFieldExpirationDateYYYY.text compare:yearString]) == NSOrderedAscending)
        {
   
            [[GlobalFunction shared] showAlertForMessage:@"Selected year should not be older year."];
            return;
            
    
        }
        
        if ([yearString isEqualToString:self.textFieldExpirationDateYYYY.text]) {
            
            if ( ([self.textFieldExpirationDateMM.text compare:monthString]) == NSOrderedAscending )
            {
                
                [[GlobalFunction shared] showAlertForMessage:@"Selected month should not be older month."];
                [self closeMonthTableView];
                
                return;
            }
            
        }
        
        
        
        if ([self.textFieldCardDigit1To4.text isEqualToString:@"XXXX"] || [self.textFieldCardDigit5To8.text isEqualToString:@"XXXX"] || [self.textFieldCardDigit9To12.text isEqualToString:@"XXXX"]) {
            
            [[GlobalFunction shared] showAlertForMessage:@"Please reenter your card number."];
            return;
            
        }

        
        
        
//        else{
//            
////            if ( ([self.textFieldExpirationDateMM.text compare:monthString]) == NSOrderedAscending )
////            {
////                
////                [[GlobalFunction shared] showAlertForMessage:@"Expiry month should not be older month."];
////                [self closeMonthTableView];
////                    
////                return;
////            }
//        }
        
      [GlobalFunction addIndicatorView];
      
     [self performSelector:@selector(updatePaymentInfoOnServer) withObject:nil afterDelay:0.5];
        
        
    }
    
}


-(void)updatePaymentInfoOnServer{
    
    if (![[WebService shared] connected]) {
        
        [GlobalFunction removeIndicatorView];
        
        [[GlobalFunction shared] showAlertForMessage:@"Can't connect to internet, please check and try again."];
        
        return;
    }

    NSString *creditCardString = [self.textFieldCardDigit1To4.text stringByAppendingString:[self.textFieldCardDigit5To8.text stringByAppendingString:[self.textFieldCardDigit9To12.text stringByAppendingString:self.textFieldCardDigit13To16.text]]];
    
    creditCardString = [creditCardString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    NSDictionary *dictPaymentResponse = [[WebService shared] addCardUserid:myAppDelegate.userData.userid payment_method:[self getSelectedCardType] card_holder_name:self.textFieldNameOnCard.text card_number:creditCardString expiry_month:self.textFieldExpirationDateMM.text expiry_year:self.textFieldExpirationDateYYYY.text cvv_number:self.textFieldCVVNO.text];
    
    if (dictPaymentResponse) {
        
        NSLog(@"response --- %@",[EncryptDecryptFile decrypt:[dictPaymentResponse valueForKey:@"responseMessage"]]);
        
        if ([[EncryptDecryptFile decrypt:[dictPaymentResponse valueForKey:@"responseCode"]] isEqualToString:@"200"]) {
        
            NSDictionary *main = [dictPaymentResponse valueForKey:@"data"];
            
            NSString *passCardNo = [EncryptDecryptFile decrypt:[main valueForKey:@"card_number"]];
            
            mainCard = passCardNo;
            
            NSMutableDictionary *dictTemp0 = [[NSMutableDictionary alloc]init];
            
            
            [dictTemp0 setValue:[EncryptDecryptFile EncryptIT:myAppDelegate.userData.userid] forKey:@"cardId"];
            [dictTemp0 setValue:[EncryptDecryptFile EncryptIT:myAppDelegate.userData.userid] forKey:@"userId"];
            [dictTemp0 setValue:[EncryptDecryptFile EncryptIT:[self getSelectedCardType]] forKey:@"payment_method"];
            [dictTemp0 setValue:[EncryptDecryptFile EncryptIT:self.textFieldNameOnCard.text] forKey:@"card_holder_name"];
            [dictTemp0 setValue:[EncryptDecryptFile EncryptIT:passCardNo]forKey:@"card_number"];
            [dictTemp0 setValue:[EncryptDecryptFile EncryptIT:self.textFieldExpirationDateMM.text] forKey:@"expiry_month"];
            [dictTemp0 setValue:[EncryptDecryptFile EncryptIT:self.textFieldExpirationDateYYYY.text] forKey:@"expiry_year"];
            [dictTemp0 setValue:[EncryptDecryptFile EncryptIT:self.textFieldCVVNO.text] forKey:@"cvv_number"];
            
            NSString *query = [NSString stringWithFormat:@"delete from %@",tablePayment];
            [DatabaseClass executeQuery:query];
            
            NSLog(@"receive card no is....%@",[EncryptDecryptFile decrypt:[main valueForKey:@"card_number"]]);
            

            
            query=[NSString stringWithFormat:@"insert into %@ values(?,?,?,?,?,?,?,?)",tablePayment];
            [DatabaseClass savePaymentData:[query UTF8String] fromDict:dictTemp0];
            
            NSDictionary *stripeId = [dictPaymentResponse valueForKey:@"data"];
            
            myAppDelegate.userData.customer_id = [EncryptDecryptFile decrypt:[stripeId valueForKey:@"customer_id"]];//customerId
            
              NSLog(@"receive customer id is....%@",[EncryptDecryptFile decrypt:[stripeId valueForKey:@"customer_id"]]);
            
            NSLog(@"response --- %@",[EncryptDecryptFile decrypt:[dictPaymentResponse valueForKey:@"customer_id"]]);
            
            NSLog(@"receive customer id is....%@",[EncryptDecryptFile decrypt:[main valueForKey:@"customer_id"]]);
            NSLog(@"%@",[main valueForKey:@"customer_id"]);
            
            
            NSLog(@"stripeUser id === %@",myAppDelegate.userData.customer_id);
            NSLog(@"%@",myAppDelegate.userData.userid);
            

            
//           NSLog(@"%@",[EncryptDecryptFile EncryptIT:myAppDelegate.userData.userid]);
//            NSLog(@"%@",[EncryptDecryptFile EncryptIT:myAppDelegate.userData.customer_id]);

            
            

            
            
//            [DatabaseClass updateCustomerIdInDataBase:[EncryptDecryptFile EncryptIT:@"1"] forUserId:[EncryptDecryptFile EncryptIT:myAppDelegate.userData.userid]];
            
             [DatabaseClass updateCustomerIdInDataBase:[EncryptDecryptFile EncryptIT:@"1"] forUserId:[EncryptDecryptFile EncryptIT:myAppDelegate.userData.userid]];
            
            NSMutableArray *arr = [DatabaseClass getDataFromUserData:[[NSString stringWithFormat:@"Select * from %@",tableUser] UTF8String]];
            
            myAppDelegate.userData = nil;
            myAppDelegate.userData = (user *)[arr firstObject];
            
            [myAppDelegate.vcObj.tblViewSideBar reloadData];
            
            if (myAppDelegate.isFromEdit) {
               // [myAppDelegate.settingsVCObj updateUserData];
            }

            myAppDelegate.selectedPaymentTypeFromServer = @"1";
            myAppDelegate.isStripeCardInfoFilled = true;
            myAppDelegate.ispayWithPayPallFilled = false;


            
            [self updatePaymentInfoSuccess:[EncryptDecryptFile decrypt:[dictPaymentResponse valueForKey:@"responseMessage"]]];
            
        }else{
            
            [[GlobalFunction shared] showAlertForMessage:[EncryptDecryptFile decrypt:[dictPaymentResponse valueForKey:@"responseMessage"]]];
            
            [GlobalFunction removeIndicatorView];
        }
        
    }else{
        
        [[GlobalFunction shared] showAlertForMessage:@"Server is down, please try again later."];
        
        [GlobalFunction removeIndicatorView];
        
    }
    
}

-(void)updatePaymentInfoSuccess:(NSString *)response{
    
    [GlobalFunction removeIndicatorView];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"BUY TIMEE" message:response delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    
   // alert.tag = 10001;
    
    alert.tag = 1111;
    
    
    [alert show];
    
}

-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex{
    
    if (alertView.tag == 10001) {
        
        if(buttonIndex == [alertView cancelButtonIndex]){
            
            myAppDelegate.createBioVCObj.isPaymentFilled = true;
            
            [self back];
            
            TaskTableViewCell *cell = (TaskTableViewCell *)[myAppDelegate.vcObj.tblViewSideBar cellForRowAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:0]];
            [myAppDelegate.vcObj performSegueWithIdentifier:@"createTask" sender:cell];

        }
    }
    
    
    if (alertView.tag == 1111) {
        
        if(buttonIndex == [alertView cancelButtonIndex]){
            
            backFromPayPal = true;
            [self dismissViewControllerAnimated:YES completion:nil];
            [self back];
           // [myAppDelegate.vcObj backToMainView:nil];
            
//            if (mainCard) {
//                
//                NSUInteger length = [mainCard length];
//                
//                NSMutableArray *array = [NSMutableArray array];
//                int j = 0;
//                for (int i = 0; i < 4; i++) {
//                    NSString *ch;
//                    if (length == 15 && j == 12)
//                    {
//                        ch = [mainCard substringWithRange:NSMakeRange(j, 3)];
//                    }
//                    else
//                    {
//                        ch = [mainCard substringWithRange:NSMakeRange(j, 4)];
//                    }
//                    
//                    j = j+4;
//                    
//                    //            if (length == 15 && j == 12) {
//                    //
//                    //                j = j-1;
//                    //            }
//                    
//                    
//                    [array addObject:ch];
//                    
//                }
//                self.textFieldCardDigit1To4.text = [array objectAtIndex:0];
//                self.textFieldCardDigit5To8.text = [array objectAtIndex:1];
//                self.textFieldCardDigit9To12.text = [array objectAtIndex:2];
//                self.textFieldCardDigit13To16.text = [array objectAtIndex:3];
//        
//            
//        }
    }

    
    
}
}

-(void)back{
    
    CATransition *transitionAnimation = [CATransition animation];
    transitionAnimation.type = kCATransitionFade;
    transitionAnimation.duration = 0.3;
    
    
    if (!myAppDelegate.isFromAlert) {
        myAppDelegate.isFromEdit = true;
    }
    
    myAppDelegate.isFromAlert = false;
//    CATransition *transitionAnimation = [CATransition animation];
//    [transitionAnimation setType:kCATransitionPush];
//    [transitionAnimation setSubtype:kCATransitionFromLeft];
//    [transitionAnimation setDuration:0.3f];
//    [transitionAnimation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
//    [transitionAnimation setFillMode:kCAFillModeBoth];
    [myAppDelegate.vcObj.viewHolderWithoutHeader.layer addAnimation:transitionAnimation forKey:@"changeView"];
    for(UIView *view in myAppDelegate.vcObj.viewHolderWithoutHeader.subviews){
        
        [view removeFromSuperview];
        
    }
    myAppDelegate.isPaymentORBankInfo = false;
    [myAppDelegate.vcObj.viewHolderWithoutHeader setHidden:YES];
    myAppDelegate.paymentInfoVCObj = nil;
    myAppDelegate.isSideBarAccesible = true; //vs
    
    [myAppDelegate.createBioVCObj setRadioButtonImages];
    [GlobalFunction removeIndicatorView];
    
}

- (IBAction)showCVVInfo:(id)sender {
    
    if (self.viewCardAndTextFieldHolder.frame.origin.y != 0) {
        
        CGRect frame = self.viewCardAndTextFieldHolder.frame;
        frame.origin.y = 0;
        
        [UIView animateWithDuration:0.3 animations:^{
            self.viewCardAndTextFieldHolder.frame = frame;
        } completion:^(BOOL finished) {
            
        }];
        
    }
    
    [txtActiveField resignFirstResponder];
    
    self.viewBgHolder.hidden = NO;
    self.viewBgHolder.alpha = 0.0;
    self.imgViewCVVInfo.hidden = NO;
    self.imgViewCVVInfo.alpha = 0.0;
    self.btnCloseCVV.hidden = NO;
    self.btnCloseCVV.alpha = 0.0;
    self.imgViewCVVInfo.center = centerToGoBack;
    
    [UIView animateWithDuration:0.3 animations:^{
        
        self.viewBgHolder.alpha = 0.85;
        self.imgViewCVVInfo.alpha = 1.0;
        self.btnCloseCVV.alpha = 1.0;
        self.imgViewCVVInfo.transform = CGAffineTransformMakeScale(1.0, 1.0);
        self.imgViewCVVInfo.center = self.view.center;
        
    } completion:^(BOOL finished) {
        
    }];
    
    
}

- (IBAction)closeCVVInfo:(id)sender {
    
    
    self.viewBgHolder.alpha = 0.85;
    self.imgViewCVVInfo.alpha = 1.0;
    self.btnCloseCVV.alpha = 1.0;
    self.imgViewCVVInfo.center = self.view.center;
    
    [UIView animateWithDuration:0.3 animations:^{
        
        self.viewBgHolder.alpha = 0.0;
        self.imgViewCVVInfo.alpha = 0.0;
        self.btnCloseCVV.alpha = 0.0;
        self.imgViewCVVInfo.transform = CGAffineTransformMakeScale(0.1, 0.1);
        self.imgViewCVVInfo.center = centerToGoBack;
        
    } completion:^(BOOL finished) {
        
        self.viewBgHolder.hidden = YES;
        self.imgViewCVVInfo.hidden = YES;
        self.btnCloseCVV.hidden = YES;
    }];
    
}


- (IBAction)openMonthTable:(id)sender {
    
    if (self.viewCardAndTextFieldHolder.frame.origin.y != 0) {
        
        CGRect frame = self.viewCardAndTextFieldHolder.frame;
        frame.origin.y = 0;
        
        [UIView animateWithDuration:0.3 animations:^{
            self.viewCardAndTextFieldHolder.frame = frame;
        } completion:^(BOOL finished) {
            
        }];
        
    }
    [txtActiveField resignFirstResponder];
    
    if (self.constraintViewExpirationDateYearHeight.constant != 0) {
        [self closeYearTableView];
    }
    
    if (self.constraintViewExpirationDateMonthHeight.constant == 0) {
        
        isTableOpened = true;
        
        [self.view layoutIfNeeded];
        
        self.tableViewMonth.hidden = false;
        self.tableViewMonth.alpha = 0.0;
        
        self.constraintMonthLblHolderHeight.constant = -122;
        
        self.constraintViewExpirationDateMonthHeight.constant = - 120;
        
        [UIView animateWithDuration:0.5 animations:^{
            
            self.tableViewMonth.alpha = 1.0;
            
            [self.view layoutIfNeeded];
            
        } completion:^(BOOL finished) {
            
            self.viewBGTapHandler.hidden = false;
            isMonthOrYear = true;
            self.viewBGTapHandlerInCardsView.hidden = false;
            
        }];
        
    }else{
        
        [self closeMonthTableView];
        
    }
    
    
}

-(void)closeMonthTableView{
    
    isTableOpened = false;
    
    [self.view layoutIfNeeded];
    
    self.constraintViewExpirationDateMonthHeight.constant = 0;
    
    self.constraintMonthLblHolderHeight.constant = -2;
    
    [UIView animateWithDuration:0.5 animations:^{
        
        [self.view layoutIfNeeded];
        
    } completion:^(BOOL finished) {
        
        self.viewBGTapHandler.hidden = true;
        self.tableViewMonth.hidden = true;
        self.viewBGTapHandlerInCardsView.hidden = true;
        
    }];
    
}

- (IBAction)openYearTable:(id)sender {
    
    if (self.view.frame.origin.y != 0) {
        
        CGRect frame = self.viewCardAndTextFieldHolder.frame;
        frame.origin.y = 0;
        
        [UIView animateWithDuration:0.3 animations:^{
            self.viewCardAndTextFieldHolder.frame = frame;
        } completion:^(BOOL finished) {
            
        }];
        
    }
    [txtActiveField resignFirstResponder];
    
    if (self.constraintViewExpirationDateMonthHeight.constant != 0) {
        [self closeMonthTableView];
    }
    
    if (self.constraintViewExpirationDateYearHeight.constant == 0) {
        
        isTableOpened = true;
        
        [self.view layoutIfNeeded];
        
        self.tableViewYear.hidden = false;
        self.tableViewYear.alpha = 0.0;
        
        self.constraintYearLblHolderHeight.constant = -122;
        
        self.constraintViewExpirationDateYearHeight.constant = - 120;
        
        [UIView animateWithDuration:0.5 animations:^{
            
            self.tableViewYear.alpha = 1.0;
            
            [self.view layoutIfNeeded];
            
        } completion:^(BOOL finished) {
            
            self.viewBGTapHandler.hidden = false;
            isMonthOrYear = false;
            self.viewBGTapHandlerInCardsView.hidden = false;
            
        }];
        
    }else{
        
        [self closeYearTableView];
        
    }
    
}

-(void)closeYearTableView{
    
    isTableOpened = false;
    
    [self.view layoutIfNeeded];
    
    self.constraintViewExpirationDateYearHeight.constant = 0;
    
    self.constraintYearLblHolderHeight.constant = -2;
    
    [UIView animateWithDuration:0.5 animations:^{
        
        [self.view layoutIfNeeded];
        
    } completion:^(BOOL finished) {
        
        self.viewBGTapHandler.hidden = true;
        self.tableViewYear.hidden = true;
        self.viewBGTapHandlerInCardsView.hidden = true;
        
    }];
    
    
}


- (IBAction)closeTableView:(id)sender {
    
    if (isMonthOrYear) {
        [self closeMonthTableView];
    }else{
        [self closeYearTableView];
    }
    
}

- (IBAction)closeAllTableViews:(id)sender {
    
    if (isMonthOrYear) {
        [self closeMonthTableView];
    }else{
        [self closeYearTableView];
    }
    
}


#pragma mark - create accessory view
-(void)createInputAccessoryView{
    // Create the view that will play the part of the input accessory view.
    // Note that the frame width (third value in the CGRectMake method)
    // should change accordingly in landscape orientation. But we don’t care
    // about that now.
    inputAccView = [[UIView alloc] initWithFrame:CGRectMake(10.0, 0.0, self.view.frame.size.width, 40.0)];
    // Set the view’s background color. We’ ll set it here to gray. Use any color you want.
    [inputAccView setBackgroundColor:[UIColor lightGrayColor]];
    // We can play a little with transparency as well using the Alpha property. Normally
    // you can leave it unchanged.
    [inputAccView setAlpha: 0.8];
    // If you want you may set or change more properties (ex. Font, background image,e.t.c.).
    // For now, what we’ ve already done is just enough.
    // Let’s create our buttons now. First the previous button.
    btnPrev = [UIButton buttonWithType: UIButtonTypeCustom];
    // Set the button’ s frame. We will set their widths to 80px and height to 40px.
    [btnPrev setFrame: CGRectMake(0.0, 0.0, 80.0, 40.0)];
    // Title.
    [btnPrev setTitle: @"Previous" forState: UIControlStateNormal];
    // Background color.
    [btnPrev setBackgroundColor: [UIColor clearColor]];
    // You can set more properties if you need to.
    // With the following command we’ ll make the button to react in finger tapping. Note that the
    // gotoPrevTextfield method that is referred to the @selector is not yet created. We’ ll create it
    // (as well as the methods for the rest of our buttons) later.
    [btnPrev addTarget: self action: @selector(gotoPrevTextfield) forControlEvents: UIControlEventTouchUpInside];
    // Do the same for the two buttons left.
    btnNext = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnNext setFrame:CGRectMake(85.0f, 0.0f, 80.0f, 40.0f)];
    [btnNext setTitle:@"Next" forState:UIControlStateNormal];
    [btnNext setBackgroundColor:[UIColor clearColor]];
    [btnNext addTarget:self action:@selector(gotoNextTextfield) forControlEvents:UIControlEventTouchUpInside];
    btnDone = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnDone setFrame:CGRectMake(self.view.frame.size.width - 80.0f, 0.0f, 80.0f, 40.0f)];
    [btnDone setTitle:@"Done" forState:UIControlStateNormal];
    [btnDone setBackgroundColor:[UIColor clearColor]];
    [btnDone setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [btnDone addTarget:self action:@selector(doneTyping) forControlEvents:UIControlEventTouchUpInside];
    // Now that our buttons are ready we just have to add them to our view.
    [inputAccView addSubview:btnPrev];
    [inputAccView addSubview:btnNext];
    [inputAccView addSubview:btnDone];
}


-(void)gotoPrevTextfield{
    // If the active textfield is the first one, can't go to any previous
    // field so just return.
    if (txtActiveField == self.textFieldNameOnCard) {
        
        CGRect frame = self.viewCardAndTextFieldHolder.frame;
        frame.origin.y = 0;
        
        [UIView animateWithDuration:0.3 animations:^{
            self.viewCardAndTextFieldHolder.frame = frame;
        } completion:^(BOOL finished) {
            
        }];
        
        [txtActiveField resignFirstResponder];
        return;
    }
    if (txtActiveField == self.textFieldCVVNO) {
        
        CGRect frame = self.viewCardAndTextFieldHolder.frame;
        frame.origin.y = -140;
        
        [UIView animateWithDuration:0.3 animations:^{
            self.viewCardAndTextFieldHolder.frame = frame;
        } completion:^(BOOL finished) {
            
        }];
        
        [self.textFieldCVVNO resignFirstResponder];
        [self.textFieldCardDigit13To16 becomeFirstResponder];
        return;
    }
    if (txtActiveField == self.textFieldCardDigit13To16) {
        [self.textFieldCardDigit13To16 resignFirstResponder];
        [self.textFieldCardDigit9To12 becomeFirstResponder];
        return;
    }
    if (txtActiveField == self.textFieldCardDigit9To12) {
        [self.textFieldCardDigit9To12 resignFirstResponder];
        [self.textFieldCardDigit5To8 becomeFirstResponder];
        return;
    }
    if (txtActiveField == self.textFieldCardDigit5To8) {
        [self.textFieldCardDigit5To8 resignFirstResponder];
        [self.textFieldCardDigit1To4 becomeFirstResponder];
        return;
    }
    if (txtActiveField == self.textFieldCardDigit1To4) {
        
        CGRect frame = self.viewCardAndTextFieldHolder.frame;
        frame.origin.y = -50;
        
        [UIView animateWithDuration:0.3 animations:^{
            self.viewCardAndTextFieldHolder.frame = frame;
        } completion:^(BOOL finished) {
            
        }];
        
        [self.textFieldCardDigit1To4 resignFirstResponder];
        [self.textFieldNameOnCard becomeFirstResponder];
        return;
    }
    
}

-(void)gotoNextTextfield{
    // If the active textfield is the second one, there is no next so just return.
    if (txtActiveField == self.textFieldCVVNO) {
        
        CGRect frame = self.viewCardAndTextFieldHolder.frame;
        frame.origin.y = 0;
        
        CGRect frame1 = self.ImageViewYouNeed.frame;
        
         frame1.origin.y = 0;
        
        
        [UIView animateWithDuration:0.3 animations:^{
            self.viewCardAndTextFieldHolder.frame = frame;
           // self.ImageViewYouNeed.frame = frame1;
        } completion:^(BOOL finished) {
            
        }];
        
        [txtActiveField resignFirstResponder];
        return;
    }
    if (txtActiveField == self.textFieldNameOnCard) {
        
        CGRect frame = self.viewCardAndTextFieldHolder.frame;
        frame.origin.y = -140;
        
                CGRect frame1 = self.ImageViewYouNeed.frame;
        
                frame1.origin.y = -140;
        
        
        [UIView animateWithDuration:0.3 animations:^{
            self.viewCardAndTextFieldHolder.frame = frame;
           // self.ImageViewYouNeed.frame = frame1;
        } completion:^(BOOL finished) {
            
        }];
        
        [self.textFieldNameOnCard resignFirstResponder];
        [self.textFieldCardDigit1To4 becomeFirstResponder];
        return;
    }
    if (txtActiveField == self.textFieldCardDigit1To4) {
        [self.textFieldCardDigit1To4 resignFirstResponder];
        [self.textFieldCardDigit5To8 becomeFirstResponder];
        return;
    }
    if (txtActiveField == self.textFieldCardDigit5To8) {
        [self.textFieldCardDigit5To8 resignFirstResponder];
        [self.textFieldCardDigit9To12 becomeFirstResponder];
        return;
    }
    if (txtActiveField == self.textFieldCardDigit9To12) {
        [self.textFieldCardDigit9To12 resignFirstResponder];
        [self.textFieldCardDigit13To16 becomeFirstResponder];
        return;
    }
    if (txtActiveField == self.textFieldCardDigit13To16) {
        
        CGRect frame = self.viewCardAndTextFieldHolder.frame;
        frame.origin.y = -190;
        
        CGRect frame1 = self.ImageViewYouNeed.frame;
        
        frame1.origin.y = -190;
        
        [UIView animateWithDuration:0.3 animations:^{
            self.viewCardAndTextFieldHolder.frame = frame;
          //  self.ImageViewYouNeed.frame = frame1;
        } completion:^(BOOL finished) {
            
        }];
        
        [self.textFieldCardDigit13To16 resignFirstResponder];
        [self.textFieldCVVNO becomeFirstResponder];
        return;
    }
    
}

-(void)doneTyping{
    // When the "done" button is tapped, the keyboard should go away.
    // That simply means that we just have to resign our first responder.
    
    CGRect frame = self.viewCardAndTextFieldHolder.frame;
    frame.origin.y = 0;
    
    [UIView animateWithDuration:0.3 animations:^{
        self.viewCardAndTextFieldHolder.frame = frame;
        //self.ImageViewYouNeed.frame = frame;
    } completion:^(BOOL finished) {
        
    }];
    
    
    [txtActiveField resignFirstResponder];
}


#pragma mark - text field delegate
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    if (self.viewCardAndTextFieldHolder.frame.origin.y != 0) {
        
        CGRect frame = self.viewCardAndTextFieldHolder.frame;
        frame.origin.y = 0;
        
        [UIView animateWithDuration:0.3 animations:^{
            self.viewCardAndTextFieldHolder.frame = frame;
        } completion:^(BOOL finished) {
            
        }];
        
    }

    [textField resignFirstResponder];
    
    return YES;
}

-(NSString *)getSelectedCardType{
    
    NSString *result;
    
    for (UIButton *btn in self.btnCreditCardCollection) {
        if (btn.isSelected) {
            
            switch([btn tag]) {
                case 1:
                    result = @"0";
                    break;
                case 2:
                    result = @"1";
                    break;
                case 3:
                    result = @"2";
                    break;
                case 4:
                    result = @"3";
                    break;
                default:
                    result = @"unknown";
            }
            
            
            break;
        }
    }
    
    return result;
}

-(BOOL)isCreditCardTypeSelected{
    
    BOOL isCardTypeSelected = false;
    for (UIButton *btn in self.btnCreditCardCollection) {
        if (btn.isSelected) {
            isCardTypeSelected = true;
            break;
        }
    }
    
    return isCardTypeSelected;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    
    [self createInputAccessoryView];
    // Now add the view as an input accessory view to the selected textfield.
    [textField setInputAccessoryView:inputAccView];
    // Set the active field. We' ll need that if we want to move properly
    // between our textfields.
    txtActiveField = textField;
    
    if (txtActiveField == self.textFieldNameOnCard) {
        
        if(IS_IPHONE_4_OR_LESS)
        {
            CGRect frame = self.viewCardAndTextFieldHolder.frame;
            frame.origin.y = -70;
            
            [UIView animateWithDuration:0.3 animations:^{
                self.viewCardAndTextFieldHolder.frame = frame;
            } completion:^(BOOL finished) {
                
            }];

        }
        
        else
        {
            CGRect frame = self.viewCardAndTextFieldHolder.frame;
            frame.origin.y = -50;
            
            [UIView animateWithDuration:0.3 animations:^{
                self.viewCardAndTextFieldHolder.frame = frame;
            } completion:^(BOOL finished) {
                
            }];

        }
        
        
    }
    if (txtActiveField == self.textFieldCVVNO) {
        
        if (![self isCreditCardTypeSelected]) {
            
            [[GlobalFunction shared] showAlertForMessage:@"Please select card type first."];
            
            if (self.viewCardAndTextFieldHolder.frame.origin.y != 0) {
                
                CGRect frame = self.viewCardAndTextFieldHolder.frame;
                frame.origin.y = 0;
                
                [UIView animateWithDuration:0.3 animations:^{
                    self.viewCardAndTextFieldHolder.frame = frame;
                } completion:^(BOOL finished) {
                    
                }];
                
            }

            [txtActiveField resignFirstResponder];
            
            return;
        }
        
        
                CGRect frame1 = self.ImageViewYouNeed.frame; //vs
        
                frame1.origin.y = -20;
        
        
        CGRect frame = self.viewCardAndTextFieldHolder.frame;
        frame.origin.y = -190;
        self.ImageViewYouNeed.frame = frame1;
        
        [UIView animateWithDuration:0.3 animations:^{
            self.viewCardAndTextFieldHolder.frame = frame;
            
        } completion:^(BOOL finished) {
            
        }];
        
    }
    if (txtActiveField == self.textFieldCardDigit13To16 || txtActiveField == self.textFieldCardDigit9To12 || txtActiveField == self.textFieldCardDigit5To8 || txtActiveField == self.textFieldCardDigit1To4) {
        
        
        if (![self isCreditCardTypeSelected]) {
            
            [[GlobalFunction shared] showAlertForMessage:@"Please select card type first."];
            
            if (self.viewCardAndTextFieldHolder.frame.origin.y != 0) {
                
                CGRect frame = self.viewCardAndTextFieldHolder.frame;
                frame.origin.y = 0;
                
                [UIView animateWithDuration:0.3 animations:^{
                    self.viewCardAndTextFieldHolder.frame = frame;
                    
                } completion:^(BOOL finished) {
                    
                }];
                
            }

            [txtActiveField resignFirstResponder];
            
            return;
        }
        
        
        CGRect frame = self.viewCardAndTextFieldHolder.frame;
        frame.origin.y = -140;
        
        CGRect frame1 = self.ImageViewYouNeed.frame; //vs
        
        frame1.origin.y = -30;
        self.ImageViewYouNeed.frame = frame1;

        [UIView animateWithDuration:0.3 animations:^{
            self.viewCardAndTextFieldHolder.frame = frame;
        } completion:^(BOOL finished) {
            
        }];
        
    }
    
}

-(BOOL) textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
        
    //work updated at 07Apr2016
    NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    const char * _char = [newString cStringUsingEncoding:NSUTF8StringEncoding];
    int isBackSpace = strcmp(_char, "\b");
    
    if (textField==self.textFieldCardDigit1To4) {
        
        if(textField == self.textFieldCardDigit1To4 && newString.length==4)
        {
            
            [self.textFieldCardDigit1To4 setText:newString];
            [self.textFieldCardDigit5To8 becomeFirstResponder];
            return NO;
            
        }
        
        else if (newLength>4) {
            return NO;
        }
    }
    else if (textField==self.textFieldCardDigit5To8) {
        
        if (textField==self.textFieldCardDigit5To8 && newString.length==4){
            [self.textFieldCardDigit5To8 setText:newString];
            [self.textFieldCardDigit9To12 becomeFirstResponder];
            return NO;
        }
        else if (isBackSpace==-8 && [newString isEqualToString:@""]) {
            textField.text = @"";
            
            [self.textFieldCardDigit1To4 becomeFirstResponder];
            return NO;
            
        }
        
        else if (newLength>4) {
            return NO;
        }
        
    }
    else if  (textField==self.textFieldCardDigit9To12) {
        
        if (textField==self.textFieldCardDigit9To12 && newString.length==4) {
            [self.textFieldCardDigit9To12 setText:newString];
            [self.textFieldCardDigit13To16 becomeFirstResponder];
            return NO;
        }
        else if (isBackSpace==-8 && [newString isEqualToString:@""]) {
            textField.text = @"";
            
            [self.textFieldCardDigit5To8 becomeFirstResponder];
            return NO;
            
        }
        
        else  if (newLength>4) {
            return NO;
        }
        
    }
    else  if (textField==self.textFieldCardDigit13To16) {
        
        
        if(AmericanCardBtnClicked==YES)
        {
            if (textField==self.textFieldCardDigit13To16 && newString.length==3) {
                [self.textFieldCardDigit13To16 setText:newString];
                
                if (self.viewCardAndTextFieldHolder.frame.origin.y != 0) {
                    
                    CGRect frame = self.viewCardAndTextFieldHolder.frame;
                    frame.origin.y = 0;
                    
                    [UIView animateWithDuration:0.3 animations:^{
                        self.viewCardAndTextFieldHolder.frame = frame;
                    } completion:^(BOOL finished) {
                        
                    }];
                    
                }
                
                [textField resignFirstResponder];
                [self.textFieldCVVNO becomeFirstResponder];
                
                
                return NO;
            }
            
            
            
            else if (isBackSpace==-8 && [newString isEqualToString:@""]) {
                textField.text = @"";
                
                [self.textFieldCardDigit9To12 becomeFirstResponder];
                return NO;
                
            }
            else if (newLength>4) {
                
                if (self.viewCardAndTextFieldHolder.frame.origin.y != 0) {
                    
                    CGRect frame = self.viewCardAndTextFieldHolder.frame;
                    frame.origin.y = 0;
                    
                    [UIView animateWithDuration:0.3 animations:^{
                        self.viewCardAndTextFieldHolder.frame = frame;
                    } completion:^(BOOL finished) {
                        
                    }];
                    
                }
                [textField resignFirstResponder];
                
                return NO;
            }

        }
        
        else
        {
            
        
        
        if (textField==self.textFieldCardDigit13To16 && newString.length==4) {
            [self.textFieldCardDigit13To16 setText:newString];
            
            if (self.viewCardAndTextFieldHolder.frame.origin.y != 0) {
                
                CGRect frame = self.viewCardAndTextFieldHolder.frame;
                frame.origin.y = 0;
                
                [UIView animateWithDuration:0.3 animations:^{
                    self.viewCardAndTextFieldHolder.frame = frame;
                } completion:^(BOOL finished) {
                    
                }];
                
            }
            
            [textField resignFirstResponder];
            [self.textFieldCVVNO becomeFirstResponder];


            return NO;
        }
        
        
        
        else if (isBackSpace==-8 && [newString isEqualToString:@""]) {
            textField.text = @"";
            
            [self.textFieldCardDigit9To12 becomeFirstResponder];
            return NO;
            
        }
        else if (newLength>4) {
            
            if (self.viewCardAndTextFieldHolder.frame.origin.y != 0) {
                
                CGRect frame = self.viewCardAndTextFieldHolder.frame;
                frame.origin.y = 0;
                
                [UIView animateWithDuration:0.3 animations:^{
                    self.viewCardAndTextFieldHolder.frame = frame;
                } completion:^(BOOL finished) {
                    
                }];
                
            }
            [textField resignFirstResponder];

            return NO;
        }
        
        
        
        }
        
        
        
        
    }
    else if (textField==self.textFieldCVVNO) {
        
        if(AmericanCardBtnClicked==YES)
        {
            if (newLength>4) {
                
                if (self.viewCardAndTextFieldHolder.frame.origin.y != 0) {
                    
                    CGRect frame = self.viewCardAndTextFieldHolder.frame;
                    frame.origin.y = 0;
                    
                    [UIView animateWithDuration:0.3 animations:^{
                        self.viewCardAndTextFieldHolder.frame = frame;
                    } completion:^(BOOL finished) {
                        
                    }];
                    
                }
                
                [textField resignFirstResponder];
                return NO;
            }

        }
        
        
        else{
            if (newLength>3) {
                
                if (self.viewCardAndTextFieldHolder.frame.origin.y != 0) {
                    
                    CGRect frame = self.viewCardAndTextFieldHolder.frame;
                    frame.origin.y = 0;
                    
                    [UIView animateWithDuration:0.3 animations:^{
                        self.viewCardAndTextFieldHolder.frame = frame;
                    } completion:^(BOOL finished) {
                        
                    }];
                    
                }
                
                [textField resignFirstResponder];
                return NO;
            }

        }
        
        
            }
    else {
        
        return YES;
    }
    
    return YES;
    
    
}


-(void)textFieldDidEndEditing:(UITextField *)textField{
    
}



- (IBAction)backToSuperView:(id)sender {
    [self back];
}



//- (void)payPalFuturePaymentDidCancel:(PayPalFuturePaymentViewController *)futurePaymentViewController {
//    // User cancelled login. Dismiss the PayPalLoginViewController, breathe deeply.
//    
//    backFromPayPal = true;
//    
//    [self dismissViewControllerAnimated:YES completion:nil];
//    
//    [self back];
//    
//}
//
//- (void)payPalFuturePaymentViewController:(PayPalFuturePaymentViewController *)futurePaymentViewController
//                didAuthorizeFuturePayment:(NSDictionary *)futurePaymentAuthorization {
//    // The user has successfully logged into PayPal, and has consented to future payments.
//    
//    // Your code must now send the authorization response to your server.
//    [self sendAuthorizationToServer:futurePaymentAuthorization];
//    
//    // Be sure to dismiss the PayPalLoginViewController.
//  //  [self dismissViewControllerAnimated:YES completion:nil];
//    
//  //  [self back];
//    
//    
//   // NSDictionary* projectDictionary = futurePaymentAuthorization;
//
//    
//}

- (void)sendAuthorizationToServer:(NSString *)dataid nonce:(NSString *)nonce1
{
    // Send the entire authorization reponse
    
   // NSDictionary* projectDictionary = [authorization objectForKey:@"response"];
  //  projectDictionary1 = [projectDictionary objectForKey:@"code"];
    
    
   // metadataID = [PayPalMobile clientMetadataID];
    
    
    NSLog(@"meta data id is...%@",metadataID);
    NSLog(@"code is...%@",projectDictionary1);
    
  //  [self dismissViewControllerAnimated:YES completion:nil];
  //  backFromPayPal = true;
  //  [self back];
   // return;
    
 //   [GlobalFunction addIndicatorView];
    
    if (![[WebService shared] connected]) {
        
        [GlobalFunction removeIndicatorView];
        
        [[GlobalFunction shared] showAlertForMessage:@"Can't connect to internet, please check and try again."];
        
        return;
    }
    
    
    NSDictionary *dictPaymentResponse = [[WebService shared] addPayPalUserid:myAppDelegate.userData.userid metaDataId:dataid codeString:nonce1];
    
    if (dictPaymentResponse)
    {
        NSLog(@"response --- %@",[EncryptDecryptFile decrypt:[dictPaymentResponse valueForKey:@"responseMessage"]]);
        
        if ([[EncryptDecryptFile decrypt:[dictPaymentResponse valueForKey:@"responseCode"]] isEqualToString:@"200"])
        {
                        myAppDelegate.isPaymentFilled = true;
                        [self updatePaymentInfoSuccess:[EncryptDecryptFile decrypt:[dictPaymentResponse valueForKey:@"responseMessage"]]];
        }
        else{
            
          //  [[GlobalFunction shared] showAlertForMessage:[EncryptDecryptFile decrypt:[dictPaymentResponse valueForKey:@"responseMessage"]]];
             [self errorShow:[EncryptDecryptFile decrypt:[dictPaymentResponse valueForKey:@"responseMessage"]]];
            [GlobalFunction removeIndicatorView];
        }

    }
    else
    {
        [self errorShow:@"Server is down, please try again later."];
        
       // [[GlobalFunction shared] showAlertForMessage:@"Server is down, please try again later."];
        
        [GlobalFunction removeIndicatorView];
        
    }

}


- (IBAction)continueWithPayPalClicked:(id)sender {
}

- (IBAction)continueWithCardClicked:(id)sender {
}
@end
