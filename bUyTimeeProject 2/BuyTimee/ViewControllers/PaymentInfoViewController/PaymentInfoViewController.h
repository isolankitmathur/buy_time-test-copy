//
//  PaymentInfoViewController.h
//  HandApp
//
//  Created by  ~ on 01/03/16.
//  Copyright (c) 2016 . All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Flurry.h"

//#import "PayPalMobile.h"
//#import "PayPalConfiguration.h"
//#import "PayPalPaymentViewController.h"

#import "BraintreeCore.h"

#define kPayPalEnvironment PayPalEnvironmentNoNetwork

@interface PaymentInfoViewController : UIViewController


@property (nonatomic, strong) BTAPIClient *braintreeClient;

@property (weak, nonatomic) IBOutlet UITextField *textFieldNameOnCard;

@property (weak, nonatomic) IBOutlet UITextField *textFieldCardDigit1To4;
@property (weak, nonatomic) IBOutlet UITextField *textFieldCardDigit5To8;
@property (weak, nonatomic) IBOutlet UITextField *textFieldCardDigit9To12;
@property (weak, nonatomic) IBOutlet UITextField *textFieldCardDigit13To16;

@property (weak, nonatomic) IBOutlet UITextField *textFieldExpirationDateMM;
@property (weak, nonatomic) IBOutlet UITextField *textFieldExpirationDateYYYY;
@property (weak, nonatomic) IBOutlet UITextField *textFieldCVVNO;

@property (weak, nonatomic) IBOutlet UIView *viewHolderForTableExpirationDateMonth;
@property (weak, nonatomic) IBOutlet UIView *viewHolderForExpirationDateYear;
@property (weak, nonatomic) IBOutlet UIView *viewYearLabelHolder;
@property (weak, nonatomic) IBOutlet UIView *viewMonthLabelHolder;
@property (weak, nonatomic) IBOutlet UIView *viewBGTapHandler;
@property (weak, nonatomic) IBOutlet UIView *viewTopHolder;
@property (weak, nonatomic) IBOutlet UIView *viewCardHolder;
@property (weak, nonatomic) IBOutlet UIView *viewTextFieldHolder;
@property (weak, nonatomic) IBOutlet UIView *viewBGTapHandlerInCardsView;

@property (weak, nonatomic) IBOutlet UITableView *tableViewMonth;
@property (weak, nonatomic) IBOutlet UITableView *tableViewYear;

//added
@property (weak, nonatomic) IBOutlet UILabel *lblMonth;
@property (weak, nonatomic) IBOutlet UIImageView *imgViewArrow;
@property (weak, nonatomic) IBOutlet UILabel *lblYear;
@property (weak, nonatomic) IBOutlet UIImageView *imgArrowYear;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintBtnSavePaymentBtnInfoHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintViewExpirationDateMonthHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintViewExpirationDateYearHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintTextFieldHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintViewPaymentInfoHolderTopSpace;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintTextFieldHolderTopSpace;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintYearLblHolderHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintMonthLblHolderHeight;

- (IBAction)visaSelected:(id)sender;
- (IBAction)masterCardSelected:(id)sender;
- (IBAction)discoverCardSelected:(id)sender;
- (IBAction)americanExpressSelected:(id)sender;
- (IBAction)savePaymentInfo:(id)sender;
- (IBAction)showCVVInfo:(id)sender;
- (IBAction)openMonthTable:(id)sender;
- (IBAction)openYearTable:(id)sender;
- (IBAction)closeTableView:(id)sender;
- (IBAction)closeAllTableViews:(id)sender;


@property (nonatomic, retain) UITextField *txtActiveField;
@property (nonatomic, retain) UIView *inputAccView;
@property (nonatomic, retain) UIButton *btnDone;
@property (nonatomic, retain) UIButton *btnNext;
@property (nonatomic, retain) UIButton *btnPrev;

@property (weak, nonatomic) IBOutlet UIImageView *ImageViewYouNeed;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consImageViewYouNeedHeight;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consImageViewYouNeedWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consImageViewYouNeedLeading;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consImageViewYouNeedTop;

@property (weak, nonatomic) IBOutlet UILabel *LblYouNeedToAdd;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *buyTimeLabelLeft;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *buyTimeLblWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *buyTimeLblTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *backBtnTop;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *headerUnderLineTop;
@property (weak, nonatomic) IBOutlet UILabel *buyTimeLbl;


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *headingLblLeft;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *headingLblTop;


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *headingLblHeight;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *consLblYouNeedTop;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *consGrayViewTop;


// New paypal work view outlets....


//@property (weak, nonatomic) IBOutlet UIView *mainPayPalView;
//@property (weak, nonatomic) IBOutlet UIImageView *ImageViewUnderPayPalView;
//@property (weak, nonatomic) IBOutlet UIView *SubViewUnderPayPalView;
//
//@property (weak, nonatomic) IBOutlet UILabel *lblChooseAnyOptionPayPal;
//@property (weak, nonatomic) IBOutlet UIButton *BtnContinueWithPayPal;
//@property (weak, nonatomic) IBOutlet UIButton *BtnContinueWithCard;
//
//- (IBAction)continueWithPayPalClicked:(id)sender;
//
//- (IBAction)continueWithCardClicked:(id)sender;
//
//@property (weak, nonatomic) IBOutlet UIImageView *paypalImgView;
//
//@property (weak, nonatomic) IBOutlet UIImageView *cardImageView;


@property (weak, nonatomic) IBOutlet UIButton *payPalPaymentProcess;
@property(nonatomic, strong, readwrite) NSString *environment;
@property (weak, nonatomic) IBOutlet UILabel *lblChooseYourPaymentMethod;
@property (weak, nonatomic) IBOutlet UILabel *lblFullNameOnCard;
@property (weak, nonatomic) IBOutlet UILabel *lblCardNo;

@property (weak, nonatomic) IBOutlet UILabel *lblExpiryDate;

@end
