//
//  cashOutPayPalViewController.h
//  BuyTimee
//
//  Created by User38 on 10/09/18.
//  Copyright © 2018 User14. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Flurry.h"

@interface cashOutPayPalViewController : UIViewController <UITextFieldDelegate,UIAlertViewDelegate>

// new outlets for add paypal view work...10Sept18...

@property (nonatomic, retain) UITextField *txtActiveField;
@property (nonatomic, retain) UIView *inputAccView;
@property (nonatomic, retain) UIButton *btnDone;
@property (nonatomic, retain) UIButton *btnNext;
@property (nonatomic, retain) UIButton *btnPrev;



@property (weak, nonatomic) IBOutlet UIButton *btnSave;
- (IBAction)saveInfoClicked:(id)sender;


@property (weak, nonatomic) IBOutlet UIView *paypalOverView;

@property (weak, nonatomic) IBOutlet UILabel *lblPaypalDescription;

@property (weak, nonatomic) IBOutlet UITextField *businessIdTxtFld;

// paypal constraints outlets 10Sept18...

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consPayPalBusinessTxtFldTop;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consPayPalBusinessTxtFldTrailing;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consPayPalBusinessTxtFldHeight;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consPayPalBusinessTxtFldLeading;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consLblPayPalPleaseVerifyTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consLblPayPalPleaseVerifyHeight;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consLblPayPalPleaseVerifyTrailing;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consLblPayPalPleaseVerifyLeading;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consBtnSaveInfoHeight;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consSaveBankInfoBtnBottom;

@end
