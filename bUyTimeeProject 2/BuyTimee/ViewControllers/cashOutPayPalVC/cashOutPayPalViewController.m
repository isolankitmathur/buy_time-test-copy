//
//  cashOutPayPalViewController.m
//  BuyTimee
//
//  Created by User38 on 10/09/18.
//  Copyright © 2018 User14. All rights reserved.
//

#import "cashOutPayPalViewController.h"
#import "Constants.h"
#import "GlobalFunction.h"
#import <QuartzCore/QuartzCore.h>

@interface cashOutPayPalViewController ()

@end

@implementation cashOutPayPalViewController
@synthesize inputAccView,txtActiveField,btnDone,btnNext,btnPrev;


- (void)viewDidLoad {
    [super viewDidLoad];
     // Do any additional setup after loading the view.
    
    myAppDelegate.vcObj.btnMapOnListView.hidden = true;//vs
    myAppDelegate.vcObj.BtnTransparentMap.hidden = true;//vs
    

    // paypal work
    
    self.lblPaypalDescription.text = @"Please verify that the email used above is a valid PayPal user email account. Transfer to your account may take up to 7 days.";
    
    self.businessIdTxtFld.delegate = self;
    self.businessIdTxtFld.backgroundColor = [UIColor clearColor];
    
   // self.businessIdTxtFld.borderStyle = UITextBorderStyleRoundedRect;
    
//    self.businessIdTxtFld.layer.cornerRadius=20.0f;
//    self.businessIdTxtFld.layer.masksToBounds=YES;
//    self.businessIdTxtFld.layer.borderColor=[[UIColor colorWithRed:231.0/255.0f green:231.0/255.0f blue:237.0/255.0f alpha:0.9f]CGColor];
//    self.businessIdTxtFld.layer.borderWidth= 3.0f;
    
    self.businessIdTxtFld.keyboardType = UIKeyboardTypeEmailAddress;
   
    
    NSAttributedString *star = [[NSAttributedString alloc] initWithString:@"*" attributes:@{ NSForegroundColorAttributeName :[UIColor redColor],
                                                                                             }];
    
    
    NSMutableAttributedString *str1 = [[NSMutableAttributedString alloc] initWithString:@"PayPal User Id" attributes:@{ NSForegroundColorAttributeName :[UIColor colorWithRed:143.0/255.0f green:143.0/255.0f blue:143.0/255.0f alpha:1.0],
                                                                                                              }];
    
    [str1 appendAttributedString: star];
    self.businessIdTxtFld.attributedPlaceholder = str1;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated
{

  [GlobalFunction addIndicatorView];
    
    NSLog(@"view will appear call");
    
    [self updateConstraints];
}
-(void)viewDidAppear:(BOOL)animated
{
    [self getBankData];
}

// placeholder position
//- (CGRect)textRectForBounds:(CGRect)bounds {
//    return CGRectInset(bounds, 10, 10);
//}
//
//// text position
//- (CGRect)editingRectForBounds:(CGRect)bounds {
//    return CGRectInset(bounds, 10, 10);
//}

-(void)updateConstraints
{
    
        
        _btnSave.backgroundColor=[UIColor colorWithRed:63.0/255.0f green:67.0/255.0f blue:70.0/255.0f alpha:1.0];
        
        
        
        if (IS_IPHONE_5 || IS_IPHONE_4_OR_LESS)
        {
           
            // for img view
            
            self.consBtnSaveInfoHeight.constant = 38;
            
                 _consSaveBankInfoBtnBottom.constant=60;
            
                    // new pay pal screen cons...
            
            self.consPayPalBusinessTxtFldHeight.constant = 40;
            self.consPayPalBusinessTxtFldLeading.constant = 20;
            self.consPayPalBusinessTxtFldTrailing.constant = 20;
            self.consPayPalBusinessTxtFldTop.constant = 160;
            
            self.consLblPayPalPleaseVerifyLeading.constant = 25;
            self.consLblPayPalPleaseVerifyTrailing.constant = 25;
            
            self.consLblPayPalPleaseVerifyTop.constant = 5;
            
            
            [self.businessIdTxtFld setFont:[UIFont fontWithName:@"Lato-Regular" size:17]];
            
            [self.lblPaypalDescription setFont:[UIFont fontWithName:@"Lato-Regular" size:18]];
            
            [self.btnSave.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:16]];
            
        }
        if (IS_IPHONE_6P)
        {
            
            // for img view
            
            self.consBtnSaveInfoHeight.constant = 50;
            
            [self.btnSave.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:21]];
            
               _consSaveBankInfoBtnBottom.constant=76;
        
            self.consPayPalBusinessTxtFldTop.constant = 195;
            
            //        self.consLblPayPalPleaseVerifyLeading.constant = 25;
            //        self.consLblPayPalPleaseVerifyTrailing.constant = 25;
            
            self.consLblPayPalPleaseVerifyTop.constant = 12;
            
            // [self.businessIdTxtFld setFont:[UIFont fontWithName:@"Lato-Regular" size:17]];
            
            [self.lblPaypalDescription setFont:[UIFont fontWithName:@"Lato-Regular" size:20]];
        }
        
        if (IS_IPHONE_6)
        {
            
            
            self.consBtnSaveInfoHeight.constant = 45;
                        [self.btnSave.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:19]];
            
         
            _consSaveBankInfoBtnBottom.constant=72;
            _consBtnSaveInfoHeight.constant=45;
            

            
        }
        
        [self.view layoutIfNeeded];
    
}

-(void)getBankData
{
    
    if (![[WebService shared] connected]) {
        
        [GlobalFunction removeIndicatorView];
        
        [[GlobalFunction shared] showAlertForMessage:@"Can't connect to internet, please check and try again."];
        
        return;
    }
    
    NSDictionary*dictReg = [webServicesShared viewBankUserid:myAppDelegate.userData.userid accType:@"paypal"];
    
    
    
    if (dictReg != nil) {
        
        NSLog(@"response --- %@",[EncryptDecryptFile decrypt:[dictReg valueForKey:@"responseMessage"]]);
        
        NSLog(@"response --- %@",[EncryptDecryptFile decrypt:[dictReg valueForKey:@"responseCode"]]);
        
        if ([[EncryptDecryptFile decrypt:[dictReg valueForKey:@"responseCode"]] isEqualToString:@"200"]) {
            
            
            
            
            
            //below code is commented bcz not show bank info only show paypal business email id... // 15 June 18.
            
            
            NSArray *val = [dictReg objectForKey:@"data"];
            
            NSDictionary *dictDataReg = [val objectAtIndex:0];
            
            if (dictDataReg) {
                
                NSString *email1 = [EncryptDecryptFile decrypt:[dictDataReg valueForKey:@"account"]];
                
                self.businessIdTxtFld.text = email1;
                
                
                //                self.textFldName.text = [EncryptDecryptFile decrypt:[dictDataReg valueForKey:@"name"]];
                //
                //                self.textFldAccNo.text = [EncryptDecryptFile decrypt:[dictDataReg valueForKey:@"accountNo"]];
                //                self.textFldRouting.text = [EncryptDecryptFile decrypt:[dictDataReg valueForKey:@"routingNo"]];
                //                self.textFldBankName.text = [EncryptDecryptFile decrypt:[dictDataReg valueForKey:@"bankName"]];
                //
                //                NSString *checkAccType = [EncryptDecryptFile decrypt:[dictDataReg valueForKey:@"accType"]];
                //
                //
                //                if ([checkAccType isEqualToString:@"0"]) {
                //
                //                    accType = @"0";
                //
                //                    [self.radioCheckingClicked setImage:[UIImage imageNamed:@"radioButton.png"] forState:UIControlStateNormal];
                //                    [self.radioSavingClicked setImage:[UIImage imageNamed:@"radioSelectedButton.png"] forState:UIControlStateNormal];
                //                }
                //                else if ([checkAccType isEqualToString:@"1"])
                //                {
                //                    accType = @"1";
                //
                //                    [self.radioSavingClicked setImage:[UIImage imageNamed:@"radioButton.png"] forState:UIControlStateNormal];
                //                    [self.radioCheckingClicked setImage:[UIImage imageNamed:@"radioSelectedButton.png"] forState:UIControlStateNormal];
                //                }
                //
                //               //aditional
                //
                //                self.ssnTextFld.text = [EncryptDecryptFile decrypt:[dictDataReg valueForKey:@"ssn"]];
                //                self.taxIdTxtFld.text = [EncryptDecryptFile decrypt:[dictDataReg valueForKey:@"taxId"]];
                //          NSString *businessType1 = [EncryptDecryptFile decrypt:[dictDataReg valueForKey:@"type"]];
                //
                //                if (businessType1.length <= 0)
                //                {
                //                   // [self.btnBusinessType setTitle:@"Type" forState:UIControlStateNormal];
                //                }
                //                else
                //                {
                //                    NSString *firstCapChar = [[businessType1 substringToIndex:1] capitalizedString];
                //                    NSString *cappedString = [businessType1 stringByReplacingCharactersInRange:NSMakeRange(0,1) withString:firstCapChar];
                //
                //                    [self.btnBusinessType setTitle:cappedString forState:UIControlStateNormal];
                //                }
                //
                //
                //          NSString *DOB = [EncryptDecryptFile decrypt:[dictDataReg valueForKey:@"dob"]];
                //
                //                if (DOB.length <= 0)
                //                {
                //
                //                }
                //                else
                //                {
                //                    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
                //                    [dateFormat setDateFormat:dateFormatFull];
                //                    NSDate *dateMain = [dateFormat dateFromString:DOB];
                ////                    NSString *dateString =  [dateFormat stringFromDate:dateMain];
                //                    
                //                    NSDateFormatter *dtFormatMDY = [[NSDateFormatter alloc] init];
                //                    [dtFormatMDY setDateFormat:dateFormatMDY];
                //                    
                //                    NSString *dateString = [dtFormatMDY  stringFromDate:dateMain];
                //                
                //                    [self.btnDOB setTitle:dateString forState:UIControlStateNormal];
                //                }
                //
                //                
                //                // additional end
            }
            
            
        }
        else
        {
            [GlobalFunction removeIndicatorView];
        }
        
    }
    else
    {
        [GlobalFunction removeIndicatorView];
        [[GlobalFunction shared] showAlertForMessage:@"Server is down, please try again later."];
    }
    [GlobalFunction removeIndicatorView];
}
#pragma mark - create accessory view

-(void)createInputAccessoryView{
    // Create the view that will play the part of the input accessory view.
    // Note that the frame width (third value in the CGRectMake method)
    // should change accordingly in landscape orientation. But we don’t care
    // about that now.
    inputAccView = [[UIView alloc] initWithFrame:CGRectMake(10.0, 0.0, self.view.frame.size.width, 40.0)];
    // Set the view’s background color. We’ ll set it here to gray. Use any color you want.
    [inputAccView setBackgroundColor:[UIColor lightGrayColor]];
    // We can play a little with transparency as well using the Alpha property. Normally
    // you can leave it unchanged.
    [inputAccView setAlpha: 0.8];
    // If you want you may set or change more properties (ex. Font, background image,e.t.c.).
    // For now, what we’ ve already done is just enough.
    // Let’s create our buttons now. First the previous button.
    btnPrev = [UIButton buttonWithType: UIButtonTypeCustom];
    // Set the button’ s frame. We will set their widths to 80px and height to 40px.
    [btnPrev setFrame: CGRectMake(0.0, 0.0, 80.0, 40.0)];
    // Title.
    [btnPrev setTitle: @"Previous" forState: UIControlStateNormal];
    // Background color.
    [btnPrev setBackgroundColor: [UIColor clearColor]];
    // You can set more properties if you need to.
    // With the following command we’ ll make the button to react in finger tapping. Note that the
    // gotoPrevTextfield method that is referred to the @selector is not yet created. We’ ll create it
    // (as well as the methods for the rest of our buttons) later.
    [btnPrev addTarget: self action: @selector(gotoPrevTextfield) forControlEvents: UIControlEventTouchUpInside];
    // Do the same for the two buttons left.
    btnNext = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnNext setFrame:CGRectMake(85.0f, 0.0f, 80.0f, 40.0f)];
    [btnNext setTitle:@"Next" forState:UIControlStateNormal];
    [btnNext setBackgroundColor:[UIColor clearColor]];
    [btnNext addTarget:self action:@selector(gotoNextTextfield) forControlEvents:UIControlEventTouchUpInside];
    btnDone = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnDone setFrame:CGRectMake(self.view.frame.size.width - 80.0f, 0.0f, 80.0f, 40.0f)];
    [btnDone setTitle:@"Done" forState:UIControlStateNormal];
    [btnDone setBackgroundColor:[UIColor clearColor]];
    [btnDone setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [btnDone addTarget:self action:@selector(doneTyping) forControlEvents:UIControlEventTouchUpInside];
    // Now that our buttons are ready we just have to add them to our view.

    [inputAccView addSubview:btnDone];
}
-(void)doneTyping{

   [txtActiveField resignFirstResponder];
//    return;
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    

    
    return YES;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    
    [self createInputAccessoryView];
    // Now add the view as an input accessory view to the selected textfield.
    [textField setInputAccessoryView:inputAccView];
    // Set the active field. We' ll need that if we want to move properly
    // between our textfields.
    txtActiveField = textField;
    
    
    
}
- (IBAction)saveInfoClicked:(id)sender
{
[GlobalFunction addIndicatorView];

[self performSelector:@selector(sendPayPalEmailToServer) withObject:nil afterDelay:0.2];


}
-(void)sendPayPalEmailToServer
{
    
    if (![[WebService shared] connected]) {
        
        [GlobalFunction removeIndicatorView];
        
        [[GlobalFunction shared] showAlertForMessage:@"Can't connect to internet, please check and try again."];
        
        return;
    }
    if ([[self.businessIdTxtFld.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""]) {
        
        [GlobalFunction removeIndicatorView];
        
        [[GlobalFunction shared] showAlertForMessage:@"PayPal user id cannot be blank."];
        
        return;
        
    }
    
    
    bool checkId = [self validateEmailWithString:self.businessIdTxtFld.text];
    
    if (!checkId)
    {
        [GlobalFunction removeIndicatorView];
        
        [[GlobalFunction shared] showAlertForMessage:@"Please enter a valid business id."];
        
        return;
    }
    
    NSDictionary *dictBioResponse;
    dictBioResponse = [[WebService shared] AddEmailUserid:myAppDelegate.userData.userid emailId:self.businessIdTxtFld.text];
    
    
    if (dictBioResponse) {
        
        NSLog(@"response --- %@",[EncryptDecryptFile decrypt:[dictBioResponse valueForKey:@"responseMessage"]]);
        
        NSLog(@"response --- %@",[EncryptDecryptFile decrypt:[dictBioResponse valueForKey:@"responseCode"]]);
        
        if ([[EncryptDecryptFile decrypt:[dictBioResponse valueForKey:@"responseCode"]] isEqualToString:@"200"]) {
            
            NSArray *dictDataReg = [dictBioResponse objectForKey:@"data"];
            
            NSDictionary *dictFromResponse = [dictBioResponse objectForKey:@"data"];
            
            
            //            email = self.businessIdTxtFld.text;
            
            //            [emailCheck setObject:email forKey:@"businessEmailPayPal"];
            //
            //            [emailCheck synchronize];
            
            myAppDelegate.isBankDetailFilled = true;
            myAppDelegate.selectedPaymentTypeFromServer = @"2";
            myAppDelegate.iscashOutPayPallFilled = true;
            myAppDelegate.isStripeBankInfoFilled = false;
            [myAppDelegate.createBioVCObj setRadioButtonImages];


            //  myAppDelegate.isBankInfoFilled = true;
            
            
            
            
            
            [self bioUpdatedSuccefully:[EncryptDecryptFile decrypt:[dictBioResponse valueForKey:@"responseMessage"]]];
            
            
        }else{
            
            [[GlobalFunction shared] showAlertForMessage:[EncryptDecryptFile decrypt:[dictBioResponse valueForKey:@"responseMessage"]]];
            
            [GlobalFunction removeIndicatorView];
            
        }
        
        
    }
    
    else
    {
        
        [[GlobalFunction shared] showAlertForMessage:@"Server is down, please try again later."];
        
        [GlobalFunction removeIndicatorView];
        
    }
    
    
    
    
}

- (BOOL)validateEmailWithString:(NSString*)checkString
{
    BOOL stricterFilter = NO; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSString *laxString = @".+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}
-(void)bioUpdatedSuccefully:(NSString *)response{
    
    [GlobalFunction removeIndicatorView];
    
    
    NSLog(@"successful .......");
    
    
    
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"BUY TIMEE" message:response delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    
    alert.tag = 10001;
    
    [alert show];
    
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    
    if (alertView.tag == 10001){
        if(buttonIndex == 0)//OK button pressed
        {
            //do something
            NSLog(@"ook pressed !!!");
            
            // self.textFldReenterAccNo.text=@"";
            
            [myAppDelegate.vcObj backToMainView:nil];
            
            
        }
        else if(buttonIndex == 1)//Annul button pressed.
        {
            //do something
        }
    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
