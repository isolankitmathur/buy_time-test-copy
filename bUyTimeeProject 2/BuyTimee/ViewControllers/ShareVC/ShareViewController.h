//
//  ShareViewController.h
//  BuyTimee
//
//  Created by mitesh on 04/03/17.
//  Copyright © 2017 User14. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import <Social/Social.h>
#import "Flurry.h"

@interface ShareViewController : UIViewController<MFMessageComposeViewControllerDelegate>

@property (weak, nonatomic) IBOutlet UIButton *phoneConatactButton;
@property (weak, nonatomic) IBOutlet UIButton *facebookButton;
@property (weak, nonatomic) IBOutlet UIButton *twitterButton;
@property (weak, nonatomic) IBOutlet UIButton *doneButton;

- (IBAction)phoneContactBtnClciked:(id)sender;
- (IBAction)facebookBtnClicked:(id)sender;
- (IBAction)twitterBtnClciked:(id)sender;
- (IBAction)DoneBtnClicked:(id)sender;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *doneBtnHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *doneBtnTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *phoneContactBtnTop;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *phoneLblTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *phoneBtnHeight;
@property (weak, nonatomic) IBOutlet UILabel *phoneLineLbl;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *facebookBtnTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *faceBookBtnHeight;
@property (weak, nonatomic) IBOutlet UILabel *fbLineLbl;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *twitterBtnTop;
@property (weak, nonatomic) IBOutlet UILabel *twitterLineLbl;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *twitterBtnHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *fbLineLblTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *twitterLineLblTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *phoneLineLblWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *fbLineLblWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *twitterLineLblWidth;

@property (weak, nonatomic) IBOutlet UIButton *btnSkip;

- (IBAction)skipBtnPressed:(id)sender;

@property (weak, nonatomic) IBOutlet UILabel *lblSkipUnderline;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consBtnSkipHeight;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consBtnSkipWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *conslblSkipUnderlineWidth;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consSkipUnderlineLeading;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consSkipUnderlineTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consBtnSkipLeading;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consBtnSkipTop;


@end
