//
//  ShareViewController.m
//  BuyTimee
//
//  Created by mitesh on 04/03/17.
//  Copyright © 2017 User14. All rights reserved.
//

#import "ShareViewController.h"
#import "Constants.h"
#import "TaskTableViewCell.h"
#import <MessageUI/MessageUI.h>
#import <Social/Social.h>
#import "GlobalFunction.h"

@interface ShareViewController ()

@end

@implementation ShareViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSLog(@"twitter top..%f",self.twitterBtnTop.constant);
    NSLog(@"twitter height..%f",self.twitterBtnHeight.constant);
    NSLog(@"twitter lbl top..%f",self.twitterLineLblTop.constant);
    NSLog(@"twitter x is..%f",_twitterButton.frame.origin.x);
    
    NSLog(@"twitter line x is..%f",_twitterLineLbl.frame.origin.x);
    NSLog(@"twitter line width is..%f",_twitterLineLblWidth.constant);
    NSLog(@"twitter btn width is..%f",_twitterButton.frame.size.width);

    
   }

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidAppear:(BOOL)animated
{
    NSString* final = @"Share Reservation page after create";
    
    [Flurry logEvent:final];
    
    _doneButton.backgroundColor=[UIColor colorWithRed:63.0/255.0f green:67.0/255.0f blue:70.0/255.0f alpha:1.0];
    [_phoneConatactButton setTitleColor:[UIColor colorWithRed:63.0/255.0f green:67.0/255.0f blue:70.0/255.0f alpha:1.0] forState:UIControlStateNormal];
    [_facebookButton setTitleColor:[UIColor colorWithRed:63.0/255.0f green:67.0/255.0f blue:70.0/255.0f alpha:1.0] forState:UIControlStateNormal];
    [_twitterButton setTitleColor:[UIColor colorWithRed:63.0/255.0f green:67.0/255.0f blue:70.0/255.0f alpha:1.0] forState:UIControlStateNormal];
    
    [_btnSkip setTitleColor:[UIColor colorWithRed:63.0/255.0f green:67.0/255.0f blue:70.0/255.0f alpha:1.0] forState:UIControlStateNormal];
    
    
    _phoneLineLbl.backgroundColor=[UIColor colorWithRed:196.0/255.0f green:196.0/255.0f blue:196.0/255.0f alpha:1.0];
    _fbLineLbl.backgroundColor=[UIColor colorWithRed:196.0/255.0f green:196.0/255.0f blue:196.0/255.0f alpha:1.0];
    _twitterLineLbl.backgroundColor=[UIColor colorWithRed:196.0/255.0f green:196.0/255.0f blue:196.0/255.0f alpha:1.0];
    _lblSkipUnderline.backgroundColor=[UIColor colorWithRed:196.0/255.0f green:196.0/255.0f blue:196.0/255.0f alpha:1.0];
    
    
    

    // Do any additional setup after loading the view.
    
    
    if(IS_IPHONE_6)
    {
        _doneBtnHeight.constant=45;
        _doneBtnTop.constant=234;
        [self.doneButton.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:19]];
        
        [self.phoneConatactButton.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:16]];
        [self.facebookButton.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:16]];
        [self.twitterButton.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:16]];
        [self.btnSkip.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:16]];
        _phoneContactBtnTop.constant=187;
        _phoneBtnHeight.constant=30;
        _phoneLblTop.constant=1;
        
        _faceBookBtnHeight.constant=30;
        _fbLineLblTop.constant=1;
        
        _twitterBtnHeight.constant=30;
        _twitterLineLblTop.constant=1;
        
        _facebookBtnTop.constant=15;
        _twitterBtnTop.constant=15;
        
        self.consBtnSkipTop.constant = 15;
        self.consBtnSkipHeight.constant = 30;
        self.consSkipUnderlineTop.constant = 1;
        
    }
    else if (IS_IPHONE_5 || IS_IPHONE_4_OR_LESS)
    {
        _doneBtnHeight.constant=40;
        _doneBtnTop.constant=199;
        _phoneContactBtnTop.constant=161;
        
        _phoneBtnHeight.constant=25;
        _phoneLblTop.constant=1;
        
        _faceBookBtnHeight.constant=25;
        _fbLineLblTop.constant=1;
        
        _twitterBtnHeight.constant=25;
        _twitterLineLblTop.constant=1;
        
        _facebookBtnTop.constant=13;
        _twitterBtnTop.constant=13;
        
        self.consBtnSkipTop.constant = 13;
        self.consBtnSkipHeight.constant = 25;
        self.consSkipUnderlineTop.constant = 1;
        
        self.conslblSkipUnderlineWidth.constant = 154;
        self.consBtnSkipWidth.constant = 144;
        
        
        
        [self.btnSkip.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:14]];
        
        
        
        
        [self.phoneConatactButton.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:14]];
        [self.facebookButton.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:14]];
        [self.twitterButton.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:14]];
        [self.doneButton.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:18]];
        
    }
    
    else if(IS_IPHONE_6P)
    {
        _doneBtnHeight.constant=50;
        _phoneContactBtnTop.constant=190;
        _doneBtnTop.constant=263;
        
        
        
        [self.doneButton.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:21]];
        
        
        [self.phoneConatactButton.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:18]];
        [self.facebookButton.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:18]];
        [self.twitterButton.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:18]];
        [self.btnSkip.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:18]];
        
        _phoneContactBtnTop.constant=211;
        _phoneBtnHeight.constant=30;
        _phoneLblTop.constant=2;
        
        _faceBookBtnHeight.constant=30;
        _fbLineLblTop.constant=2;
        
        _twitterBtnHeight.constant=30;
        _twitterLineLblTop.constant=2;
        
        _facebookBtnTop.constant=19;
        _twitterBtnTop.constant=19;
        
        self.consBtnSkipTop.constant = 19;
        self.consBtnSkipHeight.constant = 30;
        self.consSkipUnderlineTop.constant = 3;
        self.consBtnSkipLeading.constant = 88;
        self.consSkipUnderlineLeading.constant = 83;
        self.conslblSkipUnderlineWidth.constant = 248;
        self.consBtnSkipWidth.constant = 238;
        
        
    }
    

    NSLog(@"twitter top..%f",self.twitterBtnTop.constant);
    NSLog(@"twitter height..%f",self.twitterBtnHeight.constant);
    NSLog(@"twitter lbl top..%f",self.twitterLineLblTop.constant);
    NSLog(@"twitter x is..%@",_twitterButton.leadingAnchor);
    
    NSLog(@"twitter line x is..%f",_twitterLineLbl.frame.origin.x);
    NSLog(@"twitter line width is..%f",_twitterLineLblWidth.constant);
    NSLog(@"twitter btn width is..%f",_twitterButton.frame.size.width);

}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)phoneContactBtnClciked:(id)sender {
    
 
       MFMessageComposeViewController *controller = [[MFMessageComposeViewController alloc] init];
    if([MFMessageComposeViewController canSendText]) {
        controller.body = myAppDelegate.reservationDetailString_forShare;
        controller.messageComposeDelegate = self;
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            UIViewController *vc = [[[[UIApplication sharedApplication] delegate] window] rootViewController];
            [vc presentViewController:controller animated:YES completion:nil];
        }];
    }
    
}

- (IBAction)facebookBtnClicked:(id)sender {
    
    if (![[WebService shared] connected]) {
        
        [[GlobalFunction shared] showAlertForMessage:@"Can't connect to internet, please check and try again."];
        
        return;
       
    }else{
        
        SLComposeViewController *FBSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
        
        
        [FBSheet setInitialText:myAppDelegate.reservationDetailString_forShare];
        [FBSheet addImage:myAppDelegate.reservationImage_forShare];
        
        [self presentViewController:FBSheet animated:YES completion:nil];
        
        [FBSheet setCompletionHandler:^(SLComposeViewControllerResult result) {
            
            NSString *output;
            
            switch (result) {
                    
                case SLComposeViewControllerResultCancelled:
                    
                    output = @"Action Cancelled";
                    
                    break;
                    
                case SLComposeViewControllerResultDone:
                    
                    output = @"Post Successfully";
                   //  [self.doneButton setTitle: @"Done" forState: UIControlStateNormal];
                    break;
                    
                default:
                    
                    break;
                    
            } //check if everything worked properly. Give out a message on the state.
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"BUY TIMEE" message:output delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            
            [alert show];
            
        }];
        
    }

    
    
}

- (IBAction)twitterBtnClciked:(id)sender {
    
    
    if (![[WebService shared] connected]) {
        
        [[GlobalFunction shared] showAlertForMessage:@"Can't connect to internet, please check and try again."];
        
        return;
      
    }else{
        
        if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])
        {
            SLComposeViewController *tweetSheet = [SLComposeViewController
                                                   composeViewControllerForServiceType:SLServiceTypeTwitter];
            [tweetSheet setInitialText:myAppDelegate.reservationDetailString_forShare];
            
            [tweetSheet addImage:myAppDelegate.reservationImage_forShare];
            [self presentViewController:tweetSheet animated:YES completion:nil];
            
            
            
            [tweetSheet setCompletionHandler:^(SLComposeViewControllerResult result) {
                
                NSString *output;
                
                switch (result) {
                        
                    case SLComposeViewControllerResultCancelled:
                        
                        output = @"Action Cancelled";
                        
                        break;
                        
                    case SLComposeViewControllerResultDone:
                        
                        output = @"Post Successfull";

                       //  [self.doneButton setTitle: @"Done" forState: UIControlStateNormal];
                        
                        break;
                        
                    default:
                        
                        break;
                        
                } //check if everything worked properly. Give out a message on the state.
                
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"BUY TIMEE" message:output delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                
                [alert show];
                
               
                
               
            }];
            
        }
        
    }

    
    
}

- (IBAction)DoneBtnClicked:(id)sender {
    
//                myAppDelegate.isSideBarAccesible = true;
//    
//                TaskTableViewCell *cell = (TaskTableViewCell *)[myAppDelegate.vcObj.tblViewSideBar cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
//                [myAppDelegate.vcObj performSegueWithIdentifier:@"viewAllTask" sender:cell];
//    
               // myAppDelegate.taskCreateVCObj = nil;

    
}
- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult) result
{
    switch (result) {
        case MessageComposeResultCancelled:
            break;
            
        case MessageComposeResultFailed:
        {
            UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Failed to send SMS!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [warningAlert show];
            break;
        }
            
        case MessageComposeResultSent:
            break;
            
        default:
            break;
    }
    
    [controller dismissViewControllerAnimated:YES completion:nil];
}



- (IBAction)skipBtnPressed:(id)sender {
    
    myAppDelegate.isSideBarAccesible = true;
    
//    TaskTableViewCell *cell = (TaskTableViewCell *)[myAppDelegate.vcObj.tblViewSideBar cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
//    [myAppDelegate.vcObj performSegueWithIdentifier:@"viewAllTask" sender:cell];
    
    
    TaskTableViewCell *cell = (TaskTableViewCell *)[myAppDelegate.vcObj.tblViewSideBar cellForRowAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:0]];
    [myAppDelegate.vcObj performSegueWithIdentifier:@"taskhistory" sender:cell];
    
    
}
@end
