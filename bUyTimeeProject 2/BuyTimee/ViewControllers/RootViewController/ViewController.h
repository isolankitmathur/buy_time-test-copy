//
//  ViewController.h
//  BuyTimee
//
//  Created by User14 on 26/02/16.

//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import <MapKit/MapKit.h>

#import "BraintreeCore.h"

@interface ViewController : UIViewController <UITableViewDataSource,UITableViewDelegate,UIGestureRecognizerDelegate,MFMailComposeViewControllerDelegate,MKMapViewDelegate>

@property (nonatomic, strong) BTAPIClient *braintreeClient;


@property (weak, nonatomic) IBOutlet UIButton *btnPaymentInfo;
@property (weak, nonatomic) IBOutlet UIButton *btnBankInfo;
@property (weak, nonatomic) IBOutlet UIButton *btnCreateBioSegue;
@property (weak, nonatomic) IBOutlet UIButton *btnBack;
- (IBAction)backToMainView:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *btnProfileSegue;
@property (weak, nonatomic) IBOutlet UIButton *btnSideBarOpen;
@property (weak, nonatomic) IBOutlet UIButton *btnPushForTaskDetail;
@property (weak, nonatomic) IBOutlet UIButton *btnSideBarClose;

@property (weak, nonatomic) IBOutlet UILabel *lblTitleText;

@property (weak, nonatomic) IBOutlet UIView *viewHolderWithHeader;
@property (weak, nonatomic) IBOutlet UIView *viewForSideBarBackground;
@property (weak, nonatomic) IBOutlet UIView *viewSideBar;
@property (weak, nonatomic) IBOutlet UIView *viewHolderWithoutHeader;
@property (weak, nonatomic) IBOutlet UIView *viewLoginContainer;

@property (weak, nonatomic) IBOutlet UITableView *tblViewSideBar;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintSideBarViewLeadingSpace; //-278

- (IBAction)closeSideBar:(id)sender;
- (IBAction)openSideBar:(id)sender;

-(void)sendMailToEmail:(NSString *)email;

@property (weak, nonatomic) IBOutlet UIButton *btnBackForPushNotificationView;
- (IBAction)backFromPushNotificationView:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *HolderViewWithHeaderForPushNotification;

//map view handlers
@property (weak, nonatomic) IBOutlet MKMapView *mapViewToShowUserLocationOnMap;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintMapViewTrailingSpace;    //-500
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintMapViewLeadingSpace;     //500
@property (weak, nonatomic) IBOutlet UIButton *btnBackFromMapView;
- (IBAction)backFromMap:(id)sender;
-(void)showRegionOnMapForCoordinates:(NSString *)strCoordinate;

@property (weak, nonatomic) IBOutlet UIButton *btnMapOnListView;
- (IBAction)pressMapIcon:(id)sender;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *sideBarBackBtnTop;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *backBtnTop;


@property (weak, nonatomic) IBOutlet UIButton *BtnTransparentMap;

- (IBAction)pressTransMapBtn:(id)sender;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *headerViewHeight;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consBtnMenuHeight;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consBtnMenuWidth;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consBtnMapBottom;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consBtnMapTrailing;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consBtnMapHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consBtnMapWidth;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consLblTitleTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lblTitleLblWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lblTitleLblHeight;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consBtnBackBottom;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consBtnBackTop;
//******************************

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *titleLblWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *titleLblTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *titleLblLeft;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *titleLblHeight;

@property (weak, nonatomic) IBOutlet UILabel *headerUnderLineLbl;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *headerUnderLineLblHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *headerUnderLineLblBottom;

@property (weak, nonatomic) IBOutlet UIView *headerBarView;


-(void) logOutProcess;

-(void)startCheckout;

@end

