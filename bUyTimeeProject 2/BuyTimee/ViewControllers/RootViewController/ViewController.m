//
//  ViewController.m
//  BuyTimee
//
//  Created by User14 on 26/02/16.

//

#import "ViewController.h"
#import "headers.pch"
#import "Constants.h"
#import "GlobalFunction.h"
#import "MyAnnotation.h"
#import "MapAnnotationViewController.h"
#import <DXAnnotationView.h>
#import <DXAnnotationSettings.h>
#import <QuartzCore/QuartzCore.h>
#import "LogOutTableViewCell.h"
#import "RequestTableViewCell.h"

#import "BraintreePayPal.h"
#import "PPDataCollector.h"

#define METERS_PER_MILE 1609.344

@interface ViewController () <BTViewControllerPresentingDelegate,BTAppSwitchDelegate, UIGestureRecognizerDelegate,UIAlertViewDelegate>

@property (nonatomic, strong) BTPayPalDriver *payPalDriver;


@end

@implementation ViewController



- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.tblViewSideBar layoutIfNeeded];
    [self.tblViewSideBar reloadData];
    [self.tblViewSideBar layoutIfNeeded];
    
    
    //adjust radius tap gesture
    UITapGestureRecognizer *tapGestureOnBGView = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideSideBar:)];
    tapGestureOnBGView.delegate = self;
    tapGestureOnBGView.numberOfTapsRequired = 1;
    [self.viewForSideBarBackground addGestureRecognizer:tapGestureOnBGView];
    
   
    self.btnBack.hidden = YES;
    
    self.btnBackForPushNotificationView.hidden = YES;
    
    //map view working
    self.btnBackFromMapView.hidden = YES;
    self.mapViewToShowUserLocationOnMap.hidden = YES;
    self.mapViewToShowUserLocationOnMap.delegate = self;
    self.mapViewToShowUserLocationOnMap.mapType = MKMapTypeStandard;
    self.mapViewToShowUserLocationOnMap.showsUserLocation = NO;

   
    
    [self.tblViewSideBar setScrollEnabled:NO];
    
    //[self startCheckout];
}

- (void)startCheckout {
    
    // Example: Initialize BTAPIClient, if you haven't already
    self.braintreeClient = [[BTAPIClient alloc] initWithAuthorization:@"eyJ2ZXJzaW9uIjoyLCJhdXRob3JpemF0aW9uRmluZ2VycHJpbnQiOiI3MjUxODQ2ZTE3YTFkOGIyYjI1OWRjMTBjYmU5NTUyYzNmNmRhNDQ3ODg4MzQ5ZDg4YjI3OTg1YTRhZGQ3NTZhfGNsaWVudF9pZD1jbGllbnRfaWQkc2FuZGJveCQ0ZHByYmZjNnBoNTk1Y2NqXHUwMDI2Y3JlYXRlZF9hdD0yMDE4LTA3LTA5VDA2OjA4OjQyLjI5NjExMzg3NyswMDAwXHUwMDI2bWVyY2hhbnRfaWQ9NHB0eXBjYjZxNGI1MnFkZCIsImNvbmZpZ1VybCI6Imh0dHBzOi8vYXBpLnNhbmRib3guYnJhaW50cmVlZ2F0ZXdheS5jb206NDQzL21lcmNoYW50cy80cHR5cGNiNnE0YjUycWRkL2NsaWVudF9hcGkvdjEvY29uZmlndXJhdGlvbiIsImNoYWxsZW5nZXMiOltdLCJlbnZpcm9ubWVudCI6InNhbmRib3giLCJjbGllbnRBcGlVcmwiOiJodHRwczovL2FwaS5zYW5kYm94LmJyYWludHJlZWdhdGV3YXkuY29tOjQ0My9tZXJjaGFudHMvNHB0eXBjYjZxNGI1MnFkZC9jbGllbnRfYXBpIiwiYXNzZXRzVXJsIjoiaHR0cHM6Ly9hc3NldHMuYnJhaW50cmVlZ2F0ZXdheS5jb20iLCJhdXRoVXJsIjoiaHR0cHM6Ly9hdXRoLnZlbm1vLnNhbmRib3guYnJhaW50cmVlZ2F0ZXdheS5jb20iLCJhbmFseXRpY3MiOnsidXJsIjoiaHR0cHM6Ly9vcmlnaW4tYW5hbHl0aWNzLXNhbmQuc2FuZGJveC5icmFpbnRyZWUtYXBpLmNvbS80cHR5cGNiNnE0YjUycWRkIn0sInRocmVlRFNlY3VyZUVuYWJsZWQiOmZhbHNlLCJwYXlwYWxFbmFibGVkIjp0cnVlLCJwYXlwYWwiOnsiZGlzcGxheU5hbWUiOiJ0ZXN0IGZhY2lsaXRhdG9yJ3MgVGVzdCBTdG9yZSIsImNsaWVudElkIjoiQVFoZW5UVW55SnhwWVV0VE9ITWxEZDdQOTloYkwzSXhibGlqVTk5MGNmczJuMGEzU2lFbTVOWXNiZjA1dUZ4WVltdF9CY2w2UENZVWo3SnYiLCJwcml2YWN5VXJsIjoiaHR0cHM6Ly9leGFtcGxlLmNvbSIsInVzZXJBZ3JlZW1lbnRVcmwiOiJodHRwczovL2V4YW1wbGUuY29tIiwiYmFzZVVybCI6Imh0dHBzOi8vYXNzZXRzLmJyYWludHJlZWdhdGV3YXkuY29tIiwiYXNzZXRzVXJsIjoiaHR0cHM6Ly9jaGVja291dC5wYXlwYWwuY29tIiwiZGlyZWN0QmFzZVVybCI6bnVsbCwiYWxsb3dIdHRwIjp0cnVlLCJlbnZpcm9ubWVudE5vTmV0d29yayI6ZmFsc2UsImVudmlyb25tZW50Ijoib2ZmbGluZSIsInVudmV0dGVkTWVyY2hhbnQiOmZhbHNlLCJicmFpbnRyZWVDbGllbnRJZCI6Im1hc3RlcmNsaWVudDMiLCJiaWxsaW5nQWdyZWVtZW50c0VuYWJsZWQiOnRydWUsIm1lcmNoYW50QWNjb3VudElkIjoiVVNEIiwiY3VycmVuY3lJc29Db2RlIjoiVVNEIn0sIm1lcmNoYW50SWQiOiI0cHR5cGNiNnE0YjUycWRkIiwidmVubW8iOiJvZmYifQ=="];
    self.payPalDriver = [[BTPayPalDriver alloc] initWithAPIClient:self.braintreeClient];
    //  BTPayPalDriver *payPalDriver = [[BTPayPalDriver alloc] initWithAPIClient:self.braintreeClient];
    self.payPalDriver.viewControllerPresentingDelegate = myAppDelegate.vcObj;
    //self.payPalDriver.appSwitchDelegate = self; // Optional
    
    BTPayPalRequest *checkout = [[BTPayPalRequest alloc] init];
    
        [self.payPalDriver authorizeAccountWithAdditionalScopes:[NSSet setWithArray:@[@"address"]]
                                                completion:^(BTPayPalAccountNonce *tokenizedPayPalAccount, NSError *error) {
            if (tokenizedPayPalAccount) {
                BTPostalAddress *address = tokenizedPayPalAccount.billingAddress ?: tokenizedPayPalAccount.shippingAddress;
                NSLog(@"nonce is...%@",tokenizedPayPalAccount.nonce);
                NSLog(@"Address:\n%@\n%@\n%@ %@\n%@ %@", address.streetAddress, address.extendedAddress, address.locality,address.region, address.postalCode, address.countryCodeAlpha2);
            } else if (error) {
                                // Handle error
                 NSLog(@"...Error occured...%@",error);
            } else {
                                // User canceled
                NSLog(@"...Use hit cancell button...");
            }
            }];
    
    
//    checkout.billingAgreementDescription = @"Your agreement description";
//    [self.payPalDriver requestBillingAgreement:checkout completion:^(BTPayPalAccountNonce * _Nullable tokenizedPayPalCheckout, NSError * _Nullable error) {
//        if (error)
//        {
//            //self.progressBlock(error.localizedDescription);
//        } else if (tokenizedPayPalCheckout) {
//            NSLog(@"Got a nonce! %@", tokenizedPayPalCheckout.nonce);
//
//            NSLog(@"Got a type is! %@", tokenizedPayPalCheckout.type);
//
//            NSLog(@"Got a type is! %@", tokenizedPayPalCheckout.payerId);
//
//            BTPostalAddress *address = tokenizedPayPalCheckout.billingAddress?: tokenizedPayPalCheckout.shippingAddress;
//            NSLog(@"Billing address:\n%@\n%@\n%@ %@\n%@ %@", address.streetAddress, address.extendedAddress, address.locality, address.region, address.postalCode, address.countryCodeAlpha2);
//        } else
//        {
//            //self.progressBlock(@"Cancelled");
//        }
//    }];
    
    
    NSString *deviceData = [PPDataCollector collectPayPalDeviceData];
    NSLog(@"Send this device data to your server: %@", deviceData);
    
}

- (void)appSwitcher:(id)appSwitcher didPerformSwitchToTarget:(BTAppSwitchTarget)target
{
    
}


#pragma mark - BTViewControllerPresentingDelegate

// Required
- (void)paymentDriver:(id)paymentDriver
requestsPresentationOfViewController:(UIViewController *)viewController {
    [self presentViewController:viewController animated:YES completion:nil];
}

// Required
- (void)paymentDriver:(id)paymentDriver
requestsDismissalOfViewController:(UIViewController *)viewController {
    [viewController dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - BTAppSwitchDelegate

// Optional - display and hide loading indicator UI
- (void)appSwitcherWillPerformAppSwitch:(id)appSwitcher {
    [self showLoadingUI];
    
    // You may also want to subscribe to UIApplicationDidBecomeActiveNotification
    // to dismiss the UI when a customer manually switches back to your app since
    // the payment button completion block will not be invoked in that case (e.g.
    // customer switches back via iOS Task Manager)
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(hideLoadingUI:)
                                                 name:UIApplicationDidBecomeActiveNotification
                                               object:nil];
}

- (void)appSwitcherWillProcessPaymentInfo:(id)appSwitcher {
    [self hideLoadingUI:nil];
}

#pragma mark - Private methods

- (void)showLoadingUI {
    //...
}

- (void)hideLoadingUI:(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIApplicationDidBecomeActiveNotification
                                                  object:nil];
    //....
}

-(void)viewWillAppear:(BOOL)animated
{
    self.btnMapOnListView.hidden = true;
    self.BtnTransparentMap.hidden = true;
}


-(void)viewDidAppear:(BOOL)animated
{
    
    
    
    _lblTitleText.textColor=[UIColor colorWithRed:63.0/255.0f green:67.0/255.0f blue:70.0/255.0f alpha:1.0];
    _headerUnderLineLbl.backgroundColor=[UIColor colorWithRed:196.0/255.0f green:196.0/255.0f blue:196.0/255.0f alpha:1.0];

    

    if(IS_IPHONE_5)
    {
        [self.view layoutIfNeeded];
        NSLog(@"%f",self.viewHolderWithHeader.frame.size.height);
        self.headerViewHeight.constant=60;
        CGRect newFrame = self.viewHolderWithHeader.frame;
        newFrame.size.height = 508;
        [self.viewHolderWithHeader setFrame:newFrame];
        NSLog(@"%f",self.viewHolderWithHeader.frame.size.height);
        [self.view layoutIfNeeded];
        
        self.lblTitleText.font=[self.lblTitleText.font fontWithSize:18];
        self.sideBarBackBtnTop.constant = 13;
        self.consBtnMenuHeight.constant = 44;
        
        //*****************
        
        _titleLblLeft.constant=25;
        _titleLblTop.constant=22;
        _lblTitleLblWidth.constant=135;
        _lblTitleText.text=@"B U Y   T I M E E";
       // _headerUnderLineLblHeight.constant=1.5f;
        
    }
    
    if(IS_IPHONE_4_OR_LESS)
    {
        
        [self.view layoutIfNeeded];
        NSLog(@"%f",self.viewHolderWithHeader.frame.size.height);
        self.headerViewHeight.constant=60;
        CGRect newFrame = self.viewHolderWithHeader.frame;
        newFrame.size.height = 420;
        [self.viewHolderWithHeader setFrame:newFrame];
        NSLog(@"%f",self.viewHolderWithHeader.frame.size.height);
        [self.view layoutIfNeeded];
        
        self.lblTitleText.font=[self.lblTitleText.font fontWithSize:18];
        self.sideBarBackBtnTop.constant = 13;
        self.consBtnMenuHeight.constant = 44;
        
        _titleLblLeft.constant=25;
        _titleLblTop.constant=22;
        _lblTitleLblWidth.constant=135;
        _lblTitleText.text=@"B U Y   T I M E E";

    }
    
    if (IS_IPHONE_6)
        
    {
        //cell.lblTaskPrice.font= [UIFont fontWithName:@"Lato-Bold" size:14];
        //  [_lblTitleText setFont:[UIFont fontWithName:@"Lato-Bold" size:19]];
        
        self.lblTitleText.font=[self.lblTitleText.font fontWithSize:21];
        [self.view layoutIfNeeded];
        self.consBtnBackBottom.constant = 5;
        self.sideBarBackBtnTop.constant = 18;
        self.consBtnMenuWidth.constant = 46;
        self.consBtnMapBottom.constant = 11;
        self.consBtnMapTrailing.constant = 9;
        
        
        //*****************
        
        
        _lblTitleText.text=@"B U Y   T I M E E";
        
        _titleLblWidth.constant=165;
        _titleLblLeft.constant=32;
        _titleLblTop.constant=28;
       // _headerUnderLineLblHeight.constant=1.5f;
        _headerUnderLineLblBottom.constant=0;
        
        // _headerViewHeight.constant=70;
        
        
    }
    
    
    if (IS_IPHONE_6P)
    {//cell.lblTaskPrice.font= [UIFont fontWithName:@"Lato-Bold" size:14];
        //  [_lblTitleText setFont:[UIFont fontWithName:@"Lato-Bold" size:19]];
        self.lblTitleText.font=[self.lblTitleText.font fontWithSize:23];
        [self.view layoutIfNeeded];
        self.consBtnBackTop.constant = 21;
        self.consBtnBackBottom.constant = 6;
        
        self.sideBarBackBtnTop.constant = 22;
        self.consBtnMenuWidth.constant = 50;
        self.consBtnMapBottom.constant = 9;
        self.consBtnMapTrailing.constant = 11;
        self.consBtnMapWidth.constant = 30;
        self.consBtnMapHeight.constant = 31;
        self.headerViewHeight.constant=76;
        self.consLblTitleTop.constant = 35;
        
        //******************
        
        _lblTitleText.text=@"B U Y   T I M E E";
        _titleLblWidth.constant=180;
        _titleLblLeft.constant=40;
        _titleLblTop.constant=32;
        [self.view layoutIfNeeded];
    }
    


}

-(void)hideSideBar:(UIGestureRecognizer *)gesture{
    
    [self closeSideBar:self.btnSideBarClose];

}

//-(void)viewWillAppear:(BOOL)animated{
//}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return 8;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cell;
    
    if (indexPath.row == 0) {
        
        BioInfoTableViewCell *cellBio = (BioInfoTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellBioIdentifier];
        
        [cellBio layoutIfNeeded];
        
        if (myAppDelegate.userData) {
     
            if (myAppDelegate.userData.userImage) {
                cellBio.imgViewUserImage.image = myAppDelegate.userData.userImage;
            }else{
                cellBio.imgViewUserImage.image = imageUserDefault;
            }
            
            cellBio.lblUserName.text = myAppDelegate.userData.username;
            cellBio.lblUserAddress.text = myAppDelegate.userData.location;
        }else{
            cellBio.lblUserName.text = @"Name";
            cellBio.lblUserAddress.text =@""; //@"Santa Ana, CA";
        }
//        cellBio.imgViewUserImage.layer.cornerRadius = cellBio.imgViewUserImage.frame.size.height/2;
//        cellBio.imgViewUserImage.layer.masksToBounds = YES;
        
     
        
        if(IS_IPHONE_6)
        {
            //  cellBio.imgViewUserImage.image = [UIImage imageNamed:@"radioSelectedButton.png"];
            cellBio.cellBioImgViewTop.constant=15;
            cellBio.cellBioImgViewWidth.constant=36;
            cellBio.cellBioImgViewHeight.constant=36;
            cellBio.cellBioLblUserNameTop.constant=7;
            cellBio.cellBioLblUserNameLeft.constant=8;
            cellBio.imgViewUserImage.layer.cornerRadius = cellBio.imgViewUserImage.frame.size.height/2;
            cellBio.imgViewUserImage.layer.masksToBounds = YES;
            
        }
        else
        {
            cellBio.imgViewUserImage.layer.cornerRadius = cellBio.imgViewUserImage.frame.size.height/2;
            cellBio.imgViewUserImage.layer.masksToBounds = YES;
        }
           [cellBio layoutIfNeeded];
        
        return cellBio;
        
    }
    else if (indexPath.row == 1) {
        
        
        TaskTableViewCell *cellTask = (TaskTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellTaskIdentifier];
        
       // cellTask.imgViewIcon.image = [UIImage imageNamed:@"add_btn"];
       // cellTask.lblList.text = @"Tasks";
       //  cellTask.lblList.text = @"Home";
        cellTask.lblList.text = @"Browse Appointments";
        
        cell.backgroundColor=[UIColor redColor];
        
    if(IS_IPHONE_6)
    {

        
        
    }
        
        
        
        
        return cellTask;

    }
    else if (indexPath.row == 2) {
        
        
        CreateTaskTableViewCell *cellCreateTask = (CreateTaskTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellCreateTaskIdentifier];
        
        // cellCreateTask.lblList.text = @"Create Tasks";
        //  cellCreateTask.lblList.text = @"Create Listing";
        cellCreateTask.lblList.text = @"Sell Appointments";
        
        return cellCreateTask;
        
        
//        TaskHistoryTableViewCell *cellTaskHistory = (TaskHistoryTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellTaskHistoryIdentifier];
//        
//        if(IS_IPHONE_5 || IS_IPHONE_4_OR_LESS)
//        {
//            cellTaskHistory.consBgRedViewTop.constant = 12;
//            
//        }
//        if(IS_IPHONE_6P)
//        {
//            cellTaskHistory.consBgRedViewTop.constant = 21;
//            
//        }
//        //   cellTaskHistory.lblReviewNo.text = @"9";
//        // [cellTaskHistory.BgRedView.layer setCornerRadius:cellTaskHistory.BgRedView.frame.size.height/2];
//        if (myAppDelegate.isFromSkip)
//        {
//            cellTaskHistory.BgRedView.hidden = true;
//        }
//        else
//        {
//            if (![myAppDelegate.reviewCounter_ForHamburger isEqualToString:@"0"])
//            {
//                cellTaskHistory.BgRedView.hidden = false;
//                
//                cellTaskHistory.lblReviewNo.text = myAppDelegate.reviewCounter_ForHamburger;
//                
//                [cellTaskHistory.BgRedView.layer setCornerRadius:cellTaskHistory.BgRedView.frame.size.height/2];
//            }
//            else
//            {
//                cellTaskHistory.BgRedView.hidden = true;
//            }
//            
//        }
//        
//        cellTaskHistory.lblList.text = @"My Activity";
//        
//        return cellTaskHistory;

    }
    else if (indexPath.row == 3) {
   
        
        
        
        TaskHistoryTableViewCell *cellTaskHistory = (TaskHistoryTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellTaskHistoryIdentifier];
        
        if(IS_IPHONE_5 || IS_IPHONE_4_OR_LESS)
        {
            cellTaskHistory.consBgRedViewTop.constant = 12;
            
        }
        if(IS_IPHONE_6P)
        {
            cellTaskHistory.consBgRedViewTop.constant = 21;
            
        }
        //   cellTaskHistory.lblReviewNo.text = @"9";
        // [cellTaskHistory.BgRedView.layer setCornerRadius:cellTaskHistory.BgRedView.frame.size.height/2];
        if (myAppDelegate.isFromSkip)
        {
            cellTaskHistory.BgRedView.hidden = true;
        }
        else
        {
            if (![myAppDelegate.reviewCounter_ForHamburger isEqualToString:@"0"])
            {
                cellTaskHistory.BgRedView.hidden = false;
                
                cellTaskHistory.lblReviewNo.text = myAppDelegate.reviewCounter_ForHamburger;
                
                [cellTaskHistory.BgRedView.layer setCornerRadius:cellTaskHistory.BgRedView.frame.size.height/2];
            }
            else
            {
                cellTaskHistory.BgRedView.hidden = true;
            }
            
        }
        
        cellTaskHistory.lblList.text = @"My Activity";
        
        return cellTaskHistory;
        
        
//        //
//        CreateTaskTableViewCell *cellCreateTask = (CreateTaskTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellCreateTaskIdentifier];
//        
//        // cellCreateTask.lblList.text = @"Create Tasks";
//      //  cellCreateTask.lblList.text = @"Create Listing";
//        cellCreateTask.lblList.text = @"Sell Appointments";
//        
//        return cellCreateTask;

    }
    else if (indexPath.row == 4) {
        

        
        
        //cellRequestReservation
        RequestTableViewCell *cellRequest = (RequestTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellRequestReservation];
        cellRequest.lblList1.text = @"Request Appointment";
        return cellRequest;
        
       

    }
    else if (indexPath.row == 5) {
        
        
        
        ActiveTaskTableViewCell *cellActiveTask = (ActiveTaskTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellActiveTaskIdentifier];
        
        cellActiveTask.lblList.text = @"About Us";
        
        return cellActiveTask;
        
     
    }
    else if (indexPath.row == 6) {
        
        TnCTableViewCell *cellTnC = (TnCTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellTermsNConditionIdentifier];
        
        cellTnC.lblList.text =@"Terms of Use";
        
        return cellTnC;
        
           }
    else if(indexPath.row == 7)
    {
        
        if(myAppDelegate.isFromSkip==YES)
        {
            LogOutTableViewCell *cellLogOut = (LogOutTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"cellLogOut"];
            cellLogOut.lblList1.text = @"Login";
            return cellLogOut;
            
        }
        
        else
        {
            LogOutTableViewCell *cellLogOut = (LogOutTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"cellLogOut"];
            cellLogOut.lblList1.text = @"Logout";
            return cellLogOut;
            
        }
        

        
    }

    
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
  
    if (indexPath.row == 0) {
        
       // cell.backgroundColor = colorGreenDark;
    }
    else{
        
        cell.backgroundColor = [UIColor clearColor];

    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{

    CGFloat height;
    
    if (IS_IPHONE_4_OR_LESS) {
        
        if (indexPath.row == 0) {
            return 70;
        }else if(indexPath.row == 3 && tableView == _tblViewSideBar){
            return 50;
        }
        
        else{
            return 50;
        }

    }
    if (IS_IPHONE_5) {
        
        if (indexPath.row == 0) {
            return 70;
        }else if(indexPath.row == 3 && tableView == _tblViewSideBar){
            return 50;
        }
        else{
            return 50;
        }

    }
    if (IS_IPHONE_6) {
       
        if (indexPath.row == 0) {
            return 78;
        }else if(indexPath.row == 3 && tableView == _tblViewSideBar){
            return 65;
        }
        else
        {
            return 65;
        }

    }
    if (IS_IPHONE_6P) {
        
        if (indexPath.row == 0) {
            return 70;
        }else if(indexPath.row == 3 && tableView == _tblViewSideBar){
            return 70;
        }
        else{
            return 70;
        }
        
    }
    
    return height;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
   
    if (indexPath.row == 7)
    {
        
        if (myAppDelegate.isFromSkip) {
            
            // [self closeSideBar:self.btnSideBarClose];
            
//            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"BuyTimee" message:@"You need to login first." delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Login", nil];
//            
//            alert.tag = 1003;
//            
//            [alert show];
//            
//            return;
            
            
            
            CATransition *transitionAnimation = [CATransition animation];
            [transitionAnimation setType:kCATransitionPush];
            [transitionAnimation setSubtype:kCATransitionFromLeft];
            [transitionAnimation setDuration:0.4f];
            [transitionAnimation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
            [transitionAnimation setFillMode:kCAFillModeBoth];
            
            
            
            //  [myAppDelegate.activeTaskVCObj.viewTaskDetailHolder.layer addAnimation:transitionAnimation forKey:@"changeView"];
            
            [myAppDelegate.vcObj.viewLoginContainer.layer addAnimation:transitionAnimation forKey:@"changeView"];
            
            
            for(UIView *view in myAppDelegate.vcObj.viewLoginContainer.subviews){
                
                [view removeFromSuperview];
                
            }
            
            
            for(UIView *view in myAppDelegate.vcObj.viewHolderWithHeader.subviews){
                
                [view removeFromSuperview];
                
            }
            
            [myAppDelegate.vcObj performSegueWithIdentifier:@"login" sender:myAppDelegate.loginVCObj];
            myAppDelegate.vcObj.btnBack.hidden = YES;
            myAppDelegate.vcObj.btnSideBarOpen.hidden = YES;
            // myAppDelegate.vcObj = nil;
            myAppDelegate.termsNconditionVCObj = nil;
            myAppDelegate.stringNameOfChildVC = @"";
            myAppDelegate.TandC_Buyee = @"";
            return;

            
            
            
            
        }

        
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"BUY TIMEE" message:@"Are you sure to logout from BUY TIMEE?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Logout", nil];
        
        alert.tag = 1010;
        
        [alert show];
        
        return;
        
        }
    
    
     myAppDelegate.isSideBarAccesible = true;
    if(indexPath.row == 0){
        
        if (myAppDelegate.isFromSkip) {
            
            // [self closeSideBar:self.btnSideBarClose];
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"BUY TIMEE" message:@"You need to login first." delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Login", nil];
            
            alert.tag = 1003;
            
            [alert show];
            
            return;
        
        }
       // myAppDelegate.isSideBarAccesible = false;
        
        
        
        
        
//        if(myAppDelegate.isTermsAndPolicyViewOpen==YES)
//        {
//            [[GlobalFunction shared] showAlertForMessage:@"Please accept Terms of service."];
//        }
//        else
       // {
           [myAppDelegate.vcObj performSegueWithIdentifier:@"createBio" sender:myAppDelegate.vcObj.btnCreateBioSegue];
        //}
        
        
    }
    
}

-(BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender{
    
    NSLog(@"value of myAppDelegate.TandC_Buyee in should perform is...%@",myAppDelegate.TandC_Buyee);
     NSLog(@"value of identifier in should perform is...%@",identifier);
    if (myAppDelegate.isFromSkip) {
        
        
        if ([identifier isEqualToString:@"termsNCondition"] )//|| [identifier isEqualToString:@"viewAllTask"])
        {
            return YES;
        }
        
        if ([identifier isEqualToString:@"activetask"] )
        {
            return YES;
        }

        
        if ([identifier isEqualToString:@"viewAllTask"] && [myAppDelegate.TandC_Buyee isEqualToString:@"home"]) {
            
            [self closeSideBar:self.btnSideBarClose];
            
            return NO;
            
        }
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"BUY TIMEE" message:@"You need to login first." delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Login", nil];
        
        alert.tag = 1003;
        
        [alert show];
        
        return NO;
        
    }
    
    if ([identifier isEqualToString:@"viewAllTask"] && [myAppDelegate.TandC_Buyee isEqualToString:@"home"]) {
        
        [self closeSideBar:self.btnSideBarClose];
        
        return NO;
        
    }
    
//    if ([identifier isEqualToString:@"activetask"] || [identifier isEqualToString:@"taskhistory"]) {
//        
//        [self closeSideBar:self.btnSideBarClose];
//        
//        return NO;
//        
//    }
    
    if (myAppDelegate.isFromPushNotification)
    {
        if ([identifier isEqualToString:@"taskhistory"])
        {
            
            //  myAppDelegate.mapVCObj = nil;
            // myAppDelegate.mapVCObj = (MapViewController *)segue.destinationViewController;
            [myAppDelegate.vcObj performSegueWithIdentifier:@"taskhistory" sender:nil];
            return YES;
        }
    }
    

    
//    if ([identifier isEqualToString:@"createTask"]) {
//      
//        if (myAppDelegate.isBankDetailFilled == false) {
//            
//            [self closeSideBar:self.btnSideBarClose];
//            
//            [GlobalFunction removeIndicatorView];
//            
//            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"BuyTimee" message:@"You need to fill your bank information first, before creating any reservation." delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Go to fill bank information", nil];
//            
//            
//            
//            alert.tag = 1009;
//            
//            [alert show];
//            
//            return NO;
//        }
//        
//    }

    
    NSLog(@"%@",myAppDelegate.userData.tnc);
    
    
    if ([identifier isEqualToString:@"viewAllTask"])
        {
            
            if(myAppDelegate.isTermsAndPolicyViewOpen==YES)
            {
                [[GlobalFunction shared] showAlertForMessage:@"Please accept Terms of service."];
                return NO;

            }
            
            else
            {
                if(myAppDelegate.userData.mobile.length>1)
                {
                    
                }
                
                else
                {
                    [[GlobalFunction shared] showAlertForMessage:@"Please enter valid contact no."];
                    [self closeSideBar:self.btnSideBarClose];
                    return NO;
                }

            }
            
        }
    
    
    if ([identifier isEqualToString:@"taskhistory"])
    {
        if(myAppDelegate.isTermsAndPolicyViewOpen==YES)
        {
            [[GlobalFunction shared] showAlertForMessage:@"Please accept Terms of service."];
            return NO;

        }
        
        else
        {
            if(myAppDelegate.userData.mobile.length>1)
            {
                
            }
            
            else
            {
                [[GlobalFunction shared] showAlertForMessage:@"Please enter valid contact no."];
                [self closeSideBar:self.btnSideBarClose];
                return NO;
            }
            
        }
    }
    
    if ([identifier isEqualToString:@"activetask"])   //about us
    {
        if(myAppDelegate.userData.mobile.length>1)
        {
            
        }
        
        else
        {
            if(myAppDelegate.isTermsAndPolicyViewOpen==YES)
            {
                [[GlobalFunction shared] showAlertForMessage:@"Please accept Terms of service."];
                return NO;

            }
            
            else
            {
                if(myAppDelegate.userData.mobile.length>1)
                {
                    
                }
                
                else
                {
                    [[GlobalFunction shared] showAlertForMessage:@"Please enter valid contact no."];
                    [self closeSideBar:self.btnSideBarClose];
                    return NO;
                }
                
            }
        }
    }

    if ([identifier isEqualToString:@"termsNCondition"] )
    {
        if(myAppDelegate.userData.mobile.length>1)
        {
            
        }
        
        else
        {
            if(myAppDelegate.isTermsAndPolicyViewOpen==YES)
            {
                [[GlobalFunction shared] showAlertForMessage:@"Please accept Terms of service."];
                return NO;

            }
            
            else
            {
                if(myAppDelegate.userData.mobile.length>1)
                {
                    
                }
                
                else
                {
                    [[GlobalFunction shared] showAlertForMessage:@"Please enter valid contact no."];
                    [self closeSideBar:self.btnSideBarClose];
                    return NO;
                }
                
            }
        }

    }

    if ([identifier isEqualToString:@"request"] )
    {
        if(myAppDelegate.userData.mobile.length>1)
        {
            
        }
        
        else
        {
            if(myAppDelegate.isTermsAndPolicyViewOpen==YES)
            {
                [[GlobalFunction shared] showAlertForMessage:@"Please accept Terms of service."];
                return NO;
                
            }
            
            else
            {
                if(myAppDelegate.userData.mobile.length>1)
                {
                    
                }
                
                else
                {
                    [[GlobalFunction shared] showAlertForMessage:@"Please enter valid contact no."];
                    [self closeSideBar:self.btnSideBarClose];
                    return NO;
                }
                
            }
        }
    }
    
    if ([identifier isEqualToString:@"createTask"])
    {
        
        // return YES; // added by vs on 5June18 remove comment when build.
        if(myAppDelegate.userData.mobile.length>1)
        {
            
            if([myAppDelegate.selectedPaymentTypeFromServer isEqualToString:@"0"])
            {
                [self closeSideBar:self.btnSideBarClose];
                
                [GlobalFunction removeIndicatorView];
                
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"BUY TIMEE" message:@"You need to choose your payment option on profile page, before creating the appointments." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                
                // old one.. You need to add your Cashout with PayPal information before creating the reservation.
                
                // alert.tag = 1009;
                
                [alert show];
                
                return NO;
            }
           else if([myAppDelegate.selectedPaymentTypeFromServer isEqualToString:@"1"])
            {
               

                if (myAppDelegate.isBankDetailFilled == false) { // false original condition set 8 oct 18
                    
                    [self closeSideBar:self.btnSideBarClose];
                    
                    [GlobalFunction removeIndicatorView];
                    
                      UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"BUY TIMEE" message:@"You need to fill in your bank information first, before creating any reservation." delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Go to fill bank information", nil];
                    
                    // old one.. You need to add your Cashout with PayPal information before creating the reservation.
                    
                    alert.tag = 1009;
                    
                    [alert show];
                    
                    return NO;
                }
            }
               
                
            
           else if([myAppDelegate.selectedPaymentTypeFromServer isEqualToString:@"2"])
           {
               if (myAppDelegate.isBankDetailFilled == false) { // false original condition set 8 oct 18
                   
                   [self closeSideBar:self.btnSideBarClose];
                   
                   [GlobalFunction removeIndicatorView];
                   
                   UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"BUY TIMEE" message:@"You need to add your Cashout with PayPal information before creating the appointments." delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Cashout with PayPal", nil];
                   
                   // old one.. You need to add your Cashout with PayPal information before creating the reservation.
                   
                   alert.tag = 1008;
                   
                   [alert show];
                   
                   return NO;
               }
           }
            
            
        }
        
        else
        {
            if(myAppDelegate.isTermsAndPolicyViewOpen==YES)
            {
                [[GlobalFunction shared] showAlertForMessage:@"Please accept Terms of service."];
                return NO;
                
            }
            
            else
            {
                if(myAppDelegate.userData.mobile.length>1)
                {
                    
                }
                
                else
                {
                    [[GlobalFunction shared] showAlertForMessage:@"Please enter valid contact no."];
                    [self closeSideBar:self.btnSideBarClose];
                    return NO;
                }
                
            }
        }
        
        
    }
    return YES;
}





-(void) logOutProcess
{
    
    //************ BY MITESH ON DATED 25-JAN-2018**************
    myAppDelegate.TandC_Buyee =@"";
    myAppDelegate.isFromCreateBioOnTnC = false;
    //*******************************************************
    
    myAppDelegate.userData.userImage = nil;
    [self.tblViewSideBar reloadData];
    
    [myAppDelegate.LoginCheck removeObjectForKey:@"savingLoginId"];
    [myAppDelegate.LoginCheck removeObjectForKey:@"savingMailId"];
    [myAppDelegate.LoginCheck removeObjectForKey:@"flagValue"];
    [myAppDelegate.LoginCheck synchronize];
}


-(void) pressMapIcon:(id)sender
{
    [myAppDelegate.viewAllTaskVCObj performSelector:@selector(checkSelectedBtnsForForwardFilter) withObject:nil afterDelay:0.1];

    
    [myAppDelegate.viewAllTaskVCObj shouldPerformSegueWithIdentifier:@"mapView" sender:nil];
    
  //   self.btnMapOnListView.hidden = true;
   // [myAppDelegate.viewAllTaskVCObj prepareForSegue:<#(nonnull UIStoryboardSegue *)#> sender:<#(nullable id)#>
}

- (IBAction)pressTransMapBtn:(id)sender {
    
    [self pressMapIcon:_BtnTransparentMap];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    [self closeSideBar:self.btnSideBarClose];
    
    if (!myAppDelegate.isComingFromPushNotification) {
       
        if (myAppDelegate.isSideBarAccesible) {
            self.btnSideBarOpen.hidden = NO;
            self.btnBack.hidden = YES;
        }else{
            self.btnSideBarOpen.hidden = YES;
            self.btnBack.hidden = NO;
        }

        myAppDelegate.superViewClassName = @"";
        
        if ([segue.identifier isEqualToString:@"viewAllTask"]) {
            
            
                myAppDelegate.TandC_Buyee = @"home";
                
                self.btnMapOnListView.hidden = false;
                self.BtnTransparentMap.hidden = false;
                
                myAppDelegate.activeViewControllerClassName = [NSString stringWithFormat:@"%@",[ViewAllTaskViewController class]];
                
                myAppDelegate.viewAllTaskVCObj = nil;
                
                myAppDelegate.viewAllTaskVCObj = (ViewAllTaskViewController *)segue.destinationViewController;

            
            
        }
        else if ([segue.identifier isEqualToString:@"createBio"]) {
            
            myAppDelegate.activeViewControllerClassName = [NSString stringWithFormat:@"%@",[CreateBioViewController class]];
            
            myAppDelegate.createBioVCObj = nil;
            
            myAppDelegate.createBioVCObj = (CreateBioViewController *)segue.destinationViewController;
            
        }
        else if ([segue.identifier isEqualToString:@"activetask"]) {
            
            [GlobalFunction addIndicatorView];
            
            self.btnSideBarOpen.hidden = YES;
            self.btnBack.hidden = NO;

            myAppDelegate.activeTaskVCObj = nil;
            
            myAppDelegate.activeTaskVCObj = (ActiveTaskViewController *)segue.destinationViewController;
            
        }
        else if ([segue.identifier isEqualToString:@"taskhistory"]) {
            
            
                myAppDelegate.TandC_Buyee = @"myActivity";
                
                myAppDelegate.activeViewControllerClassName = [NSString stringWithFormat:@"%@",[TaskHistoryViewController class]];
                
                [GlobalFunction addIndicatorView];
                
                myAppDelegate.taskHistoryVCObj = nil;
                
                myAppDelegate.taskHistoryVCObj = (TaskHistoryViewController *)segue.destinationViewController;
            
            
        }
        else if ([segue.identifier isEqualToString:@"termsNCondition"]) {
            
//            myAppDelegate.activeViewControllerClassName = [NSString stringWithFormat:@"%@",[TermsViewController class]];
//

           // self.btnSideBarOpen.hidden = NO;  //comment by vs for check back btn conditions and set yes to no for check on 30 jan 17
            self.btnSideBarOpen.hidden = YES;
            self.btnBack.hidden = NO;
            // self.btnBack.hidden = YES; //comment by vs for check back btn conditions and set yes to no for check on 29 jan 17
            
//            if([UserDefaults boolForKey:keyAgreeTnC]){
//                
//                self.btnBack.hidden = NO;
//                
//            }else{
//                
//                // self.btnBack.hidden = YES; //comment by vs for check back btn conditions and set yes to no for check on 29 jan 17
//                self.btnBack.hidden = YES;
//                self.btnSideBarOpen.hidden = YES;
//                
//            }
            
            [GlobalFunction addIndicatorView];
//            myAppDelegate.stringNameOfChildVC = [NSString stringWithFormat:@"%@",[self class]];
//            NSLog(@"name of stringNameOfChildVC in tnc is...%@",myAppDelegate.stringNameOfChildVC);
            
            myAppDelegate.termsNconditionVCObj = nil;
            
            myAppDelegate.termsNconditionVCObj = (TermsViewController *)segue.destinationViewController;
            
        }
        else if ([segue.identifier isEqualToString:@"login"]) {
            
            myAppDelegate.loginVCObj = nil;
            
            myAppDelegate.loginVCObj = (LoginViewController *)segue.destinationViewController;
            
            [self.viewLoginContainer setHidden:NO];
            
        }
        else if ([segue.identifier isEqualToString:@"paymentInfo"]) {
            
//            if(myAppDelegate.isFromTnC){
//                self.btnSideBarOpen.hidden = YES;
//                self.btnBack.hidden = YES;
//            }else{
//                
//            }
            
            myAppDelegate.isPaymentORBankInfo = true;
            
            myAppDelegate.paymentInfoVCObj = nil;
            
            myAppDelegate.paymentInfoVCObj = (PaymentInfoViewController *)segue.destinationViewController;
            
        }
        else if ([segue.identifier isEqualToString:@"BankInfoFromRoot"]) {
            
            //            if(myAppDelegate.isFromTnC){
            //                self.btnSideBarOpen.hidden = YES;
            //                self.btnBack.hidden = YES;
            //            }else{
            //
            //            }
            
          //  myAppDelegate.isPaymentORBankInfo = true;
            
            myAppDelegate.TandC_Buyee = @"home";
            
            myAppDelegate.setUpBankInfoInfObj = nil;
            
            myAppDelegate.setUpBankInfoInfObj = (SetUpBankViewController *)segue.destinationViewController;
            
        }
        else if ([segue.identifier isEqualToString:@"cashOutPayPalFromRoot"]) {
            
            //            if(myAppDelegate.isFromTnC){
            //                self.btnSideBarOpen.hidden = YES;
            //                self.btnBack.hidden = YES;
            //            }else{
            //
            //            }
            
            //  myAppDelegate.isPaymentORBankInfo = true;
            
            myAppDelegate.TandC_Buyee = @"home";
            
            myAppDelegate.cashOutPayPalVCObj = nil;
            
            myAppDelegate.cashOutPayPalVCObj = (cashOutPayPalViewController *)segue.destinationViewController;
            
        }

        else if ([segue.identifier isEqualToString:@"ShareViewController"]) {
            
            myAppDelegate.shareVcObj = nil;
            myAppDelegate.shareVcObj = (ShareViewController *)segue.destinationViewController;
            
        }
        
        else if ([segue.identifier isEqualToString:@"review"]) {
            
            myAppDelegate.reviewVcObj = nil;
            myAppDelegate.reviewVcObj = (ReviewViewController *)segue.destinationViewController;
            
            myAppDelegate.vcObj.btnBack.hidden = NO;
            myAppDelegate.vcObj.btnSideBarOpen.hidden = YES;

            
        }
   
        else if ([segue.identifier isEqualToString:@"rateViewController"]) {
            
            self.btnSideBarOpen.hidden = YES;
            self.btnBack.hidden = NO;
            
            myAppDelegate.ratingVCObj = nil;
            myAppDelegate.ratingVCObj = (RatingViewController *)segue.destinationViewController;
            
        }
        
        
        else if ([segue.identifier isEqualToString:@"createTask"]) {
            
            
                myAppDelegate.TandC_Buyee = @"create";
                
                self.btnSideBarOpen.hidden = NO;
                self.btnBack.hidden = YES;
                
                myAppDelegate.activeViewControllerClassName = [NSString stringWithFormat:@"%@",[TaskCreateViewController class]];
                
                myAppDelegate.taskCreateVCObj = nil;
                
                myAppDelegate.taskCreateVCObj = (TaskCreateViewController *)segue.destinationViewController;
   
            
            
        }
        else if ([segue.identifier isEqualToString:@"profile"]) {
            
            [GlobalFunction addIndicatorView];
            
            if (!myAppDelegate.isComingFromPushNotification) {
                
                myAppDelegate.stringNameOfChildVC = [NSString stringWithFormat:@"%@",[self class]];
                
            }
            
            myAppDelegate.profileVCObj = nil;
            
            myAppDelegate.profileVCObj = (ProfileViewController *)segue.destinationViewController;
            
        }
        else if ([segue.identifier isEqualToString:@"settings"]) {
            
            //        self.btnSideBarOpen.hidden = NO;
            //        self.btnBack.hidden = YES;
            
            
            
            [GlobalFunction addIndicatorView];
            
            myAppDelegate.settingsVCObj = nil;
            
            myAppDelegate.settingsVCObj = (SettingsViewController *)segue.destinationViewController;
            
           // [myAppDelegate.vcObj performSegueWithIdentifier:@"createBio" sender:myAppDelegate.vcObj.btnCreateBioSegue];
            
            // [self performSegueWithIdentifier:@"paymentInfo" sender:myAppDelegate.vcObj.btnPaymentInfo];
            
        }
        else if ([segue.identifier isEqualToString:@"pushTask"]) {
            
            self.btnBackForPushNotificationView.hidden = NO;
            
            [GlobalFunction addIndicatorView];
            
            myAppDelegate.taskDetailPushVCObj = nil;
            
            myAppDelegate.taskDetailPushVCObj = (TaskDetailViewController *)segue.destinationViewController;
            
            NSLog(@"task detail push obj === %@",myAppDelegate.taskDetailPushVCObj);
            
        }
        else if ([segue.identifier isEqualToString:@"request"]) {
            
            self.btnSideBarOpen.hidden = NO;
            self.btnBack.hidden = YES;
            
            myAppDelegate.activeViewControllerClassName = [NSString stringWithFormat:@"%@",[RequestReservationViewController class]];
            
            myAppDelegate.requestReservationVcObj = nil;
            
            myAppDelegate.requestReservationVcObj = (RequestReservationViewController *)segue.destinationViewController;
             NSLog(@"task detail push obj === %@",myAppDelegate.activeViewControllerClassName);
            
            
        }

        //        else if ([segue.identifier isEqualToString:@"bankinfo"]) {
        //
        //            if(myAppDelegate.isFromTnC){
        //                self.btnSideBarOpen.hidden = YES;
        //                self.btnBack.hidden = YES;
        //            }else{
        //
        //            }
        //
        //            myAppDelegate.isPaymentORBankInfo = true;
        //
        //            myAppDelegate.bankInfoVCObj = nil;
        //
        //            myAppDelegate.bankInfoVCObj = (BankInfoViewController *)segue.destinationViewController;
        //
        //        }

    }else{
        
        if ([segue.identifier isEqualToString:@"pushTask"]) {
            self.btnBackForPushNotificationView.hidden = NO;
            self.btnSideBarOpen.hidden = YES;
            [GlobalFunction addIndicatorView];
            myAppDelegate.taskDetailPushVCObj = nil;
            myAppDelegate.taskDetailPushVCObj = (TaskDetailViewController *)segue.destinationViewController;
        }
        
    }
    
}

-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex{
    
    if (alertView.tag == 1009) {
        
        if (buttonIndex == [alertView cancelButtonIndex]) {
            
            
        }else{
            
            myAppDelegate.isSideBarAccesible = false;
            
            myAppDelegate.isFromEdit = false;
          // myAppDelegate.superViewClassName =@"new_create_bio";
        //    [self performSegueWithIdentifier:@"paymentInfo" sender:myAppDelegate.vcObj.btnPaymentInfo];
           [self performSegueWithIdentifier:@"BankInfoFromRoot" sender:nil];//myAppDelegate.vcObj.btnBankInfo
            
           
           // [self performSegueWithIdentifier:@"cashOutPayPalFromRoot" sender:nil]; // for go on cashout screen added by VS on 10 Oct 18...
            
        }
        
    }
    if (alertView.tag == 1008) {
        
        if (buttonIndex == [alertView cancelButtonIndex]) {
            
            
        }else{
            
            myAppDelegate.isSideBarAccesible = false;
            
            myAppDelegate.isFromEdit = false;
            // myAppDelegate.superViewClassName =@"new_create_bio";
            //    [self performSegueWithIdentifier:@"paymentInfo" sender:myAppDelegate.vcObj.btnPaymentInfo];
           // [self performSegueWithIdentifier:@"BankInfoFromRoot" sender:nil];//myAppDelegate.vcObj.btnBankInfo
            
            
             [self performSegueWithIdentifier:@"cashOutPayPalFromRoot" sender:nil]; // for go on cashout screen added by VS on 10 Oct 18...
            
        }
        
    }
    if (alertView.tag == 1003) {
        
        if (buttonIndex == [alertView cancelButtonIndex]) {
            
            
        }else{
            
            
            
            CATransition *transitionAnimation = [CATransition animation];
            [transitionAnimation setType:kCATransitionPush];
            [transitionAnimation setSubtype:kCATransitionFromLeft];
            [transitionAnimation setDuration:0.4f];
            [transitionAnimation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
            [transitionAnimation setFillMode:kCAFillModeBoth];
            
            
            
            //  [myAppDelegate.activeTaskVCObj.viewTaskDetailHolder.layer addAnimation:transitionAnimation forKey:@"changeView"];
            
            [myAppDelegate.vcObj.viewLoginContainer.layer addAnimation:transitionAnimation forKey:@"changeView"];
            
            
            for(UIView *view in myAppDelegate.vcObj.viewLoginContainer.subviews){
                
                [view removeFromSuperview];
                
            }
            
            
            for(UIView *view in myAppDelegate.vcObj.viewHolderWithHeader.subviews){
                
                [view removeFromSuperview];
                
            }
            
            [myAppDelegate.vcObj performSegueWithIdentifier:@"login" sender:myAppDelegate.loginVCObj];
            myAppDelegate.vcObj.btnBack.hidden = YES;
            myAppDelegate.vcObj.btnSideBarOpen.hidden = YES;
            // myAppDelegate.vcObj = nil;
            myAppDelegate.termsNconditionVCObj = nil;
            myAppDelegate.stringNameOfChildVC = @"";
            myAppDelegate.TandC_Buyee = @"";
            return;
            

            
            
            
//            myAppDelegate.stringNameOfChildVC = @"BackToLogin";
//            
//            [self backToMainView:nil];
            
        }
        
    }

    if (alertView.tag == 1010) {
        
        if (buttonIndex == [alertView cancelButtonIndex]) {
            
            
        }else{
            
            
            [self logOutProcess];
            
            CATransition *transitionAnimation = [CATransition animation];
            [transitionAnimation setType:kCATransitionPush];
            [transitionAnimation setSubtype:kCATransitionFromLeft];
            [transitionAnimation setDuration:0.4f];
            [transitionAnimation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
            [transitionAnimation setFillMode:kCAFillModeBoth];
            
            
            
            //  [myAppDelegate.activeTaskVCObj.viewTaskDetailHolder.layer addAnimation:transitionAnimation forKey:@"changeView"];
            
            [myAppDelegate.vcObj.viewLoginContainer.layer addAnimation:transitionAnimation forKey:@"changeView"];
            
            
            for(UIView *view in myAppDelegate.vcObj.viewLoginContainer.subviews){
                
                [view removeFromSuperview];
                
            }
            
            
            for(UIView *view in myAppDelegate.vcObj.viewHolderWithHeader.subviews){
                
                [view removeFromSuperview];
                
            }
            
            [myAppDelegate.vcObj performSegueWithIdentifier:@"login" sender:myAppDelegate.loginVCObj];
            myAppDelegate.vcObj.btnBack.hidden = YES;
            myAppDelegate.vcObj.btnSideBarOpen.hidden = YES;
            // myAppDelegate.vcObj = nil;
            myAppDelegate.viewAllTaskVCObj = nil;
            myAppDelegate.stringNameOfChildVC = @"";
            
            
            
            
            return;
            
            
            

            
        }
    }
}




- (IBAction)closeSideBar:(id)sender {
    
    [self.view layoutIfNeeded];
    
    self.constraintSideBarViewLeadingSpace.constant = -278;
    
    [UIView animateWithDuration:0.5 animations:^{
        
        [self.view layoutIfNeeded];
        
        self.viewForSideBarBackground.alpha = 0.0;
        
    } completion:^(BOOL finished) {
        
        self.viewForSideBarBackground.hidden = YES;
        
    }];

}

-(void) checkFromSkip
{
    
        if (myAppDelegate.isFromSkip) {
    
           // [self closeSideBar:self.btnSideBarClose];
    
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"BUY TIMEE" message:@"You need to login first." delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Login", nil];
    
            alert.tag = 1003;
    
            [alert show];
            return;
    
        }
}

- (IBAction)openSideBar:(id)sender {

    

    if ([myAppDelegate.activeViewControllerClassName isEqualToString:[NSString stringWithFormat:@"%@",[ViewAllTaskViewController class]]]) {
        // [myAppDelegate.viewAllTaskVCObj.txtActiveField resignFirstResponder];
        [myAppDelegate.viewAllTaskVCObj performSelector:@selector(checkSelectedBtnsForHideFilterFromMenuPress) withObject:nil afterDelay:0];
    }
    //    if ([myAppDelegate.activeViewControllerClassName isEqualToString:[NSString stringWithFormat:@"%@",[CreateBioViewController class]]]) {
    //        [myAppDelegate.createBioVCObj.txtActiveField resignFirstResponder];
    //    }
    if ([myAppDelegate.activeViewControllerClassName isEqualToString:[NSString stringWithFormat:@"%@",[TaskCreateViewController class]]]) {
        [myAppDelegate.taskCreateVCObj.txtActiveField resignFirstResponder];
        [myAppDelegate.taskCreateVCObj.txtViewActiveField resignFirstResponder];
        
    }
    if ([myAppDelegate.activeViewControllerClassName isEqualToString:[NSString stringWithFormat:@"%@",[TaskHistoryViewController class]]]) {
        [myAppDelegate.taskHistoryVCObj.txtActiveField resignFirstResponder];
    }
    if ([myAppDelegate.activeViewControllerClassName isEqualToString:[NSString stringWithFormat:@"%@",[RequestReservationViewController class]]]) {
        [myAppDelegate.requestReservationVcObj.txtActiveField resignFirstResponder];
         [myAppDelegate.requestReservationVcObj.txtViewActiveField resignFirstResponder];
        [myAppDelegate.requestReservationVcObj doneTyping];
    }
//    if ([self.lblTitleText.text isEqualToString:@"Bio"]) {
//        
//        if (!myAppDelegate.isFromEdit) {
//            
//            return;
//
//        }
//        
//    }
    
    
    
    
    [self.view layoutIfNeeded];
    
    self.constraintSideBarViewLeadingSpace.constant = 0;
    
    self.viewForSideBarBackground.hidden = NO;
    
    self.viewForSideBarBackground.alpha = 0.0;
    
    [UIView animateWithDuration:0.5 animations:^{
        
        [self.view layoutIfNeeded];
        
        self.viewForSideBarBackground.alpha = 0.9;
        
    } completion:^(BOOL finished) {
        
        
    }];

    
}

- (IBAction)backFromPushNotificationView:(id)sender {
    
    if (myAppDelegate.isComingFromPushNotification) {
        
        if (self.btnBackFromMapView.hidden && !self.mapViewToShowUserLocationOnMap.hidden) {
            
            [self.view layoutIfNeeded];
            
            self.constraintMapViewLeadingSpace.constant = SCREEN_WIDTH;
            self.constraintMapViewTrailingSpace.constant = -SCREEN_WIDTH;
            
            self.mapViewToShowUserLocationOnMap.alpha = 1.0;
            
            [UIView animateWithDuration:0.5 animations:^{
                
                [self.view layoutIfNeeded];
                
                self.mapViewToShowUserLocationOnMap.alpha = 0.0;
                
            } completion:^(BOOL finished) {
                
                self.mapViewToShowUserLocationOnMap.hidden = YES;
                
            }];
            
            return;
        }
        
        CATransition *transitionAnimation = [CATransition animation];
        [transitionAnimation setType:kCATransitionPush];
        [transitionAnimation setSubtype:kCATransitionFromLeft];
        [transitionAnimation setDuration:0.3f];
        [transitionAnimation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
        [transitionAnimation setFillMode:kCAFillModeBoth];
        
       // self.lblTitleText.text = myAppDelegate.lastOpenTitleText;
        
        [self.HolderViewWithHeaderForPushNotification.layer addAnimation:transitionAnimation forKey:@"changeView"];
        
        for(UIView *view in self.HolderViewWithHeaderForPushNotification.subviews){
            
            [view removeFromSuperview];
            
        }
        
        [self.HolderViewWithHeaderForPushNotification setHidden:YES];
        
        [myAppDelegate.viewAllTaskVCObj performSelector:@selector(performUpdateWork) withObject:nil afterDelay:0.2];
        
        self.btnBackForPushNotificationView.hidden = YES;
        
        if (!myAppDelegate.isSideBarButtonHiddenOrShown) {
         
            self.btnSideBarOpen.hidden = myAppDelegate.isSideBarButtonHiddenOrShown;
            self.btnBack.hidden = !myAppDelegate.isSideBarButtonHiddenOrShown;
            self.btnBackFromMapView.hidden = !myAppDelegate.isSideBarButtonHiddenOrShown;
            
        }
        
        if (myAppDelegate.isPaymentORBankInfo) {
            
            [self.viewHolderWithoutHeader setHidden:NO];
            
        }else if (!myAppDelegate.isMapViewLocationShowing) {
            
            [self.view layoutIfNeeded];
            
            self.constraintMapViewLeadingSpace.constant = 0;
            self.constraintMapViewTrailingSpace.constant = 0;
            
            self.mapViewToShowUserLocationOnMap.alpha = 0.0;
            
            self.mapViewToShowUserLocationOnMap.hidden = NO;
            self.btnBackFromMapView.hidden = NO;
            
            self.btnSideBarOpen.hidden = YES;
            self.btnBack.hidden = YES;
            
            [UIView animateWithDuration:0.5 animations:^{
                
                [self.view layoutIfNeeded];
                
                self.mapViewToShowUserLocationOnMap.alpha = 1.0;
                
            } completion:^(BOOL finished) {
                
                
            }];
            
            [self.mapViewToShowUserLocationOnMap removeAnnotations:self.mapViewToShowUserLocationOnMap.annotations];
            
            self.lblTitleText.text = @"Location";
            
            // setup the map pin with all data and add to map view
            CLLocationCoordinate2D coordinate = myAppDelegate.lastLocationSaved;
            
            CLLocationCoordinate2D zoomLocation;
            zoomLocation.latitude = coordinate.latitude;
            zoomLocation.longitude= coordinate.longitude;
            
            // 2
            MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(zoomLocation, 20.0000001*METERS_PER_MILE, 20.0000001*METERS_PER_MILE);   //for max zoom
            
            // 3
            [self.mapViewToShowUserLocationOnMap setRegion:viewRegion animated:YES];
            
            MKPointAnnotation *annotation = [[MKPointAnnotation alloc] init];
            annotation.coordinate = coordinate;
            [self.mapViewToShowUserLocationOnMap addAnnotation:annotation];
            
            
        }
        
        myAppDelegate.workerPushTaskDetail = nil;
        myAppDelegate.taskDetailPushVCObj = nil;
        myAppDelegate.taskDetailOtherPushVCObj = nil;
        myAppDelegate.taskHistoryPushOtherUserVCObj = nil;
        myAppDelegate.profilePushOtherUserVCObj = nil;
        myAppDelegate.taskPushData = nil;
        myAppDelegate.userPushOtherData = nil;
        
        myAppDelegate.lastOpenTitleText = @"";
        
        myAppDelegate.isSideBarButtonHiddenOrShown = false;
        myAppDelegate.isMapViewLocationShowing = false;
        myAppDelegate.isOfPushOtherUser = false;
        myAppDelegate.isComingFromPushNotification = false;
        
        return;
        
    }
    
}


- (IBAction)backToMainView:(id)sender {
    
    if([myAppDelegate.TandC_Buyee isEqualToString:@"request"])
    {
        myAppDelegate.TandC_Buyee = @"home";
    }
    NSLog(@"string is %@",myAppDelegate.stringNameOfChildVC);
    NSLog(@"string is %@",myAppDelegate.superViewClassName);
    NSLog(@"string is %@", myAppDelegate.TandC_Buyee);
   
    
    
    if ([myAppDelegate.superViewClassName isEqualToString:@"new_create_bio"])
    {
        CATransition *transitionAnimation = [CATransition animation];
        [transitionAnimation setType:kCATransitionPush];
        [transitionAnimation setSubtype:kCATransitionFromLeft];
        [transitionAnimation setDuration:0.3f];
        [transitionAnimation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
        [transitionAnimation setFillMode:kCAFillModeBoth];
        
        
        [myAppDelegate.createBioVCObj.mainUpperHolderVIew.layer addAnimation:transitionAnimation forKey:@"changeView"];
        
        
        //        for(UIView *view in myAppDelegate.vcObj.viewLoginContainer.subviews){
        //
        //            [view removeFromSuperview];
        //
        //        }
        
        
        for(UIView *view in myAppDelegate.createBioVCObj.mainUpperHolderVIew.subviews){
            
            [view removeFromSuperview];
            
        }
        myAppDelegate.createBioVCObj.mainUpperHolderVIew.hidden = true;
        myAppDelegate.superViewClassName = @"";
        myAppDelegate.vcObj.btnBack.hidden = YES;
        myAppDelegate.vcObj.btnSideBarOpen.hidden = NO;
        return;
    }
    
   // myAppDelegate.superViewClassName = @"ReviewViewController";
    
    
    
     if ([myAppDelegate.superViewClassName isEqualToString:@"ReviewViewController"])
    
     {
        CATransition *transitionAnimation = [CATransition animation];
        [transitionAnimation setType:kCATransitionPush];
        [transitionAnimation setSubtype:kCATransitionFromLeft];
        [transitionAnimation setDuration:0.3f];
        [transitionAnimation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
        [transitionAnimation setFillMode:kCAFillModeBoth];
        
        
        
        
        [myAppDelegate.reviewVcObj.reviewRejectionView.layer addAnimation:transitionAnimation forKey:@"changeView"];
        
        
        //        for(UIView *view in myAppDelegate.vcObj.viewLoginContainer.subviews){
        //
        //            [view removeFromSuperview];
        //
        //        }
        
        
        for(UIView *view in myAppDelegate.reviewVcObj.reviewRejectionView.subviews){
            
            [view removeFromSuperview];
            
        }
        myAppDelegate.reviewVcObj.reviewRejectionView.hidden = true;
        myAppDelegate.superViewClassName = @"";
        
        myAppDelegate.vcObj.btnBack.hidden = NO;
        myAppDelegate.vcObj.btnSideBarOpen.hidden = YES;
       // myAppDelegate.TandC_Buyee=@"myActivity";
        
        
        return;
        
        
    }
    
    if ([myAppDelegate.superViewClassName isEqualToString:@"TaskHistoryViewController"])
        
    {
        CATransition *transitionAnimation = [CATransition animation];
        [transitionAnimation setType:kCATransitionPush];
        [transitionAnimation setSubtype:kCATransitionFromLeft];
        [transitionAnimation setDuration:0.3f];
        [transitionAnimation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
        [transitionAnimation setFillMode:kCAFillModeBoth];
        
        
        
        
        [myAppDelegate.taskHistoryVCObj.reviewDetailView.layer addAnimation:transitionAnimation forKey:@"changeView"];
        
        for(UIView *view in myAppDelegate.taskHistoryVCObj.reviewDetailView.subviews){
            
            [view removeFromSuperview];
            
        }
        myAppDelegate.taskHistoryVCObj.reviewDetailView.hidden = true;
        myAppDelegate.superViewClassName = @"";
        
        myAppDelegate.vcObj.btnBack.hidden = YES;
        myAppDelegate.vcObj.btnSideBarOpen.hidden = NO;
        
        
        return;
        
        
    }

    
    
    if ([myAppDelegate.superViewClassName isEqualToString:@"ReivewDetailViewController"])
        
    {
        CATransition *transitionAnimation = [CATransition animation];
        [transitionAnimation setType:kCATransitionPush];
        [transitionAnimation setSubtype:kCATransitionFromLeft];
        [transitionAnimation setDuration:0.3f];
        [transitionAnimation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
        [transitionAnimation setFillMode:kCAFillModeBoth];
        
        
        
        
        [myAppDelegate.reviewDetailVcObj.shareReviewDetailReviewView.layer addAnimation:transitionAnimation forKey:@"changeView"];
        
        for(UIView *view in myAppDelegate.reviewDetailVcObj.shareReviewDetailReviewView.subviews){
            
            [view removeFromSuperview];
            
        }
        myAppDelegate.reviewDetailVcObj.shareReviewDetailReviewView.hidden = true;
        myAppDelegate.superViewClassName = @"";
        
        myAppDelegate.vcObj.btnBack.hidden = NO;
        myAppDelegate.vcObj.btnSideBarOpen.hidden = YES;
        
        
        return;
        
        
    }

    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    if (myAppDelegate.TandC_Buyee.length > 0) {
        
        
        if ([myAppDelegate.TandC_Buyee isEqualToString:@"home"])
        {
            
            //   self.btnSideBarOpen.hidden = NO;
            
            
            //added by kp
            
            NSLog(@"%@",myAppDelegate.userData.tnc);
            
//            if([myAppDelegate.userData.tnc isEqualToString:@"1"])
//            {
//                CATransition *transitionAnimation = [CATransition animation];
//                [transitionAnimation setType:kCATransitionPush];
//                [transitionAnimation setSubtype:kCATransitionFromLeft];
//                [transitionAnimation setDuration:0.3f];
//                [transitionAnimation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
//                [transitionAnimation setFillMode:kCAFillModeBoth];
//                
//                
//                
//                //  [myAppDelegate.activeTaskVCObj.viewTaskDetailHolder.layer addAnimation:transitionAnimation forKey:@"changeView"];
//                
//                [myAppDelegate.vcObj.viewLoginContainer.layer addAnimation:transitionAnimation forKey:@"changeView"];
//                
//                
//                for(UIView *view in myAppDelegate.vcObj.viewLoginContainer.subviews){
//                    
//                    [view removeFromSuperview];
//                    
//                }
//                
//                
//                for(UIView *view in myAppDelegate.vcObj.viewHolderWithHeader.subviews){
//                    
//                    [view removeFromSuperview];
//                    
//                }
//                
//                [myAppDelegate.vcObj performSegueWithIdentifier:@"login" sender:myAppDelegate.loginVCObj];
//                myAppDelegate.vcObj.btnBack.hidden = YES;
//                myAppDelegate.vcObj.btnSideBarOpen.hidden = YES;
//                // myAppDelegate.vcObj = nil;
//                myAppDelegate.viewAllTaskVCObj = nil;
//                myAppDelegate.stringNameOfChildVC = @"";
//                return;
//
//            }
            
            //******************************
            
            CATransition *transitionAnimation = [CATransition animation];
            [transitionAnimation setType:kCATransitionPush];
            [transitionAnimation setSubtype:kCATransitionFromLeft];
            [transitionAnimation setDuration:0.3f];
            [transitionAnimation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
            [transitionAnimation setFillMode:kCAFillModeBoth];
            
            
            
            //  [myAppDelegate.activeTaskVCObj.viewTaskDetailHolder.layer addAnimation:transitionAnimation forKey:@"changeView"];
            
            [myAppDelegate.vcObj.viewLoginContainer.layer addAnimation:transitionAnimation forKey:@"changeView"];
            
            
            //        for(UIView *view in myAppDelegate.vcObj.viewLoginContainer.subviews){
            //
            //            [view removeFromSuperview];
            //
            //        }
            
            
            for(UIView *view in myAppDelegate.vcObj.viewHolderWithHeader.subviews){
                
                [view removeFromSuperview];
                
            }
            
            [myAppDelegate.vcObj performSegueWithIdentifier:@"viewAllTask" sender:myAppDelegate.loginVCObj];
            myAppDelegate.vcObj.btnBack.hidden = YES;
            myAppDelegate.vcObj.btnSideBarOpen.hidden = NO;
            // myAppDelegate.vcObj = nil;
            myAppDelegate.termsNconditionVCObj = nil;
            myAppDelegate.stringNameOfChildVC = @"";
            // return;
            
            
        }
        else if ([myAppDelegate.TandC_Buyee isEqualToString:@"create"])
        {
            
            CATransition *transitionAnimation = [CATransition animation];
            [transitionAnimation setType:kCATransitionPush];
            [transitionAnimation setSubtype:kCATransitionFromLeft];
            [transitionAnimation setDuration:0.3f];
            [transitionAnimation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
            [transitionAnimation setFillMode:kCAFillModeBoth];
            
            
            
            //  [myAppDelegate.activeTaskVCObj.viewTaskDetailHolder.layer addAnimation:transitionAnimation forKey:@"changeView"];
            
            [myAppDelegate.vcObj.viewLoginContainer.layer addAnimation:transitionAnimation forKey:@"changeView"];
            
            
            //            for(UIView *view in myAppDelegate.vcObj.viewLoginContainer.subviews){
            //
            //                [view removeFromSuperview];
            //
            //            }
            
            
            for(UIView *view in myAppDelegate.vcObj.viewHolderWithHeader.subviews){
                
                [view removeFromSuperview];
                
            }
            
            [myAppDelegate.vcObj performSegueWithIdentifier:@"createTask" sender:myAppDelegate.loginVCObj];
            myAppDelegate.vcObj.btnBack.hidden = YES;
            myAppDelegate.vcObj.btnSideBarOpen.hidden = NO;
            // myAppDelegate.vcObj = nil;
            myAppDelegate.termsNconditionVCObj = nil;
            myAppDelegate.stringNameOfChildVC = @"";
            // return;
            
            
        }
        else if([myAppDelegate.TandC_Buyee isEqualToString:@"mapView"])
        {
            
            NSLog(@"come from map view.... ");
            
            CATransition *transitionAnimation = [CATransition animation];
            [transitionAnimation setType:kCATransitionPush];
            [transitionAnimation setSubtype:kCATransitionFromLeft];
            [transitionAnimation setDuration:0.3f];
            [transitionAnimation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
            [transitionAnimation setFillMode:kCAFillModeBoth];
            
          //  self.lblTitleText.text = @"B U Y  T I M E E";
            
            if (myAppDelegate.viewAllTaskVCObj.isMapView)
            {
                
                [myAppDelegate.viewAllTaskVCObj.viewMapHolder.layer addAnimation:transitionAnimation forKey:@"changeView"];
                
                for(UIView *view in myAppDelegate.viewAllTaskVCObj.viewMapHolder.subviews){
                    
                    [view removeFromSuperview];
                    
                }
                
                [myAppDelegate.viewAllTaskVCObj.viewMapHolder setHidden:YES];
                myAppDelegate.mapVCObj = nil;
                myAppDelegate.isSideBarAccesible = true;
                [myAppDelegate.viewAllTaskVCObj performSelector:@selector(performUpdateWork) withObject:nil afterDelay:0.2];
                myAppDelegate.viewAllTaskVCObj.isMapView = false;
                
                
                myAppDelegate.vcObj.btnBack.hidden = YES;
                myAppDelegate.vcObj.btnMapOnListView.hidden=NO;
                
                
                myAppDelegate.vcObj.btnSideBarOpen.hidden = NO;
                myAppDelegate.termsNconditionVCObj = nil;
                myAppDelegate.stringNameOfChildVC = @"";
                
                myAppDelegate.TandC_Buyee = @"home";
                
                
                
                
            }
            
        }

        else if ([myAppDelegate.TandC_Buyee isEqualToString:@"myActivity"])
        {
            
            CATransition *transitionAnimation = [CATransition animation];
            [transitionAnimation setType:kCATransitionPush];
            [transitionAnimation setSubtype:kCATransitionFromLeft];
            [transitionAnimation setDuration:0.3f];
            [transitionAnimation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
            [transitionAnimation setFillMode:kCAFillModeBoth];
            
            
            
            //  [myAppDelegate.activeTaskVCObj.viewTaskDetailHolder.layer addAnimation:transitionAnimation forKey:@"changeView"];
            
            [myAppDelegate.vcObj.viewLoginContainer.layer addAnimation:transitionAnimation forKey:@"changeView"];
            
            
            //            for(UIView *view in myAppDelegate.vcObj.viewLoginContainer.subviews){
            //
            //                [view removeFromSuperview];
            //
            //            }
            
            
            for(UIView *view in myAppDelegate.vcObj.viewHolderWithHeader.subviews){
                
                [view removeFromSuperview];
                
            }
            
            [myAppDelegate.vcObj performSegueWithIdentifier:@"taskhistory" sender:myAppDelegate.loginVCObj];
            myAppDelegate.vcObj.btnBack.hidden = YES;
            myAppDelegate.vcObj.btnSideBarOpen.hidden =NO;
            // myAppDelegate.vcObj = nil;
            myAppDelegate.termsNconditionVCObj = nil;
            myAppDelegate.stringNameOfChildVC = @"";
            // return;
            
            
        }

        
       // myAppDelegate.TandC_Buyee = @"";
        return;
    }
    NSLog(@"back clicked supview  --- %@ and main class = %@",myAppDelegate.stringNameOfChildVC,myAppDelegate.superViewClassName);
    
    if ([myAppDelegate.stringNameOfChildVC isEqualToString:@"login_BTimee"])
    {
        
        
        CATransition *transitionAnimation = [CATransition animation];
        [transitionAnimation setType:kCATransitionPush];
        [transitionAnimation setSubtype:kCATransitionFromLeft];
        [transitionAnimation setDuration:0.3f];
        [transitionAnimation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
        [transitionAnimation setFillMode:kCAFillModeBoth];
        
        
        
        //  [myAppDelegate.activeTaskVCObj.viewTaskDetailHolder.layer addAnimation:transitionAnimation forKey:@"changeView"];
        
        [myAppDelegate.vcObj.viewLoginContainer.layer addAnimation:transitionAnimation forKey:@"changeView"];
        
        
        for(UIView *view in myAppDelegate.vcObj.viewLoginContainer.subviews){
            
            [view removeFromSuperview];
            
        }
        
        
        for(UIView *view in myAppDelegate.vcObj.viewHolderWithHeader.subviews){
            
            [view removeFromSuperview];
            
        }
        
        [myAppDelegate.vcObj performSegueWithIdentifier:@"login" sender:myAppDelegate.loginVCObj];
        myAppDelegate.vcObj.btnBack.hidden = YES;
        myAppDelegate.vcObj.btnSideBarOpen.hidden = YES;
        // myAppDelegate.vcObj = nil;
        myAppDelegate.viewAllTaskVCObj = nil;
        myAppDelegate.stringNameOfChildVC = @"";
        return;
        
        
    }
    
    
    if ([myAppDelegate.stringNameOfChildVC isEqualToString:@"T&C_BTimee"])
    {
        
        
        CATransition *transitionAnimation = [CATransition animation];
        [transitionAnimation setType:kCATransitionPush];
        [transitionAnimation setSubtype:kCATransitionFromLeft];
        [transitionAnimation setDuration:0.3f];
        [transitionAnimation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
        [transitionAnimation setFillMode:kCAFillModeBoth];
        
        
        
        //  [myAppDelegate.activeTaskVCObj.viewTaskDetailHolder.layer addAnimation:transitionAnimation forKey:@"changeView"];
        
        [myAppDelegate.vcObj.viewLoginContainer.layer addAnimation:transitionAnimation forKey:@"changeView"];
        
        
        for(UIView *view in myAppDelegate.vcObj.viewLoginContainer.subviews){
            
            [view removeFromSuperview];
            
        }
        
        
        for(UIView *view in myAppDelegate.vcObj.viewHolderWithHeader.subviews){
            
            [view removeFromSuperview];
            
        }
        
        [myAppDelegate.vcObj performSegueWithIdentifier:@"login" sender:myAppDelegate.loginVCObj];
        myAppDelegate.vcObj.btnBack.hidden = YES;
        myAppDelegate.vcObj.btnSideBarOpen.hidden = YES;
        // myAppDelegate.vcObj = nil;
        myAppDelegate.termsNconditionVCObj = nil;
        myAppDelegate.stringNameOfChildVC = @"";
        

        
        
        return;
        
        
    }
    
    
    
    
    if (myAppDelegate.isFromEdit && [myAppDelegate.superViewClassName isEqualToString:@"ViewController"]) {
        
////        self.btnBack.hidden = YES;
////        self.btnSideBarOpen.hidden = NO;
////        
////        myAppDelegate.isComingBackToSettings = true;
////        
////        myAppDelegate.isSideBarAccesible = true;
////        
////        TaskTableViewCell *cell = (TaskTableViewCell *)[self.tblViewSideBar cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
////        [self performSegueWithIdentifier:@"settings" sender:cell];
////        
////        myAppDelegate.superViewClassName = @"";
////        
////        myAppDelegate.isFromEdit = false;
//        
//        
//        
//        
//        myAppDelegate.isSideBarAccesible = true;
//        myAppDelegate.vcObj.btnSideBarOpen.hidden = NO;
//        myAppDelegate.vcObj.btnBack.hidden = YES;
//        TaskTableViewCell *cell = (TaskTableViewCell *)[myAppDelegate.vcObj.tblViewSideBar cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
//        [myAppDelegate.vcObj performSegueWithIdentifier:@"viewAllTask" sender:cell];
//        

        
        
        
        
        
        CATransition *transitionAnimation = [CATransition animation];
        [transitionAnimation setType:kCATransitionPush];
        [transitionAnimation setSubtype:kCATransitionFromLeft];
        [transitionAnimation setDuration:0.3f];
        [transitionAnimation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
        [transitionAnimation setFillMode:kCAFillModeBoth];
        
      //  self.lblTitleText.text = @"B U Y  T I M E E";
        
        if (myAppDelegate.viewAllTaskVCObj.isFromMapView) {
            
            if ([myAppDelegate.superViewClassName isEqualToString:@""]) {
                
                [myAppDelegate.viewAllTaskVCObj.viewMapHolder.layer addAnimation:transitionAnimation forKey:@"changeView"];
                
                for(UIView *view in myAppDelegate.viewAllTaskVCObj.viewMapHolder.subviews){
                    
                    [view removeFromSuperview];
                    
                }
                [myAppDelegate.viewAllTaskVCObj.viewMapHolder setHidden:YES];
                
                myAppDelegate.mapVCObj = nil;
                
                myAppDelegate.viewAllTaskVCObj.isMapView = false;
                
                myAppDelegate.viewAllTaskVCObj.isFromMapView = NO;
                
                [myAppDelegate.viewAllTaskVCObj performSelector:@selector(performUpdateWork) withObject:nil afterDelay:0.2];
                
                myAppDelegate.isSideBarAccesible = true;
                
                myAppDelegate.TandC_Buyee=@"home";
                myAppDelegate.vcObj.btnMapOnListView.hidden = false;//vs
                myAppDelegate.vcObj.BtnTransparentMap.hidden = false;//vs
                
            }else{
                
               // self.lblTitleText.text = @"B U Y  T I M E E";
                
                [myAppDelegate.viewAllTaskVCObj.viewTaskDetailHolder.layer addAnimation:transitionAnimation forKey:@"changeView"];
                
                for(UIView *view in myAppDelegate.viewAllTaskVCObj.viewTaskDetailHolder.subviews){
                    
                    [view removeFromSuperview];
                    
                }
                [myAppDelegate.viewAllTaskVCObj.viewTaskDetailHolder setHidden:YES];
                
                [myAppDelegate.viewAllTaskVCObj performSelector:@selector(performUpdateWork) withObject:nil afterDelay:0.2];
                
                [myAppDelegate.mapVCObj addAllPins];
                
                myAppDelegate.isSideBarAccesible = true;
                
                myAppDelegate.taskDetailVCObj = nil;
                
            }
            
            
        }else{
            
          //  self.lblTitleText.text = @"B U Y  T I M E E";
            
            if (myAppDelegate.viewAllTaskVCObj.isMapView) {
                
                [myAppDelegate.viewAllTaskVCObj.viewMapHolder.layer addAnimation:transitionAnimation forKey:@"changeView"];
                
                for(UIView *view in myAppDelegate.viewAllTaskVCObj.viewMapHolder.subviews){
                    
                    [view removeFromSuperview];
                    
                }
                [myAppDelegate.viewAllTaskVCObj.viewMapHolder setHidden:YES];
                
                myAppDelegate.mapVCObj = nil;
                
                myAppDelegate.isSideBarAccesible = true;
                
                [myAppDelegate.viewAllTaskVCObj performSelector:@selector(performUpdateWork) withObject:nil afterDelay:0.2];
                
                myAppDelegate.viewAllTaskVCObj.isMapView = false;
                
            }else{
                
                [myAppDelegate.viewAllTaskVCObj.viewTaskDetailHolder.layer addAnimation:transitionAnimation forKey:@"changeView"];
                
                for(UIView *view in myAppDelegate.viewAllTaskVCObj.viewTaskDetailHolder.subviews){
                    
                    [view removeFromSuperview];
                    
                }
                [myAppDelegate.viewAllTaskVCObj.viewTaskDetailHolder setHidden:YES];
                
                [myAppDelegate.viewAllTaskVCObj performSelector:@selector(performUpdateWork) withObject:nil afterDelay:0.2];
                
                myAppDelegate.isSideBarAccesible = true;
                
                myAppDelegate.taskDetailVCObj = nil;
                
                myAppDelegate.TandC_Buyee=@"home";
                
                myAppDelegate.vcObj.btnMapOnListView.hidden = false;//vs
                myAppDelegate.vcObj.BtnTransparentMap.hidden = false;//vs
                
            }
            
        }
        
        
        return;
    }
    
    if (![myAppDelegate.stringNameOfChildVC isEqualToString:@""]) {
        
        
        //    if ([myAppDelegate.stringNameOfChildVC isEqualToString:[NSString stringWithFormat:@"%@",[myAppDelegate.termsNconditionVCObj class]]]) {
        //
        //        CATransition *transitionAnimation = [CATransition animation];
        //        [transitionAnimation setType:kCATransitionPush];
        //        [transitionAnimation setSubtype:kCATransitionFromLeft];
        //        [transitionAnimation setDuration:0.5f];
        //        [transitionAnimation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
        //        [transitionAnimation setFillMode:kCAFillModeBoth];
        //
        //
        //
        //      //  [myAppDelegate.activeTaskVCObj.viewTaskDetailHolder.layer addAnimation:transitionAnimation forKey:@"changeView"];
        //
        //         [myAppDelegate.vcObj.viewLoginContainer.layer addAnimation:transitionAnimation forKey:@"changeView"];
        //
        //
        //        for(UIView *view in myAppDelegate.vcObj.viewLoginContainer.subviews){
        //
        //            [view removeFromSuperview];
        //
        //        }
        //
        //
        //        for(UIView *view in myAppDelegate.vcObj.viewHolderWithHeader.subviews){
        //
        //            [view removeFromSuperview];
        //
        //        }
        //
        //        [myAppDelegate.vcObj performSegueWithIdentifier:@"login" sender:myAppDelegate.loginVCObj];
        //        myAppDelegate.vcObj.btnBack.hidden = YES;
        //        myAppDelegate.vcObj.btnSideBarOpen.hidden = YES;
        //       // myAppDelegate.vcObj = nil;
        //        myAppDelegate.termsNconditionVCObj = nil;
        //
        //
        //        }
        
        if ([myAppDelegate.stringNameOfChildVC isEqualToString:[NSString stringWithFormat:@"%@",[myAppDelegate.activeTaskVCObj class]]]) {
            
            CATransition *transitionAnimation = [CATransition animation];
            [transitionAnimation setType:kCATransitionPush];
            [transitionAnimation setSubtype:kCATransitionFromLeft];
            [transitionAnimation setDuration:0.3f];
            [transitionAnimation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
            [transitionAnimation setFillMode:kCAFillModeBoth];
            
           // self.lblTitleText.text = @"Active Task";
            
            [myAppDelegate.activeTaskVCObj.viewTaskDetailHolder.layer addAnimation:transitionAnimation forKey:@"changeView"];
            
            for(UIView *view in myAppDelegate.activeTaskVCObj.viewTaskDetailHolder.subviews){
                
                [view removeFromSuperview];
                
            }
            [myAppDelegate.activeTaskVCObj.viewTaskDetailHolder setHidden:YES];
            
            [myAppDelegate.activeTaskVCObj reloadTable];
            
            myAppDelegate.isSideBarAccesible = true;
            
            myAppDelegate.taskDetailVCObj = nil;
            
        }
        if ([myAppDelegate.stringNameOfChildVC isEqualToString:[NSString stringWithFormat:@"%@",[myAppDelegate.taskDetailVCObj class]]]) {
            
            CATransition *transitionAnimation = [CATransition animation];
            [transitionAnimation setType:kCATransitionPush];
            [transitionAnimation setSubtype:kCATransitionFromLeft];
            [transitionAnimation setDuration:0.3f];
            [transitionAnimation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
            [transitionAnimation setFillMode:kCAFillModeBoth];
            
            if (myAppDelegate.isOfOtherUser) {
                
                self.lblTitleText.text = @"Task Detail";
                
                [myAppDelegate.taskDetailVCObj.viewOtherUserProfileHolder.layer addAnimation:transitionAnimation forKey:@"changeView"];
                
                for(UIView *view in myAppDelegate.taskDetailVCObj.viewOtherUserProfileHolder.subviews){
                    
                    [view removeFromSuperview];
                    
                }
                [myAppDelegate.taskDetailVCObj.viewOtherUserProfileHolder setHidden:YES];
                
                myAppDelegate.isOfOtherUser = false;
                myAppDelegate.isOfOtherUserTaskHistory = false;
                myAppDelegate.isOfOtherUserTaskDetail = false;
                
                myAppDelegate.stringNameOfChildVC = [NSString stringWithFormat:@"%@",[self class]];
                
                myAppDelegate.taskDetailOtherUserVCObj = nil;
                
                myAppDelegate.taskHistoryOtherUserVCObj = nil;
                
                myAppDelegate.profileOtherUserVCObj = nil;
                
                myAppDelegate.isFirstOtherUser = false;
                
                return;
                
            }else{
                
              //  self.lblTitleText.text = @"B U Y  T I M E E";
                
                [myAppDelegate.taskDetailVCObj.viewRatingViewControllerHolder.layer addAnimation:transitionAnimation forKey:@"changeView"];
                
                for(UIView *view in myAppDelegate.taskDetailVCObj.viewRatingViewControllerHolder.subviews){
                    
                    [view removeFromSuperview];
                    
                }
                [myAppDelegate.taskDetailVCObj.viewRatingViewControllerHolder setHidden:YES];
                
                myAppDelegate.ratingVCObj = nil;
                
            }
            
        }
        else if ([myAppDelegate.stringNameOfChildVC isEqualToString:[NSString stringWithFormat:@"%@",[myAppDelegate.taskHistoryVCObj class]]]) {
            
            CATransition *transitionAnimation = [CATransition animation];
            [transitionAnimation setType:kCATransitionPush];
            [transitionAnimation setSubtype:kCATransitionFromLeft];
            [transitionAnimation setDuration:0.3f];
            [transitionAnimation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
            [transitionAnimation setFillMode:kCAFillModeBoth];
            
            self.lblTitleText.text = @"Task History";
            
            [myAppDelegate.taskHistoryVCObj.viewTaskDetailHolder.layer addAnimation:transitionAnimation forKey:@"changeView"];
            
            for(UIView *view in myAppDelegate.taskHistoryVCObj.viewTaskDetailHolder.subviews){
                
                [view removeFromSuperview];
                
            }
            [myAppDelegate.taskHistoryVCObj.viewTaskDetailHolder setHidden:YES];
            
            [myAppDelegate.taskHistoryVCObj reloadTable];
            
            myAppDelegate.isSideBarAccesible = true;
            
            myAppDelegate.taskDetailVCObj = nil;
            
        }
        else if ([myAppDelegate.stringNameOfChildVC isEqualToString:[NSString stringWithFormat:@"%@",[myAppDelegate.viewAllTaskVCObj class]]]) {
            
         //comment by KP
            
//            myAppDelegate.viewAllTaskVCObj.btnRadius.hidden=NO;
//            myAppDelegate.viewAllTaskVCObj.btnTime.hidden=NO;
//            myAppDelegate.viewAllTaskVCObj.btnPrice.hidden=NO;
//
            
            
            
            
            CATransition *transitionAnimation = [CATransition animation];
            [transitionAnimation setType:kCATransitionPush];
            [transitionAnimation setSubtype:kCATransitionFromLeft];
            [transitionAnimation setDuration:0.3f];
            [transitionAnimation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
            [transitionAnimation setFillMode:kCAFillModeBoth];
            
         //   self.lblTitleText.text = @"B U Y  T I M E E";
            
            if (myAppDelegate.viewAllTaskVCObj.isFromMapView) {
                
                if ([myAppDelegate.superViewClassName isEqualToString:@""]) {
                    
                    [myAppDelegate.viewAllTaskVCObj.viewMapHolder.layer addAnimation:transitionAnimation forKey:@"changeView"];
                    
                    for(UIView *view in myAppDelegate.viewAllTaskVCObj.viewMapHolder.subviews){
                        
                        [view removeFromSuperview];
                        
                    }
                    [myAppDelegate.viewAllTaskVCObj.viewMapHolder setHidden:YES];
                    
                    myAppDelegate.mapVCObj = nil;
                    
                    myAppDelegate.viewAllTaskVCObj.isMapView = false;
                    
                    myAppDelegate.viewAllTaskVCObj.isFromMapView = NO;
                    
                    [myAppDelegate.viewAllTaskVCObj performSelector:@selector(performUpdateWork) withObject:nil afterDelay:0.2];
                    
                    myAppDelegate.isSideBarAccesible = true;
                    
                    myAppDelegate.TandC_Buyee=@"home";
                    myAppDelegate.vcObj.btnMapOnListView.hidden = false;//vs
                    myAppDelegate.vcObj.BtnTransparentMap.hidden = false;//vs
                    
                }else{
                    
              //      self.lblTitleText.text = @"B U Y  T I M E E";
                    
                    [myAppDelegate.viewAllTaskVCObj.viewTaskDetailHolder.layer addAnimation:transitionAnimation forKey:@"changeView"];
                    
                    for(UIView *view in myAppDelegate.viewAllTaskVCObj.viewTaskDetailHolder.subviews){
                        
                        [view removeFromSuperview];
                        
                    }
                    [myAppDelegate.viewAllTaskVCObj.viewTaskDetailHolder setHidden:YES];
                    
                    [myAppDelegate.viewAllTaskVCObj performSelector:@selector(performUpdateWork) withObject:nil afterDelay:0.2];
                    
                    [myAppDelegate.mapVCObj addAllPins];
                    
                    myAppDelegate.isSideBarAccesible = true;
                    
                    myAppDelegate.taskDetailVCObj = nil;
                    
                }
                
                
            }else{
                
            //    self.lblTitleText.text = @"B U Y  T I M E E";
                
                if (myAppDelegate.viewAllTaskVCObj.isMapView) {
                    
                    [myAppDelegate.viewAllTaskVCObj.viewMapHolder.layer addAnimation:transitionAnimation forKey:@"changeView"];
                    
                    for(UIView *view in myAppDelegate.viewAllTaskVCObj.viewMapHolder.subviews){
                        
                        [view removeFromSuperview];
                        
                    }
                    [myAppDelegate.viewAllTaskVCObj.viewMapHolder setHidden:YES];
                    
                    myAppDelegate.mapVCObj = nil;
                    
                    myAppDelegate.isSideBarAccesible = true;
                    
                    [myAppDelegate.viewAllTaskVCObj performSelector:@selector(performUpdateWork) withObject:nil afterDelay:0.2];
                    
                    myAppDelegate.viewAllTaskVCObj.isMapView = false;
                    
                }else{
                    
                    [myAppDelegate.viewAllTaskVCObj.viewTaskDetailHolder.layer addAnimation:transitionAnimation forKey:@"changeView"];
                    
                    for(UIView *view in myAppDelegate.viewAllTaskVCObj.viewTaskDetailHolder.subviews){
                        
                        [view removeFromSuperview];
                        
                    }
                    [myAppDelegate.viewAllTaskVCObj.viewTaskDetailHolder setHidden:YES];
                    
                    [myAppDelegate.viewAllTaskVCObj performSelector:@selector(performUpdateWork) withObject:nil afterDelay:0.2];
                    
                    myAppDelegate.isSideBarAccesible = true;
                    
                    myAppDelegate.taskDetailVCObj = nil;
                    
                    myAppDelegate.TandC_Buyee=@"home";
                    
                    myAppDelegate.vcObj.btnMapOnListView.hidden = false;//vs
                    myAppDelegate.vcObj.BtnTransparentMap.hidden = false;//vs
                    
                }
                
            }
            
        }else if ([myAppDelegate.stringNameOfChildVC isEqualToString:[NSString stringWithFormat:@"%@",[self class]]]){
            
            if (myAppDelegate.isFromProfile) {
                
                if ([myAppDelegate.superViewClassName isEqualToString:[NSString stringWithFormat:@"%@",[ProfileViewController class]]]) {
                    
                    CATransition *transitionAnimation = [CATransition animation];
                    [transitionAnimation setType:kCATransitionPush];
                    [transitionAnimation setSubtype:kCATransitionFromLeft];
                    [transitionAnimation setDuration:0.3f];
                    [transitionAnimation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
                    [transitionAnimation setFillMode:kCAFillModeBoth];
                    
                    self.lblTitleText.text = @"Task History";
                    
                    [myAppDelegate.taskHistoryVCObj.viewTaskDetailHolder.layer addAnimation:transitionAnimation forKey:@"changeView"];
                    
                    for(UIView *view in myAppDelegate.taskHistoryVCObj.viewTaskDetailHolder.subviews){
                        
                        [view removeFromSuperview];
                        
                    }
                    [myAppDelegate.taskHistoryVCObj.viewTaskDetailHolder setHidden:YES];
                    
                    myAppDelegate.taskDetailVCObj = nil;
                    
                    [myAppDelegate.taskHistoryVCObj reloadTable];
                    
                    myAppDelegate.superViewClassName = @"";
                    
                    return;
                    
                }
                
                self.btnBack.hidden = YES;
                self.btnSideBarOpen.hidden = NO;
                
                myAppDelegate.isSideBarAccesible = false;
                
                [self performSegueWithIdentifier:@"profile" sender:self.btnProfileSegue];
                
                myAppDelegate.taskHistoryVCObj = nil;
                
                myAppDelegate.isFromProfile = false;
                
                myAppDelegate.stringNameOfChildVC = @"";
                
                return;
                
            }else{
                
                CATransition *transitionAnimation = [CATransition animation];
                [transitionAnimation setType:kCATransitionPush];
                [transitionAnimation setSubtype:kCATransitionFromLeft];
                [transitionAnimation setDuration:0.3f];
                [transitionAnimation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
                [transitionAnimation setFillMode:kCAFillModeBoth];
                
                if ([myAppDelegate.superViewClassName isEqualToString:[NSString stringWithFormat:@"%@",[myAppDelegate.activeTaskVCObj class]]]) {
                    
                    self.lblTitleText.text = @"Active Task";
                    
                    [myAppDelegate.activeTaskVCObj.viewTaskDetailHolder.layer addAnimation:transitionAnimation forKey:@"changeView"];
                    
                    for(UIView *view in myAppDelegate.activeTaskVCObj.viewTaskDetailHolder.subviews){
                        
                        [view removeFromSuperview];
                        
                    }
                    [myAppDelegate.activeTaskVCObj.viewTaskDetailHolder setHidden:YES];
                    
                    [myAppDelegate.activeTaskVCObj reloadTable];
                    
                    myAppDelegate.taskDetailVCObj = nil;
                    
                }
                else if ([myAppDelegate.superViewClassName isEqualToString:[NSString stringWithFormat:@"%@",[myAppDelegate.taskHistoryVCObj class]]]) {
                    
                    self.lblTitleText.text = @"Task History";
                    
                    [myAppDelegate.taskHistoryVCObj.viewTaskDetailHolder.layer addAnimation:transitionAnimation forKey:@"changeView"];
                    
                    for(UIView *view in myAppDelegate.taskHistoryVCObj.viewTaskDetailHolder.subviews){
                        
                        [view removeFromSuperview];
                        
                    }
                    [myAppDelegate.taskHistoryVCObj.viewTaskDetailHolder setHidden:YES];
                    
                    [myAppDelegate.taskHistoryVCObj reloadTable];
                    
                    myAppDelegate.taskDetailVCObj = nil;
                    
                }
                else if ([myAppDelegate.superViewClassName isEqualToString:[NSString stringWithFormat:@"%@",[myAppDelegate.viewAllTaskVCObj class]]]) {
                    
                    self.lblTitleText.text = @"Tasks";
                    
                    if (myAppDelegate.viewAllTaskVCObj.isFromMapView) {
                        
                        if ([myAppDelegate.superViewClassName isEqualToString:@""]) {
                            
                            [myAppDelegate.viewAllTaskVCObj.viewMapHolder.layer addAnimation:transitionAnimation forKey:@"changeView"];
                            
                            for(UIView *view in myAppDelegate.viewAllTaskVCObj.viewMapHolder.subviews){
                                
                                [view removeFromSuperview];
                                
                            }
                            [myAppDelegate.viewAllTaskVCObj.viewMapHolder setHidden:YES];
                            
                            myAppDelegate.mapVCObj = nil;
                            
                            myAppDelegate.viewAllTaskVCObj.isMapView = false;
                            
                            [myAppDelegate.viewAllTaskVCObj performSelector:@selector(performUpdateWork) withObject:nil afterDelay:0.2];
                            
                            myAppDelegate.viewAllTaskVCObj.isFromMapView = NO;
                            
                            myAppDelegate.isSideBarAccesible = true;
                            
                        }else{
                            
                            self.lblTitleText.text = @"Map";
                            
                            [myAppDelegate.viewAllTaskVCObj.viewTaskDetailHolder.layer addAnimation:transitionAnimation forKey:@"changeView"];
                            
                            for(UIView *view in myAppDelegate.viewAllTaskVCObj.viewTaskDetailHolder.subviews){
                                
                                [view removeFromSuperview];
                                
                            }
                            [myAppDelegate.viewAllTaskVCObj.viewTaskDetailHolder setHidden:YES];
                            
                            [myAppDelegate.viewAllTaskVCObj performSelector:@selector(performUpdateWork) withObject:nil afterDelay:0.2];
                            
                            [myAppDelegate.mapVCObj addAllPins];
                            
                            myAppDelegate.taskDetailVCObj = nil;
                            
                            myAppDelegate.isSideBarAccesible = true;
                            
                        }
                        
                    }else{
                        
                        self.lblTitleText.text = @"Tasks";
                        
                        if (myAppDelegate.viewAllTaskVCObj.isMapView) {
                            
                            [myAppDelegate.viewAllTaskVCObj.viewMapHolder.layer addAnimation:transitionAnimation forKey:@"changeView"];
                            
                            for(UIView *view in myAppDelegate.viewAllTaskVCObj.viewMapHolder.subviews){
                                
                                [view removeFromSuperview];
                                
                            }
                            [myAppDelegate.viewAllTaskVCObj.viewMapHolder setHidden:YES];
                            
                            myAppDelegate.mapVCObj = nil;
                            
                            myAppDelegate.viewAllTaskVCObj.isMapView = false;
                            
                            [myAppDelegate.viewAllTaskVCObj performSelector:@selector(performUpdateWork) withObject:nil afterDelay:0.2];
                            
                            myAppDelegate.isSideBarAccesible = true;
                            
                        }else{
                            
                            [myAppDelegate.viewAllTaskVCObj.viewTaskDetailHolder.layer addAnimation:transitionAnimation forKey:@"changeView"];
                            
                            for(UIView *view in myAppDelegate.viewAllTaskVCObj.viewTaskDetailHolder.subviews){
                                
                                [view removeFromSuperview];
                                
                            }
                            
                            [myAppDelegate.viewAllTaskVCObj.viewTaskDetailHolder setHidden:YES];
                            
                            [myAppDelegate.viewAllTaskVCObj performSelector:@selector(performUpdateWork) withObject:nil afterDelay:0.2];
                            
                            myAppDelegate.isSideBarAccesible = true;
                            
                            myAppDelegate.taskDetailVCObj = nil;
                            
                        }
                        
                    }
                    
                }else{
                    
                }
                
                myAppDelegate.stringNameOfChildVC = myAppDelegate.superViewClassName;
                
            }
            
            
        }
        
        if ([myAppDelegate.stringNameOfChildVC isEqualToString:myAppDelegate.superViewClassName]) {
            
            if (myAppDelegate.viewAllTaskVCObj.isFromMapView) {
                
                myAppDelegate.superViewClassName = @"";
                
            }else{
                
                self.btnBack.hidden = YES;
                self.btnSideBarOpen.hidden = NO;
                
                myAppDelegate.stringNameOfChildVC = @"";
                
            }
            
        }else{
            
            if ([myAppDelegate.superViewClassName isEqualToString:@""]) {
                
                self.btnBack.hidden = YES;
                self.btnSideBarOpen.hidden = NO;
                
                myAppDelegate.stringNameOfChildVC = @"";
                
            }else{
                
                if (myAppDelegate.isFromProfile) {
                    myAppDelegate.stringNameOfChildVC = [NSString stringWithFormat:@"%@",[self class]];
                }else{
                    myAppDelegate.stringNameOfChildVC = myAppDelegate.superViewClassName;
                }
                
            }
            
        }
        
    }
    
}

-(void)sendMailToEmail:(NSString *)email{
   
    myAppDelegate.isComingBackAfterMapOpen = true;
    
//    // Email Subject
//    NSString *emailTitle = @"Test Email";
//    // Email Content
//    NSString *messageBody = @"iOS programming is so fun!";
    // To address
    NSArray *toRecipents = [NSArray arrayWithObject:email];
    
    MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
    mc.mailComposeDelegate = self;
//    [mc setSubject:emailTitle];
//    [mc setMessageBody:messageBody isHTML:NO];
    [mc setToRecipients:toRecipents];
    
    // Present mail view controller on screen
    [self presentViewController:mc animated:YES completion:NULL];
    
}

- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error{
    switch (result){
        case MFMailComposeResultCancelled:
            break;
        case MFMailComposeResultSaved:
            break;
        case MFMailComposeResultSent:
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
}



//-(MyAnnotation *)addPinWithTitle:(NSString *)title address:(NSString *)address subtitle:(NSString *)subtitle AndCoordinate:(NSString *)strCoordinate taskId:(int)taskId userImage:(UIImage *)userImage{
//    
//    // clear out any white space
//    strCoordinate = [strCoordinate stringByReplacingOccurrencesOfString:@" " withString:@""];
//    
//    // convert string into actual latitude and longitude values
//    NSArray *components = [strCoordinate componentsSeparatedByString:@","];
//    
//    double latitude = [components[0] doubleValue];
//    double longitude = [components[1] doubleValue];
//    
//    // setup the map pin with all data and add to map view
//    CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(latitude, longitude);
//    
//    CLLocationCoordinate2D zoomLocation;
//    zoomLocation.latitude = coordinate.latitude; // 39.281516;
//    zoomLocation.longitude= coordinate.longitude; // -76.580806;
//    // 2
//    //    MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(zoomLocation, 0.0000001*METERS_PER_MILE, 0.0000001*METERS_PER_MILE);   //for max zoom
//    
//    MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(zoomLocation, 20.0000001*METERS_PER_MILE, 20.0000001*METERS_PER_MILE);   //for min zoom
//    
//    // 3
//    [self.mapView setRegion:viewRegion animated:YES];
//    
//    MyAnnotation *annotation = [[MyAnnotation alloc] initWithName:title address:address subtitle:subtitle userImage:userImage coordinate:coordinate andTag:taskId];
//    
//    return annotation;
//    
//}
//


//
//
//- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view{
//    
//    if([view isKindOfClass:[DXAnnotationView class]]){
//        
//        [((DXAnnotationView *)view)showCalloutView];
//        view.layer.zPosition=0;
//        
//        if (!prevDXAnnotationView) {
//            prevDXAnnotationView = (DXAnnotationView *)view;
//        }else{
//            [prevDXAnnotationView hideCalloutView];
//            prevDXAnnotationView.layer.zPosition=-1;
//            
//            prevDXAnnotationView = (DXAnnotationView *)view;
//        }
//        
//    }
//    
//}
//
//
//
//-(void)mapView:(MKMapView *)mapView didDeselectAnnotationView:(MKAnnotationView *)view{
//    
//    if([view isKindOfClass:[DXAnnotationView class]]){
//        [((DXAnnotationView *)view)hideCalloutView];
//        view.layer.zPosition=-1;
//        
//        prevDXAnnotationView = nil;
//    }
//    
//}


-(void)showRegionOnMapForCoordinates:(NSString *)strCoordinate{
    
    [self.view layoutIfNeeded];
    
    self.constraintMapViewLeadingSpace.constant = 0;
    self.constraintMapViewTrailingSpace.constant = 0;
    
    self.mapViewToShowUserLocationOnMap.alpha = 0.0;
    
    self.mapViewToShowUserLocationOnMap.hidden = NO;

    if(!myAppDelegate.isComingFromPushNotification){
        
        self.btnBackFromMapView.hidden = NO;
        
        self.btnSideBarOpen.hidden = YES;
        self.btnBack.hidden = YES;

    }

    [UIView animateWithDuration:0.5 animations:^{
        
        [self.view layoutIfNeeded];
        
        self.mapViewToShowUserLocationOnMap.alpha = 1.0;
        
    } completion:^(BOOL finished) {
        
        
    }];
    
    [self.mapViewToShowUserLocationOnMap removeAnnotations:self.mapViewToShowUserLocationOnMap.annotations];
    
    self.lblTitleText.text = @"Location";
    
    strCoordinate = [strCoordinate stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    // convert string into actual latitude and longitude values
    NSArray *components = [strCoordinate componentsSeparatedByString:@","];
    
    double latitude = [components[0] doubleValue];
    double longitude = [components[1] doubleValue];
    
    // setup the map pin with all data and add to map view
    CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(latitude, longitude);
    
    CLLocationCoordinate2D zoomLocation;
    zoomLocation.latitude = coordinate.latitude;
    zoomLocation.longitude= coordinate.longitude;
    
    // 2
    MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(zoomLocation, 20.0000001*METERS_PER_MILE, 20.0000001*METERS_PER_MILE);   //for max zoom
    
    // 3
    [self.mapViewToShowUserLocationOnMap setRegion:viewRegion animated:YES];
    
    MKPointAnnotation *annotation = [[MKPointAnnotation alloc] init];
    annotation.coordinate = coordinate;
    [self.mapViewToShowUserLocationOnMap addAnnotation:annotation];
    
}


- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation {
    
    MKAnnotationView *annotationView = nil;
    if(annotation != mapView.userLocation){
        
        static NSString *defaultPinID = @"annotation";
        annotationView = (MKAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:defaultPinID];
        if ( annotationView == nil )
            annotationView = [[MKAnnotationView alloc]
                              initWithAnnotation:annotation reuseIdentifier:defaultPinID];
        
        annotationView.image = [UIImage imageNamed:@"location"];    //as suggested by Squatch
        
    }
    else{
        
        
    }
    return annotationView;
    
}


- (IBAction)backFromMap:(id)sender {

    [self.view layoutIfNeeded];
    
    self.constraintMapViewLeadingSpace.constant = SCREEN_WIDTH;
    self.constraintMapViewTrailingSpace.constant = -SCREEN_WIDTH;
    
    self.mapViewToShowUserLocationOnMap.alpha = 1.0;
    
    [UIView animateWithDuration:0.5 animations:^{
        
        [self.view layoutIfNeeded];
        
        self.mapViewToShowUserLocationOnMap.alpha = 0.0;
        
    } completion:^(BOOL finished) {
        
        self.mapViewToShowUserLocationOnMap.hidden = YES;
        
        self.btnBackFromMapView.hidden = YES;
        
        if ([myAppDelegate.mapViewFromClass isEqualToString:@"Settings"]) {
            
            self.btnSideBarOpen.hidden = NO;
            self.btnBack.hidden = YES;

            self.lblTitleText.text = @"Settings";

        }else if ([myAppDelegate.mapViewFromClass isEqualToString:@"TaskDetail"]){
            
            self.btnSideBarOpen.hidden = YES;
            self.btnBack.hidden = NO;

            self.lblTitleText.text = @"Task";

        }else{
            
            self.btnSideBarOpen.hidden = YES;
            self.btnBack.hidden = NO;

            self.lblTitleText.text = @"Profile";

        }
        

    }];
    
}




@end
