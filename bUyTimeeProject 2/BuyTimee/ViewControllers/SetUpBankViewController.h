//
//  SetUpBankViewController.h
//  BuyTimee
//
//  Created by User38 on 08/02/17.
//  Copyright © 2017 User14. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Flurry.h"
#import "BusinessTypeTableViewCell.h"


@interface SetUpBankViewController : UIViewController<UITextFieldDelegate,UIAlertViewDelegate,UIScrollViewDelegate,UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, retain) UITextField *txtActiveField;
@property (nonatomic, retain) UIView *inputAccView;
@property (nonatomic, retain) UIButton *btnDone;
@property (nonatomic, retain) UIButton *btnNext;
@property (nonatomic, retain) UIButton *btnPrev;


@property (weak, nonatomic) IBOutlet UILabel *lblYoutBankDetail;
@property (weak, nonatomic) IBOutlet UIButton *btnSave;
- (IBAction)saveInfoClicked:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lblSaving;

@property (weak, nonatomic) IBOutlet UILabel *lblChecking;

@property (weak, nonatomic) IBOutlet UIButton *radioSavingClicked;
- (IBAction)radioSavingClicked:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *radioCheckingClicked;
- (IBAction)radioCheckingClicked:(id)sender;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consTextNameTop;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consTextAccountTop;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consTextRoutingTop;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consTextBankNameTop;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consTextReEnterTop;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consLblYourBankTop;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consBtnRadioSaveBottom;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consLblSavingBottom;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consBtnRadioCheckingBottom;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consLblCheckingBottom;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consLblChekingTrailing;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consBtnRadioSavingLeading;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consBtnRadioSavingHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consBtnRadioSavingWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consBtnRadioCheckingWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consmainLblViewTop;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consBtnRadioCheckingHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consBtnSaveInfoHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consAllTextFldViewTop;
@property (weak, nonatomic) IBOutlet UIView *LblViewBG;
@property (weak, nonatomic) IBOutlet UIView *mainViewTextsFlds;
@property (weak, nonatomic) IBOutlet UIView *TextFieldHolderVIew;

@property (weak, nonatomic) IBOutlet UITextField *textFldName;

@property (weak, nonatomic) IBOutlet UITextField *textFldAccNo;
@property (weak, nonatomic) IBOutlet UITextField *textFldReenterAccNo;
@property (weak, nonatomic) IBOutlet UITextField *textFldRouting;
@property (weak, nonatomic) IBOutlet UITextField *textFldBankName;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consTextNameHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consTextAccountHeight;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consTextFldsTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consTxlFldsLineLabelTop;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consSavingBtnBottom;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consSavingBtnLeft;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consLblDetailLeft;


- (IBAction)savingBtnClciked:(id)sender;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consChecknigBtnBottm;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consCheckingBtnLeft;
- (IBAction)checkingBtnClciked:(id)sender;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consSaveBankInfoBtnBottom;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *mainViewTxtFldTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topViewHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *txtFldNmaeLeft;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *txtFldAcntNoLeft;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *txtFldReAcntLeft;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *txtFldRoutingLeft;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *txtFldBnakNameLeft;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *txtFldAccntNoTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *txtFldReAcntTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *txtFldRoutingTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *txtFldBnakNmaeTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lineTwoTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lineThreeTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lineFourTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lineFiveTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *radioCheckingBtnLeft;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *radioCheckLeft;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lblYourBankDetailWidth;

@property (weak, nonatomic) IBOutlet UIButton *btnHelp;

- (IBAction)helpBtnClicked:(id)sender;

@property (weak, nonatomic) IBOutlet UIView *blackTransparentView;
@property (weak, nonatomic) IBOutlet UIButton *btnOK;
-(IBAction)okBtnClicked:(id)sender;

// new outlets by vs.... 20 june 17..
@property (weak, nonatomic) IBOutlet UIScrollView *mainScrollView;
@property (weak, nonatomic) IBOutlet UIView *mainContentView;
@property (weak, nonatomic) IBOutlet UIView *textFldHolderView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consContentViewHeight;

@property (weak, nonatomic) IBOutlet UIButton *btnBusinessType;
- (IBAction)businessTypeClicked:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *ssnTextFld;
@property (weak, nonatomic) IBOutlet UITextField *taxIdTxtFld;
@property (weak, nonatomic) IBOutlet UIButton *btnDOB;

- (IBAction)DOBClicked:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lblLegalDoc;
@property (weak, nonatomic) IBOutlet UIImageView *documentImgView;
@property (weak, nonatomic) IBOutlet UIButton *btnCross;
- (IBAction)crossBtnClicked:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *btnChoose;
- (IBAction)chooseBtnClicked:(id)sender;


@property (weak, nonatomic) IBOutlet UITableView *businessTypeTableView;




//constraints outlets


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consBtnHelpTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consBtnHelpLeft;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consHelpAlertViewLeft;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consHelpAlertViewTop;


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *nsconsTbleHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *nsconsImageViewHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *nsconsImageViewWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *nsconsBtnChooseLeading;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *nsconsBtnChooseTrailing;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *nsconsImgViewLeading;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *nsconsBtnCrossHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *nsconsBtnCrossWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *nsconsBtnCrossLeading;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *nsconsBtnCrossTop;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *nsconsBtnRadioTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *nsconsBtnTransSavingTop;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *nsconsLblSavingTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *nsconsLblSavingLeading;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *nsconsBtnRadioFirstLeadingspace;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *nsconsBtnRadioFirstTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *nsconsLblCheckingLeading;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *nsconsLblCheckingTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *nsconsBtnTransFirstLeading;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *nsconsBtnTransFirstTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *nsconsBtnDOBWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *nsconsBtnDOBHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *nsconsLblLegalDocWidth;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *nsconsLblLegalDocHeight;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *nsconsLblLegalDocLeading;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *nsconsDOBUnderLineWidth;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *nsconsLegalUnderLineWidth;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *nsconsLegalUnderLineLeading;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *nsconsRadioBtnFirstLeading;

@property (weak, nonatomic) IBOutlet UIButton *btnTransOverID;
- (IBAction)TransBtnOverIdClicked:(id)sender;



// fields top constraints outlets...

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consTxtFldNameTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consLblUnderNameFldTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consTxtFldAccNoTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consLblUnderAccFldTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consTxtFldReenterTop;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consLblUnderReEntrAccFldTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consTxtFldRoutingTop;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consLblUnderRoutingFldTop;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consTxtFldBankNameTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consLblUnderBankNameFldTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consBtnBusinessTypeTop;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consLblUnderBusinessBtnTop;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consTxtFldSSNTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consLblUnderSSNFldTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consTxtFldTaxIdTop;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consLblUnderTaxIDTop;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consBtnDOBTop;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consLblUnderDOBBtnTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consLblLegalIdTop;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consLblUnderLegalIdTop;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consBtnChooseTop;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consBtnChooseTrailing;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consBtnChooseHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consBtnChooseWidth;


// new outlets for add paypal view work...05June18...

@property (weak, nonatomic) IBOutlet UIView *paypalOverView;

@property (weak, nonatomic) IBOutlet UILabel *lblPaypalDescription;

@property (weak, nonatomic) IBOutlet UITextField *businessIdTxtFld;

// paypal constraints outlets 12 June 18...


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consPayPalBusinessTxtFldTop;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consPayPalBusinessTxtFldTrailing;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consPayPalBusinessTxtFldHeight;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consPayPalBusinessTxtFldLeading;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consLblPayPalPleaseVerifyTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consLblPayPalPleaseVerifyHeight;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consLblPayPalPleaseVerifyTrailing;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consLblPayPalPleaseVerifyLeading;




@end
