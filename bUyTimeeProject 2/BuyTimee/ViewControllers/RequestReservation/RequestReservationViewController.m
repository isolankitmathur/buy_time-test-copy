//
//  RequestReservationViewController.m
//  BuyTimee
//
//  Created by User38 on 21/08/18.
//  Copyright © 2018 User14. All rights reserved.
//

#import "RequestReservationViewController.h"
#import "AppDelegate.h"
#import "Constants.h"
#import "GlobalFunction.h"
#import "DatabaseClass.h"
#import "ChooseCatRequestAppointmentTableViewCell.h"
#import "category.h"
#import "GooglePlacesData.h"
#import "TaskTableViewCell.h"


#import <GooglePlaces/GooglePlaces.h>


@import GooglePlaces;
@import GooglePlacePicker;
@import GoogleMaps;

@interface RequestReservationViewController ()<UITextFieldDelegate, UITextFieldDelegate,UITextViewDelegate, UITableViewDelegate, UITableViewDataSource, CLLocationManagerDelegate, GMSAutocompleteTableDataSourceDelegate, GMSAutocompleteFetcherDelegate, UIAlertViewDelegate>


@end

@implementation RequestReservationViewController
{
    UIDatePicker *datePicker;
    NSDate *startDate;
    NSDate *startTime;
    NSString *startTimeToSend;

    
    NSMutableArray *catArr;
    NSMutableArray *subCatArr;
    
    NSMutableArray *placesArray;
    NSMutableArray *finalPlacesArray;
    
    BOOL show;
    BOOL show1;
    
    BOOL click1;
    BOOL click2;
    
    BOOL isOpened;
    
    category *catObj;
    
    NSString *catIdToSend;
    
    // google place api work...
    
    GMSPlacesClient *_placesClient;
    CLLocationManager *locationManager;
    GMSPlacePicker *_placePicker;
    
    GMSAutocompleteTableDataSource *_tableDataSource;
    UISearchController *_searchBar;
    
    UISearchDisplayController *_searchDisplayController;
    
    GMSAutocompleteFetcher *_fetcher;
    
    BOOL checkBtnClicked;
    
    
    
}
@synthesize btnDone,btnNext,btnPrev,inputAccView,txtActiveField,txtViewActiveField;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
    
    myAppDelegate.vcObj.btnMapOnListView.hidden = true;//vs
    myAppDelegate.vcObj.BtnTransparentMap.hidden = true;//vs
    
    
    CGFloat width = [UIScreen mainScreen].bounds.size.width;
    
    int x = (int)width;
    self.consviewUnderScrollViewWidth.constant = x;
    self.consviewUnderScrollViewHeight.constant = 900;
    
//    [self.mainScrollView setContentOffset:CGPointMake(0,
//                                                      -100)
//                                 animated:YES];
    // Do any additional setup after loading the view.
    
    self.descriptionTextView.text = @"Any Special Request";
    self.descriptionTextView.textColor = [UIColor colorWithRed:196.0/255.0f green:196.0/255.0f blue:196.0/255.0f alpha:1.0];
     [self.descriptionTextView setFont:[UIFont fontWithName:@"Lato-Regular" size:17]];
    
    myAppDelegate.TandC_Buyee=@"request";
    
    self.businessNameTextFld.delegate = self;
    self.LocationTextFld.delegate = self;
    self.titleTextFld.delegate = self;
    self.dateTextField.delegate = self;
    self.descriptionTextView.delegate = self;
    self.categorytitleTextFld.delegate = self;
    self.priceTextField.delegate = self;
    self.nameTextField.delegate = self;
    self.mobileTextField.delegate = self;
    
    self.priceTextField.keyboardType = UIKeyboardTypeDecimalPad;
    self.mobileTextField.keyboardType = UIKeyboardTypeNumberPad;
    
    
    datePicker = [[UIDatePicker alloc]init];
    [datePicker setDate:[NSDate date]];
    [datePicker addTarget:self action:@selector(updateTextField:) forControlEvents:UIControlEventValueChanged];
    [self.dateTextField setInputView:datePicker];
    datePicker.minimumDate = [NSDate date];
    datePicker.timeZone = [NSTimeZone systemTimeZone];
     [datePicker setDatePickerMode:UIDatePickerModeDateAndTime];
    datePicker.hidden = YES;
    
   // self.descriptionTextView.frame = CGRectMake(5,-20,self.descriptionTextView.frame.size.width,self.descriptionTextView.frame.size.height);
    
    self.tblView.delegate = self;
    self.tblView.dataSource = self;
    
    _tblView.hidden = YES;
    
    catArr = [DatabaseClass getDataFromCategoryData:[[NSString stringWithFormat:@"Select * from %@",tableCategory] UTF8String]];
    
    subCatArr = [DatabaseClass getDataFromSubCategoryData:[[NSString stringWithFormat:@"Select * from %@",tableSubCategory] UTF8String]];
    
    
    
    
    catObj = [[category alloc] init];
    catObj.name = @"Others";
    catObj.categoryId = @"110";
    catObj.categoryImg = @"BuyTimee";
    
    [catArr addObject:catObj];
    
    [GlobalFunction createBorderforView:self.tblView withWidth:1.0 cornerRadius:0.0 colorForBorder:RGB(151, 151, 151)];
    
    self.categorytitleTextFld.enabled = false;
   
   
    
//    //// google place api work...
//
//
//    locationManager = [[CLLocationManager alloc] init];
//    locationManager.delegate = self;
//    // NSLog(@"Location Services Enabled...%@",kCLAuthorizationStatusDenied);
//
//
//    CLAuthorizationStatus status = [CLLocationManager authorizationStatus];
//
//    if (status == kCLAuthorizationStatusDenied || status == kCLAuthorizationStatusRestricted)
//    {
//        NSLog(@"Location Services Enabled");
//
//
//        UIAlertView  *alert = [[UIAlertView alloc] initWithTitle:@"App Permission Denied"
//                                                         message:@"To re-enable, please go to Settings and turn on Location Service for this app."
//                                                        delegate:nil
//                                               cancelButtonTitle:@"OK"
//                                               otherButtonTitles:nil];
//        [alert show];
//
//
//
//
//
//
//    }
//    else if (status == kCLAuthorizationStatusNotDetermined)
//    {
//        [locationManager requestWhenInUseAuthorization];
//    }
//
//
//    [locationManager startUpdatingLocation];
//
//
//
    _placesClient = [GMSPlacesClient sharedClient];
    
//
//
//    _searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, self.businessNameTextFld.frame.size.width, 30)];
//
//    _tableDataSource = [[GMSAutocompleteTableDataSource alloc] init];
//    _tableDataSource.delegate = self;
//
//    _searchDisplayController = [[UISearchDisplayController alloc] initWithSearchBar:_searchBar
//                                                                 contentsController:self];
//    _searchDisplayController.searchResultsDataSource = _tableDataSource;
//    _searchDisplayController.searchResultsDelegate = _tableDataSource;
//    _searchDisplayController.delegate = self;
//
//   // [self.businessNameTextFld addSubview:_searchBar];
//
//
//    //// search bar modify work...
//
//    ///// for adit table view...
//
//    // Define some colors.
//    UIColor *darkGray = [UIColor darkGrayColor];
//    UIColor *lightGray = [UIColor lightGrayColor];
//
//    // Navigation bar background.
//    [[UINavigationBar appearance] setBarTintColor:[UIColor whiteColor]];
//    [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
//
//    // Color of typed text in the search bar.
//    NSDictionary *searchBarTextAttributes = @{
//                                              NSForegroundColorAttributeName: lightGray,
//                                              NSFontAttributeName : [UIFont systemFontOfSize:[UIFont systemFontSize]],
//                                              //  NSBackgroundColorAttributeName: [UIColor redColor],
//
//
//                                              };
//    [UITextField appearanceWhenContainedInInstancesOfClasses:@[[UISearchBar class]]]
//    .defaultTextAttributes = searchBarTextAttributes;
//
//    //   [UITextField appearanceWhenContainedInInstancesOfClasses:@[[UISearchBar class]]]
//    //   .backgroundColor = [UIColor clearColor];
//
//    // Color of the placeholder text in the search bar prior to text entry.
//    NSDictionary *placeholderAttributes = @{
//                                            NSForegroundColorAttributeName: lightGray,
//                                            NSFontAttributeName : [UIFont systemFontOfSize:[UIFont systemFontSize]]
//                                            };
//
//    // Color of the default search text.
//    // NOTE: In a production scenario, "Search" would be a localized string.
//    NSAttributedString *attributedPlaceholder =
//    [[NSAttributedString alloc] initWithString:@"Search"
//                                    attributes:placeholderAttributes];
//    [UITextField appearanceWhenContainedInInstancesOfClasses:@[[UISearchBar class]]]
//    .attributedPlaceholder = attributedPlaceholder;
//
//    // Color of the in-progress spinner.
//    [[UIActivityIndicatorView appearance] setColor:lightGray];
//
//    // To style the two image icons in the search bar (the magnifying glass
//    // icon and the 'clear text' icon), replace them with different images.
//    [[UISearchBar appearance] setImage:[UIImage imageNamed:@"custom_clear_x_high"]
//                      forSearchBarIcon:UISearchBarIconClear
//                                 state:UIControlStateHighlighted];
//    [[UISearchBar appearance] setImage:[UIImage imageNamed:@"custom_clear_x"]
//                      forSearchBarIcon:UISearchBarIconClear
//                                 state:UIControlStateNormal];
//    [[UISearchBar appearance] setImage:[UIImage imageNamed:@"custom_search"]
//                      forSearchBarIcon:UISearchBarIconSearch
//                                 state:UIControlStateNormal];
//    [[UISearchBar appearance] setSearchBarStyle:UISearchBarStyleMinimal];
//
//    [[UISearchBar appearance] setTintColor:[UIColor yellowColor]];
//
//
//
//    // Color of selected table cells.
//    UIView *selectedBackgroundView = [[UIView alloc] init];
//    selectedBackgroundView.backgroundColor = [UIColor lightGrayColor];
//    [UITableViewCell appearanceWhenContainedIn:[GMSAutocompleteViewController class], nil]
//    .selectedBackgroundView = selectedBackgroundView;
    
    
    // fetcher google
    
    // Set bounds to inner-west Sydney Australia.
    CLLocationCoordinate2D neBoundsCorner = CLLocationCoordinate2DMake(-33.843366, 151.134002);
    CLLocationCoordinate2D swBoundsCorner = CLLocationCoordinate2DMake(-33.875725, 151.200349);
    GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] initWithCoordinate:neBoundsCorner
                                                                       coordinate:swBoundsCorner];
    
    // Set up the autocomplete filter.
    GMSAutocompleteFilter *filter = [[GMSAutocompleteFilter alloc] init];
    filter.type = kGMSPlacesAutocompleteTypeFilterEstablishment;
    
    // Create the fetcher.
    //_fetcher = [[GMSAutocompleteFetcher alloc] initWithBounds:bounds
      //                                                 filter:filter];
    
   // _fetcher = [[GMSAutocompleteFetcher alloc] init];
    _fetcher = [[GMSAutocompleteFetcher alloc] initWithBounds:nil filter:filter];
//    if (@available(iOS 11.0, *)) {
//        _fetcher.accessibilityContainerType = kGMSPlacesAutocompleteTypeFilterEstablishment;
//    } else {
//        // Fallback on earlier versions
//    }
    _fetcher.delegate = self;
    
    [self.businessNameTextFld addTarget:self
                   action:@selector(textFieldDidChange:)
         forControlEvents:UIControlEventEditingChanged];

    
  //  [self placeAutocomplete];
    
    UIColor *color = [UIColor colorWithRed:63.0/255.0f green:67.0/255.0f blue:70.0/255.0f alpha:1.0];
    self.categorytitleTextFld.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Choose Category*" attributes:@{NSForegroundColorAttributeName: color}];
    
    
    self.blackTransparentView.hidden = true;
    self.blackTransparentView.alpha = 0.0;
 
    startTimeToSend = @"";
    

//    [self setAsterictOnTextFld:self.businessNameTextFld];
//    [self setAsterictOnTextFld:self.LocationTextFld];
//    [self setAsterictOnTextFld:self.titleTextFld];
//    [self setAsterictOnTextFld:self.dateTextField];
//    [self setAsterictOnTextFld:self.priceTextField];
//    [self setAsterictOnTextFld:self.categorytitleTextFld];
    
    
    
    
//    UILabel *envelopeView = [[UILabel alloc] initWithFrame:CGRectMake(self.descriptionTextView.frame.size.width -10, 0, 12, 12)];
//    // cgre
//    envelopeView.text = @"*";
//    envelopeView.textColor = [UIColor redColor];
//    //envelopeView.contentMode = UIViewContentModeScaleAspectFill;
//    UIView *test=  [[UIView alloc]initWithFrame:CGRectMake(0, 0 , 18, 18)];
//    [test addSubview:envelopeView];
//   // test.backgroundColor = [UIColor greenColor];
//    //self.descriptionTextView.backgroundColor = [UIColor grayColor];
////    [txt.rightView setFrame:envelopeView.frame];
////    txt.rightView =test;
////    txt.rightViewMode = UITextFieldViewModeAlways;
//    [self.descriptionTextView addSubview:test];
//   // self.priceTextField.backgroundColor = [UIColor grayColor];
//

    NSAttributedString *star = [[NSAttributedString alloc] initWithString:@"*" attributes:@{ NSForegroundColorAttributeName :[UIColor redColor],
                                                                                             }];

    NSMutableAttributedString *title = [[NSMutableAttributedString alloc] initWithString:@"Title" attributes:@{ NSForegroundColorAttributeName :[UIColor colorWithRed:143.0/255.0f green:143.0/255.0f blue:143.0/255.0f alpha:1.0],
                                                                                                     }];
  
    // [title appendAttributedString:star];
    
    NSMutableAttributedString *businessName = [[NSMutableAttributedString alloc] initWithString:@"Business Name" attributes:@{ NSForegroundColorAttributeName :[UIColor colorWithRed:143.0/255.0f green:143.0/255.0f blue:143.0/255.0f alpha:1.0],
                                                                                                                }];
    NSMutableAttributedString *Location = [[NSMutableAttributedString alloc] initWithString:@"Street, Unit, City, State, Zip" attributes:@{ NSForegroundColorAttributeName :[UIColor colorWithRed:143.0/255.0f green:143.0/255.0f blue:143.0/255.0f alpha:1.0],
                                                                                                                               }];
    
    NSMutableAttributedString *date = [[NSMutableAttributedString alloc] initWithString:@"Date/Time" attributes:@{ NSForegroundColorAttributeName :[UIColor colorWithRed:143.0/255.0f green:143.0/255.0f blue:143.0/255.0f alpha:1.0],
                                                                                                                               }];
    NSMutableAttributedString *price = [[NSMutableAttributedString alloc] initWithString:@"Price" attributes:@{ NSForegroundColorAttributeName :[UIColor colorWithRed:143.0/255.0f green:143.0/255.0f blue:143.0/255.0f alpha:1.0],
                                                                                                                               }];
   // [title appendAttributedString:star];
    [businessName appendAttributedString:star];
    [Location appendAttributedString:star];
    [date appendAttributedString:star];
    [price appendAttributedString:star];
    self.titleTextFld.attributedPlaceholder = title;
    self.businessNameTextFld.attributedPlaceholder = businessName;
    self.LocationTextFld.attributedPlaceholder = Location;
    self.priceTextField.attributedPlaceholder = price;
    self.dateTextField.attributedPlaceholder = date;
    
   // NSLog(@"str is...%@",str2);
}


-(void) setAsterictOnTextFld :(UITextField *)txt
{
   // UIImageView *envelopeView = [[UIImageView alloc] initWithFrame:CGRectMake(0, -8, 8, 8)];
    UILabel *envelopeView = [[UILabel alloc] initWithFrame:CGRectMake(0, -8, 15, 15)];
    // cgre
    envelopeView.text = @"*";
    envelopeView.textColor = [UIColor redColor];
    //envelopeView.contentMode = UIViewContentModeScaleAspectFill;
    UIView *test=  [[UIView alloc]initWithFrame:CGRectMake(0, 0 , 15, 15)];
    //test.backgroundColor = [UIColor greenColor];
    [test addSubview:envelopeView];
    [txt.rightView setFrame:envelopeView.frame];
    //[txt.rightView setFrame:CGRectMake(0, -20, 18, 18)];
    txt.rightView =test;
    txt.rightViewMode = UITextFieldViewModeAlways;
    
//    UIImageView *envelopeView = [[UIImageView alloc] initWithFrame:CGRectMake(0, -8, 8, 8)];
//
//    envelopeView.image = [UIImage imageNamed:@"asterisk.png"];
//    envelopeView.contentMode = UIViewContentModeScaleAspectFit;
//    UIView *test=  [[UIView alloc]initWithFrame:CGRectMake(0, 0, 10, 10)];
//    [test addSubview:envelopeView];
//    [txt.rightView setFrame:envelopeView.frame];
//    txt.rightView =test;
//    txt.rightViewMode = UITextFieldViewModeAlways;
}

#pragma mark - extraa fetcher work...


- (void)textFieldDidChange:(UITextField *)textField {
    NSLog(@"%@", textField.text);
   
    
    txtActiveField = textField;
    [self.view layoutIfNeeded];
    //self.consTableViewTop.constant = 56;
    [self.view layoutIfNeeded];
     [_fetcher sourceTextHasChanged:textField.text];

}

#pragma mark - GMSAutocompleteFetcherDelegate
- (void)didAutocompleteWithPredictions:(NSArray *)predictions {
    NSMutableString *resultsStr = [NSMutableString string];
    placesArray = [[NSMutableArray alloc] init];
    finalPlacesArray = [[NSMutableArray alloc] init];
    
    
    for (GMSAutocompletePrediction *prediction in predictions) {
       // [placesArray addObject:prediction];
        
       GooglePlacesData *googlePlaceObj = [[GooglePlacesData alloc] init];

        [resultsStr appendFormat:@"%@\n", [prediction.attributedPrimaryText string]];
        googlePlaceObj.PlaceFullString = [prediction.attributedFullText string];
        googlePlaceObj.placeName = [prediction.attributedPrimaryText string];
        googlePlaceObj.placeAddress = [prediction.attributedSecondaryText string];
        
        [placesArray addObject:googlePlaceObj];
        
    }
    [self openTable];
    //self.businessNameTextFld.text = resultsStr;
}

- (void)didFailAutocompleteWithError:(NSError *)error {
    self.businessNameTextFld.text = [NSString stringWithFormat:@"%@", error.localizedDescription];
}

- (void)placeAutocomplete {
    
    //GMSVisibleRegion visibleRegion = self.mapView.projection.visibleRegion;
 //   GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] initWithCoordinate:visibleRegion.farLeft
                                                                       //coordinate:visibleRegion.nearRight];
    GMSAutocompleteFilter *filter = [[GMSAutocompleteFilter alloc] init];
    filter.type = kGMSPlacesAutocompleteTypeFilterEstablishment;
    
    [_placesClient autocompleteQuery:@"jaipur raj mandir india"
                              bounds:nil
                              filter:filter
                            callback:^(NSArray *results, NSError *error) {
                                if (error != nil) {
                                    NSLog(@"Autocomplete error %@", [error localizedDescription]);
                                    return;
                                }
                                
                                for (GMSAutocompletePrediction* result in results) {
                                    NSLog(@"Result '%@' with placeID %@", result.attributedFullText.string, result.placeID);
                                    self.businessNameTextFld.text = result.attributedFullText.string;
                                }
                            }];
}

#pragma mark - google place api work
//
//- (BOOL)searchDisplayController:(UISearchDisplayController *)controller
//shouldReloadTableForSearchString:(NSString *)searchString
//{
//    [_tableDataSource sourceTextHasChanged:searchString];
//    return NO;
//}
//
//// Handle the user's selection.
//- (void)tableDataSource:(GMSAutocompleteTableDataSource *)tableDataSource
//didAutocompleteWithPlace:(GMSPlace *)place {
//    [_searchDisplayController setActive:NO animated:YES];
//    // Do something with the selected place.
//    NSLog(@"Place name %@", place.name);
//    NSLog(@"Place address %@", place.formattedAddress);
//    NSLog(@"Place attributions %@", place.attributions.string);
//
////    self.nameLabel.text = place.name;
////    self.addressLabel.text = place.formattedAddress;
//}
//
//- (void)tableDataSource:(GMSAutocompleteTableDataSource *)tableDataSource
//didFailAutocompleteWithError:(NSError *)error {
//    [_searchDisplayController setActive:NO animated:YES];
//    // TODO: handle the error.
//    NSLog(@"Error: %@", [error description]);
//}
//
//- (void)didUpdateAutocompletePredictionsForTableDataSource:
//(GMSAutocompleteTableDataSource *)tableDataSource {
//    // Turn the network activity indicator off.
//    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
//    // Reload table data.
//    [_searchDisplayController.searchResultsTableView reloadData];
//}
//
//- (void)didRequestAutocompletePredictionsForTableDataSource:
//(GMSAutocompleteTableDataSource *)tableDataSource {
//    // Turn the network activity indicator on.
//    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
//    // Reload table data.
//    [_searchDisplayController.searchResultsTableView reloadData];
//}
//
//

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}









/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark - Fetch date from picker work

-(void)updateTextField:(id)sender
{
  //  UIDatePicker *picker = (UIDatePicker*)self.myTextField.inputView;
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:dateFormatDate];
    
    NSDateFormatter *TimeFormat1 = [[NSDateFormatter alloc] init];
    [TimeFormat1 setDateFormat:dateFormatOnlyHour];
    
    NSString *TimeString = [GlobalFunction convertTimeFormat:[TimeFormat1 stringFromDate:datePicker.date]];
    
    startTimeToSend = TimeString;
    
    NSString *dateString = [GlobalFunction convertDateFormat:[dateFormat stringFromDate:datePicker.date]];
    NSString *str = [@[dateString, TimeString] componentsJoinedByString:@", "];
    startDate = datePicker.date;
    self.dateTextField.text = str;//[NSString stringWithFormat:@"%@",datePicker.date];
}

#pragma mark - Table View Work

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (txtActiveField == self.businessNameTextFld)
    {
        return placesArray.count;
    }
    else
    {
        return catArr.count;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 40;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    ChooseCatRequestAppointmentTableViewCell *cell = (ChooseCatRequestAppointmentTableViewCell *)[self.tblView dequeueReusableCellWithIdentifier:@"Cell"];
    
    
    if (txtActiveField == self.businessNameTextFld)
    {
    
        NSMutableString *resultsStr = [NSMutableString string];

       // [resultsStr appendFormat:@"%@\n", [placesArray.attributedPrimaryText string]];
   //   cell.lblCategory.text = [googlePlaceObj.PlaceFullString objectat]
        
        GooglePlacesData *myObj = [placesArray objectAtIndex:indexPath.row];
        
        cell.lblCategory.text = myObj.PlaceFullString;
    }
   else
   {
    NSDictionary *dataq = [catArr objectAtIndex:indexPath.row];
    cell.lblCategory.text = [ dataq valueForKey:@"name"];
   }
    
    
    
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
 
    [self closeTable];
    
    NSString *nameToShow = @"";
    
    if (txtActiveField == self.businessNameTextFld)
    {
        
        GooglePlacesData *placeObj = [placesArray objectAtIndex:indexPath.row];
        
        nameToShow = placeObj.placeName;
        
        self.businessNameTextFld.text = nameToShow;
        
        self.LocationTextFld.text = placeObj.placeAddress;
    }
    else
    {
        catObj = [catArr objectAtIndex:indexPath.row];
        
        nameToShow = catObj.name;
        
        if ([catObj.name isEqualToString:@"Others"])
        {
            catIdToSend = @"0";
        }
        else
        {
            catIdToSend = catObj.categoryId;
        }
        
        self.categorytitleTextFld.text = nameToShow;
    }

    
    
}
- (IBAction)chooseCategoryClicked:(id)sender
{
    [self.view layoutIfNeeded];
    self.consTableViewTop.constant = 7;
    [self.view layoutIfNeeded];
    //textField.tintColor = [UIColor clearColor];
    
    txtActiveField = self.categorytitleTextFld;
    
    if (isOpened == true)
    {
        [self closeTable];

    }
    else if (isOpened == false)
    {
        [self openTable];
    }
    
    datePicker.hidden = NO;
    
    
}
-(void)openTable{
    
  //  [self doneTyping];
    
    [self.tblView layoutIfNeeded];
    //[self.tblView reloadData];
    [self.tblView layoutIfNeeded];
    
    //   [txtActiveField resignFirstResponder];
    
    _tblView.hidden = NO;
    [self.view layoutIfNeeded];
    
    if (txtActiveField == self.businessNameTextFld)
    {
       
        [self.tblView reloadData];
        self.consTableViewHeight.constant = 40*placesArray.count;
        
    }
    else
    {
        [self.tblView reloadData];
          self.consTableViewHeight.constant = 40*catArr.count;
    }

    
  
    
    [UIView animateWithDuration:0.5f
                          delay:0.0f
                        options:UIViewAnimationOptionTransitionFlipFromTop | UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         [self.view layoutIfNeeded];
                         
                         isOpened = true;
                         
                     }completion:NULL];
    
}

-(void)closeTable{
    
    [self.view layoutIfNeeded];
    
    self.consTableViewHeight.constant = 0;
    
    [UIView animateWithDuration:0.5 delay:0.0 options:UIViewAnimationOptionTransitionFlipFromBottom | UIViewAnimationOptionCurveEaseInOut animations:^{
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished) {
        _tblView.hidden = YES;
        isOpened = false;
    }];
    
}

-(void)viewDidAppear:(BOOL)animated
{
     [self.view layoutIfNeeded];
    self.consTableViewHeight.constant = 0;
    [self.mainScrollView setContentOffset:CGPointMake(0,0) animated:YES];
     [self.view layoutIfNeeded];
}


-(void)viewWillAppear:(BOOL)animated
{
    
//    [GlobalFunction addIndicatorView];
    
    NSLog(@"view will appear call");
    
    [self updateConstraints];
    
//    UIImageView *envelopeView = [[UIImageView alloc] initWithFrame:CGRectMake(0, -8, 8, 8)];
//    // cgre
//        envelopeView.image = [UIImage imageNamed:@"asterisk.png"];
//        envelopeView.contentMode = UIViewContentModeScaleAspectFit;
//        UIView *test=  [[UIView alloc]initWithFrame:CGRectMake(0, 0, 10, 10)];
//        [test addSubview:envelopeView];
//        [self.mobileTextField.rightView setFrame:envelopeView.frame];
//        self.mobileTextField.rightView =test;
//        self.mobileTextField.rightViewMode = UITextFieldViewModeUnlessEditing;
//        [self.nameTextField.rightView setFrame:envelopeView.frame];
//        self.nameTextField.rightView =test;
//        self.nameTextField.rightViewMode = UITextFieldViewModeUnlessEditing;
//        [self.priceTextField.rightView setFrame:envelopeView.frame];
//        self.priceTextField.rightView =test;
//        self.priceTextField.rightViewMode = UITextFieldViewModeAlways;
}
-(void)updateConstraints
{
    
 [self.view layoutIfNeeded];
    
//    self.conschooseCatTextFldTop.constant = 0;
//    self.consBtnTransCategoryTop.constant = 0;
    self.conschooseCategoryLineTop.constant = -5;
    self.constraintsTextFldsHeight.constant = 0;
    self.consBtnTransCategoryHeight.constant = 0;
    self.lineUnderChooseCategory.hidden = true;
    self.consTableViewTop.constant = 45;

     [self.view layoutIfNeeded];
    
    if (IS_IPHONE_5 || IS_IPHONE_4_OR_LESS)
    {
        self.consCheckBoxTop.constant = 21;
        self.lblCheckCondition.font=[self.lblCheckCondition.font fontWithSize:13.5f];
        [self.btnSaveAndPublish.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:16]];
        self.consBtnSaveHeight.constant = 48;
       // self.consMainViewBottom.constant = 60;
        self.consBtnSaeAndPublishBottom.constant = 60;
     //   [self.btnChooseCategoryTrans setImageEdgeInsets:UIEdgeInsetsMake(0, 266, 0, 0)];
        
        
         self.consAlertViewLeft.constant=42;
        self.consAlertViewTop.constant=165;

        self.consviewUnderScrollViewHeight.constant = 920;
        self.consBtnHelpLeft.constant = 2;
        
    }
    if (IS_IPHONE_6P)
    {
        
        self.consCheckBoxTop.constant = 22;
        self.consLblCHeckBoxTop.constant = 21;
        
        [self.btnSaveAndPublish.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:21]];
        self.consBtnSaveHeight.constant = 50;
       //  [_btnChooseCategoryTrans setImageEdgeInsets:UIEdgeInsetsMake(0, 353, 0, 0)];
        
        self.consAlertViewTop.constant=230;
        self.consAlertViewLeft.constant=88;

    }
    
    if (IS_IPHONE_6)
    {
        
 [self.btnSaveAndPublish.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:19]];
         self.consBtnSaveHeight.constant = 45;
        
    }
    
//    [self.mainScrollView setContentOffset:CGPointMake(0,
//                                                      0)
//                                 animated:YES];
    
    
    self.consNameTextFldHeight.constant = 0;
    self.consNameFldTop.constant = 0;
    self.consLineUnderNameTop.constant = 0;
     self.lineUnderName.hidden = YES;
    self.consMobileTextFldHeight.constant = 0;
    self.consMobileFldTop.constant = 0;
    self.consLineUnderMobileTop.constant = -4;
    self.lineUnderMobile.hidden = YES;
    
    [self.view layoutIfNeeded];
    
}

- (IBAction)saveAndPublishClicked:(id)sender
{
    
    [GlobalFunction addIndicatorView];
    
    [self performSelector:@selector(sendRequestToServer) withObject:nil afterDelay:0.2];
    
}
-(void)sendRequestToServer
{
    
    if (![[WebService shared] connected]) {
        
        [GlobalFunction removeIndicatorView];
        
        [[GlobalFunction shared] showAlertForMessage:@"Can't connect to internet, please check and try again."];
        
        return;
    }
//    if ([[self.categorytitleTextFld.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""]) {
//
//        [GlobalFunction removeIndicatorView];
//
//        [[GlobalFunction shared] showAlertForMessage:@"Category name cannot be blank."];
//
//        return;
//
//    }
    if ([[self.businessNameTextFld.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""]) {
        
        [GlobalFunction removeIndicatorView];
        
        [[GlobalFunction shared] showAlertForMessage:@"Business name cannot be blank."];
        
        return;
        
    }
    if ([[self.LocationTextFld.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""]) {
        
        [GlobalFunction removeIndicatorView];
        
        [[GlobalFunction shared] showAlertForMessage:@"Location cannot be blank."];
        
        return;
        
    }
//    if ([[self.titleTextFld.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""]) {
//        
//        [GlobalFunction removeIndicatorView];
//
//        [[GlobalFunction shared] showAlertForMessage:@"Title cannot be blank."];
//
//        return;
//
//    }
    if ([[self.dateTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""]) {
        
        [GlobalFunction removeIndicatorView];
        
        [[GlobalFunction shared] showAlertForMessage:@"Date/Time cannot be blank."];
        
        return;
        
    }
    if ([[self.priceTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""]) {
        
        [GlobalFunction removeIndicatorView];
        
        [[GlobalFunction shared] showAlertForMessage:@"Price cannot be blank."];
        
        return;
        
    }
//    if ([[self.descriptionTextView.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""]) {
//
//        [GlobalFunction removeIndicatorView];
//
//        [[GlobalFunction shared] showAlertForMessage:@"Description cannot be blank."];
//
//        return;
//
//    }
    if ([self.descriptionTextView.text isEqualToString:@"Any Special Request"])
    {
        
        self.descriptionTextView.text = @"";
//        [GlobalFunction removeIndicatorView];
//
//        [[GlobalFunction shared] showAlertForMessage:@"Description cannot be blank."];
//
//        return;
    }
    if (checkBtnClicked == false)
    {
        [GlobalFunction removeIndicatorView];
        
        [[GlobalFunction shared] showAlertForMessage:@"Before requesting any appointment you need to accept the condition."];
        
        return;
    }
    NSDictionary *dictBioResponse;
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    dictBioResponse = [[WebService shared] requestReservationUserId:myAppDelegate.userData.userid timeZone:[GlobalFunction getTimeZone] title:self.titleTextFld.text locationName:self.LocationTextFld.text businessName:self.businessNameTextFld.text startDate:[formatter stringFromDate:startDate] startTime:startTimeToSend description:self.descriptionTextView.text categoryId:@"" price:self.priceTextField.text name:@"" mobile:@""];
    
    if (dictBioResponse) {
        
        NSLog(@"response --- %@",[EncryptDecryptFile decrypt:[dictBioResponse valueForKey:@"responseMessage"]]);
        
        NSLog(@"response --- %@",[EncryptDecryptFile decrypt:[dictBioResponse valueForKey:@"responseCode"]]);
        
        if ([[EncryptDecryptFile decrypt:[dictBioResponse valueForKey:@"responseCode"]] isEqualToString:@"200"]) {
            
             [self bioUpdatedSuccefully:[EncryptDecryptFile decrypt:[dictBioResponse valueForKey:@"responseMessage"]]];
            
            self.categorytitleTextFld.text = @"";
            self.businessNameTextFld.text = @"";
            self.LocationTextFld.text = @"";
            self.titleTextFld.text = @"";
            self.dateTextField.text = @"";
            self.priceTextField.text = @"";
            self.descriptionTextView.text = @"Any Special Request";
            self.nameTextField.text = @"";
            self.mobileTextField.text = @"";
            self.descriptionTextView.textColor = [UIColor colorWithRed:196.0/255.0f green:196.0/255.0f blue:196.0/255.0f alpha:1.0];
            [self.descriptionTextView setFont:[UIFont fontWithName:@"Lato-Regular" size:17]];        }
        else{
            
            [[GlobalFunction shared] showAlertForMessage:[EncryptDecryptFile decrypt:[dictBioResponse valueForKey:@"responseMessage"]]];
            
            [GlobalFunction removeIndicatorView];
            
        }

    }
    else
    {
        
        [[GlobalFunction shared] showAlertForMessage:@"Server is down, please try again later."];
        
        [GlobalFunction removeIndicatorView];
        
    }
}

-(void)bioUpdatedSuccefully:(NSString *)response{
    
    [GlobalFunction removeIndicatorView];
    
    
    NSLog(@"successful .......");
    
    
    
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"BUY TIMEE" message:response delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    
    alert.tag = 10001;
    
    [alert show];
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    
    if (alertView.tag == 10001){
        if(buttonIndex == 0)//OK button pressed
        {
            //do something
            NSLog(@"ook pressed !!!");
            
                myAppDelegate.isSideBarAccesible = true;
            
                //    TaskTableViewCell *cell = (TaskTableViewCell *)[myAppDelegate.vcObj.tblViewSideBar cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
                //    [myAppDelegate.vcObj performSegueWithIdentifier:@"viewAllTask" sender:cell];
            
            
                TaskTableViewCell *cell = (TaskTableViewCell *)[myAppDelegate.vcObj.tblViewSideBar cellForRowAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:0]];
                [myAppDelegate.vcObj performSegueWithIdentifier:@"taskhistory" sender:cell];
            
            
            
            
        }
        else if(buttonIndex == 1)//Annul button pressed.
        {
            //do something
        }
    }
}
#pragma mark - create accessory view
-(void)createInputAccessoryView{
    // Create the view that will play the part of the input accessory view.
    // Note that the frame width (third value in the CGRectMake method)
    // should change accordingly in landscape orientation. But we don’t care
    // about that now.
    inputAccView = [[UIView alloc] initWithFrame:CGRectMake(10.0, 0.0, self.view.frame.size.width, 40.0)];
    // Set the view’s background color. We’ ll set it here to gray. Use any color you want.
    [inputAccView setBackgroundColor:[UIColor lightGrayColor]];
    // We can play a little with transparency as well using the Alpha property. Normally
    // you can leave it unchanged.
    [inputAccView setAlpha: 0.8];
    // If you want you may set or change more properties (ex. Font, background image,e.t.c.).
    // For now, what we’ ve already done is just enough.
    // Let’s create our buttons now. First the previous button.
    btnPrev = [UIButton buttonWithType: UIButtonTypeCustom];
    // Set the button’ s frame. We will set their widths to 80px and height to 40px.
    [btnPrev setFrame: CGRectMake(0.0, 0.0, 80.0, 40.0)];
    // Title.
    [btnPrev setTitle: @"Previous" forState: UIControlStateNormal];
    // Background color.
    [btnPrev setBackgroundColor: [UIColor clearColor]];
    // You can set more properties if you need to.
    // With the following command we’ ll make the button to react in finger tapping. Note that the
    // gotoPrevTextfield method that is referred to the @selector is not yet created. We’ ll create it
    // (as well as the methods for the rest of our buttons) later.
    [btnPrev addTarget: self action: @selector(gotoPrevTextfield) forControlEvents: UIControlEventTouchUpInside];
    // Do the same for the two buttons left.
    btnNext = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnNext setFrame:CGRectMake(85.0f, 0.0f, 80.0f, 40.0f)];
    [btnNext setTitle:@"Next" forState:UIControlStateNormal];
    [btnNext setBackgroundColor:[UIColor clearColor]];
    [btnNext addTarget:self action:@selector(gotoNextTextfield) forControlEvents:UIControlEventTouchUpInside];
    btnDone = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnDone setFrame:CGRectMake(self.view.frame.size.width - 80.0f, 0.0f, 80.0f, 40.0f)];
    [btnDone setTitle:@"Done" forState:UIControlStateNormal];
    [btnDone setBackgroundColor:[UIColor clearColor]];
    [btnDone setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [btnDone addTarget:self action:@selector(doneTyping) forControlEvents:UIControlEventTouchUpInside];
    // Now that our buttons are ready we just have to add them to our view.
    [inputAccView addSubview:btnPrev];
    [inputAccView addSubview:btnNext];
    [inputAccView addSubview:btnDone];
}
-(void)gotoPrevTextfield
{
    if (txtActiveField == self.businessNameTextFld)
    {
        [txtActiveField resignFirstResponder];
       // [self.LocationTextFld becomeFirstResponder];
    }
    else if(txtActiveField == self.LocationTextFld)
    {
        [txtActiveField resignFirstResponder];
        [self.businessNameTextFld becomeFirstResponder];
    }
    else if(txtActiveField == self.titleTextFld)
    {
        [txtActiveField resignFirstResponder];
        [self.LocationTextFld becomeFirstResponder];
    }
    else if(txtActiveField == self.dateTextField)
    {
        [txtActiveField resignFirstResponder];
        [self.titleTextFld becomeFirstResponder];
    }
    else if(txtActiveField == self.priceTextField)
    {
        [txtActiveField resignFirstResponder];
        [self.dateTextField becomeFirstResponder];
    }
//    else if(txtActiveField == self.nameTextField)
//    {
//        [txtActiveField resignFirstResponder];
//        [self.priceTextField becomeFirstResponder];
//    }
//    else if(txtActiveField == self.mobileTextField)
//    {
//        [txtActiveField resignFirstResponder];
//        [self.nameTextField becomeFirstResponder];
//    }
    else if(txtViewActiveField == self.descriptionTextView)
    {
        [txtViewActiveField resignFirstResponder];
         [self.priceTextField becomeFirstResponder];
    }

}
-(void)gotoNextTextfield
{
    if (txtActiveField == self.businessNameTextFld)
    {
        [txtActiveField resignFirstResponder];
        [self.LocationTextFld becomeFirstResponder];
    }
    else if(txtActiveField == self.LocationTextFld)
    {
        [txtActiveField resignFirstResponder];
        [self.titleTextFld becomeFirstResponder];
    }
    else if(txtActiveField == self.titleTextFld)
    {
        [txtActiveField resignFirstResponder];
        [self.dateTextField becomeFirstResponder];
    }
    else if(txtActiveField == self.dateTextField)
    {
        [txtActiveField resignFirstResponder];
        [self.priceTextField becomeFirstResponder];
    }
    else if(txtActiveField == self.priceTextField)
    {
        [txtActiveField resignFirstResponder];
        [self.descriptionTextView becomeFirstResponder];
    }
//    else if(txtActiveField == self.nameTextField)
//    {
//        [txtActiveField resignFirstResponder];
//        [self.mobileTextField becomeFirstResponder];
//    }
//    else if(txtActiveField == self.mobileTextField)
//    {
//        [txtActiveField resignFirstResponder];
//        [self.descriptionTextView becomeFirstResponder];
//    }
    else if(txtViewActiveField == self.descriptionTextView)
    {
        [txtViewActiveField resignFirstResponder];
       // [self.descriptionTextView becomeFirstResponder];
    }
}
-(void)doneTyping
{
   
    
    [self.mainScrollView setContentOffset:CGPointMake(0,
                                                      0)
                                 animated:YES];
    if (isOpened == true)
    {
        [self closeTable];
        
    }
    
    [self.view resignFirstResponder];
    [txtActiveField resignFirstResponder];
    [txtViewActiveField resignFirstResponder];
    [self.categorytitleTextFld resignFirstResponder];
    [self.businessNameTextFld resignFirstResponder];
    [self.LocationTextFld resignFirstResponder];
    [self.titleTextFld resignFirstResponder];
    [self.dateTextField resignFirstResponder];
    [self.nameTextField resignFirstResponder];
    [self.mobileTextField resignFirstResponder];
    [self.descriptionTextView resignFirstResponder];


    
}

#pragma mark - Text Field delegate
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    //    CGRect frame = self.textFldView.frame;
    //    frame.origin.y = 0;
    
   // [textField resignFirstResponder];
    
txtActiveField = textField;
    
    return YES;
}


-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    if (textField == self.categorytitleTextFld)
    {
//        textField.tintColor = [UIColor clearColor];
//       
//        _tblView.hidden = NO;
//        [self openTable];
//        
//        datePicker.hidden = NO;
//         [self setEditing:NO animated:YES];
//       // [self canPerformAction:textField.id withSender:textField];
//        return;
    }
    
    [self createInputAccessoryView];
    [textField setInputAccessoryView:inputAccView];

    txtActiveField = textField;
       txtViewActiveField = nil;
  
    if (txtActiveField == self.businessNameTextFld)
    {
            [self.view layoutIfNeeded];
           // self.consTableViewTop.constant = 56;
            [self.view layoutIfNeeded];
    
        datePicker.hidden = NO;
    }
    else if (txtActiveField == self.titleTextFld)
    {
        if(IS_IPHONE_5 || IS_IPHONE_4_OR_LESS)
        {
            [self.mainScrollView setContentOffset:CGPointMake(0,
                                                              13)
                                         animated:YES];
        }
    }
    
    else if (txtActiveField == self.priceTextField)
    {
        if(IS_IPHONE_6)
        {
        [self.mainScrollView setContentOffset:CGPointMake(0,
                                                          32)
                                     animated:YES];
        }
        if(IS_IPHONE_5 || IS_IPHONE_4_OR_LESS)
        {
            [self.mainScrollView setContentOffset:CGPointMake(0,
                                                              120)
                                         animated:YES];
        }
        datePicker.hidden = NO;
    }
   
    else if (txtActiveField == self.dateTextField)
    {
        if(IS_IPHONE_5 || IS_IPHONE_4_OR_LESS)
        {
            [self.mainScrollView setContentOffset:CGPointMake(0,
                                                              70)
                                         animated:YES];
        }
        datePicker.hidden = NO;
    }
    else if (txtActiveField == self.nameTextField)
    {
//        if(IS_IPHONE_6)
//        {
//            [self.mainScrollView setContentOffset:CGPointMake(0,
//                                                              75)
//                                         animated:YES];
//        }
//        if(IS_IPHONE_6P)
//        {
//            [self.mainScrollView setContentOffset:CGPointMake(0,
//                                                              35)
//                                         animated:YES];
//        }
//        if(IS_IPHONE_5 || IS_IPHONE_4_OR_LESS)
//        {
//          [self.mainScrollView setContentOffset:CGPointMake(0,169)
//                                                     animated:YES];
//        }
//        datePicker.hidden = NO;
    }
    else if (txtActiveField == self.mobileTextField)
    {
//        if(IS_IPHONE_6)
//        {
//            [self.mainScrollView setContentOffset:CGPointMake(0,
//                                                              128)
//                                         animated:YES];
//        }
//        if(IS_IPHONE_6P)
//        {
//            [self.mainScrollView setContentOffset:CGPointMake(0,
//                                                              75)
//                                         animated:YES];
//        }
//        if(IS_IPHONE_5 || IS_IPHONE_4_OR_LESS)
//        {
//                        [self.mainScrollView setContentOffset:CGPointMake(0,
//                                                                          217)
//                                                     animated:YES];
//        }
//        datePicker.hidden = NO;
    }
}
-(void)textFieldDidEndEditing:(UITextField *)textField{
    
    // [txtActiveField resignFirstResponder];
    [self doneTyping];
    
}


#pragma mark - Text Field delegate
- (void)textViewDidChange:(UITextView *)textView
{
    
    
    
}

-(void)textViewDidEndEditing:(UITextView *)textView
{
     [self doneTyping];
}

-(void)textViewDidBeginEditing:(UITextView *)textView
{
   // [self.mainScrollView setContentOffset:CGPointMake(0,200) anima];
    if(IS_IPHONE_6)
    {
    [self.mainScrollView setContentOffset:CGPointMake(0,
                                                      100)
                                 animated:YES];
    }
    if(IS_IPHONE_6P)
    {
        [self.mainScrollView setContentOffset:CGPointMake(0,
                                                          60)
                                     animated:YES];
    }
    if(IS_IPHONE_5 || IS_IPHONE_4_OR_LESS)
    {
        [self.mainScrollView setContentOffset:CGPointMake(0,
                                                          150)
                                     animated:YES];
    }
   // in school you're taught a lesson and then given a test
}
-(BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    [self createInputAccessoryView];
    [textView setInputAccessoryView:inputAccView];
    txtViewActiveField = textView;
    
    txtActiveField = nil;
    
    return YES;
}
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    
    
    NSLog(@"range change");
    
    
    
    NSLog(@"range change");
    
    
    
    NSString *value = [textView.text stringByReplacingCharactersInRange:range withString:text];
    
    
    
    if ([self.descriptionTextView.text isEqualToString:@"Any Special Request"]) {
        self.descriptionTextView.text = @"";
        self.descriptionTextView.textColor=[UIColor colorWithRed:63.0/255.0f green:67.0/255.0f blue:70.0/255.0f alpha:1.0];
        
    }
    else if (value.length==0)
    {
        self.descriptionTextView.text = @"Any Special Request";
        self.descriptionTextView.textColor=[UIColor colorWithRed:196.0/255.0f green:196.0/255.0f blue:196.0/255.0f alpha:1.0];
        
        [self.descriptionTextView resignFirstResponder];
        
        
        return NO;
    }
    else if (value.length>0)
    {
        self.descriptionTextView.textColor=[UIColor colorWithRed:63.0/255.0f green:67.0/255.0f blue:70.0/255.0f alpha:1.0];
        
    }
    
    
    
    
    return YES;
}

- (IBAction)checkBoxClicked:(id)sender
{
    if (checkBtnClicked == false)
    {
        [self.btnCheckBox setBackgroundImage:[UIImage imageNamed:@"CheckImg.png"] forState:UIControlStateNormal];
        checkBtnClicked = true;
    }
    else if(checkBtnClicked == true)
    {
         [self.btnCheckBox setBackgroundImage:[UIImage imageNamed:@"UncheckImg.png"] forState:UIControlStateNormal];
        checkBtnClicked = false;
    }
        
}
- (IBAction)okBtnClicked:(id)sender
{
    
    [UIView animateWithDuration:0.3 animations:^{
        self.blackTransparentView.alpha = 0;
    } completion: ^(BOOL finished) {//creates a variable (BOOL) called "finished" that is set to *YES* when animation IS completed.
        self.blackTransparentView.hidden = true;//if animation is finished ("finished" == *YES*), then hidden = "finished" ... (aka hidden = *YES*)
    }];
    
}
- (IBAction)helpBtnClicked:(id)sender
{
    
    [self doneTyping];
    
    self.lblTransparentViewMessage.text = @"Please add complete address in following sequence: Street, Unit, City, State and Zip.";
    [UIView transitionWithView:self.btnHelp
                      duration:0.4
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^{
                        self.blackTransparentView.hidden = false;
                        self.blackTransparentView.alpha = 1.0;
                        
                    }
                    completion:NULL];
    
}
- (IBAction)priceHelpClicked:(id)sender
{
    
    [self doneTyping];
    
     self.lblTransparentViewMessage.text = @"Please note the amount that you have offered is a premium for the time slot only and is not a payment for the actual service.";
    [UIView transitionWithView:self.btnHelp
                      duration:0.4
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^{
                        self.blackTransparentView.hidden = false;
                        self.blackTransparentView.alpha = 1.0;
                        
                    }
                    completion:NULL];
}
@end
