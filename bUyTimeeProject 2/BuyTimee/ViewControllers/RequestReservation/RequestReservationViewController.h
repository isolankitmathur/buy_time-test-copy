//
//  RequestReservationViewController.h
//  BuyTimee
//
//  Created by User38 on 21/08/18.
//  Copyright © 2018 User14. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RequestReservationViewController : UIViewController

@property (nonatomic, retain) UITextField *txtActiveField;
@property (nonatomic, retain) UIView *inputAccView;
@property (nonatomic, retain) UIButton *btnDone;
@property (nonatomic, retain) UIButton *btnNext;
@property (nonatomic, retain) UIButton *btnPrev;

@property (nonatomic, retain) UITextView *txtViewActiveField;

-(void)doneTyping;



@property (weak, nonatomic) IBOutlet UIScrollView *mainScrollView;

@property (weak, nonatomic) IBOutlet UIView *viewUnderScrollView;

@property (weak, nonatomic) IBOutlet UILabel *lblRequestYourReservation;

@property (weak, nonatomic) IBOutlet UITextField *businessNameTextFld;
@property (weak, nonatomic) IBOutlet UILabel *lineUnderBusiness;

@property (weak, nonatomic) IBOutlet UITextField *LocationTextFld;
@property (weak, nonatomic) IBOutlet UILabel *lineUnderLocation;
@property (weak, nonatomic) IBOutlet UITextField *titleTextFld;
@property (weak, nonatomic) IBOutlet UILabel *lineUnderTitle;
@property (weak, nonatomic) IBOutlet UITextField *dateTextField;
@property (weak, nonatomic) IBOutlet UILabel *lineUnderDate;
@property (weak, nonatomic) IBOutlet UITextView *descriptionTextView;
@property (weak, nonatomic) IBOutlet UILabel *lineUnderDescription;


@property (weak, nonatomic) IBOutlet UIButton *btnSaveAndPublish;

- (IBAction)saveAndPublishClicked:(id)sender;

@property (weak, nonatomic) IBOutlet UITextField *categorytitleTextFld;
@property (weak, nonatomic) IBOutlet UILabel *lineUnderChooseCategory;


@property (weak, nonatomic) IBOutlet UITableView *tblView;


@property (weak, nonatomic) IBOutlet UIButton *btnChooseCategoryTrans;

- (IBAction)chooseCategoryClicked:(id)sender;

@property (weak, nonatomic) IBOutlet UITextField *priceTextField;
@property (weak, nonatomic) IBOutlet UILabel *lineUnderPrice;
@property (weak, nonatomic) IBOutlet UILabel *lblCheckCondition;

@property (weak, nonatomic) IBOutlet UIButton *btnCheckBox;
- (IBAction)checkBoxClicked:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *btnHelp;
- (IBAction)helpBtnClicked:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *btnHelpPrice;

@property (weak, nonatomic) IBOutlet UIView *blackTransparentView;
- (IBAction)priceHelpClicked:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *btnOK;
- (IBAction)okBtnClicked:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *nameTextField;
@property (weak, nonatomic) IBOutlet UILabel *lineUnderName;

@property (weak, nonatomic) IBOutlet UITextField *mobileTextField;
@property (weak, nonatomic) IBOutlet UILabel *lineUnderMobile;


// Consraints Outlates...

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consCheckBoxTop;


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consBtnHelpLeft;

@property (weak, nonatomic) IBOutlet UILabel *lblTransparentViewMessage;


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintsLblRequestReservationTop;


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *conschooseCatTextFldTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *conschooseCategoryLineTop;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintsTextFldsHeight;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consBusinessTextFldTop;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consBusinessLineTop;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consLocationTextFldTop;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consLocationLineTop;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consTitleTextFldTop;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consTitleLineTop;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consDateTextFldTop;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consDateLineTop;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consDescriptionTxtViewTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consDescriptionLineTop;


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consBtnSaveHeight;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consMainViewBottom;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consBtnSaeAndPublishBottom;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consviewUnderScrollViewHeight;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consviewUnderScrollViewWidth;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consTableViewHeight;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consTableViewTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consAlertViewTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consAlertViewLeft;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consLblCHeckBoxTop;


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consTxtFldCategoryHeight;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consBtnTransCategoryHeight;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consBtnTransCategoryTop;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consMobileTextFldHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consNameTextFldHeight;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consNameFldTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consLineUnderNameTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consMobileFldTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consLineUnderMobileTop;

@end
