//
//  RequestTableViewCell.h
//  BuyTimee
//
//  Created by User38 on 21/08/18.
//  Copyright © 2018 User14. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RequestTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imgViewIcon1;

@property (weak, nonatomic) IBOutlet UILabel *lblList1;

@end
