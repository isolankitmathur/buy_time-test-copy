//
//  GooglePlacesData.h
//  BuyTimee
//
//  Created by User on 10/16/18.
//  Copyright © 2018 User14. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GooglePlacesData : NSObject

@property(nonatomic,strong) NSString *PlaceFullString;
@property(nonatomic,strong) NSString *placeName;

@property(nonatomic,strong) NSString *placeAddress;

@property(nonatomic,strong) NSString *placeId;

@end
