//
//  MapAnnotationViewController.h
//  BuyTimee
//
//  Created by User14 on 30/03/16.
//  Copyright © 2016 User14. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MapAnnotationViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIView *viewMapAnnotation;
@property (weak, nonatomic) IBOutlet UIImageView *imgViewUser;
@property (weak, nonatomic) IBOutlet UILabel *lblUserName;
@property (weak, nonatomic) IBOutlet UILabel *lblUserAddress;
@property (weak, nonatomic) IBOutlet UILabel *lblUserUsername;
@property (weak, nonatomic) IBOutlet UILabel *lblCenter;

@end
