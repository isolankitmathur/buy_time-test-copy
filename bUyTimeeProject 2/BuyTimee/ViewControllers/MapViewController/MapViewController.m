//
//  MapViewController.m
//  HandApp
//
//  Created by  ~ on 07/03/16.
//  Copyright (c) 2016 . All rights reserved.
//

#import "MapViewController.h"
#import "GlobalFunction.h"
#import "Constants.h"
#import "CategoryTableViewCell.h"
#import "category.h"
#import "subCategory.h"
#import "task.h"
#import "MyAnnotation.h"
#import "MapAnnotationViewController.h"
#import "TaskDetailTableViewCell.h"
#import <DXAnnotationView.h>
#import <DXAnnotationSettings.h>
#import "MapCityTableViewCell.h"
#import "cities.h"

#define METERS_PER_MILE 1609.344



@interface MapViewController () <UIGestureRecognizerDelegate>{
    
    BOOL buttonClick;
    UITapGestureRecognizer *tapGesture;
    CGFloat _currentKeyboardHeight;
    CGFloat currentHeight;
    
    MKAnnotationView *prevView;
    NSArray *FilterCities;
}

@property (weak, nonatomic) IBOutlet UILabel *lblSeperatorCategory;


//radius table view work
@property (weak, nonatomic) IBOutlet UIView *viewBgTapHandlerForRadiusTable;
@property (weak, nonatomic) IBOutlet UIView *viewHolderForTableRadius;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintViewHolderTableRadiusHeight;

- (IBAction)hideRadiusTableView:(id)sender;

@property (strong, nonatomic) IBOutlet UITapGestureRecognizer *tapGestureRecognizerForHidingRadiusTableView;


@end

@implementation MapViewController{
    
    NSString *categoryName;
    NSArray *arrayOfTaskWithStatus0;
    NSMutableArray *annotationsArray;
    
    NSString *oldRadiusData;
    
    NSString *oldRadiusData_TextToShowAtEditing;
    NSString *oldRadiusData_TextToShowAsPlaceHolder;
    
    NSString *oldLocationData;
    NSString *oldMinPriceRangeData;
    NSString *oldMaxPriceRangeData;
    
    BOOL isShow;
    
    NSString *filterCategoryId;
    NSString *filterSubCategoryId;
    NSString *filterRange;
    NSString *filterLocationName;
    NSString *filterPriceMin;
    NSString *filterPriceMax;
    
    BOOL isFromTableTap;
    BOOL isFromRadiusTableTap;
    BOOL isFromSearchResult;
    
    NSArray *searchResultArrayFromRadius;
    
    DXAnnotationView *prevDXAnnotationView;
    
    NSIndexPath *indexPathForWorkerTaskData;
    
    NSArray *arrRadius;
    
//    NSIndexPath *lastSelectedIndexPath;
    
    
    //////// vs
    
    NSDate *dateStart;
    NSDate *dateEnd;
    NSString *enteredLocation;
    NSString *rangeLocation;
    
    NSString *minPriceValue;
    NSString *maxPriceValue;
    
    NSString *minTime;
    NSString *maxTime;
    UIToolbar *toolBar;

    /////// vs
    
}

@synthesize txtActiveField;
@synthesize inputAccView;
@synthesize btnDone;
@synthesize btnNext;
@synthesize btnPrev,lastSelectedIndexPath;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    myAppDelegate.isFromMapVCForFilter = true;
    
     myAppDelegate.TandC_Buyee=@"mapView"; // by kp for handle transition.
    
     myAppDelegate.vcObj.btnMapOnListView.hidden = true;//vs
    myAppDelegate.vcObj.BtnTransparentMap.hidden = true;//vs
    
    

    
    self.filterCityView.layer.borderWidth = 1;
   // self.cityNameTableView.layer.borderWidth = 1;
    self.filterCityView.layer.borderColor = [[UIColor colorWithRed:63.0/255.0f green:67.0/255.0f blue:70.0/255.0f alpha:1.0] CGColor];
    self.cityNameTableView.layer.borderColor = [[UIColor colorWithRed:63.0/255.0f green:67.0/255.0f blue:70.0/255.0f alpha:1.0] CGColor];
    self.filterCityView.layer.cornerRadius = 10;
    self.cityNameTableView.layer.cornerRadius = 10;
    
    
    self.cityNameTableView.delegate = self;
    self.cityNameTableView.dataSource = self;
    
    //  self.filterCityView.hidden = true;
    self.filterCityViewHeight.constant = 0;
   // self.filterCityViewBottom.constant = -41;
 
    
    
    filterLocationName = @"";
    filterPriceMax = @"";
    filterPriceMin = @"";
    filterRange = @"30";
    rangeLocation = @"30";

    if (myAppDelegate.currentCityName.length>0)
    {
        enteredLocation = myAppDelegate.currentCityName;
    }
    else
    {
    }
    
    
    if (myAppDelegate.mainMiles)
    {
        rangeLocation = myAppDelegate.mainMiles;
    }
    else
    {
        
    }
    if (myAppDelegate.mainLocation)
    {
        if ([myAppDelegate.mainLocation isEqualToString:myAppDelegate.lastSearchLocation])
        {
            enteredLocation = myAppDelegate.mainLocation;
        }
        else
        {
            enteredLocation = myAppDelegate.lastSearchLocation;
        }
    }
    else
    {
        
    }
    if (myAppDelegate.mainMinPrice)
    {
        minPriceValue = myAppDelegate.mainMinPrice;
    }
    else
    {
        
    }
    if (myAppDelegate.mainmaxPrice)
    {
        maxPriceValue = myAppDelegate.mainmaxPrice;
    }
    else
    {
        
    }
    

   // oldRadiusData_TextToShowAtEditing = @"50";
    //oldRadiusData_TextToShowAsPlaceHolder = @"10miles of";
    self.minTxtFld.text = oldRadiusData_TextToShowAsPlaceHolder;
    
   
    
    filterSubCategoryId = @"";
 //   lastSelectedIndexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    
    if (myAppDelegate.SelectedIndexPathForFilter.row >0) {
        
        lastSelectedIndexPath = myAppDelegate.SelectedIndexPathForFilter;
        
        //[self.tblCategory reloadData]; category *obj = [myAppDelegate.arrayCategoryDetail objectAtIndex:myAppDelegate.SelectedIndexPathForFilter.row-1];
            category *obj = [myAppDelegate.arrayCategoryDetail objectAtIndex:myAppDelegate.SelectedIndexPathForFilter.row-1];
            filterCategoryId = obj.categoryId;
            categoryName=obj.name;
            self.lblCategoryPlaceHolder.text=categoryName;

    }
    else
    {
        lastSelectedIndexPath = [NSIndexPath indexPathForRow:0 inSection:0];
         categoryName = @"All Categories";
        filterCategoryId = @"";
    }
    
    //[self.adjustRadiusBtn setSelected:YES];
    self.minTxtFld.textColor = [UIColor colorWithRed:63.0/255.0f green:67.0/255.0f blue:70.0/255.0f alpha:1.0];//RGB(185, 185, 185);
    
    [_minLabel setText:@"Adjust Radius"];
    [_maxLabel setText:@"Location"];
    
   // categoryName =obj.name; // @"All Categories";
    
    arrRadius = [NSArray arrayWithObjects:@"10",@"20",@"30",@"40",@"50",@"60",@"70",@"80",@"90",@"100",@"200",@"300",@"400",@"500", nil];
    
    //radius combo box work 26April2016
    self.tblRadius.dataSource = self;
    self.tblRadius.delegate = self;
    
    [self.view layoutIfNeeded];
    self.constraintViewHolderTableRadiusHeight.constant = 0;
    [self.view layoutIfNeeded];
    
    self.viewBgTapHandlerForRadiusTable.hidden = YES;
    self.viewHolderForTableRadius.hidden = YES;
    
    [self.tblRadius layoutIfNeeded];
    [self.tblRadius reloadData];
    [self.tblRadius layoutIfNeeded];
    //radius combo box work 26April2016
    
    
    self.minTxtFld.keyboardType = UIKeyboardTypeDecimalPad;
    
   // NSAttributedString *str1 = [[NSAttributedString alloc] initWithString:@"City, State" attributes:@{ NSForegroundColorAttributeName : RGB(185, 185, 185),
    //                                                                                                   }];
    //_maxTxtFld.attributedPlaceholder = str1;
    
    //    NSAttributedString *str2 = [[NSAttributedString alloc] initWithString:@"Within 50 miles" attributes:@{ NSForegroundColorAttributeName : RGB(185, 185, 185),
    //                                                                                                            }];
    //    _minTxtFld.attributedPlaceholder = str2;
    
    _minTxtFld.delegate=self;
    _maxTxtFld.delegate=self;
    
    self.mapView.delegate = self;
    self.mapView.mapType = MKMapTypeStandard;
    self.mapView.showsUserLocation = NO;
    
    //category view tap gesture
    tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(openCategoryTableView:)];
    tapGesture.delegate = self;
    tapGesture.numberOfTapsRequired = 1;
    
    [self.viewCategoryTapHandler addGestureRecognizer:tapGesture];
    
    self.tblCategory.hidden = YES;
    
    self.tblCategory.delegate = self;
    self.tblCategory.dataSource = self;
    
    [self.tblCategory layoutIfNeeded];
    [self.tblCategory reloadData];
    [self.tblCategory layoutIfNeeded];
    
   // myAppDelegate.vcObj.lblTitleText.text = @"B U Y  T I M E E";
    
    _currentKeyboardHeight = 0.0f;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
    oldLocationData = @"";
    oldMaxPriceRangeData = @"";
    oldMinPriceRangeData = @"";
    oldRadiusData = @"";
    
   // self.lblSeperatorCategory.hidden = YES;
    
    [GlobalFunction createBorderforView:self.mapView withWidth:0.5 cornerRadius:0.0 colorForBorder:RGB(151, 151, 151)];
    
    [GlobalFunction createBorderforView:self.viewHolderForTableRadius withWidth:0.5 cornerRadius:0.0 colorForBorder:RGB(151, 151, 151)];
    
    self.viewHolderForTableRadius.layer.cornerRadius = 5.0f;
    self.viewHolderForTableRadius.layer.masksToBounds = YES;
    
    
    ///////vs
    
        _datePickerView.hidden = true;
        self.filterView.hidden = true;
        self.parentFilterView.hidden = true;

    _blurViewHeight.constant=0;

    
//    self.parentFilterView.alpha=0.0;
//    self.filterView.alpha=0.0;
    
    _datePickerView.minimumDate = [NSDate date];
    _datePickerView.timeZone = [NSTimeZone systemTimeZone];
    [_datePickerView setDate:[NSDate date]];
  //  [_datePickerView setDatePickerMode:UIDatePickerModeDateAndTime];
     [_datePickerView setDatePickerMode:UIDatePickerModeDate];
    
    [_datePickerView addTarget:self action:@selector(updateTextField:) forControlEvents:UIControlEventValueChanged];
    //[_datePickerView setDatePickerMode:UIDatePickerModeDate];
    
    
    _parentFilterViewHeight.constant=210;
    
    if(IS_IPHONE_5 || IS_IPHONE_4_OR_LESS)
    {
    [_datePickerView setFrame:CGRectMake(10,15, 300, 200)];
    }
    
    else if (IS_IPHONE_6)
    {
        [_datePickerView setFrame:CGRectMake(15,15, 340, 200)];
    }
    
    else if (IS_IPHONE_6P)
    {
        [_datePickerView setFrame:CGRectMake(15,15, 375, 200)];
    }
    
    
    
    NSDateFormatter *dateFormat00 = [[NSDateFormatter alloc] init];
    [dateFormat00 setDateFormat:dateFormatDate];
    
    NSDateFormatter *TimeFormat11 = [[NSDateFormatter alloc] init];
    [TimeFormat11 setDateFormat:dateFormatOnlyHour];


    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"d.M.yyyy";
    NSString *string = [formatter stringFromDate:[NSDate date]];
    NSDate *mainDate = [formatter dateFromString:string];
   // dateStart = mainDate;
    
    if (myAppDelegate.mainStartDate)
    {
        dateStart = myAppDelegate.mainStartDate;
        
        NSString *TimeString = [GlobalFunction convertTimeFormat:[TimeFormat11 stringFromDate:dateStart]];
        NSString *dateString = [GlobalFunction convertDateFormat:[dateFormat00 stringFromDate:dateStart]];
        NSString *str00 = [@[dateString, TimeString] componentsJoinedByString:@""];
        minTime = dateString;
    }
    else
    {
       // dateStart = mainDate; // commented by vs for remove default date in filter to show old tasks 10 oct 17.
    }
    if (myAppDelegate.mainEndDate)
    {
        dateEnd = myAppDelegate.mainEndDate;
        
        NSString *TimeString = [GlobalFunction convertTimeFormat:[TimeFormat11 stringFromDate:dateEnd]];
        NSString *dateString = [GlobalFunction convertDateFormat:[dateFormat00 stringFromDate:dateEnd]];
        NSString *str00 = [@[dateString, TimeString] componentsJoinedByString:@""];
        maxTime = dateString;
    }
    
    NSLog(@"main date in did load is...%@",dateStart);
    [self.tblCategory layoutIfNeeded];
    [self.tblCategory reloadData];
    [self.tblCategory layoutIfNeeded];
    ////////vs
}




-(void)updateTextField:(id)sender{
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:dateFormatDate];
    
    NSDateFormatter *TimeFormat1 = [[NSDateFormatter alloc] init];
    [TimeFormat1 setDateFormat:dateFormatOnlyHour];
    
    
    
    
    if (_datePickerView.hidden == false) {
        
       // _datePickerView.hidden = true;
       // self.parentFilterView.hidden = true;
    }
    
    
    if (txtActiveField == self.minTxtFld) {
        
        dateStart = _datePickerView.date;
        NSString *TimeString = [GlobalFunction convertTimeFormat:[TimeFormat1 stringFromDate:dateStart]];
        NSString *dateString = [GlobalFunction convertDateFormat:[dateFormat stringFromDate:dateStart]];
        NSString *str = [@[dateString, TimeString] componentsJoinedByString:@","];
        // NSString *mainString = [
        self.minTxtFld.text = dateString;
        minTime = dateString;
       // [self addAllPins];
        
    }
    else if (txtActiveField == self.maxTxtFld){
        
        dateEnd = _datePickerView.date;
        
        NSString *TimeString = [GlobalFunction convertTimeFormat:[TimeFormat1 stringFromDate:dateEnd]];
        NSString *dateString = [GlobalFunction convertDateFormat:[dateFormat stringFromDate:dateEnd]];
        NSString *str = [@[dateString, TimeString] componentsJoinedByString:@","];
        // NSString *mainString = [
        self.maxTxtFld.text = dateString;
        maxTime = dateString;
      //  [self addAllPins];
    }
    
    
//    if (dateStart != nil && dateEnd != nil)
//    {
//        //        if ([dateStart compare:dateEnd] == NSOrderedDescending) {
//        //            NSLog(@"date1 is Newer than date2");
//        //        } else
//        
//        if ([dateStart compare:dateEnd] == NSOrderedAscending) {
//            NSLog(@"date1 is earlier than date2");
//          //  [self addAllPins];
//        } else {
//            
//            NSLog(@"date2 is earlier than date1");
//            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"BuyTimee" message:@"Please enter a valid date." delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:nil];
//            
//            [alertView show];
//        }
//        
//        
//    }

    
}

-(void)viewWillAppear:(BOOL)animated
{
    _adjustRadiusBtn.backgroundColor=[UIColor colorWithRed:63.0/255.0f green:67.0/255.0f blue:70.0/255.0f alpha:1.0];
    _btnTime.backgroundColor=[UIColor colorWithRed:63.0/255.0f green:67.0/255.0f blue:70.0/255.0f alpha:1.0];
    _priceRangeBtn.backgroundColor=[UIColor colorWithRed:63.0/255.0f green:67.0/255.0f blue:70.0/255.0f alpha:1.0];
    
    _filterView.backgroundColor=[UIColor colorWithRed:82.0/255.0f green:85.0/255.0f blue:88.0/255.0f alpha:1.0];
    
    _lblSeperatorCategory.backgroundColor=[UIColor colorWithRed:196.0/255.0f green:196.0/255.0f blue:196.0/255.0f alpha:1.0];
    
    _viewCategoryView.backgroundColor=[UIColor colorWithRed:244.0/255.0f green:244.0/255.0f blue:244.0/255.0f alpha:1.0];
    
    
    
    
    [self.view layoutIfNeeded];
    
    self.consBtnTimeLeading.constant = 0.5f;
    self.consBtnPriceLeading.constant = 0.5f;
    
    if(IS_IPHONE_5 || IS_IPHONE_4_OR_LESS){
        
        [self.adjustRadiusBtn.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:17]];
        [self.priceRangeBtn.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:17]];
        [self.btnTime.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:17]];
        
        _adjustBtnHeight.constant=40;
        
        [_lblCategoryPlaceHolder setFont:[UIFont fontWithName:@"Lato-Regular" size:13]];
        _constraintCategoryViewHeight.constant=37;
        _viewTapHandlerHeight.constant=37;
        self.consCategoryTblViewTop.constant = 34;
        // self.consCategoryTblViewTop.constant = 48;
        _lblSeperatorCtgryBottom.constant=0;
        _lblPlaceHolderCatgryTop.constant=12;
        
        _arrowImgTop.constant=15;
        
        _viewMapHolderTop.constant=34;
        _radiusBtnFinalBottom.constant=58;
        
        _minTop.constant=12;
        _minTxtFldLeft.constant=30;
        _minTxtFldWidth.constant=118;
        _minTxtFldHeight.constant=28;
        _maxTxtFldLeft.constant=26;
        _maxTxtFldWidth.constant=118;
        _maxTxtFldHeight.constant=28;
        _minLeft.constant=31;
        _maxLeft.constant=24;
        _minTxtFldTop.constant=3;
        _maxTxtFldTop.constant=3;
        
        [_minLabel setFont:[UIFont systemFontOfSize:13.5]];
        [_maxLabel setFont:[UIFont systemFontOfSize:13.5]];
        
        _arrowImgRight.constant=11;
        // _blurViewHeight.constant=83;
        
        _adjustRadiusBtn.titleEdgeInsets=UIEdgeInsetsMake(-3, 0, 0, 0);
        _priceRangeBtn.titleEdgeInsets=UIEdgeInsetsMake(-3, 0, 0, 0);
        _btnTime.titleEdgeInsets=UIEdgeInsetsMake(-3, 0, 0, 0);
        
        
        self.filterCityViewBottom.constant = -36;
        self.filterCityViewWidth.constant = 119;
        self.filterCityViewLeadingFromMaxTxtFld.constant = -1;
        
    }
    else if(IS_IPHONE_6){
        
        self.filterCityViewBottom.constant = -41;
        
        
        [self.adjustRadiusBtn.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:19]];
        [self.priceRangeBtn.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:19]];
        [self.btnTime.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:19]];
        [_lblCategoryPlaceHolder setFont:[UIFont fontWithName:@"Lato-Regular" size:16]];
        _adjustBtnHeight.constant=44;
        // _blurViewHeight.constant=97;
        _minTop.constant=16;
        _minTxtFldLeft.constant=36;
        _minTxtFldWidth.constant=136;
        _minTxtFldHeight.constant=33;
        _maxTxtFldLeft.constant=32;
        _maxTxtFldWidth.constant=136;
        _maxTxtFldHeight.constant=33;
        _minLeft.constant=36;
        _maxLeft.constant=50;
        _minTxtFldTop.constant=5;
        _maxTxtFldTop.constant=5;
        _radiusBtnFinalBottom.constant=72;  // change 73 to 72
        
        _viewMapHolderTop.constant=34;
        _lblPlaceHolderCatgryTop.constant=10;
        
    }
    
    else if(IS_IPHONE_6P){
        
        self.filterCityViewBottom.constant = -46;
        self.filterCityViewWidth.constant = 152;
        self.filterCityViewLeadingFromMaxTxtFld.constant = -1;
        
        [self.adjustRadiusBtn.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:22]];
        [self.priceRangeBtn.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:22]];
        [self.btnTime.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:22]];
        [_lblCategoryPlaceHolder setFont:[UIFont fontWithName:@"Lato-Regular" size:18]];
        
        _adjustBtnHeight.constant=50;
        //   _blurViewHeight.constant=107;
        _minTop.constant=21;
        _radiusBtnFinalBottom.constant=76;
        
        _minTop.constant=18;
        
        _minTxtFldLeft.constant=41;
        _minTxtFldWidth.constant=150;
        _minTxtFldHeight.constant=37;
        
        
        _maxTxtFldLeft.constant=35;
        _maxTxtFldWidth.constant=150;
        _maxTxtFldHeight.constant=37;
        
        _minLeft.constant=39;
        _maxLeft.constant=67;
        
        _minTxtFldTop.constant=7;
        _maxTxtFldTop.constant=7;
        
        _constraintCategoryViewHeight.constant=48;
        _viewTapHandlerHeight.constant=48;
        self.consCategoryTblViewTop.constant = 48;
        
        
        
        _lblSeperatorCtgryBottom.constant=0;
        _lblPlaceHolderCatgryTop.constant=15;
        
        [_minLabel setFont:[UIFont systemFontOfSize:18]];
        [_maxLabel setFont:[UIFont systemFontOfSize:18]];
        _arrowImgTop.constant=22;
        
        
        
        
    }
    else if(IS_IPHONE_4_OR_LESS){
        [self.adjustRadiusBtn.titleLabel setFont:[UIFont systemFontOfSize:12.0]];
        [self.priceRangeBtn.titleLabel setFont:[UIFont systemFontOfSize:12.0]];
        _adjustBtnHeight.constant=44;
    }
    
    [self.view layoutIfNeeded];
}


-(void)viewDidAppear:(BOOL)animated{
    
    NSString* final = @"Reservations on map page";
    
    [Flurry logEvent:final];
    
    
    /*
    _adjustRadiusBtn.backgroundColor=[UIColor colorWithRed:63.0/255.0f green:67.0/255.0f blue:70.0/255.0f alpha:1.0];
    _btnTime.backgroundColor=[UIColor colorWithRed:63.0/255.0f green:67.0/255.0f blue:70.0/255.0f alpha:1.0];
    _priceRangeBtn.backgroundColor=[UIColor colorWithRed:63.0/255.0f green:67.0/255.0f blue:70.0/255.0f alpha:1.0];
    
    _filterView.backgroundColor=[UIColor colorWithRed:82.0/255.0f green:85.0/255.0f blue:88.0/255.0f alpha:1.0];
    
    _lblSeperatorCategory.backgroundColor=[UIColor colorWithRed:196.0/255.0f green:196.0/255.0f blue:196.0/255.0f alpha:1.0];
    
    _viewCategoryView.backgroundColor=[UIColor colorWithRed:244.0/255.0f green:244.0/255.0f blue:244.0/255.0f alpha:1.0];
    
    
    
    
    [self.view layoutIfNeeded];
    
    self.consBtnTimeLeading.constant = 0.5f;
    self.consBtnPriceLeading.constant = 0.5f;
    
    if(IS_IPHONE_5){
        
        [self.adjustRadiusBtn.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:17]];
        [self.priceRangeBtn.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:17]];
        [self.btnTime.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:17]];
        
        _adjustBtnHeight.constant=40;
        
        [_lblCategoryPlaceHolder setFont:[UIFont fontWithName:@"Lato-Regular" size:13]];
        _constraintCategoryViewHeight.constant=37;
        _viewTapHandlerHeight.constant=37;
        self.consCategoryTblViewTop.constant = 34;
       // self.consCategoryTblViewTop.constant = 48;
        _lblSeperatorCtgryBottom.constant=0;
        _lblPlaceHolderCatgryTop.constant=12;
        
        _arrowImgTop.constant=15;
        
        _viewMapHolderTop.constant=34;
        _radiusBtnFinalBottom.constant=58;
        
        _minTop.constant=12;
        _minTxtFldLeft.constant=30;
        _minTxtFldWidth.constant=118;
        _minTxtFldHeight.constant=28;
        _maxTxtFldLeft.constant=26;
        _maxTxtFldWidth.constant=118;
        _maxTxtFldHeight.constant=28;
        _minLeft.constant=31;
        _maxLeft.constant=24;
        _minTxtFldTop.constant=3;
        _maxTxtFldTop.constant=3;
        
        [_minLabel setFont:[UIFont systemFontOfSize:13.5]];
        [_maxLabel setFont:[UIFont systemFontOfSize:13.5]];
        
        _arrowImgRight.constant=11;
       // _blurViewHeight.constant=83;
        
        _adjustRadiusBtn.titleEdgeInsets=UIEdgeInsetsMake(-3, 0, 0, 0);
        _priceRangeBtn.titleEdgeInsets=UIEdgeInsetsMake(-3, 0, 0, 0);
        _btnTime.titleEdgeInsets=UIEdgeInsetsMake(-3, 0, 0, 0);
        
        
        self.filterCityViewBottom.constant = -36;
        self.filterCityViewWidth.constant = 119;
        self.filterCityViewLeadingFromMaxTxtFld.constant = -1;
        
    }
    else if(IS_IPHONE_6){
        
        self.filterCityViewBottom.constant = -41;
        
        
        [self.adjustRadiusBtn.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:19]];
        [self.priceRangeBtn.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:19]];
        [self.btnTime.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:19]];
        [_lblCategoryPlaceHolder setFont:[UIFont fontWithName:@"Lato-Regular" size:16]];
        _adjustBtnHeight.constant=44;
       // _blurViewHeight.constant=97;
        _minTop.constant=16;
        _minTxtFldLeft.constant=36;
        _minTxtFldWidth.constant=136;
        _minTxtFldHeight.constant=33;
        _maxTxtFldLeft.constant=32;
        _maxTxtFldWidth.constant=136;
        _maxTxtFldHeight.constant=33;
        _minLeft.constant=36;
        _maxLeft.constant=50;
        _minTxtFldTop.constant=5;
        _maxTxtFldTop.constant=5;
        _radiusBtnFinalBottom.constant=72;  // change 73 to 72
        
        _viewMapHolderTop.constant=34;
        _lblPlaceHolderCatgryTop.constant=10;
        
    }
    
    else if(IS_IPHONE_6P){
        
        self.filterCityViewBottom.constant = -46;
        self.filterCityViewWidth.constant = 152;
        self.filterCityViewLeadingFromMaxTxtFld.constant = -1;
        
        [self.adjustRadiusBtn.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:22]];
        [self.priceRangeBtn.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:22]];
        [self.btnTime.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:22]];
        [_lblCategoryPlaceHolder setFont:[UIFont fontWithName:@"Lato-Regular" size:18]];
        
        _adjustBtnHeight.constant=50;
     //   _blurViewHeight.constant=107;
        _minTop.constant=21;
        _radiusBtnFinalBottom.constant=76;
        
        _minTop.constant=18;
        
        _minTxtFldLeft.constant=41;
        _minTxtFldWidth.constant=150;
        _minTxtFldHeight.constant=37;
        
        
        _maxTxtFldLeft.constant=35;
        _maxTxtFldWidth.constant=150;
        _maxTxtFldHeight.constant=37;
        
        _minLeft.constant=39;
        _maxLeft.constant=67;
        
        _minTxtFldTop.constant=7;
        _maxTxtFldTop.constant=7;
        
        _constraintCategoryViewHeight.constant=48;
        _viewTapHandlerHeight.constant=48;
        self.consCategoryTblViewTop.constant = 48;
        
        
        
        _lblSeperatorCtgryBottom.constant=0;
        _lblPlaceHolderCatgryTop.constant=15;
        
        [_minLabel setFont:[UIFont systemFontOfSize:18]];
        [_maxLabel setFont:[UIFont systemFontOfSize:18]];
        _arrowImgTop.constant=22;
        
        
        
        
    }
    else if(IS_IPHONE_4_OR_LESS){
        [self.adjustRadiusBtn.titleLabel setFont:[UIFont systemFontOfSize:12.0]];
        [self.priceRangeBtn.titleLabel setFont:[UIFont systemFontOfSize:12.0]];
        _adjustBtnHeight.constant=44;
    }
    
    [self.view layoutIfNeeded];
    
     
     */
    
    if (!(myAppDelegate.arrayTaskDetail.count>0)) {
        
        [self.mapView removeAnnotations:[self.mapView annotations]];
        
    }else{
        
        arrayOfTaskWithStatus0 = [GlobalFunction filterTaskForUserID:myAppDelegate.userData.userid taskStatus:TMViewAllTask];
        
        if (arrayOfTaskWithStatus0.count>0) {
            
            [self addAllPins];
            
        }
        
    }
    
    
}

-(void)mapViewDidFinishLoadingMap:(MKMapView *)mapView{
    
    [GlobalFunction removeIndicatorView];
    
}

-(void)mapViewDidFailLoadingMap:(MKMapView *)mapView withError:(NSError *)error{
    
    [GlobalFunction removeIndicatorView];
    
    //    [[GlobalFunction shared] showAlertForMessage:[error localizedDescription]];
    
}

- (void)didReceiveMemoryWarning{
    [super didReceiveMemoryWarning];
}
-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex{
    
    
    if (alertView.tag == 1001)
    {
        if (buttonIndex == [alertView cancelButtonIndex]) {
            
            
        }else{
            
            [GlobalFunction addIndicatorView];
            
            [self addAllPins];
            
        }
        
        
    }
    
}



-(void)addAllPins{
    
    // text filter handling by vs
    
    if (enteredLocation.length<=0) {
        enteredLocation = @"";
    }
    if (minPriceValue.length<=0) {
        minPriceValue = @"";
    }
    if (maxPriceValue.length<=0) {
        maxPriceValue = @"";
    }
    if (rangeLocation.length<=0) {
        rangeLocation = @"";
    }
    
    
    filterPriceMin = minPriceValue;
    
    filterPriceMax = maxPriceValue;
    
    filterRange = rangeLocation;
    
    filterLocationName = enteredLocation;
   // text filter handling by vs
    
    
    //********* handling for carry filter value on list view....
    
    myAppDelegate.mainMiles = filterRange;
    myAppDelegate.mainLocation = filterLocationName;
    myAppDelegate.mainMinPrice = filterPriceMin;
    myAppDelegate.mainmaxPrice = filterPriceMax;
    myAppDelegate.mainStartDate =  dateStart;
    myAppDelegate.mainEndDate = dateEnd;
    
      //********* handling for carry filter value on list view....
    
    //// vs
    
    NSDateFormatter *formatter111 = [[NSDateFormatter alloc] init];
    [formatter111 setDateFormat:@"yyyy-MM-dd"];
    
    //// vs
    
  //  category *obj = [myAppDelegate.arrayCategoryDetail objectAtIndex:myAppDelegate.SelectedIndexPathForFilter.row-1];
  //  filterCategoryId=obj.categoryId;
    
    
    //old work
    if (annotationsArray == nil) {
        annotationsArray = [[NSMutableArray alloc]init];
    }
    
    [self.mapView removeAnnotations:[self.mapView annotations]];
    [annotationsArray removeAllObjects];
    
    if ([filterPriceMax isEqualToString:@""] && [filterPriceMin isEqualToString:@""] && [filterRange isEqualToString:@""] && [filterCategoryId isEqualToString:@""] && [filterLocationName isEqualToString:@""] && [filterSubCategoryId isEqualToString:@""] && dateStart == nil) {
        
        arrayOfTaskWithStatus0 = [GlobalFunction filterTaskForUserID:myAppDelegate.userData.userid taskStatus:TMViewAllTask];
        
    }else{
        
        arrayOfTaskWithStatus0 = [GlobalFunction filterTaskListOnBasisiOfCategoryId:filterCategoryId SubCategoryId:filterSubCategoryId range:filterRange location:filterLocationName priceMin:filterPriceMin priceMax:filterPriceMax startDate:[formatter111 stringFromDate: dateStart]  endDate:[formatter111 stringFromDate: dateEnd]];
        
        if (arrayOfTaskWithStatus0.count>0)
        {
           
                myAppDelegate.lastSearchLocation = enteredLocation;
                //enteredLocation = myAppDelegate.lastSearchLocation;
            
            
        }else
        {
            if (myAppDelegate.isLocationApiFail == true)
            {
            [GlobalFunction removeIndicatorView];
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"BUY TIMEE" message:@"It seems something went wrong, Please retry" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Retry", nil];
            
            alertView.tag = 1001;
            
            [alertView show];
            
            return;
            }
        }
        
    }
    
    for (task *obj in arrayOfTaskWithStatus0) {
        
        NSMutableArray *arrCoordinateStr = [[NSMutableArray alloc]init];
        
        [arrCoordinateStr addObject:[NSString stringWithFormat:@"%f, %f",obj.latitude,obj.longitude]];
        
        NSString *categoryNames;
        
        for (int i = 0; i<myAppDelegate.arrayCategoryDetail.count; i++) {
            
            if ([[myAppDelegate.arrayCategoryDetail objectAtIndex:i] isKindOfClass:[subCategory class]]){
                
                subCategory *objs = [myAppDelegate.arrayCategoryDetail objectAtIndex:i];
                
                if ([objs.subCategoryId isEqualToString:obj.subcategoryId]) {
                    
                    categoryNames = objs.name;
                    
                    break;
                }
                
            }
            
        }
        
//        
//        NSArray *components = [taskObj.picpath componentsSeparatedByString:@"/"];
//        NSString *imgNameWithJPEG = [components lastObject];
//        imgNameWithJPEG = [imgNameWithJPEG stringByAppendingString:@".jpeg"];
//        NSArray *componentsWithDots = [imgNameWithJPEG componentsSeparatedByString:@"."];
//        if (componentsWithDots.count>2) {
//            imgNameWithJPEG = [NSString stringWithFormat:@"%@.%@",[componentsWithDots firstObject],[componentsWithDots lastObject]];
//        
        
        
        
        NSLog(@"pic path in map view is..%@",obj.picpath);
        
        NSArray *components = [obj.picpath componentsSeparatedByString:@"/"];
        
        NSString *imgNameWithJPEG = [components lastObject];
       
        imgNameWithJPEG = [imgNameWithJPEG stringByAppendingString:@".jpeg"];
        
        NSArray *componentsWithDots = [imgNameWithJPEG componentsSeparatedByString:@"."];
        
        if (componentsWithDots.count>2) {
            
            imgNameWithJPEG = [NSString stringWithFormat:@"%@.%@",[componentsWithDots firstObject],[componentsWithDots lastObject]];
            
        }
        
        UIImage *img = [GlobalFunction loadImageOfLogoFromCacheFolder:imgNameWithJPEG dirname:@"user"];
        
        if (img) {
            [annotationsArray addObject:[self addPinWithTitle:obj.title address:obj.locationName subtitle:categoryNames AndCoordinate:arrCoordinateStr[0] taskId:[obj.taskid intValue] userImage:img]];
        }else{
            [annotationsArray addObject:[self addPinWithTitle:obj.title address:obj.locationName subtitle:categoryNames AndCoordinate:arrCoordinateStr[0] taskId:[obj.taskid intValue] userImage:imageUserDefault]];
        }
        
    }
    
    [self mutateCoordinatesOfClashingAnnotations:annotationsArray]; //for showing multiple pins at different locations if all coordinates are same.
    
    [self.mapView addAnnotations:annotationsArray];
    
}

-(void)filterList{
    
    [self addAllPins];
    
    [self performSelector:@selector(removeIndicator) withObject:nil afterDelay:0.2];
    
}

-(void)removeIndicator{
    
    [GlobalFunction removeIndicatorView];
    
}

- (void)mutateCoordinatesOfClashingAnnotations:(NSArray *)annotations {
    
    NSDictionary *coordinateValuesToAnnotations = [self groupAnnotationsByLocationValue:annotations];
    
    for (NSValue *coordinateValue in coordinateValuesToAnnotations.allKeys) {
        NSMutableArray *outletsAtLocation = coordinateValuesToAnnotations[coordinateValue];
        if (outletsAtLocation.count > 1) {
            CLLocationCoordinate2D coordinate;
            [coordinateValue getValue:&coordinate];
            [self repositionAnnotations:outletsAtLocation toAvoidClashAtCoordination:coordinate];
        }
    }
}

- (NSDictionary *)groupAnnotationsByLocationValue:(NSArray *)annotations {
    
    NSMutableDictionary *result = [NSMutableDictionary dictionary];
    
    for (id<MKAnnotation> pin in annotations) {
        
        CLLocationCoordinate2D coordinate = pin.coordinate;
        NSValue *coordinateValue = [NSValue valueWithBytes:&coordinate objCType:@encode(CLLocationCoordinate2D)];
        
        NSMutableArray *annotationsAtLocation = result[coordinateValue];
        if (!annotationsAtLocation) {
            annotationsAtLocation = [NSMutableArray array];
            result[coordinateValue] = annotationsAtLocation;
        }
        
        [annotationsAtLocation addObject:pin];
    }
    return result;
}


- (void)repositionAnnotations:(NSMutableArray *)annotations toAvoidClashAtCoordination:(CLLocationCoordinate2D)coordinate {
    
    double distance = 4 * annotations.count / 2.0;
    double radiansBetweenAnnotations = (M_PI * 2) / annotations.count;
    
    MyAnnotation *annotationMy = [[MyAnnotation alloc] initWithName:@"" address:@"" subtitle:@"" userImage:nil coordinate:coordinate andTag:-1];
    [annotationsArray addObject:annotationMy];
    
    for (int i = 0; i < annotations.count; i++) {
        
        double heading = radiansBetweenAnnotations * i;
        CLLocationCoordinate2D newCoordinate = [self calculateCoordinateFrom:coordinate onBearing:heading atDistance:distance];
        
        CLLocationCoordinate2D coordinateArray[2];
        coordinateArray[0] = CLLocationCoordinate2DMake(coordinate.latitude, coordinate.longitude);
        coordinateArray[1] = CLLocationCoordinate2DMake(newCoordinate.latitude, newCoordinate.longitude);
        
        self.routeLine = [MKPolyline polylineWithCoordinates:coordinateArray count:2];
        //        [self.mapView setVisibleMapRect:[self.routeLine boundingMapRect]]; //If you want the route to be visible
        
        [self.mapView addOverlay:self.routeLine];
        
        MyAnnotation *annotation = annotations[i];
        
        annotation.theCoordinate = newCoordinate;
    }
}

-(MKOverlayRenderer *)mapView:(MKMapView *)mapView rendererForOverlay:(id<MKOverlay>)overlay{
    if ([overlay isKindOfClass:[MKPolyline class]]){
        MKPolylineRenderer *pr = [[MKPolylineRenderer alloc] initWithPolyline:overlay];
        pr.strokeColor = [UIColor blackColor];
        pr.lineWidth = 1;
        return pr;
    }
    
    return nil;
}


- (CLLocationCoordinate2D)calculateCoordinateFrom:(CLLocationCoordinate2D)coordinate  onBearing:(double)bearingInRadians atDistance:(double)distanceInMetres {
    
    double coordinateLatitudeInRadians = coordinate.latitude * M_PI / 180;
    double coordinateLongitudeInRadians = coordinate.longitude * M_PI / 180;
    
    double distanceComparedToEarth = distanceInMetres / 6378100;
    
    double resultLatitudeInRadians = asin(sin(coordinateLatitudeInRadians) * cos(distanceComparedToEarth) + cos(coordinateLatitudeInRadians) * sin(distanceComparedToEarth) * cos(bearingInRadians));
    double resultLongitudeInRadians = coordinateLongitudeInRadians + atan2(sin(bearingInRadians) * sin(distanceComparedToEarth) * cos(coordinateLatitudeInRadians), cos(distanceComparedToEarth) - sin(coordinateLatitudeInRadians) * sin(resultLatitudeInRadians));
    
    CLLocationCoordinate2D result;
    result.latitude = resultLatitudeInRadians * 180 / M_PI;
    result.longitude = resultLongitudeInRadians * 180 / M_PI;
    return result;
}


-(MyAnnotation *)addPinWithTitle:(NSString *)title address:(NSString *)address subtitle:(NSString *)subtitle AndCoordinate:(NSString *)strCoordinate taskId:(int)taskId userImage:(UIImage *)userImage{
    
    // clear out any white space
    strCoordinate = [strCoordinate stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    // convert string into actual latitude and longitude values
    NSArray *components = [strCoordinate componentsSeparatedByString:@","];
    
    double latitude = [components[0] doubleValue];
    double longitude = [components[1] doubleValue];
    
    // setup the map pin with all data and add to map view
    CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(latitude, longitude);
    
    CLLocationCoordinate2D zoomLocation;
    zoomLocation.latitude = coordinate.latitude; // 39.281516;
    zoomLocation.longitude= coordinate.longitude; // -76.580806;
    // 2
    //    MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(zoomLocation, 0.0000001*METERS_PER_MILE, 0.0000001*METERS_PER_MILE);   //for max zoom
    
     MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(zoomLocation, 1000, 1000);   //for min zoom
    // 3
    [self.mapView setRegion:viewRegion animated:YES];
    
    MyAnnotation *annotation = [[MyAnnotation alloc] initWithName:title address:address subtitle:subtitle userImage:userImage coordinate:coordinate andTag:taskId];
    
    return annotation;
    
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation {
    
    if ([annotation isKindOfClass:[MyAnnotation class]]) {
        
        MyAnnotation *annot = annotation;
        
        task *objTask = [[task alloc]init];
        objTask=nil;
        for (task *objs in arrayOfTaskWithStatus0) {
            
            if ([objs.taskid intValue] == annot.tagTaskId) {
                
                objTask = objs;
                
                break;
            }
            
        }
        
        if (objTask==nil) {
            return nil;
        }
        //         NSLog(@"date string is from db....%@ ---- %@",objTask.startDate, objTask.locationName);
        //        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
        //        [dateFormatter setDateFormat:dateFormatFull];
        //        NSDate *date = [dateFormatter dateFromString:objTask.startDate];
        //        [dateFormatter setDateFormat:dateFormatDate];
        //        NSString *dateString = [dateFormatter stringFromDate:date];
        
        //        First create your pinview and calloutview
        UIView *pinView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"location"]];
        //        Then you create your custom annotation view as shown below.
        
        DXAnnotationView *annotationView = (DXAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:NSStringFromClass([DXAnnotationView class])];
        
        annotationView = nil;
        
        DXAnnotationSettings *newSettings = [[DXAnnotationSettings alloc] init];
        
        newSettings.calloutOffset = 5.0;
        
        newSettings.shouldRoundifyCallout = YES;
        newSettings.calloutCornerRadius = 3.0;
        
        newSettings.shouldAddCalloutBorder = NO;
        
        newSettings.animationType = DXCalloutAnimationNone;
        
        MapAnnotationViewController *calloutView = [[[NSBundle mainBundle] loadNibNamed:@"MapAnnotationViewController" owner:self options:nil] firstObject];
        
        calloutView.view.frame = CGRectMake(0, 0, 260, 80);
        if (annot.tagTaskId == -1) {
            calloutView.view.frame = CGRectMake(0, 0, 0, 0);
        }
        
        calloutView.view.layer.cornerRadius = 15.0;
        calloutView.view.layer.masksToBounds = YES;
        
        calloutView.lblUserAddress.text = objTask.locationName;
        calloutView.lblUserName.text = objTask.title;
        /////
        
        
        NSLog(@"date string is from db....%@ ---- %@",objTask.startDate, objTask.endDate);
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
        [dateFormatter setDateFormat:dateFormatFull];
        
        NSString *dateString = @"";
        NSString *timeString = @"";
        
        if ([objTask.startDate isEqualToString:@"AnyDate"])
        {
            dateString = @"AnyDate";
        }
        else
        {
            NSDate *date = [dateFormatter dateFromString:objTask.startDate];
            [dateFormatter setDateFormat:dateFormatDate];
            dateString = [dateFormatter stringFromDate:date];
            
            dateString = [GlobalFunction convertDateFormat:dateString];
        }
        
        if ([objTask.endDate isEqualToString:@"AnyTime"])
        {
            timeString = @"AnyTime";
        }
        else
        {
            timeString = [GlobalFunction convertTimeFormat:objTask.endDate];
        }
        
        NSLog(@"Date string is....%@",dateString);
        NSLog(@"time string is....%@",timeString);
        
        
        NSLog(@"time string is....%@",timeString);
        NSLog(@"task id is....%@",objTask.taskid);
        NSLog(@"date string is....%@",dateString);
        
        
        NSString *combineString = dateString;
        NSString *str = [@[dateString, timeString] componentsJoinedByString:@", "];
        
        NSLog(@"combine string is....%@",str);
        ////
        calloutView.lblUserUsername.text = str;
        calloutView.imgViewUser.image = annot.imgUser;
        calloutView.imgViewUser.layer.cornerRadius = calloutView.imgViewUser.frame.size.height/2;
        calloutView.imgViewUser.layer.masksToBounds = YES;
        calloutView.view.tag = annot.tagTaskId;
        
        if ([calloutView.lblUserAddress.text isEqualToString:@""] && [calloutView.lblUserUsername.text isEqualToString:@""]) {
            calloutView.lblCenter.hidden = YES;
        }
        
        if (!annotationView) {
            annotationView = [[DXAnnotationView alloc] initWithAnnotation:annotation
                                                          reuseIdentifier:NSStringFromClass([DXAnnotationView class])
                                                                  pinView:pinView
                                                              calloutView:calloutView.view
                                                                 settings:newSettings];
        }
        
        annotationView.tag = annot.tagTaskId;
        
        UITapGestureRecognizer *pinTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(pinTapped:)];
        [calloutView.view addGestureRecognizer:pinTap];
        
        return annotationView;
        
    }
    
    return nil;
}

- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view{
    
    if([view isKindOfClass:[DXAnnotationView class]]){
        
        [((DXAnnotationView *)view)showCalloutView];
        view.layer.zPosition=0;
        
        if (!prevDXAnnotationView) {
            prevDXAnnotationView = (DXAnnotationView *)view;
        }else{
            [prevDXAnnotationView hideCalloutView];
            prevDXAnnotationView.layer.zPosition=-1;
            
            prevDXAnnotationView = (DXAnnotationView *)view;
        }
        
    }
    
}



-(void)mapView:(MKMapView *)mapView didDeselectAnnotationView:(MKAnnotationView *)view{
    
    if([view isKindOfClass:[DXAnnotationView class]]){
        [((DXAnnotationView *)view)hideCalloutView];
        view.layer.zPosition=-1;
        
        prevDXAnnotationView = nil;
    }
    
}

-(void)pinTapped:(id)sender{
    
    myAppDelegate.TandC_Buyee=@"home";

    
    for (task *objs in arrayOfTaskWithStatus0) {
        
        if ([objs.taskid intValue] == [[sender view] tag]) {
            
            myAppDelegate.taskData = nil;
            myAppDelegate.taskData = objs;
            
            indexPathForWorkerTaskData = [NSIndexPath indexPathForRow:[arrayOfTaskWithStatus0 indexOfObject:objs] inSection:0];
            
            break;
        }
        
    }
    
    [GlobalFunction addIndicatorView];
    
    [self performSelector:@selector(fetchTaskInfo) withObject:nil afterDelay:0.001];
    
    [self checkSelectedBtns];
}

-(void)fetchTaskInfo{
    
    if (![[WebService shared] connected]) {
        
        [GlobalFunction removeIndicatorView];
        
        [[GlobalFunction shared] showAlertForMessage:@"Can't connect to internet, please check and try again."];
        
        return;
    }
    
   
    
    NSString *timeZoneSeconds = [GlobalFunction getTimeZone];
    
    NSDictionary *dictTaskInfoResponse = [[WebService shared] viewTaskInfoTaskid:myAppDelegate.taskData.taskid userid:myAppDelegate.userData.userid timeZone:timeZoneSeconds view:@""];
    
    if (dictTaskInfoResponse) {
        
        NSLog(@"response message == %@",[EncryptDecryptFile decrypt:[dictTaskInfoResponse valueForKey:@"responseMessage"]]);
        
        if ([[EncryptDecryptFile decrypt:[dictTaskInfoResponse valueForKey:@"responseCode"]] isEqualToString:@"200"]) {
            
            NSDictionary *dictDataReg = [dictTaskInfoResponse objectForKey:@"data"];
            
            if (dictDataReg != nil) {
                
                if (myAppDelegate.isOfOtherUser) {
                    
                    myAppDelegate.workerOtherUserTaskDetail = nil;
                    
                    myAppDelegate.workerOtherUserTaskDetail = [[WorkerEmployerTaskDetail alloc]init];
                    
                    [self fillWorkerTaskDataForWorkerTaskDataObj:myAppDelegate.workerOtherUserTaskDetail fromDictionary:dictDataReg andUserData:myAppDelegate.userOtherData];
                    
                }else{
                    
                    myAppDelegate.workerAllTaskDetail = nil;
                    
                    myAppDelegate.workerAllTaskDetail = [[WorkerEmployerTaskDetail alloc] init];
                    
                    [self fillWorkerTaskDataForWorkerTaskDataObj:myAppDelegate.workerAllTaskDetail fromDictionary:dictDataReg andUserData:myAppDelegate.userData];
                    
                }
                
                [self performSelector:@selector(performSegueForTaskDetail) withObject:nil afterDelay:0.5];
                
            }else{
                
                [GlobalFunction removeIndicatorView];
                
                [[GlobalFunction shared] showAlertForMessage:[EncryptDecryptFile decrypt:[dictTaskInfoResponse valueForKey:@"responseMessage"]]];
                
            }
            
        }else{
            
            [GlobalFunction removeIndicatorView];
            
            [[GlobalFunction shared] showAlertForMessage:[EncryptDecryptFile decrypt:[dictTaskInfoResponse valueForKey:@"responseMessage"]]];
            
        }
        
    }else{
        
        [GlobalFunction removeIndicatorView];
        
        [[GlobalFunction shared] showAlertForMessage:@"Server is down, please try again later."];
        
    }
    
}


-(void)performSegueForTaskDetail{
    
    myAppDelegate.viewAllTaskVCObj.isFromMapView = YES;
    
    myAppDelegate.isSideBarAccesible = false;
    
    TaskDetailTableViewCell *cell = [myAppDelegate.viewAllTaskVCObj.tableTaskListing cellForRowAtIndexPath:indexPathForWorkerTaskData];
    [myAppDelegate.viewAllTaskVCObj performSegueWithIdentifier:@"newTaskDetail" sender:cell];
    
    indexPathForWorkerTaskData = nil;
    
}




-(void)fillWorkerTaskDataForWorkerTaskDataObj:(WorkerEmployerTaskDetail *)obj fromDictionary:(NSDictionary *)dict andUserData:(user *)userObj{
    
    
    NSDictionary *dictTemp0 =nil;
    
   // NSString *mainn = [EncryptDecryptFile decrypt:[dict valueForKey:@"Accepted By"]];
    
    dictTemp0= [dict objectForKey:@"Accepted By"];
    
    
    //
    
//    if (dictTemp0==nil) {
//        obj.isTaskAccepted = false;
//    }else{
//        obj.isTaskAccepted = true;
//        obj.userId_acceptedBy = [EncryptDecryptFile decrypt:[dictTemp0 valueForKey:@"userId"]];
//        obj.emailId_acceptedBy = [EncryptDecryptFile decrypt:[dictTemp0 valueForKey:@"emailId"]];
//        obj.picPath_acceptedBy = [EncryptDecryptFile decrypt:[dictTemp0 valueForKey:@"picPath"]];
//    }
    
    
    NSDictionary *dictTemp3 =nil;
    dictTemp3= [dict objectForKey:@"Reservation Detail"];
    
    if (dictTemp3==nil) {
        
    }else{
        
        
        obj.taskDetailObj = [[task alloc]init];
        
        obj.taskDetailObj.userid = [EncryptDecryptFile decrypt:[dictTemp3 valueForKey:@"userId"]];
        obj.taskDetailObj.categoryId = [EncryptDecryptFile decrypt:[dictTemp3 valueForKey:@"categoryId"]];
        obj.taskDetailObj.paymentMethod = [EncryptDecryptFile decrypt:[dictTemp3 valueForKey:@"paymentMethod"]];
        
        obj.taskDetailObj.title = [EncryptDecryptFile decrypt:[dictTemp3 valueForKey:@"title"]];
        obj.taskDetailObj.taskid = [EncryptDecryptFile decrypt:[dictTemp3 valueForKey:@"reservationId"]];
        //obj.taskDetailObj.subcategoryId = [EncryptDecryptFile decrypt:[dictTemp3 valueForKey:@"subcategoryId"]];
        obj.taskDetailObj.status = [EncryptDecryptFile decrypt:[dictTemp3 valueForKey:@"status"]];
        obj.taskDetailObj.startDate = [EncryptDecryptFile decrypt:[dictTemp3 valueForKey:@"startDate"]];
        obj.taskDetailObj.price = [[EncryptDecryptFile decrypt:[dictTemp3 valueForKey:@"actualPrice"]] floatValue];
        obj.taskDetailObj.longitude = [[EncryptDecryptFile decrypt:[dictTemp3 valueForKey:@"longitude"]] floatValue];
        obj.taskDetailObj.locationName = [EncryptDecryptFile decrypt:[dictTemp3 valueForKey:@"locationName"]];
        obj.taskDetailObj.latitude = [[EncryptDecryptFile decrypt:[dictTemp3 valueForKey:@"latitude"]] floatValue];
        obj.taskDetailObj.endDate = [EncryptDecryptFile decrypt:[dictTemp3 valueForKey:@"startTime"]];
        obj.taskDetailObj.taskDescription = [EncryptDecryptFile decrypt:[dictTemp3 valueForKey:@"description"]];
        obj.taskDetailObj.lastModifyTime = [EncryptDecryptFile decrypt:[dictTemp3 valueForKey:@"lastModifyTime"]];
        obj.taskDetailObj.discountValue = [EncryptDecryptFile decrypt:[dictTemp3 valueForKey:@"discount"]];
        obj.taskDetailObj.discountDiscription = [EncryptDecryptFile decrypt:[dictTemp3 valueForKey:@"discountDescription"]];
        obj.taskDetailObj.soldStatus = [EncryptDecryptFile decrypt:[dictTemp3 valueForKey:@"soldStatus"]];
        obj.taskDetailObj.feeValue = [EncryptDecryptFile decrypt:[dictTemp3 valueForKey:@"fee"]];
        obj.taskDetailObj.taskImagesArray = [[NSMutableArray alloc] init];
        
        
//        NSArray *newArray= [dict objectForKey:@"reservation_images"];
//        for (NSDictionary *dicts in newArray)
//        {
//            
//             NSLog(@"val....%@",[EncryptDecryptFile decrypt:[dicts valueForKey:@"reservationImg"]]);
//            
//            [obj.taskDetailObj.taskImagesArray addObject:[EncryptDecryptFile decrypt:[dicts valueForKey:@"reservationImg"]]];
//            
//        }
        
        
        NSArray *newArray= [dict objectForKey:@"reservation_images"];
        for (NSDictionary *dicts in newArray) {
            
            NSString *imageName=[EncryptDecryptFile decrypt:[dicts valueForKey:@"reservationImg"]];
            
            if(imageName.length<=0)
            {
                
            }
            else
            {
                [obj.taskDetailObj.taskImagesArray addObject:[EncryptDecryptFile decrypt:[dicts valueForKey:@"reservationImg"]]];
            }
            
            
        }

        
        
    }
    
    
    
    NSDictionary *dictTemp2 =nil;
    dictTemp2= [dict objectForKey:@"Created By"];
    if (dictTemp2==nil) {
        
    }else{
        obj.userId_createdBy = [EncryptDecryptFile decrypt:[dictTemp2 valueForKey:@"userId"]];
        obj.emailId_createdBy = [EncryptDecryptFile decrypt:[dictTemp2 valueForKey:@"emailId"]];
        obj.picPath_createdBy = [EncryptDecryptFile decrypt:[dictTemp2 valueForKey:@"picPath"]];
        obj.avg_rating_createdBy = [EncryptDecryptFile decrypt:[dictTemp2 valueForKey:@"avgRating"]];
        obj.name_createdBy = [EncryptDecryptFile decrypt:[dictTemp2 valueForKey:@"name"]];
    }
    
    NSDictionary *dictTemp5 =nil;
    dictTemp5= [dict objectForKey:@"Business"];
    if (dictTemp5==nil) {
        
    }else
    {
        
        obj.name_Business = [EncryptDecryptFile decrypt:[dictTemp5 valueForKey:@"name"]];
        obj.address_Business = [EncryptDecryptFile decrypt:[dictTemp5 valueForKey:@"address"]];
        obj.yelpProfile_Business = [EncryptDecryptFile decrypt:[dictTemp5 valueForKey:@"yelpProfile"]];
        obj.services_Business = [EncryptDecryptFile decrypt:[dictTemp5 valueForKey:@"services"]];
        obj.instaLink_Business = [EncryptDecryptFile decrypt:[dictTemp5 valueForKey:@"instaLink"]];
        
    }
    
    
    return;
    
    
    NSArray *arrayTerms= [dict valueForKey:@"Accepted By"];
    
    if ([[arrayTerms objectAtIndex:0] isKindOfClass:[NSString class]]) {
        
        obj.isTaskAccepted = false;
        
    }else if ([[arrayTerms objectAtIndex:0] isKindOfClass:[NSDictionary class]]){
        
        obj.isTaskAccepted = true;
        
        NSDictionary *dictTemp0=[arrayTerms objectAtIndex:0];
        
        obj.userId_acceptedBy = [EncryptDecryptFile decrypt:[dictTemp0 valueForKey:@"userId"]];
        obj.userName_acceptedBy = [EncryptDecryptFile decrypt:[dictTemp0 valueForKey:@"userName"]];
        obj.description_acceptedBy = [EncryptDecryptFile decrypt:[dictTemp0 valueForKey:@"description"]];
        obj.emailId_acceptedBy = [EncryptDecryptFile decrypt:[dictTemp0 valueForKey:@"emailId"]];
        obj.latitude_acceptedBy = [[EncryptDecryptFile decrypt:[dictTemp0 valueForKey:@"latitude"]] floatValue];
        obj.longitude_acceptedBy = [[EncryptDecryptFile decrypt:[dictTemp0 valueForKey:@"longitude"]] floatValue];
        obj.locationName_acceptedBy = [EncryptDecryptFile decrypt:[dictTemp0 valueForKey:@"location"]];
        obj.mobile_acceptedBy = [EncryptDecryptFile decrypt:[dictTemp0 valueForKey:@"mobile"]];
        obj.picPath_acceptedBy = [EncryptDecryptFile decrypt:[dictTemp0 valueForKey:@"picPath"]];
        obj.avg_rating_acceptedBy = [EncryptDecryptFile decrypt:[dictTemp0 valueForKey:@"avg_rating"]];
        
    }
    
    //task accepted by rating detail
    obj.arrayRatings_acceptedBy = [[NSMutableArray alloc]init];
    arrayTerms= [dict valueForKey:@"Worker Rating Detail"];
    if ([[arrayTerms objectAtIndex:0] isKindOfClass:[NSString class]]) {
        
        obj.rating_acceptedBy = [EncryptDecryptFile decrypt:[arrayTerms objectAtIndex:0]];
        
    }else if ([[arrayTerms objectAtIndex:0] isKindOfClass:[NSDictionary class]]){
        
        NSInteger noOfStars = 0;
        for (NSDictionary *dicts in arrayTerms) {
            
            NSString *stars = [EncryptDecryptFile decrypt:[dicts valueForKey:@"ratingStar"]];
            noOfStars = noOfStars + [stars integerValue];
            
            obj.ratingObj = nil;
            obj.ratingObj = [[rating alloc]init];
            
            obj.ratingObj.ratingdescription = [EncryptDecryptFile decrypt:[dicts valueForKey:@"description"]];
            obj.ratingObj.employerId = [EncryptDecryptFile decrypt:[dicts valueForKey:@"employerId"]];
            obj.ratingObj.ratingId = [EncryptDecryptFile decrypt:[dicts valueForKey:@"ratingId"]];
            obj.ratingObj.ratingStar = [EncryptDecryptFile decrypt:[dicts valueForKey:@"ratingStar"]];
            obj.ratingObj.taskId = [EncryptDecryptFile decrypt:[dicts valueForKey:@"taskId"]];
            obj.ratingObj.workerId = [EncryptDecryptFile decrypt:[dicts valueForKey:@"workerId"]];
            obj.ratingObj.userName = [EncryptDecryptFile decrypt:[dicts valueForKey:@"userName"]];
            
            [obj.arrayRatings_acceptedBy addObject:obj.ratingObj];
        }
        CGFloat avg = noOfStars/arrayTerms.count;
        obj.avgStars_acceptedBy = avg;
    }else if ([[arrayTerms objectAtIndex:0] isKindOfClass:[NSArray class]]){
        
        NSArray *arrRatings = [arrayTerms objectAtIndex:0];
        
        NSInteger noOfStars = 0;
        
        for (NSDictionary *dicts in arrRatings) {
            
            NSString *stars = [EncryptDecryptFile decrypt:[dicts valueForKey:@"ratingStar"]];
            noOfStars = noOfStars + [stars integerValue];
            
            obj.ratingObj = nil;
            obj.ratingObj = [[rating alloc]init];
            
            obj.ratingObj.ratingdescription = [EncryptDecryptFile decrypt:[dicts valueForKey:@"description"]];
            obj.ratingObj.employerId = [EncryptDecryptFile decrypt:[dicts valueForKey:@"employerId"]];
            obj.ratingObj.ratingId = [EncryptDecryptFile decrypt:[dicts valueForKey:@"ratingId"]];
            obj.ratingObj.ratingStar = [EncryptDecryptFile decrypt:[dicts valueForKey:@"ratingStar"]];
            obj.ratingObj.taskId = [EncryptDecryptFile decrypt:[dicts valueForKey:@"taskId"]];
            obj.ratingObj.workerId = [EncryptDecryptFile decrypt:[dicts valueForKey:@"workerId"]];
            obj.ratingObj.userName = [EncryptDecryptFile decrypt:[dicts valueForKey:@"userName"]];
            
            [obj.arrayRatings_acceptedBy addObject:obj.ratingObj];
        }
        
        CGFloat avg = noOfStars/arrayTerms.count;
        obj.avgStars_acceptedBy = avg;
        
    }
    
    //task created by rating detail
    obj.arrayRatings_createdBy = [[NSMutableArray alloc]init];
    arrayTerms= [dict valueForKey:@"Created By Rating Detail"];
    
    if ([[arrayTerms objectAtIndex:0] isKindOfClass:[NSString class]]) {
        
        obj.rating_createdBy = [EncryptDecryptFile decrypt:[arrayTerms objectAtIndex:0]];
        
    }else if ([[arrayTerms objectAtIndex:0] isKindOfClass:[NSDictionary class]]){
        
        NSInteger noOfStars = 0;
        for (NSDictionary *dicts in arrayTerms) {
            
            NSString *stars = [EncryptDecryptFile decrypt:[dicts valueForKey:@"ratingStar"]];
            noOfStars = noOfStars + [stars integerValue];
            
            obj.ratingObj = nil;
            obj.ratingObj = [[rating alloc]init];
            
            obj.ratingObj.ratingdescription = [EncryptDecryptFile decrypt:[dicts valueForKey:@"description"]];
            obj.ratingObj.employerId = [EncryptDecryptFile decrypt:[dicts valueForKey:@"employerId"]];
            obj.ratingObj.ratingId = [EncryptDecryptFile decrypt:[dicts valueForKey:@"ratingId"]];
            obj.ratingObj.ratingStar = [EncryptDecryptFile decrypt:[dicts valueForKey:@"ratingStar"]];
            obj.ratingObj.taskId = [EncryptDecryptFile decrypt:[dicts valueForKey:@"taskId"]];
            obj.ratingObj.workerId = [EncryptDecryptFile decrypt:[dicts valueForKey:@"workerId"]];
            obj.ratingObj.userName = [EncryptDecryptFile decrypt:[dicts valueForKey:@"userName"]];
            
            [obj.arrayRatings_createdBy addObject:obj.ratingObj];
        }
        
        CGFloat avg = noOfStars/arrayTerms.count;
        obj.avgStars_createdBy = avg;
        
    }else if ([[arrayTerms objectAtIndex:0] isKindOfClass:[NSArray class]]){
        
        NSArray *arrRatings = [arrayTerms objectAtIndex:0];
        
        NSInteger noOfStars = 0;
        
        for (NSDictionary *dicts in arrRatings) {
            
            NSString *stars = [EncryptDecryptFile decrypt:[dicts valueForKey:@"ratingStar"]];
            noOfStars = noOfStars + [stars integerValue];
            
            obj.ratingObj = nil;
            obj.ratingObj = [[rating alloc]init];
            
            obj.ratingObj.ratingdescription = [EncryptDecryptFile decrypt:[dicts valueForKey:@"description"]];
            obj.ratingObj.employerId = [EncryptDecryptFile decrypt:[dicts valueForKey:@"employerId"]];
            obj.ratingObj.ratingId = [EncryptDecryptFile decrypt:[dicts valueForKey:@"ratingId"]];
            obj.ratingObj.ratingStar = [EncryptDecryptFile decrypt:[dicts valueForKey:@"ratingStar"]];
            obj.ratingObj.taskId = [EncryptDecryptFile decrypt:[dicts valueForKey:@"taskId"]];
            obj.ratingObj.workerId = [EncryptDecryptFile decrypt:[dicts valueForKey:@"workerId"]];
            obj.ratingObj.userName = [EncryptDecryptFile decrypt:[dicts valueForKey:@"userName"]];
            
            [obj.arrayRatings_createdBy addObject:obj.ratingObj];
        }
        
        CGFloat avg = noOfStars/arrayTerms.count;
        obj.avgStars_createdBy = avg;
        
    }
    
    
    arrayTerms= [dict valueForKey:@"Card Detail"];
    if ([[arrayTerms objectAtIndex:0] isKindOfClass:[NSString class]]) {
        obj.cardDetail = [EncryptDecryptFile decrypt:[arrayTerms objectAtIndex:0]];
    }else if ([[arrayTerms objectAtIndex:0] isKindOfClass:[NSDictionary class]]){
        
    }else if ([[arrayTerms objectAtIndex:0] isKindOfClass:[NSArray class]]){
        obj.cardDetail = [EncryptDecryptFile decrypt:[[arrayTerms objectAtIndex:0] objectAtIndex:0]];
    }
    
    arrayTerms= [dict valueForKey:@"Created By"];
    dictTemp2=[arrayTerms objectAtIndex:0];
    
    obj.userId_createdBy = [EncryptDecryptFile decrypt:[dictTemp2 valueForKey:@"userId"]];
    obj.userName_createdBy = [EncryptDecryptFile decrypt:[dictTemp2 valueForKey:@"userName"]];
    obj.description_createdBy = [EncryptDecryptFile decrypt:[dictTemp2 valueForKey:@"description"]];
    obj.emailId_createdBy = [EncryptDecryptFile decrypt:[dictTemp2 valueForKey:@"emailId"]];
    obj.latitude_createdBy = [[EncryptDecryptFile decrypt:[dictTemp2 valueForKey:@"latitude"]] floatValue];
    obj.longitude_createdBy = [[EncryptDecryptFile decrypt:[dictTemp2 valueForKey:@"longitude"]] floatValue];
    obj.locationName_createdBy = [EncryptDecryptFile decrypt:[dictTemp2 valueForKey:@"location"]];
    obj.mobile_createdBy = [EncryptDecryptFile decrypt:[dictTemp2 valueForKey:@"mobile"]];
    obj.picPath_createdBy = [EncryptDecryptFile decrypt:[dictTemp2 valueForKey:@"picPath"]];
    obj.avg_rating_createdBy = [EncryptDecryptFile decrypt:[dictTemp2 valueForKey:@"avg_rating"]];
    
    arrayTerms= [dict valueForKey:@"Task Detail"];
    dictTemp3=[arrayTerms objectAtIndex:0];
    
    obj.taskDetailObj = [[task alloc]init];
    
    obj.taskDetailObj.userid = [EncryptDecryptFile decrypt:[dictTemp3 valueForKey:@"userId"]];
    obj.taskDetailObj.title = [EncryptDecryptFile decrypt:[dictTemp3 valueForKey:@"title"]];
    obj.taskDetailObj.taskid = [EncryptDecryptFile decrypt:[dictTemp3 valueForKey:@"taskId"]];
    obj.taskDetailObj.subcategoryId = [EncryptDecryptFile decrypt:[dictTemp3 valueForKey:@"subcategoryId"]];
    obj.taskDetailObj.status = [EncryptDecryptFile decrypt:[dictTemp3 valueForKey:@"status"]];
    obj.taskDetailObj.startDate = [EncryptDecryptFile decrypt:[dictTemp3 valueForKey:@"startDate"]];
    obj.taskDetailObj.price = [[EncryptDecryptFile decrypt:[dictTemp3 valueForKey:@"price"]] floatValue];
    obj.taskDetailObj.longitude = [[EncryptDecryptFile decrypt:[dictTemp3 valueForKey:@"longitude"]] floatValue];
    obj.taskDetailObj.locationName = [EncryptDecryptFile decrypt:[dictTemp3 valueForKey:@"locationName"]];
    obj.taskDetailObj.latitude = [[EncryptDecryptFile decrypt:[dictTemp3 valueForKey:@"latitude"]] floatValue];
    obj.taskDetailObj.endDate = [EncryptDecryptFile decrypt:[dictTemp3 valueForKey:@"endDate"]];
    obj.taskDetailObj.taskDescription = [EncryptDecryptFile decrypt:[dictTemp3 valueForKey:@"description"]];
    obj.taskDetailObj.lastModifyTime = [EncryptDecryptFile decrypt:[dictTemp3 valueForKey:@"lastModifyTime"]];
    obj.taskDetailObj.taskImagesArray = [[NSMutableArray alloc] init];
    obj.taskDetailObj.imagesArray = [[NSMutableArray alloc]init];
    
    for (rating *ratingObjs in obj.arrayRatings_acceptedBy) {
        
        if ([ratingObjs.taskId isEqualToString:[NSString stringWithFormat:@"%@",obj.taskDetailObj.taskid]]) {
            
            obj.isAlreadyRated = true;
            
            break;
            
        }
        
    }
    
    arrayTerms= [dict valueForKey:@"taskImage"];
    NSArray *newArray = [arrayTerms objectAtIndex:0];
    
    int i = 0;
    
    for (NSDictionary *dicts in newArray) {
        
        NSArray *componentsArray = [[EncryptDecryptFile decrypt:[dicts valueForKey:@"taskImg"]] componentsSeparatedByString:@"http://192.168.5.134/handapp/"];
        
        if (componentsArray.count>1) {
            [obj.taskDetailObj.taskImagesArray addObject:[NSString stringWithFormat:urlServer,[componentsArray objectAtIndex:1]]];
        }else{
            [obj.taskDetailObj.taskImagesArray addObject:[NSString stringWithFormat:urlServer,[componentsArray objectAtIndex:0]]];
        }
        
        NSString *fullPath=[obj.taskDetailObj.taskImagesArray objectAtIndex:i];
        NSArray *parts = [fullPath componentsSeparatedByString:@"/"];
        NSString *imageName=[parts lastObject];
        NSArray *imageNameParts = [imageName componentsSeparatedByString:@"."];
        
        NSString *userImageNameWithJPEG = [NSString stringWithFormat:@"%@_%@_%d.jpeg",[imageNameParts firstObject],obj.taskDetailObj.taskid,i];
        
        UIImage *img = [GlobalFunction loadImageOfLogoFromCacheFolder:userImageNameWithJPEG dirname:@"taskImages"];
        
        if (img) {
            
            [obj.taskDetailObj.imagesArray addObject:img];
            
        }
        
        i = i+1;
    }
    
    if (obj.taskDetailObj.taskImagesArray.count != obj.taskDetailObj.imagesArray.count) {
        [obj.taskDetailObj.imagesArray removeAllObjects];
    }
    
    
    if ([userObj.userid isEqualToString:obj.userId_acceptedBy]) {
        
        if ([obj.picPath_createdBy isEqualToString:@""]) {
            obj.userImage_createdBy = imageUserDefault;
        }else{
            NSArray *components = [obj.picPath_createdBy componentsSeparatedByString:@"/"];
            
            NSString *imgNameWithJPEG = [components lastObject];
            
            
            NSArray *componentsWithDots = [imgNameWithJPEG componentsSeparatedByString:@"."];
            
            if (componentsWithDots.count>2) {
                
                imgNameWithJPEG = [NSString stringWithFormat:@"%@.%@",[componentsWithDots firstObject],[componentsWithDots lastObject]];
                
            }
            
            UIImage *img = [GlobalFunction loadImageOfLogoFromCacheFolder:imgNameWithJPEG dirname:@"user"];
            
            if (img) {
                obj.userImage_createdBy = img;
            }else{
                UIImage *img2 = [GlobalFunction getImageFromURL:[NSString stringWithFormat:urlServer,obj.picPath_createdBy]];
                
                if (img2) {
                    obj.userImage_createdBy = img2;
                    
                    NSArray *components2 = [imgNameWithJPEG componentsSeparatedByString:@"."];
                    
                    NSString *imgNameWithoutJPEG = [components2 firstObject];
                    
                    [GlobalFunction saveimage:img2 imageName:imgNameWithoutJPEG dirname:@"user"];
                    
                }else{
                    obj.userImage_createdBy = imageUserDefault;
                }
            }
        }
    }else if ([userObj.userid isEqualToString:obj.userId_createdBy]){
        
        if ([obj.picPath_acceptedBy isEqualToString:@""]) {
            obj.userImage_acceptedBy = imageUserDefault;
        }else{
            NSArray *components = [obj.picPath_acceptedBy componentsSeparatedByString:@"/"];
            
            NSString *imgNameWithJPEG = [components lastObject];
            
            
            NSArray *componentsWithDots = [imgNameWithJPEG componentsSeparatedByString:@"."];
            
            if (componentsWithDots.count>2) {
                
                imgNameWithJPEG = [NSString stringWithFormat:@"%@.%@",[componentsWithDots firstObject],[componentsWithDots lastObject]];
                
            }
            
            UIImage *img = [GlobalFunction loadImageOfLogoFromCacheFolder:imgNameWithJPEG dirname:@"user"];
            
            if (img) {
                obj.userImage_acceptedBy = img;
            }else{
                UIImage *img2 = [GlobalFunction getImageFromURL:[NSString stringWithFormat:urlServer,obj.picPath_acceptedBy]];
                
                if (img2) {
                    obj.userImage_acceptedBy = img2;
                    
                    NSArray *components2 = [imgNameWithJPEG componentsSeparatedByString:@"."];
                    
                    NSString *imgNameWithoutJPEG = [components2 firstObject];
                    
                    [GlobalFunction saveimage:img2 imageName:imgNameWithoutJPEG dirname:@"user"];
                    
                }else{
                    obj.userImage_acceptedBy = imageUserDefault;
                }
            }
        }
    }
}


//******************************************************************************

-(void)openFilterView
{
//    myAppDelegate.filterViewIsOpen=true;
//    myAppDelegate.filterViewIsClose=false;
    
    _filterView.clipsToBounds=YES;
    
    [UIView animateWithDuration:0.5f
                          delay:0.0f
                        options:UIViewAnimationOptionTransitionFlipFromBottom | UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         [self.view layoutIfNeeded];
                         
                         if(IS_IPHONE_6)
                         {
                             _blurViewHeight.constant=97;  //FOR IPHONE 6
                         }
                         
                         else if (IS_IPHONE_5 || IS_IPHONE_4_OR_LESS)
                         {
                             _blurViewHeight.constant=83;  //FOR IPHONE 5
                         }
                         
                         else if (IS_IPHONE_6P)
                         {
                             _blurViewHeight.constant=107;  //FOR IPHONE 6Plus
                         }
                         
                         _parentFilterView.hidden=YES;
                         _filterView.hidden=NO;
                     }completion:NULL];
    
}

-(void)closeFilterView
{
//    myAppDelegate.filterViewIsClose=true;
//    myAppDelegate.filterViewIsOpen=false;
    
    
    _filterView.clipsToBounds=YES;
    
    [self.view layoutIfNeeded];
    
    _blurViewHeight.constant=0;
    
    [UIView animateWithDuration:0.5 delay:0.0 options:UIViewAnimationOptionTransitionFlipFromBottom | UIViewAnimationOptionCurveEaseInOut animations:^{
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished) {
        _filterView.hidden = YES;
        _parentFilterView.hidden=YES;
        
    }];
    
}


//******************************************************************************


#pragma mark - filter buttons work
//changed code
- (IBAction)AdjustRadiusBtnClicked:(id)sender{
    
    
    _minTxtFld.clearButtonMode = UITextFieldViewModeNever;
    _maxTxtFld.clearButtonMode = UITextFieldViewModeNever;
    
    
    self.datePickerView.hidden = false;
    // self.parentFilterView.hidden = true;
    
//    self.parentFilterView.alpha = 0.0;
    [self openFilterView];

    
    [self.priceRangeBtn setTitleColor:[UIColor whiteColor]forState:UIControlStateNormal];
    [self.priceRangeBtn setBackgroundColor:[UIColor colorWithRed:63/255.0 green:67/255.0 blue:70/255.0 alpha:1.0]];
    
    [_btnTime setTitleColor:[UIColor whiteColor]forState:UIControlStateNormal];
    [_btnTime setBackgroundColor:[UIColor colorWithRed:63/255.0 green:67/255.0 blue:70/255.0 alpha:1.0]];
    
    
    
    
    if (!self.adjustRadiusBtn.selected) {
        
        //  self.filterView.hidden = false;
//        [UIView animateWithDuration:0.5
//                              delay:0.0
//                            options:UIViewAnimationOptionCurveEaseOut
//                         animations:^{
//                             
//                             self.filterView.alpha = 1.0;
//                             
//                         } completion:^(BOOL finished) {
//                             
//                             
//                         }];
        
        [self openFilterView];
        
        
        [_adjustRadiusBtn setTitleColor:[UIColor colorWithRed:63/255.0 green:67/255.0 blue:70/255.0 alpha:1.0] forState:UIControlStateNormal];
        [_adjustRadiusBtn setBackgroundColor:[UIColor whiteColor]];
        
        
        
     //   oldMinPriceRangeData = self.minTxtFld.text;
     //   oldMaxPriceRangeData = self.maxTxtFld.text;
        NSString *fillCity = myAppDelegate.currentCityName;
        self.minTxtFld.text = rangeLocation;//oldRadiusData_TextToShowAsPlaceHolder;
        
        if (enteredLocation.length <=0 )
        {
            self.maxTxtFld.text = fillCity;
        }
        else
        {
            if ([enteredLocation isEqualToString:myAppDelegate.lastSearchLocation])
            {
                self.maxTxtFld.text = enteredLocation;//oldLocationData;
            }
            else
            {
                self.maxTxtFld.text = myAppDelegate.lastSearchLocation;//by vs to show success location name search 31 Aug 18...
            }

        }

        
      //  self.maxTxtFld.text = enteredLocation;//oldLocationData;
        
        self.minTxtFld.textColor = [UIColor colorWithRed:63.0/255.0f green:67.0/255.0f blue:70.0/255.0f alpha:1.0];// RGB(185, 185, 185);
        
        [self.adjustRadiusBtn setSelected:YES];
        [self.btnTime setSelected:NO];
        [self.priceRangeBtn setSelected:NO];
        
        CATransition *animation = [CATransition animation];
        animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
        animation.type = kCATransitionFade;
        animation.duration = 0.75;
        [_minLabel.layer addAnimation:animation forKey:@"kCATransitionFade"];
        [_maxLabel.layer addAnimation:animation forKey:@"kCATransitionFade"];
        [_maxTxtFld.layer addAnimation:animation forKey:@"kCATransitionFade"];
        [_minTxtFld.layer addAnimation:animation forKey:@"kCATransitionFade"];

        [_minLabel setText:@"Adjust Radius"];
        [_maxLabel setText:@"Location"];
        
        self.maxTxtFld.keyboardType = UIKeyboardTypeDefault;
       
        NSAttributedString *str1 = [[NSAttributedString alloc] initWithString:@"City" attributes:@{ NSForegroundColorAttributeName : RGB(185, 185, 185),
                                                                                                           }];
        _maxTxtFld.attributedPlaceholder = str1;
       
        NSAttributedString *str2 = [[NSAttributedString alloc] initWithString:@"Miles of" attributes:@{ NSForegroundColorAttributeName : RGB(185, 185, 185),
                                                                                                              }];
        _minTxtFld.attributedPlaceholder = str2;
        
    }else
    {
        [self.adjustRadiusBtn setSelected:FALSE];
        // self.filterView.hidden = true;
//        [UIView animateWithDuration:0.5
//                              delay:0.0
//                            options:UIViewAnimationOptionCurveEaseOut
//                         animations:^{
//                             
//                             self.filterView.alpha = 0.0;
//                             
//                             
//                         } completion:^(BOOL finished) {
//                             
//                             
//                         }];
        
        [self closeFilterView];

        
        [_adjustRadiusBtn setTitleColor:[UIColor whiteColor]forState:UIControlStateNormal];
        [_adjustRadiusBtn setBackgroundColor:[UIColor colorWithRed:63/255.0 green:67/255.0 blue:70/255.0 alpha:1.0]];
    }
    
}

- (IBAction)priceRangeBtnClicked:(id)sender{
    
    _minTxtFld.clearButtonMode = UITextFieldViewModeNever;
    _maxTxtFld.clearButtonMode = UITextFieldViewModeNever;
    
    
    self.datePickerView.hidden = false;
    self.parentFilterView.hidden = true;
    [self openFilterView];

    
    [_adjustRadiusBtn setTitleColor:[UIColor whiteColor]forState:UIControlStateNormal];
    [_adjustRadiusBtn setBackgroundColor:[UIColor colorWithRed:63/255.0 green:67/255.0 blue:70/255.0 alpha:1.0]];
    
    [_btnTime setTitleColor:[UIColor whiteColor]forState:UIControlStateNormal];
    [_btnTime setBackgroundColor:[UIColor colorWithRed:63/255.0 green:67/255.0 blue:70/255.0 alpha:1.0]];
    
    if (!self.priceRangeBtn.selected) {
        
        // self.filterView.hidden = false;
//        [UIView animateWithDuration:0.5
//                              delay:0.0
//                            options:UIViewAnimationOptionCurveEaseOut
//                         animations:^{
//                             
//                             self.filterView.alpha = 1.0;
//                             
//                         } completion:^(BOOL finished) {
//                             
//                             
//                         }];
        
        [self openFilterView];
        
        

        [_priceRangeBtn setTitleColor:[UIColor colorWithRed:63/255.0 green:67/255.0 blue:70/255.0 alpha:1.0] forState:UIControlStateNormal];
        [_priceRangeBtn setBackgroundColor:[UIColor whiteColor]];
        
        
     //   oldRadiusData_TextToShowAsPlaceHolder = self.minTxtFld.text;
     //   oldLocationData = self.maxTxtFld.text;
        
        self.minTxtFld.text = minPriceValue;//oldMinPriceRangeData;
        self.maxTxtFld.text = maxPriceValue;//oldMaxPriceRangeData;

        
        self.minTxtFld.textColor = [UIColor colorWithRed:63.0/255.0f green:67.0/255.0f blue:70.0/255.0f alpha:1.0];// [UIColor blackColor];
        
        [self.adjustRadiusBtn setSelected:NO];
        [self.btnTime setSelected:NO];
        [self.priceRangeBtn setSelected:YES];
        CATransition *animation = [CATransition animation];
        animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
        animation.type = kCATransitionFade;
        animation.duration = 0.75;
        [_minLabel.layer addAnimation:animation forKey:@"kCATransitionFade"];
        [_maxLabel.layer addAnimation:animation forKey:@"kCATransitionFade"];
        [_maxTxtFld.layer addAnimation:animation forKey:@"kCATransitionFade"];
        [_minTxtFld.layer addAnimation:animation forKey:@"kCATransitionFade"];

        [_minLabel setText:@"Min"];
        [_maxLabel setText:@"Max"];
        
        self.maxTxtFld.keyboardType = UIKeyboardTypeNumberPad;
        
        NSAttributedString *str1 = [[NSAttributedString alloc] initWithString:@"$" attributes:@{ NSForegroundColorAttributeName : RGB(185, 185, 185),
                                                                                                 }];
        _maxTxtFld.attributedPlaceholder = str1;
        
        NSAttributedString *str2 = [[NSAttributedString alloc] initWithString:@"$" attributes:@{ NSForegroundColorAttributeName : RGB(185, 185, 185),
                                                                                                 }];
        _minTxtFld.attributedPlaceholder = str2;
        
    }else{
        //self.filterView.hidden = true;
        
//        [UIView animateWithDuration:0.5
//                              delay:0.0
//                            options:UIViewAnimationOptionCurveEaseOut
//                         animations:^{
//                             
//                             self.filterView.alpha = 0.0;
//                             
//                         } completion:^(BOOL finished) {
//                             
//                             
//                         }];
        
        
        [self closeFilterView];


       
        
        [self.priceRangeBtn setSelected:NO];
        
        [_priceRangeBtn setTitleColor:[UIColor whiteColor]forState:UIControlStateNormal];
        [_priceRangeBtn setBackgroundColor:[UIColor colorWithRed:63/255.0 green:67/255.0 blue:70/255.0 alpha:1.0]];
    }
    
    
}

-(void)checkSelectedBtns
{
    
    
    [txtActiveField resignFirstResponder];
    
    
    [self.view layoutIfNeeded];
    
    self.constraintViewTextFieldHolderBottomSpace.constant = 0;
    
    [UIView animateWithDuration:0.2 animations:^{
        
        [self.view layoutIfNeeded];
        
    } completion:^(BOOL finished) {
        
    }];
    
    
    
    
    
    if (self.adjustRadiusBtn.selected) {
        
        if (self.filterCityViewHeight.constant == 180 || self.filterCityViewHeight.constant == 160)
        {
            [self.view layoutIfNeeded];
            
            self.filterCityViewHeight.constant=0;
            
            [UIView animateWithDuration:0.3 animations:^ {
                [self.view layoutIfNeeded];
                
            }];
            
        }

        [self AdjustRadiusBtnClicked:nil];
        
    }
    else if (self.btnTime.selected)
    {
        [self timeBtnClicked:nil];
        
    }
    else if (self.priceRangeBtn.selected)
    {
        [self priceRangeBtnClicked:nil];
        
    }
    else
    {
        
    }
    
}


- (IBAction)timeBtnClicked:(id)sender{
    
    [_adjustRadiusBtn setTitleColor:[UIColor whiteColor]forState:UIControlStateNormal];
    [_adjustRadiusBtn setBackgroundColor:[UIColor colorWithRed:63/255.0 green:67/255.0 blue:70/255.0 alpha:1.0]];
    
    [_priceRangeBtn setTitleColor:[UIColor whiteColor]forState:UIControlStateNormal];
    [_priceRangeBtn setBackgroundColor:[UIColor colorWithRed:63/255.0 green:67/255.0 blue:70/255.0 alpha:1.0]];
    [self openFilterView];

    self.parentFilterView.hidden = YES;
    
    if (!self.btnTime.selected) {
        
        // self.filterView.hidden = false;
//        [UIView animateWithDuration:0.5
//                              delay:0.0
//                            options:UIViewAnimationOptionCurveEaseOut
//                         animations:^{
//                             
//                             self.filterView.alpha = 1.0;
//                             
//                         } completion:^(BOOL finished) {
//                             
//                             
//                         }];
        
        [self openFilterView];

        
        [_btnTime setTitleColor:[UIColor colorWithRed:63/255.0 green:67/255.0 blue:70/255.0 alpha:1.0] forState:UIControlStateNormal];
        [_btnTime setBackgroundColor:[UIColor whiteColor]];
        
       // oldRadiusData_TextToShowAsPlaceHolder = self.minTxtFld.text;
       // oldLocationData = self.maxTxtFld.text;
        
        self.minTxtFld.text = minTime;//oldRadiusData_TextToShowAsPlaceHolder;
        self.maxTxtFld.text = maxTime;//oldLocationData;
        
       // self.minTxtFld.textColor = [UIColor colorWithRed:63.0/255.0f green:67.0/255.0f blue:70.0/255.0f alpha:1.0];//[UIColor blackColor];
        
        [self.adjustRadiusBtn setSelected:NO];
        [self.priceRangeBtn setSelected:NO];
        [self.btnTime setSelected:YES];
        CATransition *animation = [CATransition animation];
        animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
        animation.type = kCATransitionFade;
        animation.duration = 0.75;
        [_minLabel.layer addAnimation:animation forKey:@"kCATransitionFade"];
        [_maxLabel.layer addAnimation:animation forKey:@"kCATransitionFade"];
        [_maxTxtFld.layer addAnimation:animation forKey:@"kCATransitionFade"];
        [_minTxtFld.layer addAnimation:animation forKey:@"kCATransitionFade"];

        [_minLabel setText:@"Start"];
        [_maxLabel setText:@"End"];
        
        self.maxTxtFld.keyboardType = UIKeyboardTypeNumberPad;
        
        NSAttributedString *str1 = [[NSAttributedString alloc] initWithString:@"Date" attributes:@{ NSForegroundColorAttributeName : RGB(185, 185, 185),
                                                                                                          }];
        _maxTxtFld.attributedPlaceholder = str1;
        
        NSAttributedString *str2 = [[NSAttributedString alloc] initWithString:@"Date" attributes:@{ NSForegroundColorAttributeName : RGB(185, 185, 185),
                                                                                                          }];
        _minTxtFld.attributedPlaceholder = str2;
        
    }else {
        
        //  self.filterView.hidden = true;
        
//        [UIView animateWithDuration:0.5
//                              delay:0.0
//                            options:UIViewAnimationOptionCurveEaseOut
//                         animations:^{
//                             
//                             self.filterView.alpha = 0.0;
//                             
//                         } completion:^(BOOL finished) {
//                             
//                             
//                         }];
        [self closeFilterView];

        
        
        self.parentFilterView.hidden = true;
        [self.btnTime setSelected:NO];
        [_btnTime setTitleColor:[UIColor whiteColor]forState:UIControlStateNormal];
        [_btnTime setBackgroundColor:[UIColor colorWithRed:63/255.0 green:67/255.0 blue:70/255.0 alpha:1.0]];
    }
    
}

-(void)performAnimationNow{
    
    [self.view layoutIfNeeded];
    
    // if (self.constraintViewCategoryHeight.constant == 41 || self.constraintViewCategoryHeight.constant == 48 || self.constraintViewCategoryHeight.constant == 37) {
    
    if (self.constraintCategoryViewHeight.constant == 40 || self.constraintCategoryViewHeight.constant == 48 || self.constraintCategoryViewHeight.constant == 37) {
        
      //  self.constraintCategoryViewHeight.constant = SCREEN_HEIGHT - 125;
        
        if (IS_IPHONE_6P) {
            self.constraintCategoryViewHeight.constant = SCREEN_HEIGHT - 126;
        }
        if (IS_IPHONE_6) {
            self.constraintCategoryViewHeight.constant = SCREEN_HEIGHT - 117;
        }
        if (IS_IPHONE_5 || IS_IPHONE_4_OR_LESS) {
            self.constraintCategoryViewHeight.constant = SCREEN_HEIGHT - 97;
        }
        
        
        self.lblCategoryPlaceHolder.text = @"";
        
        [GlobalFunction rotateView:self.imgArrow atAngle:-90];
        
        [self.tblCategory layoutIfNeeded];
        [self.tblCategory reloadData];
        [self.tblCategory layoutIfNeeded];
        
        self.tblCategory.hidden = NO;
        
        self.lblSeperatorCategory.hidden = NO;
        
        self.tblCategory.alpha = 0.0;
        
        self.lblCategoryPlaceHolder.text = categoryName;
        
        [UIView animateWithDuration:0.5 animations:^{
            
            [self.view layoutIfNeeded];
            
            self.tblCategory.alpha = 1.0;
            
        } completion:^(BOOL finished) {
            
            
        }];
        
    }else{
        if (IS_IPHONE_6P) {
            self.constraintCategoryViewHeight.constant = 48;
        }
        if (IS_IPHONE_6) {
            self.constraintCategoryViewHeight.constant = 40;
        }
        if (IS_IPHONE_5 || IS_IPHONE_4_OR_LESS) {
            self.constraintCategoryViewHeight.constant = 37;
        }

        
        
        self.lblCategoryPlaceHolder.text = categoryName;
        
        [GlobalFunction rotateView:self.imgArrow atAngle:0];
        
        self.tblCategory.alpha = 1.0;
        
        [UIView animateWithDuration:0.5 animations:^{
            
            [self.view layoutIfNeeded];
            
            self.tblCategory.alpha = 0.0;
            
        } completion:^(BOOL finished) {
            
            self.tblCategory.hidden = YES;
            
            if (isFromTableTap) {
                
            }else{
                [self filterList];
            }
            
            //            filterCategoryId = @"";
            //            filterSubCategoryId = @"";
            isFromTableTap = false;
            //            categoryName = @"Category";
            
            self.viewCategoryTapHandler.hidden = NO;
           // self.lblSeperatorCategory.hidden = YES;
            
        }];
        
    }
    
}

-(void)openCategoryTableView:(UITapGestureRecognizer *)gesture{
    
    _datePickerView.hidden = true;
    self.parentFilterView.hidden = true;

    if (self.constraintViewTextFieldHolderBottomSpace.constant != 0) {
        
        [self.view layoutIfNeeded];
        
        self.constraintViewTextFieldHolderBottomSpace.constant = 0;
        
        [UIView animateWithDuration:0.5 animations:^{
            
            [self.view layoutIfNeeded];
            
        } completion:^(BOOL finished) {
            
            [self performAnimationNow];
            
        }];
        
        [txtActiveField resignFirstResponder];
        
    }else{
        
        [self performAnimationNow];
        
    }
    
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if (tableView == self.tblCategory) {
        return myAppDelegate.arrayCategoryDetail.count+1;
    }else if (tableView == self.tblRadius){
        
        if (isFromSearchResult) {
            return searchResultArrayFromRadius.count + 1;
        }else{
            return arrRadius.count+1;
        }
        
    }
    else if (tableView == self.cityNameTableView) {
        
        NSLog(@"arrRadius count is...%lu",(unsigned long)arrRadius.count);
        return FilterCities.count;
        
    }

    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *celltbl;

    if (tableView == self.tblCategory) {
        
        
        CategoryTableViewCell *cell;
        [cell.lblCategory setFont:[UIFont fontWithName:@"Lato-Regular" size:13]];

//        cell.backgroundColor = [UIColor whiteColor];
        
        if (indexPath.row == 0) {
            
            cell = (CategoryTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"categoryListing"];
            
            cell.lblCategory.text = @"All Categories";
            

            
        }else{
            
            
            if ([[myAppDelegate.arrayCategoryDetail objectAtIndex:indexPath.row - 1] isKindOfClass:[category class]]) {
                
                cell = (CategoryTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"categoryListing"];
                
                category *obj = [myAppDelegate.arrayCategoryDetail objectAtIndex:indexPath.row - 1];
                
                cell.lblCategory.text = obj.name;
                
            }else if ([[myAppDelegate.arrayCategoryDetail objectAtIndex:indexPath.row - 1] isKindOfClass:[subCategory class]]){
                
                cell = (CategoryTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"categorySubListing"];
                
                subCategory *obj = [myAppDelegate.arrayCategoryDetail objectAtIndex:indexPath.row - 1];
                
                cell.lblCategory.text = obj.name;
                
            }
            
        }
        
        
//        if (indexPath.row==myAppDelegate.SelectedIndexPathForFilter.row) {
//            category *obj = [myAppDelegate.arrayCategoryDetail objectAtIndex:indexPath.row - 1];
//            self.lblCategoryPlaceHolder.text =obj.name;
//        }
        
        if ([indexPath compare:lastSelectedIndexPath] == NSOrderedSame){
            cell.imgViewCheckMark.hidden = false;
        }else{
            cell.imgViewCheckMark.hidden = true;
        }
        
        return cell;
        
    }else if (tableView == self.tblRadius){
        
        celltbl = [tableView dequeueReusableCellWithIdentifier:@"cell2"];
        
        if (indexPath.row == 0) {
            
            celltbl.textLabel.text = [NSString stringWithFormat:@"All"];
            
        }else{
            
            if (isFromSearchResult) {
                
                celltbl.textLabel.text = [NSString stringWithFormat:@"%@ Miles",[searchResultArrayFromRadius objectAtIndex:indexPath.row - 1]];
                
            }else{
                
                celltbl.textLabel.text = [NSString stringWithFormat:@"%@ Miles",[arrRadius objectAtIndex:indexPath.row - 1]];
                
            }
            
        }
        
        celltbl.textLabel.textColor = RGB(185, 185, 185);
        
    }
    else if (tableView == self.cityNameTableView)
    {
        
       MapCityTableViewCell  *cellCity = (MapCityTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"cell"];
        
        cellCity.lblCityName.textColor =  RGB(63, 67, 70);
        
        
        cities *obj;
        obj=[FilterCities objectAtIndex:indexPath.row];
        
        cellCity.lblCityName.text =obj.cityName; //[FilterCities objectAtIndex:indexPath.row];
        
        cellCity.lblCityName.textAlignment = NSTextAlignmentCenter;
        
        return cellCity;
    }
    
    return celltbl;
    
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (tableView == self.tblCategory || tableView == self.tblRadius) {
        
        // Remove seperator inset
        if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
            [cell setSeparatorInset:UIEdgeInsetsZero];
        }
        
        // Prevent the cell from inheriting the Table View's margin settings
        if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
            [cell setPreservesSuperviewLayoutMargins:NO];
        }
        
        // Explictly set your cell's layout margins
        if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
            [cell setLayoutMargins:UIEdgeInsetsZero];
        }
        
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (tableView == self.tblCategory) {
        
        if (indexPath.row == 0) {
            
            filterCategoryId = @"";
            filterSubCategoryId = @"";
            
            categoryName = @"All Categories";
            
            [GlobalFunction addIndicatorView];
            
            [self filterList];
            
        }else{
            
            if ([[myAppDelegate.arrayCategoryDetail objectAtIndex:indexPath.row - 1] isKindOfClass:[category class]]) {
                
                category *obj = [myAppDelegate.arrayCategoryDetail objectAtIndex:indexPath.row - 1];
                
                categoryName = obj.name;
                
                filterSubCategoryId = @"";
                filterCategoryId = obj.categoryId;
                
                [GlobalFunction addIndicatorView];
                
                [self filterList];
                
            }else if ([[myAppDelegate.arrayCategoryDetail objectAtIndex:indexPath.row - 1] isKindOfClass:[subCategory class]]){
                
                subCategory *obj = [myAppDelegate.arrayCategoryDetail objectAtIndex:indexPath.row - 1];
                
                categoryName = obj.name;
                
                filterCategoryId = @"";
                filterSubCategoryId = obj.subCategoryId;
                
                [GlobalFunction addIndicatorView];
                
                [self filterList];
                
            }
            
        }
        
        CategoryTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        
        cell.imgViewCheckMark.hidden = false;
        
        if (lastSelectedIndexPath) {
            CategoryTableViewCell *cell2 = [tableView cellForRowAtIndexPath:lastSelectedIndexPath];
            
            cell2.imgViewCheckMark.hidden = true;
        }
        
       // lastSelectedIndexPath = nil;
        //lastSelectedIndexPath = [[NSIndexPath alloc] init];
        lastSelectedIndexPath = indexPath;
//        myAppDelegate.SelectedIndexPathForFilter = nil;
        myAppDelegate.SelectedIndexPathForFilter = indexPath;
        isFromTableTap = true;
        
        [self openCategoryTableView:tapGesture];
        
    }else if (tableView == self.tblRadius){
        
        if (indexPath.row == 0) {
            
            filterRange = @"";
            
        }else{
            
            if (isFromSearchResult) {
                
                filterRange = [NSString stringWithFormat:@"%ld",(long)[[searchResultArrayFromRadius objectAtIndex:indexPath.row - 1] integerValue]];
                
            }else{
                
                filterRange = [NSString stringWithFormat:@"%ld",(long)[[arrRadius objectAtIndex:indexPath.row - 1] integerValue]];
                
            }
            
        }
        
        if ([filterRange isEqualToString:@""]) {
            
            oldRadiusData_TextToShowAtEditing = @"";
            
            oldRadiusData_TextToShowAsPlaceHolder = [NSString stringWithFormat:@"All"];
            
        }else{
            
       // oldRadiusData_TextToShowAtEditing = filterRange;
            
        //oldRadiusData_TextToShowAsPlaceHolder = [NSString stringWithFormat:@"Within %@ miles",oldRadiusData_TextToShowAtEditing];
            
        }
        
        
        isFromRadiusTableTap = true;
        
        [txtActiveField resignFirstResponder];
        
        //  [self hideRadiusTableView:self.tapGestureRecognizerForHidingRadiusTableView];
        
        [GlobalFunction addIndicatorView];
        
        [self filterList];
        
    }
    else if (tableView == self.cityNameTableView)
    {
        cities *obj=[FilterCities objectAtIndex:indexPath.row];
        self.maxTxtFld.text=obj.cityName;
        enteredLocation = self.maxTxtFld.text;
        
        if (self.filterCityViewHeight.constant == 180 || self.filterCityViewHeight.constant == 160)
        {
            [self.view layoutIfNeeded];
            
            self.filterCityViewHeight.constant=0;
            
            [UIView animateWithDuration:0.3 animations:^ {
                [self.view layoutIfNeeded];
                
            }];
        }

    }
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (tableView == self.tblCategory) {
        
        if (indexPath.row == 0) {
            
            return 40;
            
        }else{
            
            if ([[myAppDelegate.arrayCategoryDetail objectAtIndex:indexPath.row - 1] isKindOfClass:[category class]]) {
                
                return 40;
                
            }else if ([[myAppDelegate.arrayCategoryDetail objectAtIndex:indexPath.row - 1] isKindOfClass:[subCategory class]]){
                
                return 52;
                
            }
            
        }
        
    }
    
    return 32;
    
}

#pragma mark - create accessory view
-(void)createInputAccessoryView{
    // Create the view that will play the part of the input accessory view.
    // Note that the frame width (third value in the CGRectMake method)
    // should change accordingly in landscape orientation. But we don’t care
    // about that now.
    inputAccView = [[UIView alloc] initWithFrame:CGRectMake(10.0, 0.0, self.view.frame.size.width, 40.0)];
    // Set the view’s background color. We’ ll set it here to gray. Use any color you want.
    [inputAccView setBackgroundColor:[UIColor lightGrayColor]];
    // We can play a little with transparency as well using the Alpha property. Normally
    // you can leave it unchanged.
    [inputAccView setAlpha: 0.8];
    // If you want you may set or change more properties (ex. Font, background image,e.t.c.).
    // For now, what we’ ve already done is just enough.
    // Let’s create our buttons now. First the previous button.
    btnPrev = [UIButton buttonWithType: UIButtonTypeCustom];
    // Set the button’ s frame. We will set their widths to 80px and height to 40px.
    [btnPrev setFrame: CGRectMake(0.0, 0.0, 80.0, 40.0)];
    // Title.
    [btnPrev setTitle: @"Previous" forState: UIControlStateNormal];
    // Background color.
    [btnPrev setBackgroundColor: [UIColor clearColor]];
    // You can set more properties if you need to.
    // With the following command we’ ll make the button to react in finger tapping. Note that the
    // gotoPrevTextfield method that is referred to the @selector is not yet created. We’ ll create it
    // (as well as the methods for the rest of our buttons) later.
    [btnPrev addTarget: self action: @selector(gotoPrevTextfield) forControlEvents: UIControlEventTouchUpInside];
    // Do the same for the two buttons left.
    btnNext = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnNext setFrame:CGRectMake(85.0f, 0.0f, 80.0f, 40.0f)];
    [btnNext setTitle:@"Next" forState:UIControlStateNormal];
    [btnNext setBackgroundColor:[UIColor clearColor]];
    [btnNext addTarget:self action:@selector(gotoNextTextfield) forControlEvents:UIControlEventTouchUpInside];
    btnDone = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnDone setFrame:CGRectMake(self.view.frame.size.width - 80.0f, 0.0f, 80.0f, 40.0f)];
    [btnDone setTitle:@"Done" forState:UIControlStateNormal];
    [btnDone setBackgroundColor:[UIColor clearColor]];
    [btnDone setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [btnDone addTarget:self action:@selector(doneTyping) forControlEvents:UIControlEventTouchUpInside];
    // Now that our buttons are ready we just have to add them to our view.
    [inputAccView addSubview:btnPrev];
    [inputAccView addSubview:btnNext];
    [inputAccView addSubview:btnDone];
}
-(void)changeDateFromLabel:(id)sender
{
    
    NSLog(@"done button click");
    
    _datePickerView.hidden = true;
    self.parentFilterView.hidden = true;
    
    [self updateTextField:nil];
    
    if (dateStart != nil && dateEnd != nil)
    {
        //        if ([dateStart compare:dateEnd] == NSOrderedDescending) {
        //            NSLog(@"date1 is Newer than date2");
        //        } else
        
        if ([dateStart compare:dateEnd] == NSOrderedAscending) {
            NSLog(@"date1 is earlier than date2");
              [self addAllPins];
        } else {
            
            NSLog(@"date2 is earlier than date1");
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"BUY TIMEE" message:@"Please enter a valid date." delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:nil];
            
            [alertView show];
        }
        
        
    }
    if(dateStart!=nil && dateEnd==nil)
    {
        [self addAllPins];
        
    }

    
    
    
    
    
}




//-(void)changeDateFromLabel:(id)sender
//{
//    
//    NSLog(@"done button click");
//    
//    _datePickerView.hidden = true;
//    self.filterParentView.hidden = true;
//    
//    if (dateStart != nil && dateEnd != nil)
//    {
//        //        if ([dateStart compare:dateEnd] == NSOrderedDescending) {
//        //            NSLog(@"date1 is Newer than date2");
//        //        } else
//        
//        if ([dateStart compare:dateEnd] == NSOrderedAscending) {
//            NSLog(@"date1 is earlier than date2");
//            [self performFilter];
//        } else {
//            
//            NSLog(@"date2 is earlier than date1");
//            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"BuyTimee" message:@"Please enter a valid date." delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:nil];
//            
//            [alertView show];
//        }
//        
//        
//    }
//    
//    
//    
//    
//    NSLog(@"%@",_minTxtFld.text);
//    NSLog(@"%@",_maxTxtFld.text);
//    
//    
//    if(dateStart!=nil && dateEnd==nil)
//    {
//        [self performFilter];
//        
//    }
//    
//    
//    
//    
//}


- (BOOL) textFieldShouldClear:(UITextField *)textField{
    
    
    if (txtActiveField == self.minTxtFld)
    {
        dateStart = nil;
        minTime = @"";
    }
    if (txtActiveField == self.maxTxtFld)
    {
        dateEnd = nil;
        maxTime = @"";
    }
    [textField resignFirstResponder];
    
    NSLog(@"%@",_minTxtFld.text);
    NSLog(@"%@",_maxTxtFld.text);
    
    return YES;
}

-(void)cancelPressed:(id)sender
{
    NSLog(@"cancel button click");
    
    NSLog(@"done button click");
    
    _datePickerView.hidden = true;
    self.parentFilterView.hidden = true;
    
   
}


-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    txtActiveField=textField;
    
    _minTxtFld.clearButtonMode = UITextFieldViewModeNever;
    _maxTxtFld.clearButtonMode = UITextFieldViewModeNever;
    
    
    if(_btnTime.selected){
        
        
        
        if(txtActiveField == self.minTxtFld)
        {
            if(dateStart)
            {
                [_datePickerView setDate:dateStart];
            }
        }
        else if(txtActiveField == self.maxTxtFld)
        {
            if(dateEnd)
            {
                [_datePickerView setDate:dateEnd];
            }
        }
        
        
        
        
        self.parentFilterView.hidden = false;
        self.datePickerView.hidden = false;
        
        _minTxtFld.clearButtonMode = UITextFieldViewModeAlways;
        _maxTxtFld.clearButtonMode = UITextFieldViewModeAlways;
        
        
        self.parentFilterView.backgroundColor = [UIColor whiteColor];
        self.parentFilterView.alpha = 0.98f;
        UIBarButtonItem *barButtonDone = [[UIBarButtonItem alloc] initWithTitle:@"Done"
                                                                          style:UIBarButtonItemStyleBordered target:self action:@selector(changeDateFromLabel:)];
        
        UIBarButtonItem *space = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
        
        
        UIBarButtonItem *barButtonCancel = [[UIBarButtonItem alloc] initWithTitle:@"Cancel"
                                                                            style:UIBarButtonItemStyleBordered target:self action:@selector(cancelPressed:)];
        
        if(IS_IPHONE5 || IS_IPHONE_4_OR_LESS)
        {
            toolBar= [[UIToolbar alloc] initWithFrame:CGRectMake(0,0,320,44)];
        }
        else if (IS_IPHONE_6)
        {
            toolBar= [[UIToolbar alloc] initWithFrame:CGRectMake(0,0,380,44)];
        }
        else if (IS_IPHONE_6P)
        {
            toolBar= [[UIToolbar alloc] initWithFrame:CGRectMake(0,0,420,44)];
        }
        
        
        toolBar.items = @[barButtonDone,space,barButtonCancel];
        
        
        
        
        
        
        toolBar.backgroundColor=[UIColor colorWithRed:255/255 green:255/255 blue:255/255 alpha:0.98];
        
        
        // barButtonDone.tintColor=[UIColor blackColor];
        [self.parentFilterView addSubview:toolBar];

        return NO;
    }
    
    return YES;
}
-(void)gotoPrevTextfield{
    
    if (txtActiveField == self.minTxtFld) {
        
        if (self.adjustRadiusBtn.selected) {
            
            //   filterRange = self.minTxtFld.text;
            //  filterLocationName = self.maxTxtFld.text;
            
            if ([filterRange isEqualToString:@""]) {
                
                //     oldRadiusData_TextToShowAtEditing = @"";
                
                //     oldRadiusData_TextToShowAsPlaceHolder = [NSString stringWithFormat:@"All"];
                
            }else{
                
                // oldRadiusData_TextToShowAtEditing = filterRange;
                
                // oldRadiusData_TextToShowAsPlaceHolder = [NSString stringWithFormat:@"Within %@ miles",oldRadiusData_TextToShowAtEditing];
                
            }
            
            isFromRadiusTableTap = true;
            
            //  self.minTxtFld.text = oldRadiusData_TextToShowAsPlaceHolder;
            
            //    [self hideRadiusTableView:self.tapGestureRecognizerForHidingRadiusTableView];
            
        }else if(self.priceRangeBtn.selected){
            
            //  filterPriceMin = self.minTxtFld.text;
            //  filterPriceMax = self.maxTxtFld.text;
            
        }
        
        //  [self filterList];
        [txtActiveField resignFirstResponder];
        [self checkSelectedBtns];
        return;
    }
    if (txtActiveField == self.maxTxtFld) {
        
        [self.maxTxtFld resignFirstResponder];
        [self.minTxtFld becomeFirstResponder];
        return;
    }
    
    
}

-(void)gotoNextTextfield{
    
    if (txtActiveField == self.maxTxtFld) {
        
        [self.view layoutIfNeeded];
        
        self.constraintViewTextFieldHolderBottomSpace.constant = 0;
        
        [UIView animateWithDuration:0.2 animations:^{
            
            [self.view layoutIfNeeded];
            
        } completion:^(BOOL finished) {
            
        }];
        
        [txtActiveField resignFirstResponder];
        
        if (self.adjustRadiusBtn.selected) {
            
          //  filterLocationName = self.maxTxtFld.text;
            
        }else if(self.priceRangeBtn.selected){
            
           // filterPriceMin = self.minTxtFld.text;
           // filterPriceMax = self.maxTxtFld.text;
            
        }
        
       // [self filterList];
        [self checkSelectedBtns];
        return;
        
    }else if (txtActiveField == self.minTxtFld) {
        
        [self.minTxtFld resignFirstResponder];
        [self.maxTxtFld becomeFirstResponder];
        
        if (self.adjustRadiusBtn.selected) {
            
         //   filterRange = self.minTxtFld.text;
            
            if ([filterRange isEqualToString:@""]) {
                
             //   oldRadiusData_TextToShowAtEditing = @"";
                
              //  oldRadiusData_TextToShowAsPlaceHolder = [NSString stringWithFormat:@"All"];
                
            }else{
                
              //  oldRadiusData_TextToShowAtEditing = filterRange;
                
              //  oldRadiusData_TextToShowAsPlaceHolder = [NSString stringWithFormat:@"Within %@ miles",oldRadiusData_TextToShowAtEditing];
                
            }
           // self.minTxtFld.text = oldRadiusData_TextToShowAsPlaceHolder;
            
            if (self.constraintViewHolderTableRadiusHeight.constant > 0) {
                
                [self.view layoutIfNeeded];
                
                self.constraintViewHolderTableRadiusHeight.constant = 0;
                
                self.viewHolderForTableRadius.alpha = 1.0;
                
                self.viewBgTapHandlerForRadiusTable.hidden = YES;
                
                [UIView animateWithDuration:0.3 animations:^{
                    
                    self.viewHolderForTableRadius.alpha = 0.0;
                    
                    [self.view layoutIfNeeded];
                    
                } completion:^(BOOL finished) {
                    
                    self.viewHolderForTableRadius.hidden = YES;
                    
                    isFromSearchResult = false;
                    
                  //  [self.tblRadius layoutIfNeeded];
                   // [self.tblRadius reloadData];
                   // [self.tblRadius layoutIfNeeded];
                    
                }];
                
            }
            
        }
        
        return;
    }
    
}

-(void)doneTyping{
    
    [self.view layoutIfNeeded];
    
    self.constraintViewTextFieldHolderBottomSpace.constant = 0;
    
    [UIView animateWithDuration:0.2 animations:^{
        
        [self.view layoutIfNeeded];
        
    } completion:^(BOOL finished) {
        
    }];
    
    
    [txtActiveField resignFirstResponder];
    
    
    if (self.adjustRadiusBtn.selected) {
        
           // self.parentFilterView.hidden = true;

        enteredLocation = self.maxTxtFld.text;
        rangeLocation = self.minTxtFld.text;
            
            isFromRadiusTableTap = true;
            
  
    }else if(self.priceRangeBtn.selected){
        
        maxPriceValue = self.maxTxtFld.text;
        minPriceValue = self.minTxtFld.text;
        
    }
    
    [GlobalFunction addIndicatorView];
    
    [self filterList];
    [self checkSelectedBtns];
}


- (void)filterContentForSearchText:(NSString*)searchText //scope:(NSString*)scope
{
    
    FilterCities=[[NSArray alloc] init];
    
    NSPredicate *predicateTitle = [NSPredicate predicateWithFormat:@"cityName beginswith[cd] %@", searchText];
    NSArray *subPredicates = [NSArray arrayWithObjects:predicateTitle, nil];
    NSPredicate *orPredicate = [NSCompoundPredicate orPredicateWithSubpredicates:subPredicates];
    // if(_mycontractsbtn.selected==YES)
    // {
    FilterCities = [myAppDelegate.allCityNames filteredArrayUsingPredicate:orPredicate];
    // }
    // else
    // {
    // searchResults = [arrTemplate filteredArrayUsingPredicate:orPredicate];
    // }
    
    NSLog(@"searching...%lu",(unsigned long)FilterCities.count);
    if (FilterCities.count>0) {
        [self.view layoutIfNeeded];
        
        if (IS_IPHONE5 || IS_IPHONE_4_OR_LESS) {
            self.filterCityViewHeight.constant=160;
        }
        else
        {
            self.filterCityViewHeight.constant=180;
        }
        
        
        [UIView animateWithDuration:0.3 animations:^ {
            [self.view layoutIfNeeded];
            
        }];
        
        
        [self.cityNameTableView reloadData];
    }
    else{
        [self.view layoutIfNeeded];
        
        self.filterCityViewHeight.constant=0;
        
        [UIView animateWithDuration:0.3 animations:^ {
            [self.view layoutIfNeeded];
            
        }];
        
    }
    
    
}



//- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope{
//    
//    NSPredicate *predicateFName = [NSPredicate predicateWithFormat:@"SELF contains[cd] %@", searchText];
//    
//    searchResultArrayFromRadius = [arrRadius filteredArrayUsingPredicate:predicateFName];
//    
//    if (searchResultArrayFromRadius.count>0) {
//        
//    }else{
//        
//    }
//    
//}


-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    
    
    
    
    if (_adjustRadiusBtn.selected) {
        
        if (textField == self.maxTxtFld)
        {
            NSString *value = [textField.text stringByReplacingCharactersInRange:range withString:string];
            //NSLog(@"value: %@", value);
            if (![value isEqualToString:@""] && value.length>=2) {
                [self filterContentForSearchText:value];
                                           //scope:[[self.searchDisplayController.searchBar scopeButtonTitles]
                                            //      objectAtIndex:[self.searchDisplayController.searchBar
                                            //                     selectedScopeButtonIndex]]];
                
                
            }
            else{
                [self.view layoutIfNeeded];
                
                self.filterCityViewHeight.constant=0;
                
                [UIView animateWithDuration:0.3 animations:^ {
                    [self.view layoutIfNeeded];
                    
                }];
                
                if (value.length==0) {
                    self.maxTxtFld.text=@"";
                    
                    //[self.tfLocationSearch resignFirstResponder];
                    
                }
                
                
                
            }
            
            
        }
        
        
        
    }
    
    
    
    

    
    
    
    
    
    return YES;
    
    if (self.adjustRadiusBtn.selected) {
        
        if (textField == self.minTxtFld) {
            
            NSArray *dotComponents = [textField.text componentsSeparatedByString:@"."];
            
            if (dotComponents.count>1) {
                
                if ([string isEqualToString:@"."]) {
                    return [textField.text componentsSeparatedByString:@"."].count<2;
                }
                
                NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
                NSArray *dotComponentsArrNew = [newString componentsSeparatedByString:@"."];
                if (dotComponentsArrNew.count>1) {
                    NSString *afterDotString = [dotComponentsArrNew objectAtIndex:1];
                    return afterDotString.length<3;
                }
                
            }else{
                
                
            }
            
            NSString *tempStr = textField.text;
            
            tempStr = [tempStr stringByAppendingString:string];
            
            NSString *value = [textField.text stringByReplacingCharactersInRange:range withString:string];
            
            if (![value isEqualToString:@""]) {
                
                [self filterContentForSearchText:value];// scope:[[self.searchDisplayController.searchBar scopeButtonTitles] objectAtIndex:[self.searchDisplayController.searchBar selectedScopeButtonIndex]]];
                
                isFromSearchResult=YES;
                
            }else{
                
                isFromSearchResult=NO;
                
            }
            
            [self.tblRadius layoutIfNeeded];
            [self.tblRadius reloadData];
            [self.tblRadius layoutIfNeeded];
            
            if (isFromSearchResult) {
                
                CGFloat totalHeight = (searchResultArrayFromRadius.count+1) * 32;
                
                if (totalHeight>96) {
                    
                    [self.view layoutIfNeeded];
                    
                    self.constraintViewHolderTableRadiusHeight.constant = 96;
                    
                    [UIView animateWithDuration:0.3 animations:^{
                        
                        [self.view layoutIfNeeded];
                        
                    } completion:^(BOOL finished) {
                        
                    }];
                    
                }else{
                    
                    [self.view layoutIfNeeded];
                    
                    self.constraintViewHolderTableRadiusHeight.constant = totalHeight;
                    
                    [UIView animateWithDuration:0.3 animations:^{
                        
                        [self.view layoutIfNeeded];
                        
                    } completion:^(BOOL finished) {
                        
                    }];
                    
                }
                
            }else{
                
                if (arrRadius.count>0) {
                    
                    [self.view layoutIfNeeded];
                    
                    self.constraintViewHolderTableRadiusHeight.constant = 96;
                    
                    [UIView animateWithDuration:0.3 animations:^{
                        
                        [self.view layoutIfNeeded];
                        
                    } completion:^(BOOL finished) {
                        
                    }];
                    
                }else{
                    
                }
                
            }
            
        }
        
    }else if(self.priceRangeBtn.selected){
        
        if (textField == self.minTxtFld || textField == self.maxTxtFld) {
            
            NSArray *dotComponents = [textField.text componentsSeparatedByString:@"."];
            
            if (dotComponents.count>1) {
                
                if ([string isEqualToString:@"."]) {
                    return [textField.text componentsSeparatedByString:@"."].count<2;
                }
                
                NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
                NSArray *dotComponentsArrNew = [newString componentsSeparatedByString:@"."];
                if (dotComponentsArrNew.count>1) {
                    NSString *afterDotString = [dotComponentsArrNew objectAtIndex:1];
                    return afterDotString.length<3;
                }
                
            }else{
                
            }
            
        }
        
    }
    
    return YES;
}



- (void)keyboardWillShow:(NSNotification*)notification {
    
    NSDictionary *info = [notification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    
    currentHeight = kbSize.height - _currentKeyboardHeight;
    // Write code to adjust views accordingly using deltaHeight
    
    if (self.constraintViewTextFieldHolderBottomSpace.constant == 0) {
        
        [self.view layoutIfNeeded];
        
        currentHeight = currentHeight - self.adjustBtnHeight.constant;
        
        self.constraintViewTextFieldHolderBottomSpace.constant = -currentHeight;
        
        [UIView animateWithDuration:0.2 animations:^{
            
            [self.view layoutIfNeeded];
            
        } completion:^(BOOL finished) {
            
        }];
        
    }
    
    _currentKeyboardHeight = kbSize.height;
}

- (void)keyboardWillHide:(NSNotification*)notification {
    
    // Write code to adjust views accordingly using kbSize.height
    _currentKeyboardHeight = 0.0f;
}

- (void)keyboardWasShown:(NSNotification *)notification{
    
}

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    
    
    if(_btnTime.selected)
    {
        self.datePickerView.hidden = false;
        
        return;
    }
    
    [self createInputAccessoryView];
    // Now add the view as an input accessory view to the selected textfield.
    
    [textField setInputAccessoryView:inputAccView];
    // Set the active field. We' ll need that if we want to move properly
    // between our textfields.
    txtActiveField = textField;
    
    if (self.adjustRadiusBtn.selected) {
        
        if (textField == self.minTxtFld) {
            
           // textField.text = oldRadiusData_TextToShowAtEditing;
            
            [self.view layoutIfNeeded];
            
            //self.constraintViewHolderTableRadiusHeight.constant = 96; by ak
           // self.constraintViewHolderTableRadiusHeight.constant = 0; //by vs
            
          //  self.viewHolderForTableRadius.hidden = NO;
            // self.viewBgTapHandlerForRadiusTable.hidden = NO;
            
            self.viewHolderForTableRadius.alpha = 0.0;
            
            [UIView animateWithDuration:0.3 animations:^{
                
                self.viewHolderForTableRadius.alpha = 1.0;
                
                [self.view layoutIfNeeded];
                
            } completion:^(BOOL finished) {
                
            }];
            
        }
    }
}

-(void)textFieldDidEndEditing:(UITextField *)textField{
    
    
    if (_adjustRadiusBtn.selected) {
        
        enteredLocation = self.maxTxtFld.text;
        rangeLocation = self.minTxtFld.text;
        
        if (self.filterCityViewHeight.constant == 180 || self.filterCityViewHeight.constant == 160)
        {
            [self.view layoutIfNeeded];
            
            self.filterCityViewHeight.constant=0;
            
            [UIView animateWithDuration:0.3 animations:^ {
                [self.view layoutIfNeeded];
                
            }];
            
        }

        
        
        NSLog(@"range and entered location is...%@,%@",enteredLocation,rangeLocation);
        if (enteredLocation.length>0 && rangeLocation.length>0) {
           // [self filterList]; // Comment via VS for stop two times call filter work on first 30 Aug 18...
        }
    }
    else if (_priceRangeBtn.selected)
    {
        maxPriceValue = self.maxTxtFld.text;
        minPriceValue = self.minTxtFld.text;
        
        if (maxPriceValue.length>0 && minPriceValue.length>0) {
           [self filterList];
        }
    }
    else if (_btnTime.selected)
    {
        maxTime = self.maxTxtFld.text;
        minTime = self.minTxtFld.text;
        
        if (minTime.length>0 && maxTime.length>0) {
            [self filterList];
        }
    }
    
    
    
    
    
    
    if (txtActiveField != textField) {
        
        [self.view layoutIfNeeded];
        
        self.constraintViewTextFieldHolderBottomSpace.constant = 0;
        
        [UIView animateWithDuration:0.2 animations:^{
            
            [self.view layoutIfNeeded];
            
        } completion:^(BOOL finished) {
            
        }];
        
        [textField resignFirstResponder];
        
        _currentKeyboardHeight = 0.0f;
        
    }
    
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    
    
    NSLog(@"text field should return called");
//    
//    [self.view layoutIfNeeded];
//    
//    self.constraintViewTextFieldHolderBottomSpace.constant = 0;
//    
//    [UIView animateWithDuration:0.2 animations:^{
//        
//        [self.view layoutIfNeeded];
//        
//    } completion:^(BOOL finished) {
//        
//    }];
//    
//    [textField resignFirstResponder];
//    
//    _currentKeyboardHeight = 0.0f;
//    
//    [_minTxtFld resignFirstResponder];
//    [_maxTxtFld resignFirstResponder];
//    
//    if (self.adjustRadiusBtn.selected) {
//        
//        filterRange = oldRadiusData_TextToShowAtEditing;
//        filterLocationName = self.maxTxtFld.text;
//        
//        if ([filterRange isEqualToString:@""]) {
//            
//            oldRadiusData_TextToShowAtEditing = @"";
//            
//            oldRadiusData_TextToShowAsPlaceHolder = [NSString stringWithFormat:@"All"];
//            
//        }else{
//            
//          //  oldRadiusData_TextToShowAtEditing = filterRange;
//            
//         //   oldRadiusData_TextToShowAsPlaceHolder = [NSString stringWithFormat:@"Within %@ miles",oldRadiusData_TextToShowAtEditing];
//            
//        }
//        
//        isFromRadiusTableTap = true;
//        
//        //    [self hideRadiusTableView:self.tapGestureRecognizerForHidingRadiusTableView];
//        
//    }else if(self.priceRangeBtn.selected){
//        
//        filterPriceMin = self.minTxtFld.text;
//        filterPriceMax = self.maxTxtFld.text;
//        
//    }
    
    [self filterList];
    
    return YES;
}


- (IBAction)hideRadiusTableView:(id)sender {
    
    
    
    if (!isFromRadiusTableTap) {
        
        oldRadiusData_TextToShowAtEditing = self.minTxtFld.text;
        
        filterRange = oldRadiusData_TextToShowAtEditing;
        
        if ([filterRange isEqualToString:@""]) {
            
            oldRadiusData_TextToShowAtEditing = @"";
            
            oldRadiusData_TextToShowAsPlaceHolder = [NSString stringWithFormat:@"All"];
            
        }else{
            
          //  oldRadiusData_TextToShowAtEditing = filterRange;
            
          //  oldRadiusData_TextToShowAsPlaceHolder = [NSString stringWithFormat:@"Within %@ miles",oldRadiusData_TextToShowAtEditing];
            
        }
        
        // [self filterList];
        
    }
    
    self.minTxtFld.text = oldRadiusData_TextToShowAsPlaceHolder;
    
    isFromRadiusTableTap = false;
    
    if(self.constraintViewTextFieldHolderBottomSpace.constant != 0){
        
        [self.view layoutIfNeeded];
        
        self.constraintViewTextFieldHolderBottomSpace.constant = 0;
        
        [UIView animateWithDuration:0.3 animations:^{
            
            [self.view layoutIfNeeded];
            
        } completion:^(BOOL finished) {
            
        }];
        
        [txtActiveField resignFirstResponder];
        
        _currentKeyboardHeight = 0.0f;
        
    }
    
    if (self.constraintViewHolderTableRadiusHeight.constant > 0) {
        
        [self.view layoutIfNeeded];
        
        self.constraintViewHolderTableRadiusHeight.constant = 0;
        
        self.viewHolderForTableRadius.alpha = 1.0;
        
        self.viewBgTapHandlerForRadiusTable.hidden = YES;
        
        [UIView animateWithDuration:0.3 animations:^{
            
            self.viewHolderForTableRadius.alpha = 0.0;
            
            [self.view layoutIfNeeded];
            
        } completion:^(BOOL finished) {
            
            self.viewHolderForTableRadius.hidden = YES;
            
            isFromSearchResult = false;
            
            [self.tblRadius layoutIfNeeded];
            [self.tblRadius reloadData];
            [self.tblRadius layoutIfNeeded];
            
        }];
        
    }
    
}

@end
