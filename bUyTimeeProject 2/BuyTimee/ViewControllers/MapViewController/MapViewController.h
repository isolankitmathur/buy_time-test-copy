//
//  MapViewController.h
//  HandApp
//
//  Created by  ~ on 07/03/16.
//  Copyright (c) 2016 . All rights reserved.
//




//0.949999988079071

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "Flurry.h"

@interface MapViewController : UIViewController<UITextFieldDelegate,MKMapViewDelegate,UITableViewDataSource,UITableViewDelegate>


- (IBAction)AdjustRadiusBtnClicked:(id)sender;
- (IBAction)priceRangeBtnClicked:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnTime;

@property (weak, nonatomic) IBOutlet UIDatePicker *datePickerView;
@property (weak, nonatomic) IBOutlet UIView *parentFilterView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consBtnTimeLeading;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consBtnPriceLeading;


- (IBAction)timeBtnClicked:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *filterView;

@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (nonatomic, strong) MKPolyline *routeLine; //your line

@property (weak, nonatomic) IBOutlet UITextField *minTxtFld;
@property (weak, nonatomic) IBOutlet UITextField *maxTxtFld;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *adjustBtnHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *blurViewHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *minLeft;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *minTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *minTxtFldLeft;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *minTxtFldWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *maxLeft;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *maxTxtFldLeft;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *maxTxtFldWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintViewTextFieldHolderBottomSpace;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *minTxtFldHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *maxTxtFldHeight;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *minTxtFldTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *maxTxtFldTop;

@property (weak, nonatomic) IBOutlet UILabel *minLabel;
@property (weak, nonatomic) IBOutlet UILabel *maxLabel;
@property (weak, nonatomic) IBOutlet UIButton *adjustRadiusBtn;
@property (weak, nonatomic) IBOutlet UIButton *priceRangeBtn;

-(void)pinTapped:(id)sender;

@property (weak, nonatomic) IBOutlet UILabel *lblCategoryPlaceHolder;
@property (weak, nonatomic) IBOutlet UIImageView *imgArrow;

@property (weak, nonatomic) IBOutlet UIView *viewCategoryTapHandler;
@property (weak, nonatomic) IBOutlet UITableView *tblCategory;
@property (weak, nonatomic) IBOutlet UITableView *tblRadius;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintCategoryViewHeight;

@property (nonatomic, retain) UITextField *txtActiveField;
@property (nonatomic, retain) UIView *inputAccView;
@property (nonatomic, retain) UIButton *btnDone;
@property (nonatomic, retain) UIButton *btnNext;
@property (nonatomic, retain) UIButton *btnPrev;

-(void)addAllPins;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *parentFilterViewHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *radiusBtnBottom;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *radiusBtnTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lblSeperatorCtgryBottom;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lblPlaceHolderCatgryTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *arrowImgTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewMapHolderTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *radiusBtnFinalBottom;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *arrowImgRight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *maxTop;

@property (weak, nonatomic) IBOutlet UIView *viewCategoryView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lblPlaceHolderCgryHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewTapHandlerHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consCategoryTblViewTop;


@property (weak, nonatomic) IBOutlet UIView *filterCityView;
@property (weak, nonatomic) IBOutlet UITableView *cityNameTableView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *filterCityViewWidth;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *filterCityViewHeight;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *filterCityViewBottom;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *filterCityViewLeadingFromMaxTxtFld;

@property(nonatomic,strong)NSIndexPath *lastSelectedIndexPath;

@end
