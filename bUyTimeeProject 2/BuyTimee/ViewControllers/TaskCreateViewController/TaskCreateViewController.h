//
//  TaskCreateViewController.h
//  HandApp
//
//  Created by  ~ on 02/03/16.
//  Copyright (c) 2016 . All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Flurry.h"
#import "BraintreeCore.h"

@interface TaskCreateViewController : UIViewController<UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource,UITextViewDelegate>

@property (nonatomic, strong) BTAPIClient *braintreeClient;


@property (weak, nonatomic) IBOutlet UITextField *txtFld1;  //tasktitle
@property (weak, nonatomic) IBOutlet UITextField *txtFld2;  //description
@property (weak, nonatomic) IBOutlet UITextField *txtFld3;  //bounty
@property (weak, nonatomic) IBOutlet UITextField *txtFld4;  //start date
@property (weak, nonatomic) IBOutlet UITextField *txtFld5;  //end date
@property (weak, nonatomic) IBOutlet UITextField *txtFld6;  //task location

- (IBAction)ChooseCategory:(id)sender;
- (IBAction)SubCategory:(id)sender;

@property (weak, nonatomic) IBOutlet UILabel *uploadLbl;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *creatBtnHeight;

@property (weak, nonatomic) IBOutlet UIButton *chooseBtn;

@property (weak, nonatomic) IBOutlet UILabel *lblLineChooseBtn;


@property (weak, nonatomic) IBOutlet UIButton *subBtn;
@property (weak, nonatomic) IBOutlet UITableView *tblView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tblHeight;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *_tblTop;

- (IBAction)takeAPictureClicked:(id)sender;
- (IBAction)photoLibraryClicked:(id)sender;

@property (weak, nonatomic) IBOutlet UIView *viewTableTapHandler;
@property (weak, nonatomic) IBOutlet UIView *viewInsideHolder;
@property (weak, nonatomic) IBOutlet UIView *viewMainHolder;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintTextFieldHeight;     //60 in 800

@property (nonatomic, retain) UITextField *txtActiveField;
@property (nonatomic, retain) UITextView *txtViewActiveField;



@property (nonatomic, retain) UIView *inputAccView;
@property (nonatomic, retain) UIView *DatePickerInputAccView;

@property (nonatomic, retain) UIButton *btnDone;
@property (nonatomic, retain) UIButton *btnNext;
@property (nonatomic, retain) UIButton *btnPrev;

- (IBAction)closeOpenTable:(id)sender;

@property (weak, nonatomic) IBOutlet UIView *blurView;
@property (weak, nonatomic) IBOutlet UIImageView *blurViewImgeView;
@property (weak, nonatomic) IBOutlet UIButton *blurViewCrossBtn;
- (IBAction)blurViewCrossBtn:(id)sender;
- (IBAction)HideBlurViewTap:(id)sender;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *ConsWantCashBtnBottom;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consHelpMeBtnTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consHelpMeBtnLeft;
@property (weak, nonatomic) IBOutlet UITextField *titleTextFld;
- (IBAction)dateTimeBtnClicked:(id)sender;
- (IBAction)wantCashBtnClicked:(id)sender;
- (IBAction)creditFineBtnClciked:(id)sender;
- (IBAction)helpMeDecideBtnClciked:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *wantCashBtn;
@property (weak, nonatomic) IBOutlet UIButton *creditFineBtn;
@property (weak, nonatomic) IBOutlet UIButton *dateTimeBtn;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consWantCashBtnHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consCashBtnWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consWantCashBtnTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consWantCashBtnLeft;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consHelpMeDecideRight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consHelpMeHeight;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consTitleTxtFldHeight;



@property (weak, nonatomic) IBOutlet NSLayoutConstraint *alertViewTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *alertViewLeft;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *alertViewWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *alertViewHeight;
@property (weak, nonatomic) IBOutlet UIView *alertView;
- (IBAction)okBtnClciked:(id)sender;




@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewMainHolderHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewInsideHolderHeight;
@property (weak, nonatomic) IBOutlet UIButton *helpMeDecideBtn;
@property (weak, nonatomic) IBOutlet UIButton *saveAndPublishBtn;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *saveAndPublishBtnBottom;



@property (weak, nonatomic) IBOutlet NSLayoutConstraint *chooseBtnTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lblTopTittleTxtFld;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *descriptionTxtFldTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lblTopDescriptionTxtFldTop;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *businessTxtFldTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lblTopBusinessTxtFld;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *dateAndTimeBtnTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lblTopDateAndTime;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *titleTxtFldTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *locationTxtFldTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lblTopLocationTxtFld;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *priceTxtFldTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lblTopPriceTxtFld;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lblTopChooseBtn;


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *chooseBtnWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *chooseBtnHeight;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *titleTxtFldHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *titleTxtFldWidth;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *descriptionTxtFldHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *descriptionTxtFldWidth;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *businessTxtFldHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *businessTxtFldWidth;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *dateAndTimeBtnWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *dateAndTimeBtnHeight;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *locationTxtFldHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *locationTxtFldWidth;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *priceTxtFldHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *priceTxtFldWidth;


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *catgryLineWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *titleLineWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *descriptionLineWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *businessLineWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *locationLineWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *priceLineWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *dateAndTimeLineWidth;


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imgViewTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imgViewLeft;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imgViewHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imgViewWidth;




@property (weak, nonatomic) IBOutlet NSLayoutConstraint *creditFineBtnTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *creditFineBtnLeft;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *creditIsFineWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *creditIsFineHeight;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consHelpMeWidth;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *chooseBtnLeft;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *titleTxtFldLeft;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *descriptionTxtFldLeft;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *businessTxtFldLeft;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *dateAndTimeLeft;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *locationLeft;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *priceTxtFldLeft;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tblViewWidth;


@property (weak, nonatomic) IBOutlet UITextField *quantityTxtFld;
@property (weak, nonatomic) IBOutlet UILabel *quantityLineLbl;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *quantityTxtFldWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *quantityLineLblWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *circleImgViewIconRight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *quantityTxtFldTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *quantityTxtFldHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *quantityLineLblLeft;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *quantityTxtFldLeft;

@property (weak, nonatomic) IBOutlet UITextField *discountTxtFld;
@property (weak, nonatomic) IBOutlet UILabel *discountLinelbl;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *discountTxtFkdWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *discountLineLblWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *discountTxtFldTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *discountTxtFldHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *discountLineLblLeft;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *discountTxtFldLeft;

@property (weak, nonatomic) IBOutlet UITextField *termsAndConditionTxtFld;
@property (weak, nonatomic) IBOutlet UILabel *termsLineLbl;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *termsTxtFldWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *termsTxtFldHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *termsTxtFldTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *termsLblWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *termsLblHeight;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *collectionViewTop;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *timeBtnLeft;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *timeBtnWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *timeBtnHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *timeBtnTop;

@property (weak, nonatomic) IBOutlet UIButton *timeBtn;
- (IBAction)timeBtnClciked:(id)sender;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *timeLblLeft;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *timeLblWidth;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *termsTxtFLdLeft;
@property (weak, nonatomic) IBOutlet UILabel *timeLineLbl;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *premiumBtnTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *premiumBtnLeft;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *premiumBtnHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *premiumBntWidth;
@property (weak, nonatomic) IBOutlet UIButton *premiumBtn;
- (IBAction)premiumBtnClicked:(id)sender;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *discountBtnTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *discountBtnWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *discountBtnHeight;
@property (weak, nonatomic) IBOutlet UIButton *discountBtn;
- (IBAction)discountBtnClicked:(id)sender;


//new wrok by KP

@property (strong, nonatomic) IBOutlet UITextView *descriptionTextView;
@property (strong, nonatomic) IBOutlet UITextView *termsAndConditionTextView;
@property (strong, nonatomic) IBOutlet UILabel *businessLineLbl;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *descriptionLineLblTop;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *collectionViewLeft;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *collectionViewWidth;
@property (strong, nonatomic) IBOutlet UILabel *descriptionTxtViewLineLbl;


@property (weak, nonatomic) IBOutlet UILabel *lblOnBlurView;

@property (weak, nonatomic) IBOutlet UIButton *titleHelpBtn;

- (IBAction)titleHelpClicked:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *discriptionHelpBtn;
- (IBAction)discriptionHelpClicked:(id)sender;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *nsconstraintsTitleHelpBtnWidth;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *nsconstraintsTitleHelpBtnHeight;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *nsconstraintsDiscriptionHelpBtnWidth;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *nsconstraintsDiscriptionHelpBtnHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *nsconstraintsTitleHelpBtnTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *nsconstraintsTitleHelpBtnLeading;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *nsconstraintsDiscriptionHelpBtnTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *nsconstraintsDiscriptionHelpBtnLeading;
@property (weak, nonatomic) IBOutlet UIButton *locationHelpBtn;
- (IBAction)locatioHelpClicked:(id)sender;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *nsconstraintsLocationHelpWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *nsconstraintsLocationHelpHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *nsconstraintsLocationHelpTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *nsconstraintsLocationHelpLeading;
@property (weak, nonatomic) IBOutlet UILabel *dateLineLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *quantityLineLblTop;
@property (assign, nonatomic) BOOL isImagePickFromAlbum;

@end
