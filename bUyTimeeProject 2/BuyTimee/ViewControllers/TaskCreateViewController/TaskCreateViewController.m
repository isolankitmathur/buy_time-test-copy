//
//  TaskCreateViewController.m
//  HandApp
//
//  Created by  ~ on 02/03/16.
//  Copyright (c) 2016 . All rights reserved.
//

#import "TaskCreateViewController.h"
#import "SelectCategoryTableViewCell.h"
#import "Constants.h"
#import "GlobalFunction.h"
#import "TaskCollectionViewCell.h"
#import "category.h"
#import "subCategory.h"
#import "TaskTableViewCell.h"
#import <CTAssetsPickerController/CTAssetsPickerController.h>
#import <AVFoundation/AVFoundation.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import <Photos/Photos.h>
#import "BusinessInfo.h"

#import "BraintreePayPal.h"
#import "PPDataCollector.h"

@interface TaskCreateViewController () <BTAppSwitchDelegate, BTViewControllerPresentingDelegate,UIImagePickerControllerDelegate,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,CTAssetsPickerControllerDelegate>{
    
    BOOL show;
    BOOL show1;
    
    BOOL click1;
    BOOL click2;
    
    
    BOOL helpMeBtnClciked;
     BOOL dateBtnClicked;
     BOOL timeBtnClicked;
    
    
  //  UIDatePicker *datePicker;
    NSDateFormatter *_dateFormatter;
    
    __weak IBOutlet UIButton *btnCreateTask;
    
    
    UIToolbar *toolBar;
    UIButton *checkBoxBtn;
    UIButton *timeCheckBoxBtn;
    BOOL anyDateBtnClicked;
    BOOL anyTimeBtnClicked;
    BOOL isPremiumSelected;
    BOOL isDiscountSelected;
    NSString *alertString;
    NSString *selectedBusinessName;
    NSString *reservationCreateStatus;
    
    NSString *editStatusForNavigation;
     NSString *whichButtonClicked;
    NSString *pricemain;
    NSString *discountmain;
    
    NSString *editStatus;
    NSString *reservationId_ForEdit;
    
    BOOL reservationChangePossible;
    BOOL reservationChangeDIscount;
    
    NSString *deviceData;
    NSString *nonce;
    
}

//@property (nonatomic, strong) BTAPIClient *braintreeClient;
@property (nonatomic, strong) BTPayPalDriver *payPalDriver;

@property (weak, nonatomic) IBOutlet UICollectionView *collectionViewTaskImages;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollViewHolder;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintCollectionViewHeight;

- (IBAction)deleteTaskImage:(id)sender;

- (IBAction)createTask:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnTakeAPicture;
@property (weak, nonatomic) IBOutlet UIButton *btnPhotoLibrary;

- (IBAction)openCameraToCaptureTaskImages:(id)sender;

@end

@implementation TaskCreateViewController{
    
    UIImage *PickImage;
    NSMutableArray *pics;
    NSMutableArray *picsNameArray;
    NSIndexPath *indexValue;
    
    NSMutableArray *catArr;
    NSMutableArray *subCatArr;
    
    category *selectedCatObj;
    subCategory *selectedSubCatObj;
    BusinessInfo *selectedBusinessObj;
    
    
    NSMutableArray *selectedSubCatArr;
    
    NSDate *dateStart;
    NSDate *dateEnd;
    
    BOOL isOpened;
    
    NSInteger indexForTableRow;
    CGPoint centerTogoBack;
    
    BOOL wantCashBtnClciked;
    BOOL creditFineBtnClciked;
    UIDatePicker* picker;
    UIDatePicker* timePicker;

    
    BOOL bussinessTextFldClicked;
    NSMutableArray *businessData;
    NSArray *searchResultArrayFromRadius;
    BOOL isFromSearchResult;

    
    NSString *paymentMethod;
    NSString *startTime;
    NSString *startDate;
    BOOL businessTableOpen;
    
    
    NSString *startDateForShare;
    NSString *businessNameForShare;
    
    
    BOOL premiumBtnClciked;
    BOOL discountBtnClicked;

    CGRect newFrameDescriptionTxtView;
    CGRect newFrameTermsAndConditionTxtView;
    BOOL isImagePickFromAlbum;
    int heightOfTermsTxtView;

    
    
    
}

@synthesize txtActiveField;
@synthesize inputAccView;
@synthesize btnDone;
@synthesize btnNext;
@synthesize btnPrev;

- (void)viewDidLoad {
    [super viewDidLoad];

//    self.creditIsFineHeight.constant = 0;
//    self.consWantCashBtnHeight.constant = 0;
    
    reservationChangePossible = YES;
    reservationChangeDIscount = YES;
    editStatus = @"0";
    reservationId_ForEdit = @"";
    isFromSearchResult = NO;
    whichButtonClicked = @"";
    self.discountTxtFld.hidden = true;
    self.discountLinelbl.hidden = true;
    
   // [_dateTimeBtn setTitle:@"Date/Time*" forState:UIControlStateNormal];

    myAppDelegate.vcObj.btnMapOnListView.hidden = true;//vs
    myAppDelegate.vcObj.BtnTransparentMap.hidden = true;//vs
    
    _quantityLineLbl.alpha=0.0;
    _quantityTxtFld.alpha=0.0;
    
    _discountLinelbl.alpha=0.0;
    _discountTxtFld.alpha=0.0;

   // _termsAndConditionTxtFld.alpha=0.0;
    
    _termsAndConditionTextView.alpha=0.0;
    
    _timeBtn.alpha=0.0;
    _timeLineLbl.alpha=0.0;

    anyTimeBtnClicked=YES;
    anyDateBtnClicked=YES;
    isPremiumSelected=YES;
    
     checkBoxBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [checkBoxBtn setFrame:CGRectMake(0, 0, 140, 44)];
    [checkBoxBtn setTitle:@"Any Date" forState:UIControlStateNormal];
    [checkBoxBtn.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:17]];
     checkBoxBtn.tintColor=[UIColor colorWithRed:63.0/255.0f green:67.0/255.0f blue:70.0/255.0f alpha:1.0];
    [checkBoxBtn setImage:[UIImage imageNamed:@"Unchecked"] forState:UIControlStateNormal];
    [checkBoxBtn setTitleColor:[UIColor colorWithRed:63.0/255.0f green:67.0/255.0f blue:70.0/255.0f alpha:1.0] forState:UIControlStateNormal];
    [checkBoxBtn setTitleEdgeInsets:UIEdgeInsetsMake(15,0,17,-25)];  //top,left,bottom,right
    [checkBoxBtn setImageEdgeInsets:UIEdgeInsetsMake(13, 58, 14,100)];
    
    timeCheckBoxBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [timeCheckBoxBtn setFrame:CGRectMake(0, 0, 140, 44)];
    [timeCheckBoxBtn setTitle:@"Any Time" forState:UIControlStateNormal];
    [timeCheckBoxBtn.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:17]];
    timeCheckBoxBtn.tintColor=[UIColor colorWithRed:63.0/255.0f green:67.0/255.0f blue:70.0/255.0f alpha:1.0];
    [timeCheckBoxBtn setImage:[UIImage imageNamed:@"Unchecked"] forState:UIControlStateNormal];
    [timeCheckBoxBtn setTitleEdgeInsets:UIEdgeInsetsMake(18,0,19,-25)];
    [timeCheckBoxBtn setTitleColor:[UIColor colorWithRed:63.0/255.0f green:67.0/255.0f blue:70.0/255.0f alpha:1.0] forState:UIControlStateNormal];//top,left,bottom,right
    [timeCheckBoxBtn setImageEdgeInsets:UIEdgeInsetsMake(13, 58, 14,100)];//(12, 20, 15,100)


    if(IS_IPHONE_6)
    {
        
        _premiumBtnHeight.constant=36;
        _discountBtnHeight.constant=36;
//        _txtFld6.backgroundColor=[UIColor redColor];
        
         _priceTxtFldWidth.constant=270;
         _priceLineWidth.constant=277;
        
        _locationTxtFldWidth.constant=307;
        _locationLineWidth.constant=340;
        
        _termsTxtFldHeight.constant=0;
        _termsLblHeight.constant=0;
        _businessTxtFldTop.constant = 18;
//        [_premiumBtn setTitle:@"" forState:UIControlStateNormal];
//        [_discountBtn setTitle:@"" forState:UIControlStateNormal];
        
//        _premiumBtnHeight.constant=0;
//        _discountBtnHeight.constant=0;
//        _dateAndTimeBtnTop.constant=0;
//        _timeBtnTop.constant=0;

        
    }
    else if (IS_IPHONE_5 || IS_IPHONE_4_OR_LESS)
    {
        _priceTxtFldWidth.constant=235;
        _priceLineWidth.constant=245;
        
        _termsTxtFldWidth.constant=0;
        _termsTxtFldHeight.constant=0;

        _termsLblWidth.constant = 0;
        
//        _premiumBtnHeight.constant=0;
//        _discountBtnHeight.constant=0;
        
        _premiumBtnHeight.constant = 30;
        _discountBtnHeight.constant = 30;
        
        _premiumBtnTop.constant = 16;
        _discountBtnTop.constant = 16;

//        [_premiumBtn setTitle:@"" forState:UIControlStateNormal];
//        [_discountBtn setTitle:@"" forState:UIControlStateNormal];
        
        

        

    }
    else if (IS_IPHONE_6P)
    {
        _priceTxtFldWidth.constant=300;
        _priceLineWidth.constant=314;
        
        _termsTxtFldHeight.constant=0;
        _termsLblHeight.constant=0;
        
//        _premiumBtnHeight.constant=0;
//        _discountBtnHeight.constant=0;

        _premiumBtnHeight.constant=40;
        _discountBtnHeight.constant=40;

//        [_premiumBtn setTitle:@"" forState:UIControlStateNormal];
//        [_discountBtn setTitle:@"" forState:UIControlStateNormal];

    }
    
    [_premiumBtn setTitle:@"PREMIUM" forState:UIControlStateNormal];
    [_discountBtn setTitle:@"DISCOUNT" forState:UIControlStateNormal];
    
    [self.premiumBtn setTitleColor:[UIColor colorWithRed:255.0/255.0f green:255.0/255.0f blue:255.0/255.0f alpha:1.0] forState:UIControlStateNormal];
    _premiumBtn.backgroundColor=[UIColor colorWithRed:63.0/255.0f green:67.0/255.0f blue:70.0/255.0f alpha:1.0];
    
    [self.discountBtn setTitleColor:[UIColor colorWithRed:63.0/255.0f green:67.0/255.0f blue:70.0/255.0f alpha:1.0] forState:UIControlStateNormal];
    _discountBtn.backgroundColor=[UIColor colorWithRed:244.0/255.0f green:244.0/255.0f blue:244.0/255.0f alpha:1.0];
    
    
    [self.wantCashBtn setTitleColor:[UIColor colorWithRed:63.0/255.0f green:67.0/255.0f blue:70.0/255.0f alpha:1.0] forState:UIControlStateNormal];

    
    _saveAndPublishBtn.backgroundColor=[UIColor colorWithRed:63.0/255.0f green:67.0/255.0f blue:70.0/255.0f alpha:1.0];

    _creditFineBtn.backgroundColor=[UIColor colorWithRed:63.0/255.0f green:67.0/255.0f blue:70.0/255.0f alpha:1.0];
    
//    [self.premiumBtn setTitleColor:[UIColor colorWithRed:63.0/255.0f green:67.0/255.0f blue:70.0/255.0f alpha:1.0] forState:UIControlStateNormal];
//
//    _discountBtn.backgroundColor=[UIColor colorWithRed:63.0/255.0f green:67.0/255.0f blue:70.0/255.0f alpha:1.0];

    
    
    
    
    
    self.tblView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
//    
//    NSLog(@"array count is...%lu",(unsigned long)myAppDelegate.arrayBusinessDetail.count);
//    
//    NSMutableArray *BusinessArr = [DatabaseClass getDataFromBusinessData:[[NSString stringWithFormat:@"Select * from %@",tableBusinessInfo] UTF8String]];
//    
//    myAppDelegate.arrayBusinessDetail = [[NSMutableArray alloc]init];
//    
//    for (BusinessInfo *businessObjs in BusinessArr) {
//        
//        if (![myAppDelegate.arrayBusinessDetail containsObject:businessObjs]) {
//            [myAppDelegate.arrayBusinessDetail addObject:businessObjs];
//        }
//    }
    
    NSLog(@"array count is...%lu",(unsigned long)myAppDelegate.arrayBusinessDetail.count);
    


    
    businessData=[[NSMutableArray alloc]init];
    
    
    NSLog(@"%lu",(unsigned long)myAppDelegate.arrayBusinessDetail.count);
    
    for(int i=0;i<myAppDelegate.arrayBusinessDetail.count; i++)
    {
    
    BusinessInfo *businessObj = [myAppDelegate.arrayBusinessDetail objectAtIndex:i];
    
    NSLog(@"---- BusinessName ---- %@",[EncryptDecryptFile decrypt:businessObj.BusinessName]);
    NSLog(@"---- BusinessId---- %@",[EncryptDecryptFile decrypt:businessObj.BusinessId]);
     NSLog(@"---- categoryId---- %@",[EncryptDecryptFile decrypt:businessObj.categoryId]);
        
    
    [businessData addObject:[EncryptDecryptFile decrypt:businessObj.BusinessName]];
       // NSLog(@"%@",businessData);
    
    }
    
   // NSLog(@"%@",businessData);

    
    _alertView.hidden=YES;
     
    
    if(IS_IPHONE5)
    {
         _viewMainHolderHeight.constant=460;
    }
    else if (IS_IPHONE_6)
    {
        _viewMainHolderHeight.constant=530;
    }
    else if (IS_IPHONE_6P)
    {
        _viewMainHolderHeight.constant=580;
    }
    else if (IS_IPHONE_4_OR_LESS)
    {
        _viewMainHolderHeight.constant=460;

    }
    
    
    
     picker = [[UIDatePicker alloc] init];
    timePicker= [[UIDatePicker alloc] init];
    [picker setMinimumDate: [NSDate date]];
    
//    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"NL"];
//    [picker setLocale:locale];
    
  // picker.locale = [NSLocale localeWithLocaleIdentifier:@"en_US"];
    //picker.locale = [NSLocale localeWithLocaleIdentifier:@"en_GB"];
//    self.scrollViewHolder.contentOffset = CGPointMake(0, 200);
    
 //   _txtFld1.delegate=self;
    _txtFld2.delegate=self;
    _txtFld3.delegate=self;
    _txtFld4.delegate=self;
    _txtFld5.delegate=self;
    _txtFld6.delegate=self;
    _titleTextFld.delegate=self;
    _quantityTxtFld.delegate=self;
    _discountTxtFld.delegate=self;
    _termsAndConditionTxtFld.delegate=self;
    
    
    _descriptionTextView.delegate=self;
    _termsAndConditionTextView.delegate=self;

    
    self.txtFld3.keyboardType = UIKeyboardTypeDecimalPad;
    self.discountTxtFld.keyboardType=UIKeyboardTypeNumberPad;
    
    show=true;
    show1=true;
    
    _tblView.hidden=YES;
    _tblView.delegate=self;
    _tblView.dataSource=self;
    
    pics = [[NSMutableArray alloc]init];
    picsNameArray = [[NSMutableArray alloc]init];
    
    //**************************
    
    
    UISwipeGestureRecognizer *swipeLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(imageSwipe:)];
    UISwipeGestureRecognizer *swipeRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(imageSwipe:)];
    
    [swipeLeft setDirection:UISwipeGestureRecognizerDirectionLeft];
    [swipeRight setDirection:UISwipeGestureRecognizerDirectionRight];
    
    [_blurViewImgeView addGestureRecognizer:swipeLeft];
    [_blurViewImgeView addGestureRecognizer:swipeRight];
    
    self.blurViewCrossBtn.hidden = true;
    self.blurView.hidden=YES;
    self.blurViewImgeView.hidden = true;
    
    self.blurViewImgeView.contentMode = UIViewContentModeScaleAspectFill;
    
    self.blurViewImgeView.clipsToBounds = YES;
    self.blurViewImgeView.userInteractionEnabled=YES;
    
    //***************************
    
    [GlobalFunction createBorderforView:self.tblView withWidth:1.0 cornerRadius:0.0 colorForBorder:RGB(151, 151, 151)];
    
//    datePicker = [[UIDatePicker alloc]init];
//    datePicker.minimumDate = [NSDate date];
//    datePicker.timeZone = [NSTimeZone systemTimeZone];
//    [datePicker setDate:[NSDate date]];
//    [datePicker addTarget:self action:@selector(updateTextField:) forControlEvents:UIControlEventValueChanged];
//    [datePicker setDatePickerMode:UIDatePickerModeDate];
    
    
    NSAttributedString *star = [[NSAttributedString alloc] initWithString:@"*" attributes:@{ NSForegroundColorAttributeName :[UIColor redColor],
                                                                                             }];
    
    _termsAndConditionTextView.text = @"Terms and conditions";
    _termsAndConditionTextView.textColor = [UIColor colorWithRed:143.0/255.0f green:143.0/255.0f blue:143.0/255.0f alpha:1.0];
    ;
    [self addInputViewToTextView:self.termsAndConditionTextView];


    
    
    
    _descriptionTextView.text = @"Description";
    _descriptionTextView.textColor = [UIColor colorWithRed:143.0/255.0f green:143.0/255.0f blue:143.0/255.0f alpha:1.0];
    ;
    [self addInputViewToTextView:self.descriptionTextView];

    
    NSAttributedString *str1 = [[NSAttributedString alloc] initWithString:@"Description" attributes:@{ NSForegroundColorAttributeName :[UIColor colorWithRed:143.0/255.0f green:143.0/255.0f blue:143.0/255.0f alpha:1.0],
                                                                                                         }];
    _txtFld1.attributedPlaceholder = str1;
    
    NSMutableAttributedString *str2 = [[NSMutableAttributedString alloc] initWithString:@"Business Name and Address" attributes:@{ NSForegroundColorAttributeName : [UIColor colorWithRed:143.0/255.0f green:143.0/255.0f blue:143.0/255.0f alpha:1.0],
                                                                                                                               }];
    [str2 appendAttributedString:star];
    _txtFld2.attributedPlaceholder = str2;
    
    NSAttributedString *str3 = [[NSAttributedString alloc] initWithString:@"Specify bounty" attributes:@{ NSForegroundColorAttributeName : [UIColor colorWithRed:143.0/255.0f green:143.0/255.0f blue:143.0/255.0f alpha:1.0],
                                                                                                          }];
    _txtFld3.attributedPlaceholder = str3;
    
    NSMutableAttributedString *str4 = [[NSMutableAttributedString alloc] initWithString:@"Location" attributes:@{ NSForegroundColorAttributeName : [UIColor colorWithRed:143.0/255.0f green:143.0/255.0f blue:143.0/255.0f alpha:1.0],
                                                                                                              }];
    [str4 appendAttributedString:star];
    _txtFld4.attributedPlaceholder = str4;
    
    [self addInputViewToTextField:self.txtFld4];
    
//    NSAttributedString *str5 = [[NSAttributedString alloc] initWithString:@"Price" attributes:@{ NSForegroundColorAttributeName : [UIColor colorWithRed:143.0/255.0f green:143.0/255.0f blue:143.0/255.0f alpha:1.0],
//                                                                                                            }];
//    _txtFld5.attributedPlaceholder = str5;
//    
//    [self addInputViewToTextField:self.txtFld5];
    
    NSMutableAttributedString *str6 = [[NSMutableAttributedString alloc] initWithString:@"Price" attributes:@{ NSForegroundColorAttributeName : [UIColor colorWithRed:143.0/255.0f green:143.0/255.0f blue:143.0/255.0f alpha:1.0],
                                                                                                                 }];
    [str6 appendAttributedString:star];
    _txtFld6.attributedPlaceholder = str6;
    
    [_alertView.layer setCornerRadius:5.0f];

    
    
    NSMutableAttributedString *strTitle = [[NSMutableAttributedString alloc] initWithString:@"Title" attributes:@{ NSForegroundColorAttributeName : [UIColor colorWithRed:143.0/255.0f green:143.0/255.0f blue:143.0/255.0f alpha:1.0],                                                                                                       }];
    
//    NSLog(@"length is...%ld",strTitle.length);
//    
//    [strTitle addAttribute:NSFontAttributeName
//                      value:[UIFont systemFontOfSize:9.0]
//                      range:NSMakeRange(5, 70)];
    
    [strTitle appendAttributedString:star];
    _titleTextFld.attributedPlaceholder = strTitle;
    
   // NSMutableAttributedString *hogan = [[NSMutableAttributedString alloc] initWithString:@"Presenting the great... Hulk Hogan!"];
//    [hogan addAttribute:NSFontAttributeName
//                  value:[UIFont systemFontOfSize:20.0]
//                  range:NSMakeRange(24, 11)];
    
    NSMutableAttributedString *strQuantityTitle = [[NSMutableAttributedString alloc] initWithString:@"Discount" attributes:@{ NSForegroundColorAttributeName : [UIColor colorWithRed:143.0/255.0f green:143.0/255.0f blue:143.0/255.0f alpha:1.0],
                                                                                                     }];
    
    [strQuantityTitle appendAttributedString:star];
    self.quantityTxtFld.attributedPlaceholder = strQuantityTitle;

    NSAttributedString *strDiscountTitle = [[NSAttributedString alloc] initWithString:@"Quantity" attributes:@{ NSForegroundColorAttributeName : [UIColor colorWithRed:143.0/255.0f green:143.0/255.0f blue:143.0/255.0f alpha:1.0],
                                                                                                                }];
    _discountTxtFld.attributedPlaceholder = strDiscountTitle;
    
    
    NSAttributedString *strTermsTitle = [[NSAttributedString alloc] initWithString:@"Terms and conditions" attributes:@{ NSForegroundColorAttributeName : [UIColor colorWithRed:143.0/255.0f green:143.0/255.0f blue:143.0/255.0f alpha:1.0],
                                                                                                                }];
    _termsAndConditionTxtFld.attributedPlaceholder = strTermsTitle;

    
   
    
    NSMutableAttributedString *catName = [[NSMutableAttributedString alloc] initWithString:@"Choose Category" attributes:@{ NSForegroundColorAttributeName :[UIColor colorWithRed:63.0/255.0f green:67.0/255.0f blue:70.0/255.0f alpha:1.0],
                                                                                                                            }];
    
    [catName appendAttributedString:star];
    
     [_chooseBtn setAttributedTitle:catName forState:UIControlStateNormal];
    NSMutableAttributedString *dateTime = [[NSMutableAttributedString alloc] initWithString:@"Date/Time" attributes:@{ NSForegroundColorAttributeName :[UIColor colorWithRed:63.0/255.0f green:67.0/255.0f blue:70.0/255.0f alpha:1.0],
                                                                                                                            }];
    
    [dateTime appendAttributedString:star];
    
     [_dateTimeBtn setAttributedTitle:dateTime forState:UIControlStateNormal];
    
    
    if (IS_IPHONE_4_OR_LESS) {
        [self.btnPhotoLibrary.titleLabel setFont:[UIFont systemFontOfSize:11.0]];
        [self.btnTakeAPicture.titleLabel setFont:[UIFont systemFontOfSize:11.0]];
        _dateTimeBtn.imageEdgeInsets=UIEdgeInsetsMake(-2, 260, -5, -9);
        _creatBtnHeight.constant=45;
        _consCashBtnWidth.constant=140;
        _consWantCashBtnHeight.constant=35;
        _consWantCashBtnLeft.constant=20;
        _consHelpMeDecideRight.constant=248;
        _consHelpMeHeight.constant=15;
        [_wantCashBtn.titleLabel setFont:[UIFont systemFontOfSize:14.0]];
        [_creditFineBtn.titleLabel setFont:[UIFont systemFontOfSize:14.0]];
        _alertViewLeft.constant=35;
        _alertViewWidth.constant=250;
        _alertViewHeight.constant=120;

    }
    if (IS_IPHONE_5) {
        [self.btnPhotoLibrary.titleLabel setFont:[UIFont systemFontOfSize:13.0]];
        [self.btnTakeAPicture.titleLabel setFont:[UIFont systemFontOfSize:13.0]];
        
         _dateTimeBtn.imageEdgeInsets=UIEdgeInsetsMake(-2, 260, -5, -9);
        // _btnAddPaymentInfo.imageEdgeInsets=UIEdgeInsetsMake(<#CGFloat top#>, <#CGFloat left#>, <#CGFloat bottom#>, <#CGFloat right#>)
        
        _creatBtnHeight.constant=45;
        
        _consCashBtnWidth.constant=140;
        _consWantCashBtnHeight.constant=35;
        _consWantCashBtnLeft.constant=20;

      //  _consHelpMeBtnTop.constant=100;
        _consHelpMeDecideRight.constant=248;
        _consHelpMeHeight.constant=15;
        
        [_wantCashBtn.titleLabel setFont:[UIFont systemFontOfSize:14.0]];
        [_creditFineBtn.titleLabel setFont:[UIFont systemFontOfSize:14.0]];
        
        _alertViewLeft.constant=35;
        _alertViewWidth.constant=250;
        _alertViewHeight.constant=120;
        

        
    }
    if (IS_IPHONE_6) {
        [self.btnPhotoLibrary.titleLabel setFont:[UIFont systemFontOfSize:15.0]];
        [self.btnTakeAPicture.titleLabel setFont:[UIFont systemFontOfSize:15.0]];
     //   _dateTimeBtn.imageEdgeInsets=UIEdgeInsetsMake(-2, 260, -5, -9);
        _alertViewLeft.constant=68;
//        _alertViewLeft.constant=35;
//        _alertViewWidth.constant=300;
//        _alertViewHeight.constant=160;
        
    }
    if (IS_IPHONE_6P) {
        [self.btnPhotoLibrary.titleLabel setFont:[UIFont systemFontOfSize:17.0]];
        [self.btnTakeAPicture.titleLabel setFont:[UIFont systemFontOfSize:17.0]];
        //_creatBtnHeight.constant=50;

    }
    

 //   self.collectionViewTaskImages.backgroundColor=[UIColor redColor];

    paymentMethod = @"1";
    
    catArr = [DatabaseClass getDataFromCategoryData:[[NSString stringWithFormat:@"Select * from %@",tableCategory] UTF8String]];
    
    subCatArr = [DatabaseClass getDataFromSubCategoryData:[[NSString stringWithFormat:@"Select * from %@",tableSubCategory] UTF8String]];
    
    
    NSLog(@"authorization code is...%@",myAppDelegate.Authorize_String_BrainTree);
    
    self.braintreeClient = [[BTAPIClient alloc] initWithAuthorization:myAppDelegate.Authorize_String_BrainTree];
    
    
}

-(void)viewWillAppear:(BOOL)animated
{
    if (myAppDelegate.isFromHistory_ForEdit == YES)
    {
        [GlobalFunction addIndicatorView];
        
    }
}

-(void)viewDidAppear:(BOOL)animated{
    
    
   // myAppDelegate.vcObj.lblTitleText.text = @"B U Y  T I M E E";
//    
     NSLog(@"no content%@...",selectedBusinessObj.BusinessId);
    
//    if (selectedBusinessObj.BusinessId == nil)
//    {
//          selectedBusinessObj=[[BusinessInfo alloc]init];
//        selectedBusinessObj.BusinessId = @"00";
//    }
//    
    
       [self.view layoutIfNeeded];
    
    if (pics.count>0) {
        [self.collectionViewTaskImages layoutIfNeeded];
        [self.collectionViewTaskImages reloadData];
        [self.collectionViewTaskImages layoutIfNeeded];
        
        if(IS_IPHONE_6)
        {
            _viewMainHolderHeight.constant=530+_termsTxtFldHeight.constant+_descriptionTxtFldHeight.constant;
            _viewInsideHolderHeight.constant=530+_termsTxtFldHeight.constant+_descriptionTxtFldHeight.constant;
        }
       else if(IS_IPHONE_6P)
        {
    _viewMainHolderHeight.constant=580+_termsTxtFldHeight.constant+_descriptionTxtFldHeight.constant;
    _viewInsideHolderHeight.constant=580+_termsTxtFldHeight.constant+_descriptionTxtFldHeight.constant;
        }
       else if(IS_IPHONE_5)
       {
           _viewMainHolderHeight.constant=460+_termsTxtFldHeight.constant+_descriptionTxtFldHeight.constant;
           _viewInsideHolderHeight.constant=460+_termsTxtFldHeight.constant+_descriptionTxtFldHeight.constant;
       }
        else if (IS_IPHONE_4_OR_LESS)
        {
            _viewMainHolderHeight.constant=460+_termsTxtFldHeight.constant+_descriptionTxtFldHeight.constant;
            _viewInsideHolderHeight.constant=460+_termsTxtFldHeight.constant+_descriptionTxtFldHeight.constant;

        }
        
        
        
    }else{
        self.constraintCollectionViewHeight.constant = 0;
    
    }
    
    
    CGFloat constant = 0;
    self.tblHeight.constant = 0;
   
    if(IS_IPHONE_4_OR_LESS){
        
        _creatBtnHeight.constant=48;
        constant = 15;
        _collectionViewLeft.constant=12;
        _collectionViewWidth.constant=290;
        
        [self.saveAndPublishBtn.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:17]];
        
        [self.chooseBtn.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:15]];
        
        [self.dateTimeBtn.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:15]];
        [self.timeBtn.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:15]];
        
        
        [self.titleTextFld setFont:[UIFont fontWithName:@"Lato-Regular" size:15]];
        [self.txtFld1 setFont:[UIFont fontWithName:@"Lato-Regular" size:15]];
        [self.txtFld2 setFont:[UIFont fontWithName:@"Lato-Regular" size:15]];
        [self.txtFld4 setFont:[UIFont fontWithName:@"Lato-Regular" size:15]];
        [self.txtFld6 setFont:[UIFont fontWithName:@"Lato-Regular" size:15]];
        [self.quantityTxtFld setFont:[UIFont fontWithName:@"Lato-Regular" size:15]];
        [self.termsAndConditionTxtFld setFont:[UIFont fontWithName:@"Lato-Regular" size:15]];
        [self.discountTxtFld setFont:[UIFont fontWithName:@"Lato-Regular" size:15]];
        [self.premiumBtn.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:14]];
        [self.discountBtn.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:14]];
        [self.descriptionTextView setFont:[UIFont fontWithName:@"Lato-Regular" size:15]];
        [self.termsAndConditionTextView setFont:[UIFont fontWithName:@"Lato-Regular" size:15]];
        
        _saveAndPublishBtnBottom.constant=59;
        
        _chooseBtnTop.constant=17;
        _chooseBtnHeight.constant=18;
        _chooseBtnWidth.constant=300;
        
        [_chooseBtn setImageEdgeInsets:UIEdgeInsetsMake(0, 268, 0, 0)];
        [_timeBtn setImageEdgeInsets:UIEdgeInsetsMake(0,120, 0, 0)];
        
        
        if(isPremiumSelected==YES)
        {
            
            //  [_dateTimeBtn setTitle:@"Date/Time" forState:UIControlStateNormal];
            
            [_dateTimeBtn setImageEdgeInsets:UIEdgeInsetsMake(0,268, 0, 0)];
            _dateAndTimeBtnWidth.constant=300;
            _dateAndTimeLineWidth.constant=286;
            _dateAndTimeBtnTop.constant=10;
            
            
            
        }
        else
        {
            [_dateTimeBtn setImageEdgeInsets:UIEdgeInsetsMake(0,120, 0, 0)];
            
            _dateAndTimeBtnHeight.constant=21;
            _dateAndTimeBtnWidth.constant=135;
            _dateAndTimeBtnTop.constant=14;
            _dateAndTimeLeft.constant=21;
            
            
            _timeBtnWidth.constant=135;
            _timeBtnTop.constant=14;
            _timeBtnHeight.constant=21;
            _dateAndTimeLineWidth.constant=135;
            
            _timeLblWidth.constant=135;
            _timeLblLeft.constant=168;
            
        }
        
        
        _descriptionTxtFldLeft.constant=20;
        [self.descriptionTextView setTextContainerInset:UIEdgeInsetsMake(5,-2, 0,0)];
        
        
        
        _chooseBtnLeft.constant=22;
        
        
        _titleTxtFldHeight.constant=23;
        //_titleTxtFldWidth.constant=286;
        _titleTxtFldWidth.constant=262; // by vs
        _titleTxtFldTop.constant=14;
        _titleTxtFldLeft.constant=21;
        
        _locationTxtFldHeight.constant=21;
        _locationTxtFldWidth.constant=262;
        self.nsconstraintsLocationHelpTop.constant = 12;
        self.nsconstraintsLocationHelpLeading.constant = 3;
        //        _descriptionTxtFldHeight.constant=21;
        // _descriptionTxtFldWidth.constant=286;
        _descriptionTxtFldWidth.constant=262; // by vs
        _descriptionTxtFldTop.constant=14;
        // _descriptionTxtFldLeft.constant=21;
        self.nsconstraintsTitleHelpBtnTop.constant = 12;
        self.nsconstraintsDiscriptionHelpBtnTop.constant = 12;
        
        
        
        //        if(isImagePickFromAlbum==YES)
        //        {
        //            _descriptionTxtFldHeight.constant=newFrameDescriptionTxtView.size.height;
        //            _termsTxtFldHeight.constant=newFrameTermsAndConditionTxtView.size.height;
        //
        //        }
        //
        //        else
        //        {
        //            if(isDiscountSelected==YES)
        //            {
        //                _termsTxtFldHeight.constant=21;
        //
        //            }
        //            _descriptionTxtFldHeight.constant=21;
        //        }
        
        
        
        
        if([_descriptionTextView.text isEqual:@"Description"])
        {
            NSLog(@"no content");
            _descriptionTxtFldHeight.constant=21;
            
        }
        
        else
        {
            NSLog(@"content");
            _descriptionTxtFldHeight.constant=newFrameDescriptionTxtView.size.height;
            
        }
        
        if(isDiscountSelected==YES)
        {
            
            if([_termsAndConditionTextView.text isEqual:@"Terms and conditions"])
            {
                NSLog(@"no content");
                _termsTxtFldHeight.constant=21;
                
            }
            
            else
            {
                NSLog(@"content");
                _termsTxtFldHeight.constant=newFrameTermsAndConditionTxtView.size.height;
                
            }
        }
        
        
        
        
        
        
        _businessTxtFldHeight.constant=21;
        _businessTxtFldWidth.constant=286;
        _businessTxtFldTop.constant = 16;
        _businessTxtFldLeft.constant=21;
        
        _dateAndTimeBtnHeight.constant=21;
        //        _dateAndTimeBtnWidth.constant=260;
        _dateAndTimeLeft.constant=21;
        
        
        _locationTxtFldTop.constant=14;
        _locationLeft.constant=21;
        
        
        _priceTxtFldHeight.constant=22;
        _priceTxtFldTop.constant=14;
        _priceTxtFldLeft.constant=21;
        
        
        _catgryLineWidth.constant=286;
        _titleLineWidth.constant=286;
        _descriptionLineWidth.constant=286;
        _businessLineWidth.constant=286;
        //   _dateAndTimeLineWidth.constant=135;
        _locationLineWidth.constant=286;
        //    _priceLineWidth.constant=245;
        
        //         _dateAndTimeLineWidth.constant=286;
        
        
        _imgViewTop.constant=14;
        _imgViewWidth.constant=37;
        _imgViewHeight.constant=37;
        
        _creditFineBtnTop.constant=8;
        _creditIsFineHeight.constant=30;
        
        _consWantCashBtnHeight.constant=30;
        _consWantCashBtnTop.constant=8;
        _consWantCashBtnLeft.constant=15;
        _consCashBtnWidth.constant=147;
        
        _creditIsFineWidth.constant=140;
        
        _consHelpMeBtnTop.constant=21;
        _consHelpMeBtnLeft.constant=207;
        _consHelpMeWidth.constant=92;
        
        //        _premiumBtnTop.constant=8;
        _premiumBtnTop.constant=16;
        _premiumBtnLeft.constant=18;
        _premiumBntWidth.constant=147;
        
        //        _discountBtnTop.constant=8;
        _discountBtnTop.constant=16;
        _discountBtnWidth.constant=140;
        
        
        _tblViewWidth.constant=286;

        
    }
    
    
    else if (IS_IPHONE5){
        
        _creatBtnHeight.constant=39;
        _collectionViewLeft.constant=12;
        _collectionViewWidth.constant=290;
        
        
        constant = 12;
        [self.saveAndPublishBtn.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:17]];
        
        [self.chooseBtn.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:15]];
        
        [self.dateTimeBtn.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:15]];
        [self.timeBtn.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:15]];

        
        [self.titleTextFld setFont:[UIFont fontWithName:@"Lato-Regular" size:15]];
        [self.txtFld1 setFont:[UIFont fontWithName:@"Lato-Regular" size:15]];
        [self.txtFld2 setFont:[UIFont fontWithName:@"Lato-Regular" size:15]];
        [self.txtFld4 setFont:[UIFont fontWithName:@"Lato-Regular" size:15]];
        [self.txtFld6 setFont:[UIFont fontWithName:@"Lato-Regular" size:15]];
        [self.quantityTxtFld setFont:[UIFont fontWithName:@"Lato-Regular" size:15]];
        [self.termsAndConditionTxtFld setFont:[UIFont fontWithName:@"Lato-Regular" size:15]];
        [self.discountTxtFld setFont:[UIFont fontWithName:@"Lato-Regular" size:15]];
        [self.premiumBtn.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:14]];
        [self.discountBtn.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:14]];
        [self.descriptionTextView setFont:[UIFont fontWithName:@"Lato-Regular" size:15]];
        [self.termsAndConditionTextView setFont:[UIFont fontWithName:@"Lato-Regular" size:15]];

        _saveAndPublishBtnBottom.constant=59;

        _chooseBtnTop.constant=17;
        _chooseBtnHeight.constant=18;
        _chooseBtnWidth.constant=300;
        
        [_chooseBtn setImageEdgeInsets:UIEdgeInsetsMake(0, 268, 0, 0)];
        [_timeBtn setImageEdgeInsets:UIEdgeInsetsMake(0,120, 0, 0)];
        
        
        if(isPremiumSelected==YES)
        {
            
      //  [_dateTimeBtn setTitle:@"Date/Time" forState:UIControlStateNormal];

        [_dateTimeBtn setImageEdgeInsets:UIEdgeInsetsMake(0,268, 0, 0)];
        _dateAndTimeBtnWidth.constant=300;
        _dateAndTimeLineWidth.constant=286;
        _dateAndTimeBtnTop.constant=10;
        

            
        }
        else
        {
            [_dateTimeBtn setImageEdgeInsets:UIEdgeInsetsMake(0,120, 0, 0)];

            _dateAndTimeBtnHeight.constant=21;
            _dateAndTimeBtnWidth.constant=135;
            _dateAndTimeBtnTop.constant=14;
            _dateAndTimeLeft.constant=21;
            
            
            _timeBtnWidth.constant=135;
            _timeBtnTop.constant=14;
            _timeBtnHeight.constant=21;
            _dateAndTimeLineWidth.constant=135;
            
            _timeLblWidth.constant=135;
            _timeLblLeft.constant=168;
 
        }
    

        _descriptionTxtFldLeft.constant=20;
        [self.descriptionTextView setTextContainerInset:UIEdgeInsetsMake(5,-2, 0,0)];
        

        
        _chooseBtnLeft.constant=22;
        
        
        _titleTxtFldHeight.constant=23;
        //_titleTxtFldWidth.constant=286;
        _titleTxtFldWidth.constant=262; // by vs
        _titleTxtFldTop.constant=14;
        _titleTxtFldLeft.constant=21;
        
        _locationTxtFldHeight.constant=21;
        _locationTxtFldWidth.constant=262;
        self.nsconstraintsLocationHelpTop.constant = 12;
        self.nsconstraintsLocationHelpLeading.constant = 3;
//        _descriptionTxtFldHeight.constant=21;
       // _descriptionTxtFldWidth.constant=286;
        _descriptionTxtFldWidth.constant=262; // by vs
        _descriptionTxtFldTop.constant=14;
       // _descriptionTxtFldLeft.constant=21;
        self.nsconstraintsTitleHelpBtnTop.constant = 12;
        self.nsconstraintsDiscriptionHelpBtnTop.constant = 12;
    
        
        
//        if(isImagePickFromAlbum==YES)
//        {
//            _descriptionTxtFldHeight.constant=newFrameDescriptionTxtView.size.height;
//            _termsTxtFldHeight.constant=newFrameTermsAndConditionTxtView.size.height;
//            
//        }
//        
//        else
//        {
//            if(isDiscountSelected==YES)
//            {
//                _termsTxtFldHeight.constant=21;
//
//            }
//            _descriptionTxtFldHeight.constant=21;
//        }

        
        
        
        if([_descriptionTextView.text isEqual:@"Description"])
        {
            NSLog(@"no content");
            _descriptionTxtFldHeight.constant=21;

        }
        
        else
        {
            NSLog(@"content");
            _descriptionTxtFldHeight.constant=newFrameDescriptionTxtView.size.height;

        }
    
        if(isDiscountSelected==YES)
        {
        
        if([_termsAndConditionTextView.text isEqual:@"Terms and conditions"])
        {
            NSLog(@"no content");
            _termsTxtFldHeight.constant=21;
            
        }
        
        else
        {
            NSLog(@"content");
            _termsTxtFldHeight.constant=newFrameTermsAndConditionTxtView.size.height;
            
        }
        }
        
        
        
        
        
        
        _businessTxtFldHeight.constant=21;
        _businessTxtFldWidth.constant=286;
        _businessTxtFldTop.constant = 16;
        _businessTxtFldLeft.constant=21;
        
        _dateAndTimeBtnHeight.constant=21;
//        _dateAndTimeBtnWidth.constant=260;
        _dateAndTimeLeft.constant=21;

  
        _locationTxtFldTop.constant=14;
        _locationLeft.constant=21;

        
        _priceTxtFldHeight.constant=22;
        _priceTxtFldTop.constant=14;
        _priceTxtFldLeft.constant=21;
        
        
        _catgryLineWidth.constant=286;
        _titleLineWidth.constant=286;
        _descriptionLineWidth.constant=286;
        _businessLineWidth.constant=286;
    //   _dateAndTimeLineWidth.constant=135;
        _locationLineWidth.constant=286;
    //    _priceLineWidth.constant=245;
        
//         _dateAndTimeLineWidth.constant=286;

        
        _imgViewTop.constant=14;
        _imgViewWidth.constant=37;
        _imgViewHeight.constant=37;
        
        _creditFineBtnTop.constant=8;
        _creditIsFineHeight.constant=30;
        
        _consWantCashBtnHeight.constant=30;
        _consWantCashBtnTop.constant=8;
        _consWantCashBtnLeft.constant=15;
        _consCashBtnWidth.constant=147;
        
        _creditIsFineWidth.constant=140;
        
        _consHelpMeBtnTop.constant=21;
        _consHelpMeBtnLeft.constant=207;
        _consHelpMeWidth.constant=92;

//        _premiumBtnTop.constant=8;
        _premiumBtnTop.constant=16;
        _premiumBtnLeft.constant=18;
        _premiumBntWidth.constant=147;

//        _discountBtnTop.constant=8;
        _discountBtnTop.constant=16;
        _discountBtnWidth.constant=140;

        
        _tblViewWidth.constant=286;
        
    }
    
    
    
    else if(IS_IPHONE_6)
    {

        [self.saveAndPublishBtn.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:19]];
        _businessTxtFldTop.constant = 18;
        _saveAndPublishBtnBottom.constant=72;
        _creatBtnHeight.constant=45;
        _dateAndTimeBtnWidth.constant=330;
        _dateAndTimeLineWidth.constant=340;
        [_dateTimeBtn setImageEdgeInsets:UIEdgeInsetsMake(0,320, 0, 0)];
        constant = 9;
        
 
        if(isPremiumSelected==YES)
        {
           // [_dateTimeBtn setTitle:@"Date/Time" forState:UIControlStateNormal];
        }
        
        else
        {
            
            //[self discountBtnClicked:nil];
            _dateAndTimeBtnWidth.constant=150;
            _dateAndTimeLineWidth.constant=160;
            
            _timeBtnWidth.constant=150;
            _timeLblWidth.constant=160;
            _timeBtnLeft.constant=25;
            
            [_dateTimeBtn setImageEdgeInsets:UIEdgeInsetsMake(0,140, 0, 0)];
            [_timeBtn setImageEdgeInsets:UIEdgeInsetsMake(0,140, 0, 0)];
            
            _dateAndTimeBtnTop.constant=12;
            _timeBtnTop.constant=12;
            
            
            
            
            
        }
        
        
        _descriptionTxtFldLeft.constant=25;
        [self.descriptionTextView setTextContainerInset:UIEdgeInsetsMake(5,-2, 0,0)];
//        if(isImagePickFromAlbum==YES)
//        {
//            _descriptionTxtFldHeight.constant=newFrameDescriptionTxtView.size.height;
//            _termsTxtFldHeight.constant=newFrameTermsAndConditionTxtView.size.height;
//
//        }
//        
//        else
//        {
//            if(isDiscountSelected==YES)
//            {
//                _termsTxtFldHeight.constant=30;
//
//            }
//            _descriptionTxtFldHeight.constant=30;
//        }


        if([_descriptionTextView.text isEqual:@"Description"])
        {
            NSLog(@"no content");
            _descriptionTxtFldHeight.constant=30;
            
        }
        
        else
        {
            NSLog(@"content");
            _descriptionTxtFldHeight.constant=newFrameDescriptionTxtView.size.height;
            
        }
        
        
        if(isDiscountSelected==YES)
        {
        if([_termsAndConditionTextView.text isEqual:@"Terms and conditions"])
        {
            NSLog(@"no content");
            _termsTxtFldHeight.constant=30;
            
        }
        
        else
        {
            NSLog(@"content");
            _termsTxtFldHeight.constant=newFrameTermsAndConditionTxtView.size.height;
            
        }
        
        }

    }
    
    
    
    else if(IS_IPHONE_6P){
        [self.saveAndPublishBtn.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:21]];

        _creatBtnHeight.constant=50;
        _saveAndPublishBtnBottom.constant=75;
        
        
        constant = 6;
        
        [self.chooseBtn.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:19]];
        [self.dateTimeBtn.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:19]];
        [self.timeBtn.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:19]];
        
        [self.titleTextFld setFont:[UIFont fontWithName:@"Lato-Regular" size:19]];
        [self.txtFld1 setFont:[UIFont fontWithName:@"Lato-Regular" size:19]];
        [self.txtFld2 setFont:[UIFont fontWithName:@"Lato-Regular" size:19]];
        [self.txtFld4 setFont:[UIFont fontWithName:@"Lato-Regular" size:19]];
        [self.txtFld6 setFont:[UIFont fontWithName:@"Lato-Regular" size:19]];
        [self.termsAndConditionTextView setFont:[UIFont fontWithName:@"Lato-Regular" size:19]];
        [self.discountTxtFld setFont:[UIFont fontWithName:@"Lato-Regular" size:19]];
        [self.quantityTxtFld setFont:[UIFont fontWithName:@"Lato-Regular" size:19]];
        [self.descriptionTextView setFont:[UIFont fontWithName:@"Lato-Regular" size:19]];

 
        _descriptionTxtFldLeft.constant=22;
        [self.descriptionTextView setTextContainerInset:UIEdgeInsetsMake(5,-2, 0,0)];



        _chooseBtnTop.constant=18;
        _chooseBtnHeight.constant=32;
        _chooseBtnWidth.constant=365;
        _chooseBtnLeft.constant=30;
        
        [_chooseBtn setImageEdgeInsets:UIEdgeInsetsMake(0, 353, 0, 0)];
       // [_dateTimeBtn setImageEdgeInsets:UIEdgeInsetsMake(0, 150, 0, 0)];
       // [_timeBtn setImageEdgeInsets:UIEdgeInsetsMake(0, 150, 0, 0)];
        
        if(isPremiumSelected==YES)
            
        {

        [_dateTimeBtn setImageEdgeInsets:UIEdgeInsetsMake(0, 353, 0, 0)];
        _dateAndTimeBtnWidth.constant=365;
        _dateAndTimeLineWidth.constant=375;
            
//            _dateAndTimeBtnTop.constant=0;
            _locationTxtFldHeight.constant=32;
            _locationTxtFldWidth.constant=343;
            _locationTxtFldTop.constant=18;
            _locationLeft.constant=28;
            _locationLineWidth.constant=375;



        }
        else{
            

            [_dateTimeBtn setImageEdgeInsets:UIEdgeInsetsMake(0, 150, 0, 0)];
            [_timeBtn setImageEdgeInsets:UIEdgeInsetsMake(0, 150, 0, 0)];
            
            _dateAndTimeBtnHeight.constant=32;
            _dateAndTimeBtnWidth.constant=175;
            _dateAndTimeBtnTop.constant=15;
            _dateAndTimeLeft.constant=28;
            
            _timeBtnTop.constant=15;
            _timeBtnWidth.constant=175;
            _timeBtnHeight.constant=32;
            _timeLblLeft.constant=215;
            
            _dateAndTimeLineWidth.constant=180;
            _timeLblWidth.constant=180;
        
//            _quantityTxtFldWidth.constant=120;
//            _quantityLineLblWidth.constant=130;
//            _quantityTxtFldHeight.constant=32;
//            _quantityLineLblLeft.constant=215;
//            _quantityTxtFldLeft.constant=20;
//        
//            _locationTxtFldWidth.constant=175;
//            _locationLineWidth.constant=180;
            

        
        }
        _titleTxtFldHeight.constant=30;
        _titleTxtFldWidth.constant=342;
        _titleTxtFldTop.constant=17;
        
//        _titleTxtFldLeft.constant=30;
        
        _descriptionTxtFldWidth.constant=344;
        _descriptionTxtFldTop.constant=17;
        
        self.nsconstraintsDiscriptionHelpBtnTop.constant = 18;
        self.nsconstraintsTitleHelpBtnTop.constant = 16;
        self.nsconstraintsLocationHelpTop.constant = 18;
        self.nsconstraintsTitleHelpBtnLeading.constant = 7;
        self.nsconstraintsLocationHelpLeading.constant = 4;
        self.nsconstraintsDiscriptionHelpBtnLeading.constant = 9;
        
        
//        _descriptionLineLblTop.constant=10;

        
//        if(isImagePickFromAlbum==YES)
//        {
//            _descriptionTxtFldHeight.constant=newFrameDescriptionTxtView.size.height;
//            _termsTxtFldHeight.constant=newFrameTermsAndConditionTxtView.size.height;
//
//        }
//        
//        else
//        {
//            if(isDiscountSelected==YES)
//            {
//                _termsTxtFldHeight.constant=32;
//
//            }
//            _descriptionTxtFldHeight.constant=30;
//        }


        
        if([_descriptionTextView.text isEqual:@"Description"])
        {
            NSLog(@"no content");
            _descriptionTxtFldHeight.constant=32;
            
        }
        
        else
        {
            NSLog(@"content");
            _descriptionTxtFldHeight.constant=newFrameDescriptionTxtView.size.height;
            
        }
        
        if(isDiscountSelected==YES)
        {

        
        if([_termsAndConditionTextView.text isEqual:@"Terms and conditions"])
        {
            NSLog(@"no content");
            _termsTxtFldHeight.constant=32;
            
        }
        
        else
        {
            NSLog(@"content");
            _termsTxtFldHeight.constant=newFrameTermsAndConditionTxtView.size.height;
            
        }
        }

        
        
        _businessTxtFldHeight.constant=34;
        _businessTxtFldWidth.constant=365;
        _businessTxtFldTop.constant = 20;
        _businessTxtFldLeft.constant=28;

        _dateAndTimeBtnHeight.constant=32;
     //   _dateAndTimeBtnWidth.constant=365;
        _dateAndTimeLeft.constant=28;
        


        

        _priceTxtFldHeight.constant=32;
        _priceTxtFldTop.constant=18;
        _priceTxtFldLeft.constant=28;
        
        
        _quantityTxtFldTop.constant=18;
        
        _catgryLineWidth.constant=375;
        _titleLineWidth.constant=375;
        _descriptionLineWidth.constant=375;
        _businessLineWidth.constant=375;
//        _dateAndTimeLineWidth.constant=375;

        
        
        _tblViewWidth.constant=375;

        
        _imgViewTop.constant=20;
        _imgViewWidth.constant=48;
        _imgViewHeight.constant=48;
        
        _creditFineBtnTop.constant=12;
        _creditIsFineHeight.constant=40;
        
        _consWantCashBtnHeight.constant=40;
        _consWantCashBtnTop.constant=12;
        
        _consWantCashBtnLeft.constant=15;
        _consCashBtnWidth.constant=196;
        _creditIsFineWidth.constant=181;
        
        _consHelpMeBtnTop.constant=28;
        _consHelpMeBtnLeft.constant=268;
        _consHelpMeWidth.constant=120;
        
        _alertViewTop.constant=250;
        _alertViewLeft.constant=90;

        _termsTxtFldWidth.constant=365;
        _termsLblWidth.constant=375;

        
        _premiumBtnTop.constant = 20;
        _premiumBtnLeft.constant=19;
        _premiumBntWidth.constant=196;

        _discountBtnTop.constant=20;
        _discountBtnWidth.constant=181;

    }
    
    
    
    [self.subBtn setImageEdgeInsets:UIEdgeInsetsMake(0, self.subBtn.frame.size.width - 17, 0, 0)];
    
    [self.view layoutIfNeeded];
    
    CGFloat totalTextFieldHeight = 60*7;
    CGFloat refHeight = 650;
    CGFloat viewHeight = self.viewInsideHolder.frame.size.height;
    CGFloat ratioConstant = totalTextFieldHeight/refHeight;
    CGFloat rationHeightTotal = ratioConstant * viewHeight;
    CGFloat singleHeight = rationHeightTotal/7;
    
    singleHeight = singleHeight - constant;
    
   // self.constraintTextFieldHeight.constant = singleHeight;
    
    [self.view layoutIfNeeded];
    
    if (myAppDelegate.isPaymentFilled) {
        
    }else{
        
       // [[GlobalFunction shared] showAlertForMessage:@"You need to fill your credit card information first, before creating any task."];
        
       // [GlobalFunction removeIndicatorView];
        
    }
    
    
    // Work if coming from my activity.....
    
    if (myAppDelegate.isFromHistory_ForEdit == YES)
    {
        myAppDelegate.isFromHistory_ForEdit = NO;
        editStatus = @"1";
       NSString *title1 = myAppDelegate.workerTaskDetail.taskDetailObj.title;
        NSString *catId1 = myAppDelegate.workerTaskDetail.taskDetailObj.categoryId;
        NSString *description1 = myAppDelegate.workerTaskDetail.taskDetailObj.taskDescription;
        NSString *businessName1 = myAppDelegate.workerTaskDetail.name_Business;
        NSString *startDate1 = myAppDelegate.workerTaskDetail.taskDetailObj.startDate;
        NSString *startTime1 = myAppDelegate.workerTaskDetail.taskDetailObj.endDate;
        NSString *location1 = myAppDelegate.workerTaskDetail.taskDetailObj.locationName;
        float price1 = myAppDelegate.workerTaskDetail.taskDetailObj.price;
        NSString *discount1 = myAppDelegate.workerTaskDetail.taskDetailObj.discountValue;
        NSString *payMethod = myAppDelegate.workerTaskDetail.taskDetailObj.paymentMethod;
        NSString *discountDetail = myAppDelegate.workerTaskDetail.taskDetailObj.discountDescription;
        NSString *quentityOfTasks = myAppDelegate.workerTaskDetail.taskDetailObj.quantityOfDiscount;
        
        reservationId_ForEdit = myAppDelegate.workerTaskDetail.taskDetailObj.taskid;
        
      //   discountDetail = @"jhgd hgjh hh 76578435 ^#^^( hjgjfh ^&#^(*@&(^% ngjjhg lhgj lhgtl rhrht hlrjh jhrgtl lstg lstls jhgtlss lljglts ssgtls lslgt lsgtls lsgtslg ljsgl lsgll sgnl jljl l";
        
        
        int x = [discount1 intValue];
        
        if (x > 0 && price1 < 1)
        {
            reservationChangePossible = NO;
        }
        else if(price1 > 1 && x< 1)
        {
            reservationChangeDIscount = NO;
        }
        
        
        
        
//        if([payMethod isEqualToString:@"0"])
//        {
//            [self wantCashBtnClicked:_wantCashBtn];
//        }
//        else
//        {
//            [self creditFineBtnClciked:_creditFineBtn];
//        }
        
        // these upper four lines commented by VS after hide credit and cash btns 14 july 17.
        
        
      //  description1 = @"hjkghfdk bgkjhfdk kfgkjhfjdk jfg jfhg jd o khjkg hjg klsh skh shgfs hss skdhfk hsd hsdkhgsdgh shkghs sdkfh hjksdhjgfskdhs hshgksh khgs kshgsh i jsdh";
        
        
        NSString *PriceMiann = @"";
        
        if ([GlobalFunction checkIfContainFloat:price1]) {
            PriceMiann =  [NSString stringWithFormat:@"%0.2f",price1];
        }else{
            PriceMiann = [NSString stringWithFormat:@"%0.0f",price1];
        }

        
        self.titleTextFld.text = title1;
        self.txtFld2.text = businessName1;
       // self.descriptionTextView.text = description1;
        self.txtFld4.text = location1;
        self.txtFld6.text = PriceMiann;
        
        _txtViewActiveField = self.descriptionTextView ;
        
        if (description1.length <= 0)
        {
            
        }
        else
        {
            [self.descriptionTextView setText:description1];
            [self textViewDidChange:self.descriptionTextView];
            self.descriptionTextView.textColor=[UIColor colorWithRed:63.0/255.0f green:67.0/255.0f blue:70.0/255.0f alpha:1.0];


        }
        
        NSString  *selectedCategoryName11 = @"";
        
        selectedCategoryName11 = catId1;
        

        for (int i = 0; i<= catArr.count; i++)
        {
            // selectedBusinessObj = [[BusinessInfo alloc]init];
            category *businessObj11 = [catArr objectAtIndex:i];
            NSString *name1 = businessObj11.categoryId;
           // name1 = [name1 lowercaseString];
            // selectedBusinessName = [selectedBusinessName11 lowercaseString];
            NSLog(@"name1 is... %@",name1);
            NSLog(@"selectedBusinessName is... %@",selectedCategoryName11);
            if ([name1 isEqualToString:selectedCategoryName11])
            {
                selectedCatObj = nil;
                selectedCatObj = businessObj11;
                
                break;
            }
            
        }

        [_chooseBtn setTitle:selectedCatObj.name forState:UIControlStateNormal];
        
        
       // [self textViewShouldBeginEditing:self.descriptionTextView.text];
        
        
        
        // Handling for show design if same business exist...
        
        
        NSString  *selectedBusiness = @"";
        
        selectedBusiness = [businessName1 lowercaseString];
        
        for (int i = 0; i<= myAppDelegate.arrayBusinessDetail.count; i++)
        {
            // selectedBusinessObj = [[BusinessInfo alloc]init];
            BusinessInfo *businessObj11 = [myAppDelegate.arrayBusinessDetail objectAtIndex:i];
            NSString *name1 = [EncryptDecryptFile decrypt:businessObj11.BusinessName];
            name1 = [name1 lowercaseString];
            // selectedBusinessName = [selectedBusinessName11 lowercaseString];
            NSLog(@"name1 is... %@",name1);
            NSLog(@"selectedBusinessName is... %@",selectedBusiness);
            if ([name1 isEqualToString:selectedBusiness])
            {
                selectedBusinessObj = nil;
                selectedBusinessObj = businessObj11;
                
                break;
            }
            
        }

        NSLog(@"%@",[EncryptDecryptFile decrypt:selectedBusinessObj.BusinessName]);
        NSLog(@" business id ----%@",[EncryptDecryptFile decrypt:selectedBusinessObj.BusinessId]);
        NSLog(@"user id val --- %@", myAppDelegate.userData.BusinessUserId);
        
        if([myAppDelegate.userData.BusinessUserId isEqualToString:[EncryptDecryptFile decrypt:selectedBusinessObj.BusinessId]])
        {
            
            
            //            isDiscountSelected=YES;
            //            isPremiumSelected=NO;
            
            
            [_premiumBtn setTitle:@"PREMIUM" forState:UIControlStateNormal];
            [_discountBtn setTitle:@"DISCOUNT" forState:UIControlStateNormal];
            
            [self.premiumBtn setTitleColor:[UIColor colorWithRed:255.0/255.0f green:255.0/255.0f blue:255.0/255.0f alpha:1.0] forState:UIControlStateNormal];
            _premiumBtn.backgroundColor=[UIColor colorWithRed:63.0/255.0f green:67.0/255.0f blue:70.0/255.0f alpha:1.0];
            
            [self.discountBtn setTitleColor:[UIColor colorWithRed:63.0/255.0f green:67.0/255.0f blue:70.0/255.0f alpha:1.0] forState:UIControlStateNormal];
            _discountBtn.backgroundColor=[UIColor colorWithRed:244.0/255.0f green:244.0/255.0f blue:244.0/255.0f alpha:1.0];
            
            
            
            
            [UIView transitionWithView:_wantCashBtn
                              duration:0.4
                               options:UIViewAnimationOptionTransitionCrossDissolve
                            animations:^{
                                _wantCashBtn.hidden=YES;
                            }
                            completion:NULL];
            [UIView transitionWithView:_helpMeDecideBtn
                              duration:0.4
                               options:UIViewAnimationOptionTransitionCrossDissolve
                            animations:^{
                                _helpMeDecideBtn.hidden=YES;
                            }
                            completion:NULL];
            
            [UIView transitionWithView:_creditFineBtn
                              duration:0.4
                               options:UIViewAnimationOptionTransitionCrossDissolve
                            animations:^{
                                _creditFineBtn.hidden=YES;
                            }
                            completion:NULL];
            
            
            if(IS_IPHONE_6)
            {
                _dateAndTimeBtnTop.constant=12;
                _timeBtnTop.constant=12;
                _premiumBtnHeight.constant=36;
                _discountBtnHeight.constant=36;
                _businessTxtFldTop.constant = 18;
            }
            
            else if(IS_IPHONE_5 || IS_IPHONE_4_OR_LESS)
            {
                _dateAndTimeBtnTop.constant=14;
                _timeBtnTop.constant=14;
                
                _premiumBtnHeight.constant=30;
                _discountBtnHeight.constant=30;
                
                _premiumBtnTop.constant = 16;
                _discountBtnTop.constant = 16;
                
            }
            
            else if (IS_IPHONE_6P)
            {
                _premiumBtnHeight.constant=40;
                _discountBtnHeight.constant=40;
                
                _dateAndTimeBtnTop.constant=15;
                _timeBtnTop.constant=15;
                
                
                
            }
            //discount1 = @"200";
            int val = [discount1 intValue];
            if (val <= 0)
            {
                
            }
            else
            {
                
                
                
                if (discountDetail.length != 0)
                {
                     NSRange range = NSMakeRange(0,[discountDetail length]);
                    // self.termsAndConditionTextView.text = @"";
                    
                     _txtViewActiveField = self.termsAndConditionTextView ;
                    [self.termsAndConditionTextView setText:discountDetail];
                    [self textViewDidChange:self.termsAndConditionTextView];
                   // self.termsAndConditionTextView.backgroundColor = [UIColor redColor];
                   // [self textView:self.termsAndConditionTextView shouldChangeTextInRange:range replacementText:discountDetail];
                    
                    self.termsAndConditionTextView.textColor=[UIColor colorWithRed:63.0/255.0f green:67.0/255.0f blue:70.0/255.0f alpha:1.0];
                }
                self.txtFld6.text = @"";
                self.txtFld6.text = discount1;
                self.quantityTxtFld.text = quentityOfTasks;
                [self discountBtnClicked:nil];
                
                

               // [self doneTyping];
            }
        }
        
        else{
            [UIView transitionWithView:_wantCashBtn
                              duration:0.4
                               options:UIViewAnimationOptionTransitionCrossDissolve
                            animations:^{
                                _wantCashBtn.hidden=NO;
                            }
                            completion:NULL];
            [UIView transitionWithView:_creditFineBtn
                              duration:0.4
                               options:UIViewAnimationOptionTransitionCrossDissolve
                            animations:^{
                                _creditFineBtn.hidden=NO;
                            }
                            completion:NULL];
            [UIView transitionWithView:_helpMeDecideBtn
                              duration:0.4
                               options:UIViewAnimationOptionTransitionCrossDissolve
                            animations:^{
                                _helpMeDecideBtn.hidden=NO;
                            }
                            completion:NULL];
            
            
            _wantCashBtn.hidden=YES;
             _creditFineBtn.hidden=YES;
            _helpMeDecideBtn.hidden=YES;
            
            self.quantityTxtFld.text = @"";
            self.termsAndConditionTxtFld.text = @"";
            self.discountTxtFld.text = @"";
            
            picker.userInteractionEnabled=YES ;
            _scrollViewHolder.scrollEnabled=YES;
            
            
            //            isPremiumSelected=YES;
            //            isDiscountSelected=NO;
            
//            [_premiumBtn setTitle:@"" forState:UIControlStateNormal];
//            [_discountBtn setTitle:@"" forState:UIControlStateNormal];
            
            
           
             [self premiumBtnClicked:nil];
            
//            if(IS_IPHONE_6)
//            {
////                _premiumBtnHeight.constant=0;
////                _discountBtnHeight.constant=0;
//                _dateAndTimeBtnTop.constant=0;
//                _timeBtnTop.constant=0;
//            }
//
//
//            else if(IS_IPHONE_5)
//            {
////                _premiumBtnHeight.constant=0;
////                _discountBtnHeight.constant=0;
//                _dateAndTimeBtnTop.constant=0;
//                _timeBtnTop.constant=0;
//            }
//
//
//            else if(IS_IPHONE_6P)
//            {
////                _premiumBtnHeight.constant=0;
////                _discountBtnHeight.constant=0;
//
//                _dateAndTimeBtnTop.constant=0;
//                _timeBtnTop.constant=0;
//
//            }
            
            }
        
        businessNameForShare = _txtFld2.text;
      //  show=false;
        
      //  [self businessTxtFldClciked];
       // [txtActiveField resignFirstResponder];
        CGRect frame = self.viewInsideHolder.frame;
        frame.origin.y = 0;
        
        [UIView animateWithDuration:0.3 animations:^{
            self.viewInsideHolder.frame = frame;
        } completion:^(BOOL finished) {
            
        }];
        
        
        // add date and time on buttons
        
      //  discount1 = @"100";
        
        int disVal = [discount1 intValue];
        
        if (disVal <= 0)
        {
            
            NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
            [dateFormat setDateFormat:dateFormatFull];
            
            NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
            [outputFormatter setDateFormat:dateFormatOnlyHour];
            
            NSString *timetofill = startTime1;
            
            // dateStart = picker.date;
            NSString *dateString = startDate1;
            NSString *dateVal=dateString;
            
            NSDateFormatter *dateFormat00 = [[NSDateFormatter alloc] init];
            [dateFormat00 setDateFormat:dateFormatFull];
            NSDate *dateMain = [dateFormat00 dateFromString:startDate1];
            [dateFormat00 setDateFormat:dateFormatMDY];
            dateString = [dateFormat00 stringFromDate:dateMain]; //    [dateFormat setDateFormat:dateFormatFull];
            
            
            
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            dateFormatter.dateFormat = @"HH:mm:ss";
            NSDate *date1 = [dateFormatter dateFromString:timetofill];
            
            NSDateFormatter *formatter00 = [[NSDateFormatter alloc] init];
            NSLocale *locale = [[NSLocale alloc]initWithLocaleIdentifier:@"en_US"];
            [formatter00 setLocale:locale];
            [formatter00 setDateFormat:@"hh:mm a"];
            NSString *timetoShow = [formatter00 stringFromDate:date1];
            NSLog(@"Current Date: %@", [formatter00 stringFromDate:date1]);
            
            dateString = [[dateString stringByAppendingString:@"  "] stringByAppendingString:timetoShow];
            NSMutableAttributedString *dateString1 = [[NSMutableAttributedString alloc] initWithString:dateString];

           // [_dateTimeBtn setTitle: dateString1 forState: UIControlStateNormal];
            [_dateTimeBtn setAttributedTitle: dateString1 forState: UIControlStateNormal];

            
            startDate = @"";
            startTime = @"";
            startDate=dateVal;
            startTime=timetofill;
            
        }
        else
        {
            
            if([startDate1 isEqualToString:@"AnyDate"])
            {
                
                [self butImgPressed:nil];
                
            }
            else
            {
                NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
                [dateFormat setDateFormat:dateFormatFull];
                //
                //    NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
                //    [outputFormatter setDateFormat:dateFormatOnlyHour];
                // dateStart = picker.date;
                
                NSDateFormatter *dateFormat00 = [[NSDateFormatter alloc] init];
                [dateFormat00 setDateFormat:dateFormatFull];
                NSDate *dateMain = [dateFormat00 dateFromString:startDate1];
                [dateFormat00 setDateFormat:dateFormatMDY];
                NSString* dateString = [dateFormat00 stringFromDate:dateMain];
                
                
                NSString *datetofill = startDate1;
                startDate = @"";
                startDate = datetofill;
                NSMutableAttributedString *dateString1 = [[NSMutableAttributedString alloc] initWithString:dateString];

               // [_dateTimeBtn setTitle: dateString forState: UIControlStateNormal];
                [_dateTimeBtn setAttributedTitle:dateString1 forState: UIControlStateNormal];

            }
            
            if ([startTime1 isEqualToString:@"AnyTime"])
            {
                [self butAnyTimePressed:nil];
            }
            else
            {
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            dateFormatter.dateFormat = @"HH:mm:ss";
            NSDate *date1 = [dateFormatter dateFromString:startTime1];
            
            NSDateFormatter *formatter00 = [[NSDateFormatter alloc] init];
            NSLocale *locale = [[NSLocale alloc]initWithLocaleIdentifier:@"en_US"];
            [formatter00 setLocale:locale];
            [formatter00 setDateFormat:@"hh:mm a"];
            NSString *timetoShow = [formatter00 stringFromDate:date1];
            NSLog(@"Current Date: %@", [formatter00 stringFromDate:date1]);
            
            startTime = @"";
            startTime = startTime1;
                NSMutableAttributedString *dateString1 = [[NSMutableAttributedString alloc] initWithString:timetoShow];
            //[_timeBtn setTitle:timetoShow forState: UIControlStateNormal];
                [_timeBtn setAttributedTitle:dateString1 forState: UIControlStateNormal];
            }
        }
        

      //  [self downloadPicsForCollectionView];
        
         [self performSelector:@selector(downloadPicsForCollectionView) withObject:nil afterDelay:0.1];
        
        
        
    }
    
    
    
    
    
}

-(void)downloadPicsForCollectionView
{
    
    
    
    
    
    if (myAppDelegate.workerTaskDetail.taskDetailObj.taskImagesArray.count>0)
        
    {
        
        NSLog(@"%lu",(unsigned long)myAppDelegate.workerTaskDetail.taskDetailObj.taskImagesArray.count);
        
        
        
        for(int i=0; i<myAppDelegate.workerTaskDetail.taskDetailObj.taskImagesArray.count;i++)
        {
            
                NSString *UrlStr =[myAppDelegate.workerTaskDetail.taskDetailObj.taskImagesArray objectAtIndex:i];;
                
                NSString* webStringURL = [UrlStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                
            
                    //This is what you will load lazily
                    NSURL *imageURL=[NSURL URLWithString:webStringURL];
                    
                    NSData *images=[NSData dataWithContentsOfURL:imageURL];
                    
                    UIImage *img = [UIImage imageWithData:images];
                    

                 [pics addObject:img];
            
            
           
            
            
    }
        
        
     NSInteger count = pics.count;
        
        if (count == 1) {
            NSString *imageName=[NSString stringWithFormat:@"taskImg%f",[[NSDate date] timeIntervalSince1970]];
            imageName=[imageName stringByReplacingOccurrencesOfString:@"." withString:@""];
            [picsNameArray addObject:imageName];
            //        [picsNameArray addObject:[NSString stringWithFormat:@"taskImg"]];
        }else{
            
            
            for (int i = 1; i<=pics.count; i++)
            {
                
                NSString *imageName=[NSString stringWithFormat:@"taskImg%f",[[NSDate date] timeIntervalSince1970]];
                imageName=[imageName stringByReplacingOccurrencesOfString:@"." withString:@""];
                [picsNameArray addObject:imageName];
                //[picsNameArray addObject:[NSString stringWithFormat:@"taskImg%ld",(long)(count-1)]];
                
            }
            
           
        }
        

     
        
        
        if (self.constraintCollectionViewHeight.constant == 0) {
            
            [self.view layoutIfNeeded];
            self.collectionViewTaskImages.hidden = NO;
            self.constraintCollectionViewHeight.constant = 60;
            [UIView animateWithDuration:0.5 animations:^{
                [self.view layoutIfNeeded];
            } completion:^(BOOL finished) {
                
            }];
            
        }

        [self.collectionViewTaskImages layoutIfNeeded];
        [self.collectionViewTaskImages reloadData];
        [self.collectionViewTaskImages layoutIfNeeded];
        
         [GlobalFunction removeIndicatorView];
        
        
        [self.descriptionTextView becomeFirstResponder];
         [self doneTyping];
}
else
{
    [GlobalFunction removeIndicatorView];
}
}

- (void)textViewDidChange:(UITextView *)textView
{
    
    
    if(_txtViewActiveField==_termsAndConditionTextView)
    {
        if(_termsTxtFldHeight.constant<=150)
        {
            CGFloat fixedWidth = textView.frame.size.width;
            CGSize newSize = [textView sizeThatFits:CGSizeMake(fixedWidth, MAXFLOAT)];
            newFrameTermsAndConditionTxtView = textView.frame;
            newFrameTermsAndConditionTxtView.size = CGSizeMake(fmaxf(newSize.width, fixedWidth), newSize.height);
            _termsTxtFldHeight.constant=newFrameTermsAndConditionTxtView.size.height;
            
            heightOfTermsTxtView=0;
            heightOfTermsTxtView=_termsTxtFldHeight.constant;
            _termsAndConditionTextView.scrollEnabled=NO;
            
            NSLog(@"-------- terms text field height ---------- %f",_termsTxtFldHeight.constant);
            
            
            [_termsAndConditionTextView sizeToFit];
            CGPoint bottomOffset = CGPointMake(0, self.termsAndConditionTextView.contentSize.height - self.termsAndConditionTextView.bounds.size.height);
            [self.termsAndConditionTextView setContentOffset:bottomOffset animated:YES];
        }
        
        
        else
        {
            _termsAndConditionTextView.scrollEnabled=YES;
            
        }

//        CGRect newFrame = self.viewInsideHolder.frame;
//        newFrame.size.height = 650+_termsTxtFldHeight.constant;
//        [self.viewInsideHolder setFrame:newFrame];


        
        
//        if(_termsTxtFldHeight.constant>66)
//        {
//            CGRect firstFrame = self.viewInsideHolder.frame;
//            
//            firstFrame.origin.y = -350+self.scrollViewHolder.contentOffset.y;
//            
//            [UIView animateWithDuration:0.3 animations:^{
//                self.viewInsideHolder.frame = firstFrame;
//            } completion:^(BOOL finished) {
//                
//            }];
//
//        }
//        
//        if(_termsTxtFldHeight.constant>125)
//        {
//            CGRect firstFrame = self.viewInsideHolder.frame;
//            
//            firstFrame.origin.y = -400+self.scrollViewHolder.contentOffset.y;
//            
//            [UIView animateWithDuration:0.3 animations:^{
//                self.viewInsideHolder.frame = firstFrame;
//            } completion:^(BOOL finished) {
//                
//            }];
//            
//        }
//        if(_termsTxtFldHeight.constant>185)
//        {
//            CGRect firstFrame = self.viewInsideHolder.frame;
//            
//            firstFrame.origin.y = -450+self.scrollViewHolder.contentOffset.y;
//            
//            [UIView animateWithDuration:0.3 animations:^{
//                self.viewInsideHolder.frame = firstFrame;
//            } completion:^(BOOL finished) {
//                
//            }];
//            
//        }
        
        

    }
    
    else
    {
        if(_descriptionTxtFldHeight.constant<=150)
        {
            
            CGFloat fixedWidth = textView.frame.size.width;
            CGSize newSize = [textView sizeThatFits:CGSizeMake(fixedWidth, MAXFLOAT)];
            newFrameDescriptionTxtView = textView.frame;
            newFrameDescriptionTxtView.size = CGSizeMake(fmaxf(newSize.width, fixedWidth), newSize.height);
            _descriptionTxtFldHeight.constant=newFrameDescriptionTxtView.size.height;
            _descriptionTextView.scrollEnabled=NO;
            
            NSLog(@"-------- description text view height ---------- %f",_descriptionTxtFldHeight.constant);
            
            
            
            [_descriptionTextView sizeToFit];
            CGPoint bottomOffset = CGPointMake(0, self.descriptionTextView.contentSize.height - self.descriptionTextView.bounds.size.height);
            [self.descriptionTextView setContentOffset:bottomOffset animated:YES];
        }
        
        else
        {
            _descriptionTextView.scrollEnabled=YES;
            
        }
        


    }
    
    
    
    
    
    

}

-(void)textViewDidEndEditing:(UITextView *)textView
{
    
    NSLog(@"did end editing....");
    [self.view layoutIfNeeded];
    
    NSLog(@" view inside holder height-------%f",_viewInsideHolderHeight.constant);

    
    if(_txtViewActiveField==_descriptionTextView)
    {
    
    
    if([_descriptionTextView.text isEqual:@"Description"])
    {
        if(IS_IPHONE5 || IS_IPHONE_4_OR_LESS)
        {
            _viewMainHolderHeight.constant=460;
        }
        else if (IS_IPHONE_6)
        {
            _viewMainHolderHeight.constant=530;
        }
        else if (IS_IPHONE_6P)
        {
            _viewMainHolderHeight.constant=580;
        }
 
    }

    else
    {
    if(IS_IPHONE_6)
    {
        _viewMainHolderHeight.constant=530+_termsTxtFldHeight.constant+_descriptionTxtFldHeight.constant;
        _viewInsideHolderHeight.constant=530+_termsTxtFldHeight.constant+_descriptionTxtFldHeight.constant;
    }
    
    if(IS_IPHONE_5)
    {
        _viewMainHolderHeight.constant=460+_termsTxtFldHeight.constant+_descriptionTxtFldHeight.constant;
        _viewInsideHolderHeight.constant=460+_termsTxtFldHeight.constant+_descriptionTxtFldHeight.constant;
    }
    
    if(IS_IPHONE_6P)
    {
        _viewMainHolderHeight.constant=580+_termsTxtFldHeight.constant+_descriptionTxtFldHeight.constant;
        _viewInsideHolderHeight.constant=580+_termsTxtFldHeight.constant+_descriptionTxtFldHeight.constant;
    }
    
     if(IS_IPHONE_4_OR_LESS)
     {
         _viewMainHolderHeight.constant=470+_termsTxtFldHeight.constant+_descriptionTxtFldHeight.constant;
         _viewInsideHolderHeight.constant=470+_termsTxtFldHeight.constant+_descriptionTxtFldHeight.constant;

     }
        
    }
    }
    
    else
    {
    
    if([_termsAndConditionTextView.text isEqual:@"Terms and conditions"])
    {
        if(IS_IPHONE5 || IS_IPHONE_4_OR_LESS)
        {
            _viewMainHolderHeight.constant=460;
        }
        else if (IS_IPHONE_6)
        {
            _viewMainHolderHeight.constant=530;
        }
        else if (IS_IPHONE_6P)
        {
            _viewMainHolderHeight.constant=630;
        }
        
    }
    
    else
    {
        if(IS_IPHONE_6)
        {
            _viewMainHolderHeight.constant=530+_termsTxtFldHeight.constant+_descriptionTxtFldHeight.constant;
            _viewInsideHolderHeight.constant=530+_termsTxtFldHeight.constant+_descriptionTxtFldHeight.constant;
        }
        
        if(IS_IPHONE_5)
        {
            _viewMainHolderHeight.constant=460+_termsTxtFldHeight.constant+_descriptionTxtFldHeight.constant;
            _viewInsideHolderHeight.constant=460+_termsTxtFldHeight.constant+_descriptionTxtFldHeight.constant;
        }
        
        if(IS_IPHONE_6P)
        {
            _viewMainHolderHeight.constant=580+_termsTxtFldHeight.constant+_descriptionTxtFldHeight.constant;
            _viewInsideHolderHeight.constant=580+_termsTxtFldHeight.constant+_descriptionTxtFldHeight.constant;
        }
        
        if(IS_IPHONE_4_OR_LESS)
        {
            _viewMainHolderHeight.constant=470+_termsTxtFldHeight.constant+_descriptionTxtFldHeight.constant;
            _viewInsideHolderHeight.constant=470+_termsTxtFldHeight.constant+_descriptionTxtFldHeight.constant;
   
        }
    }
    }
}

-(void)textViewDidBeginEditing:(UITextView *)textView
{
    
    dateBtnClicked = false;
    timeBtnClicked = false;
    
    _txtViewActiveField = textView;
    [self createInputAccessoryView];
    
      txtActiveField=nil;
    
    [textView setInputAccessoryView:inputAccView];
    
    
    if(_txtViewActiveField==_termsAndConditionTextView)
    {
    
    
    if (_txtViewActiveField == self.termsAndConditionTextView) {
        [picker removeFromSuperview];
        [self closeTable];
        
    }

        
        
        if([_termsAndConditionTextView.text isEqual:@"Terms and conditions"])
        {
            
            if(IS_IPHONE_5 || IS_IPHONE_4_OR_LESS)
            {
                CGRect firstFrame = self.viewInsideHolder.frame;
                
                firstFrame.origin.y = -_quantityTxtFld.frame.origin.y+self.scrollViewHolder.contentOffset.y;
                
                [UIView animateWithDuration:0.3 animations:^{
                    self.viewInsideHolder.frame = firstFrame;
                } completion:^(BOOL finished) {
                    
                }];
                

            }
            
            else
            {
            
            CGRect firstFrame = self.viewInsideHolder.frame;
            
            firstFrame.origin.y = -_txtFld6.frame.origin.y+self.scrollViewHolder.contentOffset.y;
            
            [UIView animateWithDuration:0.3 animations:^{
                self.viewInsideHolder.frame = firstFrame;
            } completion:^(BOOL finished) {
                
            }];
            }

        }
        
        
        else{
            
            CGRect firstFrame = self.viewInsideHolder.frame;
            
            firstFrame.origin.y = -_termsAndConditionTextView.frame.origin.y+self.scrollViewHolder.contentOffset.y+60;
            
            [UIView animateWithDuration:0.3 animations:^{
                self.viewInsideHolder.frame = firstFrame;
            } completion:^(BOOL finished) {
                
            }];

            
        }
        
        
        
        
    }
    
    
    else
        
    {
        
        CGPoint cursorPosition = [_descriptionTextView caretRectForPosition:_descriptionTextView.selectedTextRange.start].origin;
        
        NSLog(@"%f",cursorPosition.y);
        NSLog(@"%f",cursorPosition.x);

        
        
        
        CGRect firstFrame = self.viewInsideHolder.frame;
//        firstFrame.origin.y = -_descriptionTextView.frame.origin.y+self.scrollViewHolder.contentOffset.y;
        
//        firstFrame.origin.y = -cursorPosition.y + self.scrollViewHolder.contentOffset.y;
        firstFrame.origin.y = -_titleTextFld.frame.origin.y + self.scrollViewHolder.contentOffset.y;

        [UIView animateWithDuration:0.3 animations:^{
            self.viewInsideHolder.frame = firstFrame;
        } completion:^(BOOL finished)
         {
            
        }];

   }

    
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    
    
    NSLog(@"range change");
    NSString *value = [textView.text stringByReplacingCharactersInRange:range withString:text];
    

    if([value isEqual:@"Description"])
    {
        if (range.length == 0) {
            if ([text isEqualToString:@"\n"])
            {
                textView.text = [NSString stringWithFormat:@"%@\n",textView.text];
                
                [self textViewDidChange:textView];
                
                return NO;
            }
        }
    }
        
        
    else{
        
        
        if(_txtViewActiveField==_termsAndConditionTextView)
        {
            if ([_termsAndConditionTextView.text isEqualToString:@"Terms and conditions"]) {
                _termsAndConditionTextView.text = @"";
                _termsAndConditionTextView.textColor=[UIColor colorWithRed:63.0/255.0f green:67.0/255.0f blue:70.0/255.0f alpha:1.0];
                
            }
            else if (value.length==0)
            {
                _termsAndConditionTextView.text = @"Terms and conditions";
                _termsAndConditionTextView.textColor=[UIColor colorWithRed:143.0/255.0f green:143.0/255.0f blue:143.0/255.0f alpha:1.0];
                [self doneTyping];
                
                
                CGRect sixthFrame = self.viewInsideHolder.frame;
                sixthFrame.origin.y = 0+self.scrollViewHolder.contentOffset.y;
                [UIView animateWithDuration:0.3 animations:^{
                    self.viewInsideHolder.frame = sixthFrame;
                } completion:^(BOOL finished) {
                    
                }];
                
                NSLog(@"value length is equal to zero");
                
                if(IS_IPHONE_6P)
                {
                    _termsTxtFldHeight.constant=30;    //for 6Plus
                }
                else if (IS_IPHONE_6)
                {
                    _termsTxtFldHeight.constant=30;    //for 6
                    
                }
                else if(IS_IPHONE_5 || IS_IPHONE_4_OR_LESS)
                {
                    _termsTxtFldHeight.constant=21;    //for 5
                    
                }
                if(IS_IPHONE5)
                {
                    _viewMainHolderHeight.constant=460;
                }
                else if (IS_IPHONE_6)
                {
                    _viewMainHolderHeight.constant=530;
                }
                else if (IS_IPHONE_6P)
                {
                    _viewMainHolderHeight.constant=580;
                }
                else if (IS_IPHONE_4_OR_LESS)
                {
                    _viewMainHolderHeight.constant=460;

                }

                
                
                return NO;
            }
            else if (value.length>0)
            {
                _termsAndConditionTextView.textColor=[UIColor colorWithRed:63.0/255.0f green:67.0/255.0f blue:70.0/255.0f alpha:1.0];
                
            }

        }
        
        
        else{
        
        if ([_descriptionTextView.text isEqualToString:@"Description"]) {
            _descriptionTextView.text = @"";
            _descriptionTextView.textColor=[UIColor colorWithRed:63.0/255.0f green:67.0/255.0f blue:70.0/255.0f alpha:1.0];
            
            
            NSLog(@"first if");
            
        }
        else if (value.length==0)
        {
            _descriptionTextView.text = @"Description";
            _descriptionTextView.textColor=[UIColor colorWithRed:143.0/255.0f green:143.0/255.0f blue:143.0/255.0f alpha:1.0];
            
            [_descriptionTextView resignFirstResponder];
            CGRect sixthFrame = self.viewInsideHolder.frame;
            sixthFrame.origin.y = 0+self.scrollViewHolder.contentOffset.y;
            [UIView animateWithDuration:0.3 animations:^{
                self.viewInsideHolder.frame = sixthFrame;
            } completion:^(BOOL finished) {
                
            }];

            NSLog(@"value length is equal to zero");

            if(IS_IPHONE_6P)
            {
            _descriptionTxtFldHeight.constant=30;    //for 6Plus
            }
            else if (IS_IPHONE_6)
            {
                _descriptionTxtFldHeight.constant=30;    //for 6

            }
            else if(IS_IPHONE_5 || IS_IPHONE_4_OR_LESS)
            {
                _descriptionTxtFldHeight.constant=21;    //for 5

            }
            if(IS_IPHONE5)
            {
                _viewMainHolderHeight.constant=460;
            }
            else if (IS_IPHONE_6)
            {
                _viewMainHolderHeight.constant=530;
            }
            else if (IS_IPHONE_6P)
            {
                _viewMainHolderHeight.constant=580;
            }
            else if (IS_IPHONE_4_OR_LESS)
            {
                _viewMainHolderHeight.constant=460;

            }

            
            return NO;
        }
        else if (value.length>0)
        {
            _descriptionTextView.textColor=[UIColor colorWithRed:63.0/255.0f green:67.0/255.0f blue:70.0/255.0f alpha:1.0];
            NSLog(@"value length is greater then zero");
            

        }
            
        }

    }
    
    
//    if([text isEqualToString:@"\n"]) {
//        [textView resignFirstResponder];
//        return NO;
//    }
    NSLog(@"return yes");

    return YES;
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)addInputViewToTextField:(UITextField *)textField{
    
//    [textField setInputView:datePicker];
   // [textField setInputView:timePicker];

    
    [self createInputAccessoryView];
    
    [textField setInputAccessoryView:inputAccView];
    
}



- (void)addInputViewToTextView:(UITextView *)textView{
    
    //    [textField setInputView:datePicker];
    // [textField setInputView:timePicker];
    
    
    [self createInputAccessoryView];
    
    [textView setInputAccessoryView:inputAccView];
    
}


-(void)updateTextField:(id)sender{
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:dateFormatDate];
    
//    if (txtActiveField == self.txtFld4) {
//        
//        dateStart = datePicker.date;
//        NSString *dateString = [dateFormat stringFromDate:dateStart];
//        self.txtFld4.text = dateString;
//        
//    }else if (txtActiveField == self.txtFld5){
//        
//        dateEnd = datePicker.date;
//        NSString *dateString = [dateFormat stringFromDate:dateEnd];
//        self.txtFld5.text = dateString;
//        
//    }
    
    
    
    
//            dateStart = datePicker.date;
//            NSString *dateString = [dateFormat stringFromDate:dateStart];
//            _dateTimeBtn.titleLabel.text = dateString;
//
    
    
    
    
    
    
}

#pragma mark - create accessory view
-(void)createInputAccessoryView{
    // Create the view that will play the part of the input accessory view.
    // Note that the frame width (third value in the CGRectMake method)
    // should change accordingly in landscape orientation. But we don’t care
    // about that now.
    inputAccView = [[UIView alloc] initWithFrame:CGRectMake(10.0, 0.0, self.view.frame.size.width, 40.0)];
    // Set the view’s background color. We’ ll set it here to gray. Use any color you want.
    [inputAccView setBackgroundColor:[UIColor lightGrayColor]];
    // We can play a little with transparency as well using the Alpha property. Normally
    // you can leave it unchanged.
    [inputAccView setAlpha: 0.8];
    // If you want you may set or change more properties (ex. Font, background image,e.t.c.).
    // For now, what we’ ve already done is just enough.
    // Let’s create our buttons now. First the previous button.
    btnPrev = [UIButton buttonWithType: UIButtonTypeCustom];
    // Set the button’ s frame. We will set their widths to 80px and height to 40px.
    [btnPrev setFrame: CGRectMake(0.0, 0.0, 80.0, 40.0)];
    // Title.
    [btnPrev setTitle: @"Previous" forState: UIControlStateNormal];
    // Background color.
    [btnPrev setBackgroundColor: [UIColor clearColor]];
    // You can set more properties if you need to.
    // With the following command we’ ll make the button to react in finger tapping. Note that the
    // gotoPrevTextfield method that is referred to the @selector is not yet created. We’ ll create it
    // (as well as the methods for the rest of our buttons) later.
    [btnPrev addTarget: self action: @selector(gotoPrevTextfield) forControlEvents: UIControlEventTouchUpInside];
    // Do the same for the two buttons left.
    btnNext = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnNext setFrame:CGRectMake(85.0f, 0.0f, 80.0f, 40.0f)];
    [btnNext setTitle:@"Next" forState:UIControlStateNormal];
    [btnNext setBackgroundColor:[UIColor clearColor]];
    [btnNext addTarget:self action:@selector(gotoNextTextfield) forControlEvents:UIControlEventTouchUpInside];
    btnDone = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnDone setFrame:CGRectMake(self.view.frame.size.width - 80.0f, 0.0f, 80.0f, 40.0f)];
    [btnDone setTitle:@"Done" forState:UIControlStateNormal];
    [btnDone setBackgroundColor:[UIColor clearColor]];
    [btnDone setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [btnDone addTarget:self action:@selector(doneTyping) forControlEvents:UIControlEventTouchUpInside];
    // Now that our buttons are ready we just have to add them to our view.
    [inputAccView addSubview:btnPrev];
    [inputAccView addSubview:btnNext];
    [inputAccView addSubview:btnDone];
}

-(void)gotoPrevTextfield{
    // If the active textfield is the first one, can't go to any previous
    // field so just return.
    
    [picker removeFromSuperview];
    
    if (_txtViewActiveField == self.termsAndConditionTextView) {
        [self.termsAndConditionTextView resignFirstResponder];
        [_quantityTxtFld becomeFirstResponder];
        return;
    }
    
    if (txtActiveField == _quantityTxtFld) {
        [_quantityTxtFld resignFirstResponder];
        [ self.txtFld6 becomeFirstResponder];
        return;
    }
  
//    if (txtActiveField == _discountTxtFld) {
//        [_discountTxtFld resignFirstResponder];
//        [_txtFld4 becomeFirstResponder];
//        return;
//    }

    if (txtActiveField == _txtFld4) {
        
        CGRect secondFrame = self.viewInsideHolder.frame;
        secondFrame.origin.y = 0;
        [UIView animateWithDuration:0.3 animations:^{
            self.viewInsideHolder.frame = secondFrame;
            self.scrollViewHolder.contentOffset = CGPointMake(0, 0);
        } completion:^(BOOL finished) {
            
        }];
        
        [_txtFld4 resignFirstResponder];
        [_txtFld2 becomeFirstResponder];
        return;
    }
    if (txtActiveField == _titleTextFld) {
        [_titleTextFld resignFirstResponder];
        [_txtFld4 becomeFirstResponder];
        return;
    }

    
    if (txtActiveField == self.txtFld2) {
        
        CGRect fourthFrame = self.viewInsideHolder.frame;
        fourthFrame.origin.y = 0+self.scrollViewHolder.contentOffset.y;
        [UIView animateWithDuration:0.3 animations:^{
            self.viewInsideHolder.frame = fourthFrame;
        } completion:^(BOOL finished) {
            
        }];

        [self.txtFld2 resignFirstResponder];
        [self closeTable];
//        [self.descriptionTextView becomeFirstResponder];
        return;
        
    }
    if(_txtViewActiveField == _descriptionTextView)
    {
        [self.descriptionTextView resignFirstResponder];
        [self closeTable];
        [self.titleTextFld becomeFirstResponder];
        
        CGRect secondFrame = self.viewInsideHolder.frame;
        secondFrame.origin.y = 0;
        [UIView animateWithDuration:0.3 animations:^{
            self.viewInsideHolder.frame = secondFrame;
            self.scrollViewHolder.contentOffset = CGPointMake(0, 0);
        } completion:^(BOOL finished) {
            
        }];
        
    }


    if(isDiscountSelected==YES)
    {
        if (txtActiveField == self.txtFld6)
        {
            [self.txtFld6 resignFirstResponder];
            [self.descriptionTextView becomeFirstResponder];
            return;
        }
    }
    
    
    else
    {
        if (txtActiveField == self.txtFld6)
        {
            CGRect fourthFrame = self.viewInsideHolder.frame;
            fourthFrame.origin.y = -320+self.scrollViewHolder.contentOffset.y;
            [UIView animateWithDuration:0.3 animations:^{
                self.viewInsideHolder.frame = fourthFrame;
            } completion:^(BOOL finished) {
                
            }];
            
            [self.txtFld6 resignFirstResponder];
            [self.descriptionTextView becomeFirstResponder];
            return;
        }
    }

    
    
    
    
}




-(void)gotoNextTextfield{
    // If the active textfield is the second one, there is no next so just return.
    
    [picker removeFromSuperview];
    //  [timePicker removeFromSuperview];
    NSLog(@"%@",txtActiveField);
    
    
    if (txtActiveField == self.txtFld4)    //for location text field
    {
        [self.txtFld4 resignFirstResponder];
        
        //        if(isPremiumSelected==YES)
        //        {
        //            CGRect sixthFrame = self.viewInsideHolder.frame;
        //            sixthFrame.origin.y =-200+self.scrollViewHolder.contentOffset.y;
        //            [UIView animateWithDuration:0.3 animations:^{
        //                self.viewInsideHolder.frame = sixthFrame;
        //            } completion:^(BOOL finished) {
        //
        //            }];
        //
        //            [self.txtFld4 resignFirstResponder];
        //            [self.txtFld6 becomeFirstResponder];
        //
        //            [picker removeFromSuperview];
        //            [timePicker removeFromSuperview];
        //
        //            return;
        //        }
        //
        //        else
        //        {
        //            [self.txtFld4 resignFirstResponder];
        //            [self.txtFld6 becomeFirstResponder];
        //            return;
        //        }
        
    }
    
    
    if(txtActiveField==_txtFld2)       //for business and address text field
    {
        CGRect frame = self.viewInsideHolder.frame;
        frame.origin.y = 0;
        [UIView animateWithDuration:0.3 animations:^{
            self.viewInsideHolder.frame = frame;
        } completion:^(BOOL finished) {
            
        }];
        [self closeTable];
        [_txtFld2 resignFirstResponder];
        [self.txtFld4 becomeFirstResponder];
        return;
    }
    

    
    if (txtActiveField == self.titleTextFld)    //for title text field
        
    {
        
        CGRect frame = self.viewInsideHolder.frame;
        frame.origin.y = 0;
        
        [UIView animateWithDuration:0.3 animations:^{
            self.viewInsideHolder.frame = frame;
        } completion:^(BOOL finished) {
            
        }];
        
        [_titleTextFld resignFirstResponder];
        [self.descriptionTextView becomeFirstResponder];
        return;
    }
    
    
    
    if (_txtViewActiveField == self.descriptionTextView)   //for description text view
        
    {
//        CGRect firstFrame = self.viewInsideHolder.frame;
////        firstFrame.origin.y = -77+self.scrollViewHolder.contentOffset.y;
//        [UIView animateWithDuration:0.3 animations:^{
//            self.viewInsideHolder.frame = firstFrame;
//        } completion:^(BOOL finished) {
//
//        }];
        CGRect frame = self.viewInsideHolder.frame;
        frame.origin.y = 0;
        
        [UIView animateWithDuration:0.3 animations:^{
            self.viewInsideHolder.frame = frame;
        } completion:^(BOOL finished) {
            
        }];
        
        [self.descriptionTextView resignFirstResponder];
//        [self.txtFld2 becomeFirstResponder];
        
        return;
    }
    
    
    

    
    
    

    
    
    
    if (txtActiveField == self.txtFld6)    //for price text field
    {
        if(isPremiumSelected==YES)
        {
            
            CGRect frame = self.viewInsideHolder.frame;
            frame.origin.y = 0;
            
            [UIView animateWithDuration:0.3 animations:^{
                self.viewInsideHolder.frame = frame;
            } completion:^(BOOL finished) {
                
            }];
            
            [txtActiveField resignFirstResponder];
            
        }
        
        else
            
        {
            [self.txtFld6 resignFirstResponder];
            [self.quantityTxtFld becomeFirstResponder];
            return;
            
        }
    }
    
    
//    _discountTxtFld;
//    if (txtActiveField == self.discountTxtFld)
//    {
//
//        [self.discountTxtFld resignFirstResponder];
//        [self.quantityTxtFld becomeFirstResponder];
//        return;
//
//    }
    
    
    
    if (txtActiveField == self.quantityTxtFld)
    {
        CGRect lastFrame = self.viewInsideHolder.frame;
        lastFrame.origin.y = -200+self.scrollViewHolder.contentOffset.y;
        [UIView animateWithDuration:0.3 animations:^{
            self.viewInsideHolder.frame = lastFrame;
        } completion:^(BOOL finished) {
            
        }];
        [self.quantityTxtFld resignFirstResponder];
        [self.termsAndConditionTextView becomeFirstResponder];
        return;
    }
    
    
    
    if (_txtViewActiveField == self.termsAndConditionTextView) {
        
        [self.termsAndConditionTextView resignFirstResponder];
        [self doneTyping];
        return;
        
    }
    
    
}

//-(void)gotoNextTextfield{
//    // If the active textfield is the second one, there is no next so just return.
//
//    [picker removeFromSuperview];
//  //  [timePicker removeFromSuperview];
//    NSLog(@"%@",txtActiveField);
//
//
//
//    if (txtActiveField == self.titleTextFld)    //for title text field
//
//    {
//
//        CGRect frame = self.viewInsideHolder.frame;
//        frame.origin.y = 0;
//
//        [UIView animateWithDuration:0.3 animations:^{
//            self.viewInsideHolder.frame = frame;
//        } completion:^(BOOL finished) {
//
//        }];
//
//        [_titleTextFld resignFirstResponder];
//        [self.descriptionTextView becomeFirstResponder];
//        return;
//    }
//
//
//
//    if (_txtViewActiveField == self.descriptionTextView)   //for description text view
//
//    {
//                CGRect firstFrame = self.viewInsideHolder.frame;
//                firstFrame.origin.y = -77+self.scrollViewHolder.contentOffset.y;
//                [UIView animateWithDuration:0.3 animations:^{
//                    self.viewInsideHolder.frame = firstFrame;
//                } completion:^(BOOL finished) {
//
//                }];
//
//        [self.descriptionTextView resignFirstResponder];
//        [self.txtFld2 becomeFirstResponder];
//
//        return;
//    }
//
//
//
//    if(txtActiveField==_txtFld2)       //for business and address text field
//    {
//        CGRect frame = self.viewInsideHolder.frame;
//        frame.origin.y = 0;
//        [UIView animateWithDuration:0.3 animations:^{
//            self.viewInsideHolder.frame = frame;
//        } completion:^(BOOL finished) {
//
//        }];
//        [self closeTable];
//        [_txtFld2 resignFirstResponder];
//
//
//
//    }
//
//
//
//    if (txtActiveField == self.txtFld4)    //for location text field
//    {
//        if(isPremiumSelected==YES)
//        {
//            CGRect sixthFrame = self.viewInsideHolder.frame;
//            sixthFrame.origin.y =-200+self.scrollViewHolder.contentOffset.y;
//            [UIView animateWithDuration:0.3 animations:^{
//                self.viewInsideHolder.frame = sixthFrame;
//            } completion:^(BOOL finished) {
//
//            }];
//
//            [self.txtFld4 resignFirstResponder];
//            [self.txtFld6 becomeFirstResponder];
//
//            [picker removeFromSuperview];
//            [timePicker removeFromSuperview];
//
//            return;
//        }
//
//        else
//        {
//            [self.txtFld4 resignFirstResponder];
//            [self.txtFld6 becomeFirstResponder];
//            return;
//        }
//
//    }
//
//
//
//    if (txtActiveField == self.txtFld6)    //for price text field
//    {
//        if(isPremiumSelected==YES)
//        {
//
//        CGRect frame = self.viewInsideHolder.frame;
//        frame.origin.y = 0;
//
//        [UIView animateWithDuration:0.3 animations:^{
//            self.viewInsideHolder.frame = frame;
//        } completion:^(BOOL finished) {
//
//        }];
//
//        [txtActiveField resignFirstResponder];
//
//        }
//
//        else
//
//        {
//            [self.txtFld6 resignFirstResponder];
//            [self.quantityTxtFld becomeFirstResponder];
//            return;
//
//        }
//    }
//
//
//
//    if (txtActiveField == self.discountTxtFld)
//    {
//
//        [self.discountTxtFld resignFirstResponder];
//        [self.quantityTxtFld becomeFirstResponder];
//        return;
//
//    }
//
//
//
//    if (txtActiveField == self.quantityTxtFld)
//    {
//            CGRect lastFrame = self.viewInsideHolder.frame;
//            lastFrame.origin.y = -200+self.scrollViewHolder.contentOffset.y;
//            [UIView animateWithDuration:0.3 animations:^{
//                self.viewInsideHolder.frame = lastFrame;
//            } completion:^(BOOL finished) {
//
//            }];
//            [self.quantityTxtFld resignFirstResponder];
//            [self.termsAndConditionTextView becomeFirstResponder];
//            return;
//    }
//
//
//
//    if (_txtViewActiveField == self.termsAndConditionTextView) {
//
//        [self.termsAndConditionTextView resignFirstResponder];
//        [self doneTyping];
//        return;
//
//    }
//
//
//}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    
      txtActiveField = textField;
    
      if (txtActiveField == self.txtFld2) {
    
    if(reservationChangePossible == NO)
    {
        
        UIAlertView *alertRes = [[UIAlertView alloc] initWithTitle:@"BUY TIMEE" message:@"Sorry, you cannot change this discount type to appointment type listing." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        
        // old one..  Sorry, you cannot change this discount type to reservation type listing.
        
        [alertRes show];
        
        return NO;
        
    }
      }
    
    return YES;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField{


    dateBtnClicked = false;
    timeBtnClicked = false;
    
    [picker removeFromSuperview];
    [timePicker removeFromSuperview];

  //  [timePicker removeFromSuperview];
       txtActiveField = textField;
    if(toolBar.hidden==NO)
    {
        toolBar.hidden=YES;
    }

    _txtViewActiveField=nil;
    
    [self createInputAccessoryView];
    [textField setInputAccessoryView:inputAccView];
    

    
    if (txtActiveField == self.txtFld1) {
        
        [picker removeFromSuperview];
        [self closeTable];
        

        CGRect firstFrame = self.viewInsideHolder.frame;
        
        firstFrame.origin.y = 0+self.scrollViewHolder.contentOffset.y;
        
        [UIView animateWithDuration:0.3 animations:^{
            self.viewInsideHolder.frame = firstFrame;
        } completion:^(BOOL finished) {
            
        }];
        
    }
    
    if (txtActiveField == self.titleTextFld) {
        [picker removeFromSuperview];
        [self closeTable];

        
        if(IS_IPHONE_4_OR_LESS)
        {
            CGRect secondFrame = self.viewInsideHolder.frame;
            secondFrame.origin.y = -60+self.scrollViewHolder.contentOffset.y;
            [UIView animateWithDuration:0.3 animations:^{
                self.viewInsideHolder.frame = secondFrame;
            } completion:^(BOOL finished) {
                
            }];

        }
        else
        {
            CGRect secondFrame = self.viewInsideHolder.frame;
            secondFrame.origin.y = 0+self.scrollViewHolder.contentOffset.y;
            [UIView animateWithDuration:0.3 animations:^{
                self.viewInsideHolder.frame = secondFrame;
            } completion:^(BOOL finished) {
                
            }];

        }
        
        
        
    }
    
    
    
    if (txtActiveField == self.txtFld2) {
        

        
        CGRect secondFrame = self.viewInsideHolder.frame;
        
//        secondFrame.origin.y = -150+self.scrollViewHolder.contentOffset.y;
        
      if(IS_IPHONE_5 || IS_IPHONE_4_OR_LESS)
      {
        secondFrame.origin.y = -_txtFld2.frame.origin.y+self.scrollViewHolder.contentOffset.y;
      }

        [UIView animateWithDuration:0.3 animations:^{
            self.viewInsideHolder.frame = secondFrame;
        } completion:^(BOOL finished) {
            
        }];
    
        
        
    //  [self businessTxtFldClciked]; // comment by vs on 14 jun 17 for handling to open business table
 
 
 }
    if (txtActiveField == self.txtFld3) {
        
        CGRect thirdFrame = self.viewInsideHolder.frame;
        
        thirdFrame.origin.y =-270+self.scrollViewHolder.contentOffset.y;
        
        [UIView animateWithDuration:0.3 animations:^{
            self.viewInsideHolder.frame = thirdFrame;
        } completion:^(BOOL finished) {
            
        }];
        
    }
    if (txtActiveField == self.txtFld4) {
        [picker removeFromSuperview];
        if(toolBar.hidden==NO)
        {
            toolBar.hidden=YES;
        }
        [self closeTable];


        CGRect fourthFrame = self.viewInsideHolder.frame;
        
//        fourthFrame.origin.y = -150+self.scrollViewHolder.contentOffset.y;
        fourthFrame.origin.y = 0+self.scrollViewHolder.contentOffset.y;

        [UIView animateWithDuration:0.3 animations:^{
            self.viewInsideHolder.frame = fourthFrame;
        } completion:^(BOOL finished) {
            
        }];
        
    }
//    if (txtActiveField == self.discountTxtFld) {
//        [picker removeFromSuperview];
//        if(toolBar.hidden==NO)
//        {
//            toolBar.hidden=YES;
//        }
//        [self closeTable];
//
//
//        CGRect fourthFrame = self.viewInsideHolder.frame;
//
////        fourthFrame.origin.y = -150+self.scrollViewHolder.contentOffset.y;
//        fourthFrame.origin.y = -_txtFld2.frame.origin.y+self.scrollViewHolder.contentOffset.y;
//
//        [UIView animateWithDuration:0.3 animations:^{
//            self.viewInsideHolder.frame = fourthFrame;
//        } completion:^(BOOL finished) {
//
//        }];
//
//    }
    if (txtActiveField == self.txtFld6) {
        
        
        if(IS_IPHONE_4_OR_LESS)
        {
            CGRect fifthFrame = self.viewInsideHolder.frame;
            fifthFrame.origin.y = -_chooseBtn.frame.origin.y-50+self.scrollViewHolder.contentOffset.y;
            [UIView animateWithDuration:0.3 animations:^{
                self.viewInsideHolder.frame = fifthFrame;
            } completion:^(BOOL finished) {
                
            }];

        }
        
        else
        {
            CGRect fifthFrame = self.viewInsideHolder.frame;
            fifthFrame.origin.y = -_chooseBtn.frame.origin.y+self.scrollViewHolder.contentOffset.y;
            [UIView animateWithDuration:0.3 animations:^{
                self.viewInsideHolder.frame = fifthFrame;
            } completion:^(BOOL finished) {
                
            }];

        }
        
        
        
        
    }
//    if (txtActiveField == self.txtFld6) {
//        [picker removeFromSuperview];
//        [self closeTable];
//
//
//        CGRect sixthFrame = self.viewInsideHolder.frame;
//        
//        sixthFrame.origin.y =-150+self.scrollViewHolder.contentOffset.y;
//        
//        [UIView animateWithDuration:0.3 animations:^{
//            self.viewInsideHolder.frame = sixthFrame;
//        } completion:^(BOOL finished) {
//            
//        }];
//        
//    }
    
    if (txtActiveField == self.quantityTxtFld) {
        [picker removeFromSuperview];
        [self closeTable];
        
        if(IS_IPHONE_4_OR_LESS)
        {
            CGRect sixthFrame = self.viewInsideHolder.frame;
            sixthFrame.origin.y =-_chooseBtn.frame.origin.y-50+self.scrollViewHolder.contentOffset.y;
            [UIView animateWithDuration:0.3 animations:^{
                self.viewInsideHolder.frame = sixthFrame;
            } completion:^(BOOL finished) {
                
            }];

        }
        else
        {
            CGRect sixthFrame = self.viewInsideHolder.frame;
            sixthFrame.origin.y =-_chooseBtn.frame.origin.y+self.scrollViewHolder.contentOffset.y;
            [UIView animateWithDuration:0.3 animations:^{
                self.viewInsideHolder.frame = sixthFrame;
            } completion:^(BOOL finished) {
                
            }];

        }
        
        
    }
    

    
}

-(void)doneTyping{
    // When the "done" button is tapped, the keyboard should go away.
    // That simply means that we just have to resign our first responder.
    
    _scrollViewHolder.scrollEnabled=YES;
    [picker removeFromSuperview];
  //  [self businessTxtFldClciked];
    if(toolBar.hidden==NO)
    {
        toolBar.hidden=YES;
    }
    [self closeTable];

    CGRect frame = self.viewInsideHolder.frame;
    frame.origin.y = 0;
    
    [UIView animateWithDuration:0.3 animations:^{
        self.viewInsideHolder.frame = frame;
    } completion:^(BOOL finished) {
        
    }];
    
    if (!_dateFormatter) {
        _dateFormatter = [NSDateFormatter new];
        [_dateFormatter setDateFormat:dateFormatDate];
    }
    if (txtActiveField == self.txtFld4 || txtActiveField == self.txtFld5) {
        
//        if (txtActiveField == self.txtFld4) {
//            dateStart = datePicker.date;
//        }else if (txtActiveField == self.txtFld5){
//            dateEnd = datePicker.date;
//        }
//        
//        txtActiveField.text = [_dateFormatter stringFromDate:datePicker.date];
        
    }
    
    [txtActiveField resignFirstResponder];
    [_txtViewActiveField resignFirstResponder];
    
    if (dateBtnClicked == true)
    {
        [self dueDateChanged:picker];
        dateBtnClicked = false;
    }
    
   if(timeBtnClicked == true)
   {
       [self dueTimeChanged:timePicker];
       timeBtnClicked = false;
   }
    
    
}


#pragma mark - text field delegate



-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [self closeTable];
    
    
    CGRect frame = self.viewInsideHolder.frame;
    frame.origin.y = 0;
    
    [UIView animateWithDuration:0.3 animations:^{
        self.viewInsideHolder.frame = frame;
    } completion:^(BOOL finished) {
        
    }];
    
    [textField resignFirstResponder];
    
    return YES;
}



-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    
    
    
    
    if (textField == self.txtFld3) {
        
        NSArray *dotComponents = [self.txtFld3.text componentsSeparatedByString:@"."];
        
        if (dotComponents.count>1) {
            
            if ([string isEqualToString:@"."]) {
                return [self.txtFld3.text componentsSeparatedByString:@"."].count<2;
            }
            
            NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
            NSArray *dotComponentsArrNew = [newString componentsSeparatedByString:@"."];
            if (dotComponentsArrNew.count>1) {
                NSString *afterDotString = [dotComponentsArrNew objectAtIndex:1];
                return afterDotString.length<3;
            }
            
        }else{
            
            //work updated at 07Apr2016
            NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
            NSUInteger newLength = [textField.text length] + [string length] - range.length;
            
            if(newString.length==10){
                [self.txtFld3 setText:newString];
                return NO;
            }else if (newLength>10) {
                return NO;
            }
            
        }
        
    }
    
    else if (textField == self.txtFld2)
    {
        
        [self businessTxtFldClciked];
        
            NSArray *dotComponents = [textField.text componentsSeparatedByString:@"."];
            
            if (dotComponents.count>1) {
                
                if ([string isEqualToString:@"."]) {
                    return [textField.text componentsSeparatedByString:@"."].count<2;
                }
                
                NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
                NSArray *dotComponentsArrNew = [newString componentsSeparatedByString:@"."];
                if (dotComponentsArrNew.count>1) {
                    NSString *afterDotString = [dotComponentsArrNew objectAtIndex:1];
                    return afterDotString.length<3;
                }
                
            }else{
                
                
            }
            
            NSString *tempStr = textField.text;
            
            tempStr = [tempStr stringByAppendingString:string];
            
            NSString *value = [textField.text stringByReplacingCharactersInRange:range withString:string];

        
        
        if (![value isEqualToString:@""]) {
            
            [self filterContentForSearchText:value];// scope:[[self.searchDisplayController.searchBar scopeButtonTitles] objectAtIndex:[self.searchDisplayController.searchBar selectedScopeButtonIndex]]];
            
            isFromSearchResult=YES;
            
        }else{
            
            isFromSearchResult=NO;
//
//            [_premiumBtn setTitle:@"" forState:UIControlStateNormal];
//            [_discountBtn setTitle:@"" forState:UIControlStateNormal];
//            _premiumBtnHeight.constant=0;
//            _discountBtnHeight.constant=0;
            
            
            [UIView transitionWithView:_wantCashBtn
                              duration:0.4
                               options:UIViewAnimationOptionTransitionCrossDissolve
                            animations:^{
                                _wantCashBtn.hidden=NO;
                            }
                            completion:NULL];
            [UIView transitionWithView:_creditFineBtn
                              duration:0.4
                               options:UIViewAnimationOptionTransitionCrossDissolve
                            animations:^{
                                _creditFineBtn.hidden=NO;
                            }
                            completion:NULL];
            [UIView transitionWithView:_helpMeDecideBtn
                              duration:0.4
                               options:UIViewAnimationOptionTransitionCrossDissolve
                            animations:^{
                                _helpMeDecideBtn.hidden=NO;
                            }
                            completion:NULL];
            
            _wantCashBtn.hidden=YES;
            _creditFineBtn.hidden=YES;
            _helpMeDecideBtn.hidden=YES;
            
//            if(IS_IPHONE_6)
//            {
////                _premiumBtnHeight.constant=0;
////                _discountBtnHeight.constant=0;
//                _dateAndTimeBtnTop.constant=0;
//                _timeBtnTop.constant=0;
//            }
//
//
//            else if(IS_IPHONE_5)
//            {
////                _premiumBtnHeight.constant=0;
////                _discountBtnHeight.constant=0;
//                _dateAndTimeBtnTop.constant=0;
//                _timeBtnTop.constant=0;
//            }
//
//
//            else if(IS_IPHONE_6P)
//            {
////                _premiumBtnHeight.constant=0;
////                _discountBtnHeight.constant=0;
//
//                _dateAndTimeBtnTop.constant=0;
//                _timeBtnTop.constant=0;
//
//            }


            
        }
        
        [self.tblView layoutIfNeeded];
        [self.tblView reloadData];
        [self.tblView layoutIfNeeded];
        
        if (isFromSearchResult) {
            
            CGFloat totalHeight = (searchResultArrayFromRadius.count+1) * 32;
            
            if (totalHeight>96) {
                
                [self.view layoutIfNeeded];
                
              //  self.constraintViewHolderTableRadiusHeight.constant = 96;
                
                [UIView animateWithDuration:0.3 animations:^{
                    
                    [self.view layoutIfNeeded];
                    
                } completion:^(BOOL finished) {
                    
                }];
                
            }else{
                
                [self.view layoutIfNeeded];
                
             //   self.constraintViewHolderTableRadiusHeight.constant = totalHeight;
                
                [UIView animateWithDuration:0.3 animations:^{
                    
                    [self.view layoutIfNeeded];
                    
                } completion:^(BOOL finished) {
                    
                }];
                
            }
            
        }else{
            
            if (businessData.count>0) {
                
                [self.view layoutIfNeeded];
                
               // self.constraintViewHolderTableRadiusHeight.constant = 96;
                
                [UIView animateWithDuration:0.3 animations:^{
                    
                    [self.view layoutIfNeeded];
                    
                } completion:^(BOOL finished) {
                    
                }];
                
            }else{
                
            }
            
        }
        

        
    }


    return YES;
}


- (void)filterContentForSearchText:(NSString*)searchText //scope:(NSString*)scope
{
    
//    NSPredicate *predicateFName = [NSPredicate predicateWithFormat:@"SELF.BusinessName contains[cd] %@", searchText];
//    
//    searchResultArrayFromRadius = [myAppDelegate.arrayBusinessDetail filteredArrayUsingPredicate:predicateFName];
//    
//    if (searchResultArrayFromRadius.count>0) {
//        
//    }else{
//        
//    }

    
    
    
    NSPredicate *predicateFName = [NSPredicate predicateWithFormat:@"SELF contains[cd] %@", searchText];
    
    searchResultArrayFromRadius = [businessData filteredArrayUsingPredicate:predicateFName];
    
    if (searchResultArrayFromRadius.count>0) {
        
    }else{
        
    }
//     isFromSearchResult=YES;
//    [self.tblView reloadData];
}



-(void)textFieldDidEndEditing:(UITextField *)textField{
    
    
    [picker removeFromSuperview];
    [timePicker removeFromSuperview];
    
    if(toolBar.hidden==NO)
    {
        toolBar.hidden=YES;
    }

    if (!_dateFormatter) {
        _dateFormatter = [NSDateFormatter new];
        [_dateFormatter setDateFormat:dateFormatDate];
    }
    if (txtActiveField == self.txtFld4 || txtActiveField == self.txtFld5) {
        
//        txtActiveField.text = [_dateFormatter stringFromDate:datePicker.date];
//        
//        if (txtActiveField == self.txtFld4) {
//            dateStart = datePicker.date;
//        }else if (txtActiveField == self.txtFld5){
//            dateEnd = datePicker.date;
//        }
        
    }
    
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if (click1) {
        return catArr.count;
    }else if(click2){
        return selectedSubCatArr.count;
    }
    else if(bussinessTextFldClicked==YES)
    {
        if(isFromSearchResult)
        {
            if (searchResultArrayFromRadius.count == 0)
            {
                return 1;
            }
            else
            {
            return searchResultArrayFromRadius.count;
            }
        }
        else
        {
        return myAppDelegate.arrayBusinessDetail.count;
        }
    }
    
    return 0;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 40;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *MyIdentifier = @"Cell";
    
    SelectCategoryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    
    
    
    if (cell == nil){
        cell = [[SelectCategoryTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                                  reuseIdentifier:MyIdentifier];
    }
 //   cell.cellLbl.textAlignment = NSTextAlignmentCenter;
    
    
    if(IS_IPHONE_5 || IS_IPHONE_4_OR_LESS)
    {

    
    }
    
    
    
    if (click1) {
        
        category *catObj = [catArr objectAtIndex:indexPath.row];
        
        
        NSLog(@"%@",catArr);
        
        
        cell.cellLbl.text = catObj.name;
        
    }else if (click2){
        
        subCategory *subCAtObj = [selectedSubCatArr objectAtIndex:indexPath.row];
        
        cell.cellLbl.text = subCAtObj.name;
        
    }
    
    else if(bussinessTextFldClicked==YES)
    {
            if(isFromSearchResult)
        {
            
            if (searchResultArrayFromRadius.count == 0)
            {
                cell.cellLbl.text = @"Business not found.";
            }
            else
            {
            cell.cellLbl.text = [searchResultArrayFromRadius objectAtIndex:indexPath.row];
            }
         //   selectedBusinessName = [searchResultArrayFromRadius objectAtIndex:indexPath.row];
            
        }
        
        else
        {
        
           cell.cellLbl.text = [businessData objectAtIndex:indexPath.row];
            
         //   selectedBusinessName = [businessData objectAtIndex:indexPath.row];
            
            
//            BusinessInfo *businessObj = [myAppDelegate.arrayBusinessDetail objectAtIndex:indexPath.row];
//            
//            NSLog(@"%@",myAppDelegate.arrayBusinessDetail);
//            NSLog(@"%@",[EncryptDecryptFile decrypt:businessObj.BusinessName]);

            
         //   cell.cellLbl.text=businessData;

            
            
            
            
            
            
        }
        
        
        
    }
    
    

    
    
    return cell;
}

//-(BOOL)textFieldShouldReturn:(UITextField *)textField{

-(void)businessTxtFldClciked
{
    
    
    [_txtFld2 becomeFirstResponder];
    bussinessTextFldClicked=YES;
    
    
    _tblView.hidden = NO;
    
    click1 = false;
    click2 = false;
    
    if (!isOpened) {
        
        isOpened = true;
        
        if(show==true){
            
            [self.view layoutIfNeeded];
            
         //   businessTableOpen=YES;
            
            
            CGFloat tblTop = _lblLineChooseBtn.frame.origin.y - _businessLineLbl.frame.origin.y;
            
            
//            if(IS_IPHONE_5)
//
//            {
//                __tblTop.constant= _txtFld2.frame.origin.y-15;
//                //self._tblTop.constant = 140;
//
//            }
//
//            else if(IS_IPHONE_6)
//
//            {
//                __tblTop.constant=_txtFld2.frame.origin.y-15;
//                //self._tblTop.constant = 166;
//
//            }
//            else if(IS_IPHONE_6P)
//
//            {
//                __tblTop.constant=_txtFld2.frame.origin.y-15;
//
//                //self._tblTop.constant = 176;
//
//            }

            __tblTop.constant = -tblTop;
            
            //   [GlobalFunction rotateView:[self.chooseBtn imageView] atAngle:-180];
            
            [self openTable];
            
            //show=false;
            
        }else if(show==false){
            
            //   [GlobalFunction rotateView:[self.chooseBtn imageView] atAngle:0];
            
            [self closeTable];
            
            show=true;
            
        }
        
    }

    
}





- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    SelectCategoryTableViewCell *cell = (SelectCategoryTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
    
    NSString *s1=cell.cellLbl.text;
    NSAttributedString *s22 = [[NSAttributedString alloc] initWithString:s1];
    if(click1==true){
        
        if (selectedCatObj) {
            if (selectedCatObj == [catArr objectAtIndex:indexPath.row]) {
                
            }else{
                selectedCatObj = nil;
                selectedCatObj = [catArr objectAtIndex:indexPath.row];
                selectedSubCatArr = nil;
                selectedSubCatArr = [[NSMutableArray alloc]init];
                for (subCategory *subObj in subCatArr) {
                    if ([subObj.categoryId isEqualToString:selectedCatObj.categoryId]) {
                        [selectedSubCatArr addObject:subObj];
                    }
                }
               // [_chooseBtn setTitle:s1 forState:UIControlStateNormal];
                 [_chooseBtn setAttributedTitle:s22 forState:UIControlStateNormal];
                [_subBtn setTitle:@"Sub Category" forState:UIControlStateNormal];
            }
        }else{
            selectedCatObj = nil;
            selectedCatObj = [catArr objectAtIndex:indexPath.row];
            selectedSubCatArr = nil;
            selectedSubCatArr = [[NSMutableArray alloc]init];
            for (subCategory *subObj in subCatArr) {
                if ([subObj.categoryId isEqualToString:selectedCatObj.categoryId]) {
                    [selectedSubCatArr addObject:subObj];
                }
            }
            [_chooseBtn setAttributedTitle:s22 forState:UIControlStateNormal];
            //[_chooseBtn setTitle:s1 forState:UIControlStateNormal];
        }
        [self ChooseCategory:self.chooseBtn];
    }
    else if(click2==true)
    {
        
        selectedSubCatObj = nil;
        selectedSubCatObj = [selectedSubCatArr objectAtIndex:indexPath.row];
        
        [_subBtn setAttributedTitle:s22 forState:UIControlStateNormal];
        [self SubCategory:self.subBtn];
    }
    else if (bussinessTextFldClicked==YES)
    {
        selectedBusinessObj = nil;
        if (isFromSearchResult)
        {
            
            if (searchResultArrayFromRadius.count<=0) {
                return;
            }
            
            
            NSString  *selectedBusinessName11 = @"";

            selectedBusinessObj = [searchResultArrayFromRadius objectAtIndex:indexPath.row];
            
            NSLog(@"lowerCaseString is: %@", [(NSString *)selectedBusinessObj lowercaseString]);
            selectedBusinessName11 = [(NSString *)selectedBusinessObj lowercaseString];
            
            for (int i = 0; i<= myAppDelegate.arrayBusinessDetail.count; i++)
            {
                // selectedBusinessObj = [[BusinessInfo alloc]init];
                BusinessInfo *businessObj11 = [myAppDelegate.arrayBusinessDetail objectAtIndex:i];
                NSString *name1 = [EncryptDecryptFile decrypt:businessObj11.BusinessName];
                name1 = [name1 lowercaseString];
               // selectedBusinessName = [selectedBusinessName11 lowercaseString];
                NSLog(@"name1 is... %@",name1);
                NSLog(@"selectedBusinessName is... %@",selectedBusinessName11);
                if ([name1 isEqualToString:selectedBusinessName11])
                {
                    selectedBusinessObj = nil;
                    selectedBusinessObj = businessObj11;
                    
                    break;
                }
                
            }

            
            
        }
        else
        {
            selectedBusinessObj = [myAppDelegate.arrayBusinessDetail objectAtIndex:indexPath.row];
        }
       // selectedSubCatArr = nil;
       //selectedSubCatArr = [[NSMutableArray alloc]init];
        
        
//        for (subCategory *subObj in myAppDelegate.arrayBusinessDetail) {
//            if ([subObj.categoryId isEqualToString:selectedCatObj.categoryId]) {
//                [selectedSubCatArr addObject:subObj];
//            }
//        }
        
        
        
        
       // [EncryptDecryptFile decrypt:selectedBusinessObj.BusinessName]
        
        
        NSLog(@"business Name :%@",selectedBusinessObj.BusinessName);
        _txtFld2.text = [EncryptDecryptFile decrypt:selectedBusinessObj.BusinessName];
        NSLog(@"business BusinessAdd :%@",selectedBusinessObj.BusinessAdd);
        _txtFld4.text = [EncryptDecryptFile decrypt:selectedBusinessObj.BusinessAdd];
        
        NSLog(@"business categoryId :%@",selectedBusinessObj.categoryId);
        NSString * catID = [EncryptDecryptFile decrypt:selectedBusinessObj.categoryId];
      //  NSString *categoryName = [self categoryNameByCategoryID:catID];
        NSMutableAttributedString *categoryName = [[NSMutableAttributedString alloc] initWithString:[self categoryNameByCategoryID:catID]];
        
        NSAttributedString *star = [[NSAttributedString alloc] initWithString:@"*" attributes:@{ NSForegroundColorAttributeName :[UIColor redColor],
                                                                                                 }];
        
        NSMutableAttributedString *catName = [[NSMutableAttributedString alloc] initWithString:@"Choose Category" attributes:@{ NSForegroundColorAttributeName :[UIColor colorWithRed:63.0/255.0f green:67.0/255.0f blue:70.0/255.0f alpha:1.0],
                                                                                                                                }];
        [catName appendAttributedString:star];
        if(!categoryName)
        {
            [_chooseBtn setAttributedTitle:catName forState:UIControlStateNormal];
        }
        else
        {
          //  [_chooseBtn setTitle:categoryName forState:UIControlStateNormal];
             [_chooseBtn setAttributedTitle:categoryName forState:UIControlStateNormal];
        }
        
        selectedCatObj = [self categoryByCategoryID:catID];
        
        NSLog(@"%@",[EncryptDecryptFile decrypt:selectedBusinessObj.BusinessName]);
        NSLog(@" business id ----%@",[EncryptDecryptFile decrypt:selectedBusinessObj.BusinessId]);
        NSLog(@"user id val --- %@", myAppDelegate.userData.BusinessUserId);
        
        
        
        if([myAppDelegate.userData.BusinessUserId isEqualToString:[EncryptDecryptFile decrypt:selectedBusinessObj.BusinessId]])
        {
            
            
//            isDiscountSelected=YES;
//            isPremiumSelected=NO;

            
//            [_premiumBtn setTitle:@"PREMIUM" forState:UIControlStateNormal];
//            [_discountBtn setTitle:@"DISCOUNT" forState:UIControlStateNormal];
//
//            [self.premiumBtn setTitleColor:[UIColor colorWithRed:255.0/255.0f green:255.0/255.0f blue:255.0/255.0f alpha:1.0] forState:UIControlStateNormal];
//            _premiumBtn.backgroundColor=[UIColor colorWithRed:63.0/255.0f green:67.0/255.0f blue:70.0/255.0f alpha:1.0];
//
//            [self.discountBtn setTitleColor:[UIColor colorWithRed:63.0/255.0f green:67.0/255.0f blue:70.0/255.0f alpha:1.0] forState:UIControlStateNormal];
//            _discountBtn.backgroundColor=[UIColor colorWithRed:244.0/255.0f green:244.0/255.0f blue:244.0/255.0f alpha:1.0];
//
//
            
            
            [UIView transitionWithView:_wantCashBtn
                              duration:0.4
                               options:UIViewAnimationOptionTransitionCrossDissolve
                            animations:^{
                                _wantCashBtn.hidden=YES;
                            }
                            completion:NULL];
            [UIView transitionWithView:_helpMeDecideBtn
                              duration:0.4
                               options:UIViewAnimationOptionTransitionCrossDissolve
                            animations:^{
                                _helpMeDecideBtn.hidden=YES;
                            }
                            completion:NULL];
            
            [UIView transitionWithView:_creditFineBtn
                              duration:0.4
                               options:UIViewAnimationOptionTransitionCrossDissolve
                            animations:^{
                                _creditFineBtn.hidden=YES;
                            }
                            completion:NULL];
            
            
//            if(IS_IPHONE_6)
//            {
//                _dateAndTimeBtnTop.constant=12;
//                _timeBtnTop.constant=12;
//                _premiumBtnHeight.constant=36;
//                _discountBtnHeight.constant=36;
//            }
//
//            else if(IS_IPHONE_5)
//            {
//                _dateAndTimeBtnTop.constant=14;
//                _timeBtnTop.constant=14;
//
//                _premiumBtnHeight.constant=30;
//                _discountBtnHeight.constant=30;
//
//                _premiumBtnTop.constant=14;
//                _discountBtnTop.constant=14;
//
//            }
//
//            else if (IS_IPHONE_6P)
//            {
//                _premiumBtnHeight.constant=40;
//                _discountBtnHeight.constant=40;
//
//                _dateAndTimeBtnTop.constant=15;
//                _timeBtnTop.constant=15;
//
//
//
//            }
            
        }
        
        else{
            [UIView transitionWithView:_wantCashBtn
                              duration:0.4
                               options:UIViewAnimationOptionTransitionCrossDissolve
                            animations:^{
                                _wantCashBtn.hidden=NO;
                            }
                            completion:NULL];
            [UIView transitionWithView:_creditFineBtn
                              duration:0.4
                               options:UIViewAnimationOptionTransitionCrossDissolve
                            animations:^{
                                _creditFineBtn.hidden=NO;
                            }
                            completion:NULL];
            [UIView transitionWithView:_helpMeDecideBtn
                              duration:0.4
                               options:UIViewAnimationOptionTransitionCrossDissolve
                            animations:^{
                                _helpMeDecideBtn.hidden=NO;
                            }
                            completion:NULL];

            
            
            _wantCashBtn.hidden=YES;
            _creditFineBtn.hidden=YES;
            _helpMeDecideBtn.hidden=YES;
            
            self.quantityTxtFld.text = @"";
            self.termsAndConditionTxtFld.text = @"";
            self.discountTxtFld.text = @"";
            
            picker.userInteractionEnabled=YES ;
            _scrollViewHolder.scrollEnabled=YES;

            
//            isPremiumSelected=YES;
//            isDiscountSelected=NO;
            
//            [_premiumBtn setTitle:@"" forState:UIControlStateNormal];
//            [_discountBtn setTitle:@"" forState:UIControlStateNormal];
            
            
            [self premiumBtnClicked:nil];


//            if(IS_IPHONE_6)
//            {
////                _premiumBtnHeight.constant=0;
////                _discountBtnHeight.constant=0;
//                _dateAndTimeBtnTop.constant=0;
//                _timeBtnTop.constant=0;
//            }
//
//
//            else if(IS_IPHONE_5)
//            {
////                _premiumBtnHeight.constant=0;
////                _discountBtnHeight.constant=0;
//                _dateAndTimeBtnTop.constant=0;
//                _timeBtnTop.constant=0;
//            }
//
//
//            else if(IS_IPHONE_6P)
//            {
////                _premiumBtnHeight.constant=0;
////                _discountBtnHeight.constant=0;
//
//                _dateAndTimeBtnTop.constant=0;
//                _timeBtnTop.constant=0;
//
//            }
            

        }
        _txtFld2.text=s1;
        businessNameForShare = s1;
        show=false;
        
        [self businessTxtFldClciked];
        [txtActiveField resignFirstResponder];
        CGRect frame = self.viewInsideHolder.frame;
        frame.origin.y = 0;
        
        [UIView animateWithDuration:0.3 animations:^{
            self.viewInsideHolder.frame = frame;
        } completion:^(BOOL finished) {
            
        }];
   }
    
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (tableView == self.tblView) {
        
        // Remove seperator inset
        if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
            [cell setSeparatorInset:UIEdgeInsetsZero];
        }
        
        // Prevent the cell from inheriting the Table View's margin settings
        if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
            [cell setPreservesSuperviewLayoutMargins:NO];
        }
        
        // Explictly set your cell's layout margins
        if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
            [cell setLayoutMargins:UIEdgeInsetsZero];
        }
        
    }
}


- (IBAction)ChooseCategory:(id)sender{
    [self.view endEditing:YES];
    if (myAppDelegate.arrayCategoryDetail.count>0) {
        
        [self.tblView layoutIfNeeded];
        [self.tblView reloadData];
        [self.tblView layoutIfNeeded];
        
        _tblView.hidden = NO;
        
        click1 = true;
        click2 = false;
        
        if (!isOpened) {
            
            isOpened = true;
            
            if(show==true){
                
                [self.view layoutIfNeeded];
                
                self._tblTop.constant = -1;
                
           //     [GlobalFunction rotateView:[self.chooseBtn imageView] atAngle:-180];
                
//                CGRect firstFrame = self.viewInsideHolder.frame;
//
//                firstFrame.origin.y = -_txtFld4.frame.origin.y+self.scrollViewHolder.contentOffset.y;
//
//                [UIView animateWithDuration:0.3 animations:^{
//                    self.viewInsideHolder.frame = firstFrame;
//                } completion:^(BOOL finished) {
//
//                }];
                
                
                [self openTable];
                
                show=false;
                
            }else if(show==false){
                
            //    [GlobalFunction rotateView:[self.chooseBtn imageView] atAngle:0];
//                CGRect frame = self.viewInsideHolder.frame;
//                frame.origin.y = 0;
//
//                [UIView animateWithDuration:0.3 animations:^{
//                    self.viewInsideHolder.frame = frame;
//                } completion:^(BOOL finished) {
//
//                }];
                [self closeTable];
                
                show=true;
                
            }
            
        }
        
        
    }else{
        
        [[GlobalFunction shared] showAlertForMessage:@"No categories returned from server."];
        
    }
    
    
}

-(void)openTable{
    
    [self.tblView layoutIfNeeded];
    [self.tblView reloadData];
    [self.tblView layoutIfNeeded];
    
 //   [txtActiveField resignFirstResponder];
    
    self.viewTableTapHandler.hidden = false;
    
    [self.view layoutIfNeeded];
    
    NSInteger count = 0;
    if (click1) {
        if (catArr.count>5) {
            count = 5;
        }else{
            count = catArr.count;
        }
    }else if (click2)
    {
        if (selectedSubCatArr.count>5) {
            count = 5;
        }else{
            count = selectedSubCatArr.count;
        }
        
        
        
        
    }
    
    
    if(bussinessTextFldClicked==YES)
    {
        count = 5;
        
    }

    
    self.tblHeight.constant = 40*count;
    
    [UIView animateWithDuration:0.5f
                          delay:0.0f
                        options:UIViewAnimationOptionTransitionFlipFromTop | UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         [self.view layoutIfNeeded];
                         
                         isOpened = false;
                         
                     }completion:NULL];
    
}

-(void)closeTable{
    
    [self.view layoutIfNeeded];
    
    self.tblHeight.constant = 0;
    
    [UIView animateWithDuration:0.5 delay:0.0 options:UIViewAnimationOptionTransitionFlipFromBottom | UIViewAnimationOptionCurveEaseInOut animations:^{
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished) {
        _tblView.hidden = YES;
        click1 = false;
        click2 = false;
        self.viewTableTapHandler.hidden = true;
        isOpened = false;
    }];
    
}

- (IBAction)closeOpenTable:(id)sender {
    
    if (click1) {
        
        [self ChooseCategory:self.chooseBtn];
        
    }else if(click2){
        
        [self SubCategory:self.subBtn];
        
    }
    
}


- (IBAction)SubCategory:(id)sender{
    
//    if (!selectedCatObj) {
//        
//        [[GlobalFunction shared] showAlertForMessage:@"Please select category first."];
//        
//        return;
//    }
//    
//    [self.tblView layoutIfNeeded];
//    [self.tblView reloadData];
//    [self.tblView layoutIfNeeded];
//    
//    _tblView.hidden = NO;
//    
//    click2 = true;
//    click1 = false;
//    
//    if (!isOpened) {
//        
//        isOpened = true;
//        
//        if(show1==true){
//            
//            [self.view layoutIfNeeded];
//            
//            self._tblTop.constant = self.constraintTextFieldHeight.constant + 14;
//            
//            [GlobalFunction rotateView:[self.subBtn imageView] atAngle:-180];
//            
//            [self openTable];
//            
//            show1=false;
//            
//        }
//        else if(show1==false){
//            
//            [GlobalFunction rotateView:[self.subBtn imageView] atAngle:0];
//            
//            [self closeTable];
//            
//            show1=true;
//            
//        }
//        
//    }
    
}


- (IBAction)takeAPictureClicked:(id)sender {
    
    [self performWorkForCameraCapture];
    
}

-(void)performWorkForCameraCapture{
    
    // For check camera availability
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        
        AVAuthorizationStatus status = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
        
        if(status == AVAuthorizationStatusAuthorized) { // authorized
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [self openPhotos:1];
                
            });
        }
        else if(status == AVAuthorizationStatusDenied){ // denied
            
            [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
                
                if(granted){ // Access has been granted ..do something
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        [self openPhotos:1];
                        
                    });
                    
                    
                } else { // Access denied ..do something
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        [self camDenied];
                        
                    });
                    
                }
            }];
            
        }
        else if(status == AVAuthorizationStatusRestricted){ // restricted
            
            [[GlobalFunction shared] showAlertForMessage:@"Camera uses in application is restricted, please check and try again."];
            
        }
        else if(status == AVAuthorizationStatusNotDetermined){ // not determined
            
            [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
                
                if(granted){ // Access has been granted ..do something
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        [self openPhotos:1];
                        
                    });
                    
                } else { // Access denied ..do something
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        [self camDenied];
                        
                    });
                    
                }
            }];
        }
        
    }else{
        
        [[GlobalFunction shared] showAlertForMessage:@"There is no camera available on device."];
        
    }
    
}

- (void)camDenied{
    
    NSString *alertText;
    NSString *alertButton;
    
    alertText = @"BUY TIMEE wants access to camera for getting images to provide better user client interaction.";
    
    alertButton = @"Go";
    
    UIAlertView *alert = [[UIAlertView alloc]
                          initWithTitle:@"Buy Timee"
                          message:alertText
                          delegate:self
                          cancelButtonTitle:alertButton
                          otherButtonTitles:nil];
    
    alert.tag = 3491832;
    [alert show];
}


- (IBAction)photoLibraryClicked:(id)sender {
    
    [self performWorkForPhotoLibrary];
    
}

-(void)performWorkForPhotoLibrary{
    
    if (IS_OS_8_OR_LATER) {
        
        PHAuthorizationStatus status = [PHPhotoLibrary authorizationStatus];
        
        if (status == PHAuthorizationStatusAuthorized) {
            
            // Access has been granted.
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [self openPhotos :0];
                
            });
            
        }else if (status == PHAuthorizationStatusDenied) {
            
            // Access has been denied.
            [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
                
                if (status == PHAuthorizationStatusAuthorized) {
                    
                    // Access has been granted.
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        [self openPhotos :0];
                        
                    });
                    
                    
                }else {
                    
                    // Access has been denied.
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        [self photosDenied];
                        
                    });
                }
            }];
            
        }else if (status == PHAuthorizationStatusNotDetermined) {
            
            // Access has not been determined.
            [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
                
                if (status == PHAuthorizationStatusAuthorized) {
                    
                    // Access has been granted.
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        [self openPhotos :0];
                        
                    });
                    
                }else {
                    // Access has been denied.
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        [self photosDenied];
                        
                    });
                }
            }];
            
        }else if (status == PHAuthorizationStatusRestricted) {
            
            // Restricted access - normally won't happen.
            
        }
        
    }else{
        
        ALAuthorizationStatus statusAsset = [ALAssetsLibrary authorizationStatus];
        switch (statusAsset) {
            case ALAuthorizationStatusNotDetermined: {
                // not determined
                
                ALAssetsLibrary *lib = [[ALAssetsLibrary alloc] init];
                [lib enumerateGroupsWithTypes:ALAssetsGroupSavedPhotos usingBlock:^(ALAssetsGroup *group, BOOL *stop) {
                    
                    // Access has been granted.
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        [self openPhotos :0];
                        
                    });
                    
                } failureBlock:^(NSError *error) {
                    if (error.code == ALAssetsLibraryAccessUserDeniedError) {
                        NSLog(@"user denied access, code: %zd", error.code);
                    } else {
                        NSLog(@"Other error code: %zd", error.code);
                    }
                    
                    // Access has been denied.
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        [self photosDenied];
                        
                    });
                    
                }];
                
                break;
            }
            case ALAuthorizationStatusRestricted: {
                // restricted
                break;
            }
            case ALAuthorizationStatusDenied: {
                
                // denied
                
                ALAssetsLibrary *lib = [[ALAssetsLibrary alloc] init];
                [lib enumerateGroupsWithTypes:ALAssetsGroupSavedPhotos usingBlock:^(ALAssetsGroup *group, BOOL *stop) {
                    // Access has been granted.
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        [self openPhotos :0];
                        
                    });
                    
                } failureBlock:^(NSError *error) {
                    if (error.code == ALAssetsLibraryAccessUserDeniedError) {
                        NSLog(@"user denied access, code: %zd", error.code);
                    } else {
                        NSLog(@"Other error code: %zd", error.code);
                    }
                    
                    // Access has been denied.
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        [self photosDenied];
                        
                    });
                    
                }];
                
                break;
            }
            case ALAuthorizationStatusAuthorized: {
                // authorized
                
                // Access has been granted.
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    [self openPhotos :0];
                    
                });
                
                break;
            }
            default: {
                break;
            }
        }
        
    }
    
}

-(void)photosDenied{
    
    NSString *alertText;
    NSString *alertButton;
    
    alertText = @"BUY TIMEE wants access to photos for getting images to provide better user client interaction.";
    
    alertButton = @"Go";
    
    UIAlertView *alert = [[UIAlertView alloc]
                          initWithTitle:@"BUY TIMEE"
                          message:alertText
                          delegate:self
                          cancelButtonTitle:alertButton
                          otherButtonTitles:nil];
    alert.tag = 3491832;
    [alert show];
    
}


#pragma mark - collection view delegate

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    // Adjust cell size for orientation
    return CGSizeMake(((SCREEN_WIDTH-36)/3)-3,60);
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return pics.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    TaskCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"createTask" forIndexPath:indexPath];
    cell.imgViewTaskImage.image = (UIImage*) [pics objectAtIndex:indexPath.row];
    cell.imgViewTaskImage.clipsToBounds = YES;
    
    cell.btnDeleteImage.tag=indexPath.row;
    
    return cell;
}


-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    //    _blurView.hidden=NO;
    _blurViewImgeView.userInteractionEnabled=YES;
    _blurViewImgeView.image=(UIImage *) [pics objectAtIndex:indexPath.row];
    
    [self showPopupForIndexpath:indexPath];
    
}


#pragma mark - pop up handling
-(void)showPopupForIndexpath:(NSIndexPath *)iPath{
    
    indexForTableRow = iPath.row;
    
    TaskCollectionViewCell *tCell = (TaskCollectionViewCell *)[self.collectionViewTaskImages cellForItemAtIndexPath:iPath];
    
    CGPoint center = [[self.collectionViewTaskImages cellForItemAtIndexPath:iPath] convertPoint:tCell.imgViewTaskImage.center toView:self.collectionViewTaskImages];
    
    CGPoint center2 = [self.collectionViewTaskImages convertPoint:center toView:nil];
    
    center2.y = center2.y - 72; // - self.scrollViewHolder.contentOffset.y;
    
    centerTogoBack = center2;
    
    self.blurView.hidden = NO;
    self.blurViewImgeView.hidden = NO;
    self.blurViewCrossBtn.hidden = NO;
    
    self.blurView.alpha = 0.1;
    self.blurViewImgeView.alpha = 0.1;
    self.blurViewCrossBtn.alpha = 0.1;
    
    self.blurViewImgeView.transform = CGAffineTransformMakeScale(0.1, 0.1);
    self.blurViewImgeView.center = centerTogoBack;
    
    [UIView animateWithDuration:0.5
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         
                         self.blurViewImgeView.alpha = 1.0;
                         self.blurView.alpha = 0.8;
                         self.blurViewCrossBtn.alpha = 1.0;
                         
                         self.blurViewImgeView.transform = CGAffineTransformMakeScale(1.0, 1.0);
                         
                         CGPoint centerOfView = self.view.center;
                         
                         centerOfView.y = centerOfView.y - (72/2);
                         
                         self.blurViewImgeView.center = centerOfView;
                         
                     } completion:^(BOOL finished) {
                         
                         
                     }];
    
}

#pragma mark - popup tap handling

//************************************************

- (void)imageSwipe:(UISwipeGestureRecognizer *)swipe{
    
    [UIView animateWithDuration:0.5 animations:^(void){
        
        if (swipe.direction == UISwipeGestureRecognizerDirectionLeft){
            
            [GlobalFunction SwipeRight:_blurViewImgeView];
            
            NSUInteger index = [pics indexOfObject:_blurViewImgeView.image] + 1;
            centerTogoBack.x = centerTogoBack.x + (((SCREEN_WIDTH-36)/3)-3);
            
            if(index<pics.count){
                _blurViewImgeView.image = [pics objectAtIndex:index];//
            }else{
                centerTogoBack.x = centerTogoBack.x - (((SCREEN_WIDTH-36)/3)-3);
                
                self.blurViewImgeView.transform = CGAffineTransformMakeScale(1.0, 1.0);
                self.blurViewImgeView.alpha = 1.0;
                
                [UIView animateWithDuration:0.5
                                      delay:0.0
                                    options:UIViewAnimationOptionCurveEaseOut
                                 animations:^{
                                     
                                     self.blurViewImgeView.alpha = 0.1;
                                     self.blurView.alpha = 0.1;
                                     self.blurViewCrossBtn.alpha = 0.1;
                                     
                                     self.blurViewImgeView.transform = CGAffineTransformMakeScale(0.1, 0.1);
                                     
                                     self.blurViewImgeView.center = centerTogoBack;
                                     
                                 } completion:^(BOOL finished) {
                                     
                                     self.blurView.hidden = YES;
                                     self.blurViewImgeView.hidden = YES;
                                     self.blurViewCrossBtn.hidden = YES;
                                     
                                 }];
            }
            
        }else if (swipe.direction == UISwipeGestureRecognizerDirectionRight){
            
            [GlobalFunction SwipeLeft:_blurViewImgeView];
            
            NSUInteger index = [pics indexOfObject:_blurViewImgeView.image] - 1;
            
            centerTogoBack.x = centerTogoBack.x - (((SCREEN_WIDTH-36)/3)-3);
            
            if(index<pics.count){
                _blurViewImgeView.image = [pics objectAtIndex:index];//
            }else{
                centerTogoBack.x = centerTogoBack.x + (((SCREEN_WIDTH-36)/3)-3);
                
                self.blurViewImgeView.transform = CGAffineTransformMakeScale(1.0, 1.0);
                self.blurViewImgeView.alpha = 1.0;
                
                [UIView animateWithDuration:0.5
                                      delay:0.0
                                    options:UIViewAnimationOptionCurveEaseOut
                                 animations:^{
                                     
                                     self.blurViewImgeView.alpha = 0.1;
                                     self.blurView.alpha = 0.1;
                                     self.blurViewCrossBtn.alpha = 0.1;
                                     
                                     self.blurViewImgeView.transform = CGAffineTransformMakeScale(0.1, 0.1);
                                     
                                     self.blurViewImgeView.center = centerTogoBack;
                                     
                                 } completion:^(BOOL finished) {
                                     
                                     self.blurView.hidden = YES;
                                     self.blurViewImgeView.hidden = YES;
                                     self.blurViewCrossBtn.hidden = YES;
                                     
                                 }];
            }
            
        }
        
    }];
}

//************************************************


-(void)hideVew:(id)sender{
    
}

- (IBAction)deleteTaskImage:(id)sender {
    
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"BUY TIMEE"
                                                    message:@"Are you sure, you want to delete the image?"
                                                   delegate:self
                                          cancelButtonTitle:@"Ok"
                                          otherButtonTitles:@"Cancel", nil];
    
    alert.tag = 5001;
    [alert show];
    
    UIButton *senderButton = (UIButton *)sender;
    indexValue = [NSIndexPath indexPathForRow:senderButton.tag inSection:0];
    
}

#pragma mark - create task
- (IBAction)createTask:(id)sender {
  // [myAppDelegate.vcObj performSegueWithIdentifier:@"ShareViewController" sender:nil];

    [GlobalFunction addIndicatorView];
    [self performSelector:@selector(performCreateTaskWork) withObject:nil afterDelay:0.2];
    
}

-(void) updateShareWork

{
    
    NSLog(@"title is%@",_titleTextFld.text);
    NSLog(@"start date is%@",startDateForShare);
    NSLog(@"end date is%@",startTime);
    NSLog(@"location is%@",self.txtFld4.text);
    NSLog(@"discount is%@",discountmain);
    NSLog(@"business is%@",self.txtFld2.text);
    
    // cell.lblTaskPrice.text = [NSString stringWithFormat:@"$%0.2f",taskObj.price];
    NSString *mainMsgStr = @"I have an appointment for sale with";
    NSString *mainLastMsgStr = @"if anyone is interested check out the  Buy Timee app!";
    
    NSString *discountMsgStr = @"What an amazing deal get";
    NSString *discountvalueStr = [NSString stringWithFormat:@"$%@ off with",discountmain];
    NSString *discountLastMsgStr = @"Check out the Buy Timee app for more information! ";
        
    NSString *dateString1;
    NSString *timeString;
    
    if ([startDate isEqualToString:@"1"])
    {
        
        dateString1 = @"AnyDate";;
    }
    else
    {
        NSDateFormatter *dfForShare = [[NSDateFormatter alloc]init];
        [dfForShare setDateFormat:@"YYYY/MM/dd"];
        startDateForShare = [dfForShare stringFromDate:dateStart];
         dateString1 = [GlobalFunction convertDateFormat:startDateForShare];
    }
    if ([startTime isEqualToString:@"1"] )
    {
        
        timeString = @"AnyTime";
    }
    else
    {
        timeString = [GlobalFunction convertTimeFormat:startTime];
      //  timeString = startTime;
    }
    
    NSString *finalStr = @"";
    
    if ([reservationCreateStatus isEqualToString:@"1"])
    {
        NSString *str11 = [@[discountMsgStr, discountvalueStr]componentsJoinedByString:@" "];
        NSString *str12 = [@[str11, self.txtFld2.text]componentsJoinedByString:@" "];
        NSString *str13 = [@[str12, timeString]componentsJoinedByString:@" at "];
        NSString *str14 = [@[str13, dateString1]componentsJoinedByString:@", "];
        finalStr = [@[str14, discountLastMsgStr]componentsJoinedByString:@". "];
    }
 else
 {
     NSString *str1 = [@[mainMsgStr, self.txtFld2.text]componentsJoinedByString:@" "];
     NSString *str2 = [@[str1, timeString]componentsJoinedByString:@" at "];
     NSString *str3 = [@[str2, dateString1]componentsJoinedByString:@", "];
     finalStr = [@[str3, mainLastMsgStr]componentsJoinedByString:@" "];

 }
    
    
    
    
//    NSString *strMain0 = [@[self.titleTextFld.text, self.txtFld2.text] componentsJoinedByString:@" at "];
//    NSString *strMain = [@[strMain0, self.txtFld4.text] componentsJoinedByString:@", "];
//    NSString *strMain1 = [@[dateString1, timeString] componentsJoinedByString:@"  "];
//    
//    NSString *strMain2 = [@[strMain, dateString1] componentsJoinedByString:@" on "];
    
    
    
    myAppDelegate.reservationImage_forShare =nil;
    myAppDelegate.reservationImgName_forShare = @"";
    
    
    if (pics.count == 0) {
        
    }
    else
    {
        UIImage *imgShare =  [pics objectAtIndex:0];
        NSString *imgNameShare =  [picsNameArray objectAtIndex:0];
        
        myAppDelegate.reservationImage_forShare = imgShare;
        myAppDelegate.reservationImgName_forShare = imgNameShare;
    }
    
    myAppDelegate.reservationDetailString_forShare = finalStr;
    
}

-(void)performCreateTaskWork{
    
    if (!selectedCatObj) {
        [[GlobalFunction shared] showAlertForMessage:@"Please select a category to continue."];    //for category
        [GlobalFunction removeIndicatorView];
        return;
    }
    
    
    if ([[self.titleTextFld.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""]) {
        [[GlobalFunction shared] showAlertForMessage:@"Title cannot be blank."];    //for title
        [GlobalFunction removeIndicatorView];
        return;
    }
    
    
    if ([[self.txtFld1.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""]) {
        [[GlobalFunction shared] showAlertForMessage:@"Description cannot be blank."];   //for description
        [GlobalFunction removeIndicatorView];
        return;
    }
    
    
    if ([[self.txtFld2.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""]) {
        [[GlobalFunction shared] showAlertForMessage:@"Business name and Address cannot be blank."];  //for business and address name
        [GlobalFunction removeIndicatorView];
        return;
    }
    
    
    if ([[self.txtFld4.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""]) {
        
        [[GlobalFunction shared] showAlertForMessage:@"Location cannot be blank."];   //for task location
        [GlobalFunction removeIndicatorView];
        return;
        
    }
    
    // here edit work by vs for identify discount or main price...
    
    pricemain = @"";
    discountmain = @"";
    
    if(isDiscountSelected==YES)
    {
        if ([[self.txtFld6.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""]) {
            
            [[GlobalFunction shared] showAlertForMessage:@"Discount cannot be blank."];   //for task price
            [GlobalFunction removeIndicatorView];
            return;
            
        }
        else
        {
            int price10 = [self.txtFld6.text intValue];
            if (price10 < 10) {
                
                [[GlobalFunction shared] showAlertForMessage:@"Discount should be minimum $10."];
                [GlobalFunction removeIndicatorView];
                return;
            }
            else
            {
                discountmain = self.txtFld6.text;
            }
            
        }
        
    }
    
    else
    {
        if ([[self.txtFld6.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""]) {
            
            [[GlobalFunction shared] showAlertForMessage:@"Price cannot be blank."];   //for task price
            [GlobalFunction removeIndicatorView];
            return;
            
        }
        else
        {
            pricemain = self.txtFld6.text;
        }
        
    }
    //********
    //********
    
    if (![[self.txtFld6.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""]) {
        
        int price11 = [self.txtFld6.text intValue];
        
        if (price11 < 2) {
            
            [[GlobalFunction shared] showAlertForMessage:@"Price must be in between $2 to $499999."];
            [GlobalFunction removeIndicatorView];
            return;
        }
        if (price11 >= 500000)
        {
            
            [[GlobalFunction shared] showAlertForMessage:@"Price must be in between $2 to $499999."];
            [GlobalFunction removeIndicatorView];
            return;
        }
        // NSString *mainPrice1 = self.txtFld6.text;
        
        // [[GlobalFunction shared] showAlertForMessage:@"Price cannot be blank."];   //for task price
        if (pics.count>5) {
            
            [[GlobalFunction shared] showAlertForMessage:@"Maximum five images can be selected."];   //for number of images
            [GlobalFunction removeIndicatorView];
            return;
        }

        
        
    }
    
    
//********
    if(startDate.length==0 || startTime.length==0)
    {
        [[GlobalFunction shared] showAlertForMessage:@"Date/Time cannot be blank."];   //for date and time
        [GlobalFunction removeIndicatorView];
        return;
    }
    
    NSDateFormatter *df = [[NSDateFormatter alloc]init];
    [df setDateFormat:dateFormatDate];
    

    NSComparisonResult resultStartToEnd = [dateStart compare:dateEnd];
    
    if(resultStartToEnd == NSOrderedDescending){
        [[GlobalFunction shared] showAlertForMessage:@"End date should be greater than start date."];
        [GlobalFunction removeIndicatorView];
        return;
    }else if(resultStartToEnd == NSOrderedAscending){
    }else if (resultStartToEnd == NSOrderedSame) {
    }
    
    NSDateFormatter *df2 = [[NSDateFormatter alloc]init];
    [df2 setDateFormat:@"YYYY-MM-dd"];
   // NSString *startDate = [df2 stringFromDate:dateStart];
   // NSString *endDateStr = [df2 stringFromDate:dateEnd];
    
  //  if (myAppDelegate.isPaymentFilled) {
        
        if (![[WebService shared] connected]) {
            
            [GlobalFunction removeIndicatorView];
            
            [[GlobalFunction shared] showAlertForMessage:@"Can't connect to internet, please check and try again."];
            
            return;
        }
        
        
        CLLocationCoordinate2D coordinate = [[GlobalFunction shared] getLocationFromAddressString:self.txtFld4.text];
    
    NSString *timeZoneSeconds = [GlobalFunction getTimeZone];
    
    
      //  NSDictionary *dictCreateTaskResponse = [[WebService shared] createTaskuserid:myAppDelegate.userData.userid subcategoryId:selectedSubCatObj.subCategoryId title:self.txtFld1.text description:self.txtFld2.text taskImgNamesArray:picsNameArray price:self.txtFld3.text startDate:startDate endDate:endDateStr locationName:self.txtFld6.text latitude:[NSString stringWithFormat:@"%f",coordinate.latitude] longitude:[NSString stringWithFormat:@"%f",coordinate.longitude] taskImgArray:pics];
        
    
    NSLog(@"%@",_quantityTxtFld.text);
    NSLog(@"%@",_discountTxtFld.text);
    
    //******************************************************************** done by kp
    
    NSString *descriptionData=_descriptionTextView.text;
    NSString *termsAndConditionData=_termsAndConditionTextView.text;
    
    if([descriptionData isEqual:@"Description"])
    {
        NSLog(@"no content");
        descriptionData=@"";
        
    }
    
    if([termsAndConditionData isEqual:@"Terms and conditions"])
    {
        NSLog(@"no content");
        termsAndConditionData=@"";
        
    }

    selectedBusinessName = _txtFld2.text;
    
     NSLog(@"selected B Name is... %@",selectedBusinessName);
    
    NSPredicate *predicateFName = [NSPredicate predicateWithFormat:@"SELF == [c] %@", selectedBusinessName];//contains[cd]
    
    searchResultArrayFromRadius = [businessData filteredArrayUsingPredicate:predicateFName];
        if (searchResultArrayFromRadius.count == 0)
        {
            [[GlobalFunction shared] showAlertForMessage:@"Please select a valid business from existing businesses."];   //for date and time
            [GlobalFunction removeIndicatorView];
            return;
        }
    else
    {
        for (int i = 0; i<= myAppDelegate.arrayBusinessDetail.count; i++)
        {
           // selectedBusinessObj = [[BusinessInfo alloc]init];
            BusinessInfo *businessObj11 = [myAppDelegate.arrayBusinessDetail objectAtIndex:i];
            NSString *name1 = [EncryptDecryptFile decrypt:businessObj11.BusinessName];
            name1 = [name1 lowercaseString];
            selectedBusinessName = [selectedBusinessName lowercaseString];
             NSLog(@"name1 is... %@",name1);
             NSLog(@"selectedBusinessName is... %@",selectedBusinessName);
            if ([name1 isEqualToString:selectedBusinessName])
            {
                selectedBusinessObj = nil;
                selectedBusinessObj = businessObj11;
                
                break;
            }
            
        }
        
       // selectedBusinessObj.BusinessId = s
    }
//    if (selectedBusinessObj == nil)
//    {
//        [[GlobalFunction shared] showAlertForMessage:@"Please select a valid business from existing businesses."];   //for date and time
//        [GlobalFunction removeIndicatorView];
//        return;
//    }
    //******************************************************************** done by kp
   // NSLog(@"business id is... %@",[EncryptDecryptFile decrypt:selectedBusinessObj.BusinessId]);
    
    NSLog(@"start Date is this %@ // %@",startDate,startTime);
    
    NSDictionary *dictCreateTaskResponse = [[WebService shared]createTaskuserId:myAppDelegate.userData.userid categoryId:selectedCatObj.categoryId businessId:[EncryptDecryptFile decrypt:selectedBusinessObj.BusinessId] paymentMethod:@"0" title:_titleTextFld.text description:descriptionData taskImgNamesArray:picsNameArray price:pricemain startDate:startDate startTime:startTime locationName:self.txtFld4.text latitude:[NSString stringWithFormat:@"%f",coordinate.latitude] longitude:[NSString stringWithFormat:@"%f",coordinate.longitude] taskImgArray:pics timeZone:timeZoneSeconds discount:discountmain totalUsers:self.quantityTxtFld.text discountDescription:termsAndConditionData edit:editStatus reservationId:reservationId_ForEdit];

        if (dictCreateTaskResponse) {
            
            NSLog(@"response --- %@",[EncryptDecryptFile decrypt:[dictCreateTaskResponse valueForKey:@"responseMessage"]]);
            
            if ([[EncryptDecryptFile decrypt:[dictCreateTaskResponse valueForKey:@"responseCode"]] isEqualToString:@"200"]) {
                
                reservationCreateStatus = [EncryptDecryptFile decrypt:[dictCreateTaskResponse valueForKey:@"discountStatus"]];
                
                editStatusForNavigation =@"";
                
                editStatusForNavigation = [EncryptDecryptFile decrypt:[dictCreateTaskResponse valueForKey:@"update"]];
                
                alertString = @"";
                alertString = [EncryptDecryptFile decrypt:[dictCreateTaskResponse valueForKey:@"responseMessage"]];
                
                if ([[GlobalFunction shared] callUpdateWebservice]) {
                    
                    
                    if ([editStatusForNavigation isEqualToString:@"1"])
                    {
                   
                    }
                    else
                    {
                         [self updateShareWork];
                    }
                   
                    
                    [self performSelector:@selector(performSegue) withObject:nil afterDelay:0.2];
                    
                }else{
                    
                    //******************************************************************** done by kp
//                    _termsAndConditionTextView.text = @"Terms and conditions";
//                    _termsAndConditionTextView.textColor = [UIColor colorWithRed:143.0/255.0f green:143.0/255.0f blue:143.0/255.0f alpha:1.0];
//                    ;
//                    [self addInputViewToTextView:self.termsAndConditionTextView];
//                    
//                    _descriptionTextView.text = @"Description";
//                    _descriptionTextView.textColor = [UIColor colorWithRed:143.0/255.0f green:143.0/255.0f blue:143.0/255.0f alpha:1.0];
//                    ;
//                    [self addInputViewToTextView:self.descriptionTextView];
                    
                    //******************************************************************** done by kp

                    
                    
                    
                    [GlobalFunction removeIndicatorView];
                    
                    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"BUY TIMEE" message:@"Failed to update, please try again." delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Retry", nil];
                    
                    alertView.tag = 999;
                    
                    [alertView show];
                    
                }
                
            }else{
                
                [[GlobalFunction shared] showAlertForMessage:[EncryptDecryptFile decrypt:[dictCreateTaskResponse valueForKey:@"responseMessage"]]];
                
                [GlobalFunction removeIndicatorView];
                
            }
            
        }else{
            
            [[GlobalFunction shared] showAlertForMessage:@"Server is down, please try again later."];
            
            [GlobalFunction removeIndicatorView];
            
        }
        
//    }else{
//        
//        [[GlobalFunction shared] showAlertForMessage:@"You need to fill your credit card information first, before creating any task."];
//        
//        [GlobalFunction removeIndicatorView];
//        
//    }
    
}


-(void)updateWebServiceHandling{
    
    if (![self checkForInternet]) {
        
        [self performSelector:@selector(performSegue) withObject:nil afterDelay:0.2];
        
        return;
    }
    
    if ([[GlobalFunction shared] callUpdateWebservice]) {
        
        
        [self performSelector:@selector(performSegue) withObject:nil afterDelay:0.2];
        
    }
    else
    {
        
        [GlobalFunction removeIndicatorView];
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"BUY TIMEE" message:@"Failed to update, please try again." delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Retry", nil];
        
        alertView.tag = 999;
        
        [alertView show];
        
    }
    
}

-(BOOL)checkForInternet{
    
    BOOL isConnected = true;
    
    if (![[WebService shared] connected]) {
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"BUY TIMEE" message:@"Can't connect to internet, update not successful. Please check your internet connection then try again." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        [alertView show];
        
        isConnected = false;
        
    }
    
    return isConnected;
}


-(void)performSegue{
    
//    NSMutableArray *arr = [DatabaseClass getDataFromUserData:[[NSString stringWithFormat:@"Select * from %@",tableUser] UTF8String]];
//    
//    myAppDelegate.userData = (user *)[arr firstObject];
//    
//    [myAppDelegate.vcObj.tblViewSideBar reloadData];
    
    //these three lines are comment by vs 21 june 17 at 9.30 pm bcz not update business id at first time in db
    
    myAppDelegate.arrayTaskDetail = [DatabaseClass getDataFromTaskData:[[NSString stringWithFormat:@"Select * from %@",tableTaskDetail] UTF8String]];
    
    [myAppDelegate.arrayTaskDetail sortUsingDescriptors:
     @[
       [NSSortDescriptor sortDescriptorWithKey:@"taskid_Integer" ascending:NO],
       ]];
    
    [GlobalFunction removeIndicatorView];
    
    

    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"BUY TIMEE" message:alertString delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
    
    myAppDelegate.isBankDetailFilled = true;
    
    alertView.tag = 998;
    
    [alertView show];
    
}


- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex{
    
    if (alertView.tag == 3491832){
        
        NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
        [[UIApplication sharedApplication] openURL:url];
        
    }else if(alertView.tag == 5001){
        
        if (buttonIndex == 0){
            
            [pics removeObjectAtIndex:indexValue.row];
            [picsNameArray removeObjectAtIndex:indexValue.row];
            
            [self.collectionViewTaskImages reloadData];
            
            if(pics.count==0){
                [self.view layoutIfNeeded];
                self.constraintCollectionViewHeight.constant=0;
                [UIView animateWithDuration:0.5 animations:^{
                    [self.view layoutIfNeeded];
                } completion:^(BOOL finished) {
                    self.collectionViewTaskImages.hidden=YES;
                }];
            }
            
        }else if (buttonIndex == 1){
            
        }
        
    }else if (alertView.tag == 999) {
        
        if (buttonIndex == [alertView cancelButtonIndex]) {
            
          //  [self performSelector:@selector(reloadTable) withObject:nil afterDelay:0.2];
            
        }else{
            
            [GlobalFunction addIndicatorView];
            
            [self performSelector:@selector(updateWebServiceHandling) withObject:nil afterDelay:0.2];
            
        }
        
    }else if (alertView.tag == 998){
        
        if (buttonIndex == [alertView cancelButtonIndex]) {
            
            if ([editStatusForNavigation isEqualToString:@"1"])
             {
                 TaskTableViewCell *cell = (TaskTableViewCell *)[myAppDelegate.vcObj.tblViewSideBar cellForRowAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:0]];
                 [myAppDelegate.vcObj performSegueWithIdentifier:@"taskhistory" sender:cell];
             }
            else
                 {
            
             [myAppDelegate.vcObj performSegueWithIdentifier:@"ShareViewController" sender:nil];
                 }
        }
        
    }
    if (alertView.tag == 1000) {
        
        if (buttonIndex == [alertView cancelButtonIndex]) {
            
            
        }else{
            
//            myAppDelegate.isSideBarAccesible = true;
//
//            myAppDelegate.isFromEdit = false;
//            //   [self performSegueWithIdentifier:@"paymentInfo" sender:myAppDelegate.vcObj.btnPaymentInfo];
//            [myAppDelegate.vcObj performSegueWithIdentifier:@"paymentInfo" sender:nil];
//            //  [myAppDelegate.vcObj performSegueWithIdentifier:@"paymentInfo" sender:myAppDelegate.vcObj.btnPaymentInfo];
            
            // All above lines comment by VS for add PayPal integration 17 July 18...
            
            [GlobalFunction addIndicatorView];
            
            [self startCheckout];
            
        }
        
    }
    if (alertView.tag == 9999) {
        
        if (buttonIndex == [alertView cancelButtonIndex]) {
            
            
        }else{
            
                        myAppDelegate.isSideBarAccesible = true;
            
                        myAppDelegate.isFromEdit = false;
                        //   [self performSegueWithIdentifier:@"paymentInfo" sender:myAppDelegate.vcObj.btnPaymentInfo];
                        [myAppDelegate.vcObj performSegueWithIdentifier:@"paymentInfo" sender:nil];
                        //  [myAppDelegate.vcObj performSegueWithIdentifier:@"paymentInfo" sender:myAppDelegate.vcObj.btnPaymentInfo];
            
            // All above lines comment by VS for add PayPal integration 17 July 18...
            
//            [GlobalFunction addIndicatorView];
//
//            [self startCheckout];
//
        }
        
    }

}


-(void)openPhotos :(int)index{
    
    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
    
    if (index==0) {
        
        // Create the image picker
        // init picker
        CTAssetsPickerController *picker = [[CTAssetsPickerController alloc] init];
        
        // set delegate
        picker.delegate = self;
        
        // present picker modally
        [myAppDelegate.window.rootViewController presentViewController:picker animated:YES completion:nil];
        
        return;
        
    }
    else{
        imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
    }
    
    imagePickerController.delegate = (id)self;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        //Code that presents or dismisses a view controller here
        
        [myAppDelegate.window.rootViewController presentViewController:imagePickerController animated:YES completion:nil];
        
    });
    
}

- (void)assetsPickerController:(CTAssetsPickerController *)picker didFinishPickingAssets:(NSArray *)assets{
    // assets contains PHAsset objects.
    isImagePickFromAlbum=YES;
    NSMutableArray *imageArray = [[NSMutableArray alloc] init];
    
    for (PHAsset *assetObjs in assets) {
        
        PHImageManager *manager = [PHImageManager defaultManager];
        
        PHImageRequestOptions *requestOptions = [[PHImageRequestOptions alloc] init];
        requestOptions.resizeMode   = PHImageRequestOptionsResizeModeExact;
        requestOptions.deliveryMode = PHImageRequestOptionsDeliveryModeHighQualityFormat;
        
        // this one is key
        requestOptions.synchronous = true;
        __block UIImage *thumbnailImage = [[UIImage alloc]init];
        
        [manager requestImageForAsset:assetObjs
                           targetSize:PHImageManagerMaximumSize
                          contentMode:PHImageContentModeDefault
                              options:requestOptions
                        resultHandler:^void(UIImage *image, NSDictionary *info) {
                            thumbnailImage = image;
                            
                        }];
        
        [imageArray addObject:thumbnailImage];
    }
    
    [pics addObjectsFromArray:imageArray];
    
    
    
    NSInteger count = pics.count;
    NSInteger prevCount = count - assets.count;
    
    for (int i = 0; i<assets.count; i++) {
        if (prevCount == 0) {
            //[picsNameArray addObject:[NSString stringWithFormat:@"taskImg"]];
            NSString *imageName=[NSString stringWithFormat:@"taskImg%f",[[NSDate date] timeIntervalSince1970]];
            imageName=[imageName stringByReplacingOccurrencesOfString:@"." withString:@""];
            [picsNameArray addObject:imageName];
        }else{
            //[picsNameArray addObject:[NSString stringWithFormat:@"taskImg%ld",(long)prevCount]];
            NSString *imageName=[NSString stringWithFormat:@"taskImg%f",[[NSDate date] timeIntervalSince1970]];
            imageName=[imageName stringByReplacingOccurrencesOfString:@"." withString:@""];
            [picsNameArray addObject:imageName];
        }
        prevCount = prevCount + 1;
    }
    
    if (self.constraintCollectionViewHeight.constant == 0) {
        
        [self.view layoutIfNeeded];
        self.collectionViewTaskImages.hidden = NO;
        self.constraintCollectionViewHeight.constant = 60;
        [UIView animateWithDuration:0.5 animations:^{
            [self.view layoutIfNeeded];
        } completion:^(BOOL finished) {
            
        }];
        
    }
    
    [self.collectionViewTaskImages layoutIfNeeded];
    [self.collectionViewTaskImages reloadData];
    [self.collectionViewTaskImages layoutIfNeeded];
    
    [picker dismissViewControllerAnimated:YES completion:nil];
    
}


-(void)assetsPickerControllerDidCancel:(CTAssetsPickerController *)picker{
    isImagePickFromAlbum=YES;

    [picker dismissViewControllerAnimated:YES completion:nil];
    
}

- (UIImage *)scaleAndRotateImage:(UIImage *)image {
    int kMaxResolution = 1800; //640; // Or whatever
    
    CGImageRef imgRef = image.CGImage;
    
    CGFloat width = CGImageGetWidth(imgRef);
    CGFloat height = CGImageGetHeight(imgRef);
    
    
    CGAffineTransform transform = CGAffineTransformIdentity;
    CGRect bounds = CGRectMake(0, 0, width, height);
    if (width > kMaxResolution || height > kMaxResolution) {
        CGFloat ratio = width/height;
        if (ratio > 1) {
            bounds.size.width = kMaxResolution;
            bounds.size.height = roundf(bounds.size.width / ratio);
        }
        else {
            bounds.size.height = kMaxResolution;
            bounds.size.width = roundf(bounds.size.height * ratio);
        }
    }
    
    CGFloat scaleRatio = bounds.size.width / width;
    CGSize imageSize = CGSizeMake(CGImageGetWidth(imgRef), CGImageGetHeight(imgRef));
    CGFloat boundHeight;
    UIImageOrientation orient = image.imageOrientation;
    switch(orient) {
            
        case UIImageOrientationUp: //EXIF = 1
            transform = CGAffineTransformIdentity;
            break;
            
        case UIImageOrientationUpMirrored: //EXIF = 2
            transform = CGAffineTransformMakeTranslation(imageSize.width, 0.0);
            transform = CGAffineTransformScale(transform, -1.0, 1.0);
            break;
            
        case UIImageOrientationDown: //EXIF = 3
            transform = CGAffineTransformMakeTranslation(imageSize.width, imageSize.height);
            transform = CGAffineTransformRotate(transform, M_PI);
            break;
            
        case UIImageOrientationDownMirrored: //EXIF = 4
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.height);
            transform = CGAffineTransformScale(transform, 1.0, -1.0);
            break;
            
        case UIImageOrientationLeftMirrored: //EXIF = 5
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(imageSize.height, imageSize.width);
            transform = CGAffineTransformScale(transform, -1.0, 1.0);
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
            break;
            
        case UIImageOrientationLeft: //EXIF = 6
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.width);
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
            break;
            
        case UIImageOrientationRightMirrored: //EXIF = 7
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeScale(-1.0, 1.0);
            transform = CGAffineTransformRotate(transform, M_PI / 2.0);
            break;
            
        case UIImageOrientationRight: //EXIF = 8
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(imageSize.height, 0.0);
            transform = CGAffineTransformRotate(transform, M_PI / 2.0);
            break;
            
        default:
            [NSException raise:NSInternalInconsistencyException format:@"Invalid image orientation"];
            
    }
    
    UIGraphicsBeginImageContext(bounds.size);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    if (orient == UIImageOrientationRight || orient == UIImageOrientationLeft) {
        CGContextScaleCTM(context, -scaleRatio, scaleRatio);
        CGContextTranslateCTM(context, -height, 0);
    }
    else {
        CGContextScaleCTM(context, scaleRatio, -scaleRatio);
        CGContextTranslateCTM(context, 0, -height);
    }
    
    CGContextConcatCTM(context, transform);
    
    CGContextDrawImage(UIGraphicsGetCurrentContext(), CGRectMake(0, 0, width, height), imgRef);
    UIImage *imageCopy = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return imageCopy;
}



// This method is called when an image has been chosen from the library or taken from the camera.
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    
    //You can retrieve the actual UIImage
    isImagePickFromAlbum=YES;
    
    UIImage *image = [info valueForKey:UIImagePickerControllerOriginalImage];
    
    //*******
    
    if (picker.sourceType == UIImagePickerControllerSourceTypeCamera) {
        
//        if(picker.cameraDevice==UIImagePickerControllerCameraDeviceFront)
//        {
//            image = [UIImage imageWithCGImage:image.CGImage scale:1.0 orientation:UIImageOrientationLeftMirrored];
//        }
//        else
//        {
//            image = [UIImage imageWithCGImage:image.CGImage scale:1.0 orientation:UIImageOrientationRight];
//        }
        
        
    }
    else
    {
        //         userImage = [UIImage imageWithCGImage:userImage.CGImage scale:1.0 orientation:UIImageOrientationUp];
    }
    
    
    //*******
    
    
     image=[self scaleAndRotateImage:image];
    
    
    
    
    [pics addObject:image];
    
    NSInteger count = pics.count;
    
    if (count == 1) {
        NSString *imageName=[NSString stringWithFormat:@"taskImg%f",[[NSDate date] timeIntervalSince1970]];
        imageName=[imageName stringByReplacingOccurrencesOfString:@"." withString:@""];
        [picsNameArray addObject:imageName];
//        [picsNameArray addObject:[NSString stringWithFormat:@"taskImg"]];
    }else{
        NSString *imageName=[NSString stringWithFormat:@"taskImg%f",[[NSDate date] timeIntervalSince1970]];
        imageName=[imageName stringByReplacingOccurrencesOfString:@"." withString:@""];
        [picsNameArray addObject:imageName];
        //[picsNameArray addObject:[NSString stringWithFormat:@"taskImg%ld",(long)(count-1)]];
    }
    
    if (self.constraintCollectionViewHeight.constant == 0) {
        
        [self.view layoutIfNeeded];
        self.collectionViewTaskImages.hidden = NO;
        self.constraintCollectionViewHeight.constant = 60;
        [UIView animateWithDuration:0.5 animations:^{
            [self.view layoutIfNeeded];
        } completion:^(BOOL finished) {
            
        }];
        
    }
    
    [self.collectionViewTaskImages layoutIfNeeded];
    [self.collectionViewTaskImages reloadData];
    [self.collectionViewTaskImages layoutIfNeeded];
    
    [picker dismissViewControllerAnimated:YES completion:nil];
    
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    
    [picker dismissViewControllerAnimated:YES completion:nil];
    
}

//************************************************


- (IBAction)HideBlurViewTap:(id)sender {
    
    
    if(helpMeBtnClciked==YES)
    {
//        [UIView animateWithDuration:0.5
//                              delay:0.0
//                            options:UIViewAnimationOptionCurveEaseOut
//                         animations:^{
//                             
//                             self.blurView.hidden = YES;
//                             _alertView.hidden=YES;
//                             helpMeBtnClciked=NO;
//    
//
//                             
//                         } completion:^(BOOL finished) {
//                             
//                             self.blurView.hidden = YES;
//                             _alertView.hidden=YES;
//
//                             helpMeBtnClciked=NO;
//  
//                         }];
        
      //  helpMeBtnClciked=NO;
        
    }
    
    
    else
    {
    self.blurViewImgeView.transform = CGAffineTransformMakeScale(1.0, 1.0);
    self.blurViewImgeView.alpha = 1.0;
    
    [UIView animateWithDuration:0.5
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         
                         self.blurViewImgeView.alpha = 0.1;
                         self.blurView.alpha = 0.1;
                         self.blurViewCrossBtn.alpha = 0.1;
                         
                         self.blurViewImgeView.transform = CGAffineTransformMakeScale(0.1, 0.1);
                         
                         self.blurViewImgeView.center = centerTogoBack;
                         
                     } completion:^(BOOL finished) {
                         
                         self.blurView.hidden = YES;
                         self.blurViewImgeView.hidden = YES;
                         self.blurViewCrossBtn.hidden = YES;
                         
                     }];
        
    }
    
}


//************************************************

//*******

- (IBAction)blurViewCrossBtn:(id)sender {
    
    self.blurViewImgeView.transform = CGAffineTransformMakeScale(1.0, 1.0);
    self.blurViewImgeView.alpha = 1.0;
    
    [UIView animateWithDuration:0.5
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         
                         self.blurViewImgeView.alpha = 0.1;
                         self.blurView.alpha = 0.1;
                         self.blurViewCrossBtn.alpha = 0.1;
                         
                         self.blurViewImgeView.transform = CGAffineTransformMakeScale(0.1, 0.1);
                         
                         self.blurViewImgeView.center = centerTogoBack;
                         
                     } completion:^(BOOL finished) {
                         
                         self.blurView.hidden = YES;
                         self.blurViewImgeView.hidden = YES;
                         self.blurViewCrossBtn.hidden = YES;
                         
                     }];
    
}

//********


//click upload image icon...

- (IBAction)openCameraToCaptureTaskImages:(id)sender {
    
   // [self performWorkForCameraCapture];
    
   // [self performWorkForPhotoLibrary];

    [self doneTyping];
    [self Actionperform];

    
}
-(void)Actionperform{
    
    UIActionSheet *popup = [[UIActionSheet alloc] initWithTitle:@"Select Option:" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:
                            @"Select Existing",
                            @"Take Photo", nil];
    popup.tag = 1;
    
    [popup showInView:[UIApplication sharedApplication].keyWindow];
    
}

- (void)actionSheet:(UIActionSheet *)popup didDismissWithButtonIndex:(NSInteger)buttonIndex {
    
    if (popup.tag == 1) {
        
        if (buttonIndex == 0) {
            
            if (IS_OS_8_OR_LATER) {
                
                PHAuthorizationStatus status = [PHPhotoLibrary authorizationStatus];
                
                if (status == PHAuthorizationStatusAuthorized) {
                    
                    // Access has been granted.
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        [self openPhotos :(int)buttonIndex];
                        
                    });
                    
                }else if (status == PHAuthorizationStatusDenied) {
                    
                    // Access has been denied.
                    [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
                        
                        if (status == PHAuthorizationStatusAuthorized) {
                            
                            // Access has been granted.
                            dispatch_async(dispatch_get_main_queue(), ^{
                                
                                [self openPhotos :(int)buttonIndex];
                                
                            });
                            
                            
                        }else {
                            
                            // Access has been denied.
                            dispatch_async(dispatch_get_main_queue(), ^{
                                
                                [self photosDenied];
                                
                            });
                        }
                    }];
                    
                }else if (status == PHAuthorizationStatusNotDetermined) {
                    
                    // Access has not been determined.
                    [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
                        
                        if (status == PHAuthorizationStatusAuthorized) {
                            
                            // Access has been granted.
                            dispatch_async(dispatch_get_main_queue(), ^{
                                
                                [self openPhotos :(int)buttonIndex];
                                
                            });
                            
                        }else {
                            // Access has been denied.
                            dispatch_async(dispatch_get_main_queue(), ^{
                                
                                [self photosDenied];
                                
                            });
                        }
                    }];
                    
                }else if (status == PHAuthorizationStatusRestricted) {
                    
                    // Restricted access - normally won't happen.
                    
                }
                
            }else{
                
                ALAuthorizationStatus statusAsset = [ALAssetsLibrary authorizationStatus];
                switch (statusAsset) {
                    case ALAuthorizationStatusNotDetermined: {
                        // not determined
                        
                        ALAssetsLibrary *lib = [[ALAssetsLibrary alloc] init];
                        [lib enumerateGroupsWithTypes:ALAssetsGroupSavedPhotos usingBlock:^(ALAssetsGroup *group, BOOL *stop) {
                            
                            // Access has been granted.
                            dispatch_async(dispatch_get_main_queue(), ^{
                                
                                [self openPhotos :(int)buttonIndex];
                                
                            });
                            
                        } failureBlock:^(NSError *error) {
                            if (error.code == ALAssetsLibraryAccessUserDeniedError) {
                                NSLog(@"user denied access, code: %zd", error.code);
                            } else {
                                NSLog(@"Other error code: %zd", error.code);
                            }
                            
                            // Access has been denied.
                            dispatch_async(dispatch_get_main_queue(), ^{
                                
                                [self photosDenied];
                                
                            });
                            
                        }];
                        
                        break;
                    }
                    case ALAuthorizationStatusRestricted: {
                        // restricted
                        break;
                    }
                    case ALAuthorizationStatusDenied: {
                        
                        // denied
                        
                        ALAssetsLibrary *lib = [[ALAssetsLibrary alloc] init];
                        [lib enumerateGroupsWithTypes:ALAssetsGroupSavedPhotos usingBlock:^(ALAssetsGroup *group, BOOL *stop) {
                            // Access has been granted.
                            dispatch_async(dispatch_get_main_queue(), ^{
                                
                                [self openPhotos :(int)buttonIndex];
                                
                            });
                            
                        } failureBlock:^(NSError *error) {
                            if (error.code == ALAssetsLibraryAccessUserDeniedError) {
                                NSLog(@"user denied access, code: %zd", error.code);
                            } else {
                                NSLog(@"Other error code: %zd", error.code);
                            }
                            
                            // Access has been denied.
                            dispatch_async(dispatch_get_main_queue(), ^{
                                
                                [self photosDenied];
                                
                            });
                            
                        }];
                        
                        break;
                    }
                    case ALAuthorizationStatusAuthorized: {
                        // authorized
                        
                        // Access has been granted.
                        dispatch_async(dispatch_get_main_queue(), ^{
                            
                            [self openPhotos :(int)buttonIndex];
                            
                        });
                        
                        break;
                    }
                    default: {
                        break;
                    }
                }
                
            }
            
            
        }else if(buttonIndex == 1){
            
            // For check camera availability
            if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
                
                AVAuthorizationStatus status = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
                
                if(status == AVAuthorizationStatusAuthorized) { // authorized
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        [self openPhotos:(int)buttonIndex];
                        
                    });
                }
                else if(status == AVAuthorizationStatusDenied){ // denied
                    
                    [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
                        
                        if(granted){ // Access has been granted ..do something
                            
                            dispatch_async(dispatch_get_main_queue(), ^{
                                
                                [self openPhotos:(int)buttonIndex];
                                
                            });
                            
                            
                        } else { // Access denied ..do something
                            
                            dispatch_async(dispatch_get_main_queue(), ^{
                                
                                [self camDenied];
                                
                            });
                            
                        }
                    }];
                    
                }
                else if(status == AVAuthorizationStatusRestricted){ // restricted
                    
                    [[GlobalFunction shared] showAlertForMessage:@"Camera uses in application is restricted, please check and try again."];
                    
                }
                else if(status == AVAuthorizationStatusNotDetermined){ // not determined
                    
                    [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
                        
                        if(granted){ // Access has been granted ..do something
                            
                            dispatch_async(dispatch_get_main_queue(), ^{
                                
                                [self openPhotos:(int)buttonIndex];
                                
                            });
                            
                        } else { // Access denied ..do something
                            
                            dispatch_async(dispatch_get_main_queue(), ^{
                                
                                [self camDenied];
                                
                            });
                            
                        }
                    }];
                }
                
            }else{
                
                [[GlobalFunction shared] showAlertForMessage:@"There is no camera available on device."];
                
            }
            
        }
        
    }
    
}

- (IBAction)dateTimeBtnClicked:(id)sender {
    
  //  picker.userInteractionEnabled=YES;

    dateBtnClicked = true;
    
    if(isPremiumSelected==NO)
    {
        
    if(anyDateBtnClicked==NO)
    {
         picker.userInteractionEnabled=NO;
        _scrollViewHolder.scrollEnabled=NO;
         dateBtnClicked = false;
        
    }
    
    else
        
        
    {
        picker.userInteractionEnabled=YES;
        _scrollViewHolder.scrollEnabled=YES;
         dateBtnClicked = true;

    }
    }
    
    
    
    [timePicker removeFromSuperview];
    [self.view endEditing:YES];
//    [txtActiveField resignFirstResponder];
    
    if(toolBar.hidden==NO)
    {
        toolBar.hidden=YES;
    }
    
    
    CGRect secondFrame = self.viewInsideHolder.frame;
    
   // secondFrame.origin.y = -100+self.scrollViewHolder.contentOffset.y;
    secondFrame.origin.y = -_txtFld2.frame.origin.y+self.scrollViewHolder.contentOffset.y;

    [UIView animateWithDuration:0.3 animations:^{
        self.viewInsideHolder.frame = secondFrame;
    } completion:^(BOOL finished) {
        
    }];


    

    if(IS_IPHONE5 || IS_IPHONE_4_OR_LESS)
    {
        toolBar= [[UIToolbar alloc] initWithFrame:CGRectMake(0,257,320,44)];
    }
    else if (IS_IPHONE_6)
    {
        toolBar= [[UIToolbar alloc] initWithFrame:CGRectMake(0,370,380,44)];
    }
    else if (IS_IPHONE_6P)
    {
        toolBar= [[UIToolbar alloc] initWithFrame:CGRectMake(0,370,420,44)];
    }
    
    
    NSMutableArray *barItemsArray = [[NSMutableArray alloc] init];
    UIBarButtonItem *barButtonDone = [[UIBarButtonItem alloc] initWithTitle:@"Done"
                                                                      style:UIBarButtonItemStyleBordered target:self action:@selector(changeDateFromLabel:)];
    
    [barItemsArray addObject:barButtonDone];
    
    toolBar.backgroundColor=[UIColor colorWithRed:63.0/255 green:67.0/255 blue:70.0/255 alpha:1.0];
    barButtonDone.tintColor=[UIColor colorWithRed:63.0/255 green:67.0/255 blue:70.0/255 alpha:1.0];
    
    if(isDiscountSelected==YES){
        
        
        picker.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        picker.datePickerMode = UIDatePickerModeDate;
        picker.timeZone = [NSTimeZone systemTimeZone];
        [picker setDate:[NSDate date]];

        UIBarButtonItem *flex = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
        
        
        [barItemsArray addObject:flex];
        
//        
//        checkBoxBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//        [checkBoxBtn setFrame:CGRectMake(0, 0, 140, 44)];
//        [checkBoxBtn setTitle:@"Any Date" forState:UIControlStateNormal];
//        [checkBoxBtn.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:18]];
//        checkBoxBtn.tintColor=[UIColor blackColor];
//        
//        
//        [checkBoxBtn setImage:[UIImage imageNamed:@"Unchecked"] forState:UIControlStateNormal];
//        
//        [checkBoxBtn setTitleEdgeInsets:UIEdgeInsetsMake(2,0,0,-25)];  //top,left,bottom,right
//        [checkBoxBtn setImageEdgeInsets:UIEdgeInsetsMake(2, 0, 0, 80)];
        //  [checkBoxBtn setImage:[UIImage imageNamed:@"checkMark-1"] forState:UIControlStateNormal];
        [checkBoxBtn addTarget:self action:@selector(butImgPressed:) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *checkBoxBtnItem = [[UIBarButtonItem alloc] initWithCustomView:checkBoxBtn];
        [barItemsArray addObject:checkBoxBtnItem];
        checkBoxBtnItem.tintColor=[UIColor blackColor];
        

    }
    
    else{
        
        picker.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        picker.datePickerMode = UIDatePickerModeDateAndTime;
        picker.timeZone = [NSTimeZone systemTimeZone];
        NSLocale *locale = [[NSLocale alloc]initWithLocaleIdentifier:@"en_US"];
        [picker setLocale:locale];
        [picker setDate:[NSDate date]];

        
        
    }
    
    
    
    [toolBar setItems:barItemsArray animated:YES];
    [self.view addSubview:toolBar];
    
    
    [picker addTarget:self action:@selector(dueDateChanged:) forControlEvents:UIControlEventValueChanged];
    CGSize pickerSize = [picker sizeThatFits:CGSizeZero];
    
    if(IS_IPHONE5 || IS_IPHONE_4_OR_LESS)
    {
    picker.frame = CGRectMake(0.0, 300, pickerSize.width, 200);
    }
    
    else if (IS_IPHONE_6){
        picker.frame = CGRectMake(0.0, 410, 380, 200);

    }
    else if (IS_IPHONE_6P){
        picker.frame = CGRectMake(0.0, 410, 420, 250);
        
    }
    
    picker.backgroundColor = [UIColor colorWithRed:63.0/255.0f green:67.0/255.0f blue:70.0/255.0f alpha:1.0];
    
   // [UIColor colorWithRed:63.0/255.0f green:67.0/255.0f blue:70.0/255.0f alpha:1.0];
    [picker setValue:[UIColor whiteColor] forKey:@"textColor"];

    
    [self.view addSubview:picker];

    
}

-(void)changeDateFromLabel:(id)sender
{
    
    NSLog(@"done button click");
    [picker removeFromSuperview];
    [timePicker removeFromSuperview];
    
    toolBar.hidden=YES;
    [self doneTyping];
}

-(void)butImgPressed:(id)sender
{
    
    dateBtnClicked = false;
    
    NSLog(@"button any date pressed...");

    if(anyDateBtnClicked==YES)
    {
    [checkBoxBtn setImage:[UIImage imageNamed:@"Checked"] forState:UIControlStateNormal];
       // picker.userInteractionEnabled=NO;
      //  [_dateTimeBtn setTitle:@"Any Date" forState:UIControlStateNormal];
        NSMutableAttributedString *dateString1 = [[NSMutableAttributedString alloc] initWithString:@"Any Date"];

        [_dateTimeBtn setAttributedTitle:dateString1 forState:UIControlStateNormal];
        
        [self changeDateFromLabel:nil];
        startDate = @"";
        startDate = @"1";
        anyDateBtnClicked=NO;
        

    }
    else if(anyDateBtnClicked==NO){
        [checkBoxBtn setImage:[UIImage imageNamed:@"Unchecked"] forState:UIControlStateNormal];
        picker.userInteractionEnabled=YES;
        _scrollViewHolder.scrollEnabled=YES;
        
        NSAttributedString *star = [[NSAttributedString alloc] initWithString:@"*" attributes:@{ NSForegroundColorAttributeName :[UIColor redColor],
                                                                                                 }];
        NSMutableAttributedString *str2 = [[NSMutableAttributedString alloc] initWithString:@"Date"];
        
        [str2 appendAttributedString:star];
       
       // [_dateTimeBtn setTitle:@"Date*" forState:UIControlStateNormal];
        [_dateTimeBtn setAttributedTitle:str2 forState:UIControlStateNormal];
        
        startDate = @"";
        anyDateBtnClicked=YES;
        dateBtnClicked = true;

    }
}


-(void)butAnyTimePressed:(id)sender
{
    
    timeBtnClicked = false;
    
    NSLog(@"button any time pressed...");
    
    if(anyTimeBtnClicked==YES)
    {
        [timeCheckBoxBtn setImage:[UIImage imageNamed:@"Checked"] forState:UIControlStateNormal];
        NSMutableAttributedString *dateString1 = [[NSMutableAttributedString alloc] initWithString:@"Any Time"];
        [_timeBtn setAttributedTitle:dateString1 forState:UIControlStateNormal];
        //[_timeBtn setTitle:@"Any Time" forState:UIControlStateNormal];
        startTime = @"";
        startTime = @"1";
        [self changeDateFromLabel:nil];

        anyTimeBtnClicked=NO;
        
    }
    else if(anyTimeBtnClicked==NO){
        [timeCheckBoxBtn setImage:[UIImage imageNamed:@"Unchecked"] forState:UIControlStateNormal];
        timePicker.userInteractionEnabled=YES;
        _scrollViewHolder.scrollEnabled=YES;

        NSAttributedString *star = [[NSAttributedString alloc] initWithString:@"*" attributes:@{ NSForegroundColorAttributeName :[UIColor redColor],
                                                                                                 }];
        NSMutableAttributedString *str2 = [[NSMutableAttributedString alloc] initWithString:@"Time"];
        
        [str2 appendAttributedString:star];
        
         [_timeBtn setAttributedTitle:str2 forState:UIControlStateNormal];
      //  [_timeBtn setTitle:@"Time*" forState:UIControlStateNormal];

        anyTimeBtnClicked=YES;
        startTime = @"";
        timeBtnClicked = true;
    }
}




-(void) dueTimeChanged:(UIDatePicker *)sender
{
    NSLog(@"time change");
    
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:dateFormatFull];
    
    NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
    [outputFormatter setDateFormat:dateFormatOnlyHour];
    
    NSString *timetofill;
    timetofill = [outputFormatter stringFromDate:timePicker.date];
    
    NSDateFormatter *formatter000 = [[NSDateFormatter alloc] init];
    [formatter000 setDateFormat:@"hh:mm a"];
    NSLocale *locale = [[NSLocale alloc]initWithLocaleIdentifier:@"en_US"];
    [formatter000 setLocale:locale];
   NSString *timetoshow = [formatter000 stringFromDate:timePicker.date];
    NSLog(@"Current Date: %@", [formatter000 stringFromDate:timePicker.date]);
    
    
    startTime = @"";
    startTime = timetofill;
    NSMutableAttributedString *dateString1 = [[NSMutableAttributedString alloc] initWithString:timetoshow];

    //[_timeBtn setTitle: timetoshow forState: UIControlStateNormal];
     [_timeBtn setAttributedTitle:dateString1 forState: UIControlStateNormal];
    
}








-(void) dueDateChanged:(UIDatePicker *)sender {
    
    
    if(isDiscountSelected==YES)
    {
    
     NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
     [dateFormat setDateFormat:dateFormatFull];
//    
//    NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
//    [outputFormatter setDateFormat:dateFormatOnlyHour];
       dateStart = picker.date;
       NSString *datetofill = [dateFormat stringFromDate:picker.date];
        startDate = @"";
       startDate = datetofill;
        
        NSDateFormatter *dtFormatMDY = [[NSDateFormatter alloc] init];
        [dtFormatMDY setDateFormat:dateFormatMDY];
        
        NSString *dateString = [dtFormatMDY  stringFromDate:picker.date];
  
        
        NSMutableAttributedString *dateString1 = [[NSMutableAttributedString alloc] initWithString:dateString];
        
     
       //[_dateTimeBtn setTitle: dateString forState: UIControlStateNormal];
        [_dateTimeBtn setAttributedTitle: dateString1 forState: UIControlStateNormal];
        
        
        
    }
    
    
    else{
        
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:dateFormatFull];
        
        NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
        [outputFormatter setDateFormat:dateFormatOnlyHour];
        
        NSString *timetofill = [outputFormatter stringFromDate:picker.date];
        
         dateStart = picker.date;
         NSString *dateVal = [dateFormat stringFromDate:picker.date];
        
        NSDateFormatter *dtFormatMDY = [[NSDateFormatter alloc] init];
        [dtFormatMDY setDateFormat:dateFormatMDY];
        
         NSString *dateString = [dtFormatMDY  stringFromDate:picker.date];
        

//        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
//        dateFormatter.dateFormat = @"HH:mm:ss";
//        NSDate *date = [dateFormatter dateFromString:timetofill];
//        
//        dateFormatter.dateFormat = @"hh:mm a";
//        NSString *pmamDateString = [dateFormatter stringFromDate:date];
        
        
        
        NSDateFormatter *formatter00 = [[NSDateFormatter alloc] init];
        NSLocale *locale = [[NSLocale alloc]initWithLocaleIdentifier:@"en_US"];
        [formatter00 setLocale:locale];
        [formatter00 setDateFormat:@"hh:mm a"];
        NSString *timetoShow = [formatter00 stringFromDate:picker.date];
       // NSMutableAttributedString *timetoShow = [[NSMutableAttributedString alloc] initWithString:[formatter00 stringFromDate:picker.date];
        NSLog(@"Current Date: %@", [formatter00 stringFromDate:picker.date]);
        
        dateString = [[dateString stringByAppendingString:@"  "] stringByAppendingString:timetoShow];
        
        NSMutableAttributedString *dateString1 = [[NSMutableAttributedString alloc] initWithString:dateString];

        
       // [_dateTimeBtn setTitle: dateString1 forState: UIControlStateNormal];
         [_dateTimeBtn setAttributedTitle: dateString1 forState: UIControlStateNormal];
        
        startDate = @"";
        startTime = @"";
        startDate=dateVal;
        startTime=timetofill;

    }

    
}


- (IBAction)wantCashBtnClicked:(id)sender {
    
    paymentMethod = @"0";
    
    wantCashBtnClciked=YES;
    if(wantCashBtnClciked==YES)
    {
        
        [self.wantCashBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _wantCashBtn.backgroundColor=[UIColor colorWithRed:63.0/255.0f green:67.0/255.0f blue:70.0/255.0f alpha:1.0];
        
        [self.creditFineBtn setTitleColor:[UIColor colorWithRed:63.0/255.0f green:67.0/255.0f blue:70.0/255.0f alpha:1.0] forState:UIControlStateNormal];
        _creditFineBtn.backgroundColor=[UIColor colorWithRed:244/255.0f green:244/255.0f blue:244/255.0f alpha:1.0] ;
        
        wantCashBtnClciked=NO;
    }
    
}

- (IBAction)creditFineBtnClciked:(id)sender {
    
    paymentMethod = @"1";
    
    creditFineBtnClciked=YES;
    if(creditFineBtnClciked==YES)
    {
        [self.creditFineBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _creditFineBtn.backgroundColor=[UIColor colorWithRed:63.0/255.0f green:67.0/255.0f blue:70.0/255.0f alpha:1.0];
        
        [self.wantCashBtn setTitleColor:[UIColor colorWithRed:63.0/255.0f green:67.0/255.0f blue:70.0/255.0f alpha:1.0] forState:UIControlStateNormal];
        _wantCashBtn.backgroundColor=[UIColor colorWithRed:244/255.0f green:244/255.0f blue:244/255.0f alpha:1.0] ;
        
        
        creditFineBtnClciked=NO;
        
    }
}

- (IBAction)helpMeDecideBtnClciked:(id)sender
{
    //helpMeDecideBtn
   // whichButtonClicked = @"helpme";
    
    [self doneTyping];
    
    
    if ([whichButtonClicked isEqualToString:@"title"])
    {
        self.lblOnBlurView.text = @" Provide an attractive headline for the appointment you'd like to post.";
             // old one..  Provide an attractive headline for the reservation you'd like to post.
        whichButtonClicked = @"";
    }
    else if ([whichButtonClicked isEqualToString:@"discription"])
    {
        self.lblOnBlurView.text = @" Provide details regarding the appointment you'd like to post.";
            // old one.. Provide details regarding the reservation you'd like to post.
        whichButtonClicked = @"";
    }
    else if ([whichButtonClicked isEqualToString:@"location"])
    {
        self.lblOnBlurView.text = @"Please add complete address in following sequence: Street, Unit, City, State and Zip.";
        whichButtonClicked = @"";
    }
    else
    {
        self.lblOnBlurView.text = @"When choosing the cash button you'll recieve ~ 80% of the amount you posted for sale. Choosing the credit option you'll recieve ~ 100% of the value with the service provider.";
    }
    
    
    
    [UIView transitionWithView:self.view
                      duration:0.4
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^{
                        _blurView.alpha=1.0;
                        _blurView.hidden=NO;
                        _alertView.hidden=NO;
                        helpMeBtnClciked=YES;

                    }
                    completion:NULL];
    
 }
- (IBAction)okBtnClciked:(id)sender {
    
//    [UIView animateWithDuration:0.4 animations:^{
//        _blurView.hidden=YES;
//    } completion: ^(BOOL finished) {//creates a variable (BOOL) called "finished" that is set to *YES* when animation IS completed.
//        
//        _alertView.hidden=YES;//if animation is finished ("finished" == *YES*), then hidden = "finished" ... (aka hidden = *YES*)
//    }];
//
    
    
    [UIView animateWithDuration:0.5
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                          _blurView.hidden=YES;
                          _alertView.hidden=YES;
                         
                     } completion:^(BOOL finished) {
                         
                         
                     }];

    
   
}

- (IBAction)timeBtnClciked:(id)sender {
    NSLog(@"time press");
    
    timeBtnClicked = true;
  
    
    
    if(anyTimeBtnClicked==NO)
    {
        timePicker.userInteractionEnabled=NO;
        _scrollViewHolder.scrollEnabled=NO;
          timeBtnClicked = false;
        
    }
    
    else
        
        
    {
        timePicker.userInteractionEnabled=YES;
        _scrollViewHolder.scrollEnabled=YES;
          timeBtnClicked = true;
        
    }

    
    
    
    [txtActiveField resignFirstResponder];
    [timePicker removeFromSuperview];

    
    if(toolBar.hidden==NO)
    {
        toolBar.hidden=YES;
    }
    
    
    CGRect secondFrame = self.viewInsideHolder.frame;
    
    secondFrame.origin.y = -100+self.scrollViewHolder.contentOffset.y;
    [UIView animateWithDuration:0.3 animations:^{
        self.viewInsideHolder.frame = secondFrame;
    } completion:^(BOOL finished) {
        
    }];
    
    timePicker.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    timePicker.datePickerMode = UIDatePickerModeTime;
    timePicker.timeZone = [NSTimeZone systemTimeZone];
    NSLocale *locale = [[NSLocale alloc]initWithLocaleIdentifier:@"en_US"];
    [timePicker setLocale:locale];
    [timePicker setDate:[NSDate date]];
    
    
    
    if(IS_IPHONE5 || IS_IPHONE_4_OR_LESS)
    {
        toolBar= [[UIToolbar alloc] initWithFrame:CGRectMake(0,257,320,44)];
    }
    else if (IS_IPHONE_6)
    {
        toolBar= [[UIToolbar alloc] initWithFrame:CGRectMake(0,370,380,44)];
    }
    else if (IS_IPHONE_6P)
    {
        toolBar= [[UIToolbar alloc] initWithFrame:CGRectMake(0,370,420,44)];
    }
    
    
    NSMutableArray *barItemsArray = [[NSMutableArray alloc] init];
    UIBarButtonItem *barButtonDone = [[UIBarButtonItem alloc] initWithTitle:@"Done"
                                                                      style:UIBarButtonItemStyleBordered target:self action:@selector(changeDateFromLabel:)];
    
    [barItemsArray addObject:barButtonDone];
    
    toolBar.backgroundColor=[UIColor colorWithRed:63.0/255 green:67.0/255 blue:70.0/255 alpha:1.0];
    barButtonDone.tintColor=[UIColor colorWithRed:63.0/255 green:67.0/255 blue:70.0/255 alpha:1.0];
    
    
    
    UIBarButtonItem *flex = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    
    
    [barItemsArray addObject:flex];
    
    
//    checkBoxBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//    [checkBoxBtn setFrame:CGRectMake(0, 0, 140, 44)];
//    [checkBoxBtn setTitle:@"Any Time" forState:UIControlStateNormal];
//    [checkBoxBtn.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:18]];
//    checkBoxBtn.tintColor=[UIColor blackColor];
//    
//    
//    //[checkBoxBtn setImage:[UIImage imageNamed:@"Unchecked"] forState:UIControlStateNormal];
//
//    [checkBoxBtn setTitleEdgeInsets:UIEdgeInsetsMake(2,0,0,-25)];  //top,left,bottom,right
//    [checkBoxBtn setImageEdgeInsets:UIEdgeInsetsMake(5, 0, 0,80)];
    //  [checkBoxBtn setImage:[UIImage imageNamed:@"checkMark-1"] forState:UIControlStateNormal];
    
    [timeCheckBoxBtn addTarget:self action:@selector(butAnyTimePressed:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *checkBoxBtnItem = [[UIBarButtonItem alloc] initWithCustomView:timeCheckBoxBtn];
    [barItemsArray addObject:checkBoxBtnItem];
    checkBoxBtnItem.tintColor=[UIColor colorWithRed:63.0/255 green:67.0/255 blue:70.0/255 alpha:1.0];;
    
    
    [toolBar setItems:barItemsArray animated:YES];
    [self.view addSubview:toolBar];
    
    
    [timePicker addTarget:self action:@selector(dueTimeChanged:) forControlEvents:UIControlEventValueChanged];
    CGSize pickerSize = [timePicker sizeThatFits:CGSizeZero];
    
    if(IS_IPHONE5 || IS_IPHONE_4_OR_LESS)
    {
        timePicker.frame = CGRectMake(0.0, 300, pickerSize.width, 200);
    }
    
    else if (IS_IPHONE_6){
        timePicker.frame = CGRectMake(0.0, 410, 380, 200);
        
    }
    else if (IS_IPHONE_6P){
        timePicker.frame = CGRectMake(0.0, 410, 420, 250);
        
    }
    
    timePicker.backgroundColor = [UIColor colorWithRed:63.0/255.0f green:67.0/255.0f blue:70.0/255.0f alpha:1.0];
    
    // [UIColor colorWithRed:63.0/255.0f green:67.0/255.0f blue:70.0/255.0f alpha:1.0];
    [timePicker setValue:[UIColor whiteColor] forKey:@"textColor"];
    
    
    [self.view addSubview:timePicker];
    

    
}
- (IBAction)premiumBtnClicked:(id)sender {
    
    _txtFld2.enabled = YES;
    if(reservationChangePossible == NO)
    {
        
        UIAlertView *alertRes = [[UIAlertView alloc] initWithTitle:@"BUY TIMEE" message:@"Sorry, you cannot change this discount type to appointment type listing." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        // old one.. Sorry, you cannot change this discount type to reservation type listing.
        
        [alertRes show];
        return;
        
    }
    
    
    isPremiumSelected=YES;
    isDiscountSelected=NO;

    premiumBtnClciked=YES;
    if(premiumBtnClciked==YES)
        
        [self changeDateFromLabel:nil];
        
        
    {
        
        [self.premiumBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _premiumBtn.backgroundColor=[UIColor colorWithRed:63.0/255.0f green:67.0/255.0f blue:70.0/255.0f alpha:1.0];
        
        [self.discountBtn setTitleColor:[UIColor colorWithRed:63.0/255.0f green:67.0/255.0f blue:70.0/255.0f alpha:1.0] forState:UIControlStateNormal];
        _discountBtn.backgroundColor=[UIColor colorWithRed:244/255.0f green:244/255.0f blue:244/255.0f alpha:1.0] ;
        
        premiumBtnClciked=NO;
    }

    
//    [UIView transitionWithView:_wantCashBtn
//                      duration:0.4
//                       options:UIViewAnimationOptionTransitionCrossDissolve
//                    animations:^{
//                        _wantCashBtn.hidden=NO;
//                    }
//                    completion:NULL];
//    [UIView transitionWithView:_creditFineBtn
//                      duration:0.4
//                       options:UIViewAnimationOptionTransitionCrossDissolve
//                    animations:^{
//                        _creditFineBtn.hidden=NO;
//                    }
//                    completion:NULL];
//    [UIView transitionWithView:_helpMeDecideBtn
//                      duration:0.4
//                       options:UIViewAnimationOptionTransitionCrossDissolve
//                    animations:^{
//                        _helpMeDecideBtn.hidden=NO;
//                    }
//                    completion:NULL];
    
    NSAttributedString *star = [[NSAttributedString alloc] initWithString:@"*" attributes:@{ NSForegroundColorAttributeName :[UIColor redColor],
                                                                                             }];
    
    
    NSMutableAttributedString *strQuantityTitle = [[NSMutableAttributedString alloc] initWithString:@"Price" attributes:@{ NSForegroundColorAttributeName : [UIColor colorWithRed:143.0/255.0f green:143.0/255.0f blue:143.0/255.0f alpha:1.0],
                                                                                                                }];
    [strQuantityTitle appendAttributedString:star];
    self.txtFld6.attributedPlaceholder = strQuantityTitle;
    
//    NSAttributedString *strDiscountTitle = [[NSAttributedString alloc] initWithString:@"Quantity" attributes:@{ NSForegroundColorAttributeName : [UIColor colorWithRed:143.0/255.0f green:143.0/255.0f blue:143.0/255.0f alpha:1.0],
//                                                                                                                }];
//    self.quantityTxtFld.attributedPlaceholder = strDiscountTitle;
//    
    
    
    [UIView transitionWithView:_helpMeDecideBtn
                      duration:0.4
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^{
                        _quantityTxtFld.alpha=0.0;
                        _quantityLineLbl.alpha=0.0;
                        
                        _discountTxtFld.alpha=0.0;
                        _discountLinelbl.alpha=0.0;
                        
                       // _termsAndConditionTxtFld.alpha=0.0;
                        
                        _termsAndConditionTextView.alpha=0.0;
                        
                        _timeBtn.alpha=0.0;
                        _timeLineLbl.alpha=0.0;
                      
                        NSMutableAttributedString *DateTimePlaceHolder = [[NSMutableAttributedString alloc] initWithString:@"Date/Time" attributes:@{ NSForegroundColorAttributeName : [UIColor colorWithRed:63.0/255.0f green:67.0/255.0f blue:70.0/255.0f alpha:1.0],
                                                                                                                                             }];
                        [DateTimePlaceHolder appendAttributedString:star];
                        
                        [_dateTimeBtn setAttributedTitle:DateTimePlaceHolder forState:UIControlStateNormal];
                        startDate = @"";
                        startTime = @"";
                        
                        [self.view layoutIfNeeded];
                        
                        if(IS_IPHONE_6)
                        {
                            _priceTxtFldWidth.constant=275;
                            _priceLineWidth.constant=280;
                            
                            _locationLineWidth.constant=340;
                            _locationTxtFldWidth.constant=307;
                            
                            _termsTxtFldHeight.constant=0;
                            _termsLblHeight.constant=0;
                            
//                            [_dateTimeBtn setImageEdgeInsets:UIEdgeInsetsMake(0,310, 0, 0)];
//                            _dateAndTimeBtnWidth.constant=330;
//                            _dateAndTimeLineWidth.constant=340;

                            _dateAndTimeBtnWidth.constant=330;
                            _dateAndTimeLineWidth.constant=340;
                            [_dateTimeBtn setImageEdgeInsets:UIEdgeInsetsMake(0,320, 0, 0)];

                            
                        }
                        
                        else if (IS_IPHONE_6P)
                        {
                            _priceTxtFldWidth.constant=300;
                            _priceLineWidth.constant=310;
                            [_dateTimeBtn setImageEdgeInsets:UIEdgeInsetsMake(0, 353, 0, 0)];
                            _dateAndTimeBtnWidth.constant=365;
                            _dateAndTimeLineWidth.constant=375;
                            
                            _locationLineWidth.constant=375;
                            _locationTxtFldWidth.constant=343;
                            
                            _termsTxtFldHeight.constant=0;
                            _termsLblHeight.constant=0;
                            
                            
                        }
                        
                        else if(IS_IPHONE_5 || IS_IPHONE_4_OR_LESS)
                        {
                            
                            _termsTxtFldHeight.constant=0;
                            _termsLblHeight.constant=0;
                            
                            _priceTxtFldWidth.constant=235;
                            _priceLineWidth.constant=240;
                            
                            _locationLineWidth.constant=286;
                            _locationTxtFldWidth.constant=262;
                            
                            [_dateTimeBtn setImageEdgeInsets:UIEdgeInsetsMake(0,268, 0, 0)];
                            _dateAndTimeBtnWidth.constant=300;
                            _dateAndTimeLineWidth.constant=286;
                            
                            
                            
                        }
                        
                        
                        [self.view layoutIfNeeded];
                        
                    }
                    completion:NULL];
    
    
    /*
    
    if(_txtViewActiveField==_descriptionTextView)
    {
        
        
        if([_descriptionTextView.text isEqual:@"Description"])
        {
            if(IS_IPHONE5)
            {
                _viewMainHolderHeight.constant=450;
            }
            else if (IS_IPHONE_6)
            {
                _viewMainHolderHeight.constant=500;
            }
            else if (IS_IPHONE_6P)
            {
                _viewMainHolderHeight.constant=550;
            }
            
        }
        
        else
        {
            if(IS_IPHONE_6)
            {
                _viewMainHolderHeight.constant=500+_termsTxtFldHeight.constant+_descriptionTxtFldHeight.constant;
                _viewInsideHolderHeight.constant=500+_termsTxtFldHeight.constant+_descriptionTxtFldHeight.constant;
            }
            
            if(IS_IPHONE_5)
            {
                _viewMainHolderHeight.constant=450+_termsTxtFldHeight.constant+_descriptionTxtFldHeight.constant;
                _viewInsideHolderHeight.constant=450+_termsTxtFldHeight.constant+_descriptionTxtFldHeight.constant;
            }
            
            if(IS_IPHONE_6P)
            {
                _viewMainHolderHeight.constant=550+_termsTxtFldHeight.constant+_descriptionTxtFldHeight.constant;
                _viewInsideHolderHeight.constant=550+_termsTxtFldHeight.constant+_descriptionTxtFldHeight.constant;
            }
        }
    }
    
    else
    {
        
        if([_termsAndConditionTextView.text isEqual:@"Terms and conditions"])
        {
            if(IS_IPHONE5)
            {
                _viewMainHolderHeight.constant=450;
            }
            else if (IS_IPHONE_6)
            {
                _viewMainHolderHeight.constant=500;
            }
            else if (IS_IPHONE_6P)
            {
                _viewMainHolderHeight.constant=550;
            }
            
        }
        
        else
        {
            if(IS_IPHONE_6)
            {
                _viewMainHolderHeight.constant=500+_termsTxtFldHeight.constant+_descriptionTxtFldHeight.constant;
                _viewInsideHolderHeight.constant=500+_termsTxtFldHeight.constant+_descriptionTxtFldHeight.constant;
            }
            
            if(IS_IPHONE_5)
            {
                _viewMainHolderHeight.constant=450+_termsTxtFldHeight.constant+_descriptionTxtFldHeight.constant;
                _viewInsideHolderHeight.constant=450+_termsTxtFldHeight.constant+_descriptionTxtFldHeight.constant;
            }
            
            if(IS_IPHONE_6P)
            {
                _viewMainHolderHeight.constant=550+_termsTxtFldHeight.constant+_descriptionTxtFldHeight.constant;
                _viewInsideHolderHeight.constant=550+_termsTxtFldHeight.constant+_descriptionTxtFldHeight.constant;
            }
        }
    }

*/
    
}


-(BusinessInfo*)businessNameBybusinessID:(NSString*)businessID
{
    for (BusinessInfo * bus in myAppDelegate.arrayBusinessDetail) {
        NSString *busID = [EncryptDecryptFile decrypt:bus.BusinessId];
        if ([busID isEqualToString:businessID]) {
            return bus;
        }
        
    }
    return nil;
}

-(NSString*)categoryNameByCategoryID:(NSString*)categoryID
{
    for (category * cat in catArr) {
        if ([cat.categoryId isEqualToString:categoryID]) {
            return cat.name;
        }
        
    }
    return nil;
}

-(category*)categoryByCategoryID:(NSString*)categoryID
{
    for (category * cat in catArr) {
        if ([cat.categoryId isEqualToString:categoryID]) {
            return cat;
        }
        
    }
    return nil;
}




- (IBAction)discountBtnClicked:(id)sender
{
   

    
    if(reservationChangeDIscount == NO)
    {
        
        UIAlertView *alertRes = [[UIAlertView alloc] initWithTitle:@"BUY TIMEE" message:@"Sorry, you cannot change this appointment type to discount type listing." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
       // old one.. Sorry, you cannot change this reservation type to discount type listing.
        
        [alertRes show];
        return;
        
    }

    if([myAppDelegate.selectedPaymentTypeFromServer isEqualToString:@"1"])
    {
    
    if (myAppDelegate.isPaymentFilled == false) {
        
        // [self closeSideBar:self.btnSideBarClose];
        
      //  [GlobalFunction removeIndicatorView];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"BUY TIMEE" message:@"You need to fill your credit card information first, before creating the discount." delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Go to fill credit card information", nil];
        
        alert.tag = 9999;
        
        [alert show];
        
        return;
    }
    }
    else if([myAppDelegate.selectedPaymentTypeFromServer isEqualToString:@"2"])
    {
        if (myAppDelegate.isPaymentFilled == false) {
            
            // [self closeSideBar:self.btnSideBarClose];
            
            //  [GlobalFunction removeIndicatorView];
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"BUY TIMEE" message:@"You need to add your Pay with PayPal information before creating the discount." delegate:self cancelButtonTitle:@"Later" otherButtonTitles:@"Pay with PayPal", nil];
            
            alert.tag = 1000;
            
            [alert show];
            
            return;
        }
        
    }
    
    NSLog(@"BussinessUserId is this \"%@\"",myAppDelegate.userData.BusinessUserId);
    if((!myAppDelegate.userData.BusinessUserId) || [myAppDelegate.userData.BusinessUserId isEqual:@"0"])
    {
        UIAlertView *alertRes = [[UIAlertView alloc] initWithTitle:@"BUY TIMEE" message:@"Sorry, you cannot create discount type listing. Please add Business Info in your profile." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        [alertRes show];
        return;
    }
    else
    {
    
        
        ////////////
        _txtFld2.enabled = NO;
        selectedBusinessObj = [self businessNameBybusinessID:myAppDelegate.userData.BusinessUserId];
        NSLog(@"business Name :%@",selectedBusinessObj.BusinessName);
        _txtFld2.text = [EncryptDecryptFile decrypt:selectedBusinessObj.BusinessName];
        NSLog(@"business BusinessAdd :%@",selectedBusinessObj.BusinessAdd);
        _txtFld4.text = [EncryptDecryptFile decrypt:selectedBusinessObj.BusinessAdd];

        NSLog(@"business categoryId :%@",selectedBusinessObj.categoryId);
        NSString * catID = [EncryptDecryptFile decrypt:selectedBusinessObj.categoryId];
        NSString *categoryName = [self categoryNameByCategoryID:catID];

        NSAttributedString *star = [[NSAttributedString alloc] initWithString:@"*" attributes:@{ NSForegroundColorAttributeName :[UIColor redColor],
                                                                                                 }];

        NSMutableAttributedString *catName = [[NSMutableAttributedString alloc] initWithString:@"Choose Category" attributes:@{ NSForegroundColorAttributeName :[UIColor colorWithRed:63.0/255.0f green:67.0/255.0f blue:70.0/255.0f alpha:1.0],
                                                                                                                                }];
        [catName appendAttributedString:star];
        if(!categoryName)
        {
            [_chooseBtn setAttributedTitle:catName forState:UIControlStateNormal];
        }
        else
        {
            [_chooseBtn setTitle:categoryName forState:UIControlStateNormal];
        }

        selectedCatObj = [self categoryByCategoryID:catID];

        NSLog(@"%@",[EncryptDecryptFile decrypt:selectedBusinessObj.BusinessName]);
        NSLog(@" business id ----%@",[EncryptDecryptFile decrypt:selectedBusinessObj.BusinessId]);
        NSLog(@"user id val --- %@", myAppDelegate.userData.BusinessUserId);
        
    }

    
    isDiscountSelected=YES;
    isPremiumSelected=NO;
    
    discountBtnClicked=YES;
    
    
    [self changeDateFromLabel:nil];

    if(discountBtnClicked==YES)
    {
        [self.discountBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _discountBtn.backgroundColor=[UIColor colorWithRed:63.0/255.0f green:67.0/255.0f blue:70.0/255.0f alpha:1.0];
        
        [self.premiumBtn setTitleColor:[UIColor colorWithRed:63.0/255.0f green:67.0/255.0f blue:70.0/255.0f alpha:1.0] forState:UIControlStateNormal];
        _premiumBtn.backgroundColor=[UIColor colorWithRed:244/255.0f green:244/255.0f blue:244/255.0f alpha:1.0] ;
        discountBtnClicked=NO;
        
    }
    
    
    NSAttributedString *star = [[NSAttributedString alloc] initWithString:@"*" attributes:@{ NSForegroundColorAttributeName :[UIColor redColor],
                                                                                             }];
    NSMutableAttributedString *strQuantityTitle = [[NSMutableAttributedString alloc] initWithString:@"Discount" attributes:@{ NSForegroundColorAttributeName : [UIColor colorWithRed:143.0/255.0f green:143.0/255.0f blue:143.0/255.0f alpha:1.0],
                                                                                                                }];
    [strQuantityTitle appendAttributedString:star];
    self.txtFld6.attributedPlaceholder = strQuantityTitle;
    
    NSAttributedString *strDiscountTitle = [[NSAttributedString alloc] initWithString:@"Quantity" attributes:@{ NSForegroundColorAttributeName : [UIColor colorWithRed:143.0/255.0f green:143.0/255.0f blue:143.0/255.0f alpha:1.0],
                                                                                                                }];
    self.quantityTxtFld.attributedPlaceholder = strDiscountTitle;
    
    
    
    
    
    
    
    [UIView transitionWithView:_wantCashBtn
                      duration:0.4
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^{
                        _wantCashBtn.hidden=YES;
                    }
                    completion:NULL];
    [UIView transitionWithView:_helpMeDecideBtn
                      duration:0.4
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^{
                        _helpMeDecideBtn.hidden=YES;
                    }
                    completion:NULL];
    
    [UIView transitionWithView:_creditFineBtn
                      duration:0.4
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^{
                        _creditFineBtn.hidden=YES;
                    }
                    completion:NULL];
    [UIView transitionWithView:_quantityTxtFld
                      duration:0.4
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^{
                        _quantityTxtFld.alpha=1.0;
                        _quantityLineLbl.alpha=1.0;
                        _discountTxtFld.alpha=1.0;
                        _discountLinelbl.alpha=1.0;
                       // _termsAndConditionTxtFld.alpha=1.0;
                        _termsAndConditionTextView.alpha=1.0;

                        _timeBtn.alpha=1.0;
                        _timeLineLbl.alpha=1.0;
                        NSMutableAttributedString *datePlaceHolder = [[NSMutableAttributedString alloc] initWithString:@"Date" attributes:@{ NSForegroundColorAttributeName : [UIColor colorWithRed:63.0/255.0f green:67.0/255.0f blue:70.0/255.0f alpha:1.0],
                                                                                                                                                  }];
                        [strQuantityTitle appendAttributedString:star];
                        
                        NSMutableAttributedString *timePlaceHolder = [[NSMutableAttributedString alloc] initWithString:@"Time" attributes:@{ NSForegroundColorAttributeName : [UIColor colorWithRed:63.0/255.0f green:67.0/255.0f blue:70.0/255.0f alpha:1.0],
                                                                                                                                             }];
                        [datePlaceHolder appendAttributedString:star];
                        [timePlaceHolder appendAttributedString:star];
                        
                        [_dateTimeBtn setAttributedTitle:datePlaceHolder forState:UIControlStateNormal];
                        [_timeBtn setAttributedTitle:timePlaceHolder forState:UIControlStateNormal];
                        startTime = @"";
                        startTime = @"";
                        
                        [self.view layoutIfNeeded];
                        
                        
                        if(IS_IPHONE_6)
                        {
                            
                            
                            _priceTxtFldWidth.constant=150;
                            _priceLineWidth.constant=160;
                            

                            
                        //    _priceTxtFldWidth.constant=160;
                       //     _priceLineWidth.constant=170;
                            
                            _locationTxtFldWidth.constant=307;
                            _locationLineWidth.constant=340;
                            
                            
                            
                            
                            
                            _quantityTxtFldWidth.constant=100;
                            _quantityLineLblWidth.constant=118;
                           
                            _quantityLineLblLeft.constant=195;
                            _quantityTxtFldLeft.constant=27;
                            
                          
                            
                            self.quantityLineLblTop.constant = 9;
                            
                   //         self.txtFld6.backgroundColor = [UIColor redColor];
                   //         self.quantityTxtFld.backgroundColor = [UIColor blueColor];
                            
                            
                            
                            
                            if([_termsAndConditionTextView.text isEqual:@"Terms and conditions"])
                            {
                                _termsTxtFldHeight.constant=30;
                                
     
                            }
                            
                            else
                            {
                                _termsTxtFldHeight.constant=heightOfTermsTxtView;
                                _viewMainHolderHeight.constant=530+_termsTxtFldHeight.constant+_descriptionTxtFldHeight.constant;
                                _viewInsideHolderHeight.constant=530+_termsTxtFldHeight.constant+_descriptionTxtFldHeight.constant;
                                
                                
                            }
                            
                            
                            
                            _termsLblHeight.constant=1;
                            
                            _dateAndTimeBtnWidth.constant=150;
                            _dateAndTimeLineWidth.constant=160;
                            
                            _timeBtnWidth.constant=150;
                            _timeLblWidth.constant=160;
                            _timeBtnLeft.constant=25;
                            
                            [_dateTimeBtn setImageEdgeInsets:UIEdgeInsetsMake(0,140, 0, 0)];
                            [_timeBtn setImageEdgeInsets:UIEdgeInsetsMake(0,140, 0, 0)];
                            _termsTxtFLdLeft.constant=25;
                            [self.termsAndConditionTextView setTextContainerInset:UIEdgeInsetsMake(5,-2, 0,0)];
                            
                           
                            
                            
                            
                        }
                        else if(IS_IPHONE_6P)
                        {
                            _priceTxtFldWidth.constant=160;
                            _priceLineWidth.constant=179;
                            
                            //                            _quantityTxtFldWidth.constant=120;
                            //                            _quantityLineLblWidth.constant=130;
                            //                            _quantityTxtFldHeight.constant=32;
                            //                            _quantityLineLblLeft.constant=215;
                            //                            _quantityTxtFldLeft.constant=20;
                            
                            
                            
                            _quantityTxtFldWidth.constant=125;
                            _quantityLineLblWidth.constant=136;
                            _quantityTxtFldHeight.constant=32;
                            _quantityLineLblLeft.constant=214;
                            _quantityTxtFldLeft.constant=28;
                            
                       //     self.txtFld6.backgroundColor = [UIColor redColor];
                      //      self.quantityTxtFld.backgroundColor = [UIColor blueColor];
                            
                            
                            if([_termsAndConditionTextView.text isEqual:@"Terms and conditions"])
                            {
                                _termsTxtFldHeight.constant=32;
                                
                            }
                            
                            else
                            {
                                _termsTxtFldHeight.constant=heightOfTermsTxtView;
                                _viewMainHolderHeight.constant=580+_termsTxtFldHeight.constant+_descriptionTxtFldHeight.constant;
                                _viewInsideHolderHeight.constant=580+_termsTxtFldHeight.constant+_descriptionTxtFldHeight.constant;
                                
                                
                            }
                            
                            
                            
                            _termsLblHeight.constant=1;
                            
                            _locationTxtFldWidth.constant=343;
                            _locationLineWidth.constant=375;
                            
                            _discountTxtFkdWidth.constant=175;
                            _discountLineLblWidth.constant=180;
                            
                            _discountTxtFldTop.constant=18;
                            _discountTxtFldHeight.constant=32;
                            _discountLineLblLeft.constant=215;
                            
                            [_dateTimeBtn setImageEdgeInsets:UIEdgeInsetsMake(0, 150, 0, 0)];
                            [_timeBtn setImageEdgeInsets:UIEdgeInsetsMake(0, 150, 0, 0)];
                            
                            _dateAndTimeBtnHeight.constant=32;
                            _dateAndTimeBtnWidth.constant=175;
                            _dateAndTimeBtnTop.constant=15;
                            _dateAndTimeLeft.constant=28;
                            
                            _timeBtnTop.constant=15;
                            _timeBtnWidth.constant=175;
                            _timeBtnHeight.constant=32;
                            _timeLblLeft.constant=215;
                            
                            _dateAndTimeLineWidth.constant=180;
                            _timeLblWidth.constant=180;
                            
                            _termsTxtFLdLeft.constant=25;
                            [self.termsAndConditionTextView setTextContainerInset:UIEdgeInsetsMake(5,-2, 0,0)];
                            
                           
                            
                            
                            
                        }
                        
                        else if(IS_IPHONE_5 || IS_IPHONE_4_OR_LESS)
                            
                        {
                            [_dateTimeBtn setImageEdgeInsets:UIEdgeInsetsMake(0,120, 0, 0)];
                            
                            _priceTxtFldWidth.constant=130;
                            _priceLineWidth.constant=135;
                            
                            //                            _quantityTxtFldWidth.constant=85;
                            //                            _quantityLineLblWidth.constant=90;
                            //                            _quantityTxtFldHeight.constant=22;
                            //                            _quantityLineLblLeft.constant=170;
                            //                            _quantityTxtFldLeft.constant=20;
                            
                            
//                            _quantityTxtFldWidth.constant=240;
//                            _quantityLineLblWidth.constant=245;
//                            _quantityTxtFldHeight.constant=22;
//                            _quantityLineLblLeft.constant=20;
//                            _quantityTxtFldLeft.constant=0;
                            
                            
                            _quantityTxtFldWidth.constant=88;
                            _quantityLineLblWidth.constant=99;
                            _quantityTxtFldHeight.constant=22;
                            _quantityLineLblLeft.constant=169;
                            _quantityTxtFldLeft.constant=27;
                            
                            _priceTxtFldWidth.constant=125;
                            _priceLineWidth.constant=135;
                            
                            self.quantityLineLblTop.constant = 8;
                            
//                            self.txtFld6.backgroundColor = [UIColor redColor];
//                            self.quantityTxtFld.backgroundColor = [UIColor blueColor];
                            
                           // _locationTxtFldWidth.constant=130;
                            _locationTxtFldWidth.constant=262; // by vs
                            _locationLineWidth.constant=286;
                            _locationTxtFldHeight.constant=21;
                            
                            _discountTxtFkdWidth.constant=130;
                            _discountLineLblWidth.constant=135;
                            _discountLineLblLeft.constant=190;
                            _discountTxtFldHeight.constant=21;
                            
                            _termsTxtFldWidth.constant=280;
                            _termsLblWidth.constant=286;
                            
                            
                            
                            if([_termsAndConditionTextView.text isEqual:@"Terms and conditions"])
                            {
                                _termsTxtFldHeight.constant=21;
                                
                            }
                            
                            else
                            {
                                _termsTxtFldHeight.constant=heightOfTermsTxtView;
                                _viewMainHolderHeight.constant=460+_termsTxtFldHeight.constant+_descriptionTxtFldHeight.constant;
                                _viewInsideHolderHeight.constant=460+_termsTxtFldHeight.constant+_descriptionTxtFldHeight.constant;
                                
                                
                            }
                            
                            
                            //  _termsTxtFLdLeft.constant=20;
                            _termsLblHeight.constant=1;
                            
                            _dateAndTimeBtnHeight.constant=21;
                            _dateAndTimeBtnWidth.constant=135;
                            _dateAndTimeBtnTop.constant=14;
                            _dateAndTimeLeft.constant=21;
                            
                            
                            _timeBtnWidth.constant=135;
                            _timeBtnTop.constant=14;
                            _timeBtnHeight.constant=21;
                            _dateAndTimeLineWidth.constant=135;
                            
                            _timeLblWidth.constant=135;
                            _timeLblLeft.constant=168;
                            _tblViewWidth.constant=286;
                            
                            _termsTxtFLdLeft.constant=20;
                            [self.termsAndConditionTextView setTextContainerInset:UIEdgeInsetsMake(5,-2, 0,0)];
                          
                            
                        }
                        
                        [self.view layoutIfNeeded];
                        
                    }
                    completion:NULL];
    
  /*
    if(_txtViewActiveField==_descriptionTextView)
    {
        
        
        if([_descriptionTextView.text isEqual:@"Description"])
        {
            if(IS_IPHONE5)
            {
                _viewMainHolderHeight.constant=450;
            }
            else if (IS_IPHONE_6)
            {
                _viewMainHolderHeight.constant=500;
            }
            else if (IS_IPHONE_6P)
            {
                _viewMainHolderHeight.constant=600;
            }
            
        }
        
        else
        {
            if(IS_IPHONE_6)
            {
                _viewMainHolderHeight.constant=500+_termsTxtFldHeight.constant+_descriptionTxtFldHeight.constant;
                _viewInsideHolderHeight.constant=500+_termsTxtFldHeight.constant+_descriptionTxtFldHeight.constant;
            }
            
            if(IS_IPHONE_5)
            {
                _viewMainHolderHeight.constant=450+_termsTxtFldHeight.constant+_descriptionTxtFldHeight.constant;
                _viewInsideHolderHeight.constant=450+_termsTxtFldHeight.constant+_descriptionTxtFldHeight.constant;
            }
            
            if(IS_IPHONE_6P)
            {
                _viewMainHolderHeight.constant=550+_termsTxtFldHeight.constant+_descriptionTxtFldHeight.constant;
                _viewInsideHolderHeight.constant=550+_termsTxtFldHeight.constant+_descriptionTxtFldHeight.constant;
            }
        }
    }
    
    else
    {
        
        if([_termsAndConditionTextView.text isEqual:@"Terms and conditions"])
        {
            if(IS_IPHONE5)
            {
                _viewMainHolderHeight.constant=450;
            }
            else if (IS_IPHONE_6)
            {
                _viewMainHolderHeight.constant=500;
            }
            else if (IS_IPHONE_6P)
            {
                _viewMainHolderHeight.constant=550;
            }
            
        }
        
        else
        {
            if(IS_IPHONE_6)
            {
                _viewMainHolderHeight.constant=500+_termsTxtFldHeight.constant+_descriptionTxtFldHeight.constant;
                _viewInsideHolderHeight.constant=500+_termsTxtFldHeight.constant+_descriptionTxtFldHeight.constant;
            }
            
            if(IS_IPHONE_5)
            {
                _viewMainHolderHeight.constant=450+_termsTxtFldHeight.constant+_descriptionTxtFldHeight.constant;
                _viewInsideHolderHeight.constant=450+_termsTxtFldHeight.constant+_descriptionTxtFldHeight.constant;
            }
            
            if(IS_IPHONE_6P)
            {
                _viewMainHolderHeight.constant=550+_termsTxtFldHeight.constant+_descriptionTxtFldHeight.constant;
                _viewInsideHolderHeight.constant=550+_termsTxtFldHeight.constant+_descriptionTxtFldHeight.constant;
            }
        }
    }
    */


}
- (IBAction)titleHelpClicked:(id)sender
{
    whichButtonClicked = @"title";
    
    [self helpMeDecideBtnClciked:self.helpMeDecideBtn];
    
}
- (IBAction)discriptionHelpClicked:(id)sender
{
    whichButtonClicked = @"discription";
    [self helpMeDecideBtnClciked:self.helpMeDecideBtn];
}
- (IBAction)locatioHelpClicked:(id)sender
{
    whichButtonClicked = @"location";
    [self helpMeDecideBtnClciked:self.helpMeDecideBtn];
}

-(void)appSwitcher:(id)appSwitcher didPerformSwitchToTarget:(BTAppSwitchTarget)target
{
    
}

- (void)startCheckout {
    
    // [GlobalFunction addIndicatorView];
    
    // Example: Initialize BTAPIClient, if you haven't already
    deviceData = @"";
    nonce = @"";
    
    deviceData = [PPDataCollector collectPayPalDeviceData];
    NSLog(@"Send this device data to your server: %@", deviceData);
    
    self.payPalDriver = [[BTPayPalDriver alloc] initWithAPIClient:self.braintreeClient];
    BTPayPalDriver *payPalDriver = [[BTPayPalDriver alloc] initWithAPIClient:self.braintreeClient];
    self.payPalDriver.viewControllerPresentingDelegate = myAppDelegate.vcObj;
    //self.payPalDriver.appSwitchDelegate = self; // Optional
    
    BTPayPalRequest *checkout = [[BTPayPalRequest alloc] init];
    
    //    [self.payPalDriver authorizeAccountWithAdditionalScopes:[NSSet setWithArray:@[@"address"]]
    //                                            completion:^(BTPayPalAccountNonce *tokenizedPayPalAccount, NSError *error) {
    //        if (tokenizedPayPalAccount) {
    //            BTPostalAddress *address = tokenizedPayPalAccount.billingAddress ?: tokenizedPayPalAccount.shippingAddress;
    //            NSLog(@"nonce is...%@",tokenizedPayPalAccount.nonce);
    //            NSLog(@"Address:\n%@\n%@\n%@ %@\n%@ %@", address.streetAddress, address.extendedAddress, address.locality,address.region, address.postalCode, address.countryCodeAlpha2);
    //        } else if (error) {
    //                            // Handle error
    //             NSLog(@"...Error occured...%@",error);
    //        } else {
    //                            // User canceled
    //            NSLog(@"...Use hit cancell button...");
    //        }
    //        }];
    
    
    checkout.billingAgreementDescription = @"Your agreement description";
    
    
    [self.payPalDriver requestBillingAgreement:checkout completion:^(BTPayPalAccountNonce * _Nullable tokenizedPayPalCheckout, NSError * _Nullable error) {
        if (error)
        {
            NSLog(@"error occuewd");
            [self errorShow:[NSString stringWithFormat:@"%@",error]];
            [GlobalFunction removeIndicatorView];
            
        } else if (tokenizedPayPalCheckout) {
            
            // [GlobalFunction addIndicatorView];
            
            nonce = tokenizedPayPalCheckout.nonce;
            
            NSLog(@"Got a nonce! %@",nonce);
            
            NSLog(@"Got a type is! %@", tokenizedPayPalCheckout.type);
            
            NSLog(@"Got a type is! %@", tokenizedPayPalCheckout.payerId);
            
            BTPostalAddress *address = tokenizedPayPalCheckout.billingAddress?: tokenizedPayPalCheckout.shippingAddress;
            NSLog(@"Billing address:\n%@\n%@\n%@ %@\n%@ %@", address.streetAddress, address.extendedAddress, address.locality, address.region, address.postalCode, address.countryCodeAlpha2);
            
            [GlobalFunction addIndicatorView];
            
            [self sendAuthorizationToServer:deviceData nonce:nonce];
            
        } else
        {
            NSLog(@"user clicked cancelled");
            // [self back];
            [GlobalFunction removeIndicatorView];
        }
    }];
    
    
    
    
}

-(void) errorShow:(NSString *)message
{
    UIAlertView *alertViewErr = [[UIAlertView alloc]initWithTitle:@"BUY TIMEE" message:message delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    
    [alertViewErr show];
    alertViewErr.tag = 1111;
}

-(void)viewWillDisappear:(BOOL)animated
{
    [GlobalFunction removeIndicatorView];
    
    
}

-(void)viewDidDisappear:(BOOL)animated
{
    // [GlobalFunction removeIndicatorView];
    
    NSLog(@"Send this device data to your server");
    
}

#pragma mark - BTViewControllerPresentingDelegate

// Required
- (void)paymentDriver:(id)paymentDriver
requestsPresentationOfViewController:(UIViewController *)viewController {
    [self presentViewController:viewController animated:YES completion:nil];
}

// Required
- (void)paymentDriver:(id)paymentDriver
requestsDismissalOfViewController:(UIViewController *)viewController {
    [viewController dismissViewControllerAnimated:YES completion:nil];
    
    [GlobalFunction addIndicatorView];
}

#pragma mark - BTAppSwitchDelegate

// Optional - display and hide loading indicator UI
- (void)appSwitcherWillPerformAppSwitch:(id)appSwitcher {
    [self showLoadingUI];
    
    // You may also want to subscribe to UIApplicationDidBecomeActiveNotification
    // to dismiss the UI when a customer manually switches back to your app since
    // the payment button completion block will not be invoked in that case (e.g.
    // customer switches back via iOS Task Manager)
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(hideLoadingUI:)
                                                 name:UIApplicationDidBecomeActiveNotification
                                               object:nil];
}

- (void)appSwitcherWillProcessPaymentInfo:(id)appSwitcher {
    [self hideLoadingUI:nil];
}

#pragma mark - Private methods

- (void)showLoadingUI {
    //...
}

- (void)hideLoadingUI:(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIApplicationDidBecomeActiveNotification
                                                  object:nil];
    //....
}

- (void)sendAuthorizationToServer:(NSString *)dataid nonce:(NSString *)nonce
{
    // Send the entire authorization reponse
    
    // NSDictionary* projectDictionary = [authorization objectForKey:@"response"];
    //  projectDictionary1 = [projectDictionary objectForKey:@"code"];
    
    
    // metadataID = [PayPalMobile clientMetadataID];
    
    
    //   NSLog(@"meta data id is...%@",metadataID);
    //   NSLog(@"code is...%@",projectDictionary1);
    
    //  [self dismissViewControllerAnimated:YES completion:nil];
    //  backFromPayPal = true;
    //  [self back];
    // return;
    
    // [GlobalFunction addIndicatorView];
    
    [self performSelector:@selector(callAPI) withObject:nil afterDelay:0.1];
    
    
}

-(void)callAPI
{
    if (![[WebService shared] connected]) {
        
        [GlobalFunction removeIndicatorView];
        
        [[GlobalFunction shared] showAlertForMessage:@"Can't connect to internet, please check and try again."];
        
        return;
    }
    
    //return;
    NSDictionary *dictPaymentResponse = [[WebService shared] addPayPalUserid:myAppDelegate.userData.userid metaDataId:deviceData codeString:nonce];
    
    if (dictPaymentResponse)
    {
        NSLog(@"response --- %@",[EncryptDecryptFile decrypt:[dictPaymentResponse valueForKey:@"responseMessage"]]);
        
        if ([[EncryptDecryptFile decrypt:[dictPaymentResponse valueForKey:@"responseCode"]] isEqualToString:@"200"])
        {
            myAppDelegate.isPaymentFilled = true;
            myAppDelegate.selectedPaymentTypeFromServer = @"2";
            myAppDelegate.ispayWithPayPallFilled = true;
            myAppDelegate.isStripeCardInfoFilled = false;



            [self updatePaymentInfoSuccess:[EncryptDecryptFile decrypt:[dictPaymentResponse valueForKey:@"responseMessage"]]];
        }
        else{
            
            //  [[GlobalFunction shared] showAlertForMessage:[EncryptDecryptFile decrypt:[dictPaymentResponse valueForKey:@"responseMessage"]]];
            [self errorShow:[EncryptDecryptFile decrypt:[dictPaymentResponse valueForKey:@"responseMessage"]]];
            [GlobalFunction removeIndicatorView];
        }
        
    }
    else
    {
        [self errorShow:@"Server is down, please try again later."];
        
        // [[GlobalFunction shared] showAlertForMessage:@"Server is down, please try again later."];
        
        [GlobalFunction removeIndicatorView];
        
    }
    
}

-(void)updatePaymentInfoSuccess:(NSString *)response{
    
    [GlobalFunction removeIndicatorView];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"BUY TIMEE" message:response delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    
    // alert.tag = 10001;
    
    // alert.tag = 1111;
    
    
    [alert show];
    
}


@end
