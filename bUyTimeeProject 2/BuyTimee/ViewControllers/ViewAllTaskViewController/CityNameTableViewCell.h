//
//  CityNameTableViewCell.h
//  BuyTimee
//
//  Created by User38 on 13/04/17.
//  Copyright © 2017 User14. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CityNameTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblCityName;

@end
