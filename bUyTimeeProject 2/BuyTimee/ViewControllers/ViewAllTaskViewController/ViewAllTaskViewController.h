//
//  ViewAllTaskViewController.h
//  BuyTimee
//
//  Created by User14 on 29/02/16.

//
#import <UIKit/UIKit.h>
#import "Flurry.h"

@interface ViewAllTaskViewController : UIViewController <UITableViewDataSource,UITableViewDelegate,UIPickerViewDataSource,UIPickerViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableTaskListing;
@property (weak, nonatomic) IBOutlet UITableView *tableCategoryListing;
@property (weak, nonatomic) IBOutlet UITableView *tableRadius;

@property (weak, nonatomic) IBOutlet UITextField *textFieldPriceRangeMin;
@property (weak, nonatomic) IBOutlet UITextField *textFieldPriceRangeMax;
@property (weak, nonatomic) IBOutlet UITextField *textFieldEnterLocation;

@property (weak, nonatomic) IBOutlet UIView *viewPriceRange;
@property (weak, nonatomic) IBOutlet UIView *viewDistanceRange;
@property (weak, nonatomic) IBOutlet UIView *viewCategory;
@property (weak, nonatomic) IBOutlet UIView *viewTapHandlerForCategoryListing;
@property (weak, nonatomic) IBOutlet UIView *viewBackground;
@property (weak, nonatomic) IBOutlet UIView *viewTapHandlerForPriceRange;
@property (weak, nonatomic) IBOutlet UIView *viewAdjustRadius;
@property (weak, nonatomic) IBOutlet UIView *viewPriceRangeSelector;
@property (weak, nonatomic) IBOutlet UIView *viewTaskDetailHolder;
@property (weak, nonatomic) IBOutlet UIView *viewMapHolder;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintViewCategoryHeight;  //40
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintAdjustRadiusHeight;  //36
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintAdjustPriceRangeHeight;  //40
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintViewDistanceRangeHeight;     //107

@property (weak, nonatomic) IBOutlet UILabel *lblPlaceHolderCategory;
@property (weak, nonatomic) IBOutlet UILabel *lblWithinXMiles;


@property (weak, nonatomic) IBOutlet UIImageView *imgViewPlaceHolderArrow;
@property (weak, nonatomic) IBOutlet UIImageView *imgViewPlaceHolderArrowForPriceRange;
@property (weak, nonatomic) IBOutlet UIImageView *imgViewPlaceHolderArrowForWithinXMiles;
@property (weak, nonatomic) IBOutlet UIImageView *imgViewMapIcon;


@property (nonatomic, retain) UITextField *txtActiveField;
@property (nonatomic, retain) UIView *inputAccView;
@property (nonatomic, retain) UIButton *btnDone;
@property (nonatomic, retain) UIButton *btnNext;
@property (nonatomic, retain) UIButton *btnPrev;

@property(nonatomic,assign)BOOL isMapView;
@property(nonatomic,assign)BOOL isFromMapView;

-(void)completeAfterLoginWork;
-(void)reloadTables;

@property (weak, nonatomic) IBOutlet UILabel *priceRangeLbl;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewPriceRangeHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *priceRangeLblTop;

-(void)performUpdateWork;

// filters outlets...
-(void) checkSelectedBtnsForForwardFilter;
-(void) checkSelectedBtnsForHideFilterFromMenuPress;
@property (weak, nonatomic) IBOutlet UIButton *btnRadius;

- (IBAction)radiusBtnClicked:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *btnTime;
- (IBAction)timeBtnClicked:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *btnPrice;
- (IBAction)priceBtnClicked:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *filterView;

@property (weak, nonatomic) IBOutlet UILabel *maxLabel;

@property (weak, nonatomic) IBOutlet UILabel *minLabel;
@property (weak, nonatomic) IBOutlet UITextField *maxTxtFld;
@property (weak, nonatomic) IBOutlet UITextField *minTxtFld;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consbtntimeleading;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consbtnpriceleading;
@property (weak, nonatomic) IBOutlet UIPickerView *radiusPickerView;
@property (weak, nonatomic) IBOutlet UIView *filterParentView;

@property (weak, nonatomic) IBOutlet UIDatePicker *datePickerView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consFilterViewBottom;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *btnRadiusHeight;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consParentFilterViewBottom;
-(void)finishLoading :(NSDictionary *)data;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewTaskListHolderHeight;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *radiusBtnBottom;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *filterParentViewTop;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lblPlaceholderCagryHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lblSeperatorCatgryBottom;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tblTaskListViewTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tblTaskListViewBottom;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lblPlaceholderCtgryTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imgArrowTop;
@property (weak, nonatomic) IBOutlet UIView *taskListViewHolder;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *radiusBtnTop;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consMinTextFldHeight;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consMinTextFldWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consMaxTextFldWidth;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consMaxTextFldHeight;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consLblMInLeading;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consTextFldMaxLeading;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consLblAllCategoryLeading;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consViewCagegoryTapHandlerHeight;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consImgArrowHeight;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consImgArrowWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consTblCategoryTop;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *filterGreyViewHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *minTxtFldLeft;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *minTxtFldTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *maxTxtFldTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *minTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *maxTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *filterViewWidth;

@property (weak, nonatomic) IBOutlet UIView *ViewFilterBehindForLines;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consViewFilterBtnsBehindHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consMxTxtFldLeft;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewTapHandlerTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewCategoryTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewTaskListHolderTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consLblNoReserationTop;

@property (weak, nonatomic) IBOutlet UIView *filterCityView;
@property (weak, nonatomic) IBOutlet UITableView *cityNameTableView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *filterCityViewWidth;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *filterCityViewHeight;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *filterCityViewBottom;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *filterCityViewTrailing;
@property(nonatomic,strong)NSIndexPath *lastSelectedIndexPath;
@property(nonatomic,strong) NSString *enteredLocation;
@end
