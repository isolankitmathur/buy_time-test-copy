//
//  ViewAllTaskViewController.m
//  BuyTimee
//
//  Created by User14 on 29/02/16.

//

#import "ViewAllTaskViewController.h"
#import "Constants.h"
#import "GlobalFunction.h"
#import "headers.pch"
#import "task.h"
#import "category.h"
#import "subCategory.h"
#import "CityNameTableViewCell.h"
#import "cities.h"

@interface ViewAllTaskViewController () <UIGestureRecognizerDelegate,UITextFieldDelegate,UIAlertViewDelegate,UIScrollViewDelegate>

@property (weak, nonatomic) IBOutlet UIView *viewAdjustRadiusTapHolder;
@property (weak, nonatomic) IBOutlet UILabel *lblNoTaskInfo;

- (IBAction)animateImageViewToFullScreen:(id)sender;

@property (strong, nonatomic) IBOutlet UITapGestureRecognizer *imgViewTapGesture;
@property (weak, nonatomic) IBOutlet UILabel *lblSeperatorCategory;

@property (weak, nonatomic) IBOutlet UITextField *textFieldRadiusFilter;

@end

@implementation ViewAllTaskViewController{
    
    UITapGestureRecognizer *tapGestureAdjustRadius;
    UITapGestureRecognizer *tapGesturePriceRange;
    UITapGestureRecognizer *tapGesture;
    
    NSArray *arrRadius,*FilterCities;
    CGFloat height;
    
    NSString *categoryName;
    
    NSArray *arrayOfTaskWithStatus0;
    
    
    NSString *filterCategoryId;
    NSString *filterSubCategoryId;
    NSString *filterRange;
    NSString *filterLocationName;
    NSString *filterPriceMin;
    NSString *filterPriceMax;
    
    BOOL isFromTableTap;
    BOOL isFromRadiusTableTap;
    
    //****************** price range label animation work 18Apr2016
    
    UILabel *maxLbl;
    UILabel *minLbl;
    UILabel *lblDashForMinAndMax;
    
    CGFloat minLblWidth;
    CGFloat maxLblWidth;
    
    //******************
    
   // NSIndexPath *lastSelectedIndexPath;

    BOOL isFromSearchResult;
    NSArray *searchResultArrayFromRadius;
    
    UIRefreshControl *refreshControl;
    
    
    NSString *oldRadiusData;
    
    NSString *oldRadiusData_TextToShowAtEditing;
    NSString *oldRadiusData_TextToShowAsPlaceHolder;
    
    NSString *oldLocationData;
    NSString *oldMinPriceRangeData;
    NSString *oldMaxPriceRangeData;
    
    NSDate *dateStart;
    NSDate *dateEnd;
  //  NSString *enteredLocation;    // commented by rohit
     NSString *rangeLocation;
    
    NSString *minPriceValue;
    NSString *maxPriceValue;
    
    NSString *minTime;
    NSString *maxTime;
//    
//    NSString *oldLocation;
//    NSString *oldRadius;
//    NSString *oldMaxPrice;
//    NSString *oldMinPrice;
//    NSString *oldStartDate;
//    NSString *oldEndDate;
    UIToolbar *toolBar;
    
    UIButton *checkBoxBtn;
    
    int lowValue1;
    int highValue1;
}

@synthesize txtActiveField,enteredLocation;
@synthesize inputAccView;
@synthesize btnDone;
@synthesize btnNext;
@synthesize btnPrev,lastSelectedIndexPath;

- (void)viewDidLoad {
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    myAppDelegate.viewAllTaskVCObject=self;

    
    // datePickerView = [[UIDatePicker alloc]init];
    
   // self.filterParentView.backgroundColor = [UIColor clearColor];
    
    
    _btnRadius.backgroundColor=[UIColor colorWithRed:63.0/255.0f green:67.0/255.0f blue:70.0/255.0f alpha:1.0];
    _btnPrice.backgroundColor=[UIColor colorWithRed:63.0/255.0f green:67.0/255.0f blue:70.0/255.0f alpha:1.0];
    _btnTime.backgroundColor=[UIColor colorWithRed:63.0/255.0f green:67.0/255.0f blue:70.0/255.0f alpha:1.0];
    _filterView.backgroundColor=[UIColor colorWithRed:82.0/255.0f green:85.0/255.0f blue:88.0/255.0f alpha:1.0];
    self.filterCityView.layer.borderWidth = 1;
   // self.cityNameTableView.layer.borderWidth = 1;
    self.filterCityView.layer.borderColor = [[UIColor colorWithRed:63.0/255.0f green:67.0/255.0f blue:70.0/255.0f alpha:1.0] CGColor];
    self.cityNameTableView.layer.borderColor = [[UIColor colorWithRed:63.0/255.0f green:67.0/255.0f blue:70.0/255.0f alpha:1.0] CGColor];
    self.filterCityView.layer.cornerRadius = 10;
    self.cityNameTableView.layer.cornerRadius = 10;
    
    
    self.cityNameTableView.delegate = self;
    self.cityNameTableView.dataSource = self;
    
    self.filterCityViewHeight.constant = 0;
  
    
    
    
    self.minTxtFld.textColor = [UIColor colorWithRed:63/255.0 green:67/255.0 blue:70/255.0 alpha:1.0];
    self.maxTxtFld.textColor = [UIColor colorWithRed:63/255.0 green:67/255.0 blue:70/255.0 alpha:1.0];
    
    _radiusPickerView.delegate = self;
    
    self.radiusPickerView.hidden = true;
    
    
        self.filterParentView.hidden = true;
        self.filterView.hidden = true;
        _datePickerView.hidden = true;
    
         _filterGreyViewHeight.constant=0;        //by kp

    _datePickerView.minimumDate = [NSDate date];
   _datePickerView.timeZone = [NSTimeZone systemTimeZone];
    [_datePickerView setDate:[NSDate date]];
    // [_datePickerView setDatePickerMode:UIDatePickerModeDateAndTime];
      [_datePickerView setDatePickerMode:UIDatePickerModeDate];
    [_datePickerView addTarget:self action:@selector(updateTextField:) forControlEvents:UIControlEventValueChanged];
    //[_datePickerView setDatePickerMode:UIDatePickerModeDate];
    
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"d.M.yyyy";
    NSString *string = [formatter stringFromDate:[NSDate date]];
    NSDate *mainDate = [formatter dateFromString:string];
    //  dateStart = mainDate; // commented by vs for remove default date in filter to show old tasks 10 oct 17.

    
    NSLog(@"main date in did load is...%@",dateStart);
    
   // myAppDelegate.vcObj.btnMapOnListView.hidden = false;
    
    categoryName = @"All Categories";
    
        filterCategoryId = @"";
        filterLocationName = @"";
        filterPriceMax = @"";
        filterPriceMin = @"";
    
        filterRange = @"30";
        rangeLocation = @"30";
//        filterRange = @"50";
//        rangeLocation = @"50";
    
    
    
        filterSubCategoryId = @"";
    lastSelectedIndexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    
    self.minTxtFld.delegate = self;
    self.maxTxtFld.delegate = self;
    
    self.textFieldPriceRangeMax.keyboardType = UIKeyboardTypeDecimalPad;
    self.textFieldPriceRangeMin.keyboardType = UIKeyboardTypeDecimalPad;

    self.textFieldEnterLocation.delegate = self;
    self.textFieldPriceRangeMax.delegate = self;
    self.textFieldPriceRangeMin.delegate = self;
    
    UILabel * leftView = [[UILabel alloc] initWithFrame:CGRectMake(10,0,5,self.textFieldEnterLocation.frame.size.height)];
    leftView.backgroundColor = [UIColor clearColor];

    UILabel * leftView2 = [[UILabel alloc] initWithFrame:CGRectMake(10,0,5,self.textFieldEnterLocation.frame.size.height)];
    leftView2.backgroundColor = [UIColor clearColor];
    
    UILabel * leftView3 = [[UILabel alloc] initWithFrame:CGRectMake(10,0,5,self.textFieldEnterLocation.frame.size.height)];
    leftView3.backgroundColor = [UIColor clearColor];

    UILabel * leftView4 = [[UILabel alloc] initWithFrame:CGRectMake(10,0,5,self.textFieldEnterLocation.frame.size.height)];
    leftView4.backgroundColor = [UIColor clearColor];

    self.textFieldEnterLocation.leftView = leftView;
    self.textFieldEnterLocation.leftViewMode = UITextFieldViewModeAlways;
    self.textFieldEnterLocation.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    
    self.textFieldPriceRangeMax.leftView = leftView2;
    self.textFieldPriceRangeMax.leftViewMode = UITextFieldViewModeAlways;
    self.textFieldPriceRangeMax.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;

    self.textFieldPriceRangeMin.leftView = leftView3;
    self.textFieldPriceRangeMin.leftViewMode = UITextFieldViewModeAlways;
    self.textFieldPriceRangeMin.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;

    self.textFieldRadiusFilter.leftView = leftView4;
    self.textFieldRadiusFilter.leftViewMode = UITextFieldViewModeAlways;
    self.textFieldRadiusFilter.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    
    //category view tap gesture
    tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(openCategoryTableView:)];
    tapGesture.delegate = self;
    tapGesture.numberOfTapsRequired = 1;
    [self.viewTapHandlerForCategoryListing addGestureRecognizer:tapGesture];
    
    //price range view tap gesture
    tapGesturePriceRange = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(openPriceRangeView:)];
    tapGesturePriceRange.delegate = self;
    tapGesturePriceRange.numberOfTapsRequired = 1;
    [self.viewTapHandlerForPriceRange addGestureRecognizer:tapGesturePriceRange];
    
    //adjust radius tap gesture
    tapGestureAdjustRadius = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(openAdjustRadiusTableView:)];
    tapGestureAdjustRadius.delegate = self;
    tapGestureAdjustRadius.numberOfTapsRequired = 1;
    [self.viewAdjustRadiusTapHolder addGestureRecognizer:tapGestureAdjustRadius];
    
    //adjust radius tap gesture
    UITapGestureRecognizer *tapGestureOnBGView = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideOpenView:)];
    tapGestureOnBGView.delegate = self;
    tapGestureOnBGView.numberOfTapsRequired = 1;
    [self.viewBackground addGestureRecognizer:tapGestureOnBGView];
    
    [GlobalFunction createBorderforView:self.textFieldEnterLocation withWidth:1.0 cornerRadius:5.0 colorForBorder:[UIColor colorWithRed:151.0/255.0 green:151.0/255.0 blue:151.0/255.0 alpha:1.0]];
    [GlobalFunction createBorderforView:self.textFieldPriceRangeMax withWidth:1.0 cornerRadius:5.0 colorForBorder:[UIColor colorWithRed:151.0/255.0 green:151.0/255.0 blue:151.0/255.0 alpha:1.0]];
    [GlobalFunction createBorderforView:self.textFieldPriceRangeMin withWidth:1.0 cornerRadius:5.0 colorForBorder:[UIColor colorWithRed:151.0/255.0 green:151.0/255.0 blue:151.0/255.0 alpha:1.0]];
    
    [GlobalFunction createBorderforView:self.viewAdjustRadius withWidth:1.0 cornerRadius:5.0 colorForBorder:[UIColor colorWithRed:151.0/255.0 green:151.0/255.0 blue:151.0/255.0 alpha:1.0]];
    [GlobalFunction createBorderforView:self.viewDistanceRange withWidth:1.0 cornerRadius:0.0 colorForBorder:[UIColor colorWithRed:199.0/255.0 green:199.0/255.0 blue:199.0/255.0 alpha:1.0]];
    [GlobalFunction createBorderforView:self.viewPriceRange withWidth:1.0 cornerRadius:0.0 colorForBorder:[UIColor colorWithRed:199.0/255.0 green:199.0/255.0 blue:199.0/255.0 alpha:1.0]];
    
    self.tableCategoryListing.hidden = YES;
    
    self.tableTaskListing.delegate = self;
    self.tableTaskListing.dataSource = self;
    

    
    [self.tableTaskListing layoutIfNeeded];
    [self.tableTaskListing reloadData];
    [self.tableTaskListing layoutIfNeeded];
    
    arrRadius = [NSArray arrayWithObjects:@"10",@"20",@"30",@"40",@"50",@"60",@"70",@"80",@"90",@"100",@"200",@"300",@"400",@"500", nil];

    [self.tableRadius layoutIfNeeded];
    [self.tableRadius reloadData];
    [self.tableRadius layoutIfNeeded];
    
    //**************************
    minLbl =[[UILabel alloc]init];
    minLbl.text = @"";
    minLbl.textColor=[UIColor lightGrayColor];
    [self.viewPriceRangeSelector addSubview:minLbl];
    
    maxLbl =[[UILabel alloc]init];
    maxLbl.text = @"";
    maxLbl.textColor=[UIColor lightGrayColor];
    [maxLbl sizeToFit];
    [self.viewPriceRangeSelector addSubview:maxLbl];
    
    lblDashForMinAndMax = [[UILabel alloc] init];
    lblDashForMinAndMax.text = @"-";
    lblDashForMinAndMax.textColor = [UIColor colorWithRed:171.0/255.0 green:171.0/255.0 blue:171.0/255.0 alpha:1.0];
    
    minLbl.hidden=YES;
    maxLbl.hidden=YES;
    //***************************
    
    self.lblNoTaskInfo.hidden = YES;
    self.lblSeperatorCategory.hidden = NO;
    
    self.lblWithinXMiles.text = [NSString stringWithFormat:@"Within %ld Miles",(long)[filterRange integerValue]];
    self.textFieldRadiusFilter.hidden = YES;
    self.textFieldRadiusFilter.text = filterRange;
#pragma mark - TODO: update label text
    self.lblNoTaskInfo.text = @"No appointment is available in nearby location.";
    
    // old one..No reservation is available in nearby location.
    
    refreshControl = [[UIRefreshControl alloc] init];
    refreshControl.backgroundColor = colorGreenDark;
    refreshControl.tintColor = [UIColor whiteColor];
    [refreshControl addTarget:self action:@selector(refreshTriggered:) forControlEvents:UIControlEventValueChanged];
    [self.tableTaskListing addSubview:refreshControl];
    
    [GlobalFunction addIndicatorView];
    
    
    if (myAppDelegate.currentCityName.length>0)
    {
        enteredLocation = myAppDelegate.currentCityName;
    }
    else
    {
    }
    
    

}
-(void)finishLoading :(NSDictionary *)data
{
    
    NSDictionary *dictReg = [data copy];
}
-(NSDate *)dateWithOutTime:(NSDate *)datDate {
    if( datDate == nil ) {
        datDate = [NSDate date];
    }
    NSDateComponents* comps = [[NSCalendar currentCalendar] components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:datDate];
    
    NSDateComponents *components3 = [[NSDateComponents alloc] init];
    
    [components3 setYear:comps.year];
    [components3 setMonth:comps.month];
    [components3 setDay:comps.day];
    NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDate *mm = [gregorianCalendar dateFromComponents:components3];
    return [[NSCalendar currentCalendar] dateFromComponents:components3];
}
-(void)updateTextField:(id)sender{
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:dateFormatDate];
    
    NSDateFormatter *TimeFormat1 = [[NSDateFormatter alloc] init];
    [TimeFormat1 setDateFormat:dateFormatOnlyHour];
    
    if (_datePickerView.hidden == false) {
        
       // _datePickerView.hidden = true;
       // self.filterParentView.hidden = true;
    }
    
    if (txtActiveField == self.minTxtFld) {

//        NSDateFormatter *dateFormat11 = [[NSDateFormatter alloc] init];
//        [dateFormat11 setDateFormat:@"YYYY-MM-dd HH:mm:ss zz"];
//        
//        NSString *aa = [dateFormat11 stringFromDate:_datePickerView.date];
//        
//        NSArray *components = [aa componentsSeparatedByString:@" "];
//        NSString *imgNameWithJPEG = [components firstObject];
//        NSString* string2 = [imgNameWithJPEG stringByReplacingOccurrencesOfString:@"-" withString:@"/"];
//        
//        dateStart = [dateFormat dateFromString:string2];
//        
//        dateStart = [self dateWithOutTime:_datePickerView.date];
        
       dateStart = _datePickerView.date;
        
        NSString *TimeString = [GlobalFunction convertTimeFormat:[TimeFormat1 stringFromDate:dateStart]];
       NSString *dateString = [GlobalFunction convertDateFormat:[dateFormat stringFromDate:dateStart]];
        
        NSString *str = [@[dateString, TimeString] componentsJoinedByString:@","];
       // NSString *mainString = [
        self.minTxtFld.text = dateString;
        minTime = dateString;
       // self.datePickerView.hidden = true;
       // self.filterParentView.backgroundColor = [UIColor clearColor];
        
       // [self performFilter];
        
    }
    else if (txtActiveField == self.maxTxtFld)
    {
        dateEnd = _datePickerView.date;
        
        NSString *TimeString = [GlobalFunction convertTimeFormat:[TimeFormat1 stringFromDate:dateEnd]];
        NSString *dateString = [GlobalFunction convertDateFormat:[dateFormat stringFromDate:dateEnd]];
        NSString *str = [@[dateString, TimeString] componentsJoinedByString:@""];
        
        self.maxTxtFld.text = dateString;
        maxTime = dateString;
       // self.datePickerView.hidden = true;
       // self.filterParentView.backgroundColor = [UIColor clearColor];
       // [self performFilter];
    }
    
//    if (dateStart != nil && dateEnd != nil)
//    {
////        if ([dateStart compare:dateEnd] == NSOrderedDescending) {
////            NSLog(@"date1 is Newer than date2");
////        } else
//        
//       if ([dateStart compare:dateEnd] == NSOrderedAscending) {
//            NSLog(@"date1 is earlier than date2");
//           [self performFilter];
//        } else {
//            
//            NSLog(@"date2 is earlier than date1");
//            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"BuyTimee" message:@"Please enter a valid date." delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:nil];
//            
//            [alertView show];
//        }
//        
//
//    }
    
//    if (dateStart != nil || dateEnd != nil)
//    {
//        [self performFilter];
//    }
    
}

//-(void) dateTextField:(id)sender
//{
//   // UIDatePicker *picker = (UIDatePicker*)txtFieldBranchYear.inputView;
//    [self.datePickerView setMaximumDate:[NSDate date]];
//    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
//    NSDate *eventDate = self.datePickerView .date;
//    [dateFormat setDateFormat:@"dd/MM/yyyy"];
//    
//    NSString *dateString = [dateFormat stringFromDate:eventDate];
//    self.minTxtFld.text = [NSString stringWithFormat:@"%@",dateString];
//}

- (void)refreshTriggered:(id)sender{
    [self performSelectorInBackground:@selector(performUpdateOnSwipeDown) withObject:nil];
//    [self performSelector:@selector(performUpdateOnSwipeDown) withObject:nil afterDelay:0.5 inModes:@[NSRunLoopCommonModes]];
}

-(void)performUpdateOnSwipeDown{
    
    NSDictionary *dictUser = [UserDefaults objectForKey:keyUserloginDetailDictionary];
    
  //  NSDictionary *dictUpdate = [[WebService shared] getUpdateuserid:[dictUser valueForKey:keyUserId] lastModifyTime:[UserDefaults valueForKey:keyLastModifyTime] andToken:myAppDelegate.deviceTokenString];
    
  //  NSDictionary *dictUpdate = [[WebService shared] getUpdatelastModifyTime:[UserDefaults valueForKey:keyLastModifyTime]];
    
    NSString *timeZoneSeconds = [GlobalFunction getTimeZone];
    
    NSDictionary *dictUpdate =  [[WebService shared] getUpdatelastModifyTime:[UserDefaults valueForKey:keyLastModifyTime] userid:myAppDelegate.userData.userid timeZone:timeZoneSeconds];
    
    if (dictUpdate != nil) {
        
        if ([[EncryptDecryptFile decrypt:[dictUpdate valueForKey:@"responseCode"]] isEqualToString:@"200"]) {
            
            [UserDefaults setValue:[EncryptDecryptFile decrypt:[dictUpdate valueForKey:@"lastModifyTime"]] forKey:keyLastModifyTime];
            
            NSDictionary *dictDataReg = [dictUpdate objectForKey:@"data"];
            
            
//            NSString *name=[EncryptDecryptFile decrypt:[dictDataReg valueForKey:@"title"]];
//            NSLog(@"%@",name);
            
            if ([DatabaseClass updateDataBaseFromUpdatedDictionary:dictDataReg]) {
                
                dispatch_async(dispatch_get_main_queue(), ^(){
                    //Add method, task you want perform on mainQueue
                    //Control UIView, IBOutlet all here
                    [self reloadTables];
                    
                    [refreshControl endRefreshing];
                });

            }
            
        }else{
            
            dispatch_async(dispatch_get_main_queue(), ^(){
                //Add method, task you want perform on mainQueue
                //Control UIView, IBOutlet all here
                [refreshControl endRefreshing];
            });

        }
        
    }else{
       
        dispatch_async(dispatch_get_main_queue(), ^(){
            //Add method, task you want perform on mainQueue
            //Control UIView, IBOutlet all here
            [refreshControl endRefreshing];
        });

    }

}

-(void)viewDidDisappear:(BOOL)animated
{
    NSLog(@"calling");
}


-(void)viewWillDisappear:(BOOL)animated
{
    NSLog(@"calling");
}

-(void)viewDidAppear:(BOOL)animated{
    //
    //    self.btnPrice.hidden = false;
    //    self.btnTime.hidden = false;
    //    self.btnRadius.hidden = false;
    
    NSLog(@"top y%f",self.viewTaskListHolderTop.constant);
    NSString* final = @"Reservations on list page";
    
    [Flurry logEvent:final];

    
    NSLog(@"%@", myAppDelegate.arrayCategoryDetail);
    
    [self.view layoutIfNeeded];
    self.constraintViewDistanceRangeHeight.constant = 0; //add by vs to set size 0 to distance filter
    self.constraintAdjustPriceRangeHeight.constant = 0; //add by vs to set size 0 to price filter
    
    self.consbtntimeleading.constant = 0.5f;
    self.consbtnpriceleading.constant = 0.5f;
    
   // self.consLblNoReserationTop.constant = -50; //for urgest no reservation lbl top...
    
    NSLog(@"height of buttonns is...%f",self.btnRadius.frame.size.height);
    NSLog(@"height of buttonns is...%f",self.btnTime.frame.size.height);
    
    
    _lblSeperatorCategory.backgroundColor=[UIColor colorWithRed:196.0/255.0f green:196.0/255.0f blue:196.0/255.0f alpha:1.0];
    _viewTapHandlerForCategoryListing.backgroundColor=[UIColor colorWithRed:244.0/255.0f green:244.0/255.0f blue:244.0/255.0f alpha:1.0];
    
    
    
    if (IS_IPHONE_5 || IS_IPHONE_4_OR_LESS)
    {
        
        [self.btnRadius.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:17]];
        [self.btnTime.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:17]];
        [self.btnPrice.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:17]];
        
        myAppDelegate.vcObj.headerViewHeight.constant=60;
        self.constraintViewCategoryHeight.constant = 37;
        self.consViewCagegoryTapHandlerHeight.constant = 37;
        self.consTblCategoryTop.constant = 34;
        [_lblPlaceHolderCategory setFont:[UIFont fontWithName:@"Lato-Regular" size:13]];
        _lblSeperatorCatgryBottom.constant=0;
        _lblPlaceholderCtgryTop.constant=10;
        
       // _lblPlaceholderCagryHeight.constant=1.5f;
        
        //_lblSeperatorCategory.backgroundColor=[UIColor blackColor];

        _imgArrowTop.constant=16;
       // _radiusBtnTop.constant=40;
        
        _radiusBtnTop.constant=5;

        _radiusBtnBottom.constant=48;
        _btnRadius.titleEdgeInsets=UIEdgeInsetsMake(-11, 0, 0, 0);
        _btnTime.titleEdgeInsets=UIEdgeInsetsMake(-11, 0, 0, 0);
        _btnPrice.titleEdgeInsets=UIEdgeInsetsMake(-11, 0, 0, 0);

        _consMaxTextFldWidth.constant=117;
        _consMinTextFldWidth.constant=117;
        _minTxtFldLeft.constant=0;
        _consMinTextFldHeight.constant=29;
        _minTxtFldTop.constant=3;
        _maxTxtFldTop.constant=3;
        _consMaxTextFldHeight.constant=29;
        _consTextFldMaxLeading.constant=27;
      //  _filterGreyViewHeight.constant=81;
        _consFilterViewBottom.constant=1;
        [_minLabel setFont:[UIFont systemFontOfSize:13.5]];
        [_maxLabel setFont:[UIFont systemFontOfSize:13.5]];
        _consLblMInLeading.constant=31;
        _minTop.constant=11;
        _maxTop.constant=11;
        _consMxTxtFldLeft.constant=-2;
        self.consViewFilterBtnsBehindHeight.constant = 41;
        

        
        self.filterCityViewBottom.constant = -35;
        self.filterCityViewTrailing.constant = 29;
        self.filterCityViewWidth.constant = 119;
        
    }
    else if(IS_IPHONE_6)
    {
        
        
        self.filterCityViewBottom.constant = -41;
        self.filterCityViewTrailing.constant = 36;
        
        
        
        [_lblPlaceHolderCategory setFont:[UIFont fontWithName:@"Lato-Regular" size:16]];
        [self.btnRadius.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:18]];
        [self.btnTime.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:18]];
        [self.btnPrice.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:18]];
        NSLog(@"y of radius is..%f",self.btnRadius.frame.origin.y);
        NSLog(@"bottom of radius..%f",_radiusBtnBottom.constant);
        
        
        //_radiusBtnTop.constant=22;
        
        _radiusBtnTop.constant=5;

        _radiusBtnBottom.constant=67;
        
        self.consMaxTextFldHeight.constant = 33;
        self.consMinTextFldHeight.constant = 33;
        self.consViewFilterBtnsBehindHeight.constant = 47;
        self.constraintViewCategoryHeight.constant = 41;
        self.consViewCagegoryTapHandlerHeight.constant = 41;
       // _lblPlaceholderCagryHeight.constant=1.5f;
        _lblSeperatorCatgryBottom.constant=0;
        
        
      //  _filterGreyViewHeight.constant=96;
        _consFilterViewBottom.constant=0;
        
        _consMaxTextFldWidth.constant=137;
        _consMinTextFldWidth.constant=137;
        
        _minTxtFldLeft.constant=-1;
        _consMxTxtFldLeft.constant=-1;
        
        _consMinTextFldHeight.constant=34;
        _consMaxTextFldHeight.constant=34;
        
        _minTxtFldTop.constant=5;
        _maxTxtFldTop.constant=5;
        
        _consTextFldMaxLeading.constant=31;
        _consLblMInLeading.constant=37;
        
        
        [_minLabel setFont:[UIFont systemFontOfSize:16]];
        [_maxLabel setFont:[UIFont systemFontOfSize:16]];
        
        _minTop.constant=15;
        _maxTop.constant=15;
        
     
        _btnRadius.titleEdgeInsets=UIEdgeInsetsMake(-4, 0, 0, 0);
        _btnTime.titleEdgeInsets=UIEdgeInsetsMake(-4, 0, 0, 0);
        _btnPrice.titleEdgeInsets=UIEdgeInsetsMake(-4, 0, 0, 0);

        
        
        
    }
    else if(IS_IPHONE_6P)
    {
        
        self.filterCityViewBottom.constant = -46;
        self.filterCityViewTrailing.constant = 39;
        self.filterCityViewWidth.constant = 151;
        
        [_lblPlaceHolderCategory setFont:[UIFont fontWithName:@"Lato-Regular" size:18]];
        [self.btnRadius.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:20]];
        [self.btnTime.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:20]];
        [self.btnPrice.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:20]];
        NSLog(@"y of radius is..%f",self.btnRadius.frame.origin.y);
        NSLog(@"bottom of radius..%f",_radiusBtnBottom.constant);
        self.constraintViewCategoryHeight.constant = 48;
        self.consViewCagegoryTapHandlerHeight.constant = 48;
        self.consTblCategoryTop.constant = 48;
        self.consLblAllCategoryLeading.constant = 15;
        self.imgArrowTop.constant = 22;
        self.consImgArrowWidth.constant = 13;
        self.consImgArrowHeight.constant = 10;
        
       // _radiusBtnTop.constant=13;
        _radiusBtnTop.constant=5;

        
        _radiusBtnBottom.constant=75;
        
      //  _filterGreyViewHeight.constant=106;
        _consFilterViewBottom.constant=0;
        
        _minTxtFldLeft.constant=2;
        
        _consMinTextFldWidth.constant=150;
        _consMinTextFldHeight.constant=38;
        
        _consTextFldMaxLeading.constant=36;
        
        _consMaxTextFldWidth.constant=150;
        _consMaxTextFldHeight.constant=38;
        
        _minTxtFldTop.constant=7;
        _maxTxtFldTop.constant=7;
        
        _minTop.constant=18;
        _maxTop.constant=18;
        
        
        self.consViewFilterBtnsBehindHeight.constant = 52;
        [_minLabel setFont:[UIFont systemFontOfSize:18]];
        [_maxLabel setFont:[UIFont systemFontOfSize:18]];
        
        
    }
    
    [self.view layoutIfNeeded];
    
    NSLog(@"y of radius is..%f",self.btnRadius.frame.origin.y);
    if (myAppDelegate.isComingBackAfterMapOpen) {
        
        myAppDelegate.isComingBackAfterMapOpen = false;
        
    }else{
        
        //myAppDelegate.vcObj.lblTitleText.text = @"B U Y  T I M E E";
        
        [self.view layoutIfNeeded];
        
        height = self.viewAdjustRadius.frame.size.height;
        
        [self.tableTaskListing layoutIfNeeded];
        [self.tableTaskListing reloadData];
        [self.tableTaskListing layoutIfNeeded];
        
        [self performSelector:@selector(performUpdateWork) withObject:nil afterDelay:0.2];
        
    }
    
}


-(void)performUpdateWork{
    
    [GlobalFunction addIndicatorView];
    
    [self performSelector:@selector(performUpdateAfterSomeDelay) withObject:nil afterDelay:0.2];
    
    
}

//-(void)performUpdateAfterSomeDelay{
//    
//    if (![self checkForInternet]) {
//        
//        [self performSelector:@selector(reloadTables) withObject:nil afterDelay:0.2];
//        
//        return;
//    }
//    
//    if ([[GlobalFunction shared] callUpdateWebservice]) {
//        
//        [self performSelector:@selector(reloadTables) withObject:nil afterDelay:0.2];
//        
//    }else{
//        
//        [GlobalFunction removeIndicatorView];
//        
//        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"BuyTimee" message:@"Failed to update, please try again." delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Retry", nil];
//        
//        alertView.tag = 999;
//        
//        [alertView show];
//        
//    }
//
//}

-(void)performUpdateAfterSomeDelay{
    

    
    ////************Filter Data Handling**********//
    
    if (myAppDelegate.isFromMapVCForFilter == true)
    {
        
        
        
        if (![self checkForInternet]) {
            
            // [self performSelector:@selector(reloadTables) withObject:nil afterDelay:0.01];
            [self reloadTables];
            return;
        }
        
        if ([[GlobalFunction shared] callUpdateWebservice]) {
            
            //  [self performSelector:@selector(reloadTables) withObject:nil afterDelay:0.01];
           // [self reloadTables]; // comment by vs for repeat filters.. 31 Aug 18...
            
        }else{
            
            [GlobalFunction removeIndicatorView];
            
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"BUY TIMEE" message:@"Failed to update, please try again." delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Retry", nil];
            
            alertView.tag = 999;
            
            [alertView show];
            
        }
        
        
        
        
        
        NSLog(@"index path is...%ld",(long)myAppDelegate.SelectedIndexPathForFilter.row);
        if (myAppDelegate.SelectedIndexPathForFilter.row > 0) {
            
            lastSelectedIndexPath = myAppDelegate.SelectedIndexPathForFilter;
            
            //[self.tblCategory reloadData]; category *obj = [myAppDelegate.arrayCategoryDetail objectAtIndex:myAppDelegate.SelectedIndexPathForFilter.row-1];
            category *obj = [myAppDelegate.arrayCategoryDetail objectAtIndex:myAppDelegate.SelectedIndexPathForFilter.row-1];
            filterCategoryId = obj.categoryId;
            categoryName=obj.name;
            self.lblPlaceHolderCategory.text=categoryName;
            
        }
        else
        {
            lastSelectedIndexPath = [NSIndexPath indexPathForRow:0 inSection:0];
            categoryName = @"All Categories";
            filterCategoryId = @"";
        }
        
        
        
        
        myAppDelegate.isFromMapVCForFilter = false;
        
        if (myAppDelegate.mainMiles)
        {
            rangeLocation = myAppDelegate.mainMiles;
            filterRange = rangeLocation;
        }
        else
        {
            
        }
        if (myAppDelegate.mainLocation)
        {
            enteredLocation = myAppDelegate.mainLocation;
            if (![filterLocationName isEqualToString:enteredLocation])
            {
                filterLocationName = enteredLocation;
            }
        }
        else
        {
            
        }
        if (myAppDelegate.mainMinPrice)
        {
            minPriceValue = myAppDelegate.mainMinPrice;
        }
        else
        {
            
        }
        if (myAppDelegate.mainmaxPrice)
        {
            maxPriceValue = myAppDelegate.mainmaxPrice;
        }
        else
        {
            
        }
        
        
        NSDateFormatter *dateFormat00 = [[NSDateFormatter alloc] init];
        [dateFormat00 setDateFormat:dateFormatDate];
        
        NSDateFormatter *TimeFormat11 = [[NSDateFormatter alloc] init];
        [TimeFormat11 setDateFormat:dateFormatOnlyHour];
        
        if (myAppDelegate.mainStartDate)
        {
            dateStart = myAppDelegate.mainStartDate;
            
            NSString *TimeString = [GlobalFunction convertTimeFormat:[TimeFormat11 stringFromDate:dateStart]];
            NSString *dateString = [GlobalFunction convertDateFormat:[dateFormat00 stringFromDate:dateStart]];
            NSString *str00 = [@[dateString, TimeString] componentsJoinedByString:@""];
            minTime = dateString;
        }
        else
        {
            // dateStart = mainDate;
        }
        if (myAppDelegate.mainEndDate)
        {
            dateEnd = myAppDelegate.mainEndDate;
            
            NSString *TimeString = [GlobalFunction convertTimeFormat:[TimeFormat11 stringFromDate:dateEnd]];
            NSString *dateString = [GlobalFunction convertDateFormat:[dateFormat00 stringFromDate:dateEnd]];
            NSString *str00 = [@[dateString, TimeString] componentsJoinedByString:@""];
            maxTime = dateString;
        }
        
        NSLog(@"main date in did load is...%@",dateStart);
        NSLog(@"main date in did load is...%@",dateEnd);
        
       // [self performFilter]; // comment by vs for repeat filters.. 31 Aug 18...

        [self reloadTables];
    }
    
    else
    {
        if (![self checkForInternet]) {
            
            // [self performSelector:@selector(reloadTables) withObject:nil afterDelay:0.01];
            [self reloadTables];
            return;
        }
        
        if ([[GlobalFunction shared] callUpdateWebservice]) {
            
            //  [self performSelector:@selector(reloadTables) withObject:nil afterDelay:0.01];
            [self reloadTables]; // comment by vs for repeat filters.. 31 Aug 18...
            
        }else{
            
            [GlobalFunction removeIndicatorView];
            
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"BUY TIMEE" message:@"Failed to update, please try again." delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Retry", nil];
            
            alertView.tag = 999;
            
            [alertView show];
            
        }
        
    }
    
    ////************Filter Data Handling work end**********// 09 aprl 17
    
    

    
}

-(void)updateWebServiceHandling{
    
    if (![self checkForInternet]) {
        
        [self performSelector:@selector(reloadTables) withObject:nil afterDelay:0.2];
        
        return;
    }

    if ([[GlobalFunction shared] callUpdateWebservice]) {
        
        [self performSelector:@selector(reloadTables) withObject:nil afterDelay:0.2];
        
    }else{
        
        [GlobalFunction removeIndicatorView];
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"BUY TIMEE" message:@"Failed to update, please try again." delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Retry", nil];
        
        alertView.tag = 999;
        
        [alertView show];
        
    }

}

-(BOOL)checkForInternet{
    
    BOOL isConnected = true;
    
    if (![[WebService shared] connected]) {
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"BUY TIMEE" message:@"Can't connect to internet, update not successful. Please check your internet connection then try again." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        [alertView show];
        
        isConnected = false;
        
    }
    
    return isConnected;
}

-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex{
    
    if (alertView.tag == 999) {
        
        if (buttonIndex == [alertView cancelButtonIndex]) {
            
            [self performSelector:@selector(reloadTables) withObject:nil afterDelay:0.2];
            
        }else{
            
            [GlobalFunction addIndicatorView];
            
            [self performSelector:@selector(updateWebServiceHandling) withObject:nil afterDelay:0.2];
            
        }
        
    }
    
    if (alertView.tag == 1001)
    {
        if (buttonIndex == [alertView cancelButtonIndex])
        {
           // [txtActiveField resignFirstResponder];
            [GlobalFunction removeIndicatorView];
            
        }else{
            
            [GlobalFunction addIndicatorView];
            
            [self performFilter];
            
        }

        
    }
    if (alertView.tag == 1003)
    {
        if (buttonIndex == [alertView cancelButtonIndex])
        {
            // [txtActiveField resignFirstResponder];
            //[GlobalFunction removeIndicatorView];
            
        }else{
            
           // [GlobalFunction addIndicatorView];
            
           // [self performFilter];
            
            CATransition *transitionAnimation = [CATransition animation];
            [transitionAnimation setType:kCATransitionPush];
            [transitionAnimation setSubtype:kCATransitionFromLeft];
            [transitionAnimation setDuration:0.4f];
            [transitionAnimation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
            [transitionAnimation setFillMode:kCAFillModeBoth];
            
            
            
            //  [myAppDelegate.activeTaskVCObj.viewTaskDetailHolder.layer addAnimation:transitionAnimation forKey:@"changeView"];
            
            [myAppDelegate.vcObj.viewLoginContainer.layer addAnimation:transitionAnimation forKey:@"changeView"];
            
            
            for(UIView *view in myAppDelegate.vcObj.viewLoginContainer.subviews){
                
                [view removeFromSuperview];
                
            }
            
            
            for(UIView *view in myAppDelegate.vcObj.viewHolderWithHeader.subviews){
                
                [view removeFromSuperview];
                
            }
            
            [myAppDelegate.vcObj performSegueWithIdentifier:@"login" sender:myAppDelegate.loginVCObj];
            myAppDelegate.vcObj.btnBack.hidden = YES;
            myAppDelegate.vcObj.btnSideBarOpen.hidden = YES;
            // myAppDelegate.vcObj = nil;
            myAppDelegate.viewAllTaskVCObj = nil;
            myAppDelegate.stringNameOfChildVC = @"";
            myAppDelegate.TandC_Buyee =@"";
            myAppDelegate.isFromCreateBioOnTnC = false;

            
        }
        
        
    }
}



-(void)reloadTables{
    
    NSLog(@"count of array task detail is...%lu",(unsigned long)myAppDelegate.arrayTaskDetail.count);
    
    if (!(myAppDelegate.arrayTaskDetail.count>0)) {

        self.tableTaskListing.hidden = YES;
        
        self.lblNoTaskInfo.hidden = NO;
        self.lblNoTaskInfo.alpha = 0.0;
        [UIView animateWithDuration:0.3 animations:^{
            self.lblNoTaskInfo.alpha = 1.0;
        }];
        
    }else{
        
        NSLog(@"%@",myAppDelegate.userData.userid);
        NSLog(@"%ld",(long)TMViewAllTask);

        
        
        arrayOfTaskWithStatus0 = [GlobalFunction filterTaskForUserID:myAppDelegate.userData.userid taskStatus:TMViewAllTask];
        
        [self.tableTaskListing layoutIfNeeded];
        [self.tableTaskListing reloadData];
        [self.tableTaskListing layoutIfNeeded];
        
        if (arrayOfTaskWithStatus0.count>0) {
            self.tableTaskListing.hidden = NO;
            self.lblNoTaskInfo.hidden = YES;
        }else{
            self.tableTaskListing.hidden = YES;
            self.lblNoTaskInfo.hidden = NO;
            self.lblNoTaskInfo.alpha = 0.0;
            [UIView animateWithDuration:0.3 animations:^{
                self.lblNoTaskInfo.alpha = 1.0;
            }];
        }
        
    }
    
//    if (!myAppDelegate.isLoginComplete) {
//        myAppDelegate.vcObj.lblNoTaskInfoAvailable.hidden = YES;
//    }
    
    [self.tableTaskListing layoutIfNeeded];
    [self.tableTaskListing reloadData];
    [self.tableTaskListing layoutIfNeeded];
    
    if ([filterPriceMax isEqualToString:@""] && [filterPriceMin isEqualToString:@""] && [filterRange isEqualToString:@""] && [filterCategoryId isEqualToString:@""] && [filterLocationName isEqualToString:@""] && [filterSubCategoryId isEqualToString:@""] && dateStart == nil) {
        
        [self performSelector:@selector(removeIndicator) withObject:nil afterDelay:0.2];

    }else{
        
       [self filterList]; //vsvsvsvsvsvsvsvsvs
        
    }

    
}

-(void)completeAfterLoginWork{
    
    myAppDelegate.isLoginComplete = true;
    
  //  [self performSelector:@selector(performUpdateWork) withObject:nil afterDelay:0.2];
    
    myAppDelegate.loginVCObj = nil;
    myAppDelegate.registrationVCObj = nil;
    myAppDelegate.forgotPassVCObj = nil;

}

-(void)filterList{
    
    [self.view layoutIfNeeded];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    
    
    
    arrayOfTaskWithStatus0 = [GlobalFunction filterTaskListOnBasisiOfCategoryId:filterCategoryId SubCategoryId:filterSubCategoryId range:filterRange location:filterLocationName priceMin:filterPriceMin priceMax:filterPriceMax startDate:[formatter stringFromDate: dateStart] endDate:[formatter stringFromDate: dateEnd]];
    
   
    
    if (!(arrayOfTaskWithStatus0.count>0))
    {
    
    if (myAppDelegate.isLocationApiFail == true)
    {
        
        
         [self performSelector:@selector(removeIndicator) withObject:nil afterDelay:0.1];
        //[GlobalFunction removeIndicatorView];
       // [self.view resignFirstResponder];
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"BUY TIMEE" message:@"It seems something went wrong, Please retry" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Retry", nil];
        
        alertView.tag = 1001;
        
        [alertView show];
        
        
        return;
    }
    }
    else
    {
         myAppDelegate.lastSearchLocation = enteredLocation;
        //enteredLocation = myAppDelegate.lastSearchLocation;
    }
    if (self.constraintAdjustRadiusHeight.constant != 36) {
        
    //   [self openAdjustRadiusTableView:tapGestureAdjustRadius]; // need in future to set on new radious filter...
        
    }
    if (self.constraintAdjustPriceRangeHeight.constant != 40) {
        
        // [self openPriceRangeView:tapGesturePriceRange]; // need in future to set on new price filter...
        
    }
    
    [self.tableTaskListing layoutIfNeeded];
    [self.tableTaskListing reloadData];
    [self.tableTaskListing layoutIfNeeded];
    
    if (arrayOfTaskWithStatus0.count>0) {
        self.tableTaskListing.hidden = NO;
        self.lblNoTaskInfo.hidden = YES;
    }else{
        self.tableTaskListing.hidden = YES;
        self.lblNoTaskInfo.hidden = NO;
        self.lblNoTaskInfo.alpha = 0.0;
        [UIView animateWithDuration:0.3 animations:^{
            self.lblNoTaskInfo.alpha = 1.0;
        }];
    }
    
    [self.tableTaskListing layoutIfNeeded];
    [self.tableTaskListing reloadData];
    [self.tableTaskListing layoutIfNeeded];
    
    [self performSelector:@selector(removeIndicator) withObject:nil afterDelay:0.2];
}
-(void)showIndicatorWithDelay{
    
    [GlobalFunction addIndicatorView];
    
}
-(void)removeIndicator{
    
    [GlobalFunction removeIndicatorView];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)hideOpenView:(UITapGestureRecognizer *)gesture{
    
    [self openPriceRangeView:tapGesturePriceRange];
    
}

-(void)openPriceRangeView:(UITapGestureRecognizer *)gesture{
    
    [txtActiveField resignFirstResponder];
    
    [self.view layoutIfNeeded];
    
    if (self.constraintAdjustPriceRangeHeight.constant == 40) {
        
        self.lblNoTaskInfo.hidden = YES;
        
        if (self.constraintAdjustRadiusHeight.constant != 36) {
            
          //  [self openAdjustRadiusTableView:tapGestureAdjustRadius]; //// need in future to set on new radious filter...
            
        }
        
        self.textFieldPriceRangeMax.text = @"";
        self.textFieldPriceRangeMin.text = @"";

      //  self.constraintAdjustPriceRangeHeight.constant = 128; //comment by vs to set it's height 0 on 31 jan;
        
        //**************************** lbl price range animation work 18Apr2016
        
        [UIView animateWithDuration:0.5 animations:^{
            
            //***********   x positon set for label after animation work 20April
            self.priceRangeLbl.transform = CGAffineTransformMakeScale(1.1, 1.1);
            CGRect frame = self.priceRangeLbl.frame;
            frame.origin.x=20;
            self.priceRangeLbl.frame=frame;
            //***********
            
            [minLbl setFont: [minLbl.font fontWithSize: 13]];
            [maxLbl setFont: [maxLbl.font fontWithSize: 13]];
            
            [minLbl setFrame:CGRectMake(28, 76, 116, 36)];
            
            [lblDashForMinAndMax setFrame:CGRectMake(140, 32, 4, 2)];

            lblDashForMinAndMax.hidden = YES;
            
            if(IS_IPHONE_4_OR_LESS){
                [maxLbl setFrame:CGRectMake(156, 76, 116, 36)];
            }
            else if(IS_IPHONE_5 || IS_IPHONE_4_OR_LESS){
                [maxLbl setFrame:CGRectMake(156, 76, 116, 36)];
            }
            else if(IS_IPHONE_6){
                [maxLbl setFrame:CGRectMake(180, 76, 116, 36)];
            }
            else  if (IS_IPHONE_6P){
                [maxLbl setFrame:CGRectMake(190, 76, 116, 36)];
            }
            
            minLbl.textColor=[UIColor blackColor];
            maxLbl.textColor=[UIColor blackColor];
        
        } completion:^(BOOL finished) {
            
            minLbl.hidden=YES;
            maxLbl.hidden=YES;
            
            self.textFieldPriceRangeMax.text = filterPriceMax;   // from filter values show values on text field when open again
            self.textFieldPriceRangeMin.text = filterPriceMin;
            
        }];
        
        //***************************
        
        self.viewBackground.hidden = false;
        
        self.viewPriceRangeSelector.hidden = false;
        
        self.viewBackground.alpha = 0.0;
        
        self.viewPriceRangeSelector.alpha = 0.0;
        
        [GlobalFunction rotateView:self.imgViewPlaceHolderArrowForPriceRange atAngle:-90];

        [UIView animateWithDuration:0.5 animations:^{
            
            self.viewBackground.alpha = 0.7;
            
            self.viewPriceRangeSelector.alpha = 1.0;
            
            [self.view layoutIfNeeded];
            
        } completion:^(BOOL finished) {
            

        }];
        
    }else{
        
        if ([self.textFieldPriceRangeMin.text isEqualToString:@""] && [self.textFieldPriceRangeMax.text isEqualToString:@""]) {
            
            self.priceRangeLbl.font = [self.priceRangeLbl.font fontWithSize:17.0];
            self.priceRangeLblTop.constant = 10;
            minLbl.text = @"";
            maxLbl.text = @"";
            
            lblDashForMinAndMax.hidden = true;
            
        }else if ([self.textFieldPriceRangeMin.text isEqualToString:@""]){
            
            minLbl.font = [self.priceRangeLbl.font fontWithSize:13.0];
            maxLbl.font = [self.priceRangeLbl.font fontWithSize:13.0];
            
            minLblWidth = 0.0;
            minLbl.text=@"0";
            maxLbl.text = self.textFieldPriceRangeMax.text;
            
            minLblWidth = [minLbl.text sizeWithFont:[UIFont systemFontOfSize:13]].width;
            maxLblWidth = [maxLbl.text sizeWithFont:[UIFont systemFontOfSize:13]].width;
            
            minLbl.textColor=[UIColor blackColor];
            maxLbl.textColor=[UIColor blackColor];
            
            lblDashForMinAndMax.hidden = false;
            
            [self.viewPriceRange addSubview:minLbl];
            [self.viewPriceRange addSubview:maxLbl];
            [self.viewPriceRange addSubview:lblDashForMinAndMax];
            
            self.priceRangeLblTop.constant=0;
            
        }else{
            
            minLbl.text=self.textFieldPriceRangeMin.text;
            maxLbl.text=self.textFieldPriceRangeMax.text;
            
            minLbl.font = [self.priceRangeLbl.font fontWithSize:13.0];
            maxLbl.font = [self.priceRangeLbl.font fontWithSize:13.0];
            
            minLblWidth = [minLbl.text sizeWithFont:[UIFont systemFontOfSize:13]].width;
            maxLblWidth = [maxLbl.text sizeWithFont:[UIFont systemFontOfSize:13]].width;
            
            if ([self.textFieldPriceRangeMax.text isEqualToString:@""]) {
                
                lblDashForMinAndMax.hidden = true;
                
            }else{
                
                lblDashForMinAndMax.hidden = false;
                
            }
            
            [self.viewPriceRange addSubview:minLbl];
            [self.viewPriceRange addSubview:maxLbl];
            [self.viewPriceRange addSubview:lblDashForMinAndMax];
            self.priceRangeLblTop.constant=0;
            
        }
        
       // self.constraintAdjustPriceRangeHeight.constant = 40; //comment by vs to set it's height 0 on 31 jan;
        
        [GlobalFunction rotateView:self.imgViewPlaceHolderArrowForPriceRange atAngle:0];
        
        //******************************** lbl animation work 18Apr2016
        minLbl.hidden=false;
        maxLbl.hidden=false;

        [UIView animateWithDuration:0.5 animations:^{
            
            self.viewBackground.alpha = 0.0;
            
            self.viewPriceRangeSelector.alpha = 0.0;
        
            if ([self.textFieldPriceRangeMin.text isEqualToString:@""] && [self.textFieldPriceRangeMax.text isEqualToString:@""]) {
                
                //***********   x positon set for label after animation work 20April
                self.priceRangeLbl.transform = CGAffineTransformMakeScale(1.0,1.0);
                //***********

            }else if ([self.textFieldPriceRangeMin.text isEqualToString:@""]){
                
                //***********   x positon set for label after animation work 20April
                self.priceRangeLbl.transform = CGAffineTransformMakeScale(0.8, 0.8);
                //***********

                minLbl.frame = CGRectMake(25, 20, minLblWidth, 19);
                lblDashForMinAndMax.frame = CGRectMake(minLblWidth + 4 + minLbl.frame.origin.x, 29, 4, 2);
                [maxLbl setFrame:CGRectMake(lblDashForMinAndMax.frame.origin.x+8, 20, maxLblWidth, 19)];
            }else{
                
                //***********   x positon set for label after animation work 20April
                self.priceRangeLbl.transform = CGAffineTransformMakeScale(0.8, 0.8);
                //***********

                minLbl.frame = CGRectMake(25, 20, minLblWidth, 19);
                lblDashForMinAndMax.frame = CGRectMake(minLblWidth + 4 + minLbl.frame.origin.x, 29, 4, 2);
                [maxLbl setFrame:CGRectMake(lblDashForMinAndMax.frame.origin.x+8, 20, maxLblWidth, 19)];
                self.priceRangeLblTop.constant=0;
                
            }

            [self.view layoutIfNeeded];

            CGRect frame = self.priceRangeLbl.frame;
            frame.origin.x=20;
            self.priceRangeLbl.frame=frame;
            
            self.textFieldPriceRangeMax.text = @"";
            self.textFieldPriceRangeMin.text = @"";
            
        } completion:^(BOOL finished) {
            
            minLbl.textColor=[UIColor colorWithRed:171.0/255.0 green:171.0/255.0 blue:171.0/255.0 alpha:1.0];
            maxLbl.textColor=[UIColor colorWithRed:171.0/255.0 green:171.0/255.0 blue:171.0/255.0 alpha:1.0];
            
            self.viewBackground.hidden = true;
            
            self.viewPriceRangeSelector.hidden = true;
            
            [self.tableTaskListing layoutIfNeeded];
            [self.tableTaskListing reloadData];
            [self.tableTaskListing layoutIfNeeded];
            
            if (arrayOfTaskWithStatus0.count>0) {
                self.tableTaskListing.hidden = NO;
                self.lblNoTaskInfo.hidden = YES;
            }else{
                self.tableTaskListing.hidden = YES;
                self.lblNoTaskInfo.hidden = NO;
                self.lblNoTaskInfo.alpha = 0.0;
                [UIView animateWithDuration:0.3 animations:^{
                    self.lblNoTaskInfo.alpha = 1.0;
                }];
            }
            
//            CGRect frame = self.priceRangeLbl.frame;
//            frame.origin.x=20;
//            self.priceRangeLbl.frame=frame;

            self.textFieldPriceRangeMax.text = filterPriceMax;
            self.textFieldPriceRangeMin.text = filterPriceMin;

        }];
        
        
        //********************************
        
    }
    
}


-(void)openAdjustRadiusTableView:(UITapGestureRecognizer *)gesture{
    
    [txtActiveField resignFirstResponder];
    
    [self.view layoutIfNeeded];
    
    if (self.constraintAdjustRadiusHeight.constant == 36) {
        
        if (self.constraintAdjustPriceRangeHeight.constant != 40) {
            
          //  [self openPriceRangeView:tapGesturePriceRange]; //// need in future to set on new price filter...
            
        }
        
        self.lblNoTaskInfo.hidden = YES;

      //  self.constraintAdjustRadiusHeight.constant = height*4;
        
      //  self.constraintViewDistanceRangeHeight.constant = 107+((height*4)-36); //comment by vs to set it's height 0 on 31 jan;
        
        [GlobalFunction rotateView:self.imgViewPlaceHolderArrowForWithinXMiles atAngle:-90];
        
        self.lblWithinXMiles.text = [NSString stringWithFormat:@"Within %ld Miles",(long)[filterRange integerValue]];
        
        [UIView animateWithDuration:0.5 animations:^{
            
            [self.view layoutIfNeeded];
            
            [self.tableRadius layoutIfNeeded];
            [self.tableRadius reloadData];
            [self.tableRadius layoutIfNeeded];

        } completion:^(BOOL finished) {
            
            self.lblWithinXMiles.hidden = YES;
            self.viewAdjustRadiusTapHolder.hidden = YES;
            self.textFieldRadiusFilter.hidden = NO;
            self.textFieldRadiusFilter.text = filterRange;
            [self.textFieldRadiusFilter becomeFirstResponder];
            
        }];
        
    }else{
        
        self.constraintAdjustRadiusHeight.constant = 36;
        
      //  self.constraintViewDistanceRangeHeight.constant = 107; //comment by vs to set it's height 0 on 31 jan;
        
        [GlobalFunction rotateView:self.imgViewPlaceHolderArrowForWithinXMiles atAngle:0];
        
        if (isFromRadiusTableTap) {
            
        }else{
            
            if ([filterRange isEqualToString:@""]) {
                self.lblWithinXMiles.text = @"All";
            }else{
                self.lblWithinXMiles.text = [NSString stringWithFormat:@"Within %ld Miles",(long)[filterRange integerValue]];
            }
            
        }
        
        isFromRadiusTableTap = false;

        self.textFieldRadiusFilter.hidden = YES;
        self.lblWithinXMiles.hidden = NO;
        self.viewAdjustRadiusTapHolder.hidden = NO;

        [UIView animateWithDuration:0.5 animations:^{
            
            [self.view layoutIfNeeded];
            
        } completion:^(BOOL finished) {
    
            [self.tableTaskListing layoutIfNeeded];
            [self.tableTaskListing reloadData];
            [self.tableTaskListing layoutIfNeeded];
            
            if (arrayOfTaskWithStatus0.count>0) {
                self.tableTaskListing.hidden = NO;
                self.lblNoTaskInfo.hidden = YES;
            }else{
                self.tableTaskListing.hidden = YES;
                self.lblNoTaskInfo.hidden = NO;
                self.lblNoTaskInfo.alpha = 0.0;
                [UIView animateWithDuration:0.3 animations:^{
                    self.lblNoTaskInfo.alpha = 1.0;
                }];
            }

        }];
        
    }
    
}


-(void)openCategoryTableView:(UITapGestureRecognizer *)gesture{
    
    NSLog(@"height of category view...%@",self.constraintViewCategoryHeight);
    
    [self.view layoutIfNeeded];
    
    if (self.constraintViewCategoryHeight.constant == 41 || self.constraintViewCategoryHeight.constant == 48 || self.constraintViewCategoryHeight.constant == 37) {
        
        if (self.constraintAdjustRadiusHeight.constant != 36) {
            
          //  [self openAdjustRadiusTableView:tapGestureAdjustRadius]; //// need in future to set on new radious filter...
            
        }
        if (self.constraintAdjustPriceRangeHeight.constant != 40) {
            
           // [self openPriceRangeView:tapGesturePriceRange]; //// need in future to set on new price filter...
            
        }
        
        self.lblNoTaskInfo.hidden = YES;

        self.lblSeperatorCategory.hidden = NO;

        self.constraintViewCategoryHeight.constant = SCREEN_HEIGHT - 72;
        
     //   self.constraintViewDistanceRangeHeight.constant = 0; //comment by vs to set it's height 0 on 31 jan;
        
       // self.constraintAdjustPriceRangeHeight.constant = 0; //comment by vs to set it's height 0 on 31 jan;
       
        self.lblPlaceHolderCategory.text = categoryName;
        
        [GlobalFunction rotateView:self.imgViewPlaceHolderArrow atAngle:-90];
        
        [self.tableCategoryListing layoutIfNeeded];
        [self.tableCategoryListing reloadData];
        [self.tableCategoryListing layoutIfNeeded];
        
        self.tableCategoryListing.hidden = NO;
        
        self.tableCategoryListing.alpha = 0.0;
        
        [UIView animateWithDuration:0.5 animations:^{
            
            [self.view layoutIfNeeded];
            
            self.tableCategoryListing.alpha = 1.0;
            
        } completion:^(BOOL finished) {
            
            
        }];
        
         [self checkSelectedBtnsForHideFilterFromMenuPress];
        
        
    }else{
        if (IS_IPHONE_6P) {
            self.constraintViewCategoryHeight.constant = 48;
        }
        if (IS_IPHONE_6) {
            self.constraintViewCategoryHeight.constant = 41;
        }
        if (IS_IPHONE_5 || IS_IPHONE_4_OR_LESS) {
            self.constraintViewCategoryHeight.constant = 37;
        }
        
        
      //  self.constraintViewDistanceRangeHeight.constant = 107; //comment by vs to set it's height 0 on 31 jan;
        
      //  self.constraintAdjustPriceRangeHeight.constant = 40; //comment by vs to set it's height 0 on 31 jan;
       
        self.lblPlaceHolderCategory.text = categoryName;
        
        [GlobalFunction rotateView:self.imgViewPlaceHolderArrow atAngle:0];
        
        self.tableCategoryListing.alpha = 1.0;
        
        [UIView animateWithDuration:0.5 animations:^{
            
            [self.view layoutIfNeeded];
            
            self.tableCategoryListing.alpha = 0.0;
            
        } completion:^(BOOL finished) {
            
            self.tableCategoryListing.hidden = YES;
            
            self.lblSeperatorCategory.hidden = NO;

            if (isFromTableTap) {
                
            }else{
                [self checkSelectedBtnsForHideFilterFromMenuPress];
                [self performFilter];
            }

//            filterCategoryId = @"";
//            filterSubCategoryId = @"";
            isFromTableTap = false;
//            categoryName = @"Category";

            [self.tableTaskListing layoutIfNeeded];
            [self.tableTaskListing reloadData];
            [self.tableTaskListing layoutIfNeeded];
            
            if (arrayOfTaskWithStatus0.count>0) {
                self.tableTaskListing.hidden = NO;
                self.lblNoTaskInfo.hidden = YES;
            }else{
                self.tableTaskListing.hidden = YES;
                self.lblNoTaskInfo.hidden = NO;
                self.lblNoTaskInfo.alpha = 0.0;
                [UIView animateWithDuration:0.3 animations:^{
                    self.lblNoTaskInfo.alpha = 1.0;
                }];
            }

        }];
        
    }
    
}



- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if (tableView == self.tableTaskListing) {
        NSLog(@"arrayOfTaskWithStatus0 count is...%lu",(unsigned long)arrayOfTaskWithStatus0.count);
       
        if (arrayOfTaskWithStatus0.count > 0)
        {
            return arrayOfTaskWithStatus0.count +1;

        }
        else
        {
            return arrayOfTaskWithStatus0.count ;

        }
    }else if (tableView == self.tableCategoryListing) {
        
        NSLog(@"arrayCategoryDetail count is...%lu",(unsigned long)myAppDelegate.arrayCategoryDetail.count);
        return myAppDelegate.arrayCategoryDetail.count+1;
        
    }else if (tableView == self.tableRadius){
        if (isFromSearchResult) {
             NSLog(@"searchResultArrayFromRadius count is...%lu",(unsigned long) searchResultArrayFromRadius.count);
            return searchResultArrayFromRadius.count + 1;
        }else{
            NSLog(@"arrRadius count is...%lu",(unsigned long)arrRadius.count);
            return arrRadius.count+1;
        }
    }
    else if (tableView == self.cityNameTableView) {
        
        NSLog(@"arrRadius count is...%lu",(unsigned long)arrRadius.count);
        return FilterCities.count;
        
    }
    
    return 3;
}

-(void) updateCons
{
   // TaskDetailTableViewCell *cell = (TaskDetailTableViewCell *)[self.tableTaskListing dequeueReusableCellWithIdentifier:@"cellTaskDetail"];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *celltbl;
    
    if (tableView == self.tableCategoryListing) {
        
        CategoryTableViewCell *cell;
       // [cell.lblCategory setFont:[UIFont fontWithName:@"Helvetica Neue" size:13]];

        if (indexPath.row == 0) {
            
            cell = (CategoryTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"categoryListing"];
            
            cell.lblCategory.text = @"All Categories";
            

        }else{
           
            if ([[myAppDelegate.arrayCategoryDetail objectAtIndex:indexPath.row - 1] isKindOfClass:[category class]]) {
                
                cell = (CategoryTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"categoryListing"];
                
                category *obj = [myAppDelegate.arrayCategoryDetail objectAtIndex:indexPath.row - 1];
                
                cell.lblCategory.text = obj.name;
                
            }else if ([[myAppDelegate.arrayCategoryDetail objectAtIndex:indexPath.row - 1] isKindOfClass:[subCategory class]]){
                
                cell = (CategoryTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"categorySubListing"];
                
                subCategory *obj = [myAppDelegate.arrayCategoryDetail objectAtIndex:indexPath.row - 1];
                
                cell.lblCategory.text = obj.name;
                
            }

        }
        
        if ([indexPath compare:lastSelectedIndexPath] == NSOrderedSame){
            cell.imgViewCheckMark.hidden = false;
        }else{
            cell.imgViewCheckMark.hidden = true;
        }
        
        return cell;
        
    }
    else if(tableView == self.tableTaskListing){
        
        if (indexPath.row < arrayOfTaskWithStatus0.count) {
            
        
        
        TaskDetailTableViewCell *cell = (TaskDetailTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"cellTaskDetail"];
        
        task *taskObj = (task *)[arrayOfTaskWithStatus0 objectAtIndex:indexPath.row];
        
        cell.labelUnderlineCell.backgroundColor=[UIColor colorWithRed:196.0/255.0f green:196.0/255.0f blue:196.0/255.0f alpha:1.0];

        
        for (int i = 0; i<myAppDelegate.arrayCategoryDetail.count; i++) {
            
            if ([[myAppDelegate.arrayCategoryDetail objectAtIndex:i] isKindOfClass:[subCategory class]]){
                
                subCategory *obj = [myAppDelegate.arrayCategoryDetail objectAtIndex:i];
                
                if ([obj.subCategoryId isEqualToString:taskObj.subcategoryId]) {
                    
                    cell.lblTaskEmployerName.text = obj.name;

                }
                
            }

        }
        
        cell.imgViewUserProfile.tag = indexPath.row;
        cell.imgViewUserProfile.image = imageUserDefault;
        
        //adjust radius tap gesture
        self.imgViewTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(animateImageViewToFullScreen:)];
        self.imgViewTapGesture.delegate = self;
        self.imgViewTapGesture.numberOfTapsRequired = 1;
        [cell.imgViewUserProfile addGestureRecognizer:self.imgViewTapGesture];

         [self.view layoutIfNeeded];
        cell.lblAt.textColor=[UIColor colorWithRed:63.0/255.0f green:67.0/255.0f blue:70.0/255.0f alpha:1.0];
        
        if (IS_IPHONE_5 || IS_IPHONE_4_OR_LESS) {
            
            
            
            cell.lblTaskName.font=[cell.lblTaskName.font fontWithSize:16];
            cell.lblTaskPrice.font= [UIFont fontWithName:@"Lato-Bold" size:14];
            cell.lblAddress.font=[cell.lblTaskName.font fontWithSize:14];
            cell.lblAt.font=[cell.lblTaskName.font fontWithSize:15];
            cell.lblTaskDate.font=[cell.lblTaskName.font fontWithSize:14];
            cell.consTaskNameWidth.constant = 190;
            cell.lblTaskName.numberOfLines=0;
            NSLog(@"Height of radius button ...... %f",_btnRadius.frame.size.height);
            cell.consLblTaskNameTop.constant=8;
            cell.consImageViewRightArrowTop.constant=22;
            cell.consLabelTaskPriceBottom.constant=14;
            cell.consCellImageViewWidth.constant=63;
            cell.consCellImageViewHeight.constant=65;
            cell.consImageViewTop.constant=10;
            cell.consImageviewLeft.constant=6;
            cell.consLblTaskNmaeLeft.constant=16;
            cell.consLblTaskDateLeft.constant=16;
            cell.consLblAtLeft.constant=10;
            cell.consTaskPriceRight.constant=2;
            cell.consLabelTaskPriceBottom.constant=12;
            cell.consAddressLblLeft.constant=-1;
            cell.consLblDataTop.constant=4;
            
            
        }
        else if (IS_IPHONE_6)
        {
            
            cell.imgViewUserProfile.layer.cornerRadius = cell.imgViewUserProfile.frame.size.height/2;
            cell.imgViewUserProfile.layer.masksToBounds = YES;
            
            cell.consLblTaskNameTop.constant=10;
            cell.consImageViewRightArrowTop.constant=25;
            cell.consLabelTaskPriceBottom.constant=19;
            cell.consTaskPriceRight.constant=5;
            
            cell.consLblTaskNmaeLeft.constant=15;
            cell.consLblTaskDateLeft.constant=15;
            cell.consLblAtLeft.constant=12;
            cell.consAddressLblLeft.constant=0;
            
            cell.consLblAtTop.constant = 3;
            cell.consLblAddressTop.constant = 5;
            cell.consLblDataTop.constant = 7;
            
            cell.consCellImageViewWidth.constant=75;
            cell.consCellImageViewHeight.constant=75;
            cell.consImageViewTop.constant=14;
            cell.consImageviewLeft.constant=9;
            
            cell.lblTaskName.font=[cell.lblTaskName.font fontWithSize:15]; //18
            cell.lblAt.font=[cell.lblAt.font fontWithSize:14];  //16
            cell.lblAddress.font=[cell.lblAddress.font fontWithSize:13]; //16
            cell.lblTaskDate.font=[cell.lblTaskDate.font fontWithSize:13];  //16
            cell.consLblTaskPriceWidth.constant=75;
            cell.lblTaskPrice.font= [UIFont fontWithName:@"Lato-Bold" size:13];
            cell.consTaskNameWidth.constant = 240;

            
        }
        else if (IS_IPHONE_6P)
        {
//            cell.lblTaskName.font=[cell.lblTaskName.font fontWithSize:20];
//            cell.lblAt.font=[cell.lblAt.font fontWithSize:18];
//            cell.lblAddress.font=[cell.lblAddress.font fontWithSize:18];
//            cell.lblTaskDate.font=[cell.lblTaskDate.font fontWithSize:18];
//            cell.lblTaskPrice.font=[cell.lblTaskPrice.font fontWithSize:19];
            
            
            cell.lblTaskName.font=[cell.lblTaskName.font fontWithSize:16];
            cell.lblAt.font=[cell.lblAt.font fontWithSize:15];
            cell.lblAddress.font=[cell.lblAddress.font fontWithSize:14];
            cell.lblTaskDate.font=[cell.lblTaskDate.font fontWithSize:14];
            cell.lblTaskPrice.font= [UIFont fontWithName:@"Lato-Bold" size:14];
            
            
            cell.imgViewUserProfile.layer.cornerRadius = cell.imgViewUserProfile.frame.size.height/2;
            cell.imgViewUserProfile.layer.masksToBounds = YES;
            
            cell.consLblTaskNameTop.constant=14;
            cell.consImageViewRightArrowTop.constant=27;
            cell.consLabelTaskPriceBottom.constant=20;
            cell.consTaskPriceRight.constant=6;
            
            cell.consLblTaskNmaeLeft.constant=18;
            cell.consLblTaskDateLeft.constant=18;
            cell.consLblAtLeft.constant=14;
            
            cell.consLblAtTop.constant = 7;
            cell.consLblAddressTop.constant = 9;
            cell.consLblDataTop.constant = 10;
            
            cell.consCellImageViewWidth.constant=81;
            cell.consCellImageViewHeight.constant=81;
            cell.consImageViewTop.constant=16;
            cell.consImageviewLeft.constant=9;
            
            cell.consImgRightArrowWIdth.constant = 10;
            cell.consImgRightArrowHeight.constant = 20;
            cell.consLblTaskPriceWidth.constant=80;
            
            cell.consTaskNameWidth.constant=260;  //by kp
            
            
        }
         [self.view layoutIfNeeded];
        NSString *upperString = [taskObj.title uppercaseString];
    
        cell.lblTaskName.text = upperString; //taskObj.title;
        

   //cell.lblTaskPrice.text =@"$200000222";
        NSLog(@"Discount price is....%@",taskObj.discountValue);
        NSLog(@"Fee value is....%@",taskObj.feeValue);
        
        NSString *fees = taskObj.feeValue;
        cell.lblFeePrice.text = fees;
        cell.lblFeePrice.textColor=[UIColor colorWithRed:63.0/255.0f green:67.0/255.0f blue:70.0/255.0f alpha:0.9];
        if ([taskObj.discountValue isEqualToString:@"0"])
        {
            if ([fees isEqualToString:@"0"])
            {
                cell.lblFeePrice.hidden = true;
            }
            else
            {
            cell.lblFeePrice.hidden = false;
            }
            cell.lblTaskPrice.textColor = [UIColor blackColor];
            if ([GlobalFunction checkIfContainFloat:taskObj.price]) {
                cell.lblTaskPrice.text = [NSString stringWithFormat:@"$%0.2f",taskObj.price];
            }else{
                cell.lblTaskPrice.text = [NSString stringWithFormat:@"$%0.0f",taskObj.price];
            }

        }
        else
        {
            cell.lblFeePrice.hidden = true;
            
                cell.lblTaskPrice.textColor = [UIColor colorWithRed:214/255.0 green:52/255.0 blue:47/255.0 alpha:1.0];
            if ([GlobalFunction checkIfContainFloat:taskObj.price]) {
                cell.lblTaskPrice.text = [NSString stringWithFormat:@"$%0.2f off",taskObj.price];
            }else{
                cell.lblTaskPrice.text = [NSString stringWithFormat:@"$%0.0f off",taskObj.price];
            }

        }
        
        NSLog(@"date from server is...%@",taskObj.startDate);
        NSLog(@"time from server is...%@",taskObj.endDate);
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
        [dateFormatter setDateFormat:dateFormatFull];
        
        NSString *timeString = @"";
        NSString *dateString = @"";
        
        if ([taskObj.startDate isEqualToString:@"AnyDate"])
        {
            dateString = @"AnyDate";
        }
        else
        {
            NSDate *date = [dateFormatter dateFromString:taskObj.startDate];
            [dateFormatter setDateFormat:dateFormatDate];
             dateString = [dateFormatter stringFromDate:date];

             dateString = [GlobalFunction convertDateFormat:dateString];
        }
        
        if ([taskObj.endDate isEqualToString:@"AnyTime"])
        {
            timeString = @"AnyTime";
        }
        else
        {
            timeString = [GlobalFunction convertTimeFormat:taskObj.endDate];
        }

        NSLog(@"Date string is....%@",dateString);
        NSLog(@"time string is....%@",timeString);
        
        
        
        NSString *combineString = dateString;
        NSString *str = [@[dateString, timeString] componentsJoinedByString:@", "];
        
        cell.lblTaskDate.text = str;
//        cell.lblAddress.text = taskObj.locationName;
        cell.lblAddress.text = taskObj.businessName; // changed by RP


        [tableView layoutIfNeeded];
        
        [cell layoutIfNeeded];

//
        cell.activityIndicator.hidden=true;
        [cell.activityIndicator stopAnimating];
        
        //************ Download user image *************************************************************************
        
        
        NSLog(@"************* ID: %@ Pic:%@", taskObj.taskid, taskObj.picpath);
        if ([taskObj.picpath isEqualToString:@""]) {
            
            cell.imgViewUserProfile.image = imageUserDefault;
            
            if(taskObj.userImage){
                //cell.imgViewUserProfile.image = taskObj.userImage;
            }else{
                //cell.imgViewUserProfile.image = imageUserDefault;
            }
        }else{
            if (taskObj.userImage) {
                cell.imgViewUserProfile.image = taskObj.userImage;
            }else{
            
                NSArray *components = [taskObj.picpath componentsSeparatedByString:@"/"];
                NSString *imgNameWithJPEG = [components lastObject];
                imgNameWithJPEG = [imgNameWithJPEG stringByAppendingString:@".jpeg"];
                NSArray *componentsWithDots = [imgNameWithJPEG componentsSeparatedByString:@"."];
                if (componentsWithDots.count>2) {
                    imgNameWithJPEG = [NSString stringWithFormat:@"%@.%@",[componentsWithDots firstObject],[componentsWithDots lastObject]];
                }
                
                UIImage *img = [GlobalFunction loadImageOfLogoFromCacheFolder:imgNameWithJPEG dirname:@"user"];
                
                if (img) {
                    //taskObj.userImage = img;
                    cell.imgViewUserProfile.image = img;
                }else{
                    
                    NSString *image = taskObj.picpath;
                    NSString *fullPath=[NSString stringWithFormat:urlServer,image];
                    
                    fullPath=image;
                    NSString* webStringURL = [fullPath stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                    
                    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH,  0ul);
                    dispatch_async(queue, ^{
                        //This is what you will load lazily
                        NSURL *imageURL=[NSURL URLWithString:webStringURL];
                        
                        NSData *images=[NSData dataWithContentsOfURL:imageURL];
                        
                       // NSLog(<#NSString * _Nonnull format, ...#>)
                        NSArray *componentsArr = [image componentsSeparatedByString:@"/"];
                        
                        
                        NSString *imgNameWithJPEG = [componentsArr lastObject];
                        
                        NSArray *componentsArr2 = [imgNameWithJPEG componentsSeparatedByString:@"."];
                        
                        NSString *imgNameWithoutJPEG = [componentsArr2 firstObject];
                        
                        UIImage *img = [UIImage imageWithData:images];
                        
                        if (img) {
                            
                            dispatch_sync(dispatch_get_main_queue(), ^{
                                
                                UIImage *img = [UIImage imageWithData:images];
                                cell.imgViewUserProfile.image=img;
                                [GlobalFunction saveimage:img imageName:imgNameWithoutJPEG dirname:@"user"];
                                
                            });
                            
                        }
                        
                    });

                }

            }
            
        }
        
        //*****************************************************************************
        
        return cell;
           
    }
        else
        {
             UITableViewCell *cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"cellTaskDetail2"];
            return cell;
        }
    }
        else if (tableView == self.tableRadius){
        
        celltbl = [tableView dequeueReusableCellWithIdentifier:@"cell"];
        
        if (indexPath.row == 0) {
            
            celltbl.textLabel.text = [NSString stringWithFormat:@"All"];
            
        }else{
            
            if (isFromSearchResult) {
                
                celltbl.textLabel.text = [NSString stringWithFormat:@"%@ Miles",[searchResultArrayFromRadius objectAtIndex:indexPath.row - 1]];
                
            }else{
                
                celltbl.textLabel.text = [NSString stringWithFormat:@"%@ Miles",[arrRadius objectAtIndex:indexPath.row - 1]];
                
            }
            
        }
        
        celltbl.textLabel.textColor = RGB(185, 185, 185);

    }
    else if (tableView == self.cityNameTableView)
    {
       
          CityNameTableViewCell *cellCity = (CityNameTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"cell"];
        
          cellCity.lblCityName.textColor =  RGB(63, 67, 70);
        
        
        cities *obj;
        obj=[FilterCities objectAtIndex:indexPath.row];
        
        cellCity.lblCityName.text =obj.cityName; //[FilterCities objectAtIndex:indexPath.row];
        
        cellCity.lblCityName.textAlignment = NSTextAlignmentCenter;
        
        return cellCity;
    }
    
    
    return celltbl;
    
  
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (tableView == self.tableCategoryListing) {
        
        isFromTableTap = true;
       [self checkSelectedBtnsForHideFilterFromMenuPress];
        if (indexPath.row == 0) {
            
            filterCategoryId = @"";
            filterSubCategoryId = @"";
            
            categoryName = @"All Categories";
            
            [GlobalFunction addIndicatorView];
            
        }else{
          
            if ([[myAppDelegate.arrayCategoryDetail objectAtIndex:indexPath.row - 1] isKindOfClass:[category class]]) {
                
                category *obj = [myAppDelegate.arrayCategoryDetail objectAtIndex:indexPath.row - 1];
                
                categoryName = obj.name;
                
                filterSubCategoryId = @"";
                filterCategoryId = obj.categoryId;
                
                [GlobalFunction addIndicatorView];
                
            }else if ([[myAppDelegate.arrayCategoryDetail objectAtIndex:indexPath.row - 1] isKindOfClass:[subCategory class]]){
                
                subCategory *obj = [myAppDelegate.arrayCategoryDetail objectAtIndex:indexPath.row - 1];
                
                categoryName = obj.name;
                
                filterCategoryId = @"";
                filterSubCategoryId = obj.subCategoryId;
                
                [GlobalFunction addIndicatorView];
                
            }

        }
        
        CategoryTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        
        cell.imgViewCheckMark.hidden = false;
        
        if (lastSelectedIndexPath) {
            CategoryTableViewCell *cell2 = [tableView cellForRowAtIndexPath:lastSelectedIndexPath];
            
            cell2.imgViewCheckMark.hidden = true;
        }

        //lastSelectedIndexPath = nil;
       // lastSelectedIndexPath = [[NSIndexPath alloc] init];
        lastSelectedIndexPath = indexPath;
        myAppDelegate.SelectedIndexPathForFilter = indexPath;
        [self openCategoryTableView:tapGesture];

        [self performFilter];
      //  [self checkSelectedBtns];
        
    }
    else if(tableView == self.tableTaskListing){
        
        if (indexPath.row < arrayOfTaskWithStatus0.count) 
        {
            
            myAppDelegate.taskData = nil;
            
            myAppDelegate.taskData = [arrayOfTaskWithStatus0 objectAtIndex:indexPath.row];
            
            [GlobalFunction addIndicatorView];
            
            [self performSelector:@selector(fetchTaskInfo) withObject:nil afterDelay:0.1];
            
            // for hide filters if selected...
            [self checkSelectedBtns];
        }
      else
      {
          NSLog(@"right cell selected 34435465563454546456456#$634634++++++++++++++++++++++++++++++++++++++++++");
          
            if (myAppDelegate.isFromSkip)
            {
                
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"BUY TIMEE" message:@"You need to login first." delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Login", nil];
                
                alert.tag = 1003;
                
                [alert show];
                
                return;
                
          }
          else
          {
          
              myAppDelegate.isSideBarAccesible = true;
          
              //    TaskTableViewCell *cell = (TaskTableViewCell *)[myAppDelegate.vcObj.tblViewSideBar cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
              //    [myAppDelegate.vcObj performSegueWithIdentifier:@"viewAllTask" sender:cell];
          
          
              TaskTableViewCell *cell = (TaskTableViewCell *)[myAppDelegate.vcObj.tblViewSideBar cellForRowAtIndexPath:[NSIndexPath indexPathForRow:7 inSection:0]];
              [myAppDelegate.vcObj performSegueWithIdentifier:@"request" sender:cell];
          
              return;
          }
      }
        
    }
    else if (tableView == self.tableRadius){
        
        if (indexPath.row == 0) {
            
            filterRange = @"";
            
            self.lblWithinXMiles.text = @"All";

        }else{
            
            if (isFromSearchResult) {
                
                self.lblWithinXMiles.text = [NSString stringWithFormat:@"Within %@ Miles",[searchResultArrayFromRadius objectAtIndex:indexPath.row - 1]];

                filterRange = [NSString stringWithFormat:@"%ld",(long)[[searchResultArrayFromRadius objectAtIndex:indexPath.row - 1] integerValue]];
                
            }else{
                
                self.lblWithinXMiles.text = [NSString stringWithFormat:@"Within %@ Miles",[arrRadius objectAtIndex:indexPath.row - 1]];
                
                filterRange = [NSString stringWithFormat:@"%ld",(long)[[arrRadius objectAtIndex:indexPath.row - 1] integerValue]];
                
            }
            
        }
        
        isFromRadiusTableTap = true;
        
        [GlobalFunction addIndicatorView];
        
        [self performFilter];
        
    }
    else if (tableView == self.cityNameTableView)
    {
        cities *obj=[FilterCities objectAtIndex:indexPath.row];
        self.maxTxtFld.text=obj.cityName;
        enteredLocation = self.maxTxtFld.text;
        
        if (self.filterCityViewHeight.constant == 180 || self.filterCityViewHeight.constant == 160)
        {
            [self.view layoutIfNeeded];
            
            self.filterCityViewHeight.constant=0;
            
            [UIView animateWithDuration:0.3 animations:^ {
                [self.view layoutIfNeeded];
                
            }];
        }
    }
    
}
-(void)fetchTaskInfo{
    
    if (![[WebService shared] connected]) {
        
        [GlobalFunction removeIndicatorView];
        
        [[GlobalFunction shared] showAlertForMessage:@"Can't connect to internet, please check and try again."];
        
        return;
    }
    
    NSString *timeZoneSeconds = [GlobalFunction getTimeZone];
    
    NSDictionary *dictTaskInfoResponse = [[WebService shared] viewTaskInfoTaskid:myAppDelegate.taskData.taskid userid:myAppDelegate.userData.userid timeZone:timeZoneSeconds view:@""];
    
    if (dictTaskInfoResponse) {
        
        NSLog(@"response message == %@",[EncryptDecryptFile decrypt:[dictTaskInfoResponse valueForKey:@"responseMessage"]]);

        if ([[EncryptDecryptFile decrypt:[dictTaskInfoResponse valueForKey:@"responseCode"]] isEqualToString:@"200"]) {
            
            NSDictionary *dictDataReg = [dictTaskInfoResponse objectForKey:@"data"];
            
            if (dictDataReg != nil) {
                
                if (myAppDelegate.isOfOtherUser) {
                    
                    myAppDelegate.workerOtherUserTaskDetail = nil;
                    
                    myAppDelegate.workerOtherUserTaskDetail = [[WorkerEmployerTaskDetail alloc]init];
                    
                    [self fillWorkerTaskDataForWorkerTaskDataObj:myAppDelegate.workerOtherUserTaskDetail fromDictionary:dictDataReg andUserData:myAppDelegate.userOtherData];
                    
                }else{
                    
                    myAppDelegate.workerAllTaskDetail = nil;
                    
                    myAppDelegate.workerAllTaskDetail = [[WorkerEmployerTaskDetail alloc] init];
                    
                    [self fillWorkerTaskDataForWorkerTaskDataObj:myAppDelegate.workerAllTaskDetail fromDictionary:dictDataReg andUserData:myAppDelegate.userData];
                    
                }
                
                [self performSelector:@selector(performSegueForTaskDetail) withObject:nil afterDelay:0.5];

            }else{
                
                [GlobalFunction removeIndicatorView];
                
                [[GlobalFunction shared] showAlertForMessage:[EncryptDecryptFile decrypt:[dictTaskInfoResponse valueForKey:@"responseMessage"]]];
                
            }
            
        }else{
            
            [GlobalFunction removeIndicatorView];
            
            [[GlobalFunction shared] showAlertForMessage:[EncryptDecryptFile decrypt:[dictTaskInfoResponse valueForKey:@"responseMessage"]]];
            
        }
        
    }else{
        
        [GlobalFunction removeIndicatorView];
        
        [[GlobalFunction shared] showAlertForMessage:@"Server is down, please try again later."];
        
    }
    
}

-(void)performSegueForTaskDetail{
    
    myAppDelegate.isSideBarAccesible = false;
    
    TaskDetailTableViewCell *cell = (TaskDetailTableViewCell *)[self.tableTaskListing cellForRowAtIndexPath:[NSIndexPath indexPathForRow:[arrayOfTaskWithStatus0 indexOfObject:myAppDelegate.taskData] inSection:0]];
    [self performSegueWithIdentifier:@"newTaskDetail" sender:cell];
    
    [GlobalFunction removeIndicatorView];
    
}


-(void)fillWorkerTaskDataForWorkerTaskDataObj:(WorkerEmployerTaskDetail *)obj fromDictionary:(NSDictionary *)dict andUserData:(user *)userObj{
    
    
    NSDictionary *dictTemp0 =nil;
    
    // NSString *mainn = [EncryptDecryptFile decrypt:[dict valueForKey:@"Accepted By"]];
    
    dictTemp0= [dict objectForKey:@"Accepted By"];
    
    
    //
    
    //    if (dictTemp0==nil) {
    //        obj.isTaskAccepted = false;
    //    }else{
    //        obj.isTaskAccepted = true;
    //        obj.userId_acceptedBy = [EncryptDecryptFile decrypt:[dictTemp0 valueForKey:@"userId"]];
    //        obj.emailId_acceptedBy = [EncryptDecryptFile decrypt:[dictTemp0 valueForKey:@"emailId"]];
    //        obj.picPath_acceptedBy = [EncryptDecryptFile decrypt:[dictTemp0 valueForKey:@"picPath"]];
    //    }
    
    
    NSDictionary *purchaseDict = [dict objectForKey:@"Purchase Detail"];
    NSString *purchaseDictString = [dict objectForKey:@"Purchase Detail"];
    
    if(purchaseDict == nil)
    {
    }
    else
    {
      if(![purchaseDictString isEqual:@"7Ho+0BXmb32lzzrB1RRTXQ=="])
      {
        if([purchaseDict valueForKey:@"id"])
        {
           NSString *val1 = [EncryptDecryptFile decrypt:[EncryptDecryptFile decrypt:[purchaseDict valueForKey:@"id"]]];
           obj.purchaseID = [EncryptDecryptFile decrypt:[purchaseDict valueForKey:@"id"]];
        }
      }
    }
    
    NSDictionary *dictTemp3 =nil;
    dictTemp3= [dict objectForKey:@"Reservation Detail"];
    
    if (dictTemp3==nil) {
        
    }else{
        
        
        obj.taskDetailObj = [[task alloc]init];
        
        obj.taskDetailObj.userid = [EncryptDecryptFile decrypt:[dictTemp3 valueForKey:@"userId"]];
        obj.taskDetailObj.categoryId = [EncryptDecryptFile decrypt:[dictTemp3 valueForKey:@"categoryId"]];
        obj.taskDetailObj.paymentMethod = [EncryptDecryptFile decrypt:[dictTemp3 valueForKey:@"paymentMethod"]];
        
        obj.taskDetailObj.title = [EncryptDecryptFile decrypt:[dictTemp3 valueForKey:@"title"]];
        obj.taskDetailObj.taskid = [EncryptDecryptFile decrypt:[dictTemp3 valueForKey:@"reservationId"]];
        //obj.taskDetailObj.subcategoryId = [EncryptDecryptFile decrypt:[dictTemp3 valueForKey:@"subcategoryId"]];
        obj.taskDetailObj.status = [EncryptDecryptFile decrypt:[dictTemp3 valueForKey:@"status"]];
        obj.taskDetailObj.startDate = [EncryptDecryptFile decrypt:[dictTemp3 valueForKey:@"startDate"]];
        obj.taskDetailObj.price = [[EncryptDecryptFile decrypt:[dictTemp3 valueForKey:@"actualPrice"]] floatValue];
        obj.taskDetailObj.longitude = [[EncryptDecryptFile decrypt:[dictTemp3 valueForKey:@"longitude"]] floatValue];
        obj.taskDetailObj.locationName = [EncryptDecryptFile decrypt:[dictTemp3 valueForKey:@"locationName"]];
        obj.taskDetailObj.latitude = [[EncryptDecryptFile decrypt:[dictTemp3 valueForKey:@"latitude"]] floatValue];
        obj.taskDetailObj.endDate = [EncryptDecryptFile decrypt:[dictTemp3 valueForKey:@"startTime"]];
        obj.taskDetailObj.taskDescription = [EncryptDecryptFile decrypt:[dictTemp3 valueForKey:@"description"]];
        obj.taskDetailObj.lastModifyTime = [EncryptDecryptFile decrypt:[dictTemp3 valueForKey:@"lastModifyTime"]];
        obj.taskDetailObj.discountValue = [EncryptDecryptFile decrypt:[dictTemp3 valueForKey:@"discount"]];
        obj.taskDetailObj.discountDiscription = [EncryptDecryptFile decrypt:[dictTemp3 valueForKey:@"discountDescription"]];
        obj.taskDetailObj.soldStatus = [EncryptDecryptFile decrypt:[dictTemp3 valueForKey:@"soldStatus"]];
        
        NSLog(@"sold status is this %@",obj.taskDetailObj.soldStatus);
        obj.taskDetailObj.feeValue = [EncryptDecryptFile decrypt:[dictTemp3 valueForKey:@"fee"]];
        obj.taskDetailObj.taskImagesArray = [[NSMutableArray alloc] init];
        
        NSLog(@"%@",obj.taskDetailObj.userid);
        
        
        myAppDelegate.workerTaskDetail_taskDetailObj_taskid = obj.taskDetailObj.taskid; //update string for rate view.
        myAppDelegate.workerTaskDetail_taskDetailObj_taskName = obj.taskDetailObj.title;
        
        
        
        NSArray *newArray= [dict objectForKey:@"reservation_images"];
        for (NSDictionary *dicts in newArray) {
            
            NSString *imageName=[EncryptDecryptFile decrypt:[dicts valueForKey:@"reservationImg"]];
            
            if(imageName.length<=0)
            {
                
            }
            else
            {
               [obj.taskDetailObj.taskImagesArray addObject:[EncryptDecryptFile decrypt:[dicts valueForKey:@"reservationImg"]]];
            }
            
            
        }

    
    }
    
    
    
    NSDictionary *dictTemp2 =nil;
    dictTemp2= [dict objectForKey:@"Created By"];
    if (dictTemp2==nil) {
        
    }else
    {
        
        obj.userId_createdBy = [EncryptDecryptFile decrypt:[dictTemp2 valueForKey:@"userId"]];
        obj.emailId_createdBy = [EncryptDecryptFile decrypt:[dictTemp2 valueForKey:@"emailId"]];
        obj.picPath_createdBy = [EncryptDecryptFile decrypt:[dictTemp2 valueForKey:@"picPath"]];
        obj.name_createdBy = [EncryptDecryptFile decrypt:[dictTemp2 valueForKey:@"name"]];
        NSLog(@"%@",obj.picPath_createdBy);
        NSLog(@"%@",obj.userId_createdBy);
        
        obj.avg_rating_createdBy = [EncryptDecryptFile decrypt:[dictTemp2 valueForKey:@"avgRating"]];
        myAppDelegate.workerTaskDetail_userId_createdBy = obj.userId_createdBy;// update string for rate view.
        myAppDelegate.workerTaskDetail_name_createdBy = obj.name_createdBy;// update string for rate view.
    }
    
    NSDictionary *dictTemp5 =nil;
    dictTemp5= [dict objectForKey:@"Business"];
    if (dictTemp5==nil) {
        
    }else
    {
        
        obj.name_Business = [EncryptDecryptFile decrypt:[dictTemp5 valueForKey:@"name"]];
        obj.address_Business = [EncryptDecryptFile decrypt:[dictTemp5 valueForKey:@"address"]];
        obj.yelpProfile_Business = [EncryptDecryptFile decrypt:[dictTemp5 valueForKey:@"yelpProfile"]];
        obj.services_Business = [EncryptDecryptFile decrypt:[dictTemp5 valueForKey:@"services"]];
        obj.instaLink_Business = [EncryptDecryptFile decrypt:[dictTemp5 valueForKey:@"instaLink"]];
        
    }
    
    return;
    ///img work by vs
    
  // NSArray * arrayTerms= [dict valueForKey:@"taskImage"];
  //  NSArray *newArray = [arrayTerms objectAtIndex:0];
    
    int i = 0;
    
   // for (NSDictionary *dicts in newArray) {
        
    //    NSArray *componentsArray = [[EncryptDecryptFile decrypt:[dicts valueForKey:@"taskImg"]] componentsSeparatedByString:@"http://192.168.5.134/handapp/"];
        
    //    if (componentsArray.count>1) {
     //       [obj.taskDetailObj.taskImagesArray addObject:[NSString stringWithFormat:urlServer,[componentsArray objectAtIndex:1]]];
     //   }else{
     //       [obj.taskDetailObj.taskImagesArray addObject:[NSString stringWithFormat:urlServer,[componentsArray objectAtIndex:0]]];
     //   }
        
//        NSString *fullPath=[obj.taskDetailObj.taskImagesArray objectAtIndex:i];
//        NSArray *parts = [fullPath componentsSeparatedByString:@"/"];
//        NSString *imageName=[parts lastObject];
//        NSArray *imageNameParts = [imageName componentsSeparatedByString:@"."];
//        
//        NSString *userImageNameWithJPEG = [NSString stringWithFormat:@"%@_%@_%d.jpeg",[imageNameParts firstObject],obj.taskDetailObj.taskid,i];
//        
//        UIImage *img = [GlobalFunction loadImageOfLogoFromCacheFolder:userImageNameWithJPEG dirname:@"taskImages"];
//        
//        if (img) {
//            
//            [obj.taskDetailObj.imagesArray addObject:img];
//            
//        }
//        
//        i = i+1;
//  //  }
//    
//    if (obj.taskDetailObj.taskImagesArray.count != obj.taskDetailObj.imagesArray.count) {
//        [obj.taskDetailObj.imagesArray removeAllObjects];
//    }

    
    if ([userObj.userid isEqualToString:obj.userId_acceptedBy]) {
        
        if ([obj.picPath_createdBy isEqualToString:@""]) {
            obj.userImage_createdBy = imageUserDefault;
        }else{
            NSArray *components = [obj.picPath_createdBy componentsSeparatedByString:@"/"];
            
            NSString *imgNameWithJPEG = [components lastObject];
            
            
            NSArray *componentsWithDots = [imgNameWithJPEG componentsSeparatedByString:@"."];
            
            if (componentsWithDots.count>2) {
                
                imgNameWithJPEG = [NSString stringWithFormat:@"%@.%@",[componentsWithDots firstObject],[componentsWithDots lastObject]];
                
            }
            
            UIImage *img = [GlobalFunction loadImageOfLogoFromCacheFolder:imgNameWithJPEG dirname:@"user"];
            
            if (img) {
                obj.userImage_createdBy = img;
            }else{
                UIImage *img2 = [GlobalFunction getImageFromURL:[NSString stringWithFormat:urlServer,obj.picPath_createdBy]];
                
                if (img2) {
                    obj.userImage_createdBy = img2;
                    
                    NSArray *components2 = [imgNameWithJPEG componentsSeparatedByString:@"."];
                    
                    NSString *imgNameWithoutJPEG = [components2 firstObject];
                    
                    [GlobalFunction saveimage:img2 imageName:imgNameWithoutJPEG dirname:@"user"];
                    
                }else{
                    obj.userImage_createdBy = imageUserDefault;
                }
            }
        }
    }
    else if ([userObj.userid isEqualToString:obj.userId_createdBy]){
        
        if ([obj.picPath_acceptedBy isEqualToString:@""]) {
            obj.userImage_acceptedBy = imageUserDefault;
        }else{
            NSArray *components = [obj.picPath_acceptedBy componentsSeparatedByString:@"/"];
            
            NSString *imgNameWithJPEG = [components lastObject];
            
            
            NSArray *componentsWithDots = [imgNameWithJPEG componentsSeparatedByString:@"."];
            
            if (componentsWithDots.count>2) {
                
                imgNameWithJPEG = [NSString stringWithFormat:@"%@.%@",[componentsWithDots firstObject],[componentsWithDots lastObject]];
                
            }
            
            UIImage *img = [GlobalFunction loadImageOfLogoFromCacheFolder:imgNameWithJPEG dirname:@"user"];
            
            if (img) {
                obj.userImage_acceptedBy = img;
            }else{
                UIImage *img2 = [GlobalFunction getImageFromURL:[NSString stringWithFormat:urlServer,obj.picPath_acceptedBy]];
                
                if (img2) {
                    obj.userImage_acceptedBy = img2;
                    
                    NSArray *components2 = [imgNameWithJPEG componentsSeparatedByString:@"."];
                    
                    NSString *imgNameWithoutJPEG = [components2 firstObject];
                    
                    [GlobalFunction saveimage:img2 imageName:imgNameWithoutJPEG dirname:@"user"];
                    
                }else{
                    obj.userImage_acceptedBy = imageUserDefault;
                }
            }
        }
    }
    
    /// img work by vss
    
    
    

    
    
    /////////////////,..............
    NSArray *arrayTerms= [dict valueForKey:@"Accepted By"];
    if ([[arrayTerms objectAtIndex:0] isKindOfClass:[NSString class]]) {
        
        obj.isTaskAccepted = false;
        
    }else if ([[arrayTerms objectAtIndex:0] isKindOfClass:[NSDictionary class]]){
        
        obj.isTaskAccepted = true;
        
        NSDictionary *dictTemp0=[arrayTerms objectAtIndex:0];
        
        obj.userId_acceptedBy = [EncryptDecryptFile decrypt:[dictTemp0 valueForKey:@"userId"]];
        obj.userName_acceptedBy = [EncryptDecryptFile decrypt:[dictTemp0 valueForKey:@"userName"]];
        obj.description_acceptedBy = [EncryptDecryptFile decrypt:[dictTemp0 valueForKey:@"description"]];
        obj.emailId_acceptedBy = [EncryptDecryptFile decrypt:[dictTemp0 valueForKey:@"emailId"]];
        obj.latitude_acceptedBy = [[EncryptDecryptFile decrypt:[dictTemp0 valueForKey:@"latitude"]] floatValue];
        obj.longitude_acceptedBy = [[EncryptDecryptFile decrypt:[dictTemp0 valueForKey:@"longitude"]] floatValue];
        obj.locationName_acceptedBy = [EncryptDecryptFile decrypt:[dictTemp0 valueForKey:@"location"]];
        obj.mobile_acceptedBy = [EncryptDecryptFile decrypt:[dictTemp0 valueForKey:@"mobile"]];
        obj.picPath_acceptedBy = [EncryptDecryptFile decrypt:[dictTemp0 valueForKey:@"picPath"]];
        obj.avg_rating_acceptedBy = [EncryptDecryptFile decrypt:[dictTemp0 valueForKey:@"avg_rating"]];
        
    }
    
    //task accepted by rating detail
    obj.arrayRatings_acceptedBy = [[NSMutableArray alloc]init];
    arrayTerms= [dict valueForKey:@"Worker Rating Detail"];
    if ([[arrayTerms objectAtIndex:0] isKindOfClass:[NSString class]]) {
        
        obj.rating_acceptedBy = [EncryptDecryptFile decrypt:[arrayTerms objectAtIndex:0]];
        
    }else if ([[arrayTerms objectAtIndex:0] isKindOfClass:[NSDictionary class]]){
        
        NSInteger noOfStars = 0;
        for (NSDictionary *dicts in arrayTerms) {
            
            NSString *stars = [EncryptDecryptFile decrypt:[dicts valueForKey:@"ratingStar"]];
            noOfStars = noOfStars + [stars integerValue];
            
            obj.ratingObj = nil;
            obj.ratingObj = [[rating alloc]init];
            
            obj.ratingObj.ratingdescription = [EncryptDecryptFile decrypt:[dicts valueForKey:@"description"]];
            obj.ratingObj.employerId = [EncryptDecryptFile decrypt:[dicts valueForKey:@"employerId"]];
            obj.ratingObj.ratingId = [EncryptDecryptFile decrypt:[dicts valueForKey:@"ratingId"]];
            obj.ratingObj.ratingStar = [EncryptDecryptFile decrypt:[dicts valueForKey:@"ratingStar"]];
            obj.ratingObj.taskId = [EncryptDecryptFile decrypt:[dicts valueForKey:@"taskId"]];
            obj.ratingObj.workerId = [EncryptDecryptFile decrypt:[dicts valueForKey:@"workerId"]];
            obj.ratingObj.userName = [EncryptDecryptFile decrypt:[dicts valueForKey:@"userName"]];
            
            [obj.arrayRatings_acceptedBy addObject:obj.ratingObj];
        }
        CGFloat avg = noOfStars/arrayTerms.count;
        obj.avgStars_acceptedBy = avg;
    }else if ([[arrayTerms objectAtIndex:0] isKindOfClass:[NSArray class]]){
        
        NSArray *arrRatings = [arrayTerms objectAtIndex:0];
        
        NSInteger noOfStars = 0;
        
        for (NSDictionary *dicts in arrRatings) {
            
            NSString *stars = [EncryptDecryptFile decrypt:[dicts valueForKey:@"ratingStar"]];
            noOfStars = noOfStars + [stars integerValue];
            
            obj.ratingObj = nil;
            obj.ratingObj = [[rating alloc]init];
            
            obj.ratingObj.ratingdescription = [EncryptDecryptFile decrypt:[dicts valueForKey:@"description"]];
            obj.ratingObj.employerId = [EncryptDecryptFile decrypt:[dicts valueForKey:@"employerId"]];
            obj.ratingObj.ratingId = [EncryptDecryptFile decrypt:[dicts valueForKey:@"ratingId"]];
            obj.ratingObj.ratingStar = [EncryptDecryptFile decrypt:[dicts valueForKey:@"ratingStar"]];
            obj.ratingObj.taskId = [EncryptDecryptFile decrypt:[dicts valueForKey:@"taskId"]];
            obj.ratingObj.workerId = [EncryptDecryptFile decrypt:[dicts valueForKey:@"workerId"]];
            obj.ratingObj.userName = [EncryptDecryptFile decrypt:[dicts valueForKey:@"userName"]];
            
            [obj.arrayRatings_acceptedBy addObject:obj.ratingObj];
        }
        
        CGFloat avg = noOfStars/arrayTerms.count;
        obj.avgStars_acceptedBy = avg;
        
    }
    
    //task created by rating detail
    obj.arrayRatings_createdBy = [[NSMutableArray alloc]init];
    arrayTerms= [dict valueForKey:@"Created By Rating Detail"];
    
    if ([[arrayTerms objectAtIndex:0] isKindOfClass:[NSString class]]) {
        
        obj.rating_createdBy = [EncryptDecryptFile decrypt:[arrayTerms objectAtIndex:0]];
        
    }else if ([[arrayTerms objectAtIndex:0] isKindOfClass:[NSDictionary class]]){
        
        NSInteger noOfStars = 0;
        for (NSDictionary *dicts in arrayTerms) {
            
            NSString *stars = [EncryptDecryptFile decrypt:[dicts valueForKey:@"ratingStar"]];
            noOfStars = noOfStars + [stars integerValue];
            
            obj.ratingObj = nil;
            obj.ratingObj = [[rating alloc]init];
            
            obj.ratingObj.ratingdescription = [EncryptDecryptFile decrypt:[dicts valueForKey:@"description"]];
            obj.ratingObj.employerId = [EncryptDecryptFile decrypt:[dicts valueForKey:@"employerId"]];
            obj.ratingObj.ratingId = [EncryptDecryptFile decrypt:[dicts valueForKey:@"ratingId"]];
            obj.ratingObj.ratingStar = [EncryptDecryptFile decrypt:[dicts valueForKey:@"ratingStar"]];
            obj.ratingObj.taskId = [EncryptDecryptFile decrypt:[dicts valueForKey:@"taskId"]];
            obj.ratingObj.workerId = [EncryptDecryptFile decrypt:[dicts valueForKey:@"workerId"]];
            obj.ratingObj.userName = [EncryptDecryptFile decrypt:[dicts valueForKey:@"userName"]];
            
            [obj.arrayRatings_createdBy addObject:obj.ratingObj];
        }
        
        CGFloat avg = noOfStars/arrayTerms.count;
        obj.avgStars_createdBy = avg;
        
    }else if ([[arrayTerms objectAtIndex:0] isKindOfClass:[NSArray class]]){
        
        NSArray *arrRatings = [arrayTerms objectAtIndex:0];
        
        NSInteger noOfStars = 0;
        
        for (NSDictionary *dicts in arrRatings) {
            
            NSString *stars = [EncryptDecryptFile decrypt:[dicts valueForKey:@"ratingStar"]];
            noOfStars = noOfStars + [stars integerValue];
            
            obj.ratingObj = nil;
            obj.ratingObj = [[rating alloc]init];
            
            obj.ratingObj.ratingdescription = [EncryptDecryptFile decrypt:[dicts valueForKey:@"description"]];
            obj.ratingObj.employerId = [EncryptDecryptFile decrypt:[dicts valueForKey:@"employerId"]];
            obj.ratingObj.ratingId = [EncryptDecryptFile decrypt:[dicts valueForKey:@"ratingId"]];
            obj.ratingObj.ratingStar = [EncryptDecryptFile decrypt:[dicts valueForKey:@"ratingStar"]];
            obj.ratingObj.taskId = [EncryptDecryptFile decrypt:[dicts valueForKey:@"taskId"]];
            obj.ratingObj.workerId = [EncryptDecryptFile decrypt:[dicts valueForKey:@"workerId"]];
            obj.ratingObj.userName = [EncryptDecryptFile decrypt:[dicts valueForKey:@"userName"]];
            
            [obj.arrayRatings_createdBy addObject:obj.ratingObj];
        }
        
        CGFloat avg = noOfStars/arrayTerms.count;
        obj.avgStars_createdBy = avg;
        
    }
    
    
    arrayTerms= [dict valueForKey:@"Card Detail"];
    if ([[arrayTerms objectAtIndex:0] isKindOfClass:[NSString class]]) {
        obj.cardDetail = [EncryptDecryptFile decrypt:[arrayTerms objectAtIndex:0]];
    }else if ([[arrayTerms objectAtIndex:0] isKindOfClass:[NSDictionary class]]){
        
    }else if ([[arrayTerms objectAtIndex:0] isKindOfClass:[NSArray class]]){
        obj.cardDetail = [EncryptDecryptFile decrypt:[[arrayTerms objectAtIndex:0] objectAtIndex:0]];
    }
    
    arrayTerms= [dict valueForKey:@"Created By"];
   // NSDictionary *dictTemp2=[arrayTerms objectAtIndex:0];
    
    obj.userId_createdBy = [EncryptDecryptFile decrypt:[dictTemp2 valueForKey:@"userId"]];
    obj.userName_createdBy = [EncryptDecryptFile decrypt:[dictTemp2 valueForKey:@"userName"]];
    obj.description_createdBy = [EncryptDecryptFile decrypt:[dictTemp2 valueForKey:@"description"]];
    obj.emailId_createdBy = [EncryptDecryptFile decrypt:[dictTemp2 valueForKey:@"emailId"]];
    obj.latitude_createdBy = [[EncryptDecryptFile decrypt:[dictTemp2 valueForKey:@"latitude"]] floatValue];
    obj.longitude_createdBy = [[EncryptDecryptFile decrypt:[dictTemp2 valueForKey:@"longitude"]] floatValue];
    obj.locationName_createdBy = [EncryptDecryptFile decrypt:[dictTemp2 valueForKey:@"location"]];
    obj.mobile_createdBy = [EncryptDecryptFile decrypt:[dictTemp2 valueForKey:@"mobile"]];
    obj.picPath_createdBy = [EncryptDecryptFile decrypt:[dictTemp2 valueForKey:@"picPath"]];
    obj.avg_rating_createdBy = [EncryptDecryptFile decrypt:[dictTemp2 valueForKey:@"avg_rating"]];
    
    arrayTerms= [dict valueForKey:@"Task Detail"];
   // NSDictionary *dictTemp3=[arrayTerms objectAtIndex:0];
    
    obj.taskDetailObj = [[task alloc]init];
    
    obj.taskDetailObj.userid = [EncryptDecryptFile decrypt:[dictTemp3 valueForKey:@"userId"]];
    obj.taskDetailObj.title = [EncryptDecryptFile decrypt:[dictTemp3 valueForKey:@"title"]];
    obj.taskDetailObj.taskid = [EncryptDecryptFile decrypt:[dictTemp3 valueForKey:@"taskId"]];
    obj.taskDetailObj.subcategoryId = [EncryptDecryptFile decrypt:[dictTemp3 valueForKey:@"subcategoryId"]];
    obj.taskDetailObj.status = [EncryptDecryptFile decrypt:[dictTemp3 valueForKey:@"status"]];
    obj.taskDetailObj.startDate = [EncryptDecryptFile decrypt:[dictTemp3 valueForKey:@"startDate"]];
    obj.taskDetailObj.price = [[EncryptDecryptFile decrypt:[dictTemp3 valueForKey:@"price"]] floatValue];
    obj.taskDetailObj.longitude = [[EncryptDecryptFile decrypt:[dictTemp3 valueForKey:@"longitude"]] floatValue];
    obj.taskDetailObj.locationName = [EncryptDecryptFile decrypt:[dictTemp3 valueForKey:@"locationName"]];
    obj.taskDetailObj.latitude = [[EncryptDecryptFile decrypt:[dictTemp3 valueForKey:@"latitude"]] floatValue];
    obj.taskDetailObj.endDate = [EncryptDecryptFile decrypt:[dictTemp3 valueForKey:@"endDate"]];
    obj.taskDetailObj.taskDescription = [EncryptDecryptFile decrypt:[dictTemp3 valueForKey:@"description"]];
    obj.taskDetailObj.lastModifyTime = [EncryptDecryptFile decrypt:[dictTemp3 valueForKey:@"lastModifyTime"]];
    obj.taskDetailObj.taskImagesArray = [[NSMutableArray alloc] init];
    obj.taskDetailObj.imagesArray = [[NSMutableArray alloc]init];
    
    for (rating *ratingObjs in obj.arrayRatings_acceptedBy) {
        
        if ([ratingObjs.taskId isEqualToString:[NSString stringWithFormat:@"%@",obj.taskDetailObj.taskid]]) {
            
            obj.isAlreadyRated = true;
            
            break;
            
        }
        
    }
    
    arrayTerms= [dict valueForKey:@"taskImage"];
    NSArray *newArray = [arrayTerms objectAtIndex:0];
    
   // int i = 0;
    
    for (NSDictionary *dicts in newArray) {
        
        NSArray *componentsArray = [[EncryptDecryptFile decrypt:[dicts valueForKey:@"taskImg"]] componentsSeparatedByString:@"http://192.168.5.134/handapp/"];
        
        if (componentsArray.count>1) {
            [obj.taskDetailObj.taskImagesArray addObject:[NSString stringWithFormat:urlServer,[componentsArray objectAtIndex:1]]];
        }else{
            [obj.taskDetailObj.taskImagesArray addObject:[NSString stringWithFormat:urlServer,[componentsArray objectAtIndex:0]]];
        }
        
        NSString *fullPath=[obj.taskDetailObj.taskImagesArray objectAtIndex:i];
        NSArray *parts = [fullPath componentsSeparatedByString:@"/"];
        NSString *imageName=[parts lastObject];
        NSArray *imageNameParts = [imageName componentsSeparatedByString:@"."];
        
        NSString *userImageNameWithJPEG = [NSString stringWithFormat:@"%@_%@_%d.jpeg",[imageNameParts firstObject],obj.taskDetailObj.taskid,i];
        
        UIImage *img = [GlobalFunction loadImageOfLogoFromCacheFolder:userImageNameWithJPEG dirname:@"taskImages"];
        
        if (img) {
            
            [obj.taskDetailObj.imagesArray addObject:img];
            
        }
        
        i = i+1;
    }
    
    if (obj.taskDetailObj.taskImagesArray.count != obj.taskDetailObj.imagesArray.count) {
        [obj.taskDetailObj.imagesArray removeAllObjects];
    }
    
    
    if ([userObj.userid isEqualToString:obj.userId_acceptedBy]) {
        
        if ([obj.picPath_createdBy isEqualToString:@""]) {
            obj.userImage_createdBy = imageUserDefault;
        }else{
            NSArray *components = [obj.picPath_createdBy componentsSeparatedByString:@"/"];
            
            NSString *imgNameWithJPEG = [components lastObject];
            
            
            NSArray *componentsWithDots = [imgNameWithJPEG componentsSeparatedByString:@"."];
            
            if (componentsWithDots.count>2) {
                
                imgNameWithJPEG = [NSString stringWithFormat:@"%@.%@",[componentsWithDots firstObject],[componentsWithDots lastObject]];
                
            }
            
            UIImage *img = [GlobalFunction loadImageOfLogoFromCacheFolder:imgNameWithJPEG dirname:@"user"];
            
            if (img) {
                obj.userImage_createdBy = img;
            }else{
                UIImage *img2 = [GlobalFunction getImageFromURL:[NSString stringWithFormat:urlServer,obj.picPath_createdBy]];
                
                if (img2) {
                    obj.userImage_createdBy = img2;
                    
                    NSArray *components2 = [imgNameWithJPEG componentsSeparatedByString:@"."];
                    
                    NSString *imgNameWithoutJPEG = [components2 firstObject];
                    
                    [GlobalFunction saveimage:img2 imageName:imgNameWithoutJPEG dirname:@"user"];
                    
                }else{
                    obj.userImage_createdBy = imageUserDefault;
                }
            }
        }
    }else if ([userObj.userid isEqualToString:obj.userId_createdBy]){
        
        if ([obj.picPath_acceptedBy isEqualToString:@""]) {
            obj.userImage_acceptedBy = imageUserDefault;
        }else{
            NSArray *components = [obj.picPath_acceptedBy componentsSeparatedByString:@"/"];
            
            NSString *imgNameWithJPEG = [components lastObject];
            
            
            NSArray *componentsWithDots = [imgNameWithJPEG componentsSeparatedByString:@"."];
            
            if (componentsWithDots.count>2) {
                
                imgNameWithJPEG = [NSString stringWithFormat:@"%@.%@",[componentsWithDots firstObject],[componentsWithDots lastObject]];
                
            }
            
            UIImage *img = [GlobalFunction loadImageOfLogoFromCacheFolder:imgNameWithJPEG dirname:@"user"];
            
            if (img) {
                obj.userImage_acceptedBy = img;
            }else{
                UIImage *img2 = [GlobalFunction getImageFromURL:[NSString stringWithFormat:urlServer,obj.picPath_acceptedBy]];
                
                if (img2) {
                    obj.userImage_acceptedBy = img2;
                    
                    NSArray *components2 = [imgNameWithJPEG componentsSeparatedByString:@"."];
                    
                    NSString *imgNameWithoutJPEG = [components2 firstObject];
                    
                    [GlobalFunction saveimage:img2 imageName:imgNameWithoutJPEG dirname:@"user"];
                    
                }else{
                    obj.userImage_acceptedBy = imageUserDefault;
                }
            }
        }
    }
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (tableView == self.tableCategoryListing) {
        
        if (indexPath.row == 0) {
            
            
        }else{
            
            if ([[myAppDelegate.arrayCategoryDetail objectAtIndex:indexPath.row - 1] isKindOfClass:[category class]]) {
                
                
            }else if ([[myAppDelegate.arrayCategoryDetail objectAtIndex:indexPath.row - 1] isKindOfClass:[subCategory class]]){
                
                return 52;
                
            }
 
        }
         
        
    }
    else if(tableView == self.tableTaskListing){
        
  //***********************
        if(IS_IPHONE_5 || IS_IPHONE_4_OR_LESS)
        {
            
            return 100;

        }
        else if(IS_IPHONE_6)
        {
            
            return 117;
            
        }
        else
        {
        return 128;
        }
    }else if (tableView == self.tableRadius){
        
        return height;
        
    }
    else if (tableView == self.cityNameTableView){
        
        return 28;
        
    }
    
    return 40;
    
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (tableView == self.tableCategoryListing || tableView == self.tableTaskListing || tableView == self.tableRadius) {
        
        // Remove seperator inset
        if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
            [cell setSeparatorInset:UIEdgeInsetsZero];
        }
        
        // Prevent the cell from inheriting the Table View's margin settings
        if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
            [cell setPreservesSuperviewLayoutMargins:NO];
        }
        
        // Explictly set your cell's layout margins
        if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
            [cell setLayoutMargins:UIEdgeInsetsZero];
        }
        
    }
}


#pragma mark - new picker view by VS
// The number of columns of data
- (NSInteger)numberOfComponentsInPickerView:
(UIPickerView *)pickerView
{
    return 1;
}

// The number of rows of data
- (NSInteger)pickerView:(UIPickerView *)pickerView
numberOfRowsInComponent:(NSInteger)component
{
    return arrRadius.count;
}

// The data to return for the row and component (column) that's being passed in
- (NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return arrRadius[row];
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row
      inComponent:(NSInteger)component
{
    NSString *strThirdPickerView = [arrRadius objectAtIndex:row];
    
    self.minTxtFld.text = strThirdPickerView;
    
    self.radiusPickerView.hidden = true;
    [self performSelector:@selector(MoveTextFieldView) withObject:nil afterDelay:0.1];
}


//******************************************************************************

-(void)openFilterView
{
//    myAppDelegate.filterViewIsOpen=true;
//    myAppDelegate.filterViewIsClose=false;
    
    _filterView.clipsToBounds=YES;
    
    [UIView animateWithDuration:0.5f
                          delay:0.0f
                        options:UIViewAnimationOptionTransitionFlipFromBottom | UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         [self.view layoutIfNeeded];
                         if(IS_IPHONE_6)
                         {
                             _filterGreyViewHeight.constant=96;  //FOR IPHONE 6
                         }
                         
                         else if(IS_IPHONE_5 || IS_IPHONE_4_OR_LESS)
                         {
                             _filterGreyViewHeight.constant=81;  //FOR IPHONE 5
                         }
                         
                         else if (IS_IPHONE_6P)
                         {
                             _filterGreyViewHeight.constant=106;  //FOR IPHONE 6Plus
                         }
                         
                         _filterParentView.hidden=YES;
                         _filterView.hidden=NO;
                         
                         
                     }completion:NULL];
    
}

-(void)closeFilterView
{
//    myAppDelegate.filterViewIsClose=true;
//    myAppDelegate.filterViewIsOpen=false;
    
    
    _filterView.clipsToBounds=YES;
    
    [self.view layoutIfNeeded];
    
    _filterGreyViewHeight.constant=0;
    
    [UIView animateWithDuration:0.5 delay:0.0 options:UIViewAnimationOptionTransitionFlipFromBottom | UIViewAnimationOptionCurveEaseInOut animations:^{
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished) {
        _filterView.hidden = YES;
        _filterParentView.hidden=YES;
        
    }];
    
}


//******************************************************************************



#pragma mark - new filter view by VS
- (IBAction)radiusBtnClicked:(id)sender
{
    
    _minTxtFld.clearButtonMode = UITextFieldViewModeNever;
    _maxTxtFld.clearButtonMode = UITextFieldViewModeNever;
    
    [self openFilterView];
    
    [_btnPrice setTitleColor:[UIColor whiteColor]forState:UIControlStateNormal];
    [_btnPrice setBackgroundColor:[UIColor colorWithRed:63/255.0 green:67/255.0 blue:70/255.0 alpha:1.0]];
    
    [_btnTime setTitleColor:[UIColor whiteColor]forState:UIControlStateNormal];
    [_btnTime setBackgroundColor:[UIColor colorWithRed:63/255.0 green:67/255.0 blue:70/255.0 alpha:1.0]];
    
    
    if (!self.btnRadius.selected) {
        
        [self openFilterView];
        
        [_btnRadius setTitleColor:[UIColor colorWithRed:63/255.0 green:67/255.0 blue:70/255.0 alpha:1.0] forState:UIControlStateNormal];
        [_btnRadius setBackgroundColor:[UIColor whiteColor]];

        
        
      //  oldMinPriceRangeData = self.minTxtFld.text;
       // oldMaxPriceRangeData = self.maxTxtFld.text;
        NSString *fillCity = myAppDelegate.currentCityName;
        self.minTxtFld.text = rangeLocation;//oldRadiusData_TextToShowAsPlaceHolder;
        
        if (enteredLocation.length <=0 )
        {
            self.maxTxtFld.text = fillCity;
        }
        else
        {
            if ([enteredLocation isEqualToString:myAppDelegate.lastSearchLocation])
            {
                self.maxTxtFld.text = enteredLocation;//oldLocationData;
            }
            else
            {
              self.maxTxtFld.text = myAppDelegate.lastSearchLocation;//by vs to show success location name search 31 Aug 18...
            }
        }
        
        self.minTxtFld.textColor = [UIColor colorWithRed:63/255.0 green:67/255.0 blue:70/255.0 alpha:1.0];// RGB(185, 185, 185);
        
        [self.btnRadius setSelected:YES];
        [self.btnPrice setSelected:NO];
        [self.btnTime setSelected:NO];
        
        CATransition *animation = [CATransition animation];
        animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
        animation.type = kCATransitionFade;
        animation.duration = 0.75;
        [_minLabel.layer addAnimation:animation forKey:@"kCATransitionFade"];
        [_maxLabel.layer addAnimation:animation forKey:@"kCATransitionFade"];
        [_maxTxtFld.layer addAnimation:animation forKey:@"kCATransitionFade"];
        [_minTxtFld.layer addAnimation:animation forKey:@"kCATransitionFade"];

    
        [_minLabel setText:@"Adjust Radius"];
        [_maxLabel setText:@"Location"];
        
        self.maxTxtFld.keyboardType = UIKeyboardTypeDefault;
        
               NSAttributedString *str1 = [[NSAttributedString alloc] initWithString:@"City" attributes:@{ NSForegroundColorAttributeName : RGB(185, 185, 185),
                                                                                                           }];
            _maxTxtFld.attributedPlaceholder = str1;
        
        
        
        NSAttributedString *str2 = [[NSAttributedString alloc] initWithString:@"Miles of" attributes:@{ NSForegroundColorAttributeName : RGB(185, 185, 185),
                                                                                                              }];
        _minTxtFld.attributedPlaceholder = str2;
        
    } else {
        
        [self.btnRadius setSelected:FALSE];
        [self closeFilterView];
        
        [_btnRadius setTitleColor:[UIColor whiteColor]forState:UIControlStateNormal];
        [_btnRadius setBackgroundColor:[UIColor colorWithRed:63/255.0 green:67/255.0 blue:70/255.0 alpha:1.0]];
    }

}



- (IBAction)timeBtnClicked:(id)sender
{

    [self openFilterView];

    self.radiusPickerView.hidden = true;
    
    
    [_btnRadius setTitleColor:[UIColor whiteColor]forState:UIControlStateNormal];
    [_btnRadius setBackgroundColor:[UIColor colorWithRed:63/255.0 green:67/255.0 blue:70/255.0 alpha:1.0]];
    
    [_btnPrice setTitleColor:[UIColor whiteColor]forState:UIControlStateNormal];
    [_btnPrice setBackgroundColor:[UIColor colorWithRed:63/255.0 green:67/255.0 blue:70/255.0 alpha:1.0]];
    
    
    if (!self.btnTime.selected) {
        
        //  self.filterView.hidden = false;
//        [UIView animateWithDuration:0.5
//                              delay:0.0
//                            options:UIViewAnimationOptionCurveEaseOut
//                         animations:^{
//                             
//                             self.filterView.alpha = 1.0;
//                             
//                             
//                             
//                         } completion:^(BOOL finished) {
//                             
//                             
//                         }];
        
        [self openFilterView];

        

        
        [_btnTime setTitleColor:[UIColor colorWithRed:63/255.0 green:67/255.0 blue:70/255.0 alpha:1.0] forState:UIControlStateNormal];
        [_btnTime setBackgroundColor:[UIColor whiteColor]];

        
//        oldMinPriceRangeData = self.minTxtFld.text;
//        oldMaxPriceRangeData = self.maxTxtFld.text;
        
        self.minTxtFld.text = minTime;//oldRadiusData_TextToShowAsPlaceHolder;
        self.maxTxtFld.text = maxTime;//oldLocationData;
        
        self.minTxtFld.textColor = [UIColor colorWithRed:63/255.0 green:67/255.0 blue:70/255.0 alpha:1.0];// RGB(185, 185, 185);
        
        [self.btnRadius setSelected:NO];
        [self.btnPrice setSelected:NO];
        [self.btnTime setSelected:YES];

        CATransition *animation = [CATransition animation];
        animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
        animation.type = kCATransitionFade;
        animation.duration = 0.75;
        [_minLabel.layer addAnimation:animation forKey:@"kCATransitionFade"];
        [_maxLabel.layer addAnimation:animation forKey:@"kCATransitionFade"];
        [_maxTxtFld.layer addAnimation:animation forKey:@"kCATransitionFade"];
        [_minTxtFld.layer addAnimation:animation forKey:@"kCATransitionFade"];

        [_minLabel setText:@"Starts"];
        [_maxLabel setText:@"Ends"];
        
        self.maxTxtFld.keyboardType = UIKeyboardTypeDefault;
        
        NSAttributedString *str1 = [[NSAttributedString alloc] initWithString:@"Date" attributes:@{ NSForegroundColorAttributeName : RGB(185, 185, 185),
                                                                                                           }];
        _maxTxtFld.attributedPlaceholder = str1;
        
        NSAttributedString *str2 = [[NSAttributedString alloc] initWithString:@"Date" attributes:@{ NSForegroundColorAttributeName : RGB(185, 185, 185),
                                                                                                              }];
        _minTxtFld.attributedPlaceholder = str2;
        
    }else {
        
        //        self.filterView.hidden = true;
        //        self.filterParentView.hidden = true;
//        [UIView animateWithDuration:0.5
//                              delay:0.0
//                            options:UIViewAnimationOptionCurveEaseOut
//                         animations:^{
//                             
//                             self.filterView.alpha = 0.0;
//                             self.filterParentView.alpha = 0.0;
//                             
//                             
//                             
//                         } completion:^(BOOL finished) {
//                             
//                             
//                         }];
        
        [self closeFilterView];
        
        
        [self.btnTime setSelected:NO];
        [_btnTime setTitleColor:[UIColor whiteColor]forState:UIControlStateNormal];
        [_btnTime setBackgroundColor:[UIColor colorWithRed:63/255.0 green:67/255.0 blue:70/255.0 alpha:1.0]];
    }


    
    
}
- (IBAction)priceBtnClicked:(id)sender
{

    _minTxtFld.clearButtonMode = UITextFieldViewModeNever;
    _maxTxtFld.clearButtonMode = UITextFieldViewModeNever;
    
    self.filterParentView.hidden = true;

    self.radiusPickerView.hidden = true;
    self.datePickerView.hidden = true;
    
    [self openFilterView];

    [_btnRadius setTitleColor:[UIColor whiteColor]forState:UIControlStateNormal];
    [_btnRadius setBackgroundColor:[UIColor colorWithRed:63/255.0 green:67/255.0 blue:70/255.0 alpha:1.0]];
    
    [_btnTime setTitleColor:[UIColor whiteColor]forState:UIControlStateNormal];
    [_btnTime setBackgroundColor:[UIColor colorWithRed:63/255.0 green:67/255.0 blue:70/255.0 alpha:1.0]];
    
    if (!self.btnPrice.selected) {
        
        //  self.filterView.hidden = false;

        [self openFilterView];

        [_btnPrice setTitleColor:[UIColor colorWithRed:63/255.0 green:67/255.0 blue:70/255.0 alpha:1.0] forState:UIControlStateNormal];
        [_btnPrice setBackgroundColor:[UIColor whiteColor]];
        
        
//        oldRadiusData_TextToShowAsPlaceHolder = self.minTxtFld.text;
//        oldLocationData = self.maxTxtFld.text;
//        
        self.minTxtFld.text = minPriceValue;//oldMinPriceRangeData;
        self.maxTxtFld.text = maxPriceValue;//oldMaxPriceRangeData;
        
        
        
        self.minTxtFld.textColor =[UIColor colorWithRed:63/255.0 green:67/255.0 blue:70/255.0 alpha:1.0]; //[UIColor blackColor];
        
        [self.btnRadius setSelected:NO];
        [self.btnTime setSelected:NO];
        [self.btnPrice setSelected:YES];
        
        [_minLabel setText:@"Min"];
        [_maxLabel setText:@"Max"];
        
        self.maxTxtFld.keyboardType = UIKeyboardTypeNumberPad;
        
        CATransition *animation = [CATransition animation];
        animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
        animation.type = kCATransitionFade;
        animation.duration = 0.75;
        [_minLabel.layer addAnimation:animation forKey:@"kCATransitionFade"];
        [_maxLabel.layer addAnimation:animation forKey:@"kCATransitionFade"];
        [_maxTxtFld.layer addAnimation:animation forKey:@"kCATransitionFade"];
        [_minTxtFld.layer addAnimation:animation forKey:@"kCATransitionFade"];

        
        NSAttributedString *str1 = [[NSAttributedString alloc] initWithString:@"$" attributes:@{ NSForegroundColorAttributeName : RGB(185, 185, 185),
                                                                                                }];
        _maxTxtFld.attributedPlaceholder = str1;
        
        NSAttributedString *str2 = [[NSAttributedString alloc] initWithString:@"$" attributes:@{ NSForegroundColorAttributeName : RGB(185, 185, 185),
                                                                                                }];
        _minTxtFld.attributedPlaceholder = str2;
        
    }
    else
    {
        
        [self closeFilterView];
        
        [self.btnPrice setSelected:NO];
        
        [_btnPrice setTitleColor:[UIColor whiteColor]forState:UIControlStateNormal];
        [_btnPrice setBackgroundColor:[UIColor colorWithRed:63/255.0 green:67/255.0 blue:70/255.0 alpha:1.0]];
    }

   
}

-(void) checkSelectedBtnsForHideFilterFromMenuPress
{
    //  NSLog(@"selected btnj string is...%@",BtnClicked);
    
    [txtActiveField resignFirstResponder];
    _datePickerView.hidden = true;
    self.filterParentView.hidden = true;

    
    [self.view layoutIfNeeded];
    self.consFilterViewBottom.constant = 0;
    
    
    [UIView animateWithDuration:0.3 animations:^{
        [self.view layoutIfNeeded];
        // self.view.frame = frame;
        //  self.filterView.frame = frame;
    } completion:^(BOOL finished) {
        
    }];
}

-(void) checkSelectedBtnsForForwardFilter
{
    //  NSLog(@"selected btnj string is...%@",BtnClicked);
    
    [txtActiveField resignFirstResponder];
    _datePickerView.hidden = true;
    self.filterParentView.hidden = true;

    
    [self.view layoutIfNeeded];
    self.consFilterViewBottom.constant = 0;
    
    
    [UIView animateWithDuration:0.3 animations:^{
        [self.view layoutIfNeeded];
        // self.view.frame = frame;
        //  self.filterView.frame = frame;
    } completion:^(BOOL finished) {
        
    }];
    
    
    if (self.btnRadius.selected) {
        
        [self radiusBtnClicked:nil];
        
    }
    else if (self.btnTime.selected)
    {
        [self timeBtnClicked:nil];
        
    }
    else if (self.btnPrice.selected)
    {
        [self priceBtnClicked:nil];
        
    }
    else
    {
        
    }
    
    
}



-(void) checkSelectedBtns
{
    //  NSLog(@"selected btnj string is...%@",BtnClicked);
    
    [txtActiveField resignFirstResponder];
    _datePickerView.hidden = true;
    self.filterParentView.hidden = true;

    
    [self.view layoutIfNeeded];
    self.consFilterViewBottom.constant = 0;
    
    
    [UIView animateWithDuration:0.3 animations:^{
        [self.view layoutIfNeeded];
        // self.view.frame = frame;
        //  self.filterView.frame = frame;
    } completion:^(BOOL finished) {
        
    }];

    
    
    if (self.btnRadius.selected) {
        
        
        
        if (self.filterCityViewHeight.constant == 180 || self.filterCityViewHeight.constant == 160)
        {
            [self.view layoutIfNeeded];
            
            self.filterCityViewHeight.constant=0;
            
            [UIView animateWithDuration:0.3 animations:^ {
                [self.view layoutIfNeeded];
                
            }];
            
        }

        [self radiusBtnClicked:nil];
        
    }
    else if (self.btnTime.selected)
    {
        [self timeBtnClicked:nil];
        
    }
    else if (self.btnPrice.selected)
    {
        [self priceBtnClicked:nil];
        
    }
    else
    {
        
    }
    
    
}

#pragma mark - create accessory view
-(void)createInputAccessoryView{
    // Create the view that will play the part of the input accessory view.
    // Note that the frame width (third value in the CGRectMake method)
    // should change accordingly in landscape orientation. But we don’t care
    // about that now.
    inputAccView = [[UIView alloc] initWithFrame:CGRectMake(10.0, 0.0, self.view.frame.size.width, 40.0)];
 
   // inputAccView = self.filterView;
    
    // Set the view’s background color. We’ ll set it here to gray. Use any color you want.
    [inputAccView setBackgroundColor:[UIColor lightGrayColor]];
    // We can play a little with transparency as well using the Alpha property. Normally
    // you can leave it unchanged.
    [inputAccView setAlpha: 0.8];
    // If you want you may set or change more properties (ex. Font, background image,e.t.c.).
    // For now, what we’ ve already done is just enough.
    // Let’s create our buttons now. First the previous button.
    btnPrev = [UIButton buttonWithType: UIButtonTypeCustom];
    // Set the button’ s frame. We will set their widths to 80px and height to 40px.
    [btnPrev setFrame: CGRectMake(0.0, 0.0, 80.0, 40.0)];
    // Title.
    [btnPrev setTitle: @"Previous" forState: UIControlStateNormal];
    // Background color.
    [btnPrev setBackgroundColor: [UIColor clearColor]];
    // You can set more properties if you need to.
    // With the following command we’ ll make the button to react in finger tapping. Note that the
    // gotoPrevTextfield method that is referred to the @selector is not yet created. We’ ll create it
    // (as well as the methods for the rest of our buttons) later.
    [btnPrev addTarget: self action: @selector(gotoPrevTextfield) forControlEvents: UIControlEventTouchUpInside];
    // Do the same for the two buttons left.
    btnNext = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnNext setFrame:CGRectMake(85.0f, 0.0f, 80.0f, 40.0f)];
    [btnNext setTitle:@"Next" forState:UIControlStateNormal];
    [btnNext setBackgroundColor:[UIColor clearColor]];
    [btnNext addTarget:self action:@selector(gotoNextTextfield) forControlEvents:UIControlEventTouchUpInside];
    btnDone = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnDone setFrame:CGRectMake(self.view.frame.size.width - 80.0f, 0.0f, 80.0f, 40.0f)];
    [btnDone setTitle:@"Done" forState:UIControlStateNormal];
    [btnDone setBackgroundColor:[UIColor clearColor]];
    [btnDone setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [btnDone addTarget:self action:@selector(doneTyping) forControlEvents:UIControlEventTouchUpInside];
    // Now that our buttons are ready we just have to add them to our view.
    [inputAccView addSubview:btnPrev];
    [inputAccView addSubview:btnNext];
    [inputAccView addSubview:btnDone];
}


-(void)gotoPrevTextfield{
    // If the active textfield is the first one, can't go to any previous
    // field so just return.
    
    
    if (txtActiveField == _maxTxtFld) {
        
        [self.minTxtFld becomeFirstResponder];
        [self.maxTxtFld resignFirstResponder];
         return;
    }
    if (txtActiveField == _minTxtFld) {
        
        [self.view layoutIfNeeded];
        self.consFilterViewBottom.constant = 0;
        
        [UIView animateWithDuration:0.3 animations:^{
            [self.view layoutIfNeeded];
            // self.view.frame = frame;
            //  self.filterView.frame = frame;
        } completion:^(BOOL finished) {
            
        }];
       // [self.maxTxtFld becomeFirstResponder];
        [txtActiveField resignFirstResponder];
        [self checkSelectedBtns];
         return;
    }
        
         // old text field works...
        
//    if (txtActiveField == self.textFieldRadiusFilter) {
//        
//        [self performFilter];
//        
//        [txtActiveField resignFirstResponder];
//        
//        return;
//
//    }
//    
//    if (txtActiveField == self.textFieldEnterLocation){
//        
//        [self.textFieldEnterLocation resignFirstResponder];
//        [self.textFieldRadiusFilter becomeFirstResponder];
//        return;
//
//    }
//    
//    if (txtActiveField == self.textFieldPriceRangeMin) {
//        
//        [self performFilter];
//        
//        [txtActiveField resignFirstResponder];
//        
//        return;
//        
//    }
//    
//    if (txtActiveField == self.textFieldPriceRangeMax) {
//        
//        [self.textFieldPriceRangeMax resignFirstResponder];
//        [self.textFieldPriceRangeMin becomeFirstResponder];
//        return;
//    }
    
        // old text field works...
        
}

-(void)gotoNextTextfield{
    // If the active textfield is the second one, there is no next so just return.
    
    
    if (txtActiveField == _minTxtFld) {
        
//            CGRect frame = self.filterParentView.frame;
//            
//            frame.origin.y = -60;
//            
//            [UIView animateWithDuration:0.3 animations:^{
//                
//                self.filterParentView.frame = frame;
//                
//            } completion:^(BOOL finished) {
//                
//            }];

        //[self.minTxtFld resignFirstResponder];
        [self.maxTxtFld becomeFirstResponder];
        return;

    }
    if (txtActiveField == _maxTxtFld) {
        
        
        if(_btnRadius.selected)
        {
            if (self.radiusPickerView.hidden == false) {
                
                self.radiusPickerView.hidden = true;
                self.filterParentView.hidden = true;
            }
            
            // return;
        }
        
        //CGRect frame = self.view.frame;
       // frame.origin.y = 0;
        
        [self.view layoutIfNeeded];
        self.consFilterViewBottom.constant = 0;
        
        [UIView animateWithDuration:0.3 animations:^{
            [self.view layoutIfNeeded];
           // self.view.frame = frame;
            //  self.filterView.frame = frame;
        } completion:^(BOOL finished) {
            
        }];

         //[self performFilter];
        
        [txtActiveField resignFirstResponder];
        //[self.maxTxtFld resignFirstResponder];
       // [self.minTxtFld becomeFirstResponder];
        [self checkSelectedBtns];
        return;
        
    }
    
    // old textfield work..
    
//    if (txtActiveField == self.textFieldRadiusFilter) {
//        
//        isFromRadiusTableTap = false;
//        
//        filterRange = self.textFieldRadiusFilter.text;
//        
//        [self openAdjustRadiusTableView:tapGestureAdjustRadius];
//
//        [self.textFieldRadiusFilter resignFirstResponder];
//        [self.textFieldEnterLocation becomeFirstResponder];
//        return;
//        
//    }
//    
//    if (txtActiveField == self.textFieldEnterLocation){
//        
//        [self performFilter];
//        
//        [txtActiveField resignFirstResponder];
//        
//        return;
//
//    }
//    
//    if (txtActiveField == self.textFieldPriceRangeMin) {
//        
//        [self.textFieldPriceRangeMin resignFirstResponder];
//        [self.textFieldPriceRangeMax becomeFirstResponder];
//        return;
//        
//    }
//    
//    if (txtActiveField == self.textFieldPriceRangeMax) {
//        
//        [self performFilter];
//        
//        [txtActiveField resignFirstResponder];
//        
//        return;
//
//    }

// old textfield work..
    
}

-(void)doneTyping{
    // When the "done" button is tapped, the keyboard should go away.
    // That simply means that we just have to resign our first responder.
    
    [txtActiveField resignFirstResponder];

    
    if(_btnRadius.selected)
    {
        if (self.radiusPickerView.hidden == false) {
            
             self.radiusPickerView.hidden = true;
            self.filterParentView.hidden = true;
        }

       // return;
    }
    
    if (_btnRadius.selected) {
        
        enteredLocation = self.maxTxtFld.text;
        rangeLocation = self.minTxtFld.text;
        
//        NSLog(@"range and entered location is...%@,%@",enteredLocation,rangeLocation);
//        if (enteredLocation.length>0 && rangeLocation.length>0) {
//            [self performFilter];
//        }
    }
    else if (_btnPrice.selected)
    {
        
         lowValue1 = [self.minTxtFld.text intValue];
         highValue1 = [self.maxTxtFld.text intValue];
        
        
        
        maxPriceValue = self.maxTxtFld.text;
        minPriceValue = self.minTxtFld.text;
        
//        if (maxPriceValue.length>0 && minPriceValue.length>0) {
//            [self performFilter];
//        }
    }
//    else if (_btnTime.selected)
//    {
//        maxTime = self.maxTxtFld.text;
//        minTime = self.minTxtFld.text;
//        
////        if (minTime.length>0 && maxTime.length>0) {
////            [self performFilter];
////        }
//    }

    
    CGRect frame = self.view.frame;
   // frame.origin.y = 0;
    
    [self.view layoutIfNeeded];
    self.consFilterViewBottom.constant = 0;
    
    [UIView animateWithDuration:0.3 animations:^{
        [self.view layoutIfNeeded];
      // self.view.frame = frame;
      //  self.filterView.frame = frame;
    } completion:^(BOOL finished) {
        
    }];
//    if (lowValue1 > highValue1)
//    {
//
//        NSLog(@"date2 is earlier than date1");
//        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Buy Timee" message:@"Please enter a valid price." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
//
//        [alertView show];
//        return;
//    }
    [self performFilter];
 [self checkSelectedBtns];
}

-(void)performFilter{
    
   
    
     // [self performSelector:@selector(showIndicatorWithDelay) withObject:nil afterDelay:0.1];
    [GlobalFunction addIndicatorView];
    [self performSelector:@selector(taskOfPerformFilter) withObject:nil afterDelay:0.2];
  
}

-(void)taskOfPerformFilter
{
    self.lblNoTaskInfo.text = @"No appointments are available matching this criteria, please try differently";
    
    // old one.. No reservation available matching this criteria, please try differently
    
    if (!isFromRadiusTableTap) {
        filterRange = self.textFieldRadiusFilter.text;
        isFromSearchResult=NO;
        
        [self.tableRadius layoutIfNeeded];
        [self.tableRadius reloadData];
        [self.tableRadius layoutIfNeeded];
    }else{
        self.textFieldRadiusFilter.text = filterRange;
    }
    
    
    if (enteredLocation.length<=0) {
        enteredLocation = @"";
    }
    if (minPriceValue.length<=0) {
        minPriceValue = @"";
    }
    if (maxPriceValue.length<=0) {
        maxPriceValue = @"";
    }
    if (rangeLocation.length<=0) {
        rangeLocation = @"";
    }
    
    filterPriceMax = maxPriceValue;
    
    filterPriceMin = minPriceValue;
    
    filterLocationName = enteredLocation;
    
    filterRange = rangeLocation;
    
    
    myAppDelegate.mainMiles = filterRange;
    myAppDelegate.mainLocation = filterLocationName;
    myAppDelegate.mainMinPrice = filterPriceMin;
    myAppDelegate.mainmaxPrice = filterPriceMax;
    
    
    myAppDelegate.mainStartDate =  dateStart;
    myAppDelegate.mainEndDate = dateEnd;
    
    
    //    filterPriceMax = self.textFieldPriceRangeMax.text;
    //
    //    filterPriceMin = self.textFieldPriceRangeMin.text;
    //
    //    filterLocationName = self.textFieldEnterLocation.text;
    
    
    [txtActiveField resignFirstResponder];
    
    //[GlobalFunction addIndicatorView];
    
    [self filterList]; //vs
    

}

#pragma mark - text field delegate
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    
//    CGRect frame = self.view.frame;
//    frame.origin.y = 0;
//    
//    [UIView animateWithDuration:0.3 animations:^{
//        self.view.frame = frame;
//    } completion:^(BOOL finished) {
//        
//    }];
//
  //  [textField resignFirstResponder];
    
    [self.view layoutIfNeeded];
    self.consFilterViewBottom.constant = 0;
    
    
    [UIView animateWithDuration:0.3 animations:^{
        [self.view layoutIfNeeded];
        // self.view.frame = frame;
        //  self.filterView.frame = frame;
    } completion:^(BOOL finished) {
        
    }];

    
   [self performFilter];

    return YES;
}


-(void)cancelPressed:(id)sender
{
    NSLog(@"cancel button click");
    
    _datePickerView.hidden = true;
    self.filterParentView.hidden = true;
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    txtActiveField=textField;
    
    _minTxtFld.clearButtonMode = UITextFieldViewModeNever;
    _maxTxtFld.clearButtonMode = UITextFieldViewModeNever;
    
    
    if(_btnTime.selected){
        
        if(txtActiveField == self.minTxtFld)
        {
            if(dateStart)
            {
            [_datePickerView setDate:dateStart];
            }
        }
        else if(txtActiveField == self.maxTxtFld)
        {
            if(dateEnd)
            {
            [_datePickerView setDate:dateEnd];
            }
        }
        
        
        _minTxtFld.clearButtonMode = UITextFieldViewModeAlways;
        _maxTxtFld.clearButtonMode = UITextFieldViewModeAlways;
        
        
        self.filterParentView.hidden = false;
        self.datePickerView.hidden = false;
        
        self.filterParentView.backgroundColor = [UIColor whiteColor];
        self.filterParentView.alpha = 0.98f;
        
        
//        checkBoxBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//        [checkBoxBtn setFrame:CGRectMake(0, 0, 140, 44)];
//        [checkBoxBtn setTitle:@"Any Date" forState:UIControlStateNormal];
//        [checkBoxBtn.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:17]];
//        checkBoxBtn.tintColor=[UIColor colorWithRed:63.0/255.0f green:67.0/255.0f blue:70.0/255.0f alpha:1.0];
//        [checkBoxBtn setTitleEdgeInsets:UIEdgeInsetsMake(15,100,17,-25)];  //top,left,bottom,right
        
        
        UIBarButtonItem *barButtonDone = [[UIBarButtonItem alloc] initWithTitle:@"Done"
                                                                          style:UIBarButtonItemStyleBordered target:self action:@selector(changeDateFromLabel:)];
        UIBarButtonItem *space = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
        
        
        UIBarButtonItem *barButtonCancel = [[UIBarButtonItem alloc] initWithTitle:@"Cancel"
                                                                             style:UIBarButtonItemStyleBordered target:self action:@selector(cancelPressed:)];
        
       // barButtonCancel.accessibilityFrame=CGRectMake(15,100,17,0);
       // NSMutableArray *barItemsArray = [[NSMutableArray alloc] init];
      //  [checkBoxBtn addTarget:self action:@selector(cancelPressed:) forControlEvents:UIControlEventTouchUpInside];
    //    UIBarButtonItem *checkBoxBtnItem = [[UIBarButtonItem alloc] initWithCustomView:checkBoxBtn];
        //[barItemsArray addObject:checkBoxBtnItem];
     //   checkBoxBtnItem.tintColor=[UIColor blackColor];
        
        
        if(IS_IPHONE5 || IS_IPHONE_4_OR_LESS)
        {
            toolBar= [[UIToolbar alloc] initWithFrame:CGRectMake(0,0,320,44)];
        }
        else if (IS_IPHONE_6)
        {
            toolBar= [[UIToolbar alloc] initWithFrame:CGRectMake(0,0,380,44)];
        }
        else if (IS_IPHONE_6P)
        {
            toolBar= [[UIToolbar alloc] initWithFrame:CGRectMake(0,0,420,44)];
        }
        
        
        toolBar.items = @[barButtonDone,space,barButtonCancel];
        toolBar.backgroundColor=[UIColor colorWithRed:255/255 green:255/255 blue:255/255 alpha:0.98];
        
        
        // barButtonDone.tintColor=[UIColor blackColor];
        [self.filterParentView addSubview:toolBar];

        return NO;
    }
    
    return YES;
}
-(void)changeDateFromLabel:(id)sender
{
    
    NSLog(@"done button click");
    
    _datePickerView.hidden = true;
    self.filterParentView.hidden = true;
    
    
    [self updateTextField:nil];
    
    
    if (dateStart != nil && dateEnd != nil)
    {
        //        if ([dateStart compare:dateEnd] == NSOrderedDescending) {
        //            NSLog(@"date1 is Newer than date2");
        //        } else
        
        if ([dateStart compare:dateEnd] == NSOrderedAscending) {
            NSLog(@"date1 is earlier than date2");
            [self performFilter];
        } else {
            
            NSLog(@"date2 is earlier than date1");
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"BUY TIMEE" message:@"Please enter a valid date." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            
            [alertView show];
        }
        
        
    }
    
    
    
    
    NSLog(@"%@",_minTxtFld.text);
    NSLog(@"%@",_maxTxtFld.text);
    
    
    if(dateStart!=nil && dateEnd==nil)
    {
        [self performFilter];

    }
    

    
    
}

- (BOOL) textFieldShouldClear:(UITextField *)textField{
    
    
    if (txtActiveField == self.minTxtFld)
    {
        dateStart = nil;
        minTime = @"";
    }
    if (txtActiveField == self.maxTxtFld)
    {
        dateEnd = nil;
        maxTime = @"";
    }
    [textField resignFirstResponder];
    
    NSLog(@"%@",_minTxtFld.text);
    NSLog(@"%@",_maxTxtFld.text);
    
    return YES;
}


-(void)RadiusSelect:(BOOL)select{
    
    if (select) {
        //[self.filterParentView setHidden:NO];
        [self.filterView setHidden:NO];
        //[self.radiusPickerView setHidden:NO];
        
        [self.datePickerView setHidden:YES];
        
        
    }else{
        [self.filterView setHidden:YES];
        //[self.filterParentView setHidden:YES];
        [self.radiusPickerView setHidden:YES];
        
    }
    
}
-(void)textFieldDidBeginEditing:(UITextField *)textField{
    
  if(_btnTime.selected)
  {
      self.datePickerView.hidden = false;

      return;
  }
    
    [self createInputAccessoryView];
    // Now add the view as an input accessory view to the selected textfield.
   // [textField setInputAccessoryView:inputAccView];
    
    [textField setInputAccessoryView:inputAccView];
    // Set the active field. We' ll need that if we want to move properly
    // between our textfields.
   
    
    
    txtActiveField = textField;
    
    
    if (txtActiveField == self.minTxtFld)
    {
        if (_btnRadius.selected)
        {
      // self.filterParentView.hidden = false;
      // self.radiusPickerView.hidden = false;
        }
        else
        {
            NSLog(@"radius btn is not selected now");
        }
//        if (_btnTime.selected)
//        {
//            
//          //  self.datePickerView.hidden = false;
//            self.filterParentView.backgroundColor = [UIColor whiteColor];
//            self.filterParentView.alpha = 0.98f;
//        }
//        else
//        {
//            NSLog(@"time btn is not selected now");
//        }
        
    }
    
    if (txtActiveField == self.minTxtFld) {
        
//        CGRect frame = self.filterParentView.frame;
//        
//        frame.origin.y = -60;
//        
//        [UIView animateWithDuration:0.3 animations:^{
//            
//            self.filterParentView.frame = frame;
//            
//        } completion:^(BOOL finished) {
//            
//        }];
        [self performSelector:@selector(MoveTextFieldView) withObject:nil afterDelay:0.01];
        
    }
    if (txtActiveField == self.maxTxtFld) {
        
        [self performSelector:@selector(MoveTextFieldView) withObject:nil afterDelay:0.01];
        
    }

    
    
//    
//    if ((txtActiveField == self.textFieldPriceRangeMax || txtActiveField == self.textFieldPriceRangeMin) && self.view.frame.origin.y == 0) {
//        
//        if (self.constraintAdjustRadiusHeight.constant != 36) {
//            
//            [self openAdjustRadiusTableView:tapGestureAdjustRadius];
//            
//        }
//
//        if (IS_IPHONE_4_OR_LESS) {
//            
//            CGRect frame = self.view.frame;
//            frame.origin.y = frame.origin.y-110;
//            
//            [UIView animateWithDuration:0.3 animations:^{
//                self.view.frame = frame;
//            } completion:^(BOOL finished) {
//                
//            }];
//            
//        }
//        if (IS_IPHONE_5) {
//            
//            CGRect frame = self.view.frame;
//            frame.origin.y = frame.origin.y-90;
//            
//            [UIView animateWithDuration:0.3 animations:^{
//                self.view.frame = frame;
//            } completion:^(BOOL finished) {
//                
//            }];
//            
//            
//        }
//        
//    }else{
//        
//        if (self.constraintAdjustPriceRangeHeight.constant != 40) {
//            
//            [self openPriceRangeView:tapGesturePriceRange];
//            
//        }
//
//    }
//    [txtActiveField becomeFirstResponder];

}

-(void)MoveTextFieldView{
    CGRect frame = self.filterView.frame;
    if(IS_IPHONE_5 || IS_IPHONE_4_OR_LESS)
    {
        [self.view layoutIfNeeded];
        self.consFilterViewBottom.constant = 215;
        
        // frame.origin.y = -10;
    }
    else if (IS_IPHONE_6)
    {
        [self.view layoutIfNeeded];
        self.consFilterViewBottom.constant = 210;
        
        // frame.origin.y =33;
    }
    else if (IS_IPHONE_6P)
    {
        [self.view layoutIfNeeded];
        self.consFilterViewBottom.constant = 215;
        
        // frame.origin.y =33;
    }
    
    [UIView animateWithDuration:0.3 animations:^{
        
        // self.filterView.frame = frame;
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished) {
        
    }];
    
    
}
-(void)textFieldDidEndEditing:(UITextField *)textField{
    
    
    
    if (_btnRadius.selected) {
        
        enteredLocation = self.maxTxtFld.text;
        rangeLocation = self.minTxtFld.text;
        
        
        if (self.filterCityViewHeight.constant == 180 || self.filterCityViewHeight.constant == 160)
        {
            [self.view layoutIfNeeded];
            
            self.filterCityViewHeight.constant=0;
            
            [UIView animateWithDuration:0.3 animations:^ {
                [self.view layoutIfNeeded];
                
            }];
            
        }
        
        
        NSLog(@"range and entered location is...%@,%@",enteredLocation,rangeLocation);
        if (enteredLocation.length>0 && rangeLocation.length>0) {
            
            //[self performFilter]; // Comment via VS for stop two times call filter work on first 30 Aug 18...
        }
    }
    else if (_btnPrice.selected)
    {
        maxPriceValue = self.maxTxtFld.text;
        minPriceValue = self.minTxtFld.text;

        if (maxPriceValue.length>0 && minPriceValue.length>0)
        {
       
            
            //[self performFilter];
        }
    }
    else if (_btnTime.selected)
    {
        maxTime = self.maxTxtFld.text;
        minTime = self.minTxtFld.text;
        
        if (minTime.length>0 && maxTime.length>0) {
            [self performFilter];
        }
    }
    
    
//    CGRect frame = self.view.frame;
//    frame.origin.y = 0;
//    
//    [UIView animateWithDuration:0.3 animations:^{
//        self.view.frame = frame;
//    } completion:^(BOOL finished) {
//        
//    }];
    
}

//- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope{
//    
//    NSPredicate *predicateFName = [NSPredicate predicateWithFormat:@"SELF contains[cd] %@", searchText];
//    
//    searchResultArrayFromRadius = [arrRadius filteredArrayUsingPredicate:predicateFName];
//    
//    if (searchResultArrayFromRadius.count>0) {
//        
//    }else{
//        
//    }
//    
//}


- (void)filterContentForSearchText:(NSString*)searchText //scope:(NSString*)scope
{
    
    FilterCities=[[NSArray alloc] init];
    
    NSPredicate *predicateTitle = [NSPredicate predicateWithFormat:@"cityName beginswith[cd] %@", searchText];
    NSArray *subPredicates = [NSArray arrayWithObjects:predicateTitle, nil];
    NSPredicate *orPredicate = [NSCompoundPredicate orPredicateWithSubpredicates:subPredicates];
    // if(_mycontractsbtn.selected==YES)
    // {
    FilterCities = [myAppDelegate.allCityNames filteredArrayUsingPredicate:orPredicate];
    // }
    // else
    // {
    // searchResults = [arrTemplate filteredArrayUsingPredicate:orPredicate];
    // }
    
    NSLog(@"searching...%lu",(unsigned long)FilterCities.count);
    if (FilterCities.count>0) {
        [self.view layoutIfNeeded];
        if (IS_IPHONE5 || IS_IPHONE_4_OR_LESS)
        {
            self.filterCityViewHeight.constant=160;
        }
        else
        {
        self.filterCityViewHeight.constant=180;
        }
        
        
        
        
        
        [UIView animateWithDuration:0.3 animations:^ {
            [self.view layoutIfNeeded];
            
        }];
        
        
        [self.cityNameTableView reloadData];
    }
    else{
        [self.view layoutIfNeeded];
        
        self.filterCityViewHeight.constant=0;
        
        [UIView animateWithDuration:0.3 animations:^ {
            [self.view layoutIfNeeded];
            
        }];
        
    }
    
    
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    
    if (_btnRadius.selected) {
        
        if (textField == self.maxTxtFld)
        {
            NSString *value = [textField.text stringByReplacingCharactersInRange:range withString:string];
            //NSLog(@"value: %@", value);
            if (![value isEqualToString:@""] && value.length>=2) {
//                [self filterContentForSearchText:value
//                                           scope:[[self.searchDisplayController.searchBar scopeButtonTitles]
//                                                  objectAtIndex:[self.searchDisplayController.searchBar
//                                                                 selectedScopeButtonIndex]]];
                
                
                [self filterContentForSearchText:value];
                 
                
            }
            else{
                [self.view layoutIfNeeded];
                
                self.filterCityViewHeight.constant=0;
                
                [UIView animateWithDuration:0.3 animations:^ {
                    [self.view layoutIfNeeded];
                    
                }];
                
                if (value.length==0) {
                    self.maxTxtFld.text=@"";
                  
                    //[self.tfLocationSearch resignFirstResponder];
                    
                }
                
                
                
            }
            
            
        }

        
        
    }

    
    
    
    
    
    return YES;
    
    if (textField == self.textFieldPriceRangeMin || textField == self.textFieldPriceRangeMax) {
        
        NSArray *dotComponents = [textField.text componentsSeparatedByString:@"."];
        
        if (dotComponents.count>1) {
            
            if ([string isEqualToString:@"."]) {
                return [textField.text componentsSeparatedByString:@"."].count<2;
            }
            
            NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
            NSArray *dotComponentsArrNew = [newString componentsSeparatedByString:@"."];
            if (dotComponentsArrNew.count>1) {
                NSString *afterDotString = [dotComponentsArrNew objectAtIndex:1];
                return afterDotString.length<3;
            }
            
        }else{
            
        }
        
    }else if (textField == self.textFieldRadiusFilter){
        
        NSArray *dotComponents = [textField.text componentsSeparatedByString:@"."];
        
        if (dotComponents.count>1) {
            
            if ([string isEqualToString:@"."]) {
                return [textField.text componentsSeparatedByString:@"."].count<2;
            }
            
            NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
            NSArray *dotComponentsArrNew = [newString componentsSeparatedByString:@"."];
            if (dotComponentsArrNew.count>1) {
                NSString *afterDotString = [dotComponentsArrNew objectAtIndex:1];
                return afterDotString.length<3;
            }
            
            NSString *tempStr = textField.text;
            
            tempStr = [tempStr stringByAppendingString:string];
            
            NSString *value = [textField.text stringByReplacingCharactersInRange:range withString:string];
            
            if (![value isEqualToString:@""]) {
                
                [self filterContentForSearchText:value];// scope:[[self.searchDisplayController.searchBar scopeButtonTitles] objectAtIndex:[self.searchDisplayController.searchBar selectedScopeButtonIndex]]];
                
                isFromSearchResult=YES;
                
            }else{
                
                isFromSearchResult=NO;
                
            }
            
            [self.tableRadius layoutIfNeeded];
            [self.tableRadius reloadData];
            [self.tableRadius layoutIfNeeded];
            
            if (isFromSearchResult) {
                
                CGFloat totalHeight = ((searchResultArrayFromRadius.count+1) * height) + height;
                
                if (totalHeight>height*4) {
                    
                    [self.view layoutIfNeeded];
                    
                    self.constraintAdjustRadiusHeight.constant = height*4;
                    
              //  self.constraintViewDistanceRangeHeight.constant = 107+((height*4)-36); //comment by vs to set it's height 0 on 31 jan;
 
                    [UIView animateWithDuration:0.3 animations:^{
                        
                        [self.view layoutIfNeeded];
                        
                    } completion:^(BOOL finished) {
                        
                    }];
                    
                }else{
                    
                    [self.view layoutIfNeeded];
                    
                    self.constraintAdjustRadiusHeight.constant = totalHeight;
                    
           //    self.constraintViewDistanceRangeHeight.constant = 107+(totalHeight-36); //comment by vs to set it's height 0 on 31 jan;

                    [UIView animateWithDuration:0.3 animations:^{
                        
                        [self.view layoutIfNeeded];
                        
                    } completion:^(BOOL finished) {
                        
                    }];
                    
                }
                
            }else{
                
                if (arrRadius.count>0) {
                    
                    [self.view layoutIfNeeded];
                    
                    self.constraintAdjustRadiusHeight.constant = height*4;
                    
               //    self.constraintViewDistanceRangeHeight.constant = 107+((height*4)-36); //comment by vs to set it's height 0 on 31 jan;

                    [UIView animateWithDuration:0.3 animations:^{
                        
                        [self.view layoutIfNeeded];
                        
                    } completion:^(BOOL finished) {
                        
                    }];
                    
                }else{
                    
                }
                
            }

        }else{
            
            NSString *tempStr = textField.text;
            
            tempStr = [tempStr stringByAppendingString:string];
            
            NSString *value = [textField.text stringByReplacingCharactersInRange:range withString:string];
            
            if (![value isEqualToString:@""]) {
                
                [self filterContentForSearchText:value];// scope:[[self.searchDisplayController.searchBar scopeButtonTitles] objectAtIndex:[self.searchDisplayController.searchBar selectedScopeButtonIndex]]];
                
                isFromSearchResult=YES;
                
            }else{
                
                isFromSearchResult=NO;
                
            }
            
            [self.tableRadius layoutIfNeeded];
            [self.tableRadius reloadData];
            [self.tableRadius layoutIfNeeded];
            
            if (isFromSearchResult) {
                
                CGFloat totalHeight = ((searchResultArrayFromRadius.count+1) * height) + height;
                
                if (totalHeight>height*4) {
                    
                    [self.view layoutIfNeeded];
                    
                    self.constraintAdjustRadiusHeight.constant = height*4;
                    
             //  self.constraintViewDistanceRangeHeight.constant = 107+((height*4)-36);  //comment by vs to set it's height 0 on 31 jan;

                    [UIView animateWithDuration:0.3 animations:^{
                        
                        [self.view layoutIfNeeded];
                        
                    } completion:^(BOOL finished) {
                        
                    }];
                    
                }else{
                    
                    [self.view layoutIfNeeded];
                    
                    self.constraintAdjustRadiusHeight.constant = totalHeight;
                    
                 // self.constraintViewDistanceRangeHeight.constant = 107+(totalHeight-36); //comment by vs to set it's height 0 on 31 jan;

                    [UIView animateWithDuration:0.3 animations:^{
                        
                        [self.view layoutIfNeeded];
                        
                    } completion:^(BOOL finished) {
                        
                    }];
                    
                }
                
            }else{
                
                if (arrRadius.count>0) {
                    
                    [self.view layoutIfNeeded];
                    
                    self.constraintAdjustRadiusHeight.constant = height*4;
                    
           //   self.constraintViewDistanceRangeHeight.constant = 107+((height*4)-36);  //comment by vs to set it's height 0 on 31 jan;

                    [UIView animateWithDuration:0.3 animations:^{
                        
                        [self.view layoutIfNeeded];
                        
                    } completion:^(BOOL finished) {
                        
                    }];
                    
                }else{
                    
                }
                
            }

        }
        
    }
    
    return YES;
}


-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    myAppDelegate.vcObj.btnSideBarOpen.hidden = YES;
    myAppDelegate.vcObj.btnBack.hidden = NO;
    myAppDelegate.stringNameOfChildVC = [NSString stringWithFormat:@"%@",[self class]];

    self.isMapView = false;

    if ([segue.identifier isEqualToString:@"newTaskDetail"]) {
        
        [GlobalFunction addIndicatorView];

        myAppDelegate.taskDetailVCObj = nil;
        
        myAppDelegate.taskDetailVCObj = (TaskDetailViewController *)segue.destinationViewController;
        
    }
    if ([segue.identifier isEqualToString:@"mapView"]) {
                
        self.isMapView = true;
        
        self.lblNoTaskInfo.hidden = YES;
        
        myAppDelegate.mapVCObj = nil;
        
        myAppDelegate.mapVCObj = (MapViewController *)segue.destinationViewController;
        
    }

}

-(BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender{
 
    NSLog(@"identifier is...%@",identifier);
    
    
    if ([identifier isEqualToString:@"newTaskDetail"]) {
        return NO;
    }
    else if ([identifier isEqualToString:@"mapView"])
    {
        self.isMapView = true;
        
        self.lblNoTaskInfo.hidden = YES;
        
        myAppDelegate.mapVCObj = nil;
        
       // myAppDelegate.mapVCObj = (MapViewController *)segue.destinationViewController;
        [myAppDelegate.viewAllTaskVCObj performSegueWithIdentifier:@"mapView" sender:nil];
        return YES;
    }
    return YES;
    
}

- (IBAction)animateImageViewToFullScreen:(id)sender {
    
    TaskDetailTableViewCell *tCell = [self.tableTaskListing cellForRowAtIndexPath:[NSIndexPath indexPathForRow:[[sender view] tag] inSection:0]];
    
    [GlobalFunction animateImageview:tCell.imgViewUserProfile];
    
}

-(void) viewWillAppear:(BOOL)animated
{
    myAppDelegate.vcObj.btnMapOnListView.hidden = false;
    myAppDelegate.vcObj.BtnTransparentMap.hidden = false;//vs

}



@end
