//
//  SetUpBankViewController.m
//  BuyTimee
//
//  Created by User38 on 08/02/17.
//  Copyright © 2017 User14. All rights reserved.
//

#import "SetUpBankViewController.h"
#import "Constants.h"
#import "GlobalFunction.h"
#include <ifaddrs.h>
#include <arpa/inet.h>
#import <QuartzCore/QuartzCore.h>


@interface SetUpBankViewController() <UIImagePickerControllerDelegate> {
    NSString *accType;
    NSArray* nameArr;
    UIToolbar *toolBar;
    UIDatePicker *datepicker;
    UIImagePickerController *picker;
    
    NSDate *dateStart;
    NSString *startTime;
    NSString *startDate;
    BOOL isfromMedia;
    NSUserDefaults *emailCheck;
    NSString *email;
}


@end

@implementation SetUpBankViewController


@synthesize inputAccView,txtActiveField,btnDone,btnNext,btnPrev;
- (void)viewDidLoad {
    [super viewDidLoad];
    NSLog(@"view did load call");
    
    isfromMedia = NO;
    
    myAppDelegate.vcObj.btnMapOnListView.hidden = true;//vs
    myAppDelegate.vcObj.BtnTransparentMap.hidden = true;//vs
    
    
    
    // Do any additional setup after loading the view.
    
    self.textFldName.delegate=self;
    self.textFldAccNo.delegate=self;
    self.textFldReenterAccNo.delegate=self;
    self.textFldRouting.delegate=self;
    self.textFldBankName.delegate=self;
    self.ssnTextFld.delegate = self;
    self.taxIdTxtFld.delegate = self;
    
    
    self.ssnTextFld.keyboardType=UIKeyboardTypeNumberPad;
    
    
    nameArr = [NSArray arrayWithObjects: @"Individual", @"Compnay",nil];
    
    
    myAppDelegate.createBioVCObj.consGreyLineLbl.hidden=YES;
    
    
    accType = @"0";
    
    self.businessTypeTableView.dataSource = self;
    self.businessTypeTableView.delegate = self;
    
    self.nsconsTbleHeight.constant = 0;
    self.businessTypeTableView.layer.borderWidth = 0.5;
    self.businessTypeTableView.layer.cornerRadius = 5.0;
    // self.businessTypeTableView.backgroundColor = [UIColor redColor];
    self.mainScrollView.delegate = self;
    self.mainScrollView.scrollEnabled = YES;
    
    NSLog(@"main content size is...%f",self.lblChecking.frame.origin.y + 400);
    
    self.consContentViewHeight.constant = self.lblChecking.frame.origin.y + 400;
    self.mainScrollView.contentSize = CGSizeMake(self.mainScrollView.frame.size.width, self.lblChecking.frame.origin.y + 250);
    
    datepicker = [[UIDatePicker alloc] init];
    [datepicker setDatePickerMode:UIDatePickerModeDate];
    [datepicker setMaximumDate: [NSDate date]];
    // [picker setMinimumDate: [NSDate date]];
    
    
    // paypal work
    
    self.lblPaypalDescription.text = @"Please verify that the email used above is a valid PayPal business email account. Transfer to your account may take up to 7 days.";
    
self.businessIdTxtFld.delegate = self;
    
    self.businessIdTxtFld.borderStyle = UITextBorderStyleRoundedRect;
    
    self.businessIdTxtFld.layer.cornerRadius=20.0f;
    self.businessIdTxtFld.layer.masksToBounds=YES;
    self.businessIdTxtFld.layer.borderColor=[[UIColor colorWithRed:231.0/255.0f green:231.0/255.0f blue:237.0/255.0f alpha:0.9f]CGColor];
    self.businessIdTxtFld.layer.borderWidth= 3.0f;
    
    self.businessIdTxtFld.keyboardType = UIKeyboardTypeEmailAddress;
    
    
//    emailCheck = [NSUserDefaults standardUserDefaults];
//    
//    email = [myAppDelegate.LoginCheck stringForKey:@"businessEmailPayPal"];
//    
//    if (email)
//    {
//        self.businessIdTxtFld.text = email;
//    }
//    
    
    self.blackTransparentView.alpha = 0.0;
    self.blackTransparentView.hidden = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


// placeholder position
- (CGRect)textRectForBounds:(CGRect)bounds {
    return CGRectInset(bounds, 10, 10);
}

// text position
- (CGRect)editingRectForBounds:(CGRect)bounds {
    return CGRectInset(bounds, 10, 10);
}

-(void)openTable{
    
    //    [self.businessTypeTableView layoutIfNeeded];
    //    [self.businessTypeTableView reloadData];
    //    [self.businessTypeTableView layoutIfNeeded];
    
    //   [txtActiveField resignFirstResponder];
    
    
    [self.view layoutIfNeeded];
    
    NSInteger count = 2;
    
    
    self.nsconsTbleHeight.constant = 42*count;
    
    [UIView animateWithDuration:0.5f
                          delay:0.0f
                        options:UIViewAnimationOptionTransitionFlipFromTop | UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         [self.view layoutIfNeeded];
                         
                         
                         
                     }completion:NULL];
    [self.view layoutIfNeeded];
    
}

-(void)closeTable{
    
    [self.view layoutIfNeeded];
    
    self.nsconsTbleHeight.constant = 0;
    
    [UIView animateWithDuration:0.5 delay:0.0 options:UIViewAnimationOptionTransitionFlipFromBottom | UIViewAnimationOptionCurveEaseInOut animations:^{
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished) {
        // self.businessTypeTableView.hidden = YES;
        
    }];
    
    [self.view layoutIfNeeded];
}

-(void) viewDidAppear:(BOOL)animated
{
    
    if (isfromMedia == YES)
    {
        NSLog(@"return from did appear");
        isfromMedia = NO;
        return;
    }
    
    
    NSString* final = @"Bank information page";
    
    [Flurry logEvent:final];
    
  
    
    
    NSLog(@"view did appear call");
    
    
   [self getBankData]; // comment this by VS 13 June 18... bcz hide bank functionality and add paypal email work...
    
    
    
   }

-(void)getBankData
{
    
    if (![[WebService shared] connected]) {
        
        [GlobalFunction removeIndicatorView];
        
        [[GlobalFunction shared] showAlertForMessage:@"Can't connect to internet, please check and try again."];
        
        return;
    }
    
    NSDictionary*dictReg = [webServicesShared viewBankUserid:myAppDelegate.userData.userid accType:@"stripe"];
    
   
    
    if (dictReg != nil) {
        
         NSLog(@"response --- %@",[EncryptDecryptFile decrypt:[dictReg valueForKey:@"responseMessage"]]);
    
         NSLog(@"response --- %@",[EncryptDecryptFile decrypt:[dictReg valueForKey:@"responseCode"]]);
        
        if ([[EncryptDecryptFile decrypt:[dictReg valueForKey:@"responseCode"]] isEqualToString:@"200"]) {
      
            
            
            
            
        //below code is commented bcz not show bank info only show paypal business email id... // 15 June 18.
            
            
            NSArray *val = [dictReg objectForKey:@"data"];
            
            NSDictionary *dictDataReg = [val objectAtIndex:0];
            
            if (dictDataReg) {
                
                //NSString *email1 = [EncryptDecryptFile decrypt:[dictDataReg valueForKey:@"account"]];
                
               // self.businessIdTxtFld.text = email1;
                
                
                self.textFldName.text = [EncryptDecryptFile decrypt:[dictDataReg valueForKey:@"name"]];
                
                self.textFldAccNo.text = [EncryptDecryptFile decrypt:[dictDataReg valueForKey:@"accountNo"]];
                self.textFldRouting.text = [EncryptDecryptFile decrypt:[dictDataReg valueForKey:@"routingNo"]];
                self.textFldBankName.text = [EncryptDecryptFile decrypt:[dictDataReg valueForKey:@"bankName"]];
                
                NSString *checkAccType = [EncryptDecryptFile decrypt:[dictDataReg valueForKey:@"accType"]];
                
                
                if ([checkAccType isEqualToString:@"0"]) {
                    
                    accType = @"0";
                    
                    [self.radioCheckingClicked setImage:[UIImage imageNamed:@"radioButton.png"] forState:UIControlStateNormal];
                    [self.radioSavingClicked setImage:[UIImage imageNamed:@"radioSelectedButton.png"] forState:UIControlStateNormal];
                }
                else if ([checkAccType isEqualToString:@"1"])
                {
                    accType = @"1";
                    
                    [self.radioSavingClicked setImage:[UIImage imageNamed:@"radioButton.png"] forState:UIControlStateNormal];
                    [self.radioCheckingClicked setImage:[UIImage imageNamed:@"radioSelectedButton.png"] forState:UIControlStateNormal];
                }
                
               //aditional
                
                self.ssnTextFld.text = [EncryptDecryptFile decrypt:[dictDataReg valueForKey:@"ssn"]];
                self.taxIdTxtFld.text = [EncryptDecryptFile decrypt:[dictDataReg valueForKey:@"taxId"]];
          NSString *businessType1 = [EncryptDecryptFile decrypt:[dictDataReg valueForKey:@"type"]];
                
                if (businessType1.length <= 0)
                {
                   // [self.btnBusinessType setTitle:@"Type" forState:UIControlStateNormal];
                }
                else
                {
                    NSString *firstCapChar = [[businessType1 substringToIndex:1] capitalizedString];
                    NSString *cappedString = [businessType1 stringByReplacingCharactersInRange:NSMakeRange(0,1) withString:firstCapChar];
                    
                    [self.btnBusinessType setTitle:cappedString forState:UIControlStateNormal];
                }
                
                
          NSString *DOB = [EncryptDecryptFile decrypt:[dictDataReg valueForKey:@"dob"]];
                
                if (DOB.length <= 0)
                {
                    
                }
                else
                {
                    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
                    [dateFormat setDateFormat:dateFormatFull];
                    NSDate *dateMain = [dateFormat dateFromString:DOB];
//                    NSString *dateString =  [dateFormat stringFromDate:dateMain];
                    
                    NSDateFormatter *dtFormatMDY = [[NSDateFormatter alloc] init];
                    [dtFormatMDY setDateFormat:dateFormatMDY];
                    
                    NSString *dateString = [dtFormatMDY  stringFromDate:dateMain];
                
                    [self.btnDOB setTitle:dateString forState:UIControlStateNormal];
                }

                
                // additional end
           }
            
            
        }
        else
        {
            [GlobalFunction removeIndicatorView];
        }
        
   }
    else
    {
        [GlobalFunction removeIndicatorView];
        [[GlobalFunction shared] showAlertForMessage:@"Server is down, please try again later."];
    }
    [GlobalFunction removeIndicatorView];
}

-(void)viewWillAppear:(BOOL)animated
{
    if (isfromMedia == YES)
    {
        NSLog(@"return from will appear");
        //isfromMedia = NO;
        return;
    }
    
    [GlobalFunction addIndicatorView]; // comment by vs because comment getdata api function in did appear..
    
    NSLog(@"view will appear call");
    
    [self updateConstraints];
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (IBAction)saveInfoClicked:(id)sender
{
    [GlobalFunction addIndicatorView];
    
//     [self performSelector:@selector(sendPayPalEmailToServer) withObject:nil afterDelay:0.2];
//    
//    return;
    
    [self performSelector:@selector(updateBioToServer) withObject:nil afterDelay:0.2];
}
- (IBAction)radioCheckingClicked:(id)sender {
    
    accType = @"1";
    
    [self.radioSavingClicked setImage:[UIImage imageNamed:@"radioButton.png"] forState:UIControlStateNormal];
    [self.radioCheckingClicked setImage:[UIImage imageNamed:@"radioSelectedButton.png"] forState:UIControlStateNormal];
}
- (IBAction)radioSavingClicked:(id)sender {
    
    accType = @"0";
    
    [self.radioCheckingClicked setImage:[UIImage imageNamed:@"radioButton.png"] forState:UIControlStateNormal];
    [self.radioSavingClicked setImage:[UIImage imageNamed:@"radioSelectedButton.png"] forState:UIControlStateNormal];
    
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return nameArr.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 42;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    BusinessTypeTableViewCell *celltype = (BusinessTypeTableViewCell *)[self.businessTypeTableView dequeueReusableCellWithIdentifier:@"Cell"];
    //   cell.cellLbl.textAlignment = NSTextAlignmentCenter;
    
    
    
    celltype.lblBusinessType.text = [nameArr objectAtIndex:indexPath.row];
    
    
    return celltype;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    if (indexPath.row == 0)
    {
        [self.btnBusinessType setTitle:@"Individual" forState:UIControlStateNormal];
        
    }
    if (indexPath.row == 1)
    {
        [self.btnBusinessType setTitle:@"Company" forState:UIControlStateNormal];
    }
    [self closeTable];
}



-(void) updateConstraints
{
    
    _btnSave.backgroundColor=[UIColor colorWithRed:63.0/255.0f green:67.0/255.0f blue:70.0/255.0f alpha:1.0];
    
    NSAttributedString *star = [[NSAttributedString alloc] initWithString:@"*" attributes:@{ NSForegroundColorAttributeName :[UIColor redColor],
                                                                                             }];
    
    NSMutableAttributedString *str1 = [[NSMutableAttributedString alloc] initWithString:@"Full Name" attributes:@{ NSForegroundColorAttributeName :[UIColor colorWithRed:143.0/255.0f green:143.0/255.0f blue:143.0/255.0f alpha:1.0],
                                                                                                }];
    [str1 appendAttributedString: star];
    _textFldName.attributedPlaceholder = str1;
    
    
    NSMutableAttributedString *str2 = [[NSMutableAttributedString alloc] initWithString:@"Account Number" attributes:@{ NSForegroundColorAttributeName :[UIColor colorWithRed:143.0/255.0f green:143.0/255.0f blue:143.0/255.0f alpha:1.0],
                                                                                                          }];
    [str2 appendAttributedString: star];
    _textFldAccNo.attributedPlaceholder = str2;
    
    NSMutableAttributedString *str3 = [[NSMutableAttributedString alloc] initWithString:@"Re-enter Account Number" attributes:@{ NSForegroundColorAttributeName :[UIColor colorWithRed:143.0/255.0f green:143.0/255.0f blue:143.0/255.0f alpha:1.0],
                                                                                                                }];
    
    [str3 appendAttributedString: star];
    _textFldReenterAccNo.attributedPlaceholder = str3;
    
    
    NSMutableAttributedString *str4 = [[NSMutableAttributedString alloc] initWithString:@"Routing#" attributes:@{ NSForegroundColorAttributeName :[UIColor colorWithRed:143.0/255.0f green:143.0/255.0f blue:143.0/255.0f alpha:1.0],
                                                                                                    }];
    [str4 appendAttributedString: star];
    _textFldRouting.attributedPlaceholder = str4;
    
    NSMutableAttributedString *str5 = [[NSMutableAttributedString alloc] initWithString:@"Bank Name" attributes:@{ NSForegroundColorAttributeName :[UIColor colorWithRed:143.0/255.0f green:143.0/255.0f blue:143.0/255.0f alpha:1.0],
                                                                                                     }];
   [str5 appendAttributedString: star];
    _textFldBankName.attributedPlaceholder = str5;
    
   // NSAttributedString *str6 = [[NSAttributedString alloc] initWithString:@"SSN ID (Last 4 digits only)" attributes:@{ NSForegroundColorAttributeName :[UIColor colorWithRed:143.0/255.0f green:143.0/255.0f blue:143.0/255.0f alpha:1.0],
   //                                                                                                      }];
    NSAttributedString *str6 = [[NSAttributedString alloc] initWithString:@"Last 4 of SSN#" attributes:@{ NSForegroundColorAttributeName :[UIColor colorWithRed:143.0/255.0f green:143.0/255.0f blue:143.0/255.0f alpha:1.0],
                                                                                                                       }];
    
    //Last 4 of SSN#
    //Last 4 EIN or Last 4 of SSN
    
    self.ssnTextFld.attributedPlaceholder = str6;
    
    NSAttributedString *str7 = [[NSAttributedString alloc] initWithString:@"Tax ID" attributes:@{ NSForegroundColorAttributeName :[UIColor colorWithRed:143.0/255.0f green:143.0/255.0f blue:143.0/255.0f alpha:1.0],
                                                                                                  }];
    self.taxIdTxtFld.attributedPlaceholder = str7;

    self.documentImgView.layer.cornerRadius = 2.0;
    self.btnCross.layer.zPosition = 0;
    [self.view layoutIfNeeded];
    
    if (IS_IPHONE_5 || IS_IPHONE_4_OR_LESS)
    {
        self.consHelpAlertViewLeft.constant=20;
        self.consHelpAlertViewTop.constant=125;
        self.consBtnHelpTop.constant = 11;
        
        
        NSLog(@"leading is...%f",self.nsconsBtnRadioFirstLeadingspace.constant);
        // for img view
        self.nsconsImageViewHeight.constant = 0;
        //self.nsconsBtnCrossHeight.constant = 0;
        self.btnCross.hidden = true;
        self.nsconsBtnRadioTop.constant = self.nsconsBtnRadioTop.constant-75;
        self.nsconsLblSavingTop.constant = self.nsconsLblSavingTop.constant-75;
        self.nsconsBtnTransSavingTop.constant = self.nsconsBtnTransSavingTop.constant-75;
        
        
        self.nsconsBtnRadioFirstTop.constant = 17; //main is 27
        self.nsconsBtnTransFirstTop.constant = 13; // main is 23
        
        self.nsconsBtnDOBWidth.constant = 128;
        self.nsconsDOBUnderLineWidth.constant = 132;
        
        self.nsconsLblLegalDocWidth.constant = 90;
        self.nsconsLegalUnderLineLeading.constant =169;
        self.nsconsBtnRadioFirstLeadingspace.constant = 37;
    
        // self.LblViewBG.backgroundColor = [UIColor whiteColor];
        NSLog(@"leading is...%f",self.nsconsBtnRadioFirstLeadingspace.constant);
        
        [self.btnDOB.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:15]];
        [self.btnBusinessType.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:15]];

       // self.lblLegalDoc.font = [UIFont fontWithName:@"Lato-Regular" size:15];
        [self.lblLegalDoc setFont:[UIFont fontWithName:@"Lato-Regular" size:15]];
        self.nsconsLblLegalDocLeading.constant = 23;
        self.nsconsLegalUnderLineWidth.constant = 98;
        
        [self.btnDOB setImageEdgeInsets:UIEdgeInsetsMake(0,118, 0, 0)];
        [self.btnBusinessType setImageEdgeInsets:UIEdgeInsetsMake(0,270, 0, 0)];

        
        
        
        
        self.lblYoutBankDetail.font=[self.lblYoutBankDetail.font fontWithSize:14];
        
        
        self.lblSaving.font=[self.lblSaving.font fontWithSize:15];
        self.lblChecking.font=[self.lblChecking.font fontWithSize:15];
        
        [self.btnSave.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:16]];
        
        
        _consLblYourBankTop.constant=-2;
        _consLblDetailLeft.constant=4;
        
        
        _lblYourBankDetailWidth.constant=310;
        
        _topViewHeight.constant=80;
        _consAllTextFldViewTop.constant=-9;
        
        [self.textFldName setFont:[UIFont fontWithName:@"Lato-Regular" size:15]];
        [self.textFldAccNo setFont:[UIFont fontWithName:@"Lato-Regular" size:15]];
        [self.textFldReenterAccNo setFont:[UIFont fontWithName:@"Lato-Regular" size:15]];
        [self.textFldRouting setFont:[UIFont fontWithName:@"Lato-Regular" size:15]];
        [self.textFldBankName setFont:[UIFont fontWithName:@"Lato-Regular" size:15]];
        [self.ssnTextFld setFont:[UIFont fontWithName:@"Lato-Regular" size:15]];
        [self.taxIdTxtFld setFont:[UIFont fontWithName:@"Lato-Regular" size:15]];
        self.btnBusinessType.titleLabel.font = [UIFont fontWithName:@"Lato-Regular" size:15];
        self.btnDOB.titleLabel.font = [UIFont fontWithName:@"Lato-Regular" size:15];
        
        self.consBtnSaveInfoHeight.constant = 48;
        
        
        _consSavingBtnBottom.constant=95;
        _consSavingBtnLeft.constant=0;
        _consCheckingBtnLeft.constant=0;
        _consChecknigBtnBottm.constant=95;
        
        
        _consSaveBankInfoBtnBottom.constant=60;
        _consBtnSaveInfoHeight.constant=38;
        
        
        // by vs...
        
        self.nsconsImgViewLeading.constant = 8;
        self.nsconsBtnChooseLeading.constant = 6;
        self.nsconsBtnCrossLeading.constant = 210;
        self.nsconsBtnCrossTop.constant = 5;
        //self.nsconsBtnRadioLeading.constant = 5;
        
        // new pay pal screen cons...
        
        self.consPayPalBusinessTxtFldHeight.constant = 40;
        self.consPayPalBusinessTxtFldLeading.constant = 20;
        self.consPayPalBusinessTxtFldTrailing.constant = 20;
        self.consPayPalBusinessTxtFldTop.constant = 160;
        
        self.consLblPayPalPleaseVerifyLeading.constant = 25;
        self.consLblPayPalPleaseVerifyTrailing.constant = 25;
        
        self.consLblPayPalPleaseVerifyTop.constant = 5;
        
        [self.businessIdTxtFld setFont:[UIFont fontWithName:@"Lato-Regular" size:17]];
        
        [self.lblPaypalDescription setFont:[UIFont fontWithName:@"Lato-Regular" size:18]];

    }
    if (IS_IPHONE_6P)
    {
        //CGRec *frame = self.btnHelp.frame.origin.y;
        
       //self.btnHelp.frame.origin.y = 34;
        self.consBtnHelpTop.constant = 15;
        self.consBtnHelpLeft.constant = 8;
        self.consHelpAlertViewTop.constant=200;
        self.consHelpAlertViewLeft.constant=67;
        
        // for img view
        self.nsconsImageViewHeight.constant = 0;
        //self.nsconsBtnCrossHeight.constant = 0;
        self.btnCross.hidden = true;
        self.nsconsBtnRadioTop.constant = self.nsconsBtnRadioTop.constant-65;
        self.nsconsLblSavingTop.constant = self.nsconsLblSavingTop.constant-58;
        self.nsconsBtnTransSavingTop.constant = self.nsconsBtnTransSavingTop.constant-65;
        
        
        self.nsconsBtnDOBWidth.constant = 170;
        self.nsconsDOBUnderLineWidth.constant = 174;
        
        self.nsconsLblLegalDocWidth.constant = 138;
        self.nsconsLegalUnderLineLeading.constant =214;
        self.nsconsLblLegalDocLeading.constant = 25;
        self.nsconsLegalUnderLineWidth.constant = 148;
        [self.btnDOB setImageEdgeInsets:UIEdgeInsetsMake(0,158, 0, 0)];

        self.nsconsImgViewLeading.constant = 23;
        
        
        self.nsconsRadioBtnFirstLeading.constant = 44;
        self.nsconsBtnRadioFirstLeadingspace.constant = 80;
        
        
        
        self.consBtnSaveInfoHeight.constant = 50;
        self.consSaveBankInfoBtnBottom.constant=76;
        
        [self.btnSave.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:21]];
        
        self.lblYoutBankDetail.font=[self.lblYoutBankDetail.font fontWithSize:18];
        
        
        self.lblSaving.font=[self.lblSaving.font fontWithSize:17];
        self.lblChecking.font=[self.lblChecking.font fontWithSize:17];
        
        
        _consLblYourBankTop.constant=11;
        _consLblDetailLeft.constant=11;
        _lblYourBankDetailWidth.constant=390;
        
       
        
        _topViewHeight.constant=80;
       
        
 
        
        
        [self.textFldName setFont:[UIFont fontWithName:@"Lato-Regular" size:17]];
        [self.textFldAccNo setFont:[UIFont fontWithName:@"Lato-Regular" size:17]];
        [self.textFldReenterAccNo setFont:[UIFont fontWithName:@"Lato-Regular" size:17]];
        [self.textFldRouting setFont:[UIFont fontWithName:@"Lato-Regular" size:17]];
        [self.textFldBankName setFont:[UIFont fontWithName:@"Lato-Regular" size:17]];
        [self.ssnTextFld setFont:[UIFont fontWithName:@"Lato-Regular" size:17]];
        [self.taxIdTxtFld setFont:[UIFont fontWithName:@"Lato-Regular" size:17]];
        self.btnBusinessType.titleLabel.font = [UIFont fontWithName:@"Lato-Regular" size:17];
        self.btnDOB.titleLabel.font = [UIFont fontWithName:@"Lato-Regular" size:17];
        
        [self.btnBusinessType setImageEdgeInsets:UIEdgeInsetsMake(0,359, 0, 0)];
        [self.lblLegalDoc setFont:[UIFont fontWithName:@"Lato-Regular" size:17]];

        
        //new
        
        self.consTxtFldNameTop.constant = 17;
        self.consLblUnderNameFldTop.constant = 12;
        
        self.consTxtFldAccNoTop.constant = 13;
        self.consLblUnderAccFldTop.constant = 12;
        
        self.consTxtFldReenterTop.constant = 13;
        self.consLblUnderReEntrAccFldTop.constant = 12;
        
        self.consTxtFldRoutingTop.constant = 13;
        self.consLblUnderRoutingFldTop.constant = 12;
        
        self.consTxtFldRoutingTop.constant = 13;
        self.consLblUnderRoutingFldTop.constant = 12;
        
        self.consTxtFldBankNameTop.constant = 13;
        self.consLblUnderBankNameFldTop.constant = 12;
        
        self.consBtnBusinessTypeTop.constant = 13;
        self.consLblUnderBusinessBtnTop.constant = 12;
        
        self.consTxtFldSSNTop.constant = 13;
        self.consLblUnderSSNFldTop.constant = 12;
        
        self.consTxtFldTaxIdTop.constant = 13;
        self.consLblUnderTaxIDTop.constant = 12;
        
        self.consBtnDOBTop.constant = 13;
        self.consLblUnderDOBBtnTop.constant = 12;
        
        self.consLblLegalIdTop.constant = 13;
        self.consLblUnderLegalIdTop.constant = 12;
        
        self.consBtnChooseTop.constant = 11;
        
        
        // new pay pal screen cons...
        
//        self.consPayPalBusinessTxtFldHeight.constant = 40;
//        self.consPayPalBusinessTxtFldLeading.constant = 20;
//        self.consPayPalBusinessTxtFldTrailing.constant = 20;
        self.consPayPalBusinessTxtFldTop.constant = 195;
        
//        self.consLblPayPalPleaseVerifyLeading.constant = 25;
//        self.consLblPayPalPleaseVerifyTrailing.constant = 25;
        
        self.consLblPayPalPleaseVerifyTop.constant = 12;
        
       // [self.businessIdTxtFld setFont:[UIFont fontWithName:@"Lato-Regular" size:17]];
        
        [self.lblPaypalDescription setFont:[UIFont fontWithName:@"Lato-Regular" size:20]];
    }
    
    if (IS_IPHONE_6)
    {
        
        self.consBtnHelpLeft.constant = 10;
        self.nsconsImageViewHeight.constant = 0;
        //self.nsconsBtnCrossHeight.constant = 0;
        self.btnCross.hidden = true;
        self.nsconsBtnRadioTop.constant = self.nsconsBtnRadioTop.constant-65;
        self.nsconsLblSavingTop.constant = self.nsconsLblSavingTop.constant-65;
        self.nsconsBtnTransSavingTop.constant = self.nsconsBtnTransSavingTop.constant-65;
        
        
        self.consBtnSaveInfoHeight.constant = 50;
        _consTextFldsTop.constant=20;
        _consSaveBankInfoBtnBottom.constant=70;
        [self.btnSave.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:19]];
        self.lblYoutBankDetail.font=[self.lblYoutBankDetail.font fontWithSize:17];
        
        
        self.lblSaving.font=[self.lblSaving.font fontWithSize:16];
        self.lblChecking.font=[self.lblChecking.font fontWithSize:16];
        
        
        _consLblYourBankTop.constant=4;
        _consLblDetailLeft.constant=13;
        
        _topViewHeight.constant=80;
        _consAllTextFldViewTop.constant=-4;
        
        _txtFldNmaeLeft.constant=24;
        _txtFldAcntNoLeft.constant=24;
        _txtFldReAcntLeft.constant=24;
        _txtFldRoutingLeft.constant=24;
        _txtFldBnakNameLeft.constant=24;
        
        
        
        [self.textFldName setFont:[UIFont fontWithName:@"Lato-Regular" size:17]];
        [self.textFldAccNo setFont:[UIFont fontWithName:@"Lato-Regular" size:17]];
        [self.textFldReenterAccNo setFont:[UIFont fontWithName:@"Lato-Regular" size:17]];
        [self.textFldRouting setFont:[UIFont fontWithName:@"Lato-Regular" size:17]];
        [self.textFldBankName setFont:[UIFont fontWithName:@"Lato-Regular" size:17]];
        
        
        _consTextNameHeight.constant=33;
        _consTextAccountHeight.constant=33;
        
        
        _txtFldAccntNoTop.constant=54;
        _lineTwoTop.constant=55;
        
        _txtFldReAcntTop.constant=108;
        _lineThreeTop.constant=110;
        
        _txtFldRoutingTop.constant=164;
        _lineFourTop.constant=165;
        
        _txtFldBnakNmaeTop.constant=220;
        _lineFiveTop.constant=220;
        
        
        self.consBtnRadioSaveBottom.constant = 130;
        self.consBtnRadioCheckingBottom.constant = 130;
        
        self.consLblSavingBottom.constant = 133;
        self.consLblCheckingBottom.constant = 128;
        _consSavingBtnBottom.constant=130;
        _consChecknigBtnBottm.constant=128;
        
        _consSaveBankInfoBtnBottom.constant=72;
        _consBtnSaveInfoHeight.constant=45;
        
        
        _consBtnRadioSavingLeading.constant=56;
        _radioCheckLeft.constant=39;
        
        _consLblChekingTrailing.constant=10;
        
    }
    
    [self.view layoutIfNeeded];
}

#pragma mark - create accessory view

//-(void)createInputAccessoryView{
//    // Create the view that will play the part of the input accessory view.
//    // Note that the frame width (third value in the CGRectMake method)
//    // should change accordingly in landscape orientation. But we don’t care
//    // about that now.
//    inputAccView = [[UIView alloc] initWithFrame:CGRectMake(10.0, 0.0, self.view.frame.size.width, 40.0)];
//    // Set the view’s background color. We’ ll set it here to gray. Use any color you want.
//    [inputAccView setBackgroundColor:[UIColor lightGrayColor]];
//    // We can play a little with transparency as well using the Alpha property. Normally
//    // you can leave it unchanged.
//    [inputAccView setAlpha: 0.8];
//    // If you want you may set or change more properties (ex. Font, background image,e.t.c.).
//    // For now, what we’ ve already done is just enough.
//    // Let’s create our buttons now. First the previous button.
//    btnPrev = [UIButton buttonWithType: UIButtonTypeCustom];
//    // Set the button’ s frame. We will set their widths to 80px and height to 40px.
//    [btnPrev setFrame: CGRectMake(0.0, 0.0, 80.0, 40.0)];
//    // Title.
//    [btnPrev setTitle: @"Previous" forState: UIControlStateNormal];
//    // Background color.
//    [btnPrev setBackgroundColor: [UIColor clearColor]];
//    // You can set more properties if you need to.
//    // With the following command we’ ll make the button to react in finger tapping. Note that the
//    // gotoPrevTextfield method that is referred to the @selector is not yet created. We’ ll create it
//    // (as well as the methods for the rest of our buttons) later.
//    [btnPrev addTarget: self action: @selector(gotoPrevTextfield) forControlEvents: UIControlEventTouchUpInside];
//    // Do the same for the two buttons left.
//    btnNext = [UIButton buttonWithType:UIButtonTypeCustom];
//    [btnNext setFrame:CGRectMake(85.0f, 0.0f, 80.0f, 40.0f)];
//    [btnNext setTitle:@"Next" forState:UIControlStateNormal];
//    [btnNext setBackgroundColor:[UIColor clearColor]];
//    [btnNext addTarget:self action:@selector(gotoNextTextfield) forControlEvents:UIControlEventTouchUpInside];
//    btnDone = [UIButton buttonWithType:UIButtonTypeCustom];
//    [btnDone setFrame:CGRectMake(self.view.frame.size.width - 80.0f, 0.0f, 80.0f, 40.0f)];
//    [btnDone setTitle:@"Done" forState:UIControlStateNormal];
//    [btnDone setBackgroundColor:[UIColor clearColor]];
//    [btnDone setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
//    [btnDone addTarget:self action:@selector(doneTyping) forControlEvents:UIControlEventTouchUpInside];
//    // Now that our buttons are ready we just have to add them to our view.
//
//    [inputAccView addSubview:btnDone];
//}


-(void)createInputAccessoryView{
    // Create the view that will play the part of the input accessory view.
    // Note that the frame width (third value in the CGRectMake method)
    // should change accordingly in landscape orientation. But we don’t care
    // about that now.
    inputAccView = [[UIView alloc] initWithFrame:CGRectMake(10.0, 0.0, self.view.frame.size.width, 40.0)];
    // Set the view’s background color. We’ ll set it here to gray. Use any color you want.
    [inputAccView setBackgroundColor:[UIColor lightGrayColor]];
    // We can play a little with transparency as well using the Alpha property. Normally
    // you can leave it unchanged.
    [inputAccView setAlpha: 0.8];
    // If you want you may set or change more properties (ex. Font, background image,e.t.c.).
    // For now, what we’ ve already done is just enough.
    // Let’s create our buttons now. First the previous button.
    btnPrev = [UIButton buttonWithType: UIButtonTypeCustom];
    // Set the button’ s frame. We will set their widths to 80px and height to 40px.
    [btnPrev setFrame: CGRectMake(0.0, 0.0, 80.0, 40.0)];
    // Title.
    [btnPrev setTitle: @"Previous" forState: UIControlStateNormal];
    // Background color.
    [btnPrev setBackgroundColor: [UIColor clearColor]];
    // You can set more properties if you need to.
    // With the following command we’ ll make the button to react in finger tapping. Note that the
    // gotoPrevTextfield method that is referred to the @selector is not yet created. We’ ll create it
    // (as well as the methods for the rest of our buttons) later.
    [btnPrev addTarget: self action: @selector(gotoPrevTextfield) forControlEvents: UIControlEventTouchUpInside];
    // Do the same for the two buttons left.
    btnNext = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnNext setFrame:CGRectMake(85.0f, 0.0f, 80.0f, 40.0f)];
    [btnNext setTitle:@"Next" forState:UIControlStateNormal];
    [btnNext setBackgroundColor:[UIColor clearColor]];
    [btnNext addTarget:self action:@selector(gotoNextTextfield) forControlEvents:UIControlEventTouchUpInside];
    btnDone = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnDone setFrame:CGRectMake(self.view.frame.size.width - 80.0f, 0.0f, 80.0f, 40.0f)];
    [btnDone setTitle:@"Done" forState:UIControlStateNormal];
    [btnDone setBackgroundColor:[UIColor clearColor]];
    [btnDone setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [btnDone addTarget:self action:@selector(doneTyping) forControlEvents:UIControlEventTouchUpInside];
    // Now that our buttons are ready we just have to add them to our view.
    [inputAccView addSubview:btnPrev];
    [inputAccView addSubview:btnNext];
    [inputAccView addSubview:btnDone];
}


-(void)gotoPrevTextfield{
    // If the active textfield is the first one, can't go to any previous
    // field so just return.
    if (txtActiveField == self.textFldName) {
        [txtActiveField resignFirstResponder];
        return;
    }
    if (txtActiveField == self.textFldAccNo) {
        
        CGRect frame = self.TextFieldHolderVIew.frame;
        frame.origin.y = 0;
        
        [UIView animateWithDuration:0.3 animations:^{
            self.TextFieldHolderVIew.frame = frame;
        } completion:^(BOOL finished) {
            
        }];
        
        [self.textFldAccNo resignFirstResponder];
        [self.textFldName becomeFirstResponder];
        return;
    }
    if (txtActiveField == self.textFldReenterAccNo) {
        
        CGRect frame = self.TextFieldHolderVIew.frame;
        frame.origin.y = 2;
        
        [UIView animateWithDuration:0.3 animations:^{
            self.TextFieldHolderVIew.frame = frame;
        } completion:^(BOOL finished) {
            
        }];
        
        [self.textFldReenterAccNo resignFirstResponder];
        [self.textFldAccNo becomeFirstResponder];
        return;
    }
    if (txtActiveField == self.textFldRouting) {
        
        CGRect frame = self.TextFieldHolderVIew.frame;
        frame.origin.y = -40;
        
        [UIView animateWithDuration:0.3 animations:^{
            self.TextFieldHolderVIew.frame = frame;
        } completion:^(BOOL finished) {
            
        }];
        
        [self.textFldRouting resignFirstResponder];
        [self.textFldReenterAccNo becomeFirstResponder];
        return;
    }
    if (txtActiveField == self.textFldBankName) {
        
        
        CGRect frame = self.TextFieldHolderVIew.frame;
        frame.origin.y = -90;
        
        [UIView animateWithDuration:0.3 animations:^{
            self.TextFieldHolderVIew.frame = frame;
        } completion:^(BOOL finished) {
            
        }];
        
        [self.textFldBankName resignFirstResponder];
        [self.textFldRouting becomeFirstResponder];
        return;
    }
    if (txtActiveField == self.ssnTextFld) {
        
        
        CGRect frame = self.TextFieldHolderVIew.frame;
        frame.origin.y = -130;
        
        [UIView animateWithDuration:0.3 animations:^{
            self.TextFieldHolderVIew.frame = frame;
        } completion:^(BOOL finished) {
            
        }];
        
        [self.ssnTextFld resignFirstResponder];
        [self.textFldBankName becomeFirstResponder];
        return;
    }
    if (txtActiveField == self.taxIdTxtFld) {
        
        
        CGRect frame = self.TextFieldHolderVIew.frame;
        frame.origin.y = -155;
        
        [UIView animateWithDuration:0.3 animations:^{
            self.TextFieldHolderVIew.frame = frame;
        } completion:^(BOOL finished) {
            
        }];
        
        [self.taxIdTxtFld resignFirstResponder];
        [self.ssnTextFld becomeFirstResponder];
        return;
    }
    
}

-(void)gotoNextTextfield{
    // If the active textfield is the second one, there is no next so just return.
    if (txtActiveField == self.textFldName) {
        
        //        CGRect frame1 = self.upperHolderView.frame;
        //        frame1.origin.y = -50;
        
        //        CGRect frame = self.TextFieldHolderVIew.frame;
        //        frame.origin.y = -50;
        //
        //
        //        [UIView animateWithDuration:0.3 animations:^{
        //            //             self.upperHolderView.frame = frame1;
        //
        //            //            [self.view layoutIfNeeded];
        //            //            self.consUpperHolderViewTop.constant = -60;
        //            //            [self.view layoutIfNeeded];
        //
        //            self.TextFieldHolderVIew.frame = frame;
        //
        //        } completion:^(BOOL finished) {
        //
        //        }];
        
        [self.textFldName resignFirstResponder];
        [self.textFldAccNo becomeFirstResponder];
        return;
    }
    if (txtActiveField == self.textFldAccNo) {
        
        //        CGRect frame = self.TextFieldHolderVIew.frame;
        //        frame.origin.y = -50;
        //
        //
        //        [UIView animateWithDuration:0.3 animations:^{
        //            //self.upperHolderView.frame = frame1;
        //
        //            //            [self.view layoutIfNeeded];
        //            //            self.consUpperHolderViewTop.constant = -100;
        //            //            [self.view layoutIfNeeded];
        //
        //
        //            self.TextFieldHolderVIew.frame = frame;
        //
        //        } completion:^(BOOL finished) {
        //
        //        }];
        
        [self.textFldAccNo resignFirstResponder];
        [self.textFldReenterAccNo becomeFirstResponder];
        return;
    }
    if (txtActiveField == self.textFldReenterAccNo) {
        
        CGRect frame = self.TextFieldHolderVIew.frame;
        frame.origin.y = -40;
        
        
        
        [UIView animateWithDuration:0.3 animations:^{
            
            self.TextFieldHolderVIew.frame = frame;
            
        } completion:^(BOOL finished) {
            
        }];
        
        [self.textFldReenterAccNo resignFirstResponder];
        [self.textFldRouting becomeFirstResponder];
        return;
    }
    if (txtActiveField == self.textFldRouting) {
        
        CGRect frame = self.TextFieldHolderVIew.frame;
        frame.origin.y = -95;
        
        [UIView animateWithDuration:0.3 animations:^{
            self.TextFieldHolderVIew.frame = frame;
        } completion:^(BOOL finished) {
            
        }];
        
        [self.textFldRouting resignFirstResponder];
        [self.textFldBankName becomeFirstResponder];
        return;
    }
    if (txtActiveField == self.textFldBankName) {
        
        
        if(IS_IPHONE_4_OR_LESS)
        {
            CGRect frame = self.TextFieldHolderVIew.frame;
            frame.origin.y = -240;
            [UIView animateWithDuration:0.3 animations:^{
                
                self.TextFieldHolderVIew.frame = frame;
                
            } completion:^(BOOL finished) {
                
            }];

        }
        
        else
        {
            CGRect frame = self.TextFieldHolderVIew.frame;
            frame.origin.y = -130;
            [UIView animateWithDuration:0.3 animations:^{
                
                self.TextFieldHolderVIew.frame = frame;
                
            } completion:^(BOOL finished) {
                
            }];
        }
        
        
        
        
        
        [self.textFldBankName resignFirstResponder];
        [self.ssnTextFld becomeFirstResponder];
        // [txtActiveField resignFirstResponder];
        return;
    }
    if (txtActiveField == self.ssnTextFld) {
        
        if(IS_IPHONE_4_OR_LESS)
        {
            CGRect frame = self.TextFieldHolderVIew.frame;
            frame.origin.y = -330;
            [UIView animateWithDuration:0.3 animations:^{
                
                self.TextFieldHolderVIew.frame = frame;
                
            } completion:^(BOOL finished) {
                
            }];

        }
        
        else
        {
            CGRect frame = self.TextFieldHolderVIew.frame;
            frame.origin.y = -155;
            [UIView animateWithDuration:0.3 animations:^{
                
                self.TextFieldHolderVIew.frame = frame;
                
            } completion:^(BOOL finished) {
                
            }];

        }
        
        
        
        
        
        
        
        [self.ssnTextFld resignFirstResponder];
        [self.taxIdTxtFld becomeFirstResponder];

        return;
    }
    if (txtActiveField == self.taxIdTxtFld) {
        
        CGRect frame = self.TextFieldHolderVIew.frame;
        frame.origin.y = 2;
        [UIView animateWithDuration:0.3 animations:^{
            
            self.TextFieldHolderVIew.frame = frame;
            
        } completion:^(BOOL finished) {
            
        }];
        
        
        [txtActiveField resignFirstResponder];
        return;
    }
    
}

-(void)doneTyping{
    // When the "done" button is tapped, the keyboard should go away.
    // That simply means that we just have to resign our first responder.
    
//     [txtActiveField resignFirstResponder];
//    return;
    
    CGRect frame = self.TextFieldHolderVIew.frame;
    frame.origin.y = 0;
    
    [UIView animateWithDuration:0.3 animations:^{
        
        self.TextFieldHolderVIew.frame = frame;
        
    } completion:^(BOOL finished) {
        
    }];
    
    [txtActiveField resignFirstResponder];
    
    [datepicker removeFromSuperview];
    toolBar.hidden=YES;
    [self closeTable];
}


#pragma mark - text field delegate
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    
   // return YES;
    
    [datepicker removeFromSuperview];
    
    
    CGRect frame = self.TextFieldHolderVIew.frame;
    frame.origin.y = 0;
    
    [UIView animateWithDuration:0.3 animations:^{
        self.TextFieldHolderVIew.frame = frame;
    } completion:^(BOOL finished) {
        
    }];
    
    [textField resignFirstResponder];
    
    return YES;
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if(textField == self.ssnTextFld){
        if (textField.text.length < 4 || string.length == 0)
        {
          
            return YES;
        }
        else
        {
            NSLog(@"text more than 4");
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"BUY TIMEE" message:@"Please enter only last 4 digits of SSN ID." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            
            // alert.tag = 10001;
            
            [alert show];
            return NO;
        }
        
}
    return YES;
}
-(void)textFieldDidBeginEditing:(UITextField *)textField{
    
    [self createInputAccessoryView];
    // Now add the view as an input accessory view to the selected textfield.
    [textField setInputAccessoryView:inputAccView];
    // Set the active field. We' ll need that if we want to move properly
    // between our textfields.
    txtActiveField = textField;
    
//    return;
    
    if (txtActiveField == self.textFldName) {
        
    }
    if (txtActiveField == self.textFldAccNo) {
        
        
        //        CGRect frame1 = self.TextFieldHolderVIew.frame;
        //        frame1.origin.y = -10;
        //
        //
        //        CGRect frame = self.TextFieldHolderVIew.frame;
        //        frame.origin.y = -50;
        //
        //        [UIView animateWithDuration:0.3 animations:^{
        //            //  self.upperHolderView.frame = frame1;
        //            self.TextFieldHolderVIew.frame = frame;
        //        } completion:^(BOOL finished) {
        //
        //        }];
    }
    if (txtActiveField == self.textFldReenterAccNo) {
        
        if(IS_IPHONE_4_OR_LESS)
        {
            CGRect frame = self.TextFieldHolderVIew.frame;
            frame.origin.y = -60;
            
            [UIView animateWithDuration:0.3 animations:^{
                self.TextFieldHolderVIew.frame = frame;
                
            } completion:^(BOOL finished) {
                
            }];

        }
        
        else
        {
            CGRect frame = self.TextFieldHolderVIew.frame;
            frame.origin.y = -40;
            
            [UIView animateWithDuration:0.3 animations:^{
                self.TextFieldHolderVIew.frame = frame;
                
            } completion:^(BOOL finished) {
                
            }];

        }
        
        
    }
    if (txtActiveField == self.textFldRouting) {
        
        if(IS_IPHONE_4_OR_LESS)
        {
            CGRect frame = self.TextFieldHolderVIew.frame;
            frame.origin.y = -100;
            
            [UIView animateWithDuration:0.3 animations:^{
                self.TextFieldHolderVIew.frame = frame;
            } completion:^(BOOL finished) {
                
            }];

        }
        else
        {
            CGRect frame = self.TextFieldHolderVIew.frame;
            frame.origin.y = -80;
            
            [UIView animateWithDuration:0.3 animations:^{
                self.TextFieldHolderVIew.frame = frame;
            } completion:^(BOOL finished) {
                
            }];

        }
        
        
    }
    if (txtActiveField == self.textFldBankName) {
        
        
        
        if(IS_IPHONE_4_OR_LESS)
        {
            CGRect frame = self.TextFieldHolderVIew.frame;
            frame.origin.y = -140;
            
            [UIView animateWithDuration:0.3 animations:^{
                //  self.upperHolderView.frame = frame1;
                self.TextFieldHolderVIew.frame = frame;
            } completion:^(BOOL finished) {
                
            }];

        }
        
        else
        {
            CGRect frame = self.TextFieldHolderVIew.frame;
            frame.origin.y = -95;
            
            [UIView animateWithDuration:0.3 animations:^{
                //  self.upperHolderView.frame = frame1;
                self.TextFieldHolderVIew.frame = frame;
            } completion:^(BOOL finished) {
                
            }];

        }
        
        
    }
    if (txtActiveField == self.ssnTextFld) {
        
        
        if(IS_IPHONE_4_OR_LESS)
        {
            CGRect frame = self.TextFieldHolderVIew.frame;
            frame.origin.y = -240;
            
            [UIView animateWithDuration:0.3 animations:^{
                //  self.upperHolderView.frame = frame1;
                self.TextFieldHolderVIew.frame = frame;
            } completion:^(BOOL finished) {
                
            }];

        }
        
        else
        {
            CGRect frame = self.TextFieldHolderVIew.frame;
            frame.origin.y = -130;
            
            [UIView animateWithDuration:0.3 animations:^{
                //  self.upperHolderView.frame = frame1;
                self.TextFieldHolderVIew.frame = frame;
            } completion:^(BOOL finished) {
                
            }];

        }
        
    }
    if (txtActiveField == self.taxIdTxtFld) {
        
        
        
        
        if(IS_IPHONE_4_OR_LESS)
        {
            CGRect frame = self.TextFieldHolderVIew.frame;
            frame.origin.y = -330;
            
            [UIView animateWithDuration:0.3 animations:^{
                //  self.upperHolderView.frame = frame1;
                self.TextFieldHolderVIew.frame = frame;
            } completion:^(BOOL finished) {
                
            }];

        }
        else
        {
            CGRect frame = self.TextFieldHolderVIew.frame;
            frame.origin.y = -155;
            
            [UIView animateWithDuration:0.3 animations:^{
                //  self.upperHolderView.frame = frame1;
                self.TextFieldHolderVIew.frame = frame;
            } completion:^(BOOL finished) {
                
            }];

        }
        
        
    }
    
    
    
}

- (NSString *)getIPAddress {
    
    NSString *address = @"error";
    struct ifaddrs *interfaces = NULL;
    struct ifaddrs *temp_addr = NULL;
    int success = 0;
    // retrieve the current interfaces - returns 0 on success
    success = getifaddrs(&interfaces);
    if (success == 0) {
        // Loop through linked list of interfaces
        temp_addr = interfaces;
        while(temp_addr != NULL) {
            if(temp_addr->ifa_addr->sa_family == AF_INET) {
                // Check if interface is en0 which is the wifi connection on the iPhone
                if([[NSString stringWithUTF8String:temp_addr->ifa_name] isEqualToString:@"en0"]) {
                    // Get NSString from C String
                    address = [NSString stringWithUTF8String:inet_ntoa(((struct sockaddr_in *)temp_addr->ifa_addr)->sin_addr)];
                    
                }
                
            }
            
            temp_addr = temp_addr->ifa_next;
        }
    }
    // Free memory
    freeifaddrs(interfaces);
    return address;
    
}

- (BOOL)validateEmailWithString:(NSString*)checkString
{
    BOOL stricterFilter = NO; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSString *laxString = @".+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}



-(void)sendPayPalEmailToServer
{
    
    if (![[WebService shared] connected]) {
        
        [GlobalFunction removeIndicatorView];
        
        [[GlobalFunction shared] showAlertForMessage:@"Can't connect to internet, please check and try again."];
        
        return;
    }
    if ([[self.businessIdTxtFld.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""]) {
        
        [GlobalFunction removeIndicatorView];
        
        [[GlobalFunction shared] showAlertForMessage:@"PayPal business email id cannot be blank."];
        
        return;
        
    }
    
    
    bool checkId = [self validateEmailWithString:self.businessIdTxtFld.text];
    
    if (!checkId)
    {
        [GlobalFunction removeIndicatorView];
        
        [[GlobalFunction shared] showAlertForMessage:@"Please enter a valid business id."];
        
        return;
    }
    
    NSDictionary *dictBioResponse;
    dictBioResponse = [[WebService shared] AddEmailUserid:myAppDelegate.userData.userid emailId:self.businessIdTxtFld.text];

    
    if (dictBioResponse) {
        
           NSLog(@"response --- %@",[EncryptDecryptFile decrypt:[dictBioResponse valueForKey:@"responseMessage"]]);
        
           NSLog(@"response --- %@",[EncryptDecryptFile decrypt:[dictBioResponse valueForKey:@"responseCode"]]);
        
        if ([[EncryptDecryptFile decrypt:[dictBioResponse valueForKey:@"responseCode"]] isEqualToString:@"200"]) {
            
            NSArray *dictDataReg = [dictBioResponse objectForKey:@"data"];
            
            NSDictionary *dictFromResponse = [dictBioResponse objectForKey:@"data"];
            
            
//            email = self.businessIdTxtFld.text;
            
//            [emailCheck setObject:email forKey:@"businessEmailPayPal"];
//            
//            [emailCheck synchronize];
            
                   // myAppDelegate.isBankDetailFilled = true;
                  //  myAppDelegate.isBankInfoFilled = true;

            
           
            
                
                [self bioUpdatedSuccefully:[EncryptDecryptFile decrypt:[dictBioResponse valueForKey:@"responseMessage"]]];
                
                
            }else{
                
                [[GlobalFunction shared] showAlertForMessage:[EncryptDecryptFile decrypt:[dictBioResponse valueForKey:@"responseMessage"]]];
                
                [GlobalFunction removeIndicatorView];
                
            }
            
            
        }
        
    else
    {
        
        [[GlobalFunction shared] showAlertForMessage:@"Server is down, please try again later."];
        
        [GlobalFunction removeIndicatorView];
        
    }
    

    
    
}
-(void)updateBioToServer{
    
   
    if (![[WebService shared] connected]) {
        
        [GlobalFunction removeIndicatorView];
        
        [[GlobalFunction shared] showAlertForMessage:@"Can't connect to internet, please check and try again."];
        
        return;
    }
    
    
    
    if ([[self.textFldName.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""]) {
        
        [GlobalFunction removeIndicatorView];
        
        [[GlobalFunction shared] showAlertForMessage:@"Name field cannot be blank."];
        
        return;
        
    }
    else
    {
        NSString *foo = self.textFldName.text;
        NSRange whiteSpaceRange = [foo rangeOfCharacterFromSet:[NSCharacterSet whitespaceCharacterSet]];
        if (whiteSpaceRange.location != NSNotFound) {
            NSLog(@"Found whitespace");
            
        }
        else
        {
            NSLog(@"Not Found whitespace");
            [GlobalFunction removeIndicatorView];
            
            [[GlobalFunction shared] showAlertForMessage:@"Please enter your full name."];
            
            return;
        }
    }
    
    NSString *first = self.textFldAccNo.text;
    NSString *second = self.textFldReenterAccNo.text;
    if (first.length <= 0) {
        
        [GlobalFunction removeIndicatorView];
        
        [[GlobalFunction shared] showAlertForMessage:@"Please enter your account number."];
        
        return;
    }
    if (second.length <= 0) {
        
        [GlobalFunction removeIndicatorView];
        
        [[GlobalFunction shared] showAlertForMessage:@"Please re-enter your account number."];
        
        return;
    }
    if (![first isEqualToString:second]) {
        
        [GlobalFunction removeIndicatorView];
        
        [[GlobalFunction shared] showAlertForMessage:@"Entered account number did not match."];
        
        return;
    }
    
    
    if ([[self.textFldRouting.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""]) {
        
        [GlobalFunction removeIndicatorView];
        
        
        [[GlobalFunction shared] showAlertForMessage:@"Routing field cannot be blank."];
        
        return;
        
    }
    
    if ([[self.textFldBankName.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""]) {
        
        [GlobalFunction removeIndicatorView];
        
        
        [[GlobalFunction shared] showAlertForMessage:@"Bank name cannot be blank."];
        
        return;
        
    }
    if (![[self.ssnTextFld.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""])
    {
       NSString *len = self.ssnTextFld.text;
        
        NSUInteger length = [len length];
        
        if (length != 4)
        {
            [GlobalFunction removeIndicatorView];
            
            
            [[GlobalFunction shared] showAlertForMessage:@"SSN ID must be 4 digits."];
            
            return;

        }
    }
    //  CLLocationCoordinate2D coordinate = [[GlobalFunction shared] getLocationFromAddressString:self.textFieldEmail.text];
    
        NSString *imageName1 = [NSString stringWithFormat:@"userProfilePicture_%@_%@.jpeg",myAppDelegate.userData.userid,TimeStamp];
    //
    //    imageName1 = @"main11.jpeg";
    
    
    NSLog(@" The button's title is %@.",  myAppDelegate.Address_FromBioScreen);
    NSLog(@" The button's title is %@.",  self.btnBusinessType.currentTitle);
    
    if (myAppDelegate.Address_FromBioScreen.length <= 0)
    {
        myAppDelegate.Address_FromBioScreen = @"";
    }
    
    NSString *title = self.btnBusinessType.currentTitle;
    if ([title isEqualToString:@"Type"])
    {
        title = @"";
    }
    else
    {
        title = [title lowercaseString];
    }
    NSString *ipAddress = @"";
      ipAddress = [self getIPAddress];
    
    
    NSString *sendDate = self.btnDOB.currentTitle;
    if ([sendDate isEqualToString:@"Date Of Birth"])
    {
        sendDate = @"";
    }
    else
    {
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:dateFormatMDY];
        NSDate *dateMain = [dateFormat dateFromString:sendDate];
        [dateFormat setDateFormat:dateFormatFull];
        NSString* dateString = [dateFormat stringFromDate:dateMain];//    [dateFormat setDateFormat:dateFormatFull];
        startDate =  [dateFormat stringFromDate:dateMain];
    }
    
    NSDictionary *dictBioResponse;
    
    
    if(self.btnCross.hidden == true)
    {
       
        dictBioResponse = [[WebService shared]updateBankProfileuserId:myAppDelegate.userData.userid name:self.textFldName.text accountNo:self.textFldAccNo.text routingNo:self.textFldRouting.text bankName:self.textFldBankName.text accType:accType ip:ipAddress dob:startDate ssn:self.ssnTextFld.text type:title address:myAppDelegate.Address_FromBioScreen taxId:self.taxIdTxtFld.text andUserImage:nil andImageName:@""]; // old work to call paypal work

       // dictBioResponse = [[WebService shared]updateBankProfileuserId:myAppDelegate.userData.userid name:self.textFldName.text accountNo:self.textFldAccNo.text routingNo:self.textFldRouting.text bankName:self.textFldBankName.text accType:accType ip:ipAddress dob:startDate ssn:self.ssnTextFld.text type:title address:myAppDelegate.Address_FromBioScreen taxId:self.taxIdTxtFld.text andUserImage:nil andImageName:@""];
    }
    
    else
    {
     dictBioResponse = [[WebService shared]updateBankProfileuserId:myAppDelegate.userData.userid name:self.textFldName.text accountNo:self.textFldAccNo.text routingNo:self.textFldRouting.text bankName:self.textFldBankName.text accType:accType ip:ipAddress dob:startDate ssn:self.ssnTextFld.text type:title address:myAppDelegate.Address_FromBioScreen taxId:self.taxIdTxtFld.text andUserImage:self.documentImgView.image andImageName:imageName1];  // old work to call paypal work
    //    dictBioResponse = [[WebService shared]updateBankProfileuserId:myAppDelegate.userData.userid name:self.textFldName.text accountNo:self.textFldAccNo.text routingNo:self.textFldRouting.text bankName:self.textFldBankName.text accType:accType];
    }
    if (dictBioResponse) {
        
    //    NSLog(@"response --- %@",[EncryptDecryptFile decrypt:[dictBioResponse valueForKey:@"responseMessage"]]);
        
     //   NSLog(@"response --- %@",[EncryptDecryptFile decrypt:[dictBioResponse valueForKey:@"responseCode"]]);
        
        if ([[EncryptDecryptFile decrypt:[dictBioResponse valueForKey:@"responseCode"]] isEqualToString:@"200"]) {
            
            NSArray *dictDataReg = [dictBioResponse objectForKey:@"data"];
            
            NSDictionary *dictFromResponse = [dictBioResponse objectForKey:@"data"];
            
            
            if (dictDataReg) {
                
                
                NSString *striprIdd = [EncryptDecryptFile decrypt:[dictFromResponse valueForKey:@"stripeId"]];
                
                if (striprIdd.length>0) {
                    
                    myAppDelegate.isBankDetailFilled = true;
                    myAppDelegate.selectedPaymentTypeFromServer = @"1";
                    myAppDelegate.isStripeBankInfoFilled = true;
                    myAppDelegate.iscashOutPayPallFilled = false;
                    [myAppDelegate.createBioVCObj setRadioButtonImages];

                }
                
                // [dictTemp0 setValue:[NSArray arrayWithObject:[dictFromResponse valueForKey:@"stripeId"]] forKey:@"stripeId"];
                
                
                // NSDictionary *dictFromResponse = [[NSDictionary alloc]init];
                // NSDictionary *dictFromResponse  = [dictDataReg firstObject];
                
                //                NSMutableDictionary *dictTemp0 = [[NSMutableDictionary alloc]init];
                //
                //                [dictTemp0 setValue:[NSArray arrayWithObject:[dictFromResponse valueForKey:@"userId"]] forKey:@"userId"];
                //                [dictTemp0 setValue:[NSArray arrayWithObject:[dictFromResponse valueForKey:@"name"]] forKey:@"name"];
                //                [dictTemp0 setValue:[NSArray arrayWithObject:[dictFromResponse valueForKey:@"emailId"]] forKey:@"emailId"];
                //                [dictTemp0 setValue:[NSArray arrayWithObject:[dictFromResponse valueForKey:@"instaLink"]] forKey:@"instaLink"];
                //                [dictTemp0 setValue:[NSArray arrayWithObject:[dictFromResponse valueForKey:@"location"]] forKey:@"location"];
                //                [dictTemp0 setValue:[NSArray arrayWithObject:[dictFromResponse valueForKey:@"description"]] forKey:@"description"];
                //                [dictTemp0 setValue:[NSArray arrayWithObject:[dictFromResponse valueForKey:@"picPath"]] forKey:@"picPath"];
                //                [dictTemp0 setValue:[NSArray arrayWithObject:[dictFromResponse valueForKey:@"avgRating"]] forKey:@"avgRating"];
                //                [dictTemp0 setValue:[NSArray arrayWithObject:[dictFromResponse valueForKey:@"customerId"]] forKey:@"customerId"];
                //                [dictTemp0 setValue:[NSArray arrayWithObject:[dictFromResponse valueForKey:@"stripeId"]] forKey:@"stripeId"];
                //
                //                NSString *query = [NSString stringWithFormat:@"delete from %@",tableUser];
                //                [DatabaseClass executeQuery:query];
                //
                //                if (isPictureAvailabe) {
                //
                //                    [GlobalFunction removeImage:imageName1 dirname:@"vender"];
                //
                //                    myAppDelegate.userData.userImageName = imageName1;
                //
                //                    NSArray *components = [myAppDelegate.userData.userImageName componentsSeparatedByString:@"."];
                //
                //                    imageName1 = [components firstObject];
                //
                //                    [GlobalFunction saveimage:self.imgViewUsersPicture.image imageName:@"main11" dirname:@"vender"];
                //
                //                }
                //                   else if (isOldPictureAvailable){
                //
                //
                //                }else{
                //
                //
                //                }
                //
                //                query=[NSString stringWithFormat:@"insert into %@ values(?,?,?,?,?,?,?,?,?,?)",tableUser];
                //                [DatabaseClass saveUserData:[query UTF8String] fromDict:dictTemp0];
                //
                //                NSMutableArray *arr = [DatabaseClass getDataFromUserData:[[NSString stringWithFormat:@"Select * from %@",tableUser] UTF8String]];
                //
                //                myAppDelegate.userData = nil;
                //                myAppDelegate.userData = (user *)[arr firstObject];
                //
                //                //                NSDictionary *userDict = [UserDefaults objectForKey:keyUserloginDetailDictionary];
                //                //
                //                //                NSDictionary *dictUserData = [NSDictionary dictionaryWithObjects:@[[EncryptDecryptFile EncryptIT:self.textFieldUsername.text],[userDict valueForKey:keyPass],myAppDelegate.userData.userid,[userDict valueForKey:keysocialFlag_fb],[userDict valueForKey:keysocialEmailId_fb],[userDict valueForKey:keysocialId_fb]] forKeys:@[keyUsername,keyPass,keyUserId,keysocialFlag_fb,keysocialEmailId_fb,keysocialId_fb]];
                //                //
                //                //
                //                //                [UserDefaults setObject:dictUserData forKey:keyUserloginDetailDictionary];
                //                //                [UserDefaults synchronize];
                //
                //                [myAppDelegate.vcObj.tblViewSideBar reloadData];
                
                //[GlobalFunction saveimage:self.imgViewUsersPicture.image imageName:imageName1 dirname:@"vender"];
                
                //                NSMutableDictionary *dicBankInfo=[[NSMutableDictionary alloc] init];
                //                [dicBankInfo setValue:self.textFldName.text forKey:@"name"];
                //                [dicBankInfo setValue:self.textFldAccNo.text forKey:@"AccNo"];
                //                // [dicBankInfo setValue:self.textFldReenterAccNo.text forKey:@"Yelp"];
                //                [dicBankInfo setValue:self.textFldRouting.text forKey:@"Routing"];
                //                [dicBankInfo setValue:self.textFldBankName.text forKey:@"BankName"];
                //                [dicBankInfo setValue:accType forKey:@"AccType"];
                //
                //                [UserDefaults setObject:dicBankInfo forKey:keyBankInfoDetails];
                //                [UserDefaults synchronize];
                
                
                [self bioUpdatedSuccefully:[EncryptDecryptFile decrypt:[dictBioResponse valueForKey:@"responseMessage"]]];
                
                
            }else{
                
                [[GlobalFunction shared] showAlertForMessage:[EncryptDecryptFile decrypt:[dictBioResponse valueForKey:@"responseMessage"]]];
                
                [GlobalFunction removeIndicatorView];
                
            }
            
            
        }else{
            
            [[GlobalFunction shared] showAlertForMessage:[EncryptDecryptFile decrypt:[dictBioResponse valueForKey:@"responseMessage"]]];
            
            [GlobalFunction removeIndicatorView];
            
        }
        
    }else{
        
        [[GlobalFunction shared] showAlertForMessage:@"Server is down, please try again later."];
        
        [GlobalFunction removeIndicatorView];
        
    }
    
    
}

-(void)bioUpdatedSuccefully:(NSString *)response{
    
    [GlobalFunction removeIndicatorView];
    
    
    NSLog(@"successful .......");
    
    
    
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"BUY TIMEE" message:response delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    
    alert.tag = 10001;
    
    [alert show];
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    
    if (alertView.tag == 10001){
        if(buttonIndex == 0)//OK button pressed
        {
            //do something
            NSLog(@"ook pressed !!!");
            
            // self.textFldReenterAccNo.text=@"";
            
            [myAppDelegate.vcObj backToMainView:nil];
            
            
        }
        else if(buttonIndex == 1)//Annul button pressed.
        {
            //do something
        }
    }
    if (alertView.tag == 10101){
        if(buttonIndex == 0)//OK button pressed
        {
            //do something
            NSLog(@"ook pressed !!!");
            
            // self.textFldReenterAccNo.text=@"";
            
            //  [myAppDelegate.vcObj backToMainView:nil];
            
            
        }
        else //Annul button pressed.
        {
            
            NSLog(@"top is...%f",self.nsconsBtnRadioTop.constant);
            NSLog(@"top is...%f",self.nsconsLblSavingTop.constant);
            NSLog(@"top is...%f",self.nsconsBtnRadioTop.constant);
            NSLog(@"top is...%f",self.nsconsBtnTransSavingTop.constant);
            
             [self.view layoutIfNeeded];
            if (IS_IPHONE_6)
            {
            // for img view
                
                [UIView animateWithDuration:0.5 animations:^{
                    [self.view layoutIfNeeded];
                } completion:^(BOOL finished) {
                    
                    self.nsconsImageViewHeight.constant = 0;
                    //self.nsconsBtnCrossHeight.constant = 0;
                    self.btnCross.hidden = true;
                    self.nsconsBtnRadioTop.constant = self.nsconsBtnRadioTop.constant-65;
                    self.nsconsLblSavingTop.constant = self.nsconsLblSavingTop.constant-65;
                    self.nsconsBtnTransSavingTop.constant = self.nsconsBtnTransSavingTop.constant-65;

                    
                }];
 
                        }
            if (IS_IPHONE_6P)
            {
                // for img view
                self.nsconsImageViewHeight.constant = 0;
                //self.nsconsBtnCrossHeight.constant = 0;
                self.btnCross.hidden = true;
                self.nsconsBtnRadioTop.constant = self.nsconsBtnRadioTop.constant-65;
                self.nsconsLblSavingTop.constant = self.nsconsLblSavingTop.constant-65;
                self.nsconsBtnTransSavingTop.constant = self.nsconsBtnTransSavingTop.constant-65;
            }
            if (IS_IPHONE_5 || IS_IPHONE_4_OR_LESS) {
                self.nsconsImageViewHeight.constant = 0;
                //self.nsconsBtnCrossHeight.constant = 0;
                self.btnCross.hidden = true;
                self.nsconsBtnRadioTop.constant = self.nsconsBtnRadioTop.constant-75;
                self.nsconsLblSavingTop.constant = self.nsconsLblSavingTop.constant-75;
                self.nsconsBtnTransSavingTop.constant = self.nsconsBtnTransSavingTop.constant-75;
                
                
                self.nsconsBtnRadioFirstTop.constant = 17; //main is 27
                self.nsconsBtnTransFirstTop.constant = 13; // main is 23
            }
               [self.view layoutIfNeeded];
            NSLog(@"top is...%f",self.nsconsBtnRadioTop.constant);
            NSLog(@"top is...%f",self.nsconsLblSavingTop.constant);
            NSLog(@"top is...%f",self.nsconsBtnRadioTop.constant);
            NSLog(@"top is...%f",self.nsconsBtnTransSavingTop.constant);
        }
    }
    
}

- (IBAction)savingBtnClciked:(id)sender {
    
    [self radioSavingClicked:self];
    
    
    
}
- (IBAction)checkingBtnClciked:(id)sender {
    
    
    [self radioCheckingClicked:self];
    
    
    
}
- (IBAction)businessTypeClicked:(id)sender
{
    [txtActiveField resignFirstResponder];
    
    [self openTable];
    
}

-(void)cancelPressed:(id)sender
{
    NSLog(@"done button click");
   // [datepicker removeFromSuperview];
    
    
    
    toolBar.hidden=YES;
    
    [self.btnDOB setTitle: @"Date Of Birth" forState: UIControlStateNormal];
    
    startDate = @"";
    startTime = @"";
    [self doneTyping];


}

- (IBAction)DOBClicked:(id)sender
{
     [txtActiveField resignFirstResponder];
    
    if(IS_IPHONE5)
    {
        toolBar= [[UIToolbar alloc] initWithFrame:CGRectMake(0,257,320,44)];
    }
    else if (IS_IPHONE_6)
    {
        toolBar= [[UIToolbar alloc] initWithFrame:CGRectMake(0,370,380,44)];
    }
    else if (IS_IPHONE_6P)
    {
        toolBar= [[UIToolbar alloc] initWithFrame:CGRectMake(0,370,420,44)];
    }
    else if (IS_IPHONE_4_OR_LESS)
    {
        toolBar= [[UIToolbar alloc] initWithFrame:CGRectMake(0,185,320,44)];

    }
    
    
    NSMutableArray *barItemsArray = [[NSMutableArray alloc] init];
    UIBarButtonItem *barButtonDone = [[UIBarButtonItem alloc] initWithTitle:@"Done"
                                                                      style:UIBarButtonItemStyleBordered target:self action:@selector(changeDateFromLabel:)];
    
   
    
    UIBarButtonItem *space = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    
    
    
    
    UIBarButtonItem *barButtonCancel = [[UIBarButtonItem alloc] initWithTitle:@"Clear"
                                                                        style:UIBarButtonItemStyleBordered target:self action:@selector(cancelPressed:)];
    
    
      
    [barItemsArray addObject:barButtonDone];
    [barItemsArray addObject:space];
    [barItemsArray addObject:barButtonCancel];
    
    toolBar.backgroundColor=[UIColor colorWithRed:63.0/255 green:67.0/255 blue:70.0/255 alpha:1.0];
    barButtonDone.tintColor=[UIColor colorWithRed:63.0/255 green:67.0/255 blue:70.0/255 alpha:1.0];
    barButtonCancel.tintColor=[UIColor colorWithRed:63.0/255 green:67.0/255 blue:70.0/255 alpha:1.0];
    [toolBar setItems:barItemsArray animated:YES];
    [self.view addSubview:toolBar];
    
    
    [datepicker addTarget:self action:@selector(dueDateChanged:) forControlEvents:UIControlEventValueChanged];
    CGSize pickerSize = [datepicker sizeThatFits:CGSizeZero];
    
    if(IS_IPHONE5)
    {
        datepicker.frame = CGRectMake(0.0, 300, pickerSize.width, 200);
    }
    
    else if (IS_IPHONE_6){
        datepicker.frame = CGRectMake(0.0, 410, 380, 200);
        
    }
    else if (IS_IPHONE_6P){
        datepicker.frame = CGRectMake(0.0, 410, 420, 250);
    }
    else if (IS_IPHONE_4_OR_LESS)
    {
        datepicker.frame = CGRectMake(0.0, 220, pickerSize.width, 200);
    }
    
    datepicker.backgroundColor = [UIColor colorWithRed:63.0/255.0f green:67.0/255.0f blue:70.0/255.0f alpha:1.0];


    
    
    
    // [UIColor colorWithRed:63.0/255.0f green:67.0/255.0f blue:70.0/255.0f alpha:1.0];
    [datepicker setValue:[UIColor whiteColor] forKey:@"textColor"];
    
    
    [self.view addSubview:datepicker];
    
    
    CGRect frame = self.TextFieldHolderVIew.frame;
    frame.origin.y = -142;
    
    [UIView animateWithDuration:0.3 animations:^{
        //  self.upperHolderView.frame = frame1;
        self.TextFieldHolderVIew.frame = frame;
    } completion:^(BOOL finished) {
        
    }];
    
    [datepicker setDatePickerMode:UIDatePickerModeDate];
    
    
    
}
-(void) dueDateChanged:(UIDatePicker *)sender {
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:dateFormatFull];
    
    NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
    [outputFormatter setDateFormat:dateFormatOnlyHour];
    
    NSString *timetofill = [outputFormatter stringFromDate:datepicker.date];
    
    dateStart = datepicker.date;
    NSString *dateVal = [dateFormat stringFromDate:datepicker.date];
    
    NSDateFormatter *dtFormatMDY = [[NSDateFormatter alloc] init];
    [dtFormatMDY setDateFormat:dateFormatMDY];
    
    NSString *dateString = [dtFormatMDY  stringFromDate:datepicker.date];

    
    
    NSDateFormatter *formatter00 = [[NSDateFormatter alloc] init];
    [formatter00 setDateFormat:@"hh:mm a"];
    NSString *timetoShow = [formatter00 stringFromDate:datepicker.date];
    NSLog(@"Current Date: %@", [formatter00 stringFromDate:datepicker.date]);
    
    // dateString = [[dateString stringByAppendingString:@"  "] stringByAppendingString:timetoShow];
    [self.btnDOB setTitle: dateString forState: UIControlStateNormal];
    
    startDate = @"";
    startTime = @"";
    startDate=dateVal;
    startTime=timetofill;
    
    
    
    
}

-(void)changeDateFromLabel:(id)sender
{
    
    [self dueDateChanged:nil];
    
    NSLog(@"done button click");
    [datepicker removeFromSuperview];
    
    toolBar.hidden=YES;
    [self doneTyping];
}

- (IBAction)chooseBtnClicked:(id)sender
{
    
    [datepicker removeFromSuperview];
    toolBar.hidden=YES;
    
    [txtActiveField resignFirstResponder];
    
    UIActionSheet *popup = [[UIActionSheet alloc] initWithTitle:@"Select option:" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:
                            @"Select Existing",
                            @"Take a Photo", nil];
    popup.tag = 1;
    
    //    myAppDelegate.actionS=popup;
    //    myAppDelegate.isActionOpen=true;
    [popup showInView:[UIApplication sharedApplication].keyWindow];
    
    
    
    
    
    
    
}

- (void)actionSheet:(UIActionSheet *)popup clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    switch (popup.tag) {
        case 1: {
            switch (buttonIndex) {
                case 0:
                    [self openPhotos :(int)buttonIndex];
                    break;
                case 1:
                    [self openPhotos:(int)buttonIndex];
                    break;
                default:
                    break;
            }
            break;
        }
        default:
            break;
    }
}




#pragma mark - camera and library

-(void)openPhotos :(int)someInt{
    
    picker = [[UIImagePickerController alloc] init];
    
    if (someInt==0) {
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    }
    else{
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    }
    
    picker.delegate = (id)self;
    [myAppDelegate.window.rootViewController presentViewController:picker animated:YES completion:nil];
    // [myAppDelegate.window.rootViewController presentViewController:picker animated:true completion:^{
    
    //        myAppDelegate.isPhotoOpen=true;
    //        myAppDelegate.isActionOpen=false;
    
    //  }];
}







-(void)showCamera
{
    
   
    
    picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = true;
    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    [myAppDelegate.window.rootViewController presentViewController:picker animated:true completion:^{
    }];
    
}



- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    
    [myAppDelegate.window.rootViewController dismissViewControllerAnimated:true completion:nil];
    isfromMedia = YES;
    //    [self dismissViewControllerAnimated:true completion:^{
    //     //   myAppDelegate.isPhotoOpen=false;
    //
    //    }];
}
/** IMAGE PICKEER DELEGETES **/
- (void)imagePickerController:(UIImagePickerController *)picker1 didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info{
    
    
    isfromMedia = YES;
    
    UIImage *userImage = [info objectForKey:UIImagePickerControllerOriginalImage];
    
    
    
    
    if (picker1.sourceType == UIImagePickerControllerSourceTypeCamera) {
        
        if(picker1.cameraDevice==UIImagePickerControllerCameraDeviceFront)
        {
            userImage = [UIImage imageWithCGImage:userImage.CGImage scale:1.0 orientation:UIImageOrientationLeftMirrored];
            
        }
        else
        {
            userImage = [UIImage imageWithCGImage:userImage.CGImage scale:1.0 orientation:UIImageOrientationRight];
        }
        
        
    }
    else
    {
        //         userImage = [UIImage imageWithCGImage:userImage.CGImage scale:1.0 orientation:UIImageOrientationUp];
    }
    
    
    
    
    
    
    
    
    
    //  NSData *imageData=UIImageJPEGRepresentation(userImage, 1.0);
    
    //    isImage=true;
    //    myAppDelegate.objCVC.doneButton.enabled=true;
    //    myAppDelegate.objCVC.doneButton.alpha=1.0;
    
    self.documentImgView.clipsToBounds = true;
    self.documentImgView.image = userImage;
    isfromMedia = YES;
    [self.view layoutIfNeeded];
    
    if(IS_IPHONE_6)
    {
        
        // for img view
        self.nsconsImageViewHeight.constant = 65;
        //self.nsconsBtnCrossHeight.constant = 0;
        self.btnCross.hidden = false;
        self.nsconsBtnRadioTop.constant = self.nsconsBtnRadioTop.constant+65;
        self.nsconsLblSavingTop.constant = self.nsconsLblSavingTop.constant+65;
        self.nsconsBtnTransSavingTop.constant = self.nsconsBtnTransSavingTop.constant+65;
    }
    if(IS_IPHONE_6P)
    {
        // for img view
        self.nsconsImageViewHeight.constant = 65;
        //self.nsconsBtnCrossHeight.constant = 0;
        self.btnCross.hidden = false;
        self.nsconsBtnRadioTop.constant = self.nsconsBtnRadioTop.constant+65;
        self.nsconsLblSavingTop.constant = self.nsconsLblSavingTop.constant+65;
        self.nsconsBtnTransSavingTop.constant = self.nsconsBtnTransSavingTop.constant+65;
    }
    if (IS_IPHONE_5 || IS_IPHONE_4_OR_LESS)
    {
        self.nsconsImageViewHeight.constant = 65;
        //self.nsconsBtnCrossHeight.constant = 0;
        self.btnCross.hidden = false;
        self.nsconsImgViewLeading.constant = 22;
        self.nsconsBtnCrossLeading.constant = 80;
        self.nsconsBtnCrossTop.constant = 25;
        
        self.nsconsBtnRadioTop.constant = self.nsconsBtnRadioTop.constant+75;
        self.nsconsLblSavingTop.constant = self.nsconsLblSavingTop.constant+75;
        self.nsconsBtnTransSavingTop.constant = self.nsconsBtnTransSavingTop.constant+75;
        
        
        self.nsconsBtnRadioFirstTop.constant = 27; //main is 27
        self.nsconsBtnTransFirstTop.constant = 23; // main is 23
    }
    
     [self.view layoutIfNeeded];
    
    [myAppDelegate.window.rootViewController dismissViewControllerAnimated:true completion:nil];
    
    //    [self dismissViewControllerAnimated:true completion:^{
    //        self.documentImgView.clipsToBounds = true;
    ////        userImage = [self CreateAResizeImage:userImage ThumbSize:CGSizeMake(300, 300)];
    //        self.documentImgView.image = userImage;
    ////        self.overlay.hidden=false;
    ////        myAppDelegate.isPhotoOpen=false;
    //        
    //        isfromMedia = YES;
    //    }];
    
    
}


- (IBAction)crossBtnClicked:(id)sender
{
    
    UIAlertView *alertView11 = [[UIAlertView alloc] initWithTitle:@"BUY TIMEE" message:@"Are you sure to remove this ID." delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Remove", nil];
    alertView11.tag = 10101;
    
    [alertView11 show];
    
}
- (IBAction)TransBtnOverIdClicked:(id)sender
{
    [datepicker removeFromSuperview];
    toolBar.hidden=YES;
    
    [self chooseBtnClicked:self.btnChoose];
}
- (IBAction)helpBtnClicked:(id)sender
{
    
    [self doneTyping];
    
    
    [UIView transitionWithView:self.btnHelp
                      duration:0.4
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^{
                        self.blackTransparentView.hidden = false;
                        self.blackTransparentView.alpha = 1.0;
                        
                    }
                    completion:NULL];
    
}
-(IBAction)okBtnClicked:(id)sender
{
    [UIView animateWithDuration:0.3 animations:^{
        self.blackTransparentView.alpha = 0;
    } completion: ^(BOOL finished) {//creates a variable (BOOL) called "finished" that is set to *YES* when animation IS completed.
        self.blackTransparentView.hidden = true;//if animation is finished ("finished" == *YES*), then hidden = "finished" ... (aka hidden = *YES*)
    }];
}
@end
