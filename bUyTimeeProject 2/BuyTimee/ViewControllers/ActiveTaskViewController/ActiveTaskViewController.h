//
//  ActiveTaskViewController.h
//  HandApp
//


#import <UIKit/UIKit.h>
#import "Flurry.h"

@interface ActiveTaskViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>


@property (weak, nonatomic) IBOutlet UIView *viewTaskDetailHolder;
@property (weak, nonatomic) IBOutlet UITableView *tblActiveTask;

-(void)reloadTable;


@property (weak, nonatomic) IBOutlet UIWebView *webViewAboutUs;
@property (weak, nonatomic) IBOutlet UILabel *aboutUsLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consAboutUsLblTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consAboutUsLblLeft;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consAboutUslblWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consWebViewHeight;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consLblAboutUsHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consWebViewTop;



@end
