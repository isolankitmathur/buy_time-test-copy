//
//  ActiveTaskViewController.m
//  HandApp
//


#import "ActiveTaskViewController.h"
#import "TaskDetailTableViewCell.h"
#import "Constants.h"
#import "task.h"
#import "GlobalFunction.h"
#import "WorkerEmployerTaskDetail.h"
#import "subCategory.h"

@interface ActiveTaskViewController ()<UIWebViewDelegate>

@property (weak, nonatomic) IBOutlet UILabel *lblNoTaskListAvailable;
- (IBAction)animateImageViewToFullScreen:(id)sender;
@property (strong, nonatomic) IBOutlet UITapGestureRecognizer *imgViewTapGesture;

@end

@implementation ActiveTaskViewController{
    
    NSArray *arrayOfTaskWithStatus1Or2;

    //created by user data variables
    NSString *userID;
    NSString *userName;
    NSString *picPath;
    NSString *mobileNo;
    NSString *lat;
    NSString *longitude;
    NSString *location;
    NSString *email;
    NSString *userDescription;
    
    task *taskDataObj;
    
    NSMutableArray *createdByTaskUserInfoArray;

}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
   // myAppDelegate.vcObj.lblTitleText.text = @"Active Tasks";
    
    NSString* final = @"About_Us detail page";
    
    [Flurry logEvent:final];
    
    myAppDelegate.vcObj.btnMapOnListView.hidden = true;//kp
    myAppDelegate.vcObj.BtnTransparentMap.hidden = true;//kp


    self.tblActiveTask.delegate = self;
    self.tblActiveTask.dataSource = self;
    
    [self.tblActiveTask layoutIfNeeded];
    [self.tblActiveTask reloadData];
    [self.tblActiveTask layoutIfNeeded];
    
//    self.lblNoTaskListAvailable.text = @"No active task list available currently.";
//    self.lblNoTaskListAvailable.hidden = YES;

    
 // NSDictionary*dictReg = [webServicesShared aboutUsToken:myAppDelegate.deviceTokenString];
    NSDictionary*dictReg = [webServicesShared aboutUsToken:@"1234"];
    NSArray *arrayTerms;
    NSString * result = @"";
    NSString *myDescriptionHTML = @"";
    
    NSLog(@"main file is..%@",[EncryptDecryptFile decrypt:[dictReg valueForKey:@"responseCode"]]);
    NSLog(@"main file is..%@",[EncryptDecryptFile decrypt:[dictReg valueForKey:@"responseMessage"]]);
    
    NSLog(@"main file is..%@",[EncryptDecryptFile decrypt:result]);
    if (dictReg != nil) {
        
        if ([[EncryptDecryptFile decrypt:[dictReg valueForKey:@"responseCode"]] isEqualToString:@"200"]) {
            NSDictionary *dictDataReg = [dictReg objectForKey:@"data"];
            
            // arrayTerms= [dictDataReg valueForKey:@"description"];
            result= [dictDataReg valueForKey:@"description"];
            
            myDescriptionHTML = [NSString stringWithFormat:@"<html> \n"
                                 "<head> \n"
                                 "<style type=\"text/css\"> \n"
                                 "body {font-family: \"%@\"; font-size: %f;}\n"
                                 "</style> \n"
                                 "</head> \n"
                                 "<body>%@</body> \n"
                                 "</html>", @"Lato-Light", 14.0, [EncryptDecryptFile decrypt:result]];
            
        }
    }
    else
    {
        
        myDescriptionHTML = [NSString stringWithFormat:@"<html> \n"
                             "<head> \n"
                             "<style type=\"text/css\"> \n"
                             "body {font-family: \"%@\"; font-size: %f;}\n"
                             "</style> \n"
                             "</head> \n"
                             "<body>%@</body> \n"
                             "</html>", @"Lato-Light", 14.0, @"Network error, please try again in few minutes."];
        
    }
    
    NSLog(@"main file is..%@",[EncryptDecryptFile decrypt:result]);

    [self.webViewAboutUs loadHTMLString:myDescriptionHTML baseURL:nil];
    self.webViewAboutUs.delegate = self;

}



-(void)webViewDidFinishLoad:(UIWebView *)webView{
    [GlobalFunction removeIndicatorView];
}

-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    [GlobalFunction removeIndicatorView];
}



-(void)viewDidAppear:(BOOL)animated{
    
    
    if(myAppDelegate.isFromCreateBioOnTnC == true)
    {
        myAppDelegate.TandC_Buyee=@"home";
        myAppDelegate.isFromCreateBioOnTnC = true;
    }

    
    
    
//    if (myAppDelegate.isComingBackAfterMapOpen) {
//
//        myAppDelegate.isComingBackAfterMapOpen = false;
//        
//    }else{
//        
//     //   myAppDelegate.vcObj.lblTitleText.text = @"Active Tasks";
//        
//        [self.view layoutIfNeeded];
//        
//        [GlobalFunction addIndicatorView];
//        
//        [self performSelector:@selector(performUpdateWork) withObject:nil afterDelay:0.1];
//
//    }
   // self.webViewAboutUs.backgroundColor = [UIColor redColor];
    self.aboutUsLabel.hidden = true;
    [self.view layoutIfNeeded];
    if(IS_IPHONE_6)
        {
            _consWebViewHeight.constant=550;
            self.consLblAboutUsHeight.constant = 0;
            
        }
    if(IS_IPHONE_6P)
    {
        _consWebViewHeight.constant=620;
        self.consLblAboutUsHeight.constant = 0;
        
    }
    if(IS_IPHONE_5 || IS_IPHONE_4_OR_LESS)
    {
        _consWebViewHeight.constant=440;
        self.consLblAboutUsHeight.constant = 0;
        
    }
    [self.view layoutIfNeeded];
}


-(void)performUpdateWork{
    
    if (![self checkForInternet]) {
        
        [self performSelector:@selector(reloadTable) withObject:nil afterDelay:0.2];
        
        return;
    }
    
    if ([[GlobalFunction shared] callUpdateWebservice]) {
        
        [self performSelector:@selector(reloadTable) withObject:nil afterDelay:0.2];
        
    }else{
        
        [GlobalFunction removeIndicatorView];
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"BUY TIMEE" message:@"Failed to update, please try again." delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Retry", nil];
        
        alertView.tag = 999;
        
        [alertView show];
        
    }
    
}

-(void)updateWebServiceHandling{
    
    if (![self checkForInternet]) {
        
        [self performSelector:@selector(reloadTable) withObject:nil afterDelay:0.2];
        
        return;
    }
    
    if ([[GlobalFunction shared] callUpdateWebservice]) {
        
        [self performSelector:@selector(reloadTable) withObject:nil afterDelay:0.2];
        
    }else{
        
        [GlobalFunction removeIndicatorView];
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"BUY TIMEE" message:@"Failed to update, please try again." delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Retry", nil];
        
        alertView.tag = 999;
        
        [alertView show];
        
    }
    
}

-(BOOL)checkForInternet{
    
    BOOL isConnected = true;
    
    if (![[WebService shared] connected]) {
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"BUY TIMEE" message:@"Can't connect to internet, update not successful. Please check your internet connection then try again." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        [alertView show];
        
        isConnected = false;
        
    }
    
    return isConnected;
}

-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex{
    
    if (alertView.tag == 999) {
        
        if (buttonIndex == [alertView cancelButtonIndex]) {
            
            [self performSelector:@selector(reloadTable) withObject:nil afterDelay:0.2];
            
        }else{
            
            [GlobalFunction addIndicatorView];
            
            [self performSelector:@selector(updateWebServiceHandling) withObject:nil afterDelay:0.2];
            
        }
        
    }
    
}


-(void)reloadTable{
    
    if (!(myAppDelegate.arrayTaskDetail.count>0)) {
        
        self.tblActiveTask.hidden = YES;
        self.lblNoTaskListAvailable.hidden = NO;
        self.lblNoTaskListAvailable.alpha = 0.0;
        [UIView animateWithDuration:0.3 animations:^{
            self.lblNoTaskListAvailable.alpha = 1.0;
        }];
        
    }else{
        
        arrayOfTaskWithStatus1Or2 = [GlobalFunction filterTaskForUserID:myAppDelegate.userData.userid taskStatus:TMActiveTask_Accepted];
        
        [self.tblActiveTask layoutIfNeeded];
        [self.tblActiveTask reloadData];
        [self.tblActiveTask layoutIfNeeded];

        if (arrayOfTaskWithStatus1Or2.count>0) {
            self.tblActiveTask.hidden = NO;
            self.lblNoTaskListAvailable.hidden = YES;
            
        }else{
            self.tblActiveTask.hidden = YES;
            self.lblNoTaskListAvailable.hidden = NO;
            self.lblNoTaskListAvailable.alpha = 0.0;
            [UIView animateWithDuration:0.3 animations:^{
                self.lblNoTaskListAvailable.alpha = 1.0;
            }];
            
        }
    }
    
    [self.tblActiveTask layoutIfNeeded];
    [self.tblActiveTask reloadData];
    [self.tblActiveTask layoutIfNeeded];
    
    [GlobalFunction removeIndicatorView];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return arrayOfTaskWithStatus1Or2.count;
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    TaskDetailTableViewCell *cell = (TaskDetailTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"cellTaskDetail2"];
    
    cell.activityIndicator.hidden=true;
    [cell.activityIndicator stopAnimating];

    task *taskObj = (task *)[arrayOfTaskWithStatus1Or2 objectAtIndex:indexPath.row];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:dateFormatFull];
    NSDate *date = [dateFormatter dateFromString:taskObj.startDate];
    [dateFormatter setDateFormat:dateFormatDate];
    NSString *dateString = [dateFormatter stringFromDate:date];
    
    cell.lblTaskName.text = taskObj.title;
    if ([GlobalFunction checkIfContainFloat:taskObj.price]) {
        cell.lblTaskPrice.text = [NSString stringWithFormat:@"$%0.2f",taskObj.price];
    }else{
        cell.lblTaskPrice.text = [NSString stringWithFormat:@"$%0.0f",taskObj.price];
    }
    cell.lblTaskDate.text = dateString;
    cell.lblAddress.text = taskObj.locationName;
    
    for (int i = 0; i<myAppDelegate.arrayCategoryDetail.count; i++) {
        
        if ([[myAppDelegate.arrayCategoryDetail objectAtIndex:i] isKindOfClass:[subCategory class]]){
            
            subCategory *obj = [myAppDelegate.arrayCategoryDetail objectAtIndex:i];
            
            if ([obj.subCategoryId isEqualToString:taskObj.subcategoryId]) {
                
                cell.lblTaskEmployerName.text = obj.name;
                
                break;

            }
            
        }
        
    }
    
    cell.imgViewUserProfile.image = imageUserDefault;
    cell.imgViewUserProfile.tag = indexPath.row;
    //adjust radius tap gesture
    self.imgViewTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(animateImageViewToFullScreen:)];
    self.imgViewTapGesture.delegate = self;
    self.imgViewTapGesture.numberOfTapsRequired = 1;
    [cell.imgViewUserProfile addGestureRecognizer:self.imgViewTapGesture];

    //handling label width
    CGRect r = [GlobalFunction rectForLabel:cell.lblTaskName];
    CGRect r2 = [GlobalFunction rectForLabel:cell.lblTaskPrice];
    CGRect r3 = [GlobalFunction rectForLabel:cell.lblTaskDate];
    
    int roundedUp1 = ceil(r.size.width);
    int roundedUp2 = ceil(r2.size.width);
    int roundedUp3 = ceil(r3.size.width);
    
    [tableView layoutIfNeeded];
    
    [cell layoutIfNeeded];
    
    //for adjusting task name
    CGFloat remainingWidth = SCREEN_WIDTH - 8 - 39 - 14 - (18*2) - roundedUp3;    //space for user image and its leading and trailing
    
    CGFloat remainingWidthAfterRemovingSeperatorLabelSpace = remainingWidth - 80;
    remainingWidthAfterRemovingSeperatorLabelSpace = remainingWidthAfterRemovingSeperatorLabelSpace - 20; //constant trailing space for date label
    
    if (roundedUp2>80) {
        if (roundedUp1>remainingWidthAfterRemovingSeperatorLabelSpace) {
            cell.constraintLblTaskNameWidth.constant = remainingWidthAfterRemovingSeperatorLabelSpace;
            cell.constraintLblPriceWidth.constant = 80;
            cell.constraintLblDateWidth.constant = roundedUp3;
            cell.constraintLblDateTrailingSpace.constant = 20;
        }else{
            remainingWidthAfterRemovingSeperatorLabelSpace = remainingWidthAfterRemovingSeperatorLabelSpace - roundedUp1;
            cell.constraintLblTaskNameWidth.constant = roundedUp1;
            cell.constraintLblDateWidth.constant = roundedUp3;
            remainingWidthAfterRemovingSeperatorLabelSpace = remainingWidthAfterRemovingSeperatorLabelSpace + 80;
            if (roundedUp2>remainingWidthAfterRemovingSeperatorLabelSpace) {
                cell.constraintLblPriceWidth.constant = remainingWidthAfterRemovingSeperatorLabelSpace;
                cell.constraintLblDateTrailingSpace.constant = 20;
            }else{
                cell.constraintLblPriceWidth.constant = roundedUp2;
                remainingWidthAfterRemovingSeperatorLabelSpace = remainingWidthAfterRemovingSeperatorLabelSpace - roundedUp2;
                cell.constraintLblDateTrailingSpace.constant = 20+remainingWidthAfterRemovingSeperatorLabelSpace;
            }
        }
    }else{
        remainingWidthAfterRemovingSeperatorLabelSpace = remainingWidthAfterRemovingSeperatorLabelSpace + (80-roundedUp2);
        if (roundedUp1>remainingWidthAfterRemovingSeperatorLabelSpace) {
            cell.constraintLblTaskNameWidth.constant = remainingWidthAfterRemovingSeperatorLabelSpace;
            cell.constraintLblPriceWidth.constant = roundedUp2;
            cell.constraintLblDateWidth.constant = roundedUp3;
            cell.constraintLblDateTrailingSpace.constant = 20;
        }else{
            remainingWidthAfterRemovingSeperatorLabelSpace = remainingWidthAfterRemovingSeperatorLabelSpace - roundedUp1;
            cell.constraintLblTaskNameWidth.constant = roundedUp1;
            cell.constraintLblDateWidth.constant = roundedUp3;
            cell.constraintLblPriceWidth.constant = roundedUp2;
            cell.constraintLblDateTrailingSpace.constant = 20+remainingWidthAfterRemovingSeperatorLabelSpace;
        }
    }
    
    [cell layoutIfNeeded];
    
    [tableView layoutIfNeeded];
    
    cell.lblTaskName.lineBreakMode = NSLineBreakByTruncatingTail;
    cell.lblTaskPrice.lineBreakMode = NSLineBreakByTruncatingTail;

    CGRect r4 = [GlobalFunction rectForLabel:cell.lblAddress];
    CGRect r5 = [GlobalFunction rectForLabel:cell.lblTaskEmployerName];
    
    int roundedUp4 = ceil(r4.size.width);
    int roundedUp5 = ceil(r5.size.width);

    //for adjusting task location
    remainingWidth = SCREEN_WIDTH - 8 - 39 - 14 - 18 - 18;
    
    if (roundedUp4>remainingWidth && roundedUp5>remainingWidth) {
        
        remainingWidthAfterRemovingSeperatorLabelSpace = (remainingWidth-20)/20;
        cell.constraintLblAddressWidth.constant = remainingWidthAfterRemovingSeperatorLabelSpace;
        cell.constraintLblUserNameWidth.constant = remainingWidthAfterRemovingSeperatorLabelSpace;
        cell.constraintLblUserNameTrailingSpace.constant = 20;
        
        cell.lblTaskEmployerName.lineBreakMode = NSLineBreakByTruncatingTail;
        cell.lblAddress.lineBreakMode = NSLineBreakByTruncatingTail;

    }else if(roundedUp4>remainingWidth){
        
        remainingWidthAfterRemovingSeperatorLabelSpace = remainingWidth - roundedUp5;
        remainingWidthAfterRemovingSeperatorLabelSpace = remainingWidthAfterRemovingSeperatorLabelSpace - 20;   //constant trailing space for name label
        
        if (roundedUp4 >= remainingWidthAfterRemovingSeperatorLabelSpace) {
            cell.constraintLblAddressWidth.constant = remainingWidthAfterRemovingSeperatorLabelSpace;
            cell.constraintLblUserNameTrailingSpace.constant = 20;
        }else{
            remainingWidthAfterRemovingSeperatorLabelSpace = remainingWidthAfterRemovingSeperatorLabelSpace - roundedUp4;
            cell.constraintLblAddressWidth.constant = roundedUp4;
            cell.constraintLblUserNameTrailingSpace.constant = 20+remainingWidthAfterRemovingSeperatorLabelSpace;
        }
        cell.constraintLblUserNameWidth.constant = roundedUp5;
        
        cell.lblAddress.lineBreakMode = NSLineBreakByTruncatingTail;

    }else if (roundedUp5>remainingWidth){
        
        remainingWidthAfterRemovingSeperatorLabelSpace = remainingWidth - roundedUp4;
        remainingWidthAfterRemovingSeperatorLabelSpace = remainingWidthAfterRemovingSeperatorLabelSpace - 20;
        
        if (roundedUp5>=remainingWidthAfterRemovingSeperatorLabelSpace) {
            cell.constraintLblUserNameWidth.constant = remainingWidthAfterRemovingSeperatorLabelSpace;
            cell.constraintLblUserNameTrailingSpace.constant = 20;
        }else{
            remainingWidthAfterRemovingSeperatorLabelSpace = remainingWidthAfterRemovingSeperatorLabelSpace - roundedUp5;
            cell.constraintLblUserNameWidth.constant = roundedUp5;
            cell.constraintLblUserNameTrailingSpace.constant = 20+remainingWidthAfterRemovingSeperatorLabelSpace;
        }
        cell.constraintLblAddressWidth.constant = r4.size.width;
        
        cell.lblTaskEmployerName.lineBreakMode = NSLineBreakByTruncatingTail;

    }else if((roundedUp4 + roundedUp5) > remainingWidth){
        
        if (roundedUp4<roundedUp5) {
            
            remainingWidthAfterRemovingSeperatorLabelSpace = remainingWidth - roundedUp4;
            remainingWidthAfterRemovingSeperatorLabelSpace = remainingWidthAfterRemovingSeperatorLabelSpace - 20;
            
            if (roundedUp5>=remainingWidthAfterRemovingSeperatorLabelSpace) {
                cell.constraintLblUserNameWidth.constant = remainingWidthAfterRemovingSeperatorLabelSpace;
                cell.constraintLblUserNameTrailingSpace.constant = 20;
            }else{
                remainingWidthAfterRemovingSeperatorLabelSpace = remainingWidthAfterRemovingSeperatorLabelSpace - roundedUp5;
                cell.constraintLblUserNameWidth.constant = roundedUp5;
                cell.constraintLblUserNameTrailingSpace.constant = 20+remainingWidthAfterRemovingSeperatorLabelSpace;
            }
            cell.constraintLblAddressWidth.constant = roundedUp4;

            cell.lblTaskEmployerName.lineBreakMode = NSLineBreakByTruncatingTail;

        }else{
            
            remainingWidthAfterRemovingSeperatorLabelSpace = remainingWidth - roundedUp5;
            remainingWidthAfterRemovingSeperatorLabelSpace = remainingWidthAfterRemovingSeperatorLabelSpace - 20;   //constant trailing space for name label
            
            if (roundedUp4 >= remainingWidthAfterRemovingSeperatorLabelSpace) {
                cell.constraintLblAddressWidth.constant = remainingWidthAfterRemovingSeperatorLabelSpace;
                cell.constraintLblUserNameTrailingSpace.constant = 20;
            }else{
                remainingWidthAfterRemovingSeperatorLabelSpace = remainingWidthAfterRemovingSeperatorLabelSpace - roundedUp4;
                cell.constraintLblAddressWidth.constant = roundedUp4;
                cell.constraintLblUserNameTrailingSpace.constant = 20+remainingWidthAfterRemovingSeperatorLabelSpace;
            }
            cell.constraintLblUserNameWidth.constant = roundedUp5;

            cell.lblAddress.lineBreakMode = NSLineBreakByTruncatingTail;

        }
        
        
    }else if ((roundedUp4 + roundedUp5) < remainingWidth){
        
        remainingWidthAfterRemovingSeperatorLabelSpace = remainingWidth - roundedUp4;
        remainingWidthAfterRemovingSeperatorLabelSpace = remainingWidthAfterRemovingSeperatorLabelSpace - roundedUp5;
        
        cell.constraintLblAddressWidth.constant = roundedUp4;
        cell.constraintLblUserNameWidth.constant = roundedUp5;
        
        cell.constraintLblUserNameTrailingSpace.constant = remainingWidthAfterRemovingSeperatorLabelSpace;
        
    }else{
        
    }
    
    [cell layoutIfNeeded];
    
    [tableView layoutIfNeeded];
    
    if ([taskObj.userid isEqualToString:myAppDelegate.userData.userid]) {
        
        if ([myAppDelegate.userData.picpath isEqualToString:@""]) {
            cell.imgViewUserProfile.image = imageUserDefault;
        }else{
            
            if (myAppDelegate.userData.userImage) {
                
                taskObj.userImage = myAppDelegate.userData.userImage;
                taskObj.userImageName = myAppDelegate.userData.userImageName;
                taskObj.picpath = myAppDelegate.userData.picpath;
                
                cell.imgViewUserProfile.image = taskObj.userImage;
                
            }
            
        }
        
    }else{
        
        //************ Download user image *************************************************************************
        
        if ([taskObj.picpath isEqualToString:@""]) {
            cell.imgViewUserProfile.image = imageUserDefault;
        }else{
            
            if (taskObj.userImage) {
                cell.imgViewUserProfile.image = taskObj.userImage;
            }else{
                
                NSArray *components = [taskObj.picpath componentsSeparatedByString:@"/"];
                
                NSString *imgNameWithJPEG = [components lastObject];
                
                NSArray *componentsWithDots = [imgNameWithJPEG componentsSeparatedByString:@"."];
                
                if (componentsWithDots.count>2) {
                    
                    imgNameWithJPEG = [NSString stringWithFormat:@"%@.%@",[componentsWithDots firstObject],[componentsWithDots lastObject]];
                    
                }

                UIImage *img = [GlobalFunction loadImageOfLogoFromCacheFolder:imgNameWithJPEG dirname:@"user"];
                
                if (img) {
                    taskObj.userImage = img;
                    cell.imgViewUserProfile.image = img;
                }else{
                    
                    NSString *image = taskObj.picpath;
                    NSString *fullPath=[NSString stringWithFormat:urlServer,image];
                    NSString* webStringURL = [fullPath stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                    
                    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH,  0ul);
                    dispatch_async(queue, ^{
                        //This is what you will load lazily
                        NSURL *imageURL=[NSURL URLWithString:webStringURL];
                        
                        NSData *images=[NSData dataWithContentsOfURL:imageURL];
                        
                        NSArray *componentsArr = [image componentsSeparatedByString:@"/"];
                        
                        NSString *imgNameWithJPEG = [componentsArr lastObject];
                        
                        NSArray *componentsArr2 = [imgNameWithJPEG componentsSeparatedByString:@"."];
                        
                        NSString *imgNameWithoutJPEG = [componentsArr2 firstObject];
                        
                        UIImage *img = [UIImage imageWithData:images];
                        
                        if (img) {
                            
                            dispatch_sync(dispatch_get_main_queue(), ^{
                                
                                UIImage *img = [UIImage imageWithData:images];
                                
                                [GlobalFunction saveimage:img imageName:imgNameWithoutJPEG dirname:@"user"];
                                
//                                TaskDetailTableViewCell *updateCell = (TaskDetailTableViewCell*)[tableView cellForRowAtIndexPath:indexPath];
//                                UIImageView *view = (UIImageView*)[updateCell viewWithTag:indexPath.row];
//                                view.clipsToBounds=YES;
//                                
//                                task *updateObj = (task *)[arrayOfTaskWithStatus1Or2 objectAtIndex:indexPath.row];
//                                updateObj.userImage = img;
//                                updateCell.imgViewUserProfile.image = updateObj.userImage;
//                                updateCell.imgViewUserProfile.layer.cornerRadius = updateCell.imgViewUserProfile.frame.size.height/2;
//                                updateCell.imgViewUserProfile.layer.masksToBounds = YES;
//                                updateCell.imgViewUserProfile.layer.shouldRasterize = YES;

                            });
                            
                        }
                        
                    });
                    
                }
            }
            
        }
        
        //*****************************************************************************
        
    }

    [cell layoutIfNeeded];
    
    [tableView layoutIfNeeded];

    return cell;
    
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (tableView == self.tblActiveTask) {
        
        // Remove seperator inset
        if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
            [cell setSeparatorInset:UIEdgeInsetsZero];
        }
        
        // Prevent the cell from inheriting the Table View's margin settings
        if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
            [cell setPreservesSuperviewLayoutMargins:NO];
        }
        
        // Explictly set your cell's layout margins
        if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
            [cell setLayoutMargins:UIEdgeInsetsZero];
        }
        
    }
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 65;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [GlobalFunction addIndicatorView];
    
    myAppDelegate.taskData = nil;
    
    myAppDelegate.taskData = [arrayOfTaskWithStatus1Or2 objectAtIndex:indexPath.row];
    
    [self performSelector:@selector(fetchTaskInfo) withObject:nil afterDelay:0.2];

}

-(void)fetchTaskInfo{
    
    if (![[WebService shared] connected]) {
        
        [GlobalFunction removeIndicatorView];
        
        [[GlobalFunction shared] showAlertForMessage:@"Can't connect to internet, please check and try again."];
                
        return;
    }
    
    NSDictionary *dictTaskInfoResponse = [[WebService shared] viewTaskInfoTaskid:myAppDelegate.taskData.taskid userid:myAppDelegate.userData.userid timeZone:@"" view:@""];
    
    if (dictTaskInfoResponse) {
        
        if ([[EncryptDecryptFile decrypt:[dictTaskInfoResponse valueForKey:@"responseCode"]] isEqualToString:@"200"]) {
            
            NSLog(@"response message == %@",[EncryptDecryptFile decrypt:[dictTaskInfoResponse valueForKey:@"responseMessage"]]);
            
            NSDictionary *dictDataReg = [dictTaskInfoResponse objectForKey:@"data"];
            
            if (dictDataReg != nil) {
                
                if (myAppDelegate.isOfOtherUser) {
                    
                    myAppDelegate.workerOtherUserTaskDetail = nil;
                    
                    myAppDelegate.workerOtherUserTaskDetail = [[WorkerEmployerTaskDetail alloc]init];
                    
                    [self fillWorkerTaskDataForWorkerTaskDataObj:myAppDelegate.workerOtherUserTaskDetail fromDictionary:dictDataReg andUserData:myAppDelegate.userOtherData];
                    
                }else{
                    
                    myAppDelegate.workerTaskDetail = nil;
                    
                    myAppDelegate.workerTaskDetail = [[WorkerEmployerTaskDetail alloc] init];
                    
                    [self fillWorkerTaskDataForWorkerTaskDataObj:myAppDelegate.workerTaskDetail fromDictionary:dictDataReg andUserData:myAppDelegate.userData];
                    
                }
                
                [self performSelector:@selector(performSegueForTaskDetail) withObject:nil afterDelay:0.5];
                
            }else{
                
                [GlobalFunction removeIndicatorView];
                
                [[GlobalFunction shared] showAlertForMessage:@"Server is down, please try again later."];
                
            }
            
            
        }else{
            
            [GlobalFunction removeIndicatorView];
            
            [[GlobalFunction shared] showAlertForMessage:[EncryptDecryptFile decrypt:[dictTaskInfoResponse valueForKey:@"responseMessage"]]];
            
        }
        
    }else{
        
        [GlobalFunction removeIndicatorView];
        
        [[GlobalFunction shared] showAlertForMessage:@"Server is down, please try again later."];
        
    }
    

}

-(void)fillWorkerTaskDataForWorkerTaskDataObj:(WorkerEmployerTaskDetail *)obj fromDictionary:(NSDictionary *)dict andUserData:(user *)userObj{
    
    
    NSArray *arrayTerms= [dict valueForKey:@"Accepted By"];
    if ([[arrayTerms objectAtIndex:0] isKindOfClass:[NSString class]]) {
        
        obj.isTaskAccepted = false;
        
    }else if ([[arrayTerms objectAtIndex:0] isKindOfClass:[NSDictionary class]]){
        
        obj.isTaskAccepted = true;
        
        NSDictionary *dictTemp0=[arrayTerms objectAtIndex:0];
        
        obj.userId_acceptedBy = [EncryptDecryptFile decrypt:[dictTemp0 valueForKey:@"userId"]];
        obj.userName_acceptedBy = [EncryptDecryptFile decrypt:[dictTemp0 valueForKey:@"userName"]];
        obj.description_acceptedBy = [EncryptDecryptFile decrypt:[dictTemp0 valueForKey:@"description"]];
        obj.emailId_acceptedBy = [EncryptDecryptFile decrypt:[dictTemp0 valueForKey:@"emailId"]];
        obj.latitude_acceptedBy = [[EncryptDecryptFile decrypt:[dictTemp0 valueForKey:@"latitude"]] floatValue];
        obj.longitude_acceptedBy = [[EncryptDecryptFile decrypt:[dictTemp0 valueForKey:@"longitude"]] floatValue];
        obj.locationName_acceptedBy = [EncryptDecryptFile decrypt:[dictTemp0 valueForKey:@"location"]];
        obj.mobile_acceptedBy = [EncryptDecryptFile decrypt:[dictTemp0 valueForKey:@"mobile"]];
        obj.picPath_acceptedBy = [EncryptDecryptFile decrypt:[dictTemp0 valueForKey:@"picPath"]];
        obj.avg_rating_acceptedBy = [EncryptDecryptFile decrypt:[dictTemp0 valueForKey:@"avg_rating"]];
        
    }
    
    //task accepted by rating detail
    obj.arrayRatings_acceptedBy = [[NSMutableArray alloc]init];
    arrayTerms= [dict valueForKey:@"Worker Rating Detail"];
    if ([[arrayTerms objectAtIndex:0] isKindOfClass:[NSString class]]) {
        
        obj.rating_acceptedBy = [EncryptDecryptFile decrypt:[arrayTerms objectAtIndex:0]];
        
    }else if ([[arrayTerms objectAtIndex:0] isKindOfClass:[NSDictionary class]]){
        
        NSInteger noOfStars = 0;
        for (NSDictionary *dicts in arrayTerms) {
            
            NSString *stars = [EncryptDecryptFile decrypt:[dicts valueForKey:@"ratingStar"]];
            noOfStars = noOfStars + [stars integerValue];
            
            obj.ratingObj = nil;
            obj.ratingObj = [[rating alloc]init];
            
            obj.ratingObj.ratingdescription = [EncryptDecryptFile decrypt:[dicts valueForKey:@"description"]];
            obj.ratingObj.employerId = [EncryptDecryptFile decrypt:[dicts valueForKey:@"employerId"]];
            obj.ratingObj.ratingId = [EncryptDecryptFile decrypt:[dicts valueForKey:@"ratingId"]];
            obj.ratingObj.ratingStar = [EncryptDecryptFile decrypt:[dicts valueForKey:@"ratingStar"]];
            obj.ratingObj.taskId = [EncryptDecryptFile decrypt:[dicts valueForKey:@"taskId"]];
            obj.ratingObj.workerId = [EncryptDecryptFile decrypt:[dicts valueForKey:@"workerId"]];
            obj.ratingObj.userName = [EncryptDecryptFile decrypt:[dicts valueForKey:@"userName"]];
            
            [obj.arrayRatings_acceptedBy addObject:obj.ratingObj];
        }
        CGFloat avg = noOfStars/arrayTerms.count;
        obj.avgStars_acceptedBy = avg;
    }else if ([[arrayTerms objectAtIndex:0] isKindOfClass:[NSArray class]]){
        
        NSArray *arrRatings = [arrayTerms objectAtIndex:0];
        
        NSInteger noOfStars = 0;
        
        for (NSDictionary *dicts in arrRatings) {
            
            NSString *stars = [EncryptDecryptFile decrypt:[dicts valueForKey:@"ratingStar"]];
            noOfStars = noOfStars + [stars integerValue];
            
            obj.ratingObj = nil;
            obj.ratingObj = [[rating alloc]init];
            
            obj.ratingObj.ratingdescription = [EncryptDecryptFile decrypt:[dicts valueForKey:@"description"]];
            obj.ratingObj.employerId = [EncryptDecryptFile decrypt:[dicts valueForKey:@"employerId"]];
            obj.ratingObj.ratingId = [EncryptDecryptFile decrypt:[dicts valueForKey:@"ratingId"]];
            obj.ratingObj.ratingStar = [EncryptDecryptFile decrypt:[dicts valueForKey:@"ratingStar"]];
            obj.ratingObj.taskId = [EncryptDecryptFile decrypt:[dicts valueForKey:@"taskId"]];
            obj.ratingObj.workerId = [EncryptDecryptFile decrypt:[dicts valueForKey:@"workerId"]];
            obj.ratingObj.userName = [EncryptDecryptFile decrypt:[dicts valueForKey:@"userName"]];
            
            [obj.arrayRatings_acceptedBy addObject:obj.ratingObj];
        }
        
        CGFloat avg = noOfStars/arrayTerms.count;
        obj.avgStars_acceptedBy = avg;
        
    }
    
    //task created by rating detail
    obj.arrayRatings_createdBy = [[NSMutableArray alloc]init];
    arrayTerms= [dict valueForKey:@"Created By Rating Detail"];
    
    if ([[arrayTerms objectAtIndex:0] isKindOfClass:[NSString class]]) {
        
        obj.rating_createdBy = [EncryptDecryptFile decrypt:[arrayTerms objectAtIndex:0]];
        
    }else if ([[arrayTerms objectAtIndex:0] isKindOfClass:[NSDictionary class]]){
        
        NSInteger noOfStars = 0;
        for (NSDictionary *dicts in arrayTerms) {
            
            NSString *stars = [EncryptDecryptFile decrypt:[dicts valueForKey:@"ratingStar"]];
            noOfStars = noOfStars + [stars integerValue];
            
            obj.ratingObj = nil;
            obj.ratingObj = [[rating alloc]init];
            
            obj.ratingObj.ratingdescription = [EncryptDecryptFile decrypt:[dicts valueForKey:@"description"]];
            obj.ratingObj.employerId = [EncryptDecryptFile decrypt:[dicts valueForKey:@"employerId"]];
            obj.ratingObj.ratingId = [EncryptDecryptFile decrypt:[dicts valueForKey:@"ratingId"]];
            obj.ratingObj.ratingStar = [EncryptDecryptFile decrypt:[dicts valueForKey:@"ratingStar"]];
            obj.ratingObj.taskId = [EncryptDecryptFile decrypt:[dicts valueForKey:@"taskId"]];
            obj.ratingObj.workerId = [EncryptDecryptFile decrypt:[dicts valueForKey:@"workerId"]];
            obj.ratingObj.userName = [EncryptDecryptFile decrypt:[dicts valueForKey:@"userName"]];
            
            [obj.arrayRatings_createdBy addObject:obj.ratingObj];
        }
        
        CGFloat avg = noOfStars/arrayTerms.count;
        obj.avgStars_createdBy = avg;
        
    }else if ([[arrayTerms objectAtIndex:0] isKindOfClass:[NSArray class]]){
        
        NSArray *arrRatings = [arrayTerms objectAtIndex:0];
        
        NSInteger noOfStars = 0;
        
        for (NSDictionary *dicts in arrRatings) {
            
            NSString *stars = [EncryptDecryptFile decrypt:[dicts valueForKey:@"ratingStar"]];
            noOfStars = noOfStars + [stars integerValue];
            
            obj.ratingObj = nil;
            obj.ratingObj = [[rating alloc]init];
            
            obj.ratingObj.ratingdescription = [EncryptDecryptFile decrypt:[dicts valueForKey:@"description"]];
            obj.ratingObj.employerId = [EncryptDecryptFile decrypt:[dicts valueForKey:@"employerId"]];
            obj.ratingObj.ratingId = [EncryptDecryptFile decrypt:[dicts valueForKey:@"ratingId"]];
            obj.ratingObj.ratingStar = [EncryptDecryptFile decrypt:[dicts valueForKey:@"ratingStar"]];
            obj.ratingObj.taskId = [EncryptDecryptFile decrypt:[dicts valueForKey:@"taskId"]];
            obj.ratingObj.workerId = [EncryptDecryptFile decrypt:[dicts valueForKey:@"workerId"]];
            obj.ratingObj.userName = [EncryptDecryptFile decrypt:[dicts valueForKey:@"userName"]];
            
            [obj.arrayRatings_createdBy addObject:obj.ratingObj];
        }
        
        CGFloat avg = noOfStars/arrayTerms.count;
        obj.avgStars_createdBy = avg;
        
    }
    
    
    arrayTerms= [dict valueForKey:@"Card Detail"];
    if ([[arrayTerms objectAtIndex:0] isKindOfClass:[NSString class]]) {
        obj.cardDetail = [EncryptDecryptFile decrypt:[arrayTerms objectAtIndex:0]];
    }else if ([[arrayTerms objectAtIndex:0] isKindOfClass:[NSDictionary class]]){
        
    }else if ([[arrayTerms objectAtIndex:0] isKindOfClass:[NSArray class]]){
        obj.cardDetail = [EncryptDecryptFile decrypt:[[arrayTerms objectAtIndex:0] objectAtIndex:0]];
    }
    
    arrayTerms= [dict valueForKey:@"Created By"];
    NSDictionary *dictTemp2=[arrayTerms objectAtIndex:0];
    
    obj.userId_createdBy = [EncryptDecryptFile decrypt:[dictTemp2 valueForKey:@"userId"]];
    obj.userName_createdBy = [EncryptDecryptFile decrypt:[dictTemp2 valueForKey:@"userName"]];
    obj.description_createdBy = [EncryptDecryptFile decrypt:[dictTemp2 valueForKey:@"description"]];
    obj.emailId_createdBy = [EncryptDecryptFile decrypt:[dictTemp2 valueForKey:@"emailId"]];
    obj.latitude_createdBy = [[EncryptDecryptFile decrypt:[dictTemp2 valueForKey:@"latitude"]] floatValue];
    obj.longitude_createdBy = [[EncryptDecryptFile decrypt:[dictTemp2 valueForKey:@"longitude"]] floatValue];
    obj.locationName_createdBy = [EncryptDecryptFile decrypt:[dictTemp2 valueForKey:@"location"]];
    obj.mobile_createdBy = [EncryptDecryptFile decrypt:[dictTemp2 valueForKey:@"mobile"]];
    obj.picPath_createdBy = [EncryptDecryptFile decrypt:[dictTemp2 valueForKey:@"picPath"]];
    obj.avg_rating_createdBy = [EncryptDecryptFile decrypt:[dictTemp2 valueForKey:@"avg_rating"]];
    
    arrayTerms= [dict valueForKey:@"Task Detail"];
   NSDictionary *dictTemp3=[arrayTerms objectAtIndex:0];
    
    obj.taskDetailObj = [[task alloc]init];
    
    obj.taskDetailObj.userid = [EncryptDecryptFile decrypt:[dictTemp3 valueForKey:@"userId"]];
    obj.taskDetailObj.title = [EncryptDecryptFile decrypt:[dictTemp3 valueForKey:@"title"]];
    obj.taskDetailObj.taskid = [EncryptDecryptFile decrypt:[dictTemp3 valueForKey:@"taskId"]];
    obj.taskDetailObj.subcategoryId = [EncryptDecryptFile decrypt:[dictTemp3 valueForKey:@"subcategoryId"]];
    obj.taskDetailObj.status = [EncryptDecryptFile decrypt:[dictTemp3 valueForKey:@"status"]];
    obj.taskDetailObj.startDate = [EncryptDecryptFile decrypt:[dictTemp3 valueForKey:@"startDate"]];
    obj.taskDetailObj.price = [[EncryptDecryptFile decrypt:[dictTemp3 valueForKey:@"price"]] floatValue];
    obj.taskDetailObj.longitude = [[EncryptDecryptFile decrypt:[dictTemp3 valueForKey:@"longitude"]] floatValue];
    obj.taskDetailObj.locationName = [EncryptDecryptFile decrypt:[dictTemp3 valueForKey:@"locationName"]];
    obj.taskDetailObj.latitude = [[EncryptDecryptFile decrypt:[dictTemp3 valueForKey:@"latitude"]] floatValue];
    obj.taskDetailObj.endDate = [EncryptDecryptFile decrypt:[dictTemp3 valueForKey:@"endDate"]];
    obj.taskDetailObj.taskDescription = [EncryptDecryptFile decrypt:[dictTemp3 valueForKey:@"description"]];
    obj.taskDetailObj.lastModifyTime = [EncryptDecryptFile decrypt:[dictTemp3 valueForKey:@"lastModifyTime"]];
    obj.taskDetailObj.taskImagesArray = [[NSMutableArray alloc] init];
    obj.taskDetailObj.imagesArray = [[NSMutableArray alloc]init];
    
    for (rating *ratingObjs in obj.arrayRatings_acceptedBy) {
        
        if ([ratingObjs.taskId isEqualToString:[NSString stringWithFormat:@"%@",obj.taskDetailObj.taskid]]) {
            
            obj.isAlreadyRated = true;
            
            break;
            
        }
        
    }
    
    arrayTerms= [dict valueForKey:@"taskImage"];
    NSArray *newArray = [arrayTerms objectAtIndex:0];
    
    int i = 0;
    
    for (NSDictionary *dicts in newArray) {
        
        NSArray *componentsArray = [[EncryptDecryptFile decrypt:[dicts valueForKey:@"taskImg"]] componentsSeparatedByString:@"http://192.168.5.134/handapp/"];
        
        if (componentsArray.count>1) {
            [obj.taskDetailObj.taskImagesArray addObject:[NSString stringWithFormat:urlServer,[componentsArray objectAtIndex:1]]];
        }else{
            [obj.taskDetailObj.taskImagesArray addObject:[NSString stringWithFormat:urlServer,[componentsArray objectAtIndex:0]]];
        }
        
        NSString *fullPath=[obj.taskDetailObj.taskImagesArray objectAtIndex:i];
        NSArray *parts = [fullPath componentsSeparatedByString:@"/"];
        NSString *imageName=[parts lastObject];
        NSArray *imageNameParts = [imageName componentsSeparatedByString:@"."];
        
        NSString *userImageNameWithJPEG = [NSString stringWithFormat:@"%@_%@_%d.jpeg",[imageNameParts firstObject],obj.taskDetailObj.taskid,i];
        
        UIImage *img = [GlobalFunction loadImageOfLogoFromCacheFolder:userImageNameWithJPEG dirname:@"taskImages"];
        
        if (img) {
            
            [obj.taskDetailObj.imagesArray addObject:img];
            
        }
        
        i = i+1;
    }
    
    if (obj.taskDetailObj.taskImagesArray.count != obj.taskDetailObj.imagesArray.count) {
        [obj.taskDetailObj.imagesArray removeAllObjects];
    }
    
    
    if ([userObj.userid isEqualToString:obj.userId_acceptedBy]) {
        
        if ([obj.picPath_createdBy isEqualToString:@""]) {
            obj.userImage_createdBy = imageUserDefault;
        }else{
            NSArray *components = [obj.picPath_createdBy componentsSeparatedByString:@"/"];
            
            NSString *imgNameWithJPEG = [components lastObject];
            
            
            NSArray *componentsWithDots = [imgNameWithJPEG componentsSeparatedByString:@"."];
            
            if (componentsWithDots.count>2) {
                
                imgNameWithJPEG = [NSString stringWithFormat:@"%@.%@",[componentsWithDots firstObject],[componentsWithDots lastObject]];
                
            }
            
            UIImage *img = [GlobalFunction loadImageOfLogoFromCacheFolder:imgNameWithJPEG dirname:@"user"];
            
            if (img) {
                obj.userImage_createdBy = img;
            }else{
                UIImage *img2 = [GlobalFunction getImageFromURL:[NSString stringWithFormat:urlServer,obj.picPath_createdBy]];
                
                if (img2) {
                    obj.userImage_createdBy = img2;
                    
                    NSArray *components2 = [imgNameWithJPEG componentsSeparatedByString:@"."];
                    
                    NSString *imgNameWithoutJPEG = [components2 firstObject];
                    
                    [GlobalFunction saveimage:img2 imageName:imgNameWithoutJPEG dirname:@"user"];
                    
                }else{
                    obj.userImage_createdBy = imageUserDefault;
                }
            }
        }
    }else if ([userObj.userid isEqualToString:obj.userId_createdBy]){
        
        if ([obj.picPath_acceptedBy isEqualToString:@""]) {
            obj.userImage_acceptedBy = imageUserDefault;
        }else{
            NSArray *components = [obj.picPath_acceptedBy componentsSeparatedByString:@"/"];
            
            NSString *imgNameWithJPEG = [components lastObject];
            
            
            NSArray *componentsWithDots = [imgNameWithJPEG componentsSeparatedByString:@"."];
            
            if (componentsWithDots.count>2) {
                
                imgNameWithJPEG = [NSString stringWithFormat:@"%@.%@",[componentsWithDots firstObject],[componentsWithDots lastObject]];
                
            }
            
            UIImage *img = [GlobalFunction loadImageOfLogoFromCacheFolder:imgNameWithJPEG dirname:@"user"];
            
            if (img) {
                obj.userImage_acceptedBy = img;
            }else{
                UIImage *img2 = [GlobalFunction getImageFromURL:[NSString stringWithFormat:urlServer,obj.picPath_acceptedBy]];
                
                if (img2) {
                    obj.userImage_acceptedBy = img2;
                    
                    NSArray *components2 = [imgNameWithJPEG componentsSeparatedByString:@"."];
                    
                    NSString *imgNameWithoutJPEG = [components2 firstObject];
                    
                    [GlobalFunction saveimage:img2 imageName:imgNameWithoutJPEG dirname:@"user"];
                    
                }else{
                    obj.userImage_acceptedBy = imageUserDefault;
                }
            }
        }
    }    
}


-(void)performSegueForTaskDetail{
    
    myAppDelegate.isSideBarAccesible = false;
    
    TaskDetailTableViewCell *cell = [self.tblActiveTask cellForRowAtIndexPath:[NSIndexPath indexPathForRow:[arrayOfTaskWithStatus1Or2 indexOfObject:myAppDelegate.taskData] inSection:0]];
    [self performSegueWithIdentifier:@"taskActiveDetail" sender:cell];

}

-(BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender{
    
    if ([identifier isEqualToString:@"taskActiveDetail"]) {
        return NO;
    }
    return YES;
}


-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{

    [GlobalFunction addIndicatorView];
    
    if ([segue.identifier isEqualToString:@"taskActiveDetail"]) {
                
        myAppDelegate.vcObj.btnSideBarOpen.hidden = YES;
        myAppDelegate.vcObj.btnBack.hidden = NO;
        
        myAppDelegate.stringNameOfChildVC = [NSString stringWithFormat:@"%@",[self class]];

        myAppDelegate.superViewClassName = @"";

        myAppDelegate.taskDetailVCObj = nil;
        
        myAppDelegate.taskDetailVCObj = (TaskDetailViewController *)segue.destinationViewController;
        
    }
}


- (IBAction)animateImageViewToFullScreen:(id)sender {
    
    TaskDetailTableViewCell *tCell = [self.tblActiveTask cellForRowAtIndexPath:[NSIndexPath indexPathForRow:[[sender view] tag] inSection:0]];
    
    [GlobalFunction animateImageview:tCell.imgViewUserProfile];

}

@end
