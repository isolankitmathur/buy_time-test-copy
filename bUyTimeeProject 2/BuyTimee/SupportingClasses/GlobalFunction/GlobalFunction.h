//
//  GlobalFunction.h
//  BuyTimee
//
//  Created by User14 on 26/02/16.

//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import <CoreLocation/CoreLocation.h>

@interface GlobalFunction : NSObject <UIAlertViewDelegate>

typedef NS_ENUM(NSInteger, TMStatus) {
    TMViewAllTask = 0,
    TMActiveTask_Accepted = 1,
    TMActiveTask_Complete  = 2,
    TMTaskHistory = 3,
};


+ (int)lineCountForLabel:(UILabel *)label;
+(CGRect)rectForLabel:(UILabel *)label;
+(CGFloat)heightForWidth:(CGFloat)width usingFont:(UIFont *)font forLabel:(UILabel *)lbl;

+(void)rotateView:(UIView *)view atAngle:(NSInteger)angle;
+(void)createBorderforView:(UIView *)view withWidth:(CGFloat)width cornerRadius:(CGFloat)cornerRadius colorForBorder:(UIColor *)color;

+(BOOL)validateEmail:(NSString *)candidate;
+(BOOL)validateEmailWithString:(NSString*)checkString;
+(BOOL)validatePassword:(NSString *)password;

+ (instancetype) shared;
+(NSString *)getTimeZone;
-(CLLocationCoordinate2D) getLocationFromAddressString: (NSString*) addressStr;
-(void)showAlertForMessage:(NSString *)message Title:(NSString *)Title CancelButtonTitle:(NSString *)CancelButtonTitle;
-(void)showAlertForMessage:(NSString *)message;

-(BOOL)callUpdateWebservice;
//-(BOOL)callUpdateWebservice :(UIViewController *)obj;

//+(BOOL)hasConnectivity;
//+(NSString *)randomString;
//+ (NSString *)GetUUID;
//+(NSString *)GetCipher:(NSString *)signatureString;

+(void)SwipeLeft:(UIView *)view;
+(void)SwipeRight:(UIView *)view;
+(void)addIndicatorView;
+(void)removeIndicatorView;
+(UIImage*) scaleImage:(UIImage*)image toSize:(CGSize)newSize;
+(float)getCurrentLatitude;
+(float)getCurrentLongitude;

+(BOOL)checkIfContainFloat:(float)num;

+(void)animateImageview:(UIView *)imageView;

+(NSArray *)filterTaskListOnBasisiOfCategoryId:(NSString *)categoryId SubCategoryId:(NSString *)subCategoryId range:(NSString *)range location:(NSString *)location priceMin:(NSString *)priceMin priceMax:(NSString *)priceMax startDate:(NSString *)startDate endDate:(NSString *)endDate;

+(NSArray *)filterTaskForUserID:(NSString *)userID taskStatus:(TMStatus)taskStatus;


//save image
+(UIImage *) getImageFromURL:(NSString *)fileURL;
+(void)saveimage:(UIImage *)image imageName:(NSString *)imageName dirname:(NSString *)dirname;
+ (UIImage*)loadImageOfLogoFromCacheFolder:(NSString*)imageName dirname:(NSString *)dirname;
+ (void)removeImage:(NSString*)imageName dirname:(NSString *)dirname;
+(void)createDirForImageInCache :(NSString *)dirName;
+(void)delDirForImage :(NSString *)dirName;
+ (NSString*)convertDateFormat:(NSString *)date;
+ (NSString*)convertTimeFormat:(NSString *)time;

//+(CLLocationCoordinate2D) getLocationFromAddressString: (NSString*) addressStr;
@end
