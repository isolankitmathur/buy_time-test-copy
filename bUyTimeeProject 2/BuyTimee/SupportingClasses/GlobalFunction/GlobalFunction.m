//
//  GlobalFunction.m
//  BuyTimee
//
//  Created by User14 on 26/02/16.

//

#import "GlobalFunction.h"
#import "Constants.h"
#import "LoadingView.h"
#import "category.h"
#import "subCategory.h"
#import "accept_task_info.h"
#import "task.h"
#import "LibAnimation.h"
#import <SVProgressHUD.h>


#define CGFLOAT_MAX_WIDTH 2000.0
#define CGFLOAT_MAX_HEIGHT 2000.0
#define MileConversionConstant 0.621371192

@implementation GlobalFunction 

bool isAlertShown;
LoadingView *loadingView;
LibAnimation *globalObj;
CLLocationManager *locationManager111;


+ (BOOL) validateEmail: (NSString *) candidate {
    NSString *emailRegex =
    @"(?:[a-z0-9!#$%\\&'*+/=?\\^_`{|}~-]+(?:\\.[a-z0-9!#$%\\&'*+/=?\\^_`{|}"
    @"~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\"
    @"x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-"
    @"z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5"
    @"]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-"
    @"9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21"
    @"-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES[c] %@", emailRegex];
    
    return [emailTest evaluateWithObject:candidate];
}

+ (BOOL)validateEmailWithString:(NSString*)checkString{
    BOOL stricterFilter = NO; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSString *laxString = @".+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

+(BOOL)validatePassword:(NSString *)password{
    
    BOOL isValid = true;
    BOOL lowerCaseLetter = false,upperCaseLetter = false,digit = false,specialCharacter = 0;
    if([password length] >= 10)
    {
        for (int i = 0; i < [password length]; i++)
        {
            unichar c = [password characterAtIndex:i];
            if(!lowerCaseLetter)
            {
                lowerCaseLetter = [[NSCharacterSet lowercaseLetterCharacterSet] characterIsMember:c];
            }
            if(!upperCaseLetter)
            {
                upperCaseLetter = [[NSCharacterSet uppercaseLetterCharacterSet] characterIsMember:c];
            }
            if(!digit)
            {
                digit = [[NSCharacterSet decimalDigitCharacterSet] characterIsMember:c];
            }
            if(!specialCharacter)
            {
                specialCharacter = [[NSCharacterSet symbolCharacterSet] characterIsMember:c];
            }
        }
        
        if(specialCharacter && digit && lowerCaseLetter && upperCaseLetter){
            //do what u want
            //valid
        }
        else{
            //not valid
        }
    }
    else{
       //not valid
        if ([password length] == 0) {
            isValid = false;
        }
    }
    return isValid;
}

+(UIImage*) scaleImage:(UIImage*)image toSize:(CGSize)newSize{
    
    //new logic
    UIGraphicsBeginImageContext( newSize );
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
    
}

-(void)showAlertForMessage:(NSString *)message Title:(NSString *)Title CancelButtonTitle:(NSString *)CancelButtonTitle{
    
    UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:Title message:message delegate:self cancelButtonTitle:CancelButtonTitle otherButtonTitles:nil];
    
    if(isAlertShown){
        
    }else{
        
        [alertView show];
        isAlertShown = YES;
    }
    
}

-(void)showAlertForMessage:(NSString *)message{
    
    
    UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"BUY TIMEE" message:message delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    
    if(isAlertShown){
        
    }else{
        
        [alertView show];
        isAlertShown = YES;
    }
    
}


-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex{
    
    
    isAlertShown = NO;
    
}

-(BOOL)callUpdateWebservice{
    
    BOOL isUpdateSuccessfull = false;
    
    NSDictionary *dictUser = [UserDefaults objectForKey:keyUserloginDetailDictionary];
    
   // NSDictionary *dictUpdate = [[WebService shared] getUpdateuserid:[dictUser valueForKey:keyUserId] lastModifyTime:[UserDefaults valueForKey:keyLastModifyTime] andToken:myAppDelegate.deviceTokenString];

    
//    NSDictionary *dictUpdate = [[WebService shared] getUpdatelastModifyTime:[UserDefaults valueForKey:keyLastModifyTime]];
    
    NSString *timeZoneSeconds = [GlobalFunction getTimeZone];
    
    NSDictionary *dictUpdate = [[WebService shared] getUpdatelastModifyTime:[UserDefaults valueForKey:keyLastModifyTime] userid:myAppDelegate.userData.userid timeZone:timeZoneSeconds];
    
    
    if (dictUpdate != nil) {
        
        if ([[EncryptDecryptFile decrypt:[dictUpdate valueForKey:@"responseCode"]] isEqualToString:@"200"]) {
            
            [UserDefaults setValue:[EncryptDecryptFile decrypt:[dictUpdate valueForKey:@"lastModifyTime"]] forKey:keyLastModifyTime];
            
            NSDictionary *dictDataReg = [dictUpdate objectForKey:@"data"];
            
            
            if ([DatabaseClass updateDataBaseFromUpdatedDictionary:dictDataReg]) {//lastupdatedTime
                
                isUpdateSuccessfull = true;
                
            }
            
        }else{
            
        }
        
    }else{
        
    }
    
    return isUpdateSuccessfull;
    
}



+ (instancetype) shared{
    static dispatch_once_t once;
    static id sharedInstance;
    dispatch_once(&once, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

+(void)rotateView:(UIView *)view atAngle:(NSInteger)angle{
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3]; // Set how long your animation goes for
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    
    view.transform = CGAffineTransformMakeRotation(degreesToRadians(angle)); // if angle is in radians
    
    [UIView commitAnimations];
    
}


+(void)createBorderforView:(UIView *)view withWidth:(CGFloat)width cornerRadius:(CGFloat)cornerRadius colorForBorder:(UIColor *)color{
    
    view.layer.borderColor = color.CGColor;
    view.layer.borderWidth = width;
    view.layer.cornerRadius = cornerRadius;
    view.layer.masksToBounds = YES;
    
}


+(void)SwipeRight:(UIView *)view{
    CATransition* transition = [CATransition animation];
    [transition setDuration:0.3];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [transition setFillMode:kCAFillModeBoth];
    [transition setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    [view.layer addAnimation:transition forKey:kCATransition];
}


+(void)SwipeLeft:(UIView *)view{
    CATransition* transition = [CATransition animation];
    [transition setDuration:0.3];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    [transition setFillMode:kCAFillModeBoth];
    [transition setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    [view.layer addAnimation:transition forKey:kCATransition];
}


+ (int)lineCountForLabel:(UILabel *)label {
    
    return ceil([self heightForWidth:label.frame.size.width usingFont:label.font forLabel:label] / label.font.lineHeight);
    
}

+(BOOL)checkIfContainFloat:(float)num{
    
    return (num-(int)num != 0);
    
}


+(CGRect)rectForLabel:(UILabel *)label{
    
    CGSize maximumLabelSize2;
    
    maximumLabelSize2 = CGSizeMake(CGFLOAT_MAX_WIDTH,CGFLOAT_MAX_HEIGHT);
    
    CGRect r = [label.text boundingRectWithSize:maximumLabelSize2
                                        options:NSStringDrawingUsesFontLeading
                                     attributes:@{NSFontAttributeName:label.font}
                                        context:nil];
    return r;
}


+(CGFloat)heightForWidth:(CGFloat)width usingFont:(UIFont *)font forLabel:(UILabel *)lbl{
    NSStringDrawingContext *context = [[NSStringDrawingContext alloc] init];
    CGSize labelSize = (CGSize){width, CGFLOAT_MAX_HEIGHT};
    CGRect r = [lbl.text boundingRectWithSize:labelSize options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: font} context:context];
    return r.size.height;
}

#pragma mark - indicator view

+(void)addIndicatorView{
    
    if (!myAppDelegate.isActivityIndicatorShowing) {
        
        [SVProgressHUD setDefaultStyle:SVProgressHUDStyleCustom];
        [SVProgressHUD setBackgroundColor:colorGreenDark];
        [SVProgressHUD setForegroundColor:[UIColor whiteColor]];
        [SVProgressHUD setRingThickness:4.0];
        [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];

        [SVProgressHUD showWithStatus:@"Please wait..."];

        myAppDelegate.isActivityIndicatorShowing = true;
        
    }
    
}

+(void)removeIndicatorView{
    
    [SVProgressHUD dismiss];

    myAppDelegate.isActivityIndicatorShowing = false;

}


+(void)animateImageview:(UIView *)imageView{
    
    globalObj = [[LibAnimation alloc]initWithView:imageView colorForCenterViewWithWhite:0.0 alpha:0.8];
    
    CGPoint center = [imageView.superview convertPoint:imageView.center toView:nil];
    
    [globalObj setFromCenter:center];
    [globalObj setToCenter:myAppDelegate.window.center];
    [globalObj setFromScale:CGPointMake(1.0, 1.0)];
    [globalObj setDelayDuration:0.0];
    
    if (imageView.frame.size.width == 39) {
        [globalObj setToScale:CGPointMake(3.5*1.84, 3.5*1.84)];
    }else if (imageView.frame.size.width == 60){
        [globalObj setToScale:CGPointMake(3.5*1.2, 3.5*1.2)];
    }else if (imageView.frame.size.width == 72){
        [globalObj setToScale:CGPointMake(3.5, 3.5)];
    }else{
        [globalObj setToScale:CGPointMake(3.5, 3.5)];
    }
    
    [globalObj setAnimationDuration:0.3];
    [globalObj animateViewFromCircleToSquare];
    
}


+ (NSString*) convertToString:(TMStatus) whichStatus {
    NSString *result = nil;
    
    switch(whichStatus) {
        case TMViewAllTask:
            result = @"0";
            break;
        case TMActiveTask_Accepted:
            result = @"1";
            break;
        case TMActiveTask_Complete:
            result = @"2";
            break;
        case TMTaskHistory:
            result = @"3";
            break;
        default:
            result = @"unknown";
    }
    
    return result;
}

#pragma mark - lat long
+(float)getCurrentLatitude{
    
    float latitude = myAppDelegate.locationManager.location.coordinate.latitude;
    
    NSLog(@"latitude = %f",latitude);
    return latitude;
    
}


+(float)getCurrentLongitude{
    
    float longitude = myAppDelegate.locationManager.location.coordinate.longitude;
    NSLog(@"longitude = %f",longitude);
    return longitude;
    
}



+(float)getMinLatitudeForCurrentLocationLatitude:(float)currentLat andRadiusInKM:(float)radius{

    float minLat;
    
    minLat = currentLat - (radius/111.12);
    
    return minLat;
    
}

+(float)getMaxLatitudeForCurrentLocationLatitude:(float)currentLat andRadiusInKM:(float)radius{
    
    float minLat;
    
    minLat = currentLat + (radius/111.12);
    
    return minLat;
    
}


+(float)getMinLongitudeForCurrentLocationLongitude:(float)currentLongitude andRadiusInKM:(float)radius{
    
    float minLat;
    
    minLat = currentLongitude - (radius) / fabs(cos(degreesToRadians(currentLongitude))*111.12);
    
    return minLat;
    
}



+(float)getMaxLongitudeForCurrentLocationLongitude:(float)currentLongitude andRadiusInKM:(float)radius{
    
    float minLat;
    
    minLat = currentLongitude + (radius) / fabs(cos(degreesToRadians(currentLongitude))*111.12);
    
    return minLat;
    
}



+(NSArray *)filterTaskListOnBasisiOfCategoryId:(NSString *)categoryId SubCategoryId:(NSString *)subCategoryId range:(NSString *)range location:(NSString *)location priceMin:(NSString *)priceMin priceMax:(NSString *)priceMax  startDate:(NSString *)startDate endDate:(NSString *)endDate{
    
    
    //range=@"";
    
    NSLog(@"filter on basis of category id: %@ subcategoryid: %@ range: %@ location: %@ priceMin: %@ priceMax: %@ and total count of array before filtering ---- %lu startDate..%@ endDate..%@",categoryId,subCategoryId,range,location,priceMin,priceMax,(unsigned long)myAppDelegate.arrayTaskDetail.count,startDate,endDate);
    
    myAppDelegate.isLocationApiFail = false;
    
    if (startDate.length<=0) {
        startDate = @"";
    }
    else if (endDate.length<=0)
    {
        endDate = @"";
    }
    NSArray *arrayFiltered;
    
    NSMutableArray *predicateArray = [[NSMutableArray alloc] init];
    
    if (![[categoryId stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""]) {
        
        NSPredicate *predicateCategoryId = [NSPredicate predicateWithFormat:@"SELF.categoryId MATCHES[cd] %@", categoryId];
        
//         NSMutableArray *subCatArr = [DatabaseClass getDataFromCategoryData:[[NSString stringWithFormat:@"Select * from %@",tableCategory] UTF8String]]; // here change tableSubCategory to tableCategory because of solve crash bcz tableSubCategory fetch empty vs
//        
//        NSMutableArray *subCatArr = [DatabaseClass getDataFromSubCategoryData:[[NSString stringWithFormat:@"Select * from %@",tableCategory] UTF8String]]; // here change tableSubCategory to tableCategory because of solve crash bcz tableSubCategory fetch empty vs
        //NSArray *arrayCatFiltered = [subCatArr filteredArrayUsingPredicate:predicateCategoryId];
        
//        NSString *predicateString = @"";
//        for (subCategory *subObj in arrayCatFiltered) {
//            if ([arrayCatFiltered lastObject] == subObj) {
//                predicateString = [predicateString stringByAppendingString:[NSString stringWithFormat:@"SELF.subcategoryId MATCHES[cd] '%@'",subObj.categoryId]];
//            }else{
//                predicateString = [predicateString stringByAppendingString:[NSString stringWithFormat:@"SELF.subcategoryId MATCHES[cd] '%@' OR ",subObj.categoryId]];
//            }
//        }
    
       // predicateCategoryId = [NSPredicate predicateWithFormat:predicateString];

        [predicateArray addObject:predicateCategoryId];
    }
    else if (![[subCategoryId stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""]) {
        NSPredicate *predicateSubCategoryId = [NSPredicate predicateWithFormat:@"SELF.subcategoryId MATCHES[cd] %@", subCategoryId];
        [predicateArray addObject:predicateSubCategoryId];
    }
    
    else if (startDate.length > 0 && endDate.length<=0)   {
        NSPredicate *predicateStartDate = [NSPredicate predicateWithFormat:@"SELF.startDate>= %@", startDate];
        
        [predicateArray addObject:predicateStartDate];
    }
    
    else if (startDate.length > 0 && endDate.length>0)   {
        NSPredicate *predicateStartDate = [NSPredicate predicateWithFormat:@"SELF.startDate>= %@ AND SELF.startDate<= %@", startDate,endDate];

        [predicateArray addObject:predicateStartDate];
    }

    
else if (![[startDate stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""] && ![[endDate stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""])
    {
    NSPredicate *predicateStartDate = [NSPredicate predicateWithFormat:@"SELF.startDate>= %@ AND SELF.startDate<= %@", startDate,endDate];
        [predicateArray addObject:predicateStartDate];
    }
    
    CLLocationCoordinate2D locationFromAddress;
    
    ///////////////////
    if (![[location stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""]) {
        
        location = [location stringByAppendingString:@"*"];
        
        if ([[range stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""]) {
        NSPredicate *predicateLocation = [NSPredicate predicateWithFormat:@"SELF.locationName LIKE[c] %@", location];//contains[cd]
            [predicateArray addObject:predicateLocation];//LIKE[c]
        }
        if (![[range stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""]) {
           locationFromAddress= [self getLocationFromAddressString:location];
            
            //myAppDelegate.locationManager.location.coordinate=;
            
           // myAppDelegate.locationManager.location.coordinate.latitude=locationFromAddress.latitude;
           // myAppDelegate.locationManager.location.coordinate.latitude=locationFromAddress.longitude;
            
            if (locationFromAddress.latitude == -1 && locationFromAddress.longitude == -1)
            {
                locationFromAddress= [self getLocationFromAddressString:location];
                
                if (locationFromAddress.latitude == -1 && locationFromAddress.longitude == -1)
                {
                    
                    locationFromAddress= [self getLocationFromAddressString:location];
                    
                    if (locationFromAddress.latitude == -1 && locationFromAddress.longitude == -1)
                    {
                        
                    myAppDelegate.isLocationApiFail = true;
                    return nil;
                        
                    }
               
                    
                }
               
            }
            
            
        }
    }
    
    if (![[range stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""]) {
        
        CGFloat searchDistance = [range floatValue];
//        searchDistance = searchDistance / MileConversionConstant;
        
//        CGFloat minLat = myAppDelegate.locationManager.location.coordinate.latitude - (searchDistance / 69);
//        CGFloat maxLat = myAppDelegate.locationManager.location.coordinate.latitude + (searchDistance / 69);
//        CGFloat minLon = myAppDelegate.locationManager.location.coordinate.longitude - searchDistance / fabs(cos(degreesToRadians(myAppDelegate.locationManager.location.coordinate.latitude))*69);
//        CGFloat maxLon = myAppDelegate.locationManager.location.coordinate.longitude + searchDistance / fabs(cos(degreesToRadians(myAppDelegate.locationManager.location.coordinate.latitude))*69);
//
        CGFloat minLat =0; //myAppDelegate.locationManager.location.coordinate.latitude - (searchDistance / 69);
        CGFloat maxLat =0;// myAppDelegate.locationManager.location.coordinate.latitude + (searchDistance / 69);
        CGFloat minLon =0;// myAppDelegate.locationManager.location.coordinate.longitude - searchDistance / fabs(cos(degreesToRadians(myAppDelegate.locationManager.location.coordinate.latitude))*69);
        CGFloat maxLon =0;// myAppDelegate.locationManager.location.coordinate.longitude + searchDistance / fabs(cos(degreesToRadians(myAppDelegate.locationManager.location.coordinate.latitude))*69);
        if (![[location stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""])
        {
            minLat = locationFromAddress.latitude - (searchDistance / 69);
            maxLat = locationFromAddress.latitude + (searchDistance / 69);
            minLon = locationFromAddress.longitude - searchDistance / fabs(cos(degreesToRadians(locationFromAddress.latitude))*69);
            maxLon = locationFromAddress.longitude + searchDistance / fabs(cos(degreesToRadians(locationFromAddress.latitude))*69);
        
        NSLog(@"min lat == %f max lat == %f min long == %f max long == %f if location string exist",minLat,maxLat,minLon,maxLon);
        }else{
            minLat = myAppDelegate.locationManager.location.coordinate.latitude - (searchDistance / 69);
            maxLat = myAppDelegate.locationManager.location.coordinate.latitude + (searchDistance / 69);
            minLon = myAppDelegate.locationManager.location.coordinate.longitude - searchDistance / fabs(cos(degreesToRadians(myAppDelegate.locationManager.location.coordinate.latitude))*69);
            maxLon = myAppDelegate.locationManager.location.coordinate.longitude + searchDistance / fabs(cos(degreesToRadians(myAppDelegate.locationManager.location.coordinate.latitude))*69);
            NSLog(@"min lat == %f max lat == %f min long == %f max long == %f if location field empty",minLat,maxLat,minLon,maxLon);
            
        }
        
        
        NSLog(@"lat == %f long == %f",myAppDelegate.locationManager.location.coordinate.latitude,myAppDelegate.locationManager.location.coordinate.longitude);
        
//        NSPredicate *predicateDistanceRange = [NSPredicate predicateWithFormat:@"SELF.latitude <= %f AND SELF.latitude >= %f AND SELF.longitude <= %f AND SELF.longitude >= %f", maxLat, minLat, maxLon, minLon];

        
        NSPredicate *predicateDistanceRange = [NSPredicate predicateWithFormat:@"SELF.latitude >= %f AND SELF.latitude <= %f AND SELF.longitude >= %f AND SELF.longitude <= %f", minLat, maxLat, minLon, maxLon ];
        
        [predicateArray addObject:predicateDistanceRange];
        
 ////////
    }
       if (![[priceMin stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""] && ![[priceMax stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""]) {
        NSPredicate *predicatePriceMin = [NSPredicate predicateWithFormat:@"SELF.price >= %f AND SELF.price <= %f", [priceMin floatValue],[priceMax floatValue]];
        [predicateArray addObject:predicatePriceMin];
    }else{
        
        if (![[priceMin stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""]) {
            NSPredicate *predicatePriceMin = [NSPredicate predicateWithFormat:@"SELF.price >= %f", [priceMin floatValue]];
            [predicateArray addObject:predicatePriceMin];
        }
        else if (![[priceMax stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""]) {
            NSPredicate *predicatePriceMax = [NSPredicate predicateWithFormat:@"SELF.price <= %f",[priceMax floatValue]];
            [predicateArray addObject:predicatePriceMax];
        }

    }

    NSPredicate *predicateStatus = [NSPredicate predicateWithFormat:@"SELF.status MATCHES[cd] %@", [self convertToString:TMViewAllTask]];
    [predicateArray addObject:predicateStatus];
    
    
   // NSLog(@"Total Records Count:%lu ** %@", (unsigned long)[myAppDelegate.arrayTaskDetail count], myAppDelegate.arrayTaskDetail);
    
    NSArray *subPredicates = [NSArray arrayWithArray:predicateArray];
    NSPredicate *orPredicate = [NSCompoundPredicate andPredicateWithSubpredicates:subPredicates];
    arrayFiltered = [myAppDelegate.arrayTaskDetail filteredArrayUsingPredicate:orPredicate];
    
//    NSPredicate *userIdPredicate = [NSPredicate predicateWithFormat:@"SELF.userid MATCHES[cd] %@",myAppDelegate.userData.userid];
//    NSArray *userIDFilteredArray = [arrayFiltered filteredArrayUsingPredicate:userIdPredicate];
//    
//    NSMutableArray *copyOfArrayFiltered = [arrayFiltered mutableCopy];
//    
//    [copyOfArrayFiltered removeObjectsInArray:userIDFilteredArray];
//    
//    arrayFiltered = nil;
//    
//    arrayFiltered = copyOfArrayFiltered;
    
    NSMutableArray *array = [arrayFiltered mutableCopy];
    
    [array sortUsingDescriptors:
     @[
       [NSSortDescriptor sortDescriptorWithKey:@"taskid_Integer" ascending:NO],
       ]];

    arrayFiltered = array;

    NSLog(@"final count after filter is....%lu",(unsigned long)arrayFiltered.count);
    
    return arrayFiltered;

}
//#pragma mark location manager delegate
//-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations{
//
//   // self.latitude = [NSString stringWithFormat:@"%f", manager.location.coordinate.latitude];
//  //  self.longitude = [NSString stringWithFormat:@"%f", manager.location.coordinate.longitude];
//
//
// //   [self.locationManager stopUpdatingLocation];
//
//    CLGeocoder * geoCoder = [[CLGeocoder alloc] init];
//    [geoCoder reverseGeocodeLocation:locationManager111.location
//                   completionHandler:^(NSArray *placemarks, NSError *error) {
//
//                       NSString *cityName = @"";
//                       //                       CLPlacemark *myPlacemark = [placemarks objectAtIndex:0];
//                       //
//                       //                       NSString *cityName =  myPlacemark.subAdministrativeArea;
//
//                       for (CLPlacemark *placemark in placemarks) {
//                           NSString *currentCityName = [placemark locality];
//
//
//
//                           //self.viewAllTaskVCObject.enteredLocation = self.currentCityName;
//
//
//                       }
//                   }];
//
//
//}
+(CLLocationCoordinate2D) getLocationFromAddressString: (NSString*) addressStr {
    
    
//   locationManager111 = [[CLLocationManager alloc] init];
//
//    locationManager111.delegate = self;
//
//    Float32 latitude11 = -1;
//    Float32 latitude22 = -1;
//    CLLocationCoordinate2D center;
//    CLGeocoder * geoCoder = [[CLGeocoder alloc] init];
//
//    [geoCoder geocodeAddressString:addressStr completionHandler:^(NSArray *placemarks, NSError *error)
//     {
//
//         if(!error)
//         {
//             CLPlacemark *placemark = [placemarks objectAtIndex:0];
//             NSLog(@"%f",placemark.location.coordinate.latitude);
//             NSLog(@"%f",placemark.location.coordinate.longitude);
//             NSLog(@"%@",[NSString stringWithFormat:@"%@",[placemark description]]);
//
//             latitude11 = placemark.location.coordinate.latitude;
//             latitude22 = placemark.location.coordinate.longitude;
//         }
//         else
//         {
//             NSLog(@"There was a forward geocoding error\n%@",[error localizedDescription]);
//         }
//
//
//     }];
//
//   // CLLocationCoordinate2D center;
//    center.latitude=latitude11;
//    center.longitude = latitude22;
//    NSLog(@"View Controller get Location Logitute : %f",center.latitude);
//    NSLog(@"View Controller get Location Latitute : %f",center.longitude);
//    return center;
    
    double latitude = 0, longitude = 0;
    Float32 latitude1 = -1;
    Float32 latitude2 = -1;
    NSString *esc_addr =  [addressStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
//  NSString *req = [NSString stringWithFormat:@"https://maps.google.com/maps/api/geocode/json?sensor=false&address=%@", esc_addr];
   // https://maps.googleapis.com/maps/api/geocode/json?key=AbCdEfGhIjKlMnOpQrStUvWxYz&address=Dallas&sensor=true
     NSString *req = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyCAp9xXjxpQNnpBUT9znFsB6eoagYhKeVQ&address=%@&sensor=true", esc_addr];
    
    //AIzaSyCAp9xXjxpQNnpBUT9znFsB6eoagYhKeVQ ...// Key by Neeraj Sir 14 JAn 19
    // New billing key with luke la account  AIzaSyAMAlYUxS3GeG2yzjZaEs5ae9PO5eUpU7k
    // NEW....AIzaSyCXCXTyEwyOr9g-EMsu-wdsTesytP6QEw0 ...// john la, my project...
    // Project by Navratan api key... AIzaSyBNqOO86dgxXL5d4SH74jNKoJN5nUBsA6Y...AIzaSyBNqOO86dgxXL5d4SH74jNKoJN5nUBsA6Y
//   // https://maps.googleapis.com/maps/api/geocode/json?key=AbCdEfGhIjKlMnOpQrStUvWxYz&address=Dallas&sensor=true
//    
//   // 292616665360-ctqcbcheau6tp8sik9u9344vsn5govnj.apps.googleusercontent.com
    
  //  https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=yourlatitude,yourlongitude&radius=5000&sensor=true&key=SERVERKEY
    
    
    NSData *data=[NSData dataWithContentsOfURL:[NSURL URLWithString:req]];
    
    if (data != nil) {
        
    
    
    NSDictionary *result1 = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];

    //NSString *result = [NSString stringWithContentsOfURL:[NSURL URLWithString:req] encoding:NSUTF8StringEncoding error:NULL];
    
    
     if (result1) {
    
         
         NSArray *arrayTemp =[result1 objectForKey:@"results"];
         if ([arrayTemp count]>0) {
             
         
    NSDictionary *partialJsonDict = [arrayTemp objectAtIndex:0];
    NSDictionary *geometryDict = [partialJsonDict objectForKey:@"geometry"];
     latitude1 = [[[geometryDict objectForKey:@"location"] objectForKey:@"lat"] floatValue];
     latitude2 = [[[geometryDict objectForKey:@"location"] objectForKey:@"lng"] floatValue];
         }
     }
    }
    
   

    CLLocationCoordinate2D center;
    center.latitude=latitude1;
    center.longitude = latitude2;
    NSLog(@"View Controller get Location Logitute : %f",center.latitude);
    NSLog(@"View Controller get Location Latitute : %f",center.longitude);
    return center;
    
}


+(NSArray *)filterTaskForUserID:(NSString *)userID taskStatus:(TMStatus)taskStatus{
    
    NSArray *arrayFiltered;
    
//    for (task *obj in myAppDelegate.arrayTaskDetail) {
//        NSLog(@"tast id === %@ and task status === %@ and created by id === %@",obj.taskid,obj.status,obj.userid);
//    }
    NSLog(@"count of array detail in filterTaskForUserID is....%lu",(unsigned long)myAppDelegate.arrayTaskDetail.count);
    
    if (taskStatus == TMViewAllTask) {
        
        NSPredicate *statusPredicate = [NSPredicate predicateWithFormat:@"SELF.status MATCHES[cd] %@",[self convertToString:taskStatus]];
        arrayFiltered = [myAppDelegate.arrayTaskDetail filteredArrayUsingPredicate:statusPredicate];

#pragma mark - for showing task only of other users not self in view all task list
//        NSPredicate *userIdPredicate = [NSPredicate predicateWithFormat:@"SELF.userid MATCHES[cd] %@",userID];
//        NSArray *userIDFilteredArray = [arrayFiltered filteredArrayUsingPredicate:userIdPredicate];
//        
//        NSMutableArray *copyOfArrayFiltered = [arrayFiltered mutableCopy];
//        
//        [copyOfArrayFiltered removeObjectsInArray:userIDFilteredArray];
//        
//        arrayFiltered = nil;
//        
//        arrayFiltered = copyOfArrayFiltered;
        
    }
    else if (taskStatus == TMActiveTask_Accepted || taskStatus == TMActiveTask_Complete || taskStatus == TMTaskHistory) {
        
        NSPredicate *statusPredicate = [NSPredicate predicateWithFormat:@"SELF.status MATCHES[cd] %@ AND SELF.userid MATCHES[cd] %@",[self convertToString:taskStatus],userID];
        arrayFiltered = [myAppDelegate.arrayTaskDetail filteredArrayUsingPredicate:statusPredicate];
        
        NSMutableArray *copyOfArrayFiltered = [arrayFiltered mutableCopy];

#pragma mark - for showing self created task with status 0 in active task list
//        if (taskStatus == TMActiveTask_Accepted || taskStatus == TMActiveTask_Complete) {
//            
//            NSPredicate *status0Predicate = [NSPredicate predicateWithFormat:@"SELF.status MATCHES[cd] %@ AND SELF.userid MATCHES[cd] %@",[self convertToString:TMViewAllTask],userID];
//            NSArray *selfFilteredArray = [myAppDelegate.arrayTaskDetail filteredArrayUsingPredicate:status0Predicate];
//
//            [copyOfArrayFiltered addObjectsFromArray:selfFilteredArray];
//
//        }

        NSPredicate *workerIdPredicate = [NSPredicate predicateWithFormat:@"SELF.workerId MATCHES[cd] %@",userID];
        NSArray *workerIdFilteredArray = [myAppDelegate.arrayAcceptedTaskDetail filteredArrayUsingPredicate:workerIdPredicate];
        
        NSArray *uniqueArray = [workerIdFilteredArray valueForKeyPath:@"@distinctUnionOfObjects.self.taskId"];

        NSMutableArray *taskIdArr = [[NSMutableArray alloc]init];
        for (NSString *acceptTaskObj in uniqueArray) {
            
            NSString *filterString =[NSString stringWithFormat:@"SELF.taskid MATCHES[cd] '%@' AND SELF.status MATCHES[cd] '%@'",acceptTaskObj,[self convertToString:taskStatus]];
            NSPredicate *taskIdPredicate = [NSPredicate predicateWithFormat:filterString];
            NSArray *newAcceptedTaskFilteredArray = [myAppDelegate.arrayTaskDetail filteredArrayUsingPredicate:taskIdPredicate];
            [taskIdArr addObjectsFromArray:newAcceptedTaskFilteredArray];
            
        }
        
        [copyOfArrayFiltered addObjectsFromArray:taskIdArr];
        
        NSArray *uniqueArray2 = [copyOfArrayFiltered valueForKeyPath:@"@distinctUnionOfObjects.self"];

        arrayFiltered = nil;
        
        arrayFiltered = uniqueArray2;

    }
    
    NSMutableArray *array = [arrayFiltered mutableCopy];
    
    [array sortUsingDescriptors:
     @[
       [NSSortDescriptor sortDescriptorWithKey:@"taskid_Integer" ascending:NO],
       ]];

    arrayFiltered = array;
    
//    for (task *obj in arrayFiltered) {
//        NSLog(@"filtered tast id === %@ and task status === %@ and created by id ==`= %@",obj.taskid,obj.status,obj.userid);
//    }

    return arrayFiltered;

}


//image work
#pragma mark - image handling
// seconddd
-(CLLocationCoordinate2D) getLocationFromAddressString: (NSString*) addressStr {
    
    
    double latitude = 0, longitude = 0;
    
    Float32 latitude1 = -1;
    Float32 latitude2 = -1;
    
    
    NSString *esc_addr =  [addressStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
  //  NSString *req = [NSString stringWithFormat:@"https://maps.google.com/maps/api/geocode/json?sensor=false&address=%@", esc_addr];
    
 NSString *req = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyCAp9xXjxpQNnpBUT9znFsB6eoagYhKeVQ&address=%@&sensor=true", esc_addr];
    
      //AIzaSyCAp9xXjxpQNnpBUT9znFsB6eoagYhKeVQ ...// Key by Neeraj Sir 14 JAn 19
    
  // New billing key with luke la account  AIzaSyAMAlYUxS3GeG2yzjZaEs5ae9PO5eUpU7k
    
    // NEW Key Geocoding by vs....AIzaSyCXCXTyEwyOr9g-EMsu-wdsTesytP6QEw0
    
    // Project by Navratan api key... AIzaSyBNqOO86dgxXL5d4SH74jNKoJN5nUBsA6Y
    
    NSData *data=[NSData dataWithContentsOfURL:[NSURL URLWithString:req]];
    
    //You have exceeded your daily request quota for this API. If you did not set a custom daily request quota, verify your project has an active billing account: http://g.co/dev/maps-no-account
    
    if (data != nil)
    {
        
    
    
    NSDictionary *result1 = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
    
   // NSString *result = [NSString stringWithContentsOfURL:[NSURL URLWithString:req] encoding:NSUTF8StringEncoding error:NULL];

    
    if (result1) {
        
        
        NSArray *arrayTemp =[result1 objectForKey:@"results"];
        if ([arrayTemp count]>0) {
            
            
            NSDictionary *partialJsonDict = [arrayTemp objectAtIndex:0];
            NSDictionary *geometryDict = [partialJsonDict objectForKey:@"geometry"];
            latitude1 = [[[geometryDict objectForKey:@"location"] objectForKey:@"lat"] floatValue];
            latitude2 = [[[geometryDict objectForKey:@"location"] objectForKey:@"lng"] floatValue];
        }
    }

    }
//    if (result) {
//        NSScanner *scanner = [NSScanner scannerWithString:result];
//        if ([scanner scanUpToString:@"\"lat\" :" intoString:nil] && [scanner scanString:@"\"lat\" :" intoString:nil]) {
//            [scanner scanDouble:&latitude];
//            if ([scanner scanUpToString:@"\"lng\" :" intoString:nil] && [scanner scanString:@"\"lng\" :" intoString:nil]) {
//                [scanner scanDouble:&longitude];
//            }
//        }
//    }
    CLLocationCoordinate2D center;
    center.latitude=latitude1;
    center.longitude = latitude2;
    NSLog(@"View Controller get Location Logitute : %f",center.latitude);
    NSLog(@"View Controller get Location Latitute : %f",center.longitude);
    return center;
}

+(UIImage *) getImageFromURL:(NSString *)fileURL {
    UIImage * result;
    
    NSString *newUrl =[fileURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];

    NSData * data = [NSData dataWithContentsOfURL:[NSURL URLWithString:newUrl]];
    result = [UIImage imageWithData:data];
    
    return result;
}

+(void)createDirForImageInCache :(NSString *)dirName{
    NSString *path;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    path = [[paths objectAtIndex:0] stringByAppendingPathComponent:dirName];
    NSError *error;
    if (![[NSFileManager defaultManager] fileExistsAtPath:path]){ //Does directory already exist?
        if (![[NSFileManager defaultManager] createDirectoryAtPath:path
                                       withIntermediateDirectories:NO
                                                        attributes:nil
                                                             error:&error]){
            NSLog(@"Create directory error: %@", error);
        }
    }
}


+(void)delDirForImage :(NSString *)dirName{
    NSString *path;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    path = [[paths objectAtIndex:0] stringByAppendingPathComponent:dirName];
    NSError *error;
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:path]){ //Does directory exist?
        if (![[NSFileManager defaultManager] removeItemAtPath:path error:&error]){    //Delete it
            NSLog(@"Delete directory error: %@", error);
        }
    }
}

+(void)saveimage:(UIImage *)image imageName:(NSString *)imageName dirname:(NSString *)dirname{
    NSString *imageURL = imageName;
    NSString *jpgFilePath = [NSString stringWithFormat:@"/%@/%@.jpeg", dirname,imageURL];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *documentDirectory = [paths objectAtIndex:0];
    NSString *dir = [documentDirectory stringByAppendingPathComponent:jpgFilePath];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if (![fileManager fileExistsAtPath:dir]){
        NSData *data1 = [NSData dataWithData:UIImageJPEGRepresentation(image, 1.0f)];
        [data1 writeToFile:dir atomically:YES];
    }
}

+ (UIImage*)loadImageOfLogoFromCacheFolder:(NSString*)imageName dirname:(NSString *)dirname{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *fullPath = [documentsDirectory stringByAppendingPathComponent:
                          [NSString stringWithFormat:@"/%@/%@",dirname, imageName]];
    return [UIImage imageWithContentsOfFile:fullPath];
}

+ (void)removeImage:(NSString*)imageName dirname:(NSString *)dirname{
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *fullPath = [documentsDirectory stringByAppendingPathComponent:
                          [NSString stringWithFormat:@"/%@/%@",dirname, imageName]];
    [fileManager removeItemAtPath: fullPath error:NULL];
}
+ (NSString*)convertDateFormat:(NSString *)date{
    

    
    NSArray* foo = [date componentsSeparatedByString: @"/"];
    NSString* year = [foo objectAtIndex: 0];
    int monthNumber = [[foo objectAtIndex:1]intValue];
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    NSString *monthName = [[df monthSymbols] objectAtIndex:(monthNumber-1)];
    NSString *day = [foo objectAtIndex:2];
    NSArray* dayArray = [day componentsSeparatedByString: @" "];
    day = [dayArray objectAtIndex:0];
    monthName=[monthName uppercaseString];
    
    
    
    NSString *str = [monthName lowercaseString];
    NSString *subStr = [str substringWithRange:NSMakeRange(0, 3)];
    
    /* create a locale where diacritic marks are not considered important, e.g. US English */
    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en-US"];
    
    /* get first char */
    NSString *firstChar = [subStr substringToIndex:1];
    
    /* remove any diacritic mark */
    NSString *folded = [firstChar stringByFoldingWithOptions:NSDiacriticInsensitiveSearch locale:locale];
    
    /* create the new string */
    NSString *result = [[folded uppercaseString] stringByAppendingString:[subStr substringFromIndex:1]];
    
  result = [result stringByAppendingString:@"."];
    NSString *creator_datestring=[NSString stringWithFormat:@"%@ %@, %@",result,day,year];
    
    return creator_datestring;
    
    
}
+ (NSString*)convertTimeFormat:(NSString *)time{

NSDateFormatter *dateFormatter3 = [[NSDateFormatter alloc] init];
[dateFormatter3 setDateStyle:NSDateFormatterMediumStyle];
[dateFormatter3 setDateFormat:@"HH:mm:ss"];
NSDate *date1 = [dateFormatter3 dateFromString:time];


NSDateFormatter *formatter0 = [[NSDateFormatter alloc] init];
NSLocale *locale = [[NSLocale alloc]initWithLocaleIdentifier:@"en_US"];
[formatter0 setLocale:locale];
[formatter0 setDateFormat:@"hh:mm a"];
NSString *finalTime = [formatter0 stringFromDate:date1];



return finalTime;
}

+(NSString *)getTimeZone
{
    
    NSTimeZone *timeZone = [NSTimeZone localTimeZone];
    NSString *tzName = [timeZone name];
    
    NSLog(@"Local time zone is...%@",tzName);
    
    NSInteger millisecondsFromGMT = [[NSTimeZone localTimeZone] secondsFromGMT];
    
    NSString *timeZonewithSeconds = [NSString stringWithFormat:@"%ld",millisecondsFromGMT];
    
    return timeZonewithSeconds;
}

@end
