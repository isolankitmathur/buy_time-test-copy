//
//  CustomSegue.m
//  Patrobuddy
//
//  Created by User14 on 20/03/15.
//
//

#import "CustomSegue.h"
#import "Constants.h"
#import "ViewController.h"
#import "TaskHistoryViewController.h"
#import "ActiveTaskViewController.h"
#import "ViewAllTaskViewController.h"
#import "TaskDetailViewController.h"
#import "CreateBioViewController.h"

#import "ReviewViewController.h"
#import "ShareReviewDetailViewController.h"

//tag constant values defination
//tag 450 task detail view
//tag 452 user profile view
//tag 451 task history view


@implementation CustomSegue

- (void) perform {
    
    [self performSelector:@selector(animateSegue) withObject:nil afterDelay:0.1];
    
}

-(void)animateSegue{
    
    if ([self.sourceViewController isKindOfClass:[TaskDetailViewController class]]) {
        
        TaskDetailViewController *src = (TaskDetailViewController *)self.sourceViewController;
        UIViewController *dst = (UIViewController *) self.destinationViewController;
        
        if ([self.identifier isEqualToString:@"rating"]) {
            
            for(UIView *view in src.viewRatingViewControllerHolder.subviews){
                [view removeFromSuperview];
            }
            
            [src.viewRatingViewControllerHolder setHidden:NO];
            
            CATransition *transitionAnimation = [CATransition animation];
            [transitionAnimation setType:kCATransitionPush];
            [transitionAnimation setSubtype:kCATransitionFromRight];
            [transitionAnimation setDuration:0.3f];
            [transitionAnimation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
            [transitionAnimation setFillMode:kCAFillModeBoth];
            
            [src.viewRatingViewControllerHolder.layer addAnimation:transitionAnimation forKey:@"changeView"];
            
            [src.viewRatingViewControllerHolder addSubview:dst.view];
            
        }else{
            
            //view handling
            CATransition *transitionAnimation = [CATransition animation];
            [transitionAnimation setType:kCATransitionPush];
            [transitionAnimation setSubtype:kCATransitionFromRight];
            [transitionAnimation setDuration:0.5f];
            [transitionAnimation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
            [transitionAnimation setFillMode:kCAFillModeBoth];
            
            if (!myAppDelegate.isComingFromPushNotification) {
                
                for(UIView *view in src.viewOtherUserProfileHolder.subviews){
                    if (view.tag == 450 || view.tag == 451 || view.tag == 452) {
                        [view removeFromSuperview];
                    }
                }
                
                if (myAppDelegate.isOfOtherUserTaskDetail){
                    
                    dst.view.tag = 450;
                    
                }else if (myAppDelegate.isOfOtherUserTaskHistory){
                    
                    dst.view.tag = 452;
                    
                }else if (myAppDelegate.isOfOtherUser) {
                    
                    dst.view.tag = 451;
                    
                }
                
            }else{
                
                for(UIView *view in src.viewOtherUserProfileHolder.subviews){
                    [view removeFromSuperview];
                }
                
            }
            
            
            [src.viewOtherUserProfileHolder.layer addAnimation:transitionAnimation forKey:@"changeView"];
            
            [src.viewOtherUserProfileHolder setHidden:NO];
            
            [src.viewOtherUserProfileHolder addSubview:dst.view];
            
        }
    }else if ([self.sourceViewController isKindOfClass:[ViewAllTaskViewController class]]) {
        
        ViewAllTaskViewController *src = (ViewAllTaskViewController *)self.sourceViewController;
        UIViewController *dst = (UIViewController *) self.destinationViewController;
        
        if ([self.identifier isEqualToString:@"mapView"]) {
            
            for(UIView *view in src.viewMapHolder.subviews){
                [view removeFromSuperview];
            }
            
            [src.viewMapHolder setHidden:NO];
            
            CATransition *transitionAnimation = [CATransition animation];
            [transitionAnimation setType:kCATransitionPush];
            [transitionAnimation setSubtype:kCATransitionFromRight];
            [transitionAnimation setDuration:0.3f];
            [transitionAnimation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
            [transitionAnimation setFillMode:kCAFillModeBoth];
            
            [src.viewMapHolder.layer addAnimation:transitionAnimation forKey:@"changeView"];
            
            [src.viewMapHolder addSubview:dst.view];
            
        }else{
            
            myAppDelegate.comingFromClass = @"ViewAllTaskViewController";
            
            myAppDelegate.superViewClassName = @"ViewAllTaskViewController";
            
            for(UIView *view in src.viewTaskDetailHolder.subviews){
                [view removeFromSuperview];
            }
            
            [src.viewTaskDetailHolder setHidden:NO];
            
            CATransition *transitionAnimation = [CATransition animation];
            [transitionAnimation setType:kCATransitionPush];
            [transitionAnimation setSubtype:kCATransitionFromRight];
            [transitionAnimation setDuration:0.3f];
            [transitionAnimation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
            [transitionAnimation setFillMode:kCAFillModeBoth];
            
            [src.viewTaskDetailHolder.layer addAnimation:transitionAnimation forKey:@"changeView"];
            
            [src.viewTaskDetailHolder addSubview:dst.view];
            
        }
        
    }
    
    
    
//    else if ([self.sourceViewController isKindOfClass:[TaskHistoryViewController class]]) {
//        
//        TaskHistoryViewController *src = (TaskHistoryViewController *)self.sourceViewController;
//        UIViewController *dst = (UIViewController *) self.destinationViewController;
//        
//        if (!myAppDelegate.isComingFromPushNotification) {
//            
//            myAppDelegate.superViewClassName = @"TaskHistoryViewController";
//            myAppDelegate.comingFromClass = @"TaskHistoryViewController";
//            
//            if (myAppDelegate.isFromProfile) {
//                myAppDelegate.superViewClassName = @"ProfileViewController";
//            }
//            
//        }
//        
//        for(UIView *view in src.viewTaskDetailHolder.subviews){
//            [view removeFromSuperview];
//        }
//        
//        [src.viewTaskDetailHolder setHidden:NO];
//        
//        CATransition *transitionAnimation = [CATransition animation];
//        [transitionAnimation setType:kCATransitionPush];
//        [transitionAnimation setSubtype:kCATransitionFromRight];
//        [transitionAnimation setDuration:0.3f];
//        [transitionAnimation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
//        [transitionAnimation setFillMode:kCAFillModeBoth];
//        
//        [src.viewTaskDetailHolder.layer addAnimation:transitionAnimation forKey:@"changeView"];
//        
//        [src.viewTaskDetailHolder addSubview:dst.view];
//        
//    }
    
    
    
    else if ([self.sourceViewController isKindOfClass:[ActiveTaskViewController class]]) {
        
        ActiveTaskViewController *src = (ActiveTaskViewController *)self.sourceViewController;
        UIViewController *dst = (UIViewController *) self.destinationViewController;
        
        myAppDelegate.superViewClassName = @"ActiveTaskViewController";
        myAppDelegate.comingFromClass = @"ActiveTaskViewController";
        
        for(UIView *view in src.viewTaskDetailHolder.subviews){
            [view removeFromSuperview];
        }
        
        [src.viewTaskDetailHolder setHidden:NO];
        
        CATransition *transitionAnimation = [CATransition animation];
        [transitionAnimation setType:kCATransitionPush];
        [transitionAnimation setSubtype:kCATransitionFromRight];
        [transitionAnimation setDuration:0.3f];
        [transitionAnimation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
        [transitionAnimation setFillMode:kCAFillModeBoth];
        
        [src.viewTaskDetailHolder.layer addAnimation:transitionAnimation forKey:@"changeView"];
        
        [src.viewTaskDetailHolder addSubview:dst.view];
        
    }else if ([self.sourceViewController isKindOfClass:[ViewController class]]) {
        
        ViewController *src = (ViewController *)self.sourceViewController;
        UIViewController *dst = (UIViewController *) self.destinationViewController;
        
        if (!myAppDelegate.isComingFromPushNotification) {
            
            if (myAppDelegate.isPaymentORBankInfo){
                
                if (!myAppDelegate.isFromEdit  && !myAppDelegate.isFromAlert) {
                    myAppDelegate.superViewClassName = @"ViewController";
                }else if (myAppDelegate.isFromAlert) {
                    myAppDelegate.superViewClassName = @"ViewAllTaskViewController";
                }
                
                
                for (UIView *view in src.viewHolderWithoutHeader.subviews) {
                    [view removeFromSuperview];
                }
                
                [src.viewHolderWithoutHeader setHidden:NO];
                
                CATransition *transitionAnimation = [CATransition animation];
                [transitionAnimation setType:kCATransitionPush];
                [transitionAnimation setSubtype:kCATransitionFromRight];
                [transitionAnimation setDuration:0.3f];
                [transitionAnimation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
                [transitionAnimation setFillMode:kCAFillModeBoth];
                
                [src.viewHolderWithoutHeader.layer addAnimation:transitionAnimation forKey:@"changeView"];
                
                [src.viewHolderWithoutHeader addSubview:dst.view];
                
            }else{
                
                if ([self.identifier isEqualToString:@"pushTask"]) {
                    
                    CATransition *transitionAnimation = [CATransition animation];
                    [transitionAnimation setType:kCATransitionPush];
                    [transitionAnimation setSubtype:kCATransitionFromRight];
                    [transitionAnimation setDuration:0.3f];
                    [transitionAnimation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
                    [transitionAnimation setFillMode:kCAFillModeBoth];
                    
                    for(UIView *view in src.HolderViewWithHeaderForPushNotification.subviews){
                        [view removeFromSuperview];
                    }
                    
                    [src.viewHolderWithoutHeader setHidden:YES];
                    
                    [src.mapViewToShowUserLocationOnMap setHidden:YES];
                    
                    [src.HolderViewWithHeaderForPushNotification setHidden:NO];
                    
                    [src.HolderViewWithHeaderForPushNotification.layer addAnimation:transitionAnimation forKey:@"changeView"];
                    
                    [src.HolderViewWithHeaderForPushNotification addSubview:dst.view];
                    
                }else{
                    
                    if (myAppDelegate.isFromEdit) {
                        myAppDelegate.superViewClassName = @"ViewController";
                    }
                    
                    for(UIView *view in src.viewHolderWithHeader.subviews){
                        [view removeFromSuperview];
                    }
                    
                    [src.viewHolderWithHeader setHidden:NO];
                    
                    CATransition *transitionAnimation = [CATransition animation];
                    [transitionAnimation setType:kCATransitionPush];
                    
                    if (myAppDelegate.isComingFromRating || myAppDelegate.isComingBackToSettings) {
                        [transitionAnimation setSubtype:kCATransitionFromLeft];
                        myAppDelegate.isComingFromRating = false;
                        myAppDelegate.isComingBackToSettings = false;
                    }else{
                        [transitionAnimation setSubtype:kCATransitionFromRight];
                    }
                    
                    [transitionAnimation setDuration:0.3f];
                    [transitionAnimation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
                    [transitionAnimation setFillMode:kCAFillModeBoth];
                    
                    [src.viewHolderWithHeader.layer addAnimation:transitionAnimation forKey:@"changeView"];
                    
                    [src.viewHolderWithHeader addSubview:dst.view];
                    
                }
                
            }

        }else{
            
            CATransition *transitionAnimation = [CATransition animation];
            [transitionAnimation setType:kCATransitionPush];
            [transitionAnimation setSubtype:kCATransitionFromRight];
            [transitionAnimation setDuration:0.3f];
            [transitionAnimation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
            [transitionAnimation setFillMode:kCAFillModeBoth];

            if ([self.identifier isEqualToString:@"pushTask"]) {
                
                for(UIView *view in src.HolderViewWithHeaderForPushNotification.subviews){
                    [view removeFromSuperview];
                }
                
                [src.viewHolderWithoutHeader setHidden:YES];
                
                [src.mapViewToShowUserLocationOnMap setHidden:YES];
                
                [src.HolderViewWithHeaderForPushNotification setHidden:NO];
                
                [src.HolderViewWithHeaderForPushNotification.layer addAnimation:transitionAnimation forKey:@"changeView"];
                
                [src.HolderViewWithHeaderForPushNotification addSubview:dst.view];
                
            }else{
                
                for(UIView *view in src.HolderViewWithHeaderForPushNotification.subviews){
                    [view removeFromSuperview];
                }
                
                [src.HolderViewWithHeaderForPushNotification setHidden:NO];
                
                [src.HolderViewWithHeaderForPushNotification.layer addAnimation:transitionAnimation forKey:@"changeView"];
                
                [src.HolderViewWithHeaderForPushNotification addSubview:dst.view];

            }

        }
        
    }else if ([self.sourceViewController isKindOfClass:[CreateBioViewController class]]) {
        
        CreateBioViewController *src = (CreateBioViewController *)self.sourceViewController;
        UIViewController *dst = (UIViewController *) self.destinationViewController;
        CATransition *transitionAnimation = [CATransition animation];
        [transitionAnimation setType:kCATransitionPush];
        [transitionAnimation setSubtype:kCATransitionFromRight];
        [transitionAnimation setDuration:0.3f];
        [transitionAnimation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
        [transitionAnimation setFillMode:kCAFillModeBoth];
        
//        if ([self.identifier isEqualToString:@"pushTask"]) {
//            
//            for(UIView *view in src.HolderViewWithHeaderForPushNotification.subviews){
//                [view removeFromSuperview];
//            }
        
            for(UIView *view in src.mainUpperHolderVIew.subviews){
                [view removeFromSuperview];
            }
            [src.mainUpperHolderVIew setHidden:NO];
            
            [src.mainUpperHolderVIew.layer addAnimation:transitionAnimation forKey:@"changeView"];
            
            [src.mainUpperHolderVIew addSubview:dst.view];
        }
    
    
    
    else if ([self.sourceViewController isKindOfClass:[ReviewViewController class]]) {
        
        ReviewViewController *src = (ReviewViewController *)self.sourceViewController;
        UIViewController *dst = (UIViewController *) self.destinationViewController;
        CATransition *transitionAnimation = [CATransition animation];
        [transitionAnimation setType:kCATransitionPush];
        [transitionAnimation setSubtype:kCATransitionFromRight];
        [transitionAnimation setDuration:0.3f];
        [transitionAnimation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
        [transitionAnimation setFillMode:kCAFillModeBoth];
        
        
        for(UIView *view in src.reviewRejectionView.subviews){
            [view removeFromSuperview];
        }
        [src.reviewRejectionView setHidden:NO];
        
        [src.reviewRejectionView.layer addAnimation:transitionAnimation forKey:@"changeView"];
        
        [src.reviewRejectionView addSubview:dst.view];
    }
    
    
    else if ([self.sourceViewController isKindOfClass:[TaskHistoryViewController class]]) {
        
        TaskHistoryViewController *src = (TaskHistoryViewController *)self.sourceViewController;
        UIViewController *dst = (UIViewController *) self.destinationViewController;
        CATransition *transitionAnimation = [CATransition animation];
        [transitionAnimation setType:kCATransitionPush];
        [transitionAnimation setSubtype:kCATransitionFromRight];
        [transitionAnimation setDuration:0.3f];
        [transitionAnimation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
        [transitionAnimation setFillMode:kCAFillModeBoth];
        
        
        for(UIView *view in src.reviewDetailView.subviews){
            [view removeFromSuperview];
        }
        [src.reviewDetailView setHidden:NO];
        
        [src.reviewDetailView.layer addAnimation:transitionAnimation forKey:@"changeView"];
        
        [src.reviewDetailView addSubview:dst.view];
    }

    else if ([self.sourceViewController isKindOfClass:[ReivewDetailViewController class]]) {
        
        ReivewDetailViewController *src = (ReivewDetailViewController *)self.sourceViewController;
        UIViewController *dst = (UIViewController *) self.destinationViewController;
        CATransition *transitionAnimation = [CATransition animation];
        [transitionAnimation setType:kCATransitionPush];
        [transitionAnimation setSubtype:kCATransitionFromRight];
        [transitionAnimation setDuration:0.3f];
        [transitionAnimation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
        [transitionAnimation setFillMode:kCAFillModeBoth];
        
        
        for(UIView *view in src.shareReviewDetailReviewView.subviews){
            [view removeFromSuperview];
        }
        [src.shareReviewDetailReviewView setHidden:NO];
        [src.shareReviewDetailReviewView.layer addAnimation:transitionAnimation forKey:@"changeView"];
        [src.shareReviewDetailReviewView addSubview:dst.view];
    }
      
    
}

@end
