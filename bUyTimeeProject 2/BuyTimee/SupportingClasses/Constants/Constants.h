//
//  Constants.h
//  BuyTimee
//
//  Created by User14 on 26/02/16.

//

#import "AppDelegate.h"
#import "WebService.h"
#import "EncryptDecryptFile.h"
#import "ParsingClass.h"
#import "DatabaseClass.h"


#ifndef Constants_h
#define Constants_h

#define myAppDelegate ((AppDelegate *)[[UIApplication sharedApplication] delegate])
#define degreesToRadians(x)(x * M_PI / 180)
#define TimeStamp [NSString stringWithFormat:@"%0.0f",[[NSDate date] timeIntervalSince1970] * 1000]

#pragma mark - color codes
#define colorGreenLight [UIColor colorWithRed:72.0/255.0 green:215.0/255.0 blue:199.0/255.0 alpha:1]
#define colorGreenDark [UIColor colorWithRed:63.0/255.0 green:67.0/255.0 blue:70.0/255.0 alpha:1]
//#define colorGreenLight2 [UIColor colorWithRed:72.0/255.0 green:215.0/255.0 blue:199.0/255.0 alpha:1]
//#define colorGreenLight3 [UIColor colorWithRed:72.0/255.0 green:215.0/255.0 blue:199.0/255.0 alpha:1]
//#define colorGreenLight4 [UIColor colorWithRed:72.0/255.0 green:215.0/255.0 blue:199.0/255.0 alpha:1]


//*************************** old account***************************************
//#define STRIPE_TEST_PUBLIC_KEY @"pk_test_fAnrdZxBAHGgSY0xD8bxcVjD"  // to be changed
//#define STRIPE_LIVE_PUBLIC_KEY @"pk_live_qvuLqMQS8CQEbXscvffuHyVh"  // to be changed
//***************************************************************************



#define STRIPE_TEST_PUBLIC_KEY @"pk_test_AU6CF1aD62wT54sxiIRTsUCH"  // to be changed
#define STRIPE_LIVE_PUBLIC_KEY @"pk_live_F7Wag5Kwkk82rxXbhNI69MHz"  // to be changed




#define imageVisa [UIImage imageNamed:@"settings_visa_icon"]
#define imageMasterCard [UIImage imageNamed:@"settings_MasterCard"]
#define imageDiscover [UIImage imageNamed:@"settings_Discover-dark"]
#define imageAmericanExpress [UIImage imageNamed:@"settings_AmericanExpress-dark"]
#define imageUserDefault [UIImage imageNamed:@"User_Circle"]





////////// **************** BUY TIMEE Links....


//static NSString *urlServer=@"http://admin.buytimee.com/%@";// new client url given by client on 06 march 17 for build...

//static NSString *urlServer=@"http://isolherbal.com/isn/buytimee_web/%@";// new dev url given by ishan on 02 march 17 for build...

//static NSString *urlServerCreateTask = @"http://isolherbal.com/isn/buytimee_web/Reservation_Info/create_reservation"; // new crate reservation dev url given by ishan  on 02 march 17 for build...

//static NSString *urlServerCreateTask = @"http://admin.buytimee.com/Reservation_Info/create_reservation"; // link by client 06 march 17.


//static NSString *urlServer=@"https://admin.buytimee.com/development/%@";// new development clent url given by client on 07 march 17 for build...
//
//static NSString *urlServerCreateTask = @"https://admin.buytimee.com/development/Reservation_Info/create_reservation"; // link by client 06 march 17.


//static NSString *urlServer=@"https://admin.buytimee.com/test/%@";// new test clent url given by client on 28 march 17 for build...

//static NSString *urlServerCreateTask = @"https://admin.buytimee.com/test/Reservation_Info/create_reservation"; // test link by client 28 march 17.


static NSString *urlServer = @"https://admin.buytimee.com/%@";//  Taken by RP for live app 20 Dec 2017
//
static NSString *urlServerCreateTask = @"https://admin.buytimee.com/Reservation_Info/create_reservation"; //  Taken by RP for live app 20 Dec 2017
 

//
//static NSString *urlServer=@"http://192.168.5.130/BuyTimee/%@"; // For anil 11 april 18
//
//static NSString *urlServerCreateTask=@"https://192.168.5.130/BuyTimee/Reservation_Info/%@"; // For Anil 11 april 18

//static NSString *urlServer=@"http://192.168.5.136/BuyTimee/%@"; // For Naveen sir 01 June 18

//static NSString *urlServerCreateTask=@"https://192.168.5.136/BuyTimee/Reservation_Info/%@"; // For Naveen sir 01 June 18

//https://admin.buytimee.com/developmentest/
//static NSString *urlServer=@"https://admin.buytimee.com/developmentest/%@";// new development url given by naveen sir on 04 Oct 18 for build...

//static NSString *urlServerCreateTask = @"https://admin.buytimee.com/developmentest//Reservation_Info/create_reservation"; // new development url given by naveen sir on 04 Oct 18 for build...



#define last_update_time_default @"0000-00-00 00:00:00"
#define dateFormatOnlyHour @"HH:mm:ss"
#define dateFormatFull @"YYYY-MM-dd"//old AK @"YYYY-MM-dd HH:mm:ss"
#define dateFormatDate @"YYYY/MM/dd"
#define dateFormatTimeDate @"YYYY-MM-dd HH:mm:ss"

#define dateFormatMDY @"MM-dd-yyyy" //Change by RP
//#define dateFormatDate @"MM/dd/YYYY" //Change by RP
//#define dateFormatTimeDate @"MM-dd-YYYY HH:mm:ss" //Change by RP

#pragma mark - nsuserdefaultKeys

#define keyIsLogin @"isLogin"
#define keyUserloginDetailDictionary @"UserloginDetailDictionary"
#define keyvenderDetails @"venderDetails"
#define keyBankInfoDetails @"BankInfo"
#define keyLastModifyTime @"lastModifyTime"
#define keyUserType @"usertype"
#define keyUsername @"username"
#define keyPass @"password"
#define keyUserId @"userId"

#define keysocialFlag_fb @"flag_fb"
#define keysocialEmailId_fb @"email_fb"
#define keysocialId_fb @"socialId_fb"

#define keyAgreeTnC @"AgreeTnC"
#define keyBioFilled @"BioFilled"


#pragma mark - cell reuse identifier
#define cellBioIdentifier @"cellBio"
#define cellTaskIdentifier @"cellTask"
#define cellCreateTaskIdentifier @"cellCreateTask"
#define cellActiveTaskIdentifier @"cellActiveTask"
#define cellTaskHistoryIdentifier @"cellTaskHistory"
#define cellTermsNConditionIdentifier @"cellTermsNCondition"
#define cellTaskDetailIdentifier @"cellTaskDetail"
#define cellRequestReservation @"requestReservationCell"


#pragma mark - table names
#define tableAcceptedTask @"AcceptedTaskDetail"
#define tableCategory @"CategoryData"
#define tableRating @"RatingTable"
#define tableSubCategory @"SubCategoryData"
#define tableTaskDetail @"TaskData"
#define tableTnC @"TnCData"
#define tableBank @"UserBankData"
#define tableUser @"UserData"
#define tablePayment @"UserPaymentData"
#define tablePaymentPaid @"UserPaymentPaidInfo"
#define tableTaskImage @""
#define tableBusinessInfo @"BusinessInfo"
#define tableBankInfo @"BankInfo"
#define tableVandorProfle @"vendor"

#pragma mark - additional

#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define IS_RETINA ([[UIScreen mainScreen] scale] >= 2.0)

#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)
#define SCREEN_MAX_LENGTH (MAX(SCREEN_WIDTH, SCREEN_HEIGHT))
#define SCREEN_MIN_LENGTH (MIN(SCREEN_WIDTH, SCREEN_HEIGHT))

#define IS_IPHONE_4_OR_LESS (IS_IPHONE && SCREEN_MAX_LENGTH < 568.0)
#define IS_IPHONE_5 (IS_IPHONE && SCREEN_MAX_LENGTH == 568.0)
#define IS_IPHONE_6 (IS_IPHONE && SCREEN_MAX_LENGTH == 667.0)
#define IS_IPHONE_6P (IS_IPHONE && SCREEN_MAX_LENGTH == 736.0)

#define IS_IPHONE5 (([[UIScreen mainScreen] bounds].size.height-568)?NO:YES)
#define IS_OS_5_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 5.0)
#define IS_OS_6_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 6.0)
#define IS_OS_7_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)
#define IS_OS_8_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)

#define IS_IPAD_PRO_1366 (IS_IPAD && MAX(SCREEN_WIDTH,SCREEN_HEIGHT) == 1366.0)
#define IS_IPAD_PRO_1024 (IS_IPAD && MAX(SCREEN_WIDTH,SCREEN_HEIGHT) == 1024.0)

/** String: Identifier **/
#define DEVICE_IDENTIFIER ( ( IS_IPAD ) ? DEVICE_IPAD : ( IS_IPHONE ) ? DEVICE_IPHONE , DEVICE_SIMULATOR )

/** String: iPhone **/
#define DEVICE_IPHONE @"iPhone"

/** String: iPad **/
#define DEVICE_IPAD @"iPad"

/** String: Device Model **/
#define DEVICE_MODEL ( [[UIDevice currentDevice ] model ] )

/** String: Localized Device Model **/
#define DEVICE_MODEL_LOCALIZED ( [[UIDevice currentDevice ] localizedModel ] )

/** String: Device Name **/
#define DEVICE_NAME ( [[UIDevice currentDevice ] name ] )

/** Double: Device Orientation **/
#define DEVICE_ORIENTATION ( [[UIDevice currentDevice ] orientation ] )

/** String: Simulator **/
#define DEVICE_SIMULATOR @"Simulator"


//#define ApplicationDelegate                 ((AppDelegate *)[[UIApplication sharedApplication] delegate])
#define UserDefaults                        [NSUserDefaults standardUserDefaults]
#define webServicesShared                   [WebService shared]
//#define NotificationCenter                  [NSNotificationCenter defaultCenter]
//#define SharedApplication                   [UIApplication sharedApplication]
#define Bundle                              [NSBundle mainBundle]
//#define MainScreen                          [UIScreen mainScreen]
//#define ShowNetworkActivityIndicator()      [UIApplication sharedApplication].networkActivityIndicatorVisible = YES
//#define HideNetworkActivityIndicator()      [UIApplication sharedApplication].networkActivityIndicatorVisible = NO
//#define NetworkActivityIndicatorVisible(x)  [UIApplication sharedApplication].networkActivityIndicatorVisible = x
//#define NavBar                              self.navigationController.navigationBar
//#define TabBar                              self.tabBarController.tabBar
//#define NavBarHeight                        self.navigationController.navigationBar.bounds.size.height
//#define TabBarHeight                        self.tabBarController.tabBar.bounds.size.height
//#define ScreenWidth                         [[UIScreen mainScreen] bounds].size.width
//#define ScreenHeight                        [[UIScreen mainScreen] bounds].size.height
//#define TouchHeightDefault                  44
//#define TouchHeightSmall                    32

#define ViewWidth(v)                        v.frame.size.width
#define ViewHeight(v)                       v.frame.size.height
#define ViewX(v)                            v.frame.origin.x
#define ViewY(v)                            v.frame.origin.y
#define SelfViewWidth                       self.view.bounds.size.width
#define SelfViewHeight                      self.view.bounds.size.height
#define RectX(f)                            f.origin.x
#define RectY(f)                            f.origin.y
#define RectWidth(f)                        f.size.width
#define RectHeight(f)                       f.size.height
#define RectSetWidth(f, w)                  CGRectMake(RectX(f), RectY(f), w, RectHeight(f))
#define RectSetHeight(f, h)                 CGRectMake(RectX(f), RectY(f), RectWidth(f), h)
#define RectSetX(f, x)                      CGRectMake(x, RectY(f), RectWidth(f), RectHeight(f))
#define RectSetY(f, y)                      CGRectMake(RectX(f), y, RectWidth(f), RectHeight(f))
#define RectSetSize(f, w, h)                CGRectMake(RectX(f), RectY(f), w, h)
#define RectSetOrigin(f, x, y)              CGRectMake(x, y, RectWidth(f), RectHeight(f))
#define DATE_COMPONENTS                     NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit
#define TIME_COMPONENTS                     NSHourCalendarUnit|NSMinuteCalendarUnit|NSSecondCalendarUnit
#define RGB(r, g, b)                        [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:1.0]
#define RGBA(r, g, b, a)                    [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:a]
#define HEXCOLOR(c)                         [UIColor colorWithRed:((c>>16)&0xFF)/255.0 green:((c>>8)&0xFF)/255.0 blue:(c&0xFF)/255.0 alpha:1.0];
//
//#define DEGREES_TO_RADIANS(angle) (angle / 180.0 * M_PI)


#endif /* Constants_h */
