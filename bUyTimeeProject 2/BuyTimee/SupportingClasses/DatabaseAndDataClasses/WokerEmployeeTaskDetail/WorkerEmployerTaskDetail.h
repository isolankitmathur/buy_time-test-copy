//
//  WorkerEmployerTaskDetail.h
//  BuyTimee
//
//  Created by User14 on 05/04/16.
//  Copyright © 2016 User14. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "task.h"
#import "rating.h"


@interface WorkerEmployerTaskDetail : NSObject


//accepted by worker detail
@property(nonatomic,strong) NSString *userId_acceptedBy;
@property(nonatomic,strong) NSString *userName_acceptedBy;
@property(nonatomic,strong) NSString *picPath_acceptedBy;
@property(nonatomic,strong) NSString *mobile_acceptedBy;
@property(nonatomic,strong) NSString *emailId_acceptedBy;
@property(nonatomic,strong) NSString *description_acceptedBy;
@property(nonatomic,assign) CGFloat latitude_acceptedBy;
@property(nonatomic,assign) CGFloat longitude_acceptedBy;
@property(nonatomic,strong) NSString *locationName_acceptedBy;
@property(nonatomic,strong) NSString *twuserId_acceptedBy;
@property(nonatomic,strong) NSString *socialemailId_acceptedBy;
@property(nonatomic,strong) NSString *socialMobile_acceptedBy;
@property(nonatomic,strong) NSString *fbuserId_acceptedBy;
@property(nonatomic,strong) NSString *lastModifyTime_acceptedBy;
@property(nonatomic,strong) UIImage *userImage_acceptedBy;
@property(nonatomic,strong) NSString *rating_acceptedBy;
@property(nonatomic,strong) NSString *avg_rating_acceptedBy;
@property(nonatomic,assign) CGFloat avgStars_acceptedBy;
@property(nonatomic,strong) NSMutableArray *arrayRatings_acceptedBy;

@property (assign,nonatomic) BOOL isTaskAccepted;

@property(nonatomic,strong) NSString *cardDetail;

//created by worker detail
@property(nonatomic,strong) NSString *userId_createdBy;
@property(nonatomic,strong) NSString *userName_createdBy;
@property(nonatomic,strong) NSString *picPath_createdBy;
@property(nonatomic,strong) NSString *name_createdBy;
@property(nonatomic,strong) NSString *mobile_createdBy;
@property(nonatomic,strong) NSString *emailId_createdBy;
@property(nonatomic,strong) NSString *description_createdBy;
@property(nonatomic,assign) CGFloat latitude_createdBy;
@property(nonatomic,assign) CGFloat longitude_createdBy;
@property(nonatomic,strong) NSString *locationName_createdBy;
@property(nonatomic,strong) NSString *twuserId_createdBy;
@property(nonatomic,strong) NSString *socialemailId_createdBy;
@property(nonatomic,strong) NSString *socialMobile_createdBy;
@property(nonatomic,strong) NSString *fbuserId_createdBy;
@property(nonatomic,strong) NSString *lastModifyTime_createdBy;
@property(nonatomic,strong) UIImage *userImage_createdBy;
@property(nonatomic,strong) NSString *rating_createdBy;
@property(nonatomic,strong) NSString *avg_rating_createdBy;
@property(nonatomic,strong) NSMutableArray *arrayRatings_createdBy;
@property(nonatomic,assign) CGFloat avgStars_createdBy;

// handle business detail for show on resercation detail...

@property(nonatomic,strong) NSString *name_Business;
@property(nonatomic,strong) NSString *address_Business;
@property(nonatomic,strong) NSString *yelpProfile_Business;
@property(nonatomic,strong) NSString *services_Business;
@property(nonatomic,strong) NSString *instaLink_Business;
//@property(nonatomic,strong) NSString *instaLink_Business;

//task detail
@property(nonatomic,strong) task *taskDetailObj;

//rating detail
@property(nonatomic,strong) rating *ratingObj;

@property(nonatomic,assign) BOOL isAlreadyRated;


//Purchase id of discount

@property NSString *purchaseID;




@end
