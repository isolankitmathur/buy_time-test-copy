//
//  rating.h
//  
//
//  Created by  ~ on 17/03/16.
//  Copyright (c) 2016 . All rights reserved.
//

#import <Foundation/Foundation.h>

@interface rating : NSObject

@property(nonatomic,strong)NSString *ratingId;
@property(nonatomic,strong)NSString *workerId;
@property(nonatomic,strong)NSString *ratingStar;
@property(nonatomic,strong)NSString *ratingdescription;
@property(nonatomic,strong)NSString *taskId;
@property(nonatomic,strong)NSString *employerId;
@property(nonatomic,strong)NSString *userName;


@end
