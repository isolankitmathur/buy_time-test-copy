//
//  DatabaseClass.m
//  Patrobuddy
//
//  Created by User14 on 01/04/15.
//
//

#import "DatabaseClass.h"


#import "category.h"
#import "subCategory.h"
#import "task.h"
#import "TermsAndCondition.h"
#import "user.h"
#import "ViewBank.h"
#import "PaymentInfoDataClass.h"
#import "GlobalFunction.h"
#import "accept_task_info.h"
#import "Constants.h"
#import "rating.h"
#import "BusinessInfo.h"

@implementation DatabaseClass


static NSString *databaseName=@"BuyTimeeDB.sqlite";
static sqlite3 *buyTimeDB = nil; // buyTimeDB


+(void) checkDataBase{
    NSFileManager *fileManager=[NSFileManager defaultManager];
    NSError *error;
    NSString *dbPath =[self getDBPath];
    NSLog(@"DB path = %@", dbPath);
    BOOL success=[fileManager fileExistsAtPath:dbPath];
    if(!success){
        
        NSString *defaultDBPath=[[[NSBundle mainBundle]resourcePath]stringByAppendingPathComponent:databaseName];
        success=[fileManager copyItemAtPath:defaultDBPath  toPath:dbPath error:&error];
        if(success){
        }
        else{
            
            NSAssert1(0,@"failed to create database with message '%@'.",[error localizedDescription]);
        }
    }
    else
    {
        
        
    }
    
    if (![UserDefaults boolForKey:keyIsLogin]) {
        
        [self clearDataBase];
        
    }
    
    [self updateDataBase];
    
}

#pragma mark - clear old records
+(void)clearDataBase{
    
    [DatabaseClass deleteRecord:[[NSString stringWithFormat:@"delete from %@",tableAcceptedTask] UTF8String]];
    [DatabaseClass deleteRecord:[[NSString stringWithFormat:@"delete from %@",tableBank] UTF8String]];
    [DatabaseClass deleteRecord:[[NSString stringWithFormat:@"delete from %@",tableCategory] UTF8String]];
    [DatabaseClass deleteRecord:[[NSString stringWithFormat:@"delete from %@",tablePayment] UTF8String]];
    [DatabaseClass deleteRecord:[[NSString stringWithFormat:@"delete from %@",tablePaymentPaid] UTF8String]];
    [DatabaseClass deleteRecord:[[NSString stringWithFormat:@"delete from %@",tableRating] UTF8String]];
    [DatabaseClass deleteRecord:[[NSString stringWithFormat:@"delete from %@",tableSubCategory] UTF8String]];
    [DatabaseClass deleteRecord:[[NSString stringWithFormat:@"delete from %@",tableTaskDetail] UTF8String]];
    [DatabaseClass deleteRecord:[[NSString stringWithFormat:@"delete from %@",tableTnC] UTF8String]];
    [DatabaseClass deleteRecord:[[NSString stringWithFormat:@"delete from %@",tableUser] UTF8String]];
    [DatabaseClass deleteRecord:[[NSString stringWithFormat:@"delete from %@",tableBusinessInfo] UTF8String]];
    
}

+(void)updateDataBase{
    
 //   ALTER TABLE `tableUser` CHANGE `mobile` `manufacturerid` instaLink;
    
//    NSString *main111 = [NSString stringWithFormat:@"select mobile from %@",tableUser];
//    
//    if ([self CheckColumnExists:main111]) {
//        
//        NSString * AlterQuery = [NSString stringWithFormat:@"ALTER TABLE `UserData` CHANGE `mobile` `instaLink`"];
//        [self executeQuery:AlterQuery];
//        
//    }
    
    
    
    NSString *temp1 = [NSString stringWithFormat:@"select lastModifyTime from %@",tableTaskDetail];
    
    if (![self CheckColumnExists:temp1]) {
        
        NSString * AlterQuery = [NSString stringWithFormat:@"ALTER TABLE %@ ADD COLUMN lastModifyTime VARCHAR",tableTaskDetail];
        [self executeQuery:AlterQuery];
        
    }
    
    NSString *tem = [NSString stringWithFormat:@"select businessName from %@",tableTaskDetail];
    
    if (![self CheckColumnExists:tem])
    {
        
        NSString * AlterQuery = [NSString stringWithFormat:@"ALTER TABLE %@ ADD COLUMN businessName VARCHAR",tableTaskDetail];
        [self executeQuery:AlterQuery];
        
    }
    
    NSString *createTblQuery = [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS `taskImages` (`imageId` VARCHAR NOT NULL,`taskId` VARCHAR NOT NULL,`taskImg` VARCHAR NOT NULL)"];
    [self executeQuery:createTblQuery];
    
    
    
    NSString *temp2 = [NSString stringWithFormat:@"select lastModifyTime from %@",tableTnC];
    
    if (![self CheckColumnExists:temp2]) {
        
        NSString * AlterQuery = [NSString stringWithFormat:@"ALTER TABLE %@ ADD COLUMN lastModifyTime VARCHAR",tableTnC];
        [self executeQuery:AlterQuery];
        
    }
    
    NSString *temp3 = [NSString stringWithFormat:@"select lastModifyTime from %@",tableCategory];
    
    if (![self CheckColumnExists:temp3]) {
        
        NSString * AlterQuery = [NSString stringWithFormat:@"ALTER TABLE %@ ADD COLUMN lastModifyTime VARCHAR",tableCategory];
        [self executeQuery:AlterQuery];
        
    }
    
    NSString *temp4 = [NSString stringWithFormat:@"select lastModifyTime from %@",tableSubCategory];
    
    if (![self CheckColumnExists:temp4]) {
        
        NSString * AlterQuery = [NSString stringWithFormat:@"ALTER TABLE %@ ADD COLUMN lastModifyTime VARCHAR",tableSubCategory];
        [self executeQuery:AlterQuery];
        
    }
    
    NSString *temp5 = [NSString stringWithFormat:@"select lastModifyTime from %@",tableAcceptedTask];
    
    if (![self CheckColumnExists:temp5]) {
        
        NSString * AlterQuery = [NSString stringWithFormat:@"ALTER TABLE %@ ADD COLUMN lastModifyTime VARCHAR",tableAcceptedTask];
        [self executeQuery:AlterQuery];
        
    }
    
    NSString *temp6 = [NSString stringWithFormat:@"select userName from %@",tableRating];
    
    if (![self CheckColumnExists:temp6]) {
        
        NSString * AlterQuery = [NSString stringWithFormat:@"ALTER TABLE %@ ADD COLUMN userName VARCHAR",tableRating];
        [self executeQuery:AlterQuery];
        
    }
    
    NSString *temp7 = [NSString stringWithFormat:@"select avg_rating from %@",tableUser];
    
    if (![self CheckColumnExists:temp7]) {
        
        NSString * AlterQuery = [NSString stringWithFormat:@"ALTER TABLE %@ ADD COLUMN avg_rating VARCHAR",tableUser];
        [self executeQuery:AlterQuery];
        
    }
    
    NSString *temp8 = [NSString stringWithFormat:@"select customer_id from %@",tableUser];
    
    if (![self CheckColumnExists:temp8]) {
        
        NSString * AlterQuery = [NSString stringWithFormat:@"ALTER TABLE %@ ADD COLUMN customer_id VARCHAR",tableUser];
        [self executeQuery:AlterQuery];
        
    }
    
    NSString *temp9 = [NSString stringWithFormat:@"select stripe_userid from %@",tableUser];
    
    if (![self CheckColumnExists:temp9]) {
        
        NSString * AlterQuery = [NSString stringWithFormat:@"ALTER TABLE %@ ADD COLUMN stripe_userid VARCHAR",tableUser];
        [self executeQuery:AlterQuery];
        
    }
    NSString *temp10 = [NSString stringWithFormat:@"select businessId from %@",tableUser];
    
    if (![self CheckColumnExists:temp10]) {
        
        NSString * AlterQuery10 = [NSString stringWithFormat:@"ALTER TABLE %@ ADD COLUMN businessId VARCHAR",tableUser];
        [self executeQuery:AlterQuery10];
        
    }
    
    
    //********* by kp
    NSString *temp11 = [NSString stringWithFormat:@"select mobile from %@",tableUser];
    
    if (![self CheckColumnExists:temp11])
    {
        
        NSString * AlterQuery11 = [NSString stringWithFormat:@"ALTER TABLE %@ ADD COLUMN mobile VARCHAR",tableUser];
        [self executeQuery:AlterQuery11];
        
    }
    
    
    
    //********* Rohitash Prajapati ************//
    NSString *temp12 = [NSString stringWithFormat:@"select categoryId from %@",tableBusinessInfo];
    
//    BOOL checkBI = [self CheckColumnExists:temp12];
    if (![self CheckColumnExists:temp12])
    {
        NSString * AlterQuery12 = [NSString stringWithFormat:@"ALTER TABLE %@ ADD COLUMN categoryId VARCHAR NOT NULL DEFAULT '0'",tableBusinessInfo];
        [self executeQuery:AlterQuery12];
        
    }
    
    
    //*** by kp
    
    NSString *temp13 = [NSString stringWithFormat:@"select tnc from %@",tableUser];
    
    if (![self CheckColumnExists:temp13])
    {
        
        NSString * AlterQuery13 = [NSString stringWithFormat:@"ALTER TABLE %@ ADD COLUMN tnc VARCHAR",tableUser];
        [self executeQuery:AlterQuery13];
        
    }

    NSString *temp14 = [NSString stringWithFormat:@"select paymentType from %@",tableUser];
    
    if (![self CheckColumnExists:temp14])
    {
        
        NSString * AlterQuery14 = [NSString stringWithFormat:@"ALTER TABLE %@ ADD COLUMN paymentType VARCHAR",tableUser];
        [self executeQuery:AlterQuery14];
        
    }
    
    NSString *temp15 = [NSString stringWithFormat:@"select account from %@",tableUser];
    
    if (![self CheckColumnExists:temp15])
    {
        
        NSString * AlterQuery15 = [NSString stringWithFormat:@"ALTER TABLE %@ ADD COLUMN account VARCHAR",tableUser];
        [self executeQuery:AlterQuery15];
        
    }
    
    
    

    //    NSString *main111 = [NSString stringWithFormat:@"select mobile from %@",tableUser];
    //
    //    if ([self CheckColumnExists:main111]) {
    //
    //        NSString * AlterQuery = [NSString stringWithFormat:@"ALTER TABLE `UserData` CHANGE `mobile` `instaLink`"];
    //        [self executeQuery:AlterQuery];
    //        
    //    }
    
}


#pragma mark - fill database
+(BOOL)updateDataBaseFromDictionary:(NSDictionary *)dict{
    
    [self clearDataBase];
    
    BOOL updateSuccessfull = false;
    
    NSLog(@"DB path = %@", [self getDBPath]);
    
 if(myAppDelegate.isFromSkip)
 {
    // NSDictionary *dictTemp0=[ParsingClass parseUserDict:dict];
     NSDictionary *dictTemp1=[ParsingClass parseTaskDict:dict];
     NSDictionary *dictTemp2=[ParsingClass parseTermsAndConditionDict:dict];
     NSDictionary *dictTemp3=[ParsingClass parseCategoryDict:dict];
     //NSDictionary *dictTemp4=[ParsingClass parseSubCategoryDict:dict]; // c
     // NSDictionary *dictTemp5=[ParsingClass parseAcceptTaskInfoDict:dict]; //c
   //  NSDictionary *dictTemp6=[ParsingClass parseCardInfoDict:dict]; //c
     // NSDictionary *dictTemp8=[ParsingClass parseRatingInfoDict:dict];//c
     
     if(dictTemp1==nil || dictTemp2==nil || dictTemp3==nil){
         
         updateSuccessfull = false;
         
         UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                         message:@"Server is down."
                                                        delegate:self
                                               cancelButtonTitle:@"OK"
                                               otherButtonTitles:nil];
         [alert show];
         
         
     }
     else{
         
        // NSString *query=[NSString stringWithFormat:@"insert into %@ values(?,?,?,?,?,?,?,?,?,?)",tableUser];
        // [self saveUserData:[query UTF8String] fromDict:dictTemp0];
         
        // query = nil;
       NSString *query=[NSString stringWithFormat:@"insert into %@ values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",tableTaskDetail];
         [self saveTaskData:[query UTF8String] fromDict:dictTemp1];
         
         query = nil;
         query=[NSString stringWithFormat:@"insert into %@ values(?,?)",tableTnC];
         [self saveTnCData:[query UTF8String] fromDict:dictTemp2];
         
         query = nil;
         query=[NSString stringWithFormat:@"insert into %@ values(?,?,?)",tableCategory];
         [self saveCategoryData:[query UTF8String] fromDict:dictTemp3];
         
         query = nil;
         //        query=[NSString stringWithFormat:@"insert into %@ values(?,?,?,?)",tableSubCategory];
         //        [self saveSubCategoryData:[query UTF8String] fromDict:dictTemp4];
         //
         //        query = nil;
         //        query=[NSString stringWithFormat:@"insert into %@ values(?,?,?,?,?,?)",tableAcceptedTask];
         //        [self saveAcceptedTaskDetailWith:[query UTF8String] fromDict:dictTemp5];
         //
         //        query = nil;
         //        query=[NSString stringWithFormat:@"insert into %@ values(?,?,?,?,?,?,?)",tableRating];
         //        [self saveRatingData:[query UTF8String] fromDict:dictTemp8];
         //
         //        query = nil;
         //        query=[NSString stringWithFormat:@"insert into %@ values(?,?,?,?,?,?,?,?)",tablePayment];
         //        [self savePaymentData:[query UTF8String] fromDict:dictTemp6];
         //        
         //        query = nil;
         
         updateSuccessfull = true;
         
     }

 }
else
{
    NSDictionary *dictTemp0=[ParsingClass parseUserDict:dict];
    NSDictionary *dictTemp1=[ParsingClass parseTaskDict:dict];
    NSDictionary *dictTemp2=[ParsingClass parseTermsAndConditionDict:dict];
    NSDictionary *dictTemp3=[ParsingClass parseCategoryDict:dict];
    NSDictionary *dictTemp4=[ParsingClass parseBusinessDict:dict]; // c
   // NSDictionary *dictTemp4=[ParsingClass parseSubCategoryDict:dict]; // c
    // NSDictionary *dictTemp5=[ParsingClass parseAcceptTaskInfoDict:dict]; //c
   NSDictionary *dictTemp6=[ParsingClass parseCardInfoDict:dict]; //c
    // NSDictionary *dictTemp8=[ParsingClass parseRatingInfoDict:dict];//c
   // NSDictionary *dictTemp7=[ParsingClass parseVendorDict:dict];//c
   // NSDictionary *dictTemp8=[ParsingClass parseBankInfoDict:dict];
    if(dictTemp0==nil || dictTemp1==nil || dictTemp2==nil || dictTemp3==nil || dictTemp4==nil || dictTemp6==nil){
        
        updateSuccessfull = false;
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                        message:@"Server is down."
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        
        
    }
    else{
        
        NSString *query=[NSString stringWithFormat:@"insert into %@ values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",tableUser];
        [self saveUserData:[query UTF8String] fromDict:dictTemp0];
        
        query = nil;
        query=[NSString stringWithFormat:@"insert into %@ values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",tableTaskDetail];
        [self saveTaskData:[query UTF8String] fromDict:dictTemp1];
        
        query = nil;
        query=[NSString stringWithFormat:@"insert into %@ values(?,?)",tableTnC];
        [self saveTnCData:[query UTF8String] fromDict:dictTemp2];
        
        query = nil;
        query=[NSString stringWithFormat:@"insert into %@ values(?,?,?)",tableCategory];
        [self saveCategoryData:[query UTF8String] fromDict:dictTemp3];
        
        query = nil;
        
        query=[NSString stringWithFormat:@"insert into %@ values(?,?,?,?)",tableBusinessInfo];
        [self saveBusinessData:[query UTF8String] fromDict:dictTemp4];
        
        query = nil;
        
        
        //        query=[NSString stringWithFormat:@"insert into %@ values(?,?,?,?)",tableSubCategory];
        //        [self saveSubCategoryData:[query UTF8String] fromDict:dictTemp4];
        //
        //        query = nil;
        //        query=[NSString stringWithFormat:@"insert into %@ values(?,?,?,?,?,?)",tableAcceptedTask];
        //        [self saveAcceptedTaskDetailWith:[query UTF8String] fromDict:dictTemp5];
        //
        //        query = nil;
        //        query=[NSString stringWithFormat:@"insert into %@ values(?,?,?,?,?,?,?)",tableRating];
        //        [self saveRatingData:[query UTF8String] fromDict:dictTemp8];
        //
        //        query = nil;
                query=[NSString stringWithFormat:@"insert into %@ values(?,?,?,?,?,?,?,?)",tablePayment];
                [self savePaymentData:[query UTF8String] fromDict:dictTemp6];
        
                query = nil;
        
//        query=[NSString stringWithFormat:@"insert into %@ values(?,?,?,?,?,?,?,?)",tableBankInfo];
//        [self saveBankInfoData:[query UTF8String] fromDict:dictTemp6];
//        
//        query = nil;
//        
//        query=[NSString stringWithFormat:@"insert into %@ values(?,?,?,?,?,?,?,?)",tableVandorProfle];
//        [self savePaymentData:[query UTF8String] fromDict:dictTemp6];
//        
//        query = nil;
        
        
        updateSuccessfull = true;
        
    }

}
    
    return updateSuccessfull;
}


+(BOOL)updateDataBaseFromUpdatedDictionary:(NSDictionary *)dict{
    
    BOOL updateSuccessfull = false;
    
    NSDictionary *dictTemp1=[ParsingClass parseTaskDict:dict];
    NSDictionary *dictTemp2=[ParsingClass parseTermsAndConditionDict:dict];
    NSDictionary *dictTemp3=[ParsingClass parseCategoryDict:dict];
    NSDictionary *dictTemp4=[ParsingClass parseBusinessDict:dict];
    //NSDictionary *dictTemp4=[ParsingClass parseSubCategoryDict:dict];
    //NSDictionary *dictTemp5=[ParsingClass parseAcceptTaskInfoDict:dict];
    
    NSString *query;
    
    if(dictTemp1.count>0){
        
        for (id dicts in dictTemp1) {
            query = nil;
            
            NSLog(@"%@",[EncryptDecryptFile decrypt:[dicts valueForKey:@"reservationId"]]);
            NSLog(@"%@",[EncryptDecryptFile decrypt:[dicts valueForKey:@"title"]]);

            query = [NSString stringWithFormat:@"delete from %@ where taskId = '%@'",tableTaskDetail,[dicts valueForKey:@"reservationId"]];
            [self executeQuery:query];
        }
        
        query = nil;
        query=[NSString stringWithFormat:@"insert into %@ values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",tableTaskDetail];
        [self saveTaskData:[query UTF8String] fromDict:dictTemp1];
        
        myAppDelegate.arrayTaskDetail = [DatabaseClass getDataFromTaskData:[[NSString stringWithFormat:@"Select * from %@",tableTaskDetail] UTF8String]];
        
        [myAppDelegate.arrayTaskDetail sortUsingDescriptors:
         @[
           [NSSortDescriptor sortDescriptorWithKey:@"taskid_Integer" ascending:NO],
           ]];
        
    }
    if(dictTemp2.count>0){
        
        query = nil;
        query = [NSString stringWithFormat:@"delete from %@",tableTnC];
        [self executeQuery:query];
        
        query = nil;
        query=[NSString stringWithFormat:@"insert into %@ values(?,?)",tableTnC];
        [self saveTnCData:[query UTF8String] fromDict:dictTemp2];
        
        
    }
    if(dictTemp3.count>0){
        
        query = nil;
        query = [NSString stringWithFormat:@"delete from %@",tableCategory];
        [self executeQuery:query];
        
        query = nil;
        query=[NSString stringWithFormat:@"insert into %@ values(?,?,?)",tableCategory];
        [self saveCategoryData:[query UTF8String] fromDict:dictTemp3];
        
        
    }
    if(dictTemp4.count>0){
        
        for (id dicts in dictTemp4) {
            query = nil;
           // query = [NSString stringWithFormat:@"delete from %@ where taskId = '%@'",tableTaskDetail,[dicts valueForKey:@"reservationId"]];
            query = [NSString stringWithFormat:@"delete from %@ where businessId = '%@'",tableBusinessInfo,[dicts valueForKey:@"businessId"]];
            [self executeQuery:query];
        }

//        
//        query = nil;
//        query = [NSString stringWithFormat:@"delete from %@",tableBusinessInfo];
//        [self executeQuery:query];
        
        query = nil;
        query=[NSString stringWithFormat:@"insert into %@ values(?,?,?,?)",tableBusinessInfo];
        [self saveBusinessData:[query UTF8String] fromDict:dictTemp4];
        
        
        myAppDelegate.arrayBusinessDetail = [DatabaseClass getDataFromBusinessData:[[NSString stringWithFormat:@"Select * from %@",tableBusinessInfo] UTF8String]];

        [myAppDelegate.arrayBusinessDetail sortUsingDescriptors:
         @[
           [NSSortDescriptor sortDescriptorWithKey:@"businessid_Integer" ascending:NO],
           ]];

        
    }
   //    if(dictTemp4.count>0){
//        
//        query = nil;
//        query = [NSString stringWithFormat:@"delete from %@",tableSubCategory];
//        [self executeQuery:query];
//        
//        query = nil;
//        query=[NSString stringWithFormat:@"insert into %@ values(?,?,?,?)",tableSubCategory];
//        [self saveSubCategoryData:[query UTF8String] fromDict:dictTemp4];
//        
//        NSMutableArray *catArr = [DatabaseClass getDataFromCategoryData:[[NSString stringWithFormat:@"Select * from %@",tableCategory] UTF8String]];
//        
//        NSMutableArray *subCatArr = [DatabaseClass getDataFromSubCategoryData:[[NSString stringWithFormat:@"Select * from %@",tableSubCategory] UTF8String]];
//        
//        myAppDelegate.arrayCategoryDetail = [[NSMutableArray alloc] init];
//        
//        for (category *catObjs in catArr) {
//            
//            if (![myAppDelegate.arrayCategoryDetail containsObject:catObjs]) {
//                [myAppDelegate.arrayCategoryDetail addObject:catObjs];
//            }
//            
//            for (subCategory *objs in subCatArr) {
//                
//                if ([objs.categoryId isEqualToString:catObjs.categoryId]) {
//                    
//                    if (![myAppDelegate.arrayCategoryDetail containsObject:objs]) {
//                        
//                        [myAppDelegate.arrayCategoryDetail addObject:objs];
//                        
//                    }
//                    
//                }
//                
//            }
//            
//        }
//        
//    }
//    if(dictTemp5.count>0){
//        
//        for (id dicts in dictTemp5) {
//            query = nil;
//            query = [NSString stringWithFormat:@"delete from %@ where acceptId = '%@'",tableAcceptedTask,[dicts valueForKey:@"acceptId"]];
//            [self executeQuery:query];
//        }
//        
//        query = nil;
//        query=[NSString stringWithFormat:@"insert into %@ values(?,?,?,?,?,?)",tableAcceptedTask];
//        [self saveAcceptedTaskDetailWith:[query UTF8String] fromDict:dictTemp5];
//        
//        myAppDelegate.arrayAcceptedTaskDetail = nil;
//        myAppDelegate.arrayAcceptedTaskDetail = [DatabaseClass getDataFromAcceptedTaskDetail:[[NSString stringWithFormat:@"Select * from %@",tableAcceptedTask] UTF8String]];
//        
//    }
    query = nil;
    
    updateSuccessfull = true;
    
    return updateSuccessfull;
    
}


#pragma mark - getdbpath
+(NSString *) getDBPath{
    NSArray *paths=NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)	;
    NSString *documentDir = [paths objectAtIndex:0];
    return [documentDir stringByAppendingPathComponent:databaseName];
}

#pragma mark - extras
+ (void) finalizeStatements{
    if(buyTimeDB) sqlite3_close(buyTimeDB);
}

+(void)insertIntoDatabase:(const char *)query tempArray:(NSArray *)tempArray{
    
    sqlite3_stmt *dataRows=nil;
    if(sqlite3_open([[self getDBPath] UTF8String],&buyTimeDB) == SQLITE_OK){
        
        if (sqlite3_prepare_v2(buyTimeDB, query, -1, &dataRows, NULL)!=SQLITE_OK){
            
            NSAssert1(0,@"error while preparing  %s",sqlite3_errmsg(buyTimeDB));
            
        }
        for(int x=0;x<[tempArray count];x++){
            if([tempArray objectAtIndex:x] == [NSNull null])
            {
                NSMutableArray * temp =[NSMutableArray arrayWithArray:tempArray];
                [temp replaceObjectAtIndex:x withObject:@"N.A."];
                tempArray=[NSArray arrayWithArray:temp];
            }
            
            sqlite3_bind_text(dataRows, x+1, [[tempArray objectAtIndex:x] UTF8String],-1,SQLITE_TRANSIENT);
            
        }
        
        if (SQLITE_DONE!=sqlite3_step(dataRows)){
            char *err;
            err=(char *) sqlite3_errmsg(buyTimeDB);
            if (err){                err = nil;
                sqlite3_free(err);
            }
            
            
        }
        sqlite3_finalize(dataRows);
        sqlite3_close(buyTimeDB);
        buyTimeDB=nil;
    }
}

+(void)executeQuery:(NSString *)query{
    //********************************************************************
    //  EXECUTE THE SQL QUERY INSERT/UPDATE
    //********************************************************************
    
    sqlite3_stmt *statement;
    if(sqlite3_open([[self getDBPath] UTF8String],&buyTimeDB) == SQLITE_OK){
        
        
        
        
        if (sqlite3_prepare_v2(buyTimeDB, [query UTF8String], -1, &statement, NULL) == SQLITE_OK){
            if (sqlite3_step(statement) == SQLITE_DONE){
                sqlite3_close(buyTimeDB);
            }
            else{
                NSLog(@"Error %s while preparing statement", sqlite3_errmsg(buyTimeDB));
                NSLog(@"query Statement not  Compiled");
            }
        }
    }
    else{
        NSLog(@"Data not Opened");
    }
}

+(void)updateStatus :(const char *)query{
    
    //sqlite3_stmt *dataRows=nil;
    
    if(sqlite3_open([[self getDBPath] UTF8String], &buyTimeDB) == SQLITE_OK){
        
        //const char *dbpath = [[self getDBPath] UTF8String];
        
        //if (sqlite3_open(dbpath, &contactDB) == SQLITE_OK) {
        char *errMsg;
        //const char *sql_stmt = [query UTF8String];
        
        if (sqlite3_exec(buyTimeDB, query, NULL, NULL, &errMsg) != SQLITE_OK) {
            NSLog(@"Doneeeeee");
        }
        
        NSLog(@"errmsg ==== %s",errMsg);
        
        sqlite3_close(buyTimeDB);
        //}
        //else {}
    }
    else{
        NSLog(@"Data not Opened");
    }
    
}



+(void)deleteRecord:(const char *)query{
    
    sqlite3_stmt *dataRows=nil;
    if(sqlite3_open([[self getDBPath] UTF8String],&buyTimeDB) == SQLITE_OK){
        if (sqlite3_prepare_v2(buyTimeDB, query, -1, &dataRows, NULL)!=SQLITE_OK){
            char *err;
            err=(char *) sqlite3_errmsg(buyTimeDB);
            if (err){
                err = nil;
                sqlite3_free(err);
            }
            
        }
        
        if (SQLITE_DONE!=sqlite3_step(dataRows)){
            char *err;
            err=(char *) sqlite3_errmsg(buyTimeDB);
            if (err){
                err = nil;
                sqlite3_free(err);
            }
            
            //NSAssert1(0,@"error while deleting. %s",sqlite3_errmsg(financeDatabase));
        }
        sqlite3_reset(dataRows);
        sqlite3_close(buyTimeDB);
        buyTimeDB=nil;
    }
}


+ (sqlite3_stmt *) getDataset:(const char *)query{
    
    sqlite3_stmt *dataRows=nil;
    if(sqlite3_open([[self getDBPath] UTF8String],&buyTimeDB) == SQLITE_OK)
    {
        if (sqlite3_prepare_v2(buyTimeDB, query, -1, &dataRows, NULL)!=SQLITE_OK)
        {
            char *err;
            err=(char *) sqlite3_errmsg(buyTimeDB);
            if (err){
                err = nil;
                sqlite3_free(err);
            }
        }
        sqlite3_close(buyTimeDB);
        buyTimeDB=nil;
    }
    return dataRows;
}



+(BOOL)CheckTableExists:(NSString *)query{
    
    sqlite3_stmt *statement;
    bool boo = FALSE;
    if(sqlite3_open([[self getDBPath] UTF8String],&buyTimeDB) == SQLITE_OK)
    {
        
        if (sqlite3_prepare_v2(buyTimeDB, [query UTF8String], -1, &statement, NULL) == SQLITE_OK)
        {
            if (sqlite3_step(statement) == SQLITE_ROW) {
                boo = TRUE;
            }
            sqlite3_finalize(statement);
        }
    }
    else
    {
        NSLog(@"Data not Opened");
    }
    return boo;
}


+(BOOL)CheckColumnExists:(NSString *)query{
    
    sqlite3_stmt *statement;
    BOOL columnExists = NO;
    if(sqlite3_open([[self getDBPath] UTF8String],&buyTimeDB) == SQLITE_OK)
    {
        if (sqlite3_prepare_v2(buyTimeDB, [query UTF8String], -1, &statement, NULL) == SQLITE_OK)
        {
            columnExists = YES;
            //            if (sqlite3_step(statement) == SQLITE_ROW) {
            //                boo = TRUE;
            //            }
            sqlite3_finalize(statement);
        }
    }
    else
    {
        NSLog(@"Data not Opened");
    }
    
    return columnExists;
    
}


+(int)totalRecord{
    
    NSString *databasePath =[self getDBPath];
    int articlesCount = 0;
    if (sqlite3_open([databasePath UTF8String], &buyTimeDB) == SQLITE_OK)
    {
        const char* sqlStatement = "SELECT COUNT(*) FROM qrData";
        sqlite3_stmt* statement = nil;
        if( sqlite3_prepare_v2(buyTimeDB, sqlStatement, -1, &statement, NULL) == SQLITE_OK )
        {
            if( sqlite3_step(statement) == SQLITE_ROW )
                articlesCount = sqlite3_column_int(statement, 0);
        }
        else
        {
            NSLog( @"Failed from sqlite3_prepare_v2. Error is: %s", sqlite3_errmsg(buyTimeDB) );
        }
        // Finalize and close database.
        sqlite3_finalize(statement);
        sqlite3_close(buyTimeDB);
        buyTimeDB=nil;
    }
    return articlesCount;
}


+(int)totalRecord:(const char *)query{
    
    NSString *databasePath =[self getDBPath];
    int articlesCount = 0;
    if (sqlite3_open([databasePath UTF8String], &buyTimeDB) == SQLITE_OK)
    {
        //const char* sqlStatement = "SELECT COUNT(*) FROM gobo_Data";
        sqlite3_stmt* statement = nil;
        
        if( sqlite3_prepare_v2(buyTimeDB, query, -1, &statement, NULL) == SQLITE_OK )
        {
            if( sqlite3_step(statement) == SQLITE_ROW )
                articlesCount = sqlite3_column_int(statement, 0);
        }
        else
        {
            NSLog( @"Failed from sqlite3_prepare_v2. Error is: %s", sqlite3_errmsg(buyTimeDB) );
        }
        
        // Finalize and close database.
        sqlite3_finalize(statement);
        sqlite3_close(buyTimeDB);
        buyTimeDB=nil;
    }
    
    return articlesCount;
    
}


+(void)deleteLastRecord:(const char *)query rowId:(int)tag{
    
    sqlite3_stmt *dataRows=nil;
    if(sqlite3_open([[self getDBPath] UTF8String],&buyTimeDB) == SQLITE_OK)
    {
        if (sqlite3_prepare_v2(buyTimeDB, query, -1, &dataRows, NULL)!=SQLITE_OK)
        {
            NSAssert1(0,@"error while preparing  %s",sqlite3_errmsg(buyTimeDB));
        }
        sqlite3_bind_int(dataRows, 1, tag);
        if (SQLITE_DONE!=sqlite3_step(dataRows))
        {
            char *err;
            err=(char *) sqlite3_errmsg(buyTimeDB);
            if (err){
                err = nil;
                sqlite3_free(err);
            }
            //NSAssert1(0,@"error while deleting. %s",sqlite3_errmsg(financeDatabase));
        }
        sqlite3_reset(dataRows);
        sqlite3_close(buyTimeDB);
        buyTimeDB=nil;
    }
}


+(NSMutableArray *)fetchTableNames{
    
    sqlite3_stmt* statement;
    NSString *query = @"SELECT name FROM sqlite_master WHERE type=\'table\'";
    int retVal = sqlite3_prepare_v2(buyTimeDB,
                                    [query UTF8String],
                                    -1,
                                    &statement,
                                    NULL);
    
    NSMutableArray *selectedRecords = [NSMutableArray array];
    if ( retVal == SQLITE_OK )
    {
        while(sqlite3_step(statement) == SQLITE_ROW )
        {
            NSString *value = [NSString stringWithCString:(const char *)sqlite3_column_text(statement, 0)
                                                 encoding:NSUTF8StringEncoding];
            [selectedRecords addObject:value];
        }
    }
    
    sqlite3_clear_bindings(statement);
    sqlite3_finalize(statement);
    
    return selectedRecords;
}



#pragma mark - accepted task data store

+(void)saveAcceptedTaskDetailWith:(const char *)query fromDict:(NSDictionary *)dict{
    
    sqlite3_stmt *dataRows=nil;
    if(sqlite3_open([[self getDBPath] UTF8String],&buyTimeDB) == SQLITE_OK){
        
        for (id dicts in dict) {
            
            if (sqlite3_prepare_v2(buyTimeDB, query, -1, &dataRows, NULL)!=SQLITE_OK){
                NSAssert1(0,@"error while preparing  %s",sqlite3_errmsg(buyTimeDB));
            }
            sqlite3_bind_text(dataRows, 1, [[dicts valueForKey:@"acceptId"] UTF8String],-1,SQLITE_TRANSIENT);
            sqlite3_bind_text(dataRows, 2, [[dicts valueForKey:@"taskId"] UTF8String],-1,SQLITE_TRANSIENT);
            sqlite3_bind_text(dataRows, 3, [[dicts valueForKey:@"acceptTime"] UTF8String],-1,SQLITE_TRANSIENT);
            sqlite3_bind_text(dataRows, 4, [[dicts valueForKey:@"workerId"] UTF8String],-1,SQLITE_TRANSIENT);
            sqlite3_bind_text(dataRows, 5, [[dicts valueForKey:@"completedTime"] UTF8String],-1,SQLITE_TRANSIENT);
            sqlite3_bind_text(dataRows, 6, [[dicts valueForKey:@"lastModifyTime"] UTF8String],-1,SQLITE_TRANSIENT);
            
            if (SQLITE_DONE!=sqlite3_step(dataRows)){
                char *err;
                err=(char *) sqlite3_errmsg(buyTimeDB);
                if (err){
                    err = nil;
                    sqlite3_free(err);
                }
                
            }
            sqlite3_finalize(dataRows);
            
        }
        
        sqlite3_close(buyTimeDB);
        buyTimeDB=nil;
    }
    
}

+(NSMutableArray *)getDataFromAcceptedTaskDetail:(const char *)query{
    
    NSMutableArray *array = [[NSMutableArray alloc]init];
    
    if(sqlite3_open([[self getDBPath] UTF8String], &buyTimeDB) == SQLITE_OK)
    {
        sqlite3_stmt *statement;
        
        if (sqlite3_prepare_v2(buyTimeDB, query , -1, &statement, nil)==SQLITE_OK)
        {
            while(sqlite3_step(statement)==SQLITE_ROW)
            {
                
                accept_task_info *data=[[accept_task_info alloc]init];
                data.acceptId=[EncryptDecryptFile decrypt:[[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 0)]];
                data.taskId=[EncryptDecryptFile decrypt:[[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 1)]];
                data.acceptTime=[EncryptDecryptFile decrypt:[[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 2)]];
                data.workerId=[EncryptDecryptFile decrypt:[[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 3)]];
                data.completedTime=[EncryptDecryptFile decrypt:[[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 4)]];
                data.lastModifyTime=[EncryptDecryptFile decrypt:[[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 5)]];
                [array addObject:data];
                
            }
        }
        
        sqlite3_close(buyTimeDB);
        buyTimeDB=nil;
        
    }
    NSLog(@"array length in DBCls is =%lu",(unsigned long)[array count]);
    
    return array;
    
}
#pragma mark - Business Info..




+(void)saveBusinessDataFromVendor:(const char *)query fromDict:(NSDictionary *)dicts{
    
    sqlite3_stmt *dataRows=nil;
    if(sqlite3_open([[self getDBPath] UTF8String],&buyTimeDB) == SQLITE_OK){
        
        
        if (sqlite3_prepare_v2(buyTimeDB, query, -1, &dataRows, NULL)!=SQLITE_OK){
            NSAssert1(0,@"error while preparing  %s",sqlite3_errmsg(buyTimeDB));
        }
        sqlite3_bind_text(dataRows, 1, [[dicts valueForKey:@"businessId"] UTF8String],-1,SQLITE_TRANSIENT);
        sqlite3_bind_text(dataRows, 2, [[dicts valueForKey:@"name"] UTF8String],-1,SQLITE_TRANSIENT);
        sqlite3_bind_text(dataRows, 3, [[dicts valueForKey:@"address"] UTF8String],-1,SQLITE_TRANSIENT);
        
        if (SQLITE_DONE!=sqlite3_step(dataRows)){
            char *err;
            err=(char *) sqlite3_errmsg(buyTimeDB);
            if (err){
                err = nil;
                sqlite3_free(err);
            }
            
        }
        sqlite3_finalize(dataRows);
        sqlite3_close(buyTimeDB);
        buyTimeDB=nil;
    }
    
    
    
}




+(void)saveBusinessData:(const char *)query fromDict:(NSDictionary *)dict{
    
    sqlite3_stmt *dataRows=nil;
    if(sqlite3_open([[self getDBPath] UTF8String],&buyTimeDB) == SQLITE_OK){
        
        for (id dicts in dict) {
            
            if (sqlite3_prepare_v2(buyTimeDB, query, -1, &dataRows, NULL)!=SQLITE_OK){
                NSAssert1(0,@"error while preparing  %s",sqlite3_errmsg(buyTimeDB));
            }
            
            
            sqlite3_bind_text(dataRows, 1, [[dicts valueForKey:@"businessId"] UTF8String],-1,SQLITE_TRANSIENT);
            sqlite3_bind_text(dataRows, 2, [[dicts valueForKey:@"name"] UTF8String],-1,SQLITE_TRANSIENT);
            sqlite3_bind_text(dataRows, 3, [[dicts valueForKey:@"address"] UTF8String],-1,SQLITE_TRANSIENT);
            sqlite3_bind_text(dataRows, 4, [[dicts valueForKey:@"categoryId"] UTF8String],-1,SQLITE_TRANSIENT);
            
            if (SQLITE_DONE!=sqlite3_step(dataRows)){
                char *err;
                err=(char *) sqlite3_errmsg(buyTimeDB);
                if (err){
                    err = nil;
                    sqlite3_free(err);
                }
                
            }
            sqlite3_finalize(dataRows);
            
        }
        
        sqlite3_close(buyTimeDB);
        buyTimeDB=nil;
    }
    
    

}

+(NSMutableArray *)getDataFromBusinessData:(const char *)query{
    
    NSMutableArray *array = [[NSMutableArray alloc]init];
    
    if(sqlite3_open([[self getDBPath] UTF8String], &buyTimeDB) == SQLITE_OK)
    {
        sqlite3_stmt *statement;
        
        if (sqlite3_prepare_v2(buyTimeDB, query , -1, &statement, nil)==SQLITE_OK)
        {
            while(sqlite3_step(statement)==SQLITE_ROW)
            {
                
                BusinessInfo *data=[[BusinessInfo alloc]init];
                data.BusinessId=[[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 0)];
                data.businessid_Integer = [data.BusinessId integerValue];
                data.BusinessName=[[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 1)];
                data.BusinessAdd=[[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 2)];
                data.categoryId=[[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 3)];
                
                [array addObject:data];
            }
        }
        sqlite3_close(buyTimeDB);
        buyTimeDB=nil;
    }
    NSLog(@"array length in DBCls is =%lu",(unsigned long)[array count]);
    return array;
}



#pragma mark - t n c data
+(void)saveTnCData:(const char *)query fromDict:(NSDictionary *)dict{
    
    sqlite3_stmt *dataRows=nil;
    if(sqlite3_open([[self getDBPath] UTF8String],&buyTimeDB) == SQLITE_OK){
        if (sqlite3_prepare_v2(buyTimeDB, query, -1, &dataRows, NULL)!=SQLITE_OK){
            NSAssert1(0,@"error while preparing  %s",sqlite3_errmsg(buyTimeDB));
        }
//        sqlite3_bind_text(dataRows, 1, [[[dict valueForKey:@"terms_and_condition"] firstObject] UTF8String],-1,SQLITE_TRANSIENT);
//        sqlite3_bind_text(dataRows, 2, [[[dict valueForKey:@"lastModifyTime"] firstObject] UTF8String],-1,SQLITE_TRANSIENT);

        
        NSLog(@"%@", [dict valueForKey:@"description"]);
        NSLog(@"%@", [dict valueForKey:@"lastModifyTime"]);
        
        sqlite3_bind_text(dataRows, 1, [[dict valueForKey:@"description"]  UTF8String],-1,SQLITE_TRANSIENT);
        sqlite3_bind_text(dataRows, 2, [[dict valueForKey:@"lastModifyTime"] UTF8String],-1,SQLITE_TRANSIENT);

        
        if (SQLITE_DONE!=sqlite3_step(dataRows)){
            char *err;
            err=(char *) sqlite3_errmsg(buyTimeDB);
            if (err){
                err = nil;
                sqlite3_free(err);
            }
            
        }
        sqlite3_finalize(dataRows);
        sqlite3_close(buyTimeDB);
        buyTimeDB=nil;
    }
    
}

+(NSMutableArray *)getDataFromTnCData:(const char *)query{
    
    NSMutableArray *array = [[NSMutableArray alloc]init];
    
    if(sqlite3_open([[self getDBPath] UTF8String], &buyTimeDB) == SQLITE_OK)
    {
        sqlite3_stmt *statement;
        
        if (sqlite3_prepare_v2(buyTimeDB, query , -1, &statement, nil)==SQLITE_OK)
        {
            while(sqlite3_step(statement)==SQLITE_ROW)
            {
                
                TermsAndCondition *data=[[TermsAndCondition alloc]init];
                data.termsAndCondition=[[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 0)];
                
                [array addObject:data];
                
            }
        }
        
        sqlite3_close(buyTimeDB);
        buyTimeDB=nil;
        
    }
    NSLog(@"array length in DBCls is =%lu",(unsigned long)[array count]);
    
    return array;
    
}



#pragma mark - category data
+(void)saveCategoryData:(const char *)query fromDict:(NSDictionary *)dict{
    
    
    sqlite3_stmt *dataRows=nil;
    if(sqlite3_open([[self getDBPath] UTF8String],&buyTimeDB) == SQLITE_OK){
        
        for (id dicts in dict) {
            
            if (sqlite3_prepare_v2(buyTimeDB, query, -1, &dataRows, NULL)!=SQLITE_OK){
                NSAssert1(0,@"error while preparing  %s",sqlite3_errmsg(buyTimeDB));
            }
            
            
            sqlite3_bind_text(dataRows, 1, [[dicts valueForKey:@"categoryId"] UTF8String],-1,SQLITE_TRANSIENT);
            sqlite3_bind_text(dataRows, 2, [[dicts valueForKey:@"name"] UTF8String],-1,SQLITE_TRANSIENT);
            sqlite3_bind_text(dataRows, 3, [[dicts valueForKey:@"categoryImage"] UTF8String],-1,SQLITE_TRANSIENT);
            
            if (SQLITE_DONE!=sqlite3_step(dataRows)){
                char *err;
                err=(char *) sqlite3_errmsg(buyTimeDB);
                if (err){
                    err = nil;
                    sqlite3_free(err);
                }
                
            }
            sqlite3_finalize(dataRows);
            
       }
        
        sqlite3_close(buyTimeDB);
        buyTimeDB=nil;
    }
    
    
}

+(NSMutableArray *)getDataFromCategoryData:(const char *)query{
    
    NSMutableArray *array = [[NSMutableArray alloc]init];
    
    if(sqlite3_open([[self getDBPath] UTF8String], &buyTimeDB) == SQLITE_OK){
        sqlite3_stmt *statement;
        
        if (sqlite3_prepare_v2(buyTimeDB, query , -1, &statement, nil)==SQLITE_OK){
            while(sqlite3_step(statement)==SQLITE_ROW){
                
                category *data=[[category alloc]init];
                data.categoryId=[EncryptDecryptFile decrypt:[[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 0)]];
                data.name=[EncryptDecryptFile decrypt:[[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 1)]];
                data.categoryImg=[EncryptDecryptFile decrypt:[[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 1)]];
                [array addObject:data];
                
            }
        }
        
        sqlite3_close(buyTimeDB);
        buyTimeDB=nil;
        
    }
    NSLog(@"array length in DBCls is =%lu",(unsigned long)[array count]);
    
    
    return array;
    
}


#pragma mark - rating data
+(void)saveRatingData:(const char *)query fromDict:(NSDictionary *)dict{
    
    sqlite3_stmt *dataRows=nil;
    if(sqlite3_open([[self getDBPath] UTF8String],&buyTimeDB) == SQLITE_OK){
        
        for (id dicts in dict) {
            
            if (sqlite3_prepare_v2(buyTimeDB, query, -1, &dataRows, NULL)!=SQLITE_OK){
                NSAssert1(0,@"error while preparing  %s",sqlite3_errmsg(buyTimeDB));
            }
            sqlite3_bind_text(dataRows, 1, [[dicts valueForKey:@"ratingId"] UTF8String],-1,SQLITE_TRANSIENT);
            sqlite3_bind_text(dataRows, 2, [[dicts valueForKey:@"workerId"] UTF8String],-1,SQLITE_TRANSIENT);
            sqlite3_bind_text(dataRows, 3, [[dicts valueForKey:@"ratingStar"] UTF8String],-1,SQLITE_TRANSIENT);
            sqlite3_bind_text(dataRows, 4, [[dicts valueForKey:@"description"] UTF8String],-1,SQLITE_TRANSIENT);
            sqlite3_bind_text(dataRows, 5, [[dicts valueForKey:@"taskId"] UTF8String],-1,SQLITE_TRANSIENT);
            sqlite3_bind_text(dataRows, 6, [[dicts valueForKey:@"employerId"] UTF8String],-1,SQLITE_TRANSIENT);
            sqlite3_bind_text(dataRows, 7, [[dicts valueForKey:@"userName"] UTF8String],-1,SQLITE_TRANSIENT);
            
            if (SQLITE_DONE!=sqlite3_step(dataRows)){
                char *err;
                err=(char *) sqlite3_errmsg(buyTimeDB);
                if (err){
                    err = nil;
                    sqlite3_free(err);
                }
                
            }
            sqlite3_finalize(dataRows);
            
        }
        
        sqlite3_close(buyTimeDB);
        buyTimeDB=nil;
    }
    
}

+(NSMutableArray *)getDataFromRatingTable:(const char *)query{
    
    NSMutableArray *array = [[NSMutableArray alloc]init];
    
    if(sqlite3_open([[self getDBPath] UTF8String], &buyTimeDB) == SQLITE_OK){
        sqlite3_stmt *statement;
        
        if (sqlite3_prepare_v2(buyTimeDB, query , -1, &statement, nil)==SQLITE_OK){
            while(sqlite3_step(statement)==SQLITE_ROW){
                
                rating *data=[[rating alloc]init];
                data.ratingId=[EncryptDecryptFile decrypt:[[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 0)]];
                data.workerId=[EncryptDecryptFile decrypt:[[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 1)]];
                data.ratingStar=[EncryptDecryptFile decrypt:[[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 2)]];
                data.ratingdescription=[EncryptDecryptFile decrypt:[[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 3)]];
                data.taskId=[EncryptDecryptFile decrypt:[[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 4)]];
                data.employerId=[EncryptDecryptFile decrypt:[[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 5)]];
                data.userName=[EncryptDecryptFile decrypt:[[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 6)]];
                
                [array addObject:data];
                
            }
        }
        
        sqlite3_close(buyTimeDB);
        buyTimeDB=nil;
        
    }
    NSLog(@"array length in DBCls is =%lu",(unsigned long)[array count]);
    
    
    return array;
    
}


#pragma mark - sub category data
+(void)saveSubCategoryData:(const char *)query fromDict:(NSDictionary *)dict{
    
    sqlite3_stmt *dataRows=nil;
    if(sqlite3_open([[self getDBPath] UTF8String],&buyTimeDB) == SQLITE_OK){
        
        for (id dicts in dict) {
            
            if (sqlite3_prepare_v2(buyTimeDB, query, -1, &dataRows, NULL)!=SQLITE_OK){
                NSAssert1(0,@"error while preparing  %s",sqlite3_errmsg(buyTimeDB));
            }
            sqlite3_bind_text(dataRows, 1, [[dicts valueForKey:@"subCategoryId"] UTF8String],-1,SQLITE_TRANSIENT);
            sqlite3_bind_text(dataRows, 2, [[dicts valueForKey:@"categoryId"] UTF8String],-1,SQLITE_TRANSIENT);
            sqlite3_bind_text(dataRows, 3, [[dicts valueForKey:@"name"] UTF8String],-1,SQLITE_TRANSIENT);
            sqlite3_bind_text(dataRows, 4, [[dicts valueForKey:@"lastModifyTime"] UTF8String],-1,SQLITE_TRANSIENT);
            
            if (SQLITE_DONE!=sqlite3_step(dataRows)){
                char *err;
                err=(char *) sqlite3_errmsg(buyTimeDB);
                if (err){
                    err = nil;
                    sqlite3_free(err);
                }
                
            }
            sqlite3_finalize(dataRows);
            
        }
        
        sqlite3_close(buyTimeDB);
        buyTimeDB=nil;
    }
    
}


+(NSMutableArray *)getDataFromSubCategoryData:(const char *)query{
    
    NSMutableArray *array = [[NSMutableArray alloc]init];
    
    if(sqlite3_open([[self getDBPath] UTF8String], &buyTimeDB) == SQLITE_OK){
        sqlite3_stmt *statement;
        
        if (sqlite3_prepare_v2(buyTimeDB, query , -1, &statement, nil)==SQLITE_OK){
            while(sqlite3_step(statement)==SQLITE_ROW){
                
                subCategory *data=[[subCategory alloc]init];
                data.subCategoryId=[EncryptDecryptFile decrypt:[[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 0)]];
                data.categoryId=[EncryptDecryptFile decrypt:[[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 1)]];
                data.name=[EncryptDecryptFile decrypt:[[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 2)]];
                
                [array addObject:data];
                
            }
        }
        
        sqlite3_close(buyTimeDB);
        buyTimeDB=nil;
        
    }
    NSLog(@"array length in DBCls is =%lu",(unsigned long)[array count]);
    
    
    return array;
    
}


#pragma mark - task data
+(void)saveTaskData:(const char *)query fromDict:(NSDictionary *)dict{
    
    sqlite3_stmt *dataRows=nil;
    if(sqlite3_open([[self getDBPath] UTF8String],&buyTimeDB) == SQLITE_OK){
        
        for (id dicts in dict) {
            
            if (sqlite3_prepare_v2(buyTimeDB, query, -1, &dataRows, NULL)!=SQLITE_OK){
                NSAssert1(0,@"error while preparing  %s",sqlite3_errmsg(buyTimeDB));
            }
            
            sqlite3_bind_text(dataRows, 1, [[dicts valueForKey:@"reservationId"] UTF8String],-1,SQLITE_TRANSIENT);
            sqlite3_bind_text(dataRows, 2, [[dicts valueForKey:@"userId"] UTF8String],-1,SQLITE_TRANSIENT);
            sqlite3_bind_text(dataRows, 3, [[dicts valueForKey:@"businessId"] UTF8String],-1,SQLITE_TRANSIENT); // subcategoryId changed with businessId bcz no need of subcategory id here...
            sqlite3_bind_text(dataRows, 4, [[dicts valueForKey:@"title"] UTF8String],-1,SQLITE_TRANSIENT);
            sqlite3_bind_text(dataRows, 5, [[dicts valueForKey:@"description"] UTF8String],-1,SQLITE_TRANSIENT);
            sqlite3_bind_text(dataRows, 6, [[dicts valueForKey:@"price"] UTF8String],-1,SQLITE_TRANSIENT);//price
            sqlite3_bind_text(dataRows, 7, [[dicts valueForKey:@"startDate"] UTF8String],-1,SQLITE_TRANSIENT);
            sqlite3_bind_text(dataRows, 8, [[dicts valueForKey:@"startTime"] UTF8String],-1,SQLITE_TRANSIENT); // change with this endDate
            sqlite3_bind_text(dataRows, 9, [[dicts valueForKey:@"latitude"] UTF8String],-1,SQLITE_TRANSIENT);
            sqlite3_bind_text(dataRows, 10, [[dicts valueForKey:@"longitude"] UTF8String],-1,SQLITE_TRANSIENT);
            sqlite3_bind_text(dataRows, 11, [[dicts valueForKey:@"locationName"] UTF8String],-1,SQLITE_TRANSIENT);
            sqlite3_bind_text(dataRows, 12, [[dicts valueForKey:@"status"] UTF8String],-1,SQLITE_TRANSIENT);
            sqlite3_bind_text(dataRows, 13, [[dicts valueForKey:@"picPath"] UTF8String],-1,SQLITE_TRANSIENT);
            sqlite3_bind_text(dataRows, 14, [[dicts valueForKey:@"lastModifyTime"] UTF8String],-1,SQLITE_TRANSIENT);
            sqlite3_bind_text(dataRows, 15, [[dicts valueForKey:@"categoryId"] UTF8String],-1,SQLITE_TRANSIENT);
            sqlite3_bind_text(dataRows, 16, [[dicts valueForKey:@"paymentMethod"] UTF8String],-1,SQLITE_TRANSIENT);
             sqlite3_bind_text(dataRows, 17, [[dicts valueForKey:@"discount"] UTF8String],-1,SQLITE_TRANSIENT);
            sqlite3_bind_text(dataRows, 18, [[dicts valueForKey:@"fee"] UTF8String],-1,SQLITE_TRANSIENT);
            sqlite3_bind_text(dataRows, 19, [[dicts valueForKey:@"businessName"] UTF8String],-1,SQLITE_TRANSIENT);
            
            if (SQLITE_DONE!=sqlite3_step(dataRows)){
                char *err;
                err=(char *) sqlite3_errmsg(buyTimeDB);
                if (err){
                     NSLog(@"error is...%s",err);
                    err = nil;
                    sqlite3_free(err);
                }
                
            }
            sqlite3_finalize(dataRows);
        }
        
        sqlite3_close(buyTimeDB);
        buyTimeDB=nil;
    }
    
}


+(NSMutableArray *)getDataFromTaskData:(const char *)query{
    
    NSMutableArray *array = [[NSMutableArray alloc]init];
    
    if(sqlite3_open([[self getDBPath] UTF8String], &buyTimeDB) == SQLITE_OK)
    {
        sqlite3_stmt *statement;
        
        if (sqlite3_prepare_v2(buyTimeDB, query , -1, &statement, nil)==SQLITE_OK)
        {
            while(sqlite3_step(statement)==SQLITE_ROW)
            {
                
                task *data=[[task alloc]init];
                data.taskid= [EncryptDecryptFile decrypt:[[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 0)]];
                data.taskid_Integer = [data.taskid integerValue];
                data.userid=[EncryptDecryptFile decrypt:[[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 1)]];
                data.businessIdFromTask=[EncryptDecryptFile decrypt:[[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 2)]];
                data.title=[EncryptDecryptFile decrypt:[[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 3)]];
                data.taskDescription=[EncryptDecryptFile decrypt:[[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 4)]];
                data.price=[[EncryptDecryptFile decrypt:[[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 5)]] floatValue];
                data.startDate=[EncryptDecryptFile decrypt:[[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 6)]];
                data.endDate=[EncryptDecryptFile decrypt:[[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 7)]];
                data.latitude=[[EncryptDecryptFile decrypt:[[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 8)]] floatValue];
                data.longitude=[[EncryptDecryptFile decrypt:[[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 9)]] floatValue];
                data.locationName=[EncryptDecryptFile decrypt:[[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 10)]];
                data.status=[EncryptDecryptFile decrypt:[[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 11)]];
                data.picpath=[EncryptDecryptFile decrypt:[[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 12)]];
                data.lastModifyTime=[EncryptDecryptFile decrypt:[[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 13)]];
                data.categoryId=[EncryptDecryptFile decrypt:[[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 14)]];
                data.paymentMethod=[EncryptDecryptFile decrypt:[[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 15)]];
                data.discountValue=[EncryptDecryptFile decrypt:[[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 16)]];
                
                
                
           //      NSLog(@"user name in getdata from task data%@",[EncryptDecryptFile decrypt:[[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 17)]]);
                 data.feeValue=[EncryptDecryptFile decrypt:[[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 17)]];
                data.businessName=[EncryptDecryptFile decrypt:[[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 18)]];
                
                NSArray *components = [data.picpath componentsSeparatedByString:@"/"];
                
                data.userImageName = [components lastObject];
                NSLog(@"user name in getdata from task data%@",data.userImageName);
                
                if ([data.userid isEqualToString:myAppDelegate.userData.userid]) {
                    data.userImage = myAppDelegate.userData.userImage;
                    NSLog(@"user id in getdata from task data%@",data.userImage);
                }else{
                    
                  //  NSLog(@"user image in getdata from task data%@",data.userImage);
                    
                    if ([data.picpath isEqualToString:@""]) {
                        
                        data.userImage = imageUserDefault;
                        
                    }else{
                        NSLog(@"user image name in getdata from task data%@",data.userImageName);
                        
                        NSString *imgNameWithJPEG =  data.userImageName;
                        
                        UIImage *img = [GlobalFunction loadImageOfLogoFromCacheFolder:imgNameWithJPEG dirname:@"user"];
                        
                        if (img) {
                            
                            data.userImage = img;
                            
                        }else{
                            
                            data.userImage = nil;
                            
                        }
                        
                    }
                    
                }
                
                [array addObject:data];
                
            }
        }
        
        sqlite3_close(buyTimeDB);
        buyTimeDB=nil;
        
    }
    NSLog(@"array length in DBCls is =%lu",(unsigned long)[array count]);
    
    
    return array;
    
}


#pragma mark - bank detail
+(void)saveBankData:(const char *)query fromDict:(NSDictionary *)dict{
    
    sqlite3_stmt *dataRows=nil;
    if(sqlite3_open([[self getDBPath] UTF8String],&buyTimeDB) == SQLITE_OK){
        
        if (sqlite3_prepare_v2(buyTimeDB, query, -1, &dataRows, NULL)!=SQLITE_OK){
            NSAssert1(0,@"error while preparing  %s",sqlite3_errmsg(buyTimeDB));
        }
        sqlite3_bind_text(dataRows, 1, [[[dict valueForKey:@"bankId"] firstObject] UTF8String],-1,SQLITE_TRANSIENT);
        sqlite3_bind_text(dataRows, 2, [[[dict valueForKey:@"userId"] firstObject] UTF8String],-1,SQLITE_TRANSIENT);
        sqlite3_bind_text(dataRows, 3, [[[dict valueForKey:@"name"] firstObject] UTF8String],-1,SQLITE_TRANSIENT);
        sqlite3_bind_text(dataRows, 4, [[[dict valueForKey:@"accountNo"] firstObject] UTF8String],-1,SQLITE_TRANSIENT);
        sqlite3_bind_text(dataRows, 5, [[[dict valueForKey:@"routingNo"] firstObject] UTF8String],-1,SQLITE_TRANSIENT);
        sqlite3_bind_text(dataRows, 6, [[[dict valueForKey:@"bankName"] firstObject] UTF8String],-1,SQLITE_TRANSIENT);
        sqlite3_bind_text(dataRows, 7, [[[dict valueForKey:@"accType"] firstObject] UTF8String],-1,SQLITE_TRANSIENT);
        
        
        if (SQLITE_DONE!=sqlite3_step(dataRows)){
            char *err;
            err=(char *) sqlite3_errmsg(buyTimeDB);
            if (err){
                err = nil;
                sqlite3_free(err);
            }
            
        }
        sqlite3_finalize(dataRows);
        sqlite3_close(buyTimeDB);
        buyTimeDB=nil;
    }
    
}


+(NSMutableArray *)getDataFromUserBankData:(const char *)query{
    
    NSMutableArray *array = [[NSMutableArray alloc]init];
    
    if(sqlite3_open([[self getDBPath] UTF8String], &buyTimeDB) == SQLITE_OK)
    {
        sqlite3_stmt *statement;
        
        if (sqlite3_prepare_v2(buyTimeDB, query , -1, &statement, nil)==SQLITE_OK)
        {
            while(sqlite3_step(statement)==SQLITE_ROW)
            {
                
                ViewBank *data=[[ViewBank alloc]init];
                data.bankId=[EncryptDecryptFile decrypt:[[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 0)]];
                data.userId=[EncryptDecryptFile decrypt:[[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 1)]];
                data.name=[EncryptDecryptFile decrypt:[[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 2)]];
                data.accountNo=[EncryptDecryptFile decrypt:[[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 3)]];
                data.routingNo=[EncryptDecryptFile decrypt:[[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 4)]];
                data.bankName=[EncryptDecryptFile decrypt:[[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 5)]];
                data.accType=[EncryptDecryptFile decrypt:[[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 6)]];
                
                [array addObject:data];
                
            }
        }
        
        sqlite3_close(buyTimeDB);
        buyTimeDB=nil;
        
    }
    NSLog(@"array length in DBCls is =%lu",(unsigned long)[array count]);
    
    
    return array;
    
}


#pragma mark - user details
+(void)saveUserData:(const char *)query fromDict:(NSDictionary *)dict{
    
    sqlite3_stmt *dataRows=nil;
    if(sqlite3_open([[self getDBPath] UTF8String],&buyTimeDB) == SQLITE_OK){
        if (sqlite3_prepare_v2(buyTimeDB, query, -1, &dataRows, NULL)!=SQLITE_OK){
            NSAssert1(0,@"error while preparing  %s",sqlite3_errmsg(buyTimeDB));
        }
        
        sqlite3_bind_text(dataRows, 1, [[dict valueForKey:@"userId"]  UTF8String],-1,SQLITE_TRANSIENT);
        sqlite3_bind_text(dataRows, 2, [[dict valueForKey:@"name"]  UTF8String],-1,SQLITE_TRANSIENT);
        sqlite3_bind_text(dataRows, 3, [[dict valueForKey:@"emailId"]  UTF8String],-1,SQLITE_TRANSIENT);
        sqlite3_bind_text(dataRows, 4, [[dict valueForKey:@"instaLink"]  UTF8String],-1,SQLITE_TRANSIENT);
        sqlite3_bind_text(dataRows, 5, [[dict valueForKey:@"location"]  UTF8String],-1,SQLITE_TRANSIENT);
        sqlite3_bind_text(dataRows, 6, [[dict valueForKey:@"description"]  UTF8String],-1,SQLITE_TRANSIENT);
        sqlite3_bind_text(dataRows, 7, [[dict valueForKey:@"picPath"] UTF8String],-1,SQLITE_TRANSIENT);    //temporary
        sqlite3_bind_text(dataRows, 8, [[dict valueForKey:@"avgRating"] UTF8String],-1,SQLITE_TRANSIENT);    //temporary
        
        NSString *cusId = [EncryptDecryptFile decrypt:[dict valueForKey:@"customerId"]];
        if ([cusId isEqualToString:@""]) {
            sqlite3_bind_text(dataRows, 9, [[EncryptDecryptFile EncryptIT:@""] UTF8String],-1,SQLITE_TRANSIENT);    //temporary
        }else{
            sqlite3_bind_text(dataRows, 9, [[EncryptDecryptFile EncryptIT:@"1"] UTF8String],-1,SQLITE_TRANSIENT);    //temporary
        }
        
        NSString *stripeId = [EncryptDecryptFile decrypt:[dict valueForKey:@"stripeId"]];
        if ([stripeId isEqualToString:@""]) {
            sqlite3_bind_text(dataRows, 10, [[EncryptDecryptFile EncryptIT:@""] UTF8String],-1,SQLITE_TRANSIENT);    //temporary
        }else{
            sqlite3_bind_text(dataRows, 10, [[EncryptDecryptFile EncryptIT:@"1"] UTF8String],-1,SQLITE_TRANSIENT);    //temporary
        }
        
        sqlite3_bind_text(dataRows, 11, [[dict valueForKey:@"businessId"] UTF8String],-1,SQLITE_TRANSIENT);
        
        sqlite3_bind_text(dataRows, 12, [[dict valueForKey:@"mobile"] UTF8String],-1,SQLITE_TRANSIENT);
        sqlite3_bind_text(dataRows, 13, [[dict valueForKey:@"tnc"] UTF8String],-1,SQLITE_TRANSIENT);
        
          NSString *fdsdf = [EncryptDecryptFile decrypt:[dict valueForKey:@"paymentType"]];
        sqlite3_bind_text(dataRows, 14, [[dict valueForKey:@"paymentType"] UTF8String],-1,SQLITE_TRANSIENT);
        
        NSString *fdwwsdf = [EncryptDecryptFile decrypt:[dict valueForKey:@"account"]];
        sqlite3_bind_text(dataRows, 15, [[dict valueForKey:@"account"] UTF8String],-1,SQLITE_TRANSIENT);

        
        if (SQLITE_DONE!=sqlite3_step(dataRows)){
            char *err;
            err=(char *) sqlite3_errmsg(buyTimeDB);
            if (err){
                NSLog(@"error is...%s",err);
                err = nil;
                sqlite3_free(err);
            }
            
        }
        sqlite3_finalize(dataRows);
        sqlite3_close(buyTimeDB);
        buyTimeDB=nil;
    }
    
}

+(void)updateStripeIdInDataBase:(NSString *)stripeId forUserId:(NSString *)userId{
    
    NSString *query = [NSString stringWithFormat:@"update %@ Set stripe_userid = '%@' Where userId = '%@'",tableUser,stripeId,userId];
    
    [self executeQuery:query];
    
}

+(void)updateBusinessIdInDataBase:(NSString *)bsuinessId forUserId:(NSString *)userId{
  
   // [EncryptDecryptFile EncryptIT:myAppDelegate.userData.BusinessUserId]
    NSLog(@"bus is ... %@",[EncryptDecryptFile decrypt:bsuinessId]);
    NSLog(@"bus is ... %@",[EncryptDecryptFile decrypt:userId]);
    
    
    NSString *query = [NSString stringWithFormat:@"update %@ Set bsuinessId = '%@' Where userId = '%@'",tableUser,bsuinessId,userId];
  
    [self executeQuery:query];
  
}


+(void)updateCustomerIdInDataBase:(NSString *)customerId forUserId:(NSString *)userId{
    
    
    NSLog(@"bus is ... %@",[EncryptDecryptFile decrypt:customerId]);
    NSLog(@"bus is ... %@",[EncryptDecryptFile decrypt:userId]);
    
    NSString *query = [NSString stringWithFormat:@"update %@ Set customer_id = '%@' Where userId = '%@'",tableUser,customerId,userId];

    [self executeQuery:query];

}

//+(void)updatePaymentMethodInDataBase:(NSString *)customerId forUserId:(NSString *)userId{
//
//
//    NSLog(@"bus is ... %@",[EncryptDecryptFile decrypt:customerId]);
//    NSLog(@"bus is ... %@",[EncryptDecryptFile decrypt:userId]);
//
//    NSString *query = [NSString stringWithFormat:@"update %@ Set customer_id = '%@' Where userId = '%@'",tableUser,customerId,userId];
//
//    [self executeQuery:query];
//
//}

+(NSMutableArray *)getDataFromUserData:(const char *)query{
    
    NSMutableArray *array = [[NSMutableArray alloc]init];
    
    if(sqlite3_open([[self getDBPath] UTF8String], &buyTimeDB) == SQLITE_OK){
        
        sqlite3_stmt *statement;
        
        if (sqlite3_prepare_v2(buyTimeDB, query , -1, &statement, nil)==SQLITE_OK){
            
            while(sqlite3_step(statement)==SQLITE_ROW){
                
                user *data=[[user alloc]init];
                data.userid=[EncryptDecryptFile decrypt:[[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 0)]];
                data.username=[EncryptDecryptFile decrypt:[[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 1)]];
                data.emailId=[EncryptDecryptFile decrypt:[[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 2)]];
                data.instaLink=[EncryptDecryptFile decrypt:[[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 3)]];
                data.location=[EncryptDecryptFile decrypt:[[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 4)]];
                data.userDescription=[EncryptDecryptFile decrypt:[[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 5)]];
                data.picpath=[EncryptDecryptFile decrypt:[[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 6)]];
                data.avg_rating=[EncryptDecryptFile decrypt:[[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 7)]];
                data.customer_id=[EncryptDecryptFile decrypt:[[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 8)]];
                data.stripe_userid=[EncryptDecryptFile decrypt:[[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 9)]];
                data.BusinessUserId=[EncryptDecryptFile decrypt:[[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 10)]];
                
                data.mobile=[EncryptDecryptFile decrypt:[[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 11)]];
                data.tnc=[EncryptDecryptFile decrypt:[[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 12)]];

                data.paymentType=[EncryptDecryptFile decrypt:[[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 13)]];
                data.accountFromServer=[EncryptDecryptFile decrypt:[[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 14)]];
                
                NSLog(@"payment type is..%@",data.paymentType);
                myAppDelegate.selectedPaymentTypeFromServer = data.paymentType;
                myAppDelegate.selectedPaymentTypeFromServer = @"1"; // By vs on 10 Jan 19 for using only Stripe approach and remove PayPal.
                NSLog(@"cust id is..%@",data.customer_id);
                NSLog(@"main pic path is..%@",data.picpath);
                NSLog(@"%@",data.mobile);
                
                NSArray *components = [data.picpath componentsSeparatedByString:@"/"];
                
                data.userImageName = [components lastObject];
                
                NSArray *components2 = [data.userImageName componentsSeparatedByString:@"."];
                
                NSString *imgNameWithJPEG =  data.userImageName;
                NSString *imgNameWithoutJPEG = [components2 firstObject]; //14184450_1111798702248394_7158270764418729077_n.jpeg
                
                UIImage *img = [GlobalFunction loadImageOfLogoFromCacheFolder:imgNameWithJPEG dirname:@"user"];
                
                if (img) {
                    data.userImage = img;
                }else{
                    NSLog(@"main pic path is..%@",data.picpath);
                    UIImage *img2 = [GlobalFunction getImageFromURL:data.picpath];
                    
                    if (img2) {
                        data.userImage = img2;
                        [GlobalFunction saveimage:data.userImage imageName:imgNameWithoutJPEG dirname:@"user"];
                    }else{
                        data.userImage = nil;
                    }
                    
                }
                
#pragma mark - database changes for handling payment data and bank data
                
                if (data.customer_id) {
                    
                    if ([data.customer_id isEqualToString:@""]) {
                        
                        myAppDelegate.isPaymentFilled = false;
                        
                    }else{
                        
                        myAppDelegate.isPaymentFilled = true;
                        
                    }
                    
                }else{
                    
                    myAppDelegate.isPaymentFilled = false;
                    
                }
                
                if (data.stripe_userid) {
                    
                    if ([data.stripe_userid isEqualToString:@""]) {
                        
                        myAppDelegate.isBankInfoFilled = false;
                        
                        myAppDelegate.isBankDetailFilled = false;
                        
                    }else{
                        if([data.paymentType isEqualToString:@"2"])
                        {
                            if(![data.accountFromServer isEqualToString:@""])
                            {
                                myAppDelegate.isBankInfoFilled = true;
                                myAppDelegate.isBankDetailFilled = true;
                            }
                            else
                            {
                                myAppDelegate.isBankInfoFilled = false;
                                myAppDelegate.isBankDetailFilled = false;
                            }
                        }
                        else
                        {
                        myAppDelegate.isBankInfoFilled = true;
                        myAppDelegate.isBankDetailFilled = true;
                        }
                        
                    }
                    
                }else{
                    
                    myAppDelegate.isBankInfoFilled = false;
                    myAppDelegate.isBankDetailFilled = false;
                    
                }

                if ([data.paymentType isEqualToString:@"1"])
                {
                    
                    if (myAppDelegate.isBankDetailFilled == true)
                    {
                        myAppDelegate.isStripeBankInfoFilled = true;
                    }
                    if (myAppDelegate.isPaymentFilled == true)
                    {
                        myAppDelegate.isStripeCardInfoFilled = true;
                    }
                }
                if ([data.paymentType isEqualToString:@"2"])
                {
                    if (myAppDelegate.isBankDetailFilled == true)
                    {
                        myAppDelegate.iscashOutPayPallFilled = true;
                    }
                    if (myAppDelegate.isPaymentFilled == true)
                    {
                        myAppDelegate.ispayWithPayPallFilled = true;

                    }
                  
                }
//                if ([data.paymentType isEqualToString:@"1"] && myAppDelegate.isFromTapPayPalRadioBtnOnCreateBio == true)
//                {
//                    myAppDelegate.isBankDetailFilled = false;
//                    myAppDelegate.isPaymentFilled = false;
//
//
//                }
//                if ([data.paymentType isEqualToString:@"2"] && myAppDelegate.isFromTapStripeRadioBtnOnCreateBio == true)
//                {
//                    myAppDelegate.isBankDetailFilled = false;
//                    myAppDelegate.isPaymentFilled = false;
//
//
//                }
//
                NSMutableArray *arr = [self getDataFromUserPaymentData:[[NSString stringWithFormat:@"select * from %@",tablePayment] UTF8String]];
                if (arr.count>0) {
                    
                    data.paymentData = [arr firstObject];
                    
                }else{
                    
                    data.paymentData = nil;
                    
                }
                
                [array addObject:data];
                
            }
        }
        sqlite3_close(buyTimeDB);
        buyTimeDB=nil;
    }
    NSLog(@"array length in DBCls is =%lu",(unsigned long)[array count]);
    
    return array;
}
#pragma mark - user Payment info by vs
+(void)savePaymentData:(const char *)query fromDict:(NSDictionary *)dict{
    
    if (dict.count<=0) {
        
        return;
    }
    sqlite3_stmt *dataRows=nil;
    if(sqlite3_open([[self getDBPath] UTF8String],&buyTimeDB) == SQLITE_OK){
        if (sqlite3_prepare_v2(buyTimeDB, query, -1, &dataRows, NULL)!=SQLITE_OK){
            NSAssert1(0,@"error while preparing  %s",sqlite3_errmsg(buyTimeDB));
        }
        sqlite3_bind_text(dataRows, 1, [[dict valueForKey:@"cardId"] UTF8String],-1,SQLITE_TRANSIENT);
        sqlite3_bind_text(dataRows, 2, [[dict valueForKey:@"userId"] UTF8String],-1,SQLITE_TRANSIENT);
        sqlite3_bind_text(dataRows, 3, [[dict valueForKey:@"payment_method"] UTF8String],-1,SQLITE_TRANSIENT);
        sqlite3_bind_text(dataRows, 4, [[dict valueForKey:@"card_holder_name"] UTF8String],-1,SQLITE_TRANSIENT);
        sqlite3_bind_text(dataRows, 5, [[dict valueForKey:@"card_number"] UTF8String],-1,SQLITE_TRANSIENT);
        sqlite3_bind_text(dataRows, 6, [[dict valueForKey:@"expiry_month"] UTF8String],-1,SQLITE_TRANSIENT);
        sqlite3_bind_text(dataRows, 7, [[dict valueForKey:@"expiry_year"] UTF8String],-1,SQLITE_TRANSIENT);
        sqlite3_bind_text(dataRows, 8, [[dict valueForKey:@"cvv_number"] UTF8String],-1,SQLITE_TRANSIENT);
        
        if (SQLITE_DONE!=sqlite3_step(dataRows)){
            char *err;
            err=(char *) sqlite3_errmsg(buyTimeDB);
            if (err){
                err = nil;
                sqlite3_free(err);
            }
            
        }
        sqlite3_finalize(dataRows);
        sqlite3_close(buyTimeDB);
        buyTimeDB=nil;
    }
    
}


+(NSMutableArray *)getDataFromUserPaymentData:(const char *)query{
    
    NSMutableArray *array = [[NSMutableArray alloc]init];
    
    if(sqlite3_open([[self getDBPath] UTF8String], &buyTimeDB) == SQLITE_OK)
    {
        sqlite3_stmt *statement;
        
        if (sqlite3_prepare_v2(buyTimeDB, query , -1, &statement, nil)==SQLITE_OK)
        {
            while(sqlite3_step(statement)==SQLITE_ROW)
            {
                
                PaymentInfoDataClass *data=[[PaymentInfoDataClass alloc]init];
                data.cardId=[EncryptDecryptFile decrypt:[[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 0)]];
                data.userId=[EncryptDecryptFile decrypt:[[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 1)]];
                data.payment_method=[EncryptDecryptFile decrypt:[[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 2)]];
                data.card_holder_name=[EncryptDecryptFile decrypt:[[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 3)]];
                data.card_number=[EncryptDecryptFile decrypt:[[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 4)]];
                data.expiry_month=[EncryptDecryptFile decrypt:[[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 5)]];
                data.expiry_year=[EncryptDecryptFile decrypt:[[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 6)]];
                data.cvv_number=[EncryptDecryptFile decrypt:[[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 7)]];
                
                [array addObject:data];
                
            }
        }
        
        sqlite3_close(buyTimeDB);
        buyTimeDB=nil;
        
    }
    NSLog(@"array length in DBCls is =%lu",(unsigned long)[array count]);
    
    
    return array;
    
}


#pragma mark - user Bank detailed by vs

+(void)saveBankInfoData:(const char *)query fromDict:(NSDictionary *)dict{
    
    if (dict.count<=0) {
        
        return;
    }
    
    
    sqlite3_stmt *dataRows=nil;
    if(sqlite3_open([[self getDBPath] UTF8String],&buyTimeDB) == SQLITE_OK){
        if (sqlite3_prepare_v2(buyTimeDB, query, -1, &dataRows, NULL)!=SQLITE_OK){
            NSAssert1(0,@"error while preparing  %s",sqlite3_errmsg(buyTimeDB));
        }
        sqlite3_bind_text(dataRows, 1, [[dict valueForKey:@"cardId"] UTF8String],-1,SQLITE_TRANSIENT);
        sqlite3_bind_text(dataRows, 2, [[dict valueForKey:@"userId"] UTF8String],-1,SQLITE_TRANSIENT);
        sqlite3_bind_text(dataRows, 3, [[dict valueForKey:@"payment_method"] UTF8String],-1,SQLITE_TRANSIENT);
        sqlite3_bind_text(dataRows, 4, [[dict valueForKey:@"card_holder_name"] UTF8String],-1,SQLITE_TRANSIENT);
        sqlite3_bind_text(dataRows, 5, [[dict valueForKey:@"card_number"] UTF8String],-1,SQLITE_TRANSIENT);
        sqlite3_bind_text(dataRows, 6, [[dict valueForKey:@"expiry_month"] UTF8String],-1,SQLITE_TRANSIENT);
        sqlite3_bind_text(dataRows, 7, [[dict valueForKey:@"expiry_year"] UTF8String],-1,SQLITE_TRANSIENT);
        sqlite3_bind_text(dataRows, 8, [[dict valueForKey:@"cvv_number"] UTF8String],-1,SQLITE_TRANSIENT);
        
        if (SQLITE_DONE!=sqlite3_step(dataRows)){
            char *err;
            err=(char *) sqlite3_errmsg(buyTimeDB);
            if (err){
                err = nil;
                sqlite3_free(err);
            }
            
        }
        sqlite3_finalize(dataRows);
        sqlite3_close(buyTimeDB);
        buyTimeDB=nil;
    }
    
}


+(NSMutableArray *)getUserBankInfoData:(const char *)query{
    
    NSMutableArray *array = [[NSMutableArray alloc]init];
    
    if(sqlite3_open([[self getDBPath] UTF8String], &buyTimeDB) == SQLITE_OK)
    {
        sqlite3_stmt *statement;
        
        if (sqlite3_prepare_v2(buyTimeDB, query , -1, &statement, nil)==SQLITE_OK)
        {
            while(sqlite3_step(statement)==SQLITE_ROW)
            {
                
                PaymentInfoDataClass *data=[[PaymentInfoDataClass alloc]init];
                data.cardId=[EncryptDecryptFile decrypt:[[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 0)]];
                data.userId=[EncryptDecryptFile decrypt:[[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 1)]];
                data.payment_method=[EncryptDecryptFile decrypt:[[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 2)]];
                data.card_holder_name=[EncryptDecryptFile decrypt:[[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 3)]];
                data.card_number=[EncryptDecryptFile decrypt:[[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 4)]];
                data.expiry_month=[EncryptDecryptFile decrypt:[[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 5)]];
                data.expiry_year=[EncryptDecryptFile decrypt:[[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 6)]];
                data.cvv_number=[EncryptDecryptFile decrypt:[[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 7)]];
                
                [array addObject:data];
                
            }
        }
        
        sqlite3_close(buyTimeDB);
        buyTimeDB=nil;
        
    }
    NSLog(@"array length in DBCls is =%lu",(unsigned long)[array count]);
    
    
    return array;
    
}
#pragma mark - Vendor detailed by vs

+(void)saveVendorData:(const char *)query fromDict:(NSDictionary *)dict{
    
    if (dict.count<=0) {
        
        return;
    }
    
    
    sqlite3_stmt *dataRows=nil;
    if(sqlite3_open([[self getDBPath] UTF8String],&buyTimeDB) == SQLITE_OK){
        if (sqlite3_prepare_v2(buyTimeDB, query, -1, &dataRows, NULL)!=SQLITE_OK){
            NSAssert1(0,@"error while preparing  %s",sqlite3_errmsg(buyTimeDB));
        }
        sqlite3_bind_text(dataRows, 1, [[dict valueForKey:@"address"] UTF8String],-1,SQLITE_TRANSIENT);
        sqlite3_bind_text(dataRows, 2, [[dict valueForKey:@"businessId"] UTF8String],-1,SQLITE_TRANSIENT);
        sqlite3_bind_text(dataRows, 3, [[dict valueForKey:@"instaLink"] UTF8String],-1,SQLITE_TRANSIENT);
        sqlite3_bind_text(dataRows, 4, [[dict valueForKey:@"name"] UTF8String],-1,SQLITE_TRANSIENT);
        sqlite3_bind_text(dataRows, 5, [[dict valueForKey:@"picPath"] UTF8String],-1,SQLITE_TRANSIENT);
        sqlite3_bind_text(dataRows, 6, [[dict valueForKey:@"services"] UTF8String],-1,SQLITE_TRANSIENT);
        sqlite3_bind_text(dataRows, 7, [[dict valueForKey:@"yelpProfile"] UTF8String],-1,SQLITE_TRANSIENT);
        
        
        if (SQLITE_DONE!=sqlite3_step(dataRows)){
            char *err;
            err=(char *) sqlite3_errmsg(buyTimeDB);
            if (err){
                err = nil;
                sqlite3_free(err);
            }
            
        }
        sqlite3_finalize(dataRows);
        sqlite3_close(buyTimeDB);
        buyTimeDB=nil;
    }
    
}


+(NSMutableArray *)getVendorInfoData:(const char *)query{
    
    NSMutableArray *array = [[NSMutableArray alloc]init];
    
    if(sqlite3_open([[self getDBPath] UTF8String], &buyTimeDB) == SQLITE_OK)
    {
        sqlite3_stmt *statement;
        
        if (sqlite3_prepare_v2(buyTimeDB, query , -1, &statement, nil)==SQLITE_OK)
        {
            while(sqlite3_step(statement)==SQLITE_ROW)
            {
                
                PaymentInfoDataClass *data=[[PaymentInfoDataClass alloc]init];
                data.cardId=[EncryptDecryptFile decrypt:[[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 0)]];
                data.userId=[EncryptDecryptFile decrypt:[[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 1)]];
                data.payment_method=[EncryptDecryptFile decrypt:[[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 2)]];
                data.card_holder_name=[EncryptDecryptFile decrypt:[[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 3)]];
                data.card_number=[EncryptDecryptFile decrypt:[[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 4)]];
                data.expiry_month=[EncryptDecryptFile decrypt:[[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 5)]];
                data.expiry_year=[EncryptDecryptFile decrypt:[[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 6)]];
                data.cvv_number=[EncryptDecryptFile decrypt:[[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 7)]];
                
                [array addObject:data];
                
            }
        }
        
        sqlite3_close(buyTimeDB);
        buyTimeDB=nil;
        
    }
    NSLog(@"array length in DBCls is =%lu",(unsigned long)[array count]);
    
    
    return array;
    
}


#pragma mark - user payment paid info
+(void)savePaymentPaidData:(const char *)query fromDict:(NSDictionary *)dict{
    
    sqlite3_stmt *dataRows=nil;
    if(sqlite3_open([[self getDBPath] UTF8String],&buyTimeDB) == SQLITE_OK){
        if (sqlite3_prepare_v2(buyTimeDB, query, -1, &dataRows, NULL)!=SQLITE_OK){
            NSAssert1(0,@"error while preparing  %s",sqlite3_errmsg(buyTimeDB));
        }
        sqlite3_bind_text(dataRows, 1, [[[dict valueForKey:@""] firstObject] UTF8String],-1,SQLITE_TRANSIENT);
        sqlite3_bind_text(dataRows, 2, [[[dict valueForKey:@""] firstObject] UTF8String],-1,SQLITE_TRANSIENT);
        sqlite3_bind_text(dataRows, 3, [[[dict valueForKey:@""] firstObject] UTF8String],-1,SQLITE_TRANSIENT);
        sqlite3_bind_text(dataRows, 4, [[[dict valueForKey:@""] firstObject] UTF8String],-1,SQLITE_TRANSIENT);
        sqlite3_bind_text(dataRows, 5, [[[dict valueForKey:@""] firstObject] UTF8String],-1,SQLITE_TRANSIENT);
        sqlite3_bind_text(dataRows, 6, [[[dict valueForKey:@""] firstObject] UTF8String],-1,SQLITE_TRANSIENT);
        sqlite3_bind_text(dataRows, 7, [[[dict valueForKey:@""] firstObject] UTF8String],-1,SQLITE_TRANSIENT);
        
        if (SQLITE_DONE!=sqlite3_step(dataRows)){
            char *err;
            err=(char *) sqlite3_errmsg(buyTimeDB);
            if (err){
                err = nil;
                sqlite3_free(err);
            }
            
        }
        sqlite3_finalize(dataRows);
        sqlite3_close(buyTimeDB);
        buyTimeDB=nil;
    }
    
}

+(NSMutableArray *)getDataFromUserPaymentPaidInfo:(const char *)query{
    
    NSMutableArray *array = [[NSMutableArray alloc]init];
    return array;
    
}


@end
