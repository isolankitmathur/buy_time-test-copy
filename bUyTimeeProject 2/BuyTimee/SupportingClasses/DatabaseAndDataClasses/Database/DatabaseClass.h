//
//  DatabaseClass.h
//  Patrobuddy
//
//  Created by User14 on 01/04/15.
//
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>

@interface DatabaseClass : NSObject


+(void) checkDataBase;
+(NSString *) getDBPath;
+(void)deleteRecord:(const char *)query;
+(sqlite3_stmt *) getDataset:(const char *)query;
+(int)totalRecord;
+(int)totalRecord:(const char *)query;
+(void)deleteLastRecord:(const char *)query rowId:(int)tag;
+(NSMutableArray *)fetchTableNames;
+(BOOL)CheckTableExists:(NSString *)query;
+(BOOL)CheckColumnExists:(NSString *)query;
+(void)finalizeStatements;
+(void)executeQuery:(NSString *)query;
+(void)updateStatus :(const char *)query;
+(void)insertIntoDatabase:(const char *)query tempArray:(NSArray *)tempArray;

+(void)updateStripeIdInDataBase:(NSString *)stripeId forUserId:(NSString *)userId;
+(void)updateCustomerIdInDataBase:(NSString *)customerId forUserId:(NSString *)userId;

+(BOOL)updateDataBaseFromDictionary:(NSDictionary *)dict;

+(BOOL)updateDataBaseFromUpdatedDictionary:(NSDictionary *)dict;
+(void)saveBusinessData:(const char *)query fromDict:(NSDictionary *)dict;



//fetching data from database
+(NSMutableArray *)getDataFromBusinessData:(const char *)query;

+(NSMutableArray *)getDataFromAcceptedTaskDetail:(const char *)query;

+(NSMutableArray *)getDataFromCategoryData:(const char *)query;

+(NSMutableArray *)getDataFromRatingTable:(const char *)query;

+(NSMutableArray *)getDataFromSubCategoryData:(const char *)query;

+(NSMutableArray *)getDataFromTaskData:(const char *)query;

+(NSMutableArray *)getDataFromTnCData:(const char *)query;

+(NSMutableArray *)getDataFromUserData:(const char *)query;

+(NSMutableArray *)getDataFromUserPaymentPaidInfo:(const char *)query;

+(void)saveBankData:(const char *)query fromDict:(NSDictionary *)dict;
+(void)saveUserData:(const char *)query fromDict:(NSDictionary *)dict;
+(void)savePaymentData:(const char *)query fromDict:(NSDictionary *)dict;
+(void)saveRatingData:(const char *)query fromDict:(NSDictionary *)dict;
+(void)saveBusinessDataFromVendor:(const char *)query fromDict:(NSDictionary *)dicts;
+(void)updateBusinessIdInDataBase:(NSString *)bsuinessId forUserId:(NSString *)userId;

@end
