//
//  VendorInfo.h
//  BuyTimee
//
//  Created by User38 on 03/03/17.
//  Copyright © 2017 User14. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VendorInfo : NSObject

@property(nonatomic,strong)NSString *vendor_Address;
@property(nonatomic,strong)NSString *vendor_BusinessId;
@property(nonatomic,strong)NSString *vendor_InstaLink;
@property(nonatomic,strong)NSString *vendor_Name;
@property(nonatomic,strong)NSString *vendor_PicPath;
@property(nonatomic,strong)NSString *vendor_Services;
@property(nonatomic,strong)NSString *vendor_yelpProfile;

@end
