//
//  task.h
//  
//
//  Created by  ~ on 17/03/16.
//  Copyright (c) 2016 . All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface task : NSObject

@property(nonatomic,strong) NSString *taskid;
@property(nonatomic,assign) NSInteger taskid_Integer;
@property(nonatomic,strong) NSString *userid;
@property(nonatomic,strong) NSString *subcategoryId;
@property(nonatomic,strong) NSString *title;
@property(nonatomic,strong) NSString *taskDescription;
@property(nonatomic,strong) NSString *taskImg;
@property(nonatomic,assign) CGFloat price;
@property(nonatomic,strong) NSString *startDate;
@property(nonatomic,strong) NSString *endDate;
@property(nonatomic,assign) CGFloat latitude;
@property(nonatomic,assign) CGFloat longitude;
@property(nonatomic,strong) NSString *locationName;
@property(nonatomic,strong) NSString *lastModifyTime;
@property(nonatomic,strong) NSString *status;
@property(nonatomic,strong) NSString *picpath;
@property(nonatomic,strong) UIImage *userImage;
@property(nonatomic,strong) NSString *userImageName;
@property(nonatomic,strong) NSMutableArray *taskImagesArray;
@property(nonatomic,strong) NSMutableArray *imagesArray;
@property(nonatomic,strong) NSString *businessIdFromTask;
@property(nonatomic,strong) NSString *businessName; // Added by RP


@property(nonatomic,strong) NSString *reservationType_ForHistory;
@property(nonatomic,strong) NSString *reservationStatus_ForHistory;
@property(nonatomic,strong) NSString *reviewStatus_ForHistory;
@property(nonatomic,strong) NSString *editStatus;


@property(nonatomic,strong) NSString *paymentMethod;
@property(nonatomic,strong) NSString *categoryId;
@property(nonatomic,strong) NSString *discountValue;
@property(nonatomic,strong) NSString *discountDiscription;
@property(nonatomic,strong) NSString *soldStatus;
@property(nonatomic,strong) NSString *discountDescription;
@property(nonatomic,strong) NSString *quantityOfDiscount;
@property(nonatomic,strong) NSString *feeValue;

//***by kp

@property(nonatomic,strong) NSString *priceNewVal;

// by vs 18 Oct 18 to check request appoinment...

@property(nonatomic,strong) NSString *requestAppointmentStatus;

//************* Rohitash Prajapati **********//
@property(nonatomic,strong) NSString *purchaseID;

@end
