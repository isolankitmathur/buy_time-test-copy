//
//  accept_task_info.h
//  
//
//  Created by  ~ on 17/03/16.
//  Copyright (c) 2016 . All rights reserved.
//

#import <Foundation/Foundation.h>

@interface accept_task_info : NSObject

@property(nonatomic,strong) NSString *acceptId;
@property(nonatomic,strong) NSString *taskId;
@property(nonatomic,strong) NSString *acceptTime;
@property(nonatomic,strong) NSString *workerId;
@property(nonatomic,strong) NSString *completedTime;
@property(nonatomic,strong) NSString *lastModifyTime;


@end
