//
//  category.h
//  
//
//  Created by  ~ on 17/03/16.
//  Copyright (c) 2016 . All rights reserved.
//

#import <Foundation/Foundation.h>

@interface category : NSObject

@property(nonatomic,strong) NSString *categoryId;
@property(nonatomic,strong) NSString *name;

@property(nonatomic,strong) NSString *categoryImg;


@end
