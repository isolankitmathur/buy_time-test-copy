//
//  PaymentInfoDataClass.h
//  BuyTimee
//
//  Created by User14 on 22/03/16.
//  Copyright © 2016 User14. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PaymentInfoDataClass : NSObject

@property(nonatomic,strong)NSString *cardId;
@property(nonatomic,strong)NSString *card_holder_name;
@property(nonatomic,strong)NSString *card_number;
@property(nonatomic,strong)NSString *cvv_number;
@property(nonatomic,strong)NSString *expiry_month;
@property(nonatomic,strong)NSString *expiry_year;
@property(nonatomic,strong)NSString *payment_method;
@property(nonatomic,strong)NSString *userId;

@end
