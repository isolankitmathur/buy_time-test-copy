//
//  user.h
//  
//
//  Created by  ~ on 17/03/16.
//  Copyright (c) 2016 . All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "ViewBank.h"
#import "PaymentInfoDataClass.h"

@interface user : NSObject

@property(nonatomic,strong)NSString *userid;
@property(nonatomic,strong)NSString *username;
@property(nonatomic,strong)NSString *emailId;
@property(nonatomic,strong)NSString *mobile;
@property(nonatomic,strong)NSString *instaLink;
@property(nonatomic,strong)NSString *tnc;

@property(nonatomic,strong)NSString *paymentType;

@property(nonatomic,strong)NSString *accountFromServer;

@property(nonatomic,strong)NSString *location;
@property(nonatomic,strong)NSString *userDescription;
@property(nonatomic,strong)NSString *picpath;
@property(nonatomic,strong)NSString *avg_rating;
@property(nonatomic,strong)UIImage *userImage;
@property(nonatomic,strong)NSString *userImageName;
@property(nonatomic,strong)NSString *customer_id;
@property(nonatomic,strong)NSString *stripe_userid;
@property(nonatomic,strong)NSString *BusinessUserId;

@property(nonatomic,assign)NSInteger socialFlag_fb;
@property(nonatomic,strong)NSString *socialuserId_fb;
@property(nonatomic,strong)NSString *socialemailId_fb;

@property(nonatomic,strong)ViewBank *bankInfoData;
@property(nonatomic,strong)PaymentInfoDataClass *paymentData;

@end
