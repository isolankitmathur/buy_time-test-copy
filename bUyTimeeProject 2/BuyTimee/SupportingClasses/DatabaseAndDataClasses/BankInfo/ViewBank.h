//
//  ViewBank.h
//  
//
//  Created by  ~ on 17/03/16.
//  Copyright (c) 2016 . All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ViewBank : NSObject

@property(nonatomic,strong)NSString *bankId;
@property(nonatomic,strong)NSString *userId;
@property(nonatomic,strong)NSString *name;
@property(nonatomic,strong)NSString *accountNo;
@property(nonatomic,strong)NSString *routingNo;
@property(nonatomic,strong)NSString *bankName;
@property(nonatomic,strong)NSString *accType;


@end
