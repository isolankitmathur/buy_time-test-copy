//
//  BusinessInfo.h
//  BuyTimee
//
//  Created by User38 on 24/02/17.
//  Copyright © 2017 User14. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BusinessInfo : NSObject

@property(nonatomic,strong) NSString *BusinessId;
@property(nonatomic,assign) NSInteger businessid_Integer;
@property(nonatomic,strong) NSString *BusinessName;
@property(nonatomic,strong) NSString *BusinessAdd;
@property(nonatomic,strong) NSString *categoryId;
@end
