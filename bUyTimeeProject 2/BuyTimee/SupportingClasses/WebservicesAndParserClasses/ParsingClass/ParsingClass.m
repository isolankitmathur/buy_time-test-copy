//
//  ParsingClass.m
//  
//
//  Created by  ~ on 16/03/16.
//  Copyright (c) 2016 . All rights reserved.
//

#import "ParsingClass.h"
#import "EncryptDecryptFile.h"
@implementation ParsingClass


+(NSDictionary *)parseUserDict:(NSDictionary *)userDict{
    
    NSArray *arrayTerms= [userDict valueForKey:@"user"];
    
    
    NSDictionary *userDictionary;
    if (arrayTerms.count>0) {
        userDictionary=[arrayTerms objectAtIndex:0];
        
        NSString *lastupdatetm = [EncryptDecryptFile decrypt:[userDictionary valueForKey:@"mobile"]];
        NSLog(@"%@",lastupdatetm);

        
    }
    else
    {
        userDictionary=[[NSDictionary alloc] init];
        
    }
    
    
    return userDictionary;
}

+(NSDictionary *)parseTaskDict:(NSDictionary *)taskDict;{
    
    NSDictionary *dictTemp =[taskDict objectForKey:@"reservation"];
    return dictTemp;
    
    NSArray *arrayTerms= [taskDict valueForKey:@"reservation"];//[taskDict valueForKey:@"task"];
    NSDictionary *TaskDictionary;
    if (arrayTerms.count>0) {
        TaskDictionary=[arrayTerms objectAtIndex:0];

    }
    else
    {
        TaskDictionary=[[NSDictionary alloc] init];

    }
    
    
    return TaskDictionary;
}

+(NSDictionary *)parseTermsAndConditionDict:(NSDictionary *)TermsAndConditionDict{
    NSArray *arrayTerms= [TermsAndConditionDict valueForKey:@"terms_and_condition"];//[TermsAndConditionDict valueForKey:@"Terms and condition"]
    
    NSDictionary *TermsAndConditionDictionary=[arrayTerms objectAtIndex:0];

    return TermsAndConditionDictionary;
}

+(NSDictionary *)parseCategoryDict:(NSDictionary *)categoryDict{
    
    
    NSDictionary *dictTemp =[categoryDict objectForKey:@"category"];
    return dictTemp;
    
    NSArray *arrayTerms= [categoryDict valueForKey:@"category"];
    
    NSDictionary *CategoryDictionary=[arrayTerms objectAtIndex:0];
    
    return CategoryDictionary;
}

+(NSDictionary *)parseBusinessDict:(NSDictionary *)businessDict{
    
    
    NSDictionary *dictTemp =[businessDict objectForKey:@"business"];
    return dictTemp;
    
    NSArray *arrayTerms= [businessDict valueForKey:@"business"];
    
    NSDictionary *CategoryDictionary=[arrayTerms objectAtIndex:0];
    
    return CategoryDictionary;
}
// sub category commented bcz of no need in this approach here. TM

//+(NSDictionary *)parseSubCategoryDict:(NSDictionary *)SubcategoryDict;{
//    
//    NSArray *arrayTerms= [SubcategoryDict valueForKey:@"subCategory"];
//    
//    NSDictionary *SubCategoryDictionary=[arrayTerms objectAtIndex:0];
//    
//    return SubCategoryDictionary;
//}

// sub category commented bcz of no need in this approach here. TM

//+(NSDictionary *)parseAcceptTaskInfoDict:(NSDictionary *)AcceptTaskInfoDict{
//    
//    NSArray *arrayTerms= [AcceptTaskInfoDict valueForKey:@"accept_task_Info"];
//    
//    NSDictionary *AcceptTaskInfoDictionary=[arrayTerms objectAtIndex:0];
//    
//    return AcceptTaskInfoDictionary;
//}

+(NSDictionary *)parseRatingInfoDict:(NSDictionary *)AcceptTaskInfoDict{
    
    NSArray *arrayTerms= [AcceptTaskInfoDict valueForKey:@"ratingInfo"];
    
    NSDictionary *AcceptTaskInfoDictionary=[arrayTerms objectAtIndex:0];
    
    return AcceptTaskInfoDictionary;
}

+(NSDictionary *)parseVendorDict:(NSDictionary *)AcceptTaskInfoDict{
    
    NSDictionary *dictTemp =[AcceptTaskInfoDict objectForKey:@"vendor"];
    return dictTemp;
}
+(NSDictionary *)parseBankInfoDict:(NSDictionary *)AcceptTaskInfoDict{
    
    NSDictionary *dictTemp =[AcceptTaskInfoDict objectForKey:@"bankInfo"];
    return dictTemp;
}
+(NSDictionary *)parseCardInfoDict:(NSDictionary *)AcceptTaskInfoDict{
    
    NSDictionary *dictTemp =[AcceptTaskInfoDict objectForKey:@"cardInfo"];
    return dictTemp;
    
//    NSArray *arrayTerms= [businessDict valueForKey:@"business"];
//    
//    NSDictionary *CategoryDictionary=[arrayTerms objectAtIndex:0];
//    
//    return CategoryDictionary;

    
    
    NSArray *arrayTerms= [AcceptTaskInfoDict valueForKey:@"cardInfo"];
    
    NSDictionary *AcceptTaskInfoDictionary=[arrayTerms objectAtIndex:0];
    
    return AcceptTaskInfoDictionary;
}



@end
