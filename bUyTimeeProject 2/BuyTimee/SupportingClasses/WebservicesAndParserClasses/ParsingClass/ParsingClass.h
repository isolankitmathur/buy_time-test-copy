//
//  ParsingClass.h
//  
//
//  Created by  ~ on 16/03/16.
//  Copyright (c) 2016 . All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ParsingClass : NSObject

+(NSDictionary *)parseUserDict:(NSDictionary *)userDict;
+(NSDictionary *)parseTaskDict:(NSDictionary *)taskDict;
+(NSDictionary *)parseTermsAndConditionDict:(NSDictionary *)TermsAndConditionDict;
+(NSDictionary *)parseCategoryDict:(NSDictionary *)categoryDict;
+(NSDictionary *)parseSubCategoryDict:(NSDictionary *)SubcategoryDict;
+(NSDictionary *)parseAcceptTaskInfoDict:(NSDictionary *)AcceptTaskInfoDict;
+(NSDictionary *)parseRatingInfoDict:(NSDictionary *)AcceptTaskInfoDict;
+(NSDictionary *)parseCardInfoDict:(NSDictionary *)AcceptTaskInfoDict;
+(NSDictionary *)parseBankInfoDict:(NSDictionary *)AcceptTaskInfoDict;
+(NSDictionary *)parseBusinessDict:(NSDictionary *)businessDict;
//+(NSDictionary *)parseBankInfoDict:(NSDictionary *)AcceptTaskInfoDict;
+(NSDictionary *)parseVendorDict:(NSDictionary *)vendorDict;

@end
