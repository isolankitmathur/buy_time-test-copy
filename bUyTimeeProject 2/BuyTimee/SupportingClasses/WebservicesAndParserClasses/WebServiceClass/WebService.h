//
//  WebService.h
//  
//
//  Created by  ~ on 14/03/16.
//  Copyright (c) 2016 . All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface WebService : NSObject<NSURLConnectionDataDelegate>
{
    NSMutableData *responseDataforSession;
}


+ (instancetype) shared;
@property(nonatomic,strong)UIViewController *VCObj;

-(void)showRequest:(NSData *)body with:(NSString *)Devicetype with:(NSString*)Cipher with:(NSString *)Apiversion;

-(NSDictionary *)description:(NSString *)description picPath:(NSString *)picPath token:(NSString *)token flag:(NSInteger)flag fbId:(NSString *)fbId googleId:(NSString *)googleID fname:(NSString *)fname lname:(NSString *)lname emailId:(NSString *)emailId latitude:(NSString *)latitude longitude:(NSString *)longitude timeZone:(NSString *)timeZone; // by vs

-(NSDictionary *)regisusername:(NSString *)username emailId:(NSString *)emailId password:(NSString *)password token:(NSString *)token;

-(NSDictionary *)forgotemailId:(NSString *)emailId;

-(NSDictionary *)resetPasswordForUserId:(NSString *)userId newPassword:(NSString *)newPassword otp:(NSString *)otp;


//-(NSDictionary *)createTaskuserid:(NSString *)userid  subcategoryId:(NSString *)subcategoryId title:(NSString *)title description:(NSString *)description taskImgNamesArray:(NSMutableArray *)taskImgNamesArray price:(NSString *)price startDate:(NSString *)startDate endDate:(NSString *)endDate locationName:(NSString *)locationName latitude:(NSString *)latitude longitude:(NSString *)longitude taskImgArray:(NSMutableArray *)taskImgArray;

-(NSDictionary *)createTaskuserId:(NSString *)userId categoryId:(NSString *)categoryId businessId:(NSString *)businessId paymentMethod:(NSString *)paymentMethod title:(NSString *)title description:(NSString *)description taskImgNamesArray:(NSMutableArray *)taskImgNamesArray price:(NSString *)price startDate:(NSString *)startDate startTime:(NSString *)startTime locationName:(NSString *)locationName latitude:(NSString *)latitude longitude:(NSString *)longitude taskImgArray:(NSMutableArray *)taskImgArray timeZone:(NSString *)timeZone discount:(NSString *)discount totalUsers:(NSString *)totalUsers discountDescription:(NSString *)discountDescription edit:(NSString *)edit reservationId:(NSString *)reservationId;

-(NSDictionary *)updateProfileuserId:(NSString *)userId name:(NSString *)name description:(NSString *)description latitude:(NSString *)latitude longitude:(NSString *)longitude andUserImage:(UIImage *)image andImageName:(NSString *)ImageName instaLink:(NSString *)instaLink location:(NSString *)location; // by vs


-(NSDictionary *)requestReservationUserId:(NSString *)userId timeZone:(NSString*)timeZone title:(NSString *)title locationName:(NSString *)locationName businessName:(NSString *)businessName startDate:(NSString *)startDate startTime:(NSString *)startTime description:(NSString *)description categoryId:(NSString *)categoryId price:(NSString *)price name:(NSString *)name mobile:(NSString *)mobile; // request reservation by VS 27 Sept 18...

//***************************
//by kp

-(NSDictionary *)newUpdateProfileuserId:(NSString *)userId name:(NSString *)name description:(NSString *)description latitude:(NSString *)latitude longitude:(NSString *)longitude andUserImage:(UIImage *)image andImageName:(NSString *)ImageName instaLink:(NSString *)instaLink location:(NSString *)location mobile: (NSString *) mobile; // by kp

//***************************








//-(NSDictionary *)updateVendorProfileuserId:(NSString *)userId name:(NSString *)name andUserImage:(UIImage *)image andImageName:(NSString *)ImageName instaLink:(NSString *)instaLink yelpProfile:(NSString *)yelpProfile address:(NSString *)address services:(NSString *)services;  // by vs

-(NSDictionary *)updateVendorProfileuserId:(NSString *)userId name:(NSString *)name andUserImage:(UIImage *)image andImageName:(NSString *)ImageName instaLink:(NSString *)instaLink yelpProfile:(NSString *)yelpProfile address:(NSString *)address services:(NSString *)services categoryId:(NSString*)categoryId; // by Rohitash Prajapati

-(NSDictionary *)updateBankProfileuserId:(NSString *)userId name:(NSString *)name accountNo:(NSString *)accountNo routingNo:(NSString *)routingNo bankName:(NSString *)bankName accType:(NSString *)accType ip:(NSString *)ip dob:(NSString *)dob ssn:(NSString *)ssn type:(NSString *)type address:(NSString *)address taxId:(NSString *)taxId andUserImage:(UIImage *)image andImageName:(NSString *)ImageName; // by vs

-(NSDictionary *)addBankUserid:(NSString *)userid name:(NSString *)name accountNo:(NSString *)accountNo routingNo:(NSString *)routingNo bankName:(NSString *)bankName accType:(NSString *)accType; // by vs

-(NSDictionary *)addCardUserid:(NSString *)userid payment_method:(NSString *)payment_method card_holder_name:(NSString *)card_holder_name card_number:(NSString *)card_number expiry_month:(NSString *)expiry_month expiry_year:(NSString *)expiry_year cvv_number:(NSString *)cvv_number; // by vs

//-(NSDictionary *)purchaseReservationid:(NSString *)taskid userid:(NSString *)userid LastModifyTime:(NSString *)LastModifyTime; //vs

-(NSDictionary *)purchaseReservationid:(NSString *)taskid userid:(NSString *)userid LastModifyTime:(NSString *)LastModifyTime timeZone:(NSString*)timeZone; // By RP


-(NSDictionary *)reportTaskid:(NSString *)taskid userid:(NSString *)userid; // by vs
-(NSDictionary *)viewVendorUserid:(NSString *)userid; // by vs
-(NSDictionary *)viewCardUserid:(NSString *)userid; // by vs
-(NSDictionary *)viewBankUserid:(NSString *)userid accType:(NSString *)type; // by vs
-(NSDictionary *)viewTaskInfoTaskid:(NSString *)taskid userid:(NSString *)userid timeZone:(NSString *)timeZone view:(NSString *)view;
-(NSDictionary *)acceptTaskTaskid:(NSString *)taskid userid:(NSString *)userid;
-(NSDictionary *)employeeStatusTaskid:(NSString *)taskid status:(NSString *)status;
-(NSDictionary *)taskCompleteTaskid:(NSString *)taskid status:(NSString *)status;

-(NSDictionary *)addRatingUserid:(NSString *)userid ratingStar:(NSString *)ratingStar description:(NSString *)description taskid:(NSString *)taskid employerId:(NSString *)employerId; //by vs

-(NSDictionary *)getUpdatelastModifyTime:(NSString *)lastModifyTime userid:(NSString *)userid timeZone:(NSString *)timeZone; // by vs

-(NSDictionary *)generateStripeUserIdForUserId:(NSString *)userId andAuthCode:(NSString *)authCode;

-(NSDictionary *)removeStripeForUserid:(NSString *)userid;

-(NSDictionary *)token:(NSString *)token; //vs

-(NSDictionary *)reservationHistoryUserid:(NSString *)userid timeZone:(NSString *)timeZone; //vs

-(NSDictionary *)vendorstatusTaskid:(NSString *)taskid status:(NSString *)status vendorComment:(NSString *)vendorComment userId:(NSString *)userId;


-(NSDictionary *)aboutUsToken:(NSString *)token; //kp


-(NSDictionary *)deleteReservationTaskid:(NSString *)taskid userid:(NSString *)userid; // vs



//-(NSDictionary *)webViewInfoUserId:(NSString *)userId reservationId:(NSString *)reservationId; //kp

//-(NSDictionary *) webViewInfoUserId:(NSString *)userId reservationId:(NSString *)reservationId withPurchaseID:(NSString*)purchaseID;

-(NSDictionary *) webViewInfoUserId:(NSString *)userId reservationId:(NSString *)reservationId withPurchaseID:(NSString*)purchaseID timeZone:(NSString*)timeZone; // Rohitash prajapati




-(NSDictionary *) termsAndPolicyUserId:(NSString *)userId tnc:(NSString *)tnc;

-(NSDictionary *)addPayPalUserid:(NSString *)Userid metaDataId:(NSString *)metaDataId codeString:(NSString *)codeString; // by vs 12 June 18


-(NSDictionary *)AddEmailUserid:(NSString *)Userid emailId:(NSString *)emailId; // by vs 13 June 18




//old methods of aks replaced by vs//

//-(NSDictionary *)loginusername:(NSString *)username password:(NSString *)password token:(NSString *)token socialFlag:(NSInteger)socialFlag socialID:(NSString *)socialID socialEmailId:(NSString *)socialEmailId;

//-(NSDictionary *)updateProfileUserid:(NSString *)userid username:(NSString *)username emailId:(NSString *)emailId location:(NSString *)location mobile:(NSString *)mobile description:(NSString *)description latitude:(NSString *)latitude longitude:(NSString *)longitude andUserImage:(UIImage *)image andImageName:(NSString *)ImageName;

//-(NSDictionary *)addCardUserid:(NSString *)userid payment_method:(NSString *)payment_method card_holder_name:(NSString *)card_holder_name card_number:(NSString *)card_number expiry_month:(NSString *)expiry_month expiry_year:(NSString *)expiry_year cvv_number:(NSString *)cvv_number;
//-(NSDictionary *)getUpdateuserid :(NSString *)userid lastModifyTime:(NSString *)lastModifyTime andToken:(NSString *)token;

//-(NSDictionary *)addRatingUserid:(NSString *)userid ratingStar:(NSString *)ratingStar description:(NSString *)description taskid:(NSString *)taskid employerId:(NSString *)employerId;
-(BOOL)connected;

//- (NSData *)sendSynchronousRequest:(NSURLRequest *)request returningResponse:(NSURLResponse *__strong*)response error:(NSError *__strong*)error;

@end


