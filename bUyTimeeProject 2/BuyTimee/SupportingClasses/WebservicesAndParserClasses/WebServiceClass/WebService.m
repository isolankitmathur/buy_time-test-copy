//
//  WebService.m
//
//
//  Created by  ~ on 14/03/16.
//  Copyright (c) 2016 . All rights reserved.
//

#import "WebService.h"
#import "EncryptDecryptFile.h"
#import <CommonCrypto/CommonCryptor.h>
#import "AppDelegate.h"
#import "Constants.h"
#import "Reachability.h"
#import "LoginViewController.h"
#import "GlobalFunction.h"


#define AppObj ((AppDelegate*)[[UIApplication sharedApplication]delegate])


@implementation WebService

static NSString *cipherSecretKey=@"$%x*";
static NSString *Apiversion=@"1.0";
static NSString *Devicetype=@"ios";


static NSString *methodName;
static NSDictionary*jsonDict;
//NSMutableData * receivedData;

+ (instancetype) shared{
    
    jsonDict = nil;
    
    static dispatch_once_t once;
    static id sharedInstance;
    dispatch_once(&once, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
    
}
-(BOOL)connected{
    
//    Reachability *reachability = [Reachability reachabilityForInternetConnection];
//    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
//    return networkStatus != NotReachable;
    
    Reachability *internetReachableFoo = [Reachability reachabilityWithHostName:@"www.google.com"];
    NetworkStatus networkStatus = [internetReachableFoo currentReachabilityStatus];
    return networkStatus != NotReachable;
    
}


-(void)showRequest:(NSData *)body with:(NSString *)Devicetype with:(NSString*)Cipher with:(NSString *)Apiversion{
    
    NSData *returnData=[[NSData alloc]init];
    
   NSURL *url1 = [NSURL URLWithString:[NSString stringWithFormat:urlServer,methodName]];
    
//     NSURL *url1 = [NSURL URLWithString:@"https://admin.buytimee.com/User/terms_of_services"];
    
    NSMutableURLRequest *request=[[NSMutableURLRequest alloc]init];
   // NSURLRequest *request=[[NSURLRequest alloc]init];
    [request addValue:Devicetype forHTTPHeaderField:@"Devicetype"];
    [request addValue:Cipher forHTTPHeaderField:@"Cipher"];
    [request addValue:Apiversion forHTTPHeaderField:@"Apiversion"];
    [request setURL:url1];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:body];
    [request setTimeoutInterval:60.0];
 
    NSURLResponse *response;
    NSError *error;

  //  NSURL *url = [NSURL URLWithString:@"https://api.twitter.com/1.1/search/tweets.json"];
    
    
    //  [NSURLConnection connectionWithRequest:request delegate:self];
    
    returnData = [ NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    
   // returnData = [self sendSynchronousRequest:request returningResponse:&response error:&error];
    
    NSLog(@"response from url -=-- %@",response);
    
    if (error) {
        NSLog(@"error ==== %@",error);
    }
    if (returnData) {
        
        NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
        NSLog(@"data === %@",returnString);
        
    }
    
    jsonDict = nil;
    if (returnData != nil) {
        
        jsonDict = [NSJSONSerialization JSONObjectWithData:returnData
                                                   options:NSJSONReadingMutableContainers
                                                     error:nil];
    }
    
}

///////////////************** for ssl handle by vssss 17 april ***********//////

//- (NSData *)sendSynchronousRequest:(NSURLRequest *)request returningResponse:(NSURLResponse *__strong*)response error:(NSError *__strong*)error
//{
////    _finishedLoading=NO;
//  receivedData=[NSMutableData new];
////    _error=error;
////    _response=response;
//    
//    NSURLConnection*con=[NSURLConnection connectionWithRequest:request delegate:self];
//    [con start];
//    CFRunLoopRun();
//    
//    return receivedData;
//}

//- (BOOL)connection:(NSURLConnection *)connection canAuthenticateAgainstProtectionSpace:(NSURLProtectionSpace *)protectionSpace {
//    return [protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust];
//}

//- (void)connection:(NSURLConnection *)connection didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge
//{
//    //handle the challenge
//}

//- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
//{
//    response=response;
//}
//
//- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
//{
//  //  [receivedData appendData:data];
//}
//
//- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
//{
//    error=error;
//    CFRunLoopStop(CFRunLoopGetCurrent());
//}
//
//- (void)connectionDidFinishLoading:(NSURLConnection *)connection
//{
//    CFRunLoopStop(CFRunLoopGetCurrent());
//}


///////////////************** for ssl handle by vssss 17 april end work ***********//////

-(NSDictionary *)token:(NSString *)token
{
methodName=@"User/terms_of_services";
    
    NSString *encryptToken=[EncryptDecryptFile EncryptIT:token];
    encryptToken=[EncryptDecryptFile urlencode:encryptToken];
    
    NSMutableData *DataValue = [NSMutableData data];
    
    [DataValue appendData:[[NSString stringWithFormat:@"&token=%@",encryptToken]dataUsingEncoding:NSASCIIStringEncoding]];
    
NSString *input = [NSString stringWithFormat:@"%@%@%@%@%@%@",Apiversion,cipherSecretKey,Devicetype,cipherSecretKey,token,cipherSecretKey];
    
    NSString *output=[[NSString alloc]init];
    output = [EncryptDecryptFile md5:input];
    
    if([self connected] == true){
        
        [self showRequest:DataValue with:Devicetype with:output with:Apiversion];
        
    }else{
        
    }

    
    
 return jsonDict;
}


-(NSDictionary *)aboutUsToken:(NSString *)token
{
    methodName=@"User/about_us";
    
    
    
    NSString *encryptToken=[EncryptDecryptFile EncryptIT:token];
    encryptToken=[EncryptDecryptFile urlencode:encryptToken];
    
    NSMutableData *DataValue = [NSMutableData data];
    
    [DataValue appendData:[[NSString stringWithFormat:@"&token=%@",encryptToken]dataUsingEncoding:NSASCIIStringEncoding]];
    
    NSString *input = [NSString stringWithFormat:@"%@%@%@%@%@%@",Apiversion,cipherSecretKey,Devicetype,cipherSecretKey,token,cipherSecretKey];
    
    NSString *output=[[NSString alloc]init];
    output = [EncryptDecryptFile md5:input];
    
    if([self connected] == true){
        
        [self showRequest:DataValue with:Devicetype with:output with:Apiversion];
        
    }else{
        
    }
    
    
    return jsonDict;
}



-(NSDictionary *) webViewInfoUserId:(NSString *)userId reservationId:(NSString *)reservationId
{
    
//    [GlobalFunction addIndicatorView];
    
    
    methodName=@"Reservation_Info/web_view";
    
    
    
    NSString *encryptUserId=[EncryptDecryptFile EncryptIT:userId];
    encryptUserId=[EncryptDecryptFile urlencode:encryptUserId];
    
    NSString *encryptReservationId=[EncryptDecryptFile EncryptIT:reservationId];
    encryptReservationId=[EncryptDecryptFile urlencode:encryptReservationId];

    
    NSMutableData *DataValue = [NSMutableData data];
    
    [DataValue appendData:[[NSString stringWithFormat:@"&userId=%@",encryptUserId]dataUsingEncoding:NSASCIIStringEncoding]];
    [DataValue appendData:[[NSString stringWithFormat:@"&reservationId=%@",encryptReservationId]dataUsingEncoding:NSASCIIStringEncoding]];

    
    NSString *input = [NSString stringWithFormat:@"%@%@%@%@%@%@%@%@",Apiversion,cipherSecretKey,Devicetype,cipherSecretKey,userId,cipherSecretKey,reservationId,cipherSecretKey];
    
    NSString *output=[[NSString alloc]init];
    output = [EncryptDecryptFile md5:input];
    
    if([self connected] == true){
        
        [self showRequest:DataValue with:Devicetype with:output with:Apiversion];
        
    }else{
        
    }
    
    
    return jsonDict;
}


//-(NSDictionary *) webViewInfoUserId:(NSString *)userId reservationId:(NSString *)reservationId withPurchaseID:(NSString*)purchaseID timeZone:(NSString*)timeZone
//{
//
//    //    [GlobalFunction addIndicatorView];
//
//
//    methodName=@"Reservation_Info/web_view";
//
//
//    NSString *encryptUserId=[EncryptDecryptFile EncryptIT:userId];
//    encryptUserId=[EncryptDecryptFile urlencode:encryptUserId];
//
//    NSString *encryptReservationId=[EncryptDecryptFile EncryptIT:reservationId];
//    encryptReservationId=[EncryptDecryptFile urlencode:encryptReservationId];
//
//    NSString *encryptpurchaseID =[EncryptDecryptFile EncryptIT:purchaseID];
//    encryptpurchaseID = [EncryptDecryptFile urlencode:encryptpurchaseID];
//
//
//    NSMutableData *DataValue = [NSMutableData data];
//
//    [DataValue appendData:[[NSString stringWithFormat:@"&userId=%@",encryptUserId]dataUsingEncoding:NSASCIIStringEncoding]];
//    [DataValue appendData:[[NSString stringWithFormat:@"&reservationId=%@",encryptReservationId]dataUsingEncoding:NSASCIIStringEncoding]];
//
//    [DataValue appendData:[[NSString stringWithFormat:@"&purchaseId=%@",encryptpurchaseID]dataUsingEncoding:NSASCIIStringEncoding]];
//
//
//    NSString *input = [NSString stringWithFormat:@"%@%@%@%@%@%@%@%@%@%@",Apiversion,cipherSecretKey,Devicetype,cipherSecretKey,userId,cipherSecretKey,reservationId,cipherSecretKey,purchaseID,cipherSecretKey];
//
//    NSString *output=[[NSString alloc]init];
//    output = [EncryptDecryptFile md5:input];
//
//    if([self connected] == true){
//
//        [self showRequest:DataValue with:Devicetype with:output with:Apiversion];
//
//    }else{
//
//    }
//
//
//    return jsonDict;
//}


-(NSDictionary *) webViewInfoUserId:(NSString *)userId reservationId:(NSString *)reservationId withPurchaseID:(NSString*)purchaseID timeZone:(NSString*)timeZone
{
    
    //    [GlobalFunction addIndicatorView];
    
    
    methodName=@"Reservation_Info/web_view";
    
    
    NSString *encryptUserId=[EncryptDecryptFile EncryptIT:userId];
    encryptUserId=[EncryptDecryptFile urlencode:encryptUserId];
    
    NSString *encryptReservationId=[EncryptDecryptFile EncryptIT:reservationId];
    encryptReservationId=[EncryptDecryptFile urlencode:encryptReservationId];
    
    NSString *encryptpurchaseID =[EncryptDecryptFile EncryptIT:purchaseID];
    encryptpurchaseID = [EncryptDecryptFile urlencode:encryptpurchaseID];
    
    NSString *encrypttimeZone =[EncryptDecryptFile EncryptIT:timeZone];
    encrypttimeZone = [EncryptDecryptFile urlencode:encrypttimeZone];
    
    
    NSMutableData *DataValue = [NSMutableData data];
    
    [DataValue appendData:[[NSString stringWithFormat:@"&userId=%@",encryptUserId]dataUsingEncoding:NSASCIIStringEncoding]];
    [DataValue appendData:[[NSString stringWithFormat:@"&reservationId=%@",encryptReservationId]dataUsingEncoding:NSASCIIStringEncoding]];
    
    [DataValue appendData:[[NSString stringWithFormat:@"&purchaseId=%@",encryptpurchaseID]dataUsingEncoding:NSASCIIStringEncoding]];
    
    [DataValue appendData:[[NSString stringWithFormat:@"&timeZone=%@",encrypttimeZone]dataUsingEncoding:NSASCIIStringEncoding]];
    
    
    NSString *input = [NSString stringWithFormat:@"%@%@%@%@%@%@%@%@%@%@%@%@",Apiversion,cipherSecretKey,Devicetype,cipherSecretKey,userId,cipherSecretKey,reservationId,cipherSecretKey,purchaseID,cipherSecretKey,timeZone,cipherSecretKey];
    
    NSString *output=[[NSString alloc]init];
    output = [EncryptDecryptFile md5:input];
    
    if([self connected] == true){
        
        [self showRequest:DataValue with:Devicetype with:output with:Apiversion];
        
    }else{
        
    }
    
    
    return jsonDict;
}









-(NSDictionary *)description:(NSString *)description picPath:(NSString *)picPath token:(NSString *)token flag:(NSInteger)flag fbId:(NSString *)fbId googleId:(NSString *)googleId fname:(NSString *)fname lname:(NSString *)lname emailId:(NSString *)emailId latitude:(NSString *)latitude longitude:(NSString *)longitude timeZone:(NSString *)timeZone;
{
   // token = @"123456";
    
//    googleId = @"";
//    emailId = @"";
//    flag = 1;
    methodName=@"User/login";
    
    NSMutableData *DataValue = [NSMutableData data];
    
    NSString *encryptdescription=[EncryptDecryptFile EncryptIT:description];
    encryptdescription=[EncryptDecryptFile urlencode:encryptdescription];
    
    NSString *encryptpicPath=[EncryptDecryptFile EncryptIT:picPath];
    encryptpicPath=[EncryptDecryptFile urlencode:encryptpicPath];
    
    NSString *encryptToken=[EncryptDecryptFile EncryptIT:token];
    encryptToken=[EncryptDecryptFile urlencode:encryptToken];
    
    NSString *encryptflag=[EncryptDecryptFile EncryptIT:[NSString stringWithFormat:@"%ld",(long)flag]];
    encryptflag=[EncryptDecryptFile urlencode:encryptflag];
    
    NSString *encryptfbId=[EncryptDecryptFile EncryptIT:fbId];
    encryptfbId=[EncryptDecryptFile urlencode:encryptfbId];
    
    NSString *encryptgoogleId=[EncryptDecryptFile EncryptIT:googleId];
    encryptgoogleId=[EncryptDecryptFile urlencode:encryptgoogleId];
    
    NSString *encryptfname=[EncryptDecryptFile EncryptIT:fname];
    encryptfname=[EncryptDecryptFile urlencode:encryptfname];
    
    NSString *encryptlname=[EncryptDecryptFile EncryptIT:lname];
    encryptlname=[EncryptDecryptFile urlencode:encryptlname];
    
    NSString *encryptemailId=[EncryptDecryptFile EncryptIT:emailId];
    encryptemailId=[EncryptDecryptFile urlencode:encryptemailId];
    
    NSString *encryptLat=[EncryptDecryptFile EncryptIT:latitude];
    encryptLat=[EncryptDecryptFile urlencode:encryptLat];
    
    NSString *encryptLong=[EncryptDecryptFile EncryptIT:longitude];
    encryptLong=[EncryptDecryptFile urlencode:encryptLong];
    
    NSString *encrypttimeZone=[EncryptDecryptFile EncryptIT:timeZone];
    encrypttimeZone=[EncryptDecryptFile urlencode:encrypttimeZone];
    
    [DataValue appendData:[[NSString stringWithFormat:@"&description=%@",encryptdescription]dataUsingEncoding:NSASCIIStringEncoding]];
    
    [DataValue appendData:[[NSString stringWithFormat:@"&picPath=%@",encryptpicPath]dataUsingEncoding:NSASCIIStringEncoding]];
    
    [DataValue appendData:[[NSString stringWithFormat:@"&token=%@",encryptToken]dataUsingEncoding:NSASCIIStringEncoding]];
    
    [DataValue appendData:[[NSString stringWithFormat:@"&flag=%@",encryptflag]dataUsingEncoding:NSASCIIStringEncoding]];
    
    [DataValue appendData:[[NSString stringWithFormat:@"&fbId=%@",encryptfbId]dataUsingEncoding:NSASCIIStringEncoding]];
    [DataValue appendData:[[NSString stringWithFormat:@"&googleId=%@",encryptgoogleId]dataUsingEncoding:NSASCIIStringEncoding]];
    
    [DataValue appendData:[[NSString stringWithFormat:@"&fname=%@",encryptfname]dataUsingEncoding:NSASCIIStringEncoding]];
    
      [DataValue appendData:[[NSString stringWithFormat:@"&lname=%@",encryptlname]dataUsingEncoding:NSASCIIStringEncoding]];
    
    [DataValue appendData:[[NSString stringWithFormat:@"&emailId=%@",encryptemailId]dataUsingEncoding:NSASCIIStringEncoding]];

     [DataValue appendData:[[NSString stringWithFormat:@"&latitude=%@",encryptLat]dataUsingEncoding:NSASCIIStringEncoding]];
    
     [DataValue appendData:[[NSString stringWithFormat:@"&longitude=%@",encryptLong]dataUsingEncoding:NSASCIIStringEncoding]];
    [DataValue appendData:[[NSString stringWithFormat:@"&timeZone=%@",encrypttimeZone]dataUsingEncoding:NSASCIIStringEncoding]];
    
    NSString *input = [NSString stringWithFormat:@"%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@",Apiversion,cipherSecretKey,Devicetype,cipherSecretKey,description,cipherSecretKey,picPath,cipherSecretKey,token,cipherSecretKey,[NSString stringWithFormat:@"%ld",(long)flag],cipherSecretKey,fbId,cipherSecretKey,googleId,cipherSecretKey,fname,cipherSecretKey,lname,cipherSecretKey,emailId,cipherSecretKey,latitude,cipherSecretKey,longitude,cipherSecretKey,timeZone,cipherSecretKey];
    
    
    NSString *output=[[NSString alloc]init];
    output = [EncryptDecryptFile md5:input];
    
    if([self connected] == true){
        
        [self showRequest:DataValue with:Devicetype with:output with:Apiversion];
        
    }else{
        
    }
    
    return jsonDict;
    
    
}

-(NSDictionary *)loginusername:(NSString *)username password:(NSString *)password token:(NSString *)token socialFlag:(NSInteger)socialFlag socialID:(NSString *)socialID socialEmailId:(NSString *)socialEmailId{
    
    token = @"123456";
    
    methodName=@"User/login";
    
    NSMutableData *DataValue = [NSMutableData data];
    
    NSString *encryptUsername=[EncryptDecryptFile EncryptIT:username];
    encryptUsername=[EncryptDecryptFile urlencode:encryptUsername];
    
    NSString *encryptpass=[EncryptDecryptFile EncryptIT:password];
    encryptpass=[EncryptDecryptFile urlencode:encryptpass];
    
    NSString *encryptToken=[EncryptDecryptFile EncryptIT:token];
    encryptToken=[EncryptDecryptFile urlencode:encryptToken];
    
    NSString *encryptSocialFlag=[EncryptDecryptFile EncryptIT:[NSString stringWithFormat:@"%ld",(long)socialFlag]];
    encryptSocialFlag=[EncryptDecryptFile urlencode:encryptSocialFlag];
    
    NSString *encryptSocialId=[EncryptDecryptFile EncryptIT:socialID];
    encryptSocialId=[EncryptDecryptFile urlencode:encryptSocialId];
    
    NSString *encryptSocialEmailId=[EncryptDecryptFile EncryptIT:socialEmailId];
    encryptSocialEmailId=[EncryptDecryptFile urlencode:encryptSocialEmailId];

    [DataValue appendData:[[NSString stringWithFormat:@"&username=%@",encryptUsername]dataUsingEncoding:NSASCIIStringEncoding]];
    
    [DataValue appendData:[[NSString stringWithFormat:@"&password=%@",encryptpass]dataUsingEncoding:NSASCIIStringEncoding]];
    
    [DataValue appendData:[[NSString stringWithFormat:@"&token=%@",encryptToken]dataUsingEncoding:NSASCIIStringEncoding]];
    
    [DataValue appendData:[[NSString stringWithFormat:@"&flag=%@",encryptSocialFlag]dataUsingEncoding:NSASCIIStringEncoding]];
    
    [DataValue appendData:[[NSString stringWithFormat:@"&socialuserId=%@",encryptSocialId]dataUsingEncoding:NSASCIIStringEncoding]];
    
    [DataValue appendData:[[NSString stringWithFormat:@"&emailId=%@",encryptSocialEmailId]dataUsingEncoding:NSASCIIStringEncoding]];

    NSString *input = [NSString stringWithFormat:@"%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@",Apiversion,cipherSecretKey,Devicetype,cipherSecretKey,username,cipherSecretKey,password,cipherSecretKey,token,cipherSecretKey,[NSString stringWithFormat:@"%d",socialFlag],cipherSecretKey,socialID,cipherSecretKey,socialEmailId,cipherSecretKey];
    
    NSString *output=[[NSString alloc]init];
    output = [EncryptDecryptFile md5:input];
    
    if([self connected] == true){
        
        [self showRequest:DataValue with:Devicetype with:output with:Apiversion];
        
    }else{
        
    }
    
    return jsonDict;
    
}

-(NSDictionary *)regisusername:(NSString *)username emailId:(NSString *)emailId password:(NSString *)password token:(NSString *)token{
    
    methodName=@"User/register";
    
    NSMutableData *DataValue = [NSMutableData data];
    
    NSString *encryptUsername=[EncryptDecryptFile EncryptIT:username];
    encryptUsername=[EncryptDecryptFile urlencode:encryptUsername];
    
    NSString *encryptEmailId=[EncryptDecryptFile EncryptIT:emailId];
    encryptEmailId=[EncryptDecryptFile urlencode:encryptEmailId];
    
    NSString *encryptpass=[EncryptDecryptFile EncryptIT:password];
    encryptpass=[EncryptDecryptFile urlencode:encryptpass];
    
    NSString *encryptToken=[EncryptDecryptFile EncryptIT:token];
    encryptToken=[EncryptDecryptFile urlencode:encryptToken];
    
    [DataValue appendData:[[NSString stringWithFormat:@"username=%@",encryptUsername]dataUsingEncoding:NSASCIIStringEncoding]];
    
    [DataValue appendData:[[NSString stringWithFormat:@"&emailId=%@",encryptEmailId]dataUsingEncoding:NSASCIIStringEncoding]];
    
    [DataValue appendData:[[NSString stringWithFormat:@"&password=%@",encryptpass]dataUsingEncoding:NSASCIIStringEncoding]];
    
    [DataValue appendData:[[NSString stringWithFormat:@"&token=%@",encryptToken]dataUsingEncoding:NSASCIIStringEncoding]];
    
    NSString *input = [NSString stringWithFormat:@"%@%@%@%@%@%@%@%@%@%@%@%@",Apiversion,cipherSecretKey,Devicetype,cipherSecretKey,username,cipherSecretKey,emailId,cipherSecretKey,password,cipherSecretKey,token,cipherSecretKey];
    
    NSString *output=[[NSString alloc]init];
    output = [EncryptDecryptFile md5:input];
    
    if([self connected] == true){
        
        [self showRequest:DataValue with:Devicetype with:output with:Apiversion];
        
    }else{
        
    }
    
    return jsonDict;
    
}

-(NSDictionary *)forgotemailId:(NSString *)emailId{
    
    methodName=@"User/forgotPassword";
    
    NSMutableData *DataValue = [NSMutableData data];
    
    NSString *encryptEmailId=[EncryptDecryptFile EncryptIT:emailId];
    encryptEmailId=[EncryptDecryptFile urlencode:encryptEmailId];
    
    [DataValue appendData:[[NSString stringWithFormat:@"emailId=%@",encryptEmailId]dataUsingEncoding:NSASCIIStringEncoding]];
    
    NSString *input = [NSString stringWithFormat:@"%@%@%@%@%@%@",Apiversion,cipherSecretKey,Devicetype,cipherSecretKey,emailId,cipherSecretKey];
    
    NSString *output=[[NSString alloc]init];
    output = [EncryptDecryptFile md5:input];
    
    if([self connected] == true){
        
        [self showRequest:DataValue with:Devicetype with:output with:Apiversion];
        
    }else{
        
    }
    
    return jsonDict;
    
}

-(NSDictionary *)resetPasswordForUserId:(NSString *)userId newPassword:(NSString *)newPassword otp:(NSString *)otp{

    methodName=@"User/updatePasswordOTP";
    
    NSMutableData *DataValue = [NSMutableData data];
    
    NSString *encryptUsername=[EncryptDecryptFile EncryptIT:userId];
    encryptUsername=[EncryptDecryptFile urlencode:encryptUsername];
    
    NSString *encryptEmailId=[EncryptDecryptFile EncryptIT:newPassword];
    encryptEmailId=[EncryptDecryptFile urlencode:encryptEmailId];
    
    NSString *encryptpass=[EncryptDecryptFile EncryptIT:otp];
    encryptpass=[EncryptDecryptFile urlencode:encryptpass];
    
    [DataValue appendData:[[NSString stringWithFormat:@"userId=%@",encryptUsername]dataUsingEncoding:NSASCIIStringEncoding]];
    
    [DataValue appendData:[[NSString stringWithFormat:@"&password=%@",encryptEmailId]dataUsingEncoding:NSASCIIStringEncoding]];
    
    [DataValue appendData:[[NSString stringWithFormat:@"&otp_password=%@",encryptpass]dataUsingEncoding:NSASCIIStringEncoding]];
    
    NSString *input = [NSString stringWithFormat:@"%@%@%@%@%@%@%@%@%@%@",Apiversion,cipherSecretKey,Devicetype,cipherSecretKey,userId,cipherSecretKey,newPassword,cipherSecretKey,otp,cipherSecretKey];
    
    NSString *output=[[NSString alloc]init];
    output = [EncryptDecryptFile md5:input];
    
    if([self connected] == true){
        
        [self showRequest:DataValue with:Devicetype with:output with:Apiversion];
        
    }else{
        
    }
    
    return jsonDict;
}



#pragma mark - todo - update for multiple images
-(NSDictionary *)createTaskuserId:(NSString *)userId categoryId:(NSString *)categoryId businessId:(NSString *)businessId paymentMethod:(NSString *)paymentMethod title:(NSString *)title description:(NSString *)description taskImgNamesArray:(NSMutableArray *)taskImgNamesArray price:(NSString *)price startDate:(NSString *)startDate startTime:(NSString *)startTime locationName:(NSString *)locationName latitude:(NSString *)latitude longitude:(NSString *)longitude taskImgArray:(NSMutableArray *)taskImgArray timeZone:(NSString *)timeZone discount:(NSString *)discount totalUsers:(NSString *)totalUsers discountDescription:(NSString *)discountDescription edit:(NSString *)edit reservationId:(NSString *)reservationId
{
    NSData *returnData;
    
    NSString *encryptUserid=[EncryptDecryptFile EncryptIT:userId];
    encryptUserid=[EncryptDecryptFile urlencode:encryptUserid];
    
    NSString *encrypCategoryId=[EncryptDecryptFile EncryptIT:categoryId];
    encrypCategoryId=[EncryptDecryptFile urlencode:encrypCategoryId];
    
    
    NSString *encrypBusinessId=[EncryptDecryptFile EncryptIT:businessId];
    encrypBusinessId=[EncryptDecryptFile urlencode:encrypBusinessId];
    
    NSString *encryptTitle=[EncryptDecryptFile EncryptIT:title];
    encryptTitle=[EncryptDecryptFile urlencode:encryptTitle];
    
    NSString *encryptDescription=[EncryptDecryptFile EncryptIT:description];
    encryptDescription=[EncryptDecryptFile urlencode:encryptDescription];
    
    NSString *encryptPrice=[EncryptDecryptFile EncryptIT:price];
    encryptPrice=[EncryptDecryptFile urlencode:encryptPrice];
    
    NSString *encryptStartDate=[EncryptDecryptFile EncryptIT:startDate];
    encryptStartDate=[EncryptDecryptFile urlencode:encryptStartDate];
    
    NSString *encryptStartTime=[EncryptDecryptFile EncryptIT:startTime];
    encryptStartTime=[EncryptDecryptFile urlencode:encryptStartTime];
    
    
    NSString *encryptPaymentMethod=[EncryptDecryptFile EncryptIT:paymentMethod];
    encryptPaymentMethod=[EncryptDecryptFile urlencode:encryptPaymentMethod];
    
    NSString *encryptLocationName=[EncryptDecryptFile EncryptIT:locationName];
    encryptLocationName=[EncryptDecryptFile urlencode:encryptLocationName];
    
    NSString *encryptLatitude=[EncryptDecryptFile EncryptIT:latitude];
    encryptLatitude=[EncryptDecryptFile urlencode:encryptLatitude];
    
    NSString *encryptLongitude=[EncryptDecryptFile EncryptIT:longitude];
    encryptLongitude=[EncryptDecryptFile urlencode:encryptLongitude];
    
    NSString *encrypttimeZone=[EncryptDecryptFile EncryptIT:timeZone];
    encrypttimeZone=[EncryptDecryptFile urlencode:encrypttimeZone];
    
    NSString *encryptdiscount=[EncryptDecryptFile EncryptIT:discount];
    encryptdiscount=[EncryptDecryptFile urlencode:encryptdiscount];
    
    NSString *encrypttotalUsers=[EncryptDecryptFile EncryptIT:totalUsers];
    encrypttotalUsers=[EncryptDecryptFile urlencode:encrypttotalUsers];
    
    NSString *encryptdiscountDescription=[EncryptDecryptFile EncryptIT:discountDescription];
    encryptdiscountDescription=[EncryptDecryptFile urlencode:encryptdiscountDescription];
    
    NSString *encryptedit=[EncryptDecryptFile EncryptIT:edit];
    encryptedit=[EncryptDecryptFile urlencode:encryptedit];
    
    NSString *encryptreservationId=[EncryptDecryptFile EncryptIT:reservationId];
    encryptreservationId=[EncryptDecryptFile urlencode:encryptreservationId];
    
    NSString *postString = [NSString stringWithFormat:@"[{\"userId\":\"%@\",\"categoryId\":\"%@\",\"title\":\"%@\",\"description\":\"%@\",\"price\":\"%@\",\"startDate\":\"%@\",\"startTime\":\"%@\",\"locationName\":\"%@\",\"latitude\":\"%@\",\"longitude\":\"%@\",\"paymentMethod\":\"%@\",\"businessId\":\"%@\",\"timeZone\":\"%@\",\"discount\":\"%@\",\"totalUsers\":\"%@\",\"discountDescription\":\"%@\",\"edit\":\"%@\",\"reservationId\":\"%@\"}]",encryptUserid,encrypCategoryId,encryptTitle,encryptDescription,encryptPrice,encryptStartDate,encryptStartTime,encryptLocationName,encryptLatitude,encryptLongitude,encryptPaymentMethod,encrypBusinessId,encrypttimeZone,encryptdiscount,encrypttotalUsers,encryptdiscountDescription,encryptedit,encryptreservationId];
    
    //    NSString *postString = [NSString stringWithFormat:@"[{\"userid\":\"%@\",\"subcategoryId\":\"%@\",\"title\":\"%@\",\"description\":\"%@\",\"price\":\"%@\",\"startDate\":\"%@\",\"endDate\":\"%@\",\"locationName\":\"%@\",\"latitude\":\"%@\",\"longitude\":\"%@\"}]",userid,subcategoryId,title,description,price,startDate,endDate,locationName,latitude,longitude];
    
    //    postString = [EncryptDecryptFile EncryptIT:postString];
    //    postString = [EncryptDecryptFile urlencode:postString];
    
    NSMutableData *mutableData = [NSMutableData data];
    NSString *stringBoundary = @"*****";
    NSString *headerBoundary = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",stringBoundary];
    
    [mutableData appendData:[[NSString stringWithFormat:@"--%@\r\n",stringBoundary] dataUsingEncoding:NSASCIIStringEncoding]];
    
    [mutableData appendData:[@"Content-Disposition: form-data; name=\"data\"\r\n\r\n" dataUsingEncoding:NSASCIIStringEncoding]];
    
    [mutableData appendData:[postString dataUsingEncoding:NSASCIIStringEncoding]];
    
    [mutableData appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",stringBoundary] dataUsingEncoding:NSASCIIStringEncoding]];
    
    [mutableData appendData:[[NSString stringWithFormat:@"--%@\r\n",stringBoundary] dataUsingEncoding:NSASCIIStringEncoding]];
    
    NSInteger count = 0;
    
    for (UIImage *img in taskImgArray) {
        
        [mutableData appendData:[[NSString stringWithFormat:@"--%@\r\n",stringBoundary] dataUsingEncoding:NSASCIIStringEncoding]];
        
        if (count == 0) {
            [mutableData appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"reservationImg\"; filename=\"%@\"\r\n",[taskImgNamesArray objectAtIndex:count]] dataUsingEncoding:NSASCIIStringEncoding]];\
        }else{
            [mutableData appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"reservationImg%ld\"; filename=\"%@\"\r\n",(long)count,[taskImgNamesArray objectAtIndex:count]] dataUsingEncoding:NSASCIIStringEncoding]];
        }
        //append image 2 data
        [mutableData appendData:[@"Content-Type: image/jpeg\r\n\r\n" dataUsingEncoding:NSASCIIStringEncoding]];
        
        [mutableData appendData:UIImageJPEGRepresentation(img,1.0)];
        
        [mutableData appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",stringBoundary] dataUsingEncoding:NSASCIIStringEncoding]];
        [mutableData appendData:[[NSString stringWithFormat:@"--%@\r\n",stringBoundary] dataUsingEncoding:NSASCIIStringEncoding]];
        
        count = count + 1;
        
    }
    
    NSURL *url = [NSURL URLWithString:urlServerCreateTask];
    
    NSMutableURLRequest *request=[[NSMutableURLRequest alloc]init];
    
    NSString *input = [NSString stringWithFormat:@"%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@",Apiversion,cipherSecretKey,Devicetype,cipherSecretKey,userId,cipherSecretKey,categoryId,cipherSecretKey,title,cipherSecretKey,description,cipherSecretKey,price,cipherSecretKey,startDate,cipherSecretKey,startTime,cipherSecretKey,locationName,cipherSecretKey,latitude,cipherSecretKey,longitude,cipherSecretKey,paymentMethod,cipherSecretKey,businessId,cipherSecretKey,timeZone,cipherSecretKey,discount,cipherSecretKey,totalUsers,cipherSecretKey,discountDescription,cipherSecretKey,edit,cipherSecretKey,reservationId,cipherSecretKey];
    
    
    
    NSString *output;
    
    output = [EncryptDecryptFile md5:input];
    
    [request addValue:@"ios" forHTTPHeaderField:@"Devicetype"];
    [request addValue:@"1.0" forHTTPHeaderField:@"Apiversion"];
    [request setValue:output forHTTPHeaderField:@"Cipher"];
    
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:mutableData];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[mutableData length]];
    [request setValue:headerBoundary forHTTPHeaderField:@"Content-Type"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setTimeoutInterval:60.0];
    
    if([self connected] == true){
        
        
        
        
        
        returnData = [ NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
        NSLog(@"%@",returnData);
        
        if (returnData) {
            
            NSString*str=[[NSString alloc]initWithData:returnData encoding:NSUTF8StringEncoding];
            NSLog(@"return string--- %@",str);
            jsonDict = [NSJSONSerialization JSONObjectWithData:returnData
                                                       options:NSJSONReadingMutableContainers
                                                         error:nil];
            NSLog(@"value of dict---- %@",jsonDict);
            
        }
        
    }else{
        
    }
    
    return jsonDict;
    
    
}




-(NSDictionary *)updateBankProfileuserId:(NSString *)userId name:(NSString *)name accountNo:(NSString *)accountNo routingNo:(NSString *)routingNo bankName:(NSString *)bankName accType:(NSString *)accType ip:(NSString *)ip dob:(NSString *)dob ssn:(NSString *)ssn type:(NSString *)type address:(NSString *)address taxId:(NSString *)taxId andUserImage:(UIImage *)image andImageName:(NSString *)ImageName
{
    methodName=@"Stripe/addBank";
    
    NSMutableData *DataValue = [NSMutableData data];
    
    NSString *encryptuserId=[EncryptDecryptFile EncryptIT:userId];
    encryptuserId=[EncryptDecryptFile urlencode:encryptuserId];
    
    NSString *encryptname=[EncryptDecryptFile EncryptIT:name];
    encryptname=[EncryptDecryptFile urlencode:encryptname];
    
    NSString *encryptaccountNo=[EncryptDecryptFile EncryptIT:accountNo];
    encryptaccountNo=[EncryptDecryptFile urlencode:encryptaccountNo];
    
    NSString *encryptroutingNo=[EncryptDecryptFile EncryptIT:routingNo];
    encryptroutingNo=[EncryptDecryptFile urlencode:encryptroutingNo];
    
    NSString *encryptbankName=[EncryptDecryptFile EncryptIT:bankName];
    encryptbankName=[EncryptDecryptFile urlencode:encryptbankName];
    
    NSString *encryptaccType=[EncryptDecryptFile EncryptIT:accType];
    encryptaccType=[EncryptDecryptFile urlencode:encryptaccType];
    
    NSString *encryptIp=[EncryptDecryptFile EncryptIT:ip];
    encryptIp=[EncryptDecryptFile urlencode:encryptIp];
    
    NSString *encryptDOB=[EncryptDecryptFile EncryptIT:dob];
    encryptDOB=[EncryptDecryptFile urlencode:encryptDOB];
    
    NSString *encryptSSN=[EncryptDecryptFile EncryptIT:ssn];
    encryptSSN=[EncryptDecryptFile urlencode:encryptSSN];
    
    NSString *encryptType=[EncryptDecryptFile EncryptIT:type];
    encryptType=[EncryptDecryptFile urlencode:encryptType];
    
    NSString *encryptAdress=[EncryptDecryptFile EncryptIT:address];
    encryptAdress=[EncryptDecryptFile urlencode:encryptAdress];
    
    NSString *encryptTaxId=[EncryptDecryptFile EncryptIT:taxId];
    encryptTaxId=[EncryptDecryptFile urlencode:encryptTaxId];
    
    
    NSString *postString = [NSString stringWithFormat:@"[{\"userId\":\"%@\",\"name\":\"%@\",\"accountNo\":\"%@\",\"routingNo\":\"%@\",\"bankName\":\"%@\",\"accType\":\"%@\",\"ip\":\"%@\",\"dob\":\"%@\",\"ssn\":\"%@\",\"type\":\"%@\",\"address\":\"%@\",\"taxId\":\"%@\"}]",encryptuserId,encryptname,encryptaccountNo,encryptroutingNo,encryptbankName,encryptaccType,encryptIp,encryptDOB,encryptSSN,encryptType,encryptAdress,encryptTaxId];
    
    NSMutableData *mutableData = [NSMutableData data];
    NSString *stringBoundary = @"*****";
    NSString *headerBoundary = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",stringBoundary];
    
    [mutableData appendData:[[NSString stringWithFormat:@"--%@\r\n",stringBoundary] dataUsingEncoding:NSASCIIStringEncoding]];
    
    [mutableData appendData:[@"Content-Disposition: form-data; name=\"data\"\r\n\r\n" dataUsingEncoding:NSASCIIStringEncoding]];
    
    [mutableData appendData:[postString dataUsingEncoding:NSASCIIStringEncoding]];
    
    [mutableData appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",stringBoundary] dataUsingEncoding:NSASCIIStringEncoding]];
    
    [mutableData appendData:[[NSString stringWithFormat:@"--%@\r\n",stringBoundary] dataUsingEncoding:NSASCIIStringEncoding]];
    
    if (image) {
        
        //append image 2 data
        [mutableData appendData:[[NSString stringWithFormat:@"--%@\r\n",stringBoundary] dataUsingEncoding:NSASCIIStringEncoding]];
        [mutableData appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"legal_id\"; filename=\"%@\"\r\n",ImageName] dataUsingEncoding:NSASCIIStringEncoding]];
        [mutableData appendData:[@"Content-Type: image/jpeg\r\n\r\n" dataUsingEncoding:NSASCIIStringEncoding]];
        
        [mutableData appendData:UIImageJPEGRepresentation(image,1.0)];
        
        [mutableData appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",stringBoundary] dataUsingEncoding:NSASCIIStringEncoding]];
        [mutableData appendData:[[NSString stringWithFormat:@"--%@\r\n",stringBoundary] dataUsingEncoding:NSASCIIStringEncoding]];
        
    }
    
    NSString *input = [NSString stringWithFormat:@"%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@",Apiversion,cipherSecretKey,Devicetype,cipherSecretKey,userId,cipherSecretKey,name,cipherSecretKey,accountNo,cipherSecretKey,routingNo,cipherSecretKey,bankName,cipherSecretKey,accType,cipherSecretKey,ip,cipherSecretKey,dob,cipherSecretKey,ssn,cipherSecretKey,type,cipherSecretKey,address,cipherSecretKey,taxId,cipherSecretKey];
    
    NSString *output=[[NSString alloc]init];
    output = [EncryptDecryptFile md5:input];
    
    if([self connected] == true){
        
        NSData *returnData;
        
        NSURL *url1 = [NSURL URLWithString:[NSString stringWithFormat:urlServer,methodName]];
        
        NSLog(@"value of url %@",url1);
        
        NSMutableURLRequest *request=[[NSMutableURLRequest alloc]init];
        
        [request addValue:@"ios" forHTTPHeaderField:@"Devicetype"];
        [request addValue:@"1.0" forHTTPHeaderField:@"Apiversion"];
        [request setValue:output forHTTPHeaderField:@"Cipher"];
        
        [request setURL:url1];
        [request setHTTPMethod:@"POST"];
        [request setHTTPBody:mutableData];
        NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[mutableData length]];
        [request setValue:headerBoundary forHTTPHeaderField:@"Content-Type"];
        [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
        [request setTimeoutInterval:60.0];
        
        //NSLog(@"request data------ %@",body);
        returnData = [ NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
        if (returnData) {
            
            NSString*str=[[NSString alloc]initWithData:returnData encoding:NSUTF8StringEncoding];
            NSLog(@"return string--- %@",str);
            jsonDict = [NSJSONSerialization JSONObjectWithData:returnData
                                                       options:NSJSONReadingMutableContainers
                                                         error:nil];
            NSLog(@"value of dict---- %@",jsonDict);
            
        }
        
        return jsonDict;
        
    }else{
        
    }
    
    return jsonDict;
    
}



-(NSDictionary *)updateVendorProfileuserId:(NSString *)userId name:(NSString *)name andUserImage:(UIImage *)image andImageName:(NSString *)ImageName instaLink:(NSString *)instaLink yelpProfile:(NSString *)yelpProfile address:(NSString *)address services:(NSString *)services categoryId:(NSString*)categoryId
{
    methodName=@"User/add_vendor";
    
    NSString *encryptUserid=[EncryptDecryptFile EncryptIT:userId];
    encryptUserid=[EncryptDecryptFile urlencode:encryptUserid];
    
    NSString *encryptUsername=[EncryptDecryptFile EncryptIT:name];
    encryptUsername=[EncryptDecryptFile urlencode:encryptUsername];
    
    NSString *encryptyelpProfile=[EncryptDecryptFile EncryptIT:yelpProfile];
    encryptyelpProfile=[EncryptDecryptFile urlencode:encryptyelpProfile];
    
    NSString *encryptaddress=[EncryptDecryptFile EncryptIT:address];
    encryptaddress=[EncryptDecryptFile urlencode:encryptaddress];
    
    NSString *encryptinstaLink=[EncryptDecryptFile EncryptIT:instaLink];
    encryptinstaLink=[EncryptDecryptFile urlencode:encryptinstaLink];
    
    NSString *encryptservices=[EncryptDecryptFile EncryptIT:services];
    encryptservices=[EncryptDecryptFile urlencode:encryptservices];
    
    NSString *encryptcategoryId = [EncryptDecryptFile EncryptIT:categoryId];
    encryptcategoryId = [EncryptDecryptFile urlencode:encryptcategoryId];
    
    
    NSString *postString = [NSString stringWithFormat:@"[{\"userId\":\"%@\",\"name\":\"%@\",\"yelpProfile\":\"%@\",\"address\":\"%@\",\"services\":\"%@\",\"instaLink\":\"%@\",\"categoryId\":\"%@\"}]", encryptUserid,encryptUsername,encryptyelpProfile,encryptaddress,encryptservices,encryptinstaLink,encryptcategoryId];
    
    
    NSMutableData *mutableData = [NSMutableData data];
    NSString *stringBoundary = @"*****";
    NSString *headerBoundary = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",stringBoundary];
    
    [mutableData appendData:[[NSString stringWithFormat:@"--%@\r\n",stringBoundary] dataUsingEncoding:NSASCIIStringEncoding]];
    
    [mutableData appendData:[@"Content-Disposition: form-data; name=\"data\"\r\n\r\n" dataUsingEncoding:NSASCIIStringEncoding]];
    
    [mutableData appendData:[postString dataUsingEncoding:NSASCIIStringEncoding]];
    
    [mutableData appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",stringBoundary] dataUsingEncoding:NSASCIIStringEncoding]];
    
    [mutableData appendData:[[NSString stringWithFormat:@"--%@\r\n",stringBoundary] dataUsingEncoding:NSASCIIStringEncoding]];
    
    if (image) {
        
        //append image 2 data
        [mutableData appendData:[[NSString stringWithFormat:@"--%@\r\n",stringBoundary] dataUsingEncoding:NSASCIIStringEncoding]];
        [mutableData appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"picPath\"; filename=\"%@\"\r\n",ImageName] dataUsingEncoding:NSASCIIStringEncoding]];
        [mutableData appendData:[@"Content-Type: image/jpeg\r\n\r\n" dataUsingEncoding:NSASCIIStringEncoding]];
        
        [mutableData appendData:UIImageJPEGRepresentation(image,1.0)];
        
        [mutableData appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",stringBoundary] dataUsingEncoding:NSASCIIStringEncoding]];
        [mutableData appendData:[[NSString stringWithFormat:@"--%@\r\n",stringBoundary] dataUsingEncoding:NSASCIIStringEncoding]];
        
    }
    
    
    
    NSString *input = [NSString stringWithFormat:@"%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@",Apiversion,cipherSecretKey,Devicetype,cipherSecretKey,userId,cipherSecretKey,name,cipherSecretKey,yelpProfile,cipherSecretKey,address,cipherSecretKey,services,cipherSecretKey,instaLink,cipherSecretKey,categoryId,cipherSecretKey];
    
    NSString *output=[[NSString alloc]init];
    output = [EncryptDecryptFile md5:input];
    
    if([self connected] == true){
        
        NSData *returnData;
        
        NSURL *url1 = [NSURL URLWithString:[NSString stringWithFormat:urlServer,methodName]];
        
        NSLog(@"value of url %@",url1);
        
        NSMutableURLRequest *request=[[NSMutableURLRequest alloc]init];
        
        [request addValue:@"ios" forHTTPHeaderField:@"Devicetype"];
        [request addValue:@"1.0" forHTTPHeaderField:@"Apiversion"];
        [request setValue:output forHTTPHeaderField:@"Cipher"];
        
        [request setURL:url1];
        [request setHTTPMethod:@"POST"];
        [request setHTTPBody:mutableData];
        NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[mutableData length]];
        [request setValue:headerBoundary forHTTPHeaderField:@"Content-Type"];
        [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
        [request setTimeoutInterval:60.0];
        
        //NSLog(@"request data------ %@",body);
        returnData = [ NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
        if (returnData) {
            
            NSString*str=[[NSString alloc]initWithData:returnData encoding:NSUTF8StringEncoding];
            NSLog(@"return string--- %@",str);
            jsonDict = [NSJSONSerialization JSONObjectWithData:returnData
                                                       options:NSJSONReadingMutableContainers
                                                         error:nil];
            NSLog(@"value of dict---- %@",jsonDict);
            
        }
        
        return jsonDict;
        
    }else{
        
    }
    
    return jsonDict;
    
}

//-(NSDictionary *)updateVendorProfileuserId:(NSString *)userId name:(NSString *)name andUserImage:(UIImage *)image andImageName:(NSString *)ImageName instaLink:(NSString *)instaLink yelpProfile:(NSString *)yelpProfile address:(NSString *)address services:(NSString *)services
//{
//    methodName=@"User/add_vendor";
//
//    NSString *encryptUserid=[EncryptDecryptFile EncryptIT:userId];
//    encryptUserid=[EncryptDecryptFile urlencode:encryptUserid];
//
//    NSString *encryptUsername=[EncryptDecryptFile EncryptIT:name];
//    encryptUsername=[EncryptDecryptFile urlencode:encryptUsername];
//
//    NSString *encryptyelpProfile=[EncryptDecryptFile EncryptIT:yelpProfile];
//    encryptyelpProfile=[EncryptDecryptFile urlencode:encryptyelpProfile];
//
//    NSString *encryptaddress=[EncryptDecryptFile EncryptIT:address];
//    encryptaddress=[EncryptDecryptFile urlencode:encryptaddress];
//
//    NSString *encryptinstaLink=[EncryptDecryptFile EncryptIT:instaLink];
//    encryptinstaLink=[EncryptDecryptFile urlencode:encryptinstaLink];
//
//    NSString *encryptservices=[EncryptDecryptFile EncryptIT:services];
//    encryptservices=[EncryptDecryptFile urlencode:encryptservices];
//
//    NSString *postString = [NSString stringWithFormat:@"[{\"userId\":\"%@\",\"name\":\"%@\",\"yelpProfile\":\"%@\",\"address\":\"%@\",\"services\":\"%@\",\"instaLink\":\"%@\"}]",encryptUserid,encryptUsername,encryptyelpProfile,encryptaddress,encryptservices,encryptinstaLink];
//
//
//    NSMutableData *mutableData = [NSMutableData data];
//    NSString *stringBoundary = @"*****";
//    NSString *headerBoundary = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",stringBoundary];
//
//    [mutableData appendData:[[NSString stringWithFormat:@"--%@\r\n",stringBoundary] dataUsingEncoding:NSASCIIStringEncoding]];
//
//    [mutableData appendData:[@"Content-Disposition: form-data; name=\"data\"\r\n\r\n" dataUsingEncoding:NSASCIIStringEncoding]];
//
//    [mutableData appendData:[postString dataUsingEncoding:NSASCIIStringEncoding]];
//
//    [mutableData appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",stringBoundary] dataUsingEncoding:NSASCIIStringEncoding]];
//
//    [mutableData appendData:[[NSString stringWithFormat:@"--%@\r\n",stringBoundary] dataUsingEncoding:NSASCIIStringEncoding]];
//
//    if (image) {
//
//        //append image 2 data
//        [mutableData appendData:[[NSString stringWithFormat:@"--%@\r\n",stringBoundary] dataUsingEncoding:NSASCIIStringEncoding]];
//        [mutableData appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"picPath\"; filename=\"%@\"\r\n",ImageName] dataUsingEncoding:NSASCIIStringEncoding]];
//        [mutableData appendData:[@"Content-Type: image/jpeg\r\n\r\n" dataUsingEncoding:NSASCIIStringEncoding]];
//
//        [mutableData appendData:UIImageJPEGRepresentation(image,1.0)];
//
//        [mutableData appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",stringBoundary] dataUsingEncoding:NSASCIIStringEncoding]];
//        [mutableData appendData:[[NSString stringWithFormat:@"--%@\r\n",stringBoundary] dataUsingEncoding:NSASCIIStringEncoding]];
//
//    }
//
//    NSString *input = [NSString stringWithFormat:@"%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@",Apiversion,cipherSecretKey,Devicetype,cipherSecretKey,userId,cipherSecretKey,name,cipherSecretKey,yelpProfile,cipherSecretKey,address,cipherSecretKey,services,cipherSecretKey,instaLink,cipherSecretKey];
//
//    NSString *output=[[NSString alloc]init];
//    output = [EncryptDecryptFile md5:input];
//
//    if([self connected] == true){
//
//        NSData *returnData;
//
//        NSURL *url1 = [NSURL URLWithString:[NSString stringWithFormat:urlServer,methodName]];
//
//        NSLog(@"value of url %@",url1);
//
//        NSMutableURLRequest *request=[[NSMutableURLRequest alloc]init];
//
//        [request addValue:@"ios" forHTTPHeaderField:@"Devicetype"];
//        [request addValue:@"1.0" forHTTPHeaderField:@"Apiversion"];
//        [request setValue:output forHTTPHeaderField:@"Cipher"];
//
//        [request setURL:url1];
//        [request setHTTPMethod:@"POST"];
//        [request setHTTPBody:mutableData];
//        NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[mutableData length]];
//        [request setValue:headerBoundary forHTTPHeaderField:@"Content-Type"];
//        [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
//        [request setTimeoutInterval:60.0];
//
//        //NSLog(@"request data------ %@",body);
//        returnData = [ NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
//        if (returnData) {
//
//            NSString*str=[[NSString alloc]initWithData:returnData encoding:NSUTF8StringEncoding];
//            NSLog(@"return string--- %@",str);
//            jsonDict = [NSJSONSerialization JSONObjectWithData:returnData
//                                                       options:NSJSONReadingMutableContainers
//                                                         error:nil];
//            NSLog(@"value of dict---- %@",jsonDict);
//
//        }
//
//        return jsonDict;
//
//    }else{
//
//    }
//
//    return jsonDict;
//
//}

-(NSDictionary *)updateProfileuserId:(NSString *)userId name:(NSString *)name description:(NSString *)description latitude:(NSString *)latitude longitude:(NSString *)longitude andUserImage:(UIImage *)image andImageName:(NSString *)ImageName instaLink:(NSString *)instaLink location:(NSString *)location{
    
    methodName=@"User/update_Profile";
    
    NSString *encryptUserid=[EncryptDecryptFile EncryptIT:userId];
    encryptUserid=[EncryptDecryptFile urlencode:encryptUserid];
    
    NSString *encryptUsername=[EncryptDecryptFile EncryptIT:name];
    encryptUsername=[EncryptDecryptFile urlencode:encryptUsername];
   
    
    NSString *encryptDescription=[EncryptDecryptFile EncryptIT:description];
    encryptDescription=[EncryptDecryptFile urlencode:encryptDescription];
    
    NSString *encryptLatitude=[EncryptDecryptFile EncryptIT:latitude];
    encryptLatitude=[EncryptDecryptFile urlencode:encryptLatitude];
    
    NSString *encryptLongitude=[EncryptDecryptFile EncryptIT:longitude];
    encryptLongitude=[EncryptDecryptFile urlencode:encryptLongitude];
    
    NSString *encryptinstaLink=[EncryptDecryptFile EncryptIT:instaLink];
    encryptinstaLink=[EncryptDecryptFile urlencode:encryptinstaLink];
    
    NSString *encryptLocation=[EncryptDecryptFile EncryptIT:location];
    encryptLocation=[EncryptDecryptFile urlencode:encryptLocation];
    
    NSString *postString = [NSString stringWithFormat:@"[{\"userId\":\"%@\",\"name\":\"%@\",\"description\":\"%@\",\"latitude\":\"%@\",\"longitude\":\"%@\",\"instaLink\":\"%@\",\"location\":\"%@\"}]",encryptUserid,encryptUsername,encryptDescription,encryptLatitude,encryptLongitude,encryptinstaLink,encryptLocation];
    
    NSMutableData *mutableData = [NSMutableData data];
    NSString *stringBoundary = @"*****";
    NSString *headerBoundary = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",stringBoundary];
    
    [mutableData appendData:[[NSString stringWithFormat:@"--%@\r\n",stringBoundary] dataUsingEncoding:NSASCIIStringEncoding]];
    
    [mutableData appendData:[@"Content-Disposition: form-data; name=\"data\"\r\n\r\n" dataUsingEncoding:NSASCIIStringEncoding]];
    
    [mutableData appendData:[postString dataUsingEncoding:NSASCIIStringEncoding]];
    
    [mutableData appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",stringBoundary] dataUsingEncoding:NSASCIIStringEncoding]];
    
    [mutableData appendData:[[NSString stringWithFormat:@"--%@\r\n",stringBoundary] dataUsingEncoding:NSASCIIStringEncoding]];
    
    if (image) {
        
        //append image 2 data
        [mutableData appendData:[[NSString stringWithFormat:@"--%@\r\n",stringBoundary] dataUsingEncoding:NSASCIIStringEncoding]];
        [mutableData appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"picPath\"; filename=\"%@\"\r\n",ImageName] dataUsingEncoding:NSASCIIStringEncoding]];
        [mutableData appendData:[@"Content-Type: image/jpeg\r\n\r\n" dataUsingEncoding:NSASCIIStringEncoding]];
        
        [mutableData appendData:UIImageJPEGRepresentation(image,1.0)];
        
        [mutableData appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",stringBoundary] dataUsingEncoding:NSASCIIStringEncoding]];
        [mutableData appendData:[[NSString stringWithFormat:@"--%@\r\n",stringBoundary] dataUsingEncoding:NSASCIIStringEncoding]];

    }
    
    NSString *input = [NSString stringWithFormat:@"%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@",Apiversion,cipherSecretKey,Devicetype,cipherSecretKey,userId,cipherSecretKey,name,cipherSecretKey,description,cipherSecretKey,latitude,cipherSecretKey,longitude,cipherSecretKey,instaLink,cipherSecretKey,location,cipherSecretKey];
    
    NSString *output=[[NSString alloc]init];
    output = [EncryptDecryptFile md5:input];
    
    if([self connected] == true){
        
        NSData *returnData;

        NSURL *url1 = [NSURL URLWithString:[NSString stringWithFormat:urlServer,methodName]];
        
        NSLog(@"value of url %@",url1);
        
        NSMutableURLRequest *request=[[NSMutableURLRequest alloc]init];
        
        [request addValue:@"ios" forHTTPHeaderField:@"Devicetype"];
        [request addValue:@"1.0" forHTTPHeaderField:@"Apiversion"];
        [request setValue:output forHTTPHeaderField:@"Cipher"];
        
        [request setURL:url1];
        [request setHTTPMethod:@"POST"];
        [request setHTTPBody:mutableData];
        NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[mutableData length]];
        [request setValue:headerBoundary forHTTPHeaderField:@"Content-Type"];
        [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
        [request setTimeoutInterval:60.0];

        //NSLog(@"request data------ %@",body);
        returnData = [ NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
        if (returnData) {
            
            NSString*str=[[NSString alloc]initWithData:returnData encoding:NSUTF8StringEncoding];
            NSLog(@"return string--- %@",str);
            jsonDict = [NSJSONSerialization JSONObjectWithData:returnData
                                                       options:NSJSONReadingMutableContainers
                                                         error:nil];
            NSLog(@"value of dict---- %@",jsonDict);

        }

        return jsonDict;
        
    }else{
        
    }
    
    return jsonDict;
    
}


-(NSDictionary *)newUpdateProfileuserId:(NSString *)userId name:(NSString *)name description:(NSString *)description latitude:(NSString *)latitude longitude:(NSString *)longitude andUserImage:(UIImage *)image andImageName:(NSString *)ImageName instaLink:(NSString *)instaLink location:(NSString *)location mobile:(NSString *)mobile
{
    methodName=@"User/update_Profile";
    
    NSString *encryptUserid=[EncryptDecryptFile EncryptIT:userId];
    encryptUserid=[EncryptDecryptFile urlencode:encryptUserid];
    
    NSString *encryptUsername=[EncryptDecryptFile EncryptIT:name];
    encryptUsername=[EncryptDecryptFile urlencode:encryptUsername];
    
    
    NSString *encryptDescription=[EncryptDecryptFile EncryptIT:description];
    encryptDescription=[EncryptDecryptFile urlencode:encryptDescription];
    
    NSString *encryptLatitude=[EncryptDecryptFile EncryptIT:latitude];
    encryptLatitude=[EncryptDecryptFile urlencode:encryptLatitude];
    
    NSString *encryptLongitude=[EncryptDecryptFile EncryptIT:longitude];
    encryptLongitude=[EncryptDecryptFile urlencode:encryptLongitude];
    
    NSString *encryptinstaLink=[EncryptDecryptFile EncryptIT:instaLink];
    encryptinstaLink=[EncryptDecryptFile urlencode:encryptinstaLink];
    
    NSString *encryptLocation=[EncryptDecryptFile EncryptIT:location];
    encryptLocation=[EncryptDecryptFile urlencode:encryptLocation];
    
    
    NSString *encryptMobile=[EncryptDecryptFile EncryptIT:mobile];
    encryptMobile=[EncryptDecryptFile urlencode:encryptMobile];

    
    
    
    
    NSString *postString = [NSString stringWithFormat:@"[{\"userId\":\"%@\",\"name\":\"%@\",\"description\":\"%@\",\"latitude\":\"%@\",\"longitude\":\"%@\",\"instaLink\":\"%@\",\"location\":\"%@\",\"mobile\":\"%@\"}]",encryptUserid,encryptUsername,encryptDescription,encryptLatitude,encryptLongitude,encryptinstaLink,encryptLocation,encryptMobile];
    
    NSMutableData *mutableData = [NSMutableData data];
    NSString *stringBoundary = @"*****";
    NSString *headerBoundary = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",stringBoundary];
    
    [mutableData appendData:[[NSString stringWithFormat:@"--%@\r\n",stringBoundary] dataUsingEncoding:NSASCIIStringEncoding]];
    
    [mutableData appendData:[@"Content-Disposition: form-data; name=\"data\"\r\n\r\n" dataUsingEncoding:NSASCIIStringEncoding]];
    
    [mutableData appendData:[postString dataUsingEncoding:NSASCIIStringEncoding]];
    
    [mutableData appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",stringBoundary] dataUsingEncoding:NSASCIIStringEncoding]];
    
    [mutableData appendData:[[NSString stringWithFormat:@"--%@\r\n",stringBoundary] dataUsingEncoding:NSASCIIStringEncoding]];
    
    if (image) {
        
        //append image 2 data
        [mutableData appendData:[[NSString stringWithFormat:@"--%@\r\n",stringBoundary] dataUsingEncoding:NSASCIIStringEncoding]];
        [mutableData appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"picPath\"; filename=\"%@\"\r\n",ImageName] dataUsingEncoding:NSASCIIStringEncoding]];
        [mutableData appendData:[@"Content-Type: image/jpeg\r\n\r\n" dataUsingEncoding:NSASCIIStringEncoding]];
        
        [mutableData appendData:UIImageJPEGRepresentation(image,1.0)];
        
        [mutableData appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",stringBoundary] dataUsingEncoding:NSASCIIStringEncoding]];
        [mutableData appendData:[[NSString stringWithFormat:@"--%@\r\n",stringBoundary] dataUsingEncoding:NSASCIIStringEncoding]];
        
    }
    
    NSString *input = [NSString stringWithFormat:@"%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@",Apiversion,cipherSecretKey,Devicetype,cipherSecretKey,userId,cipherSecretKey,name,cipherSecretKey,description,cipherSecretKey,latitude,cipherSecretKey,longitude,cipherSecretKey,instaLink,cipherSecretKey,location,cipherSecretKey,mobile,cipherSecretKey];
    
    NSString *output=[[NSString alloc]init];
    output = [EncryptDecryptFile md5:input];
    
    if([self connected] == true){
        
        NSData *returnData;
        
        NSURL *url1 = [NSURL URLWithString:[NSString stringWithFormat:urlServer,methodName]];
        
        NSLog(@"value of url %@",url1);
        
        NSMutableURLRequest *request=[[NSMutableURLRequest alloc]init];
        
        [request addValue:@"ios" forHTTPHeaderField:@"Devicetype"];
        [request addValue:@"1.0" forHTTPHeaderField:@"Apiversion"];
        [request setValue:output forHTTPHeaderField:@"Cipher"];
        
        [request setURL:url1];
        [request setHTTPMethod:@"POST"];
        [request setHTTPBody:mutableData];
        NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[mutableData length]];
        [request setValue:headerBoundary forHTTPHeaderField:@"Content-Type"];
        [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
        [request setTimeoutInterval:60.0];
        
        //NSLog(@"request data------ %@",body);
        returnData = [ NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
        if (returnData) {
            
            NSString*str=[[NSString alloc]initWithData:returnData encoding:NSUTF8StringEncoding];
            NSLog(@"return string--- %@",str);
            jsonDict = [NSJSONSerialization JSONObjectWithData:returnData
                                                       options:NSJSONReadingMutableContainers
                                                         error:nil];
            NSLog(@"value of dict---- %@",jsonDict);
            
        }
        
        return jsonDict;
        
    }else{
        
    }
    
    return jsonDict;
    
}
-(NSDictionary *)requestReservationUserId:(NSString *)userId timeZone:(NSString*)timeZone title:(NSString *)title locationName:(NSString *)locationName businessName:(NSString *)businessName startDate:(NSString *)startDate startTime:(NSString *)startTime description:(NSString *)description categoryId:(NSString *)categoryId price:(NSString *)price name:(NSString *)name mobile:(NSString *)mobile
{
     methodName= @"Reservation_Info/request_reservation";
    
    
    NSString *encryptUserid=[EncryptDecryptFile EncryptIT:userId];
    encryptUserid=[EncryptDecryptFile urlencode:encryptUserid];
    
    NSString *encrypttimeZone=[EncryptDecryptFile EncryptIT:timeZone];
    encrypttimeZone=[EncryptDecryptFile urlencode:encrypttimeZone];
    
    NSString *encrypttitle=[EncryptDecryptFile EncryptIT:title];
    encrypttitle=[EncryptDecryptFile urlencode:encrypttitle];
    
    NSString *encryptlocationName=[EncryptDecryptFile EncryptIT:locationName];
    encryptlocationName=[EncryptDecryptFile urlencode:encryptlocationName];
    
    NSString *encryptbusinessName=[EncryptDecryptFile EncryptIT:businessName];
    encryptbusinessName=[EncryptDecryptFile urlencode:encryptbusinessName];
    
    NSString *encryptstartDate=[EncryptDecryptFile EncryptIT:startDate];
    encryptstartDate=[EncryptDecryptFile urlencode:encryptstartDate];
    
    NSString *encryptstartTime=[EncryptDecryptFile EncryptIT:startTime];
    encryptstartTime=[EncryptDecryptFile urlencode:encryptstartTime];
    
    NSString *encryptdescription=[EncryptDecryptFile EncryptIT:description];
    encryptdescription=[EncryptDecryptFile urlencode:encryptdescription];
    
    NSString *encryptcategoryId=[EncryptDecryptFile EncryptIT:categoryId];
    encryptcategoryId=[EncryptDecryptFile urlencode:encryptcategoryId];
    
    NSString *encryptPrice=[EncryptDecryptFile EncryptIT:price];
    encryptPrice=[EncryptDecryptFile urlencode:encryptPrice];
    
    NSString *encryptName=[EncryptDecryptFile EncryptIT:name];
    encryptName=[EncryptDecryptFile urlencode:encryptName];
    
    NSString *encryptMobile=[EncryptDecryptFile EncryptIT:mobile];
    encryptMobile=[EncryptDecryptFile urlencode:encryptMobile];
    
    NSMutableData *DataValue = [NSMutableData data];
    
    [DataValue appendData:[[NSString stringWithFormat:@"userId=%@",encryptUserid]dataUsingEncoding:NSASCIIStringEncoding]];
    
    [DataValue appendData:[[NSString stringWithFormat:@"&timeZone=%@",encrypttimeZone]dataUsingEncoding:NSASCIIStringEncoding]];
    
    [DataValue appendData:[[NSString stringWithFormat:@"&title=%@",encrypttitle]dataUsingEncoding:NSASCIIStringEncoding]];
    
    [DataValue appendData:[[NSString stringWithFormat:@"&locationName=%@",encryptlocationName]dataUsingEncoding:NSASCIIStringEncoding]];
    
    [DataValue appendData:[[NSString stringWithFormat:@"&businessName=%@",encryptbusinessName]dataUsingEncoding:NSASCIIStringEncoding]];
    
    [DataValue appendData:[[NSString stringWithFormat:@"&startDate=%@",encryptstartDate]dataUsingEncoding:NSASCIIStringEncoding]];
    
    [DataValue appendData:[[NSString stringWithFormat:@"&startTime=%@",encryptstartTime]dataUsingEncoding:NSASCIIStringEncoding]];
    
    [DataValue appendData:[[NSString stringWithFormat:@"&description=%@",encryptdescription]dataUsingEncoding:NSASCIIStringEncoding]];
    
    [DataValue appendData:[[NSString stringWithFormat:@"&categoryId=%@",encryptcategoryId]dataUsingEncoding:NSASCIIStringEncoding]];
    
     [DataValue appendData:[[NSString stringWithFormat:@"&price=%@",encryptPrice]dataUsingEncoding:NSASCIIStringEncoding]];
    
    [DataValue appendData:[[NSString stringWithFormat:@"&name=%@",encryptName]dataUsingEncoding:NSASCIIStringEncoding]];
    
    [DataValue appendData:[[NSString stringWithFormat:@"&mobile=%@",encryptMobile]dataUsingEncoding:NSASCIIStringEncoding]];
    
    NSString *input = [NSString stringWithFormat:@"%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@",Apiversion,cipherSecretKey,Devicetype,cipherSecretKey,userId,cipherSecretKey,timeZone,cipherSecretKey,title,cipherSecretKey,locationName,cipherSecretKey,businessName,cipherSecretKey,startDate,cipherSecretKey,startTime,cipherSecretKey,description,cipherSecretKey,categoryId,cipherSecretKey,price,cipherSecretKey,name,cipherSecretKey,mobile,cipherSecretKey];
    
    NSString *output=[[NSString alloc]init];
    output = [EncryptDecryptFile md5:input];
    
    if([self connected] == true){
        
        [self showRequest:DataValue with:Devicetype with:output with:Apiversion];
        
    }else{
        
    }
    
    return jsonDict;
}





-(NSDictionary *)addBankUserid:(NSString *)userid name:(NSString *)name accountNo:(NSString *)accountNo routingNo:(NSString *)routingNo bankName:(NSString *)bankName accType:(NSString *)accType{
    methodName=@"Stripe/addBank";
    
    NSString *encryptUserid=[EncryptDecryptFile EncryptIT:userid];
    encryptUserid=[EncryptDecryptFile urlencode:encryptUserid];
    
    NSString *encryptName=[EncryptDecryptFile EncryptIT:name];
    encryptName=[EncryptDecryptFile urlencode:encryptName];
    
    NSString *encryptAccountNo=[EncryptDecryptFile EncryptIT:accountNo];
    encryptAccountNo=[EncryptDecryptFile urlencode:encryptAccountNo];
    
    NSString *encryptRountingNo=[EncryptDecryptFile EncryptIT:routingNo];
    encryptRountingNo=[EncryptDecryptFile urlencode:encryptRountingNo];
    
    NSString *encryptBankName=[EncryptDecryptFile EncryptIT:bankName];
    encryptBankName=[EncryptDecryptFile urlencode:encryptBankName];
    
    NSString *encryptAccType=[EncryptDecryptFile EncryptIT:accType];
    encryptAccType=[EncryptDecryptFile urlencode:encryptAccType];
    
    NSMutableData *DataValue = [NSMutableData data];
    
    [DataValue appendData:[[NSString stringWithFormat:@"userid=%@",encryptUserid]dataUsingEncoding:NSASCIIStringEncoding]];
    
    [DataValue appendData:[[NSString stringWithFormat:@"&name=%@",encryptName]dataUsingEncoding:NSASCIIStringEncoding]];
    
    [DataValue appendData:[[NSString stringWithFormat:@"&accountNo=%@",encryptAccountNo]dataUsingEncoding:NSASCIIStringEncoding]];
    
    [DataValue appendData:[[NSString stringWithFormat:@"&routingNo=%@",encryptRountingNo]dataUsingEncoding:NSASCIIStringEncoding]];
    
    [DataValue appendData:[[NSString stringWithFormat:@"&bankName=%@",encryptBankName]dataUsingEncoding:NSASCIIStringEncoding]];
    
    [DataValue appendData:[[NSString stringWithFormat:@"&accType=%@",encryptAccType]dataUsingEncoding:NSASCIIStringEncoding]];

    NSString *input = [NSString stringWithFormat:@"%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@",Apiversion,cipherSecretKey,Devicetype,cipherSecretKey,userid,cipherSecretKey,name,cipherSecretKey,accountNo,cipherSecretKey,routingNo,cipherSecretKey,bankName,cipherSecretKey,accType,cipherSecretKey];
    
    NSString *output=[[NSString alloc]init];
    output = [EncryptDecryptFile md5:input];
    
    if([self connected] == true){
        
        [self showRequest:DataValue with:Devicetype with:output with:Apiversion];
        
    }else{
        
    }
    
    return jsonDict;
    
}


-(NSDictionary *)viewCardUserid:(NSString *)userid{
    methodName=@"Bank_info/viewCard";
    
    NSString *encryptUserid=[EncryptDecryptFile EncryptIT:userid];
    encryptUserid=[EncryptDecryptFile urlencode:encryptUserid];
    
    NSMutableData *DataValue = [NSMutableData data];
    
    [DataValue appendData:[[NSString stringWithFormat:@"userid=%@",encryptUserid]dataUsingEncoding:NSASCIIStringEncoding]];
    
    NSString *input = [NSString stringWithFormat:@"%@%@%@%@%@%@",Apiversion,cipherSecretKey,Devicetype,cipherSecretKey,userid,cipherSecretKey];
    
    NSString *output=[[NSString alloc]init];
    output = [EncryptDecryptFile md5:input];
    
    if([self connected] == true){
        
        [self showRequest:DataValue with:Devicetype with:output with:Apiversion];
        
    }else{
        
    }
    
    return jsonDict;
    
}

-(NSDictionary *)viewVendorUserid:(NSString *)userid{
    methodName=@"User/view_vendor";
    
    NSString *encryptUserid=[EncryptDecryptFile EncryptIT:userid];
    encryptUserid=[EncryptDecryptFile urlencode:encryptUserid];
    
    NSMutableData *DataValue = [NSMutableData data];
    
    [DataValue appendData:[[NSString stringWithFormat:@"userId=%@",encryptUserid]dataUsingEncoding:NSASCIIStringEncoding]];
    
    NSString *input = [NSString stringWithFormat:@"%@%@%@%@%@%@",Apiversion,cipherSecretKey,Devicetype,cipherSecretKey,userid,cipherSecretKey];
    
    NSString *output=[[NSString alloc]init];
    output = [EncryptDecryptFile md5:input];
    
    if([self connected] == true){
        
        [self showRequest:DataValue with:Devicetype with:output with:Apiversion];
        
    }else{
        
    }
    
    return jsonDict;
    
}



-(NSDictionary *)viewBankUserid:(NSString *)userid accType:(NSString *)type{
    methodName=@"Bank_info/view_bank";
    
    NSString *encryptUserid=[EncryptDecryptFile EncryptIT:userid];
    encryptUserid=[EncryptDecryptFile urlencode:encryptUserid];
    
    NSString *encrypttype=[EncryptDecryptFile EncryptIT:type];
    encrypttype=[EncryptDecryptFile urlencode:encrypttype];
    
    NSMutableData *DataValue = [NSMutableData data];
    
    [DataValue appendData:[[NSString stringWithFormat:@"userId=%@",encryptUserid]dataUsingEncoding:NSASCIIStringEncoding]];
    
    [DataValue appendData:[[NSString stringWithFormat:@"&type=%@",encrypttype]dataUsingEncoding:NSASCIIStringEncoding]];
    
    NSString *input = [NSString stringWithFormat:@"%@%@%@%@%@%@%@%@",Apiversion,cipherSecretKey,Devicetype,cipherSecretKey,userid,cipherSecretKey,type,cipherSecretKey];
    
    NSString *output=[[NSString alloc]init];
    
    output = [EncryptDecryptFile md5:input];
    
    if([self connected] == true){
        
        [self showRequest:DataValue with:Devicetype with:output with:Apiversion];
        
    }else{
        
    }
    
    return jsonDict;
    
}
-(NSDictionary *)reportTaskid:(NSString *)taskid userid:(NSString *)userid{
    
    //    methodName=@"Task_Info/viewtaskInfo";
    methodName=@"Reservation_Info/reported_reservation";
    
    NSString *encryptTaskid=[EncryptDecryptFile EncryptIT:taskid];
    encryptTaskid=[EncryptDecryptFile urlencode:encryptTaskid];
    
    NSString *encryptUserid=[EncryptDecryptFile EncryptIT:userid];
    encryptUserid=[EncryptDecryptFile urlencode:encryptUserid];
    
    NSMutableData *DataValue = [NSMutableData data];
    
    [DataValue appendData:[[NSString stringWithFormat:@"reservationId=%@",encryptTaskid]dataUsingEncoding:NSASCIIStringEncoding]];
    
    [DataValue appendData:[[NSString stringWithFormat:@"&userId=%@",encryptUserid]dataUsingEncoding:NSASCIIStringEncoding]];
    
    
    NSString *input;
    
    if(myAppDelegate.isFromSkip)
    {
        input = [NSString stringWithFormat:@"%@%@%@%@%@%@%@",Apiversion,cipherSecretKey,Devicetype,cipherSecretKey,taskid,cipherSecretKey,cipherSecretKey];
    }
    else
    {
        input = [NSString stringWithFormat:@"%@%@%@%@%@%@%@%@",Apiversion,cipherSecretKey,Devicetype,cipherSecretKey,taskid,cipherSecretKey,userid,cipherSecretKey];
    }
    
    NSString *output=[[NSString alloc]init];
    output = [EncryptDecryptFile md5:input];
    
    if([self connected] == true){
        
        [self showRequest:DataValue with:Devicetype with:output with:Apiversion];
        
    }else{
        
    }
    
    return jsonDict;
    
}



-(NSDictionary *)reservationHistoryUserid:(NSString *)userid timeZone:(NSString *)timeZone
{
    methodName=@"Reservation_Info/reservation_history";
    
    NSString *encryptUserid=[EncryptDecryptFile EncryptIT:userid];
    encryptUserid=[EncryptDecryptFile urlencode:encryptUserid];
    
    NSString *encrypttimeZone=[EncryptDecryptFile EncryptIT:timeZone];
    encrypttimeZone=[EncryptDecryptFile urlencode:encrypttimeZone];
    
    NSMutableData *DataValue = [NSMutableData data];
    
    
    [DataValue appendData:[[NSString stringWithFormat:@"&userId=%@",encryptUserid]dataUsingEncoding:NSASCIIStringEncoding]];
    
    [DataValue appendData:[[NSString stringWithFormat:@"&timeZone=%@",encrypttimeZone]dataUsingEncoding:NSASCIIStringEncoding]];
    NSString *input;
    
    //    if(myAppDelegate.isFromSkip)
    //    {
    //        input = [NSString stringWithFormat:@"%@%@%@%@%@%@%@",Apiversion,cipherSecretKey,Devicetype,cipherSecretKey,cipherSecretKey,timeZone,cipherSecretKey];
    //    }
    //    else
    //    {
    input = [NSString stringWithFormat:@"%@%@%@%@%@%@%@%@",Apiversion,cipherSecretKey,Devicetype,cipherSecretKey,userid,cipherSecretKey,timeZone,cipherSecretKey];
    // }
    NSString *output=[[NSString alloc]init];
    output = [EncryptDecryptFile md5:input];
    
    if([self connected] == true){
        
        [self showRequest:DataValue with:Devicetype with:output with:Apiversion];
        
    }else{
        
    }
    
    return jsonDict;
    
    
}


-(NSDictionary *)vendorstatusTaskid:(NSString *)taskid status:(NSString *)status vendorComment:(NSString *)vendorComment userId:(NSString *)userId
{
    methodName=@"/Reservation_Info/vendor_status";
    
    
    NSString *encryptTaskid=[EncryptDecryptFile EncryptIT:taskid];
    encryptTaskid=[EncryptDecryptFile urlencode:encryptTaskid];
    
    NSString *encryptstatus=[EncryptDecryptFile EncryptIT:status];
    encryptstatus=[EncryptDecryptFile urlencode:encryptstatus];
    
    NSString *encryptvendorComment=[EncryptDecryptFile EncryptIT:vendorComment];
    encryptvendorComment=[EncryptDecryptFile urlencode:encryptvendorComment];
    
    NSString *encryptuserId=[EncryptDecryptFile EncryptIT:userId];
    encryptuserId=[EncryptDecryptFile urlencode:encryptuserId];
    
    NSMutableData *DataValue = [NSMutableData data];
    
    [DataValue appendData:[[NSString stringWithFormat:@"reservationId=%@",encryptTaskid]dataUsingEncoding:NSASCIIStringEncoding]];
    
    [DataValue appendData:[[NSString stringWithFormat:@"&status=%@",encryptstatus]dataUsingEncoding:NSASCIIStringEncoding]];
    
    [DataValue appendData:[[NSString stringWithFormat:@"&vendorComment=%@",encryptvendorComment]dataUsingEncoding:NSASCIIStringEncoding]];
    
    [DataValue appendData:[[NSString stringWithFormat:@"&userId=%@",encryptuserId]dataUsingEncoding:NSASCIIStringEncoding]];
    
    NSString *input;
    
        input = [NSString stringWithFormat:@"%@%@%@%@%@%@%@%@%@%@%@%@",Apiversion,cipherSecretKey,Devicetype,cipherSecretKey,taskid,cipherSecretKey,status,cipherSecretKey,vendorComment,cipherSecretKey,userId,cipherSecretKey];
   
    NSString *output=[[NSString alloc]init];
    output = [EncryptDecryptFile md5:input];
    
    if([self connected] == true){
        
        [self showRequest:DataValue with:Devicetype with:output with:Apiversion];
        
    }else{
        
    }
    
    return jsonDict;
    

}

-(NSDictionary *)deleteReservationTaskid:(NSString *)taskid userid:(NSString *)userid{
    
    //    methodName=@"Task_Info/viewtaskInfo";
    methodName=@"Reservation_Info/delete_reservation";
    
    NSString *encryptTaskid=[EncryptDecryptFile EncryptIT:taskid];
    encryptTaskid=[EncryptDecryptFile urlencode:encryptTaskid];
    
    NSString *encryptUserid=[EncryptDecryptFile EncryptIT:userid];
    encryptUserid=[EncryptDecryptFile urlencode:encryptUserid];
    
    NSMutableData *DataValue = [NSMutableData data];
    
    [DataValue appendData:[[NSString stringWithFormat:@"reservationId=%@",encryptTaskid]dataUsingEncoding:NSASCIIStringEncoding]];
    
    [DataValue appendData:[[NSString stringWithFormat:@"&userId=%@",encryptUserid]dataUsingEncoding:NSASCIIStringEncoding]];
    
    NSString *input;
    
        input = [NSString stringWithFormat:@"%@%@%@%@%@%@%@%@",Apiversion,cipherSecretKey,Devicetype,cipherSecretKey,taskid,cipherSecretKey,userid,cipherSecretKey];
    
    NSString *output=[[NSString alloc]init];
    output = [EncryptDecryptFile md5:input];
    
    if([self connected] == true){
        
        [self showRequest:DataValue with:Devicetype with:output with:Apiversion];
        
    }else{
        
    }
    
    return jsonDict;
    
}

-(NSDictionary *)addPayPalUserid:(NSString *)Userid metaDataId:(NSString *)metaDataId codeString:(NSString *)codeString{
  //  [GlobalFunction addIndicatorView];
    //    methodName=@"Task_Info/viewtaskInfo";
    methodName=@"Stripe/AddPayPal";
    
    NSString *encryptUserid=[EncryptDecryptFile EncryptIT:Userid];
    encryptUserid=[EncryptDecryptFile urlencode:encryptUserid];
    
    NSString *encryptmetaDataId=[EncryptDecryptFile EncryptIT:metaDataId];
    encryptmetaDataId=[EncryptDecryptFile urlencode:encryptmetaDataId];
    
    NSString *encryptmetacodeString=[EncryptDecryptFile EncryptIT:codeString];
    encryptmetacodeString=[EncryptDecryptFile urlencode:encryptmetacodeString];
    
    NSMutableData *DataValue = [NSMutableData data];
    
    [DataValue appendData:[[NSString stringWithFormat:@"userId=%@",encryptUserid]dataUsingEncoding:NSASCIIStringEncoding]];
    
    [DataValue appendData:[[NSString stringWithFormat:@"&metaDataId=%@",encryptmetaDataId]dataUsingEncoding:NSASCIIStringEncoding]];
    
    
    [DataValue appendData:[[NSString stringWithFormat:@"&codeString=%@",encryptmetacodeString]dataUsingEncoding:NSASCIIStringEncoding]];
    
    NSString *input;
    
    input = [NSString stringWithFormat:@"%@%@%@%@%@%@%@%@%@%@",Apiversion,cipherSecretKey,Devicetype,cipherSecretKey,Userid,cipherSecretKey,metaDataId,cipherSecretKey,codeString,cipherSecretKey];
    
    NSString *output=[[NSString alloc]init];
    output = [EncryptDecryptFile md5:input];
    
    if([self connected] == true){
        
        [self showRequest:DataValue with:Devicetype with:output with:Apiversion];
        
    }else{
        
    }
    
    return jsonDict;
    
}

-(NSDictionary *)AddEmailUserid:(NSString *)Userid emailId:(NSString *)emailId {
    
    //    methodName=@"Task_Info/viewtaskInfo";
    methodName=@"Stripe/AddEmail";
    
    NSString *encryptUserid=[EncryptDecryptFile EncryptIT:Userid];
    encryptUserid=[EncryptDecryptFile urlencode:encryptUserid];
    
    NSString *encryptemailId=[EncryptDecryptFile EncryptIT:emailId];
    encryptemailId=[EncryptDecryptFile urlencode:encryptemailId];
    
    
    NSMutableData *DataValue = [NSMutableData data];
    
    [DataValue appendData:[[NSString stringWithFormat:@"userId=%@",encryptUserid]dataUsingEncoding:NSASCIIStringEncoding]];
    
    [DataValue appendData:[[NSString stringWithFormat:@"&emailId=%@",encryptemailId]dataUsingEncoding:NSASCIIStringEncoding]];
    
    
    
    
    NSString *input;
    
    input = [NSString stringWithFormat:@"%@%@%@%@%@%@%@%@",Apiversion,cipherSecretKey,Devicetype,cipherSecretKey,Userid,cipherSecretKey,emailId,cipherSecretKey];
    
    NSString *output=[[NSString alloc]init];
    output = [EncryptDecryptFile md5:input];
    
    if([self connected] == true){
        
        [self showRequest:DataValue with:Devicetype with:output with:Apiversion];
        
    }else{
        
    }
    
    return jsonDict;
    
}


-(NSDictionary *)viewTaskInfoTaskid:(NSString *)taskid userid:(NSString *)userid timeZone:(NSString *)timeZone view:(NSString *)view{
    
    //    methodName=@"Task_Info/viewtaskInfo";
    methodName=@"Reservation_Info/view_reservation_info";
    
    
    NSString *encryptTaskid=[EncryptDecryptFile EncryptIT:taskid];
    encryptTaskid=[EncryptDecryptFile urlencode:encryptTaskid];
    
    NSString *encryptUserid=[EncryptDecryptFile EncryptIT:userid];
    encryptUserid=[EncryptDecryptFile urlencode:encryptUserid];
    
    NSString *encrypttimeZone=[EncryptDecryptFile EncryptIT:timeZone];
    encrypttimeZone=[EncryptDecryptFile urlencode:encrypttimeZone];
    
    NSString *encryptview=[EncryptDecryptFile EncryptIT:view];
    encryptview=[EncryptDecryptFile urlencode:encryptview];
    
    
    NSMutableData *DataValue = [NSMutableData data];
    
    [DataValue appendData:[[NSString stringWithFormat:@"reservationId=%@",encryptTaskid]dataUsingEncoding:NSASCIIStringEncoding]];
    
    [DataValue appendData:[[NSString stringWithFormat:@"&userId=%@",encryptUserid]dataUsingEncoding:NSASCIIStringEncoding]];
    
     [DataValue appendData:[[NSString stringWithFormat:@"&timeZone=%@",encrypttimeZone]dataUsingEncoding:NSASCIIStringEncoding]];
    
     [DataValue appendData:[[NSString stringWithFormat:@"&view=%@",encryptview]dataUsingEncoding:NSASCIIStringEncoding]];
    
    
    NSString *input;
    
        if(myAppDelegate.isFromSkip)
        {
         input = [NSString stringWithFormat:@"%@%@%@%@%@%@%@%@%@%@%@",Apiversion,cipherSecretKey,Devicetype,cipherSecretKey,taskid,cipherSecretKey,cipherSecretKey,timeZone,cipherSecretKey,view,cipherSecretKey];
        }
        else
        {
    input = [NSString stringWithFormat:@"%@%@%@%@%@%@%@%@%@%@%@%@",Apiversion,cipherSecretKey,Devicetype,cipherSecretKey,taskid,cipherSecretKey,userid,cipherSecretKey,timeZone,cipherSecretKey,view,cipherSecretKey];
    }
    NSString *output=[[NSString alloc]init];
    output = [EncryptDecryptFile md5:input];
    
    if([self connected] == true){
        
        [self showRequest:DataValue with:Devicetype with:output with:Apiversion];
        
    }else{
        
    }
    
    return jsonDict;
    
}

//-(NSDictionary *)purchaseReservationid:(NSString *)taskid userid:(NSString *)userid LastModifyTime:(NSString *)LastModifyTime {
//
//
//    methodName=@"Reservation_Info/purchase_reservation";
//    
//    NSString *encryptTaskid=[EncryptDecryptFile EncryptIT:taskid];
//    encryptTaskid=[EncryptDecryptFile urlencode:encryptTaskid];
//
//    NSString *encryptUserid=[EncryptDecryptFile EncryptIT:userid];
//    encryptUserid=[EncryptDecryptFile urlencode:encryptUserid];
//
//    NSString *encryptLastModifyTime=[EncryptDecryptFile EncryptIT:LastModifyTime];
//    encryptLastModifyTime=[EncryptDecryptFile urlencode:encryptLastModifyTime];
//
//    NSMutableData *DataValue = [NSMutableData data];
//
//    [DataValue appendData:[[NSString stringWithFormat:@"reservationId=%@",encryptTaskid]dataUsingEncoding:NSASCIIStringEncoding]];
//
//    [DataValue appendData:[[NSString stringWithFormat:@"&userId=%@",encryptUserid]dataUsingEncoding:NSASCIIStringEncoding]];
//
//    [DataValue appendData:[[NSString stringWithFormat:@"&LastModifyTime=%@",encryptLastModifyTime]dataUsingEncoding:NSASCIIStringEncoding]];
//
//
//    NSString *input;
//
////    if(myAppDelegate.isFromSkip)
////    {
////     input = [NSString stringWithFormat:@"%@%@%@%@%@%@%@",Apiversion,cipherSecretKey,Devicetype,cipherSecretKey,taskid,cipherSecretKey,cipherSecretKey];
////    }
////    else
////    {
//        input = [NSString stringWithFormat:@"%@%@%@%@%@%@%@%@%@%@",Apiversion,cipherSecretKey,Devicetype,cipherSecretKey,taskid,cipherSecretKey,userid,cipherSecretKey,LastModifyTime,cipherSecretKey];
//    //}
//    NSString *output=[[NSString alloc]init];
//    output = [EncryptDecryptFile md5:input];
//
//    if([self connected] == true){
//        
//        [self showRequest:DataValue with:Devicetype with:output with:Apiversion];
//        
//    }else{
//
//    }
//
//    return jsonDict;
//
//}


-(NSDictionary *)purchaseReservationid:(NSString *)taskid userid:(NSString *)userid LastModifyTime:(NSString *)LastModifyTime timeZone:(NSString*)timeZone {
    
    
    methodName=@"Reservation_Info/purchase_reservation";
    
    NSString *encryptTaskid=[EncryptDecryptFile EncryptIT:taskid];
    encryptTaskid=[EncryptDecryptFile urlencode:encryptTaskid];
    
    NSString *encryptUserid=[EncryptDecryptFile EncryptIT:userid];
    encryptUserid=[EncryptDecryptFile urlencode:encryptUserid];
    
    NSString *encryptLastModifyTime=[EncryptDecryptFile EncryptIT:LastModifyTime];
    encryptLastModifyTime=[EncryptDecryptFile urlencode:encryptLastModifyTime];
    
    NSString *encrypttimeZone = [EncryptDecryptFile EncryptIT:timeZone];
    encrypttimeZone=[EncryptDecryptFile urlencode:encrypttimeZone];
    
    NSMutableData *DataValue = [NSMutableData data];
    
    [DataValue appendData:[[NSString stringWithFormat:@"reservationId=%@",encryptTaskid]dataUsingEncoding:NSASCIIStringEncoding]];
    
    [DataValue appendData:[[NSString stringWithFormat:@"&userId=%@",encryptUserid]dataUsingEncoding:NSASCIIStringEncoding]];
    
    [DataValue appendData:[[NSString stringWithFormat:@"&LastModifyTime=%@",encryptLastModifyTime]dataUsingEncoding:NSASCIIStringEncoding]];
    
    [DataValue appendData:[[NSString stringWithFormat:@"&timeZone=%@",encrypttimeZone]dataUsingEncoding:NSASCIIStringEncoding]];
    
    
    NSString *input;
    
    //    if(myAppDelegate.isFromSkip)
    //    {
    //     input = [NSString stringWithFormat:@"%@%@%@%@%@%@%@",Apiversion,cipherSecretKey,Devicetype,cipherSecretKey,taskid,cipherSecretKey,cipherSecretKey];
    //    }
    //    else
    //    {
    input = [NSString stringWithFormat:@"%@%@%@%@%@%@%@%@%@%@%@%@",Apiversion,cipherSecretKey,Devicetype,cipherSecretKey,taskid,cipherSecretKey,userid,cipherSecretKey,LastModifyTime,cipherSecretKey,timeZone,cipherSecretKey];
    //}
    NSString *output=[[NSString alloc]init];
    output = [EncryptDecryptFile md5:input];
    
    if([self connected] == true){
        
        [self showRequest:DataValue with:Devicetype with:output with:Apiversion];
        
    }else{
        
    }
    
    return jsonDict;
    
}


-(NSDictionary *)addCardUserid:(NSString *)userid payment_method:(NSString *)payment_method card_holder_name:(NSString *)card_holder_name card_number:(NSString *)card_number expiry_month:(NSString *)expiry_month expiry_year:(NSString *)expiry_year cvv_number:(NSString *)cvv_number{
    
    methodName=@"Stripe/customer_id";
    
    NSString *encryptUserid=[EncryptDecryptFile EncryptIT:userid];
    encryptUserid=[EncryptDecryptFile urlencode:encryptUserid];
    
    NSString *encryptPaymentMethod=[EncryptDecryptFile EncryptIT:payment_method];
    encryptPaymentMethod=[EncryptDecryptFile urlencode:encryptPaymentMethod];
    
    NSString *encryptCardName=[EncryptDecryptFile EncryptIT:card_holder_name];
    encryptCardName=[EncryptDecryptFile urlencode:encryptCardName];
    
    NSString *encryptCardNumber=[EncryptDecryptFile EncryptIT:card_number];
    encryptCardNumber=[EncryptDecryptFile urlencode:encryptCardNumber];
    
    NSString *encryptExpireMonth=[EncryptDecryptFile EncryptIT:expiry_month];
    encryptExpireMonth=[EncryptDecryptFile urlencode:encryptExpireMonth];
    
    NSString *encryptExpireYear=[EncryptDecryptFile EncryptIT:expiry_year];
    encryptExpireYear=[EncryptDecryptFile urlencode:encryptExpireYear];
    
    NSString *encryptCvvNo=[EncryptDecryptFile EncryptIT:cvv_number];
    encryptCvvNo=[EncryptDecryptFile urlencode:encryptCvvNo];
    
    NSMutableData *DataValue = [NSMutableData data];
    
    [DataValue appendData:[[NSString stringWithFormat:@"userId=%@",encryptUserid]dataUsingEncoding:NSASCIIStringEncoding]];
    
    [DataValue appendData:[[NSString stringWithFormat:@"&payment_method=%@",encryptPaymentMethod]dataUsingEncoding:NSASCIIStringEncoding]];
    
    [DataValue appendData:[[NSString stringWithFormat:@"&card_holder_name=%@",encryptCardName]dataUsingEncoding:NSASCIIStringEncoding]];
    
    [DataValue appendData:[[NSString stringWithFormat:@"&card_number=%@",encryptCardNumber]dataUsingEncoding:NSASCIIStringEncoding]];
    
    [DataValue appendData:[[NSString stringWithFormat:@"&expiry_month=%@",encryptExpireMonth]dataUsingEncoding:NSASCIIStringEncoding]];
    
    [DataValue appendData:[[NSString stringWithFormat:@"&expiry_year=%@",encryptExpireYear]dataUsingEncoding:NSASCIIStringEncoding]];
    
    [DataValue appendData:[[NSString stringWithFormat:@"&cvv_number=%@",encryptCvvNo]dataUsingEncoding:NSASCIIStringEncoding]];
    
    NSString *input = [NSString stringWithFormat:@"%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@",Apiversion,cipherSecretKey,Devicetype,cipherSecretKey,userid,cipherSecretKey,payment_method,cipherSecretKey,card_holder_name,cipherSecretKey,card_number,cipherSecretKey,expiry_month,cipherSecretKey,expiry_year,cipherSecretKey,cvv_number,cipherSecretKey];
    
    NSString *output=[[NSString alloc]init];
    output = [EncryptDecryptFile md5:input];
    
    if([self connected] == true){
        
        [self showRequest:DataValue with:Devicetype with:output with:Apiversion];
        
    }else{

    }
    
    return jsonDict;
    
}


-(NSDictionary *)acceptTaskTaskid:(NSString *)taskid userid:(NSString *)userid{
    
    methodName=@"Task_Info/acceptTask";
    
    NSString *encryptTaskid=[EncryptDecryptFile EncryptIT:taskid];
    encryptTaskid=[EncryptDecryptFile urlencode:encryptTaskid];
    
    NSString *encryptUserid=[EncryptDecryptFile EncryptIT:userid];
    encryptUserid=[EncryptDecryptFile urlencode:encryptUserid];
    
    NSMutableData *DataValue = [NSMutableData data];
    
    [DataValue appendData:[[NSString stringWithFormat:@"taskid=%@",encryptTaskid]dataUsingEncoding:NSASCIIStringEncoding]];
    
    [DataValue appendData:[[NSString stringWithFormat:@"&userid=%@",encryptUserid]dataUsingEncoding:NSASCIIStringEncoding]];
    
    NSString *input = [NSString stringWithFormat:@"%@%@%@%@%@%@%@%@",Apiversion,cipherSecretKey,Devicetype,cipherSecretKey,taskid,cipherSecretKey,userid,cipherSecretKey];
    
    NSString *output=[[NSString alloc]init];
    output = [EncryptDecryptFile md5:input];
    
    if([self connected] == true){
        
        [self showRequest:DataValue with:Devicetype with:output with:Apiversion];
        
    }
    else{
        
    }
    
    return jsonDict;
    
}


-(NSDictionary *)removeStripeForUserid:(NSString *)userid{
    
    methodName=@"Task_Info/removeStripe";
    
    NSString *encryptUserid=[EncryptDecryptFile EncryptIT:userid];
    encryptUserid=[EncryptDecryptFile urlencode:encryptUserid];
    
    NSMutableData *DataValue = [NSMutableData data];
    
    [DataValue appendData:[[NSString stringWithFormat:@"userid=%@",encryptUserid]dataUsingEncoding:NSASCIIStringEncoding]];
    
    NSString *input = [NSString stringWithFormat:@"%@%@%@%@%@%@",Apiversion,cipherSecretKey,Devicetype,cipherSecretKey,userid,cipherSecretKey];
    
    NSString *output=[[NSString alloc]init];
    output = [EncryptDecryptFile md5:input];
    
    if([self connected] == true){
        
        [self showRequest:DataValue with:Devicetype with:output with:Apiversion];
        
    }
    else{
        
    }
    
    return jsonDict;
    
}


-(NSDictionary *)employeeStatusTaskid:(NSString *)taskid status:(NSString *)status{
    
    methodName=@"Task_Info/Employerstatus";
    
    NSString *encryptTaskid=[EncryptDecryptFile EncryptIT:taskid];
    encryptTaskid=[EncryptDecryptFile urlencode:encryptTaskid];
    
    NSString *encryptStatus=[EncryptDecryptFile EncryptIT:status];
    encryptStatus=[EncryptDecryptFile urlencode:encryptStatus];
    
    
    NSMutableData *DataValue = [NSMutableData data];
    
    [DataValue appendData:[[NSString stringWithFormat:@"taskid=%@",encryptTaskid]dataUsingEncoding:NSASCIIStringEncoding]];
    
    [DataValue appendData:[[NSString stringWithFormat:@"&status=%@",encryptStatus]dataUsingEncoding:NSASCIIStringEncoding]];
    
    NSString *input = [NSString stringWithFormat:@"%@%@%@%@%@%@%@%@",Apiversion,cipherSecretKey,Devicetype,cipherSecretKey,taskid,cipherSecretKey,status,cipherSecretKey];
    
    NSString *output=[[NSString alloc]init];
    output = [EncryptDecryptFile md5:input];
    
    if([self connected] == true){
        
        [self showRequest:DataValue with:Devicetype with:output with:Apiversion];
        
    }else{
        
    }
    
    return jsonDict;
    
}

-(NSDictionary *)taskCompleteTaskid:(NSString *)taskid status:(NSString *)status{
    
    methodName=@"Task_Info/taskComplete";
    
    NSString *encryptTaskid=[EncryptDecryptFile EncryptIT:taskid];
    encryptTaskid=[EncryptDecryptFile urlencode:encryptTaskid];
    
    NSString *encryptStatus=[EncryptDecryptFile EncryptIT:status];
    encryptStatus=[EncryptDecryptFile urlencode:encryptStatus];
    
    NSMutableData *DataValue = [NSMutableData data];
    
    [DataValue appendData:[[NSString stringWithFormat:@"taskid=%@",encryptTaskid]dataUsingEncoding:NSASCIIStringEncoding]];
    
    
    [DataValue appendData:[[NSString stringWithFormat:@"&status=%@",encryptStatus]dataUsingEncoding:NSASCIIStringEncoding]];
    
    NSString *input = [NSString stringWithFormat:@"%@%@%@%@%@%@%@%@",Apiversion,cipherSecretKey,Devicetype,cipherSecretKey,taskid,cipherSecretKey,status,cipherSecretKey];
    
    NSString *output=[[NSString alloc]init];
    output = [EncryptDecryptFile md5:input];
    
    if([self connected] == true){
        
        [self showRequest:DataValue with:Devicetype with:output with:Apiversion];
        return jsonDict;
        
    }else{
        
    }
    
    
    return 0;
    
}

-(NSDictionary *)addRatingUserid:(NSString *)userid ratingStar:(NSString *)ratingStar description:(NSString *)description taskid:(NSString *)taskid employerId:(NSString *)employerId{
    
    methodName=@"Rating_info/addRating";
    
    NSString *encryptUserid=[EncryptDecryptFile EncryptIT:userid];
    encryptUserid=[EncryptDecryptFile urlencode:encryptUserid];
    
    NSString *encryptRatingStar=[EncryptDecryptFile EncryptIT:ratingStar];
    encryptRatingStar=[EncryptDecryptFile urlencode:encryptRatingStar];
    
    NSString *encryptDescription=[EncryptDecryptFile EncryptIT:description];
    encryptDescription=[EncryptDecryptFile urlencode:encryptDescription];
    
    NSString *encryptTaskId=[EncryptDecryptFile EncryptIT:taskid];
    encryptTaskId=[EncryptDecryptFile urlencode:encryptTaskId];
    
    NSString *encryptEmployerId=[EncryptDecryptFile EncryptIT:employerId];
    encryptEmployerId=[EncryptDecryptFile urlencode:encryptEmployerId];
    
    NSMutableData *DataValue = [NSMutableData data];
    
    [DataValue appendData:[[NSString stringWithFormat:@"userId=%@",encryptUserid]dataUsingEncoding:NSASCIIStringEncoding]];
    
    [DataValue appendData:[[NSString stringWithFormat:@"&ratingStar=%@",encryptRatingStar]dataUsingEncoding:NSASCIIStringEncoding]];
    
    [DataValue appendData:[[NSString stringWithFormat:@"&description=%@",encryptDescription]dataUsingEncoding:NSASCIIStringEncoding]];
    
    [DataValue appendData:[[NSString stringWithFormat:@"&reservationId=%@",encryptTaskId]dataUsingEncoding:NSASCIIStringEncoding]];
    
    [DataValue appendData:[[NSString stringWithFormat:@"&sellerId=%@",encryptEmployerId]dataUsingEncoding:NSASCIIStringEncoding]];
    
    NSString *input = [NSString stringWithFormat:@"%@%@%@%@%@%@%@%@%@%@%@%@%@%@",Apiversion,cipherSecretKey,Devicetype,cipherSecretKey,userid,cipherSecretKey,ratingStar,cipherSecretKey,description,cipherSecretKey,taskid,cipherSecretKey,employerId,cipherSecretKey];
    
    NSString *output=[[NSString alloc]init];
    output = [EncryptDecryptFile md5:input];
    
    if([self connected] == true){
        
        [self showRequest:DataValue with:Devicetype with:output with:Apiversion];
        
    }else{

    }
    
    return jsonDict;
    
}

-(NSDictionary *)generateStripeUserIdForUserId:(NSString *)userId andAuthCode:(NSString *)authCode{
    
    methodName = @"Stripe/stripe_user_id";
    
    NSString *encryptUserid=[EncryptDecryptFile EncryptIT:userId];
    encryptUserid=[EncryptDecryptFile urlencode:encryptUserid];
    
    NSString *encryptAuthCode=[EncryptDecryptFile EncryptIT:authCode];
    encryptAuthCode=[EncryptDecryptFile urlencode:encryptAuthCode];
    
    NSMutableData *DataValue = [NSMutableData data];
    [DataValue appendData:[[NSString stringWithFormat:@"userId=%@",encryptUserid]dataUsingEncoding:NSASCIIStringEncoding]];
    
    [DataValue appendData:[[NSString stringWithFormat:@"&code=%@",encryptAuthCode]dataUsingEncoding:NSASCIIStringEncoding]];
    
    NSString *input = [NSString stringWithFormat:@"%@%@%@%@%@%@%@%@",Apiversion,cipherSecretKey,Devicetype,cipherSecretKey,userId,cipherSecretKey,authCode,cipherSecretKey];
    
    NSString *output=[[NSString alloc]init];
    output = [EncryptDecryptFile md5:input];
    
    if([self connected] == true){
        
        [self showRequest:DataValue with:Devicetype with:output with:Apiversion];
        
    }else{
        
    }
    
    return jsonDict;
}


-(NSDictionary *)termsAndPolicyUserId:(NSString *)userId tnc:(NSString *)tnc
{
    methodName=@"User/tnc";
    
    NSString *encryptUserId=[EncryptDecryptFile EncryptIT:userId];
    encryptUserId=[EncryptDecryptFile urlencode:encryptUserId];
    
    NSString *encryptTnc=[EncryptDecryptFile EncryptIT:tnc];
    encryptTnc=[EncryptDecryptFile urlencode:encryptTnc];

    NSMutableData *DataValue = [NSMutableData data];
    
    
    [DataValue appendData:[[NSString stringWithFormat:@"&userId=%@",encryptUserId]dataUsingEncoding:NSASCIIStringEncoding]];
    [DataValue appendData:[[NSString stringWithFormat:@"&tnc=%@",encryptTnc]dataUsingEncoding:NSASCIIStringEncoding]];
    
    
    NSString *input = [NSString stringWithFormat:@"%@%@%@%@%@%@%@%@",Apiversion,cipherSecretKey,Devicetype,cipherSecretKey,userId,cipherSecretKey,tnc,cipherSecretKey];
    
    NSString *output=[[NSString alloc]init];
    output = [EncryptDecryptFile md5:input];
    
    if([self connected] == true){
        
        [self showRequest:DataValue with:Devicetype with:output with:Apiversion];
        
    }else{
        
    }
    
    return jsonDict;

}


-(NSDictionary *)getUpdatelastModifyTime:(NSString *)lastModifyTime userid:(NSString *)userid timeZone:(NSString *)timeZone{
    
    methodName=@"Reservation_Info/getUpdates";
    
    NSString *encryptLastModifyTime=[EncryptDecryptFile EncryptIT:lastModifyTime];
    encryptLastModifyTime=[EncryptDecryptFile urlencode:encryptLastModifyTime];
    
    NSString *encryptuserId=[EncryptDecryptFile EncryptIT:userid];
    encryptuserId=[EncryptDecryptFile urlencode:encryptuserId];

    NSString *encrypttimeZone=[EncryptDecryptFile EncryptIT:timeZone];
    encrypttimeZone=[EncryptDecryptFile urlencode:encrypttimeZone];
    
    NSMutableData *DataValue = [NSMutableData data];
    
    [DataValue appendData:[[NSString stringWithFormat:@"&lastModifyTime=%@",encryptLastModifyTime]dataUsingEncoding:NSASCIIStringEncoding]];
    [DataValue appendData:[[NSString stringWithFormat:@"&userId=%@",encryptuserId]dataUsingEncoding:NSASCIIStringEncoding]];
 [DataValue appendData:[[NSString stringWithFormat:@"&timeZone=%@",encrypttimeZone]dataUsingEncoding:NSASCIIStringEncoding]];
    
    NSString *input;
    
    if(myAppDelegate.isFromSkip)
    {
       input = [NSString stringWithFormat:@"%@%@%@%@%@%@%@%@%@",Apiversion,cipherSecretKey,Devicetype,cipherSecretKey,lastModifyTime,cipherSecretKey,cipherSecretKey,timeZone,cipherSecretKey];
        NSLog(@" cipher from skip ---- %@",input);
    }
    else
    {
       input = [NSString stringWithFormat:@"%@%@%@%@%@%@%@%@%@%@",Apiversion,cipherSecretKey,Devicetype,cipherSecretKey,lastModifyTime,cipherSecretKey,userid,cipherSecretKey,timeZone,cipherSecretKey];
        NSLog(@" cipher wihout skip ---- %@",input);

    }
    NSString *output=[[NSString alloc]init];
    output = [EncryptDecryptFile md5:input];
    
    if([self connected] == true){
        
        [self showRequest:DataValue with:Devicetype with:output with:Apiversion];
        
    }else{
        
    }
    
    return jsonDict;
    
}







#pragma mark -
#pragma mark NSURLConnection Delegate Methods



//- (void)connection:(NSURLConnection *)connection didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge
//{
//    if ([challenge.protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust])
//    {
//        if ([@"buytimee.com" isEqualToString:challenge.protectionSpace.host])
//        {
//            [challenge.sender useCredential:[NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust] forAuthenticationChallenge:challenge];
//        }
//    }
//    [challenge.sender continueWithoutCredentialForAuthenticationChallenge:challenge];
//}


-(void)connection:(NSURLConnection *)connection didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge{
    
    if ([[[challenge protectionSpace] authenticationMethod] isEqualToString: NSURLAuthenticationMethodServerTrust])
    {
        
            SecTrustRef serverTrust = [[challenge protectionSpace] serverTrust];
            if(nil != serverTrust)
             
            [challenge.sender useCredential:[NSURLCredential credentialForTrust:serverTrust]
                          forAuthenticationChallenge: challenge];
   
        
    }
    
   return [challenge.sender continueWithoutCredentialForAuthenticationChallenge:challenge];
   // return [challenge.sender cancelAuthenticationChallenge:challenge];
}


- (BOOL)connection:(NSURLConnection *)connection canAuthenticateAgainstProtectionSpace:(NSURLProtectionSpace *)protectionSpace
{
    

    return [protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust];
}

//
//
//
//- (void)connection:(NSURLConnection *)connection willSendRequestForAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge{
//    
//}


//- (BOOL)connection:(NSURLConnection *)connection canAuthenticateAgainstProtectionSpace:(NSURLProtectionSpace *)protectionSpace {
//    return [protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust];
//}

//- (void)connection:(NSURLConnection *)connection didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge {
////    if ([challenge.protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust])
////        if ([trustedHosts containsObject:challenge.protectionSpace.host])
////            [challenge.sender useCredential:[NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust] forAuthenticationChallenge:challenge];
//    
//    [challenge.sender continueWithoutCredentialForAuthenticationChallenge:challenge];
//}






//- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
//    responseDataforSession = [[NSMutableData alloc] init];
//    
//    NSDictionary* headers = [(NSHTTPURLResponse *)response allHeaderFields];
//
//    NSLog(@"Header is From Server = %@", headers);
//}
//
//- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
//    
//    [responseDataforSession appendData:data];
//    
//   // NSString *str = [[NSString alloc]initWithData:responseDataforSession encoding:NSUTF8StringEncoding];
//    
//    // NSLog(@"Response From Server = %@", str);
//}
//
//- (NSCachedURLResponse *)connection:(NSURLConnection *)connection
//                  willCacheResponse:(NSCachedURLResponse*)cachedResponse {
//    
//    return nil;
//}

//- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
//    
//    if (responseDataforSession == (id)[NSNull null]|| responseDataforSession== NULL || responseDataforSession== nil || (NSNull *)responseDataforSession == [NSNull null])
//    {
//        
//        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"BuyTimee" message:@"Unable to process request, please try again." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//        [alert show];
//        
//    }
//    else
//    {
//        //  NSLog(@"response from url -=-- %@",responseDataforSession);
//        jsonDict = nil;
//        if (responseDataforSession != nil) {
//            
//            jsonDict = [NSJSONSerialization JSONObjectWithData:responseDataforSession
//                                                       options:NSJSONReadingMutableContainers
//                                                         error:nil];
//            
//            
//            
//            
//            
//            if ([NSStringFromClass(self.VCObj.class) isEqualToString:@"LoginViewController"]) {
//                
//                     [(LoginViewController*)self.VCObj finishLoading:jsonDict];
//                    return;
//                }
//   
////            NSString *rescode=[EncryptDecryptFile decrypt:[jsonDict valueForKey:@"responseCode"]];
////            NSString *resMsg=[EncryptDecryptFile decrypt:[jsonDict valueForKey:@"responseMessage"]];
//           
//
//        }
//        
//        
//        
//    }
//
//}

//- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
//    NSString *msg = [error localizedDescription];
//
//    if (msg.length==0 || msg==nil) {
//        
//        msg = @"Unable to process request, please try again.";
//    }
//    NSLog(@"Connection DidFailWithError");
//    NSLog(@"connection failed with error %@", error);
// 
//}





@end
