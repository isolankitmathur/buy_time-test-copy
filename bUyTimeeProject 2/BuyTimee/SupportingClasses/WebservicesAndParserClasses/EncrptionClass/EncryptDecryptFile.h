//
//  EncryptDecryptFile.h
//  
//
//  Created by  ~ on 14/03/16.
//  Copyright (c) 2016 . All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EncryptDecryptFile : NSObject

+(NSString *)EncryptIT:(NSString *)encryptvalue;
+(NSString *)decrypt:(NSString *) dataToDecrypt;
+(NSString *)urlencode:(NSString *)string;
+(NSString *)md5 :(NSString *) input;

@end
