
//
//  EncryptDecryptFile.m
//  
//
//  Created by  ~ on 14/03/16.
//  Copyright (c) 2016 . All rights reserved.
//

#import "EncryptDecryptFile.h"
#import <CommonCrypto/CommonCryptor.h>
#import <CommonCrypto/CommonDigest.h>

@implementation EncryptDecryptFile

+(NSString *)EncryptIT:(NSString *)encryptvalue{
    
    NSString* key = @"q9Q5Di4Dsi5QtsarQhWxY7PDAEAUcqyk";
    
    char keyPtr[kCCKeySizeAES256+1];
    bzero( keyPtr, sizeof(keyPtr) );
    
    [key getCString: keyPtr maxLength: sizeof(keyPtr) encoding:NSUTF8StringEncoding];
    size_t numBytesEncrypted = 0;
    
    NSData *msg = [encryptvalue dataUsingEncoding:NSUTF8StringEncoding];
    size_t bufferSize = msg.length + kCCKeySizeAES256;
    void *buffer = malloc(bufferSize);
    const unsigned char iv[] = {0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00};
    
    CCCryptorStatus result = CCCrypt( kCCEncrypt,
                                     kCCAlgorithmAES128,
                                     kCCOptionPKCS7Padding,
                                     keyPtr,
                                     kCCKeySizeAES256,
                                     iv,
                                     msg.bytes, msg.length,
                                     buffer, bufferSize,
                                     &numBytesEncrypted );
    
    if( result == kCCSuccess ){
        //        return [NSData dataWithBytesNoCopy:buffer length:numBytesEncrypted];
        NSData *myData = [NSData dataWithBytesNoCopy:buffer length:numBytesEncrypted];
        NSString *base64Encoded = [myData base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
        return base64Encoded;
    }
    else {
        NSLog(@"Failed AES");
    }
    return @"";
    
}

+ (NSString *) decrypt:(NSString *) dataToDecrypt{
    
    if (!dataToDecrypt) {
        
        NSLog(@"the data to decrypt is nil");
        
//        return @"";
        
    }
    
    if ([dataToDecrypt isEqualToString:@""]) {
        
        NSLog(@"the data to decrypt is blank");

//        return @"";
        
    }
    
    NSString* key = @"q9Q5Di4Dsi5QtsarQhWxY7PDAEAUcqyk";
    
    char keyPtr[kCCKeySizeAES256+1];
    bzero( keyPtr, sizeof(keyPtr) );
    
    [key getCString: keyPtr maxLength: sizeof(keyPtr) encoding:NSUTF8StringEncoding];
    size_t numBytesEncrypted = 0;
    
    NSData *msg = [[NSData alloc] initWithBase64EncodedString:dataToDecrypt options:0];
    
    size_t bufferSize = msg.length + kCCKeySizeAES256;
    void *buffer = malloc(bufferSize);
    const unsigned char iv[] = {0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00};
    
    CCCryptorStatus result = CCCrypt( kCCDecrypt,
                                     kCCAlgorithmAES128,
                                     kCCOptionPKCS7Padding,
                                     keyPtr,
                                     kCCKeySizeAES256,
                                     iv,
                                     msg.bytes, msg.length,
                                     buffer, bufferSize,
                                     &numBytesEncrypted );
    
    if( result == kCCSuccess ){
        
        NSData *myData = [NSData dataWithBytesNoCopy:buffer length:numBytesEncrypted];
        NSString *decodedString = [[NSString alloc] initWithData:myData encoding:NSUTF8StringEncoding];
        return decodedString;
    }
    else {
        NSLog(@"Failed AES");
    }
    return @"";
    
}


+(NSString *)urlencode:(NSString *)string {
    
    NSMutableString *output = [NSMutableString string];
    const unsigned char *source = (const unsigned char *)[string UTF8String];
    int sourceLen = strlen((const char *)source);
    for (int i = 0; i < sourceLen; ++i) {
        const unsigned char thisChar = source[i];
        if (thisChar == ' '){
            [output appendString:@"+"];
        } else if (thisChar == '.' || thisChar == '-' || thisChar == '_' || thisChar == '~' ||
                   (thisChar >= 'a' && thisChar <= 'z') ||
                   (thisChar >= 'A' && thisChar <= 'Z') ||
                   (thisChar >= '0' && thisChar <= '9')) {
            [output appendFormat:@"%c", thisChar];
        } else {
            [output appendFormat:@"%%%02X", thisChar];
        }
    }
    return output;
    
}


+(NSString *)md5 :(NSString *) input{
    
    const char *cStr = [input UTF8String];
    unsigned char result[16];
    CC_MD5( cStr, strlen(cStr), result);
    return [NSString stringWithFormat:
            @"%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x",
            result[0], result[1], result[2], result[3],
            result[4], result[5], result[6], result[7],
            result[8], result[9], result[10], result[11],
            result[12], result[13], result[14], result[15]];
}


@end
