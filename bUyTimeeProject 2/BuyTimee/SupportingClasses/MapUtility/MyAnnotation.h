//
//  MyAnnotation.h
//  Patrobuddy
//
//  Created by User14 on 30/11/15.
//
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface MyAnnotation : NSObject <MKAnnotation>

- (id)initWithName:(NSString*)name address:(NSString*)addres subtitle:(NSString *)subTitl userImage:(UIImage *)userImg coordinate:(CLLocationCoordinate2D)coordinate andTag:(int)tag;
- (MKMapItem*)mapItem;

@property (nonatomic, assign) CLLocationCoordinate2D theCoordinate;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *subTitle;
@property (nonatomic, copy) NSString *address;
@property(nonatomic,assign) int tagTaskId;
@property (nonatomic, copy) UIImage *imgUser;

@end
