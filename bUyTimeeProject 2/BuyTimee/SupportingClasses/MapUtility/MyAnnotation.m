//
//  MyAnnotation.m
//  Patrobuddy
//
//  Created by User14 on 30/11/15.
//
//

#import "MyAnnotation.h"
#import <AddressBook/AddressBook.h>

@interface MyAnnotation ()

@end

@implementation MyAnnotation

@synthesize theCoordinate,title,subTitle,tagTaskId,address,imgUser;

- (id)initWithName:(NSString*)name address:(NSString*)addres subtitle:(NSString *)subTitl userImage:(UIImage *)userImg coordinate:(CLLocationCoordinate2D)coordinate andTag:(int)tag{
    if ((self = [super init])) {
        if ([name isKindOfClass:[NSString class]]) {
            title = name;
        } else {
            title = @"Unknown charge";
        }
        address = addres;
        subTitle = subTitl;
        imgUser = userImg;
        theCoordinate = coordinate;
        tagTaskId = tag;
    }
    return self;
}

- (NSString *)title {
    return title;
}

- (NSString *)subtitle {
    return subTitle;
}

- (CLLocationCoordinate2D)coordinate {
    return theCoordinate;
}

- (NSString *)address {
    return address;
}

- (UIImage *)imgUser {
    return imgUser;
}

-(int)tagTaskId{
    return tagTaskId;
}

- (MKMapItem*)mapItem {
    NSDictionary *addressDict = @{(NSString*)kABPersonAddressStreetKey : subTitle};
    
    MKPlacemark *placemark = [[MKPlacemark alloc]
                              initWithCoordinate:self.coordinate
                              addressDictionary:addressDict];
    
    MKMapItem *mapItem = [[MKMapItem alloc] initWithPlacemark:placemark];
    mapItem.name = self.title;
    
    return mapItem;
}

@end
