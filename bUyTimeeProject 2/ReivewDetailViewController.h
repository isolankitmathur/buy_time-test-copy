//
//  ReivewDetailViewController.h
//  BuyTimee
//
//  Created by mitesh on 31/03/17.
//  Copyright © 2017 User14. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReivewDetailViewController : UIViewController<UIGestureRecognizerDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *sliderImageView;

@property (weak, nonatomic) IBOutlet UIView *viewCirleImageView;
@property (weak, nonatomic) IBOutlet UIImageView *circleImgOne;
@property (weak, nonatomic) IBOutlet UIImageView *circleImgTwo;
@property (weak, nonatomic) IBOutlet UIImageView *circleImgThree;
@property (weak, nonatomic) IBOutlet UIImageView *circleImgFour;
@property (weak, nonatomic) IBOutlet UIImageView *circleImgFive;

@property (weak, nonatomic) IBOutlet UIView *firstDateMnetionView;
@property (weak, nonatomic) IBOutlet UIView *secondAddressView;

@property (weak, nonatomic) IBOutlet UILabel *firstViewHeadingLabel;
@property (weak, nonatomic) IBOutlet UILabel *secondViewHeadingLabel;

@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UILabel *firstViewDetailLabel;

@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UILabel *secondViewDetailLabel;

@property (weak, nonatomic) IBOutlet UILabel *firstViewLine;
@property (weak, nonatomic) IBOutlet UILabel *secondViewLine;

@property (weak, nonatomic) IBOutlet UIImageView *profileImgView;
@property (weak, nonatomic) IBOutlet UILabel *postedByLabel;

@property (weak, nonatomic) IBOutlet UIImageView *starOne;
@property (weak, nonatomic) IBOutlet UIImageView *starTwo;
@property (weak, nonatomic) IBOutlet UIImageView *starThree;
@property (weak, nonatomic) IBOutlet UIImageView *starFour;
@property (weak, nonatomic) IBOutlet UIImageView *starFive;

@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;


@property(nonatomic,strong)UISwipeGestureRecognizer*leftSwipe;
@property(nonatomic,strong)UISwipeGestureRecognizer*rightSwipe;


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consLblDetailFirstViewHeight;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consLblDetailSecondViewHeight;
@property (weak, nonatomic) IBOutlet UILabel *labelInstaLink;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consFirstViewHeight;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consSecondViewHeight;


//************************************

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *scrollViewWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *scrollViewHeight;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentViewWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentViewHeght;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *sliderImgViewHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewCircleImgViewTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewCircleImgViewLeft;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consFirstViewHeadingLblLeft;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consFirstViewHeadingLblTop;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consFirstViewDateLblLeft;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consFirstViewDateLblTop;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consPriceLblWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consPriceLblRight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consPriceLblTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consFirstViewDetailLblLeft;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consFirstViewDEtailLblTop;


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consSecondViewHeadingLblTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consSecondViewHeadingLblLeft;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consAddressLblTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consAddressLblLeft;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consInstaLinkTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consInstaLinkLeft;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consSecondViewDetailLblTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consSecondViewDeatilLblLeft;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consSecondViewLblAddressHeight;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consProfileImgViewHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consProfileImgViewWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consProfileImgViewTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consProfileImgViewLeft;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consPostedBuLeft;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consPostedByTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consPostedByLblWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consBtnCloseWebViewTrailing;


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consStarOneLeft;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consStarTwoLeft;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consStarThreeLeft;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consStarFourLeft;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consStarFiveLeft;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consStarOneTop;


@property (weak, nonatomic) IBOutlet UIView *viewImageViewAndActivityHolder;

@property (weak, nonatomic) IBOutlet UIView *shareReviewDetailReviewView;

@property (weak, nonatomic) IBOutlet UIButton *shareBtn;
- (IBAction)shareBtnClicked:(id)sender;


@property (weak, nonatomic) IBOutlet UIView *webBlurView;

@property (weak, nonatomic) IBOutlet UIWebView *instaWebView;

@property (weak, nonatomic) IBOutlet UIButton *btnCloseWebView;

- (IBAction)pressCloseWebBtn:(id)sender;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consBtnCloseWebViewHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consBtnCloseWebViewWidth;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consBtnCloseWebViewTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consBlurViewHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consWebViewBottom;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consWebViewTop;
@property (weak, nonatomic) IBOutlet UILabel *firstviewDiscountDescriptionLbl;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consDiscountDesciptionLblTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consDiscountDescriptionLblLeft;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *activityIndicatorLeft;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *activityIndicatorTop;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *nsConsLblAddressWidth;


@property (weak, nonatomic) IBOutlet UIButton *editOrShareBtn;

- (IBAction)editOrshareClicked:(id)sender;

@property (strong, nonatomic) IBOutlet UILabel *lblTransactionLink;


@end
