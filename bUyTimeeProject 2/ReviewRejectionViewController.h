//
//  ReviewRejectionViewController.h
//  BuyTimee
//
//  Created by mitesh on 30/03/17.
//  Copyright © 2017 User14. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Flurry.h"

@interface ReviewRejectionViewController : UIViewController<UITextViewDelegate>
@property (weak, nonatomic) IBOutlet UITextView *reasonTextView;
@property (weak, nonatomic) IBOutlet UILabel *reasonForRejectionLbl;
@property (weak, nonatomic) IBOutlet UIButton *continueBtn;
- (IBAction)continueBtnClciked:(id)sender;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *reasonLblLeft;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *rejectionLblTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *rejectionLblHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *rejectionLblWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *reasonTxtViewTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *reasonTxtViewLeft;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *reasonTxtViewHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *continueBtnBottom;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *continueBtnHeight;


@property (nonatomic, retain) UITextView *txtActiveField;
@property (nonatomic, retain) UIView *inputAccView;
@property (nonatomic, retain) UIView *DatePickerInputAccView;
@property (nonatomic, retain) UIButton *btnDone;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *reasonTxtViewWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *continueBtnTop;



@end
